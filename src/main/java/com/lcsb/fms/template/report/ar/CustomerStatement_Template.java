/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.template.report.ar;

import com.lcsb.fms.dao.financial.ar.ArReportDAO;
import com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO;
import com.lcsb.fms.dao.financial.tx.TaxOutputDAO;
import com.lcsb.fms.dao.setup.configuration.BuyerDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.ar.ArGLParam;
import com.lcsb.fms.model.financial.ar.ArReportStatement;
import com.lcsb.fms.model.financial.cashbook.OfficialCreditItem;
import com.lcsb.fms.model.financial.cashbook.OfficialReceipt;
import com.lcsb.fms.model.financial.gl.GeneralLedger;
import com.lcsb.fms.model.setup.configuration.Buyer;
import com.lcsb.fms.template.voucher.FormattedVoucher;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.dao.EstateDAO;
import com.lcsb.fms.util.model.Estate;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class CustomerStatement_Template {
    
    public static FormattedVoucher printReport(ArGLParam v, LoginProfile log) throws Exception {

        FormattedVoucher fv = new FormattedVoucher();
        int length = 28;
        
        List<ArReportStatement> listx = (List<ArReportStatement>) ArReportDAO.getAllStatement(log, v,0,0, true);
        
        
        int totPage = listx.size();
        int page = totPage / length;
        int modulo = totPage % length;
        int perPage = 0;
        String output = "";
        double totalEachDebit = 0.0;
        double totalEachCredit = 0.0;
        double totalEachBalance = 0.0;

        if (modulo > 0) {
            page = page + 1;
        }

        perPage = page * 30;

        Logger.getLogger(TaxOutputDAO.class.getName()).log(Level.INFO, "-------" + page);

        for (int j = 0; j < page; j++) {

            FormattedVoucher fm = getTemplate(v, log, j + 1, j * length, length, page, totalEachDebit, totalEachCredit, totalEachBalance);

            output += fm.getOutput();
            totalEachDebit += fm.getTotalEachDebit();
            totalEachCredit += fm.getTotalEachCredit();
            totalEachBalance += fm.getTotalEachBalance();

        }
        
        fv.setModuleid(v.getModuleid());
        fv.setOutput(output);
        fv.setPage(page);
        //fv.setRefer(v.getRefer());

        return fv;
    }

    private static int getCount(LoginProfile log, String refno) throws Exception {

        int i = 0;

        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from cb_official_credit where voucherno = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            i = rs.getInt(1);
        }

        return i;
    }

    private static FormattedVoucher getTemplate(ArGLParam v, LoginProfile log, int page, int row, int length, int totalPage, double totalCFDebit, double totalCFCredit, double totalCFBalance ) throws Exception {

        FormattedVoucher fm = new FormattedVoucher();

        int i = row;
        int k = 0;
         double totalDebit = 0.0;
        double totalCredit = 0.0;
        double totalBalance = 0.0;

        Estate est = EstateDAO.getEstateInfo(log, log.getEstateCode(), "estatecode");

        List<ArReportStatement> slist = (List<ArReportStatement>) ArReportDAO.getAllStatement(log, v, row, length, true);
        
        Buyer vi = (Buyer) BuyerDAO.getInfo(log, v.getBuyercode());

        String report = "<div class=\"page\">\n"
                + "                                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\">\n"
                + "                                                        <tr>\n"
                + "                                                            <td>\n"
                + "                                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td width=\"50%\">\n"
                + "                                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td width=\"10%\"><img src=\"" + est.getLogopath() + "\" width=\"25\" height=\"25\"></td>\n"
                + "                                                                                    <td width=\"90%\">&nbsp;<span class=\"comnamevc\"><strong>" + log.getEstateDescp() + "</strong></span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                            </table>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td align=\"right\"><strong>STATEMENT</strong></td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td>\n"
                + "                                                                            <p class=\"medfont2\">&nbsp;</p>\n"
                + "                                                                            <p class=\"medfont3\">" + est.getAddress() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">" + est.getPostcode() + " " + est.getCity() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">" + est.getState() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">&nbsp;</p>\n"
                + "                                                                            <p class=\"medfont3\">Tel : " + est.getPhone() + ", Fax : " + est.getFax() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">&nbsp;</p>\n"
                + "                                                                            <p class=\"medfont3\">GST ID : " + est.getGstid() + "</p>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td valign=\"top\">\n"
                + "                                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td width=\"20%\">\n"
                + "                                                                                        <span class=\"boldtitle\">To</span>\n"
                + "                                                                                    </td>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"normtitle\">" + vi.getCompanyname()+ "</span>  \n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"boldtitle\"></span>\n"
                + "                                                                                    </td>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"normtitle\">" + vi.getAddress() + ", " + vi.getPostcode()+ "</span>  \n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"boldtitle\"></span>\n"
                + "                                                                                    </td>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"normtitle\">" + vi.getCity()+ ", " + vi.getState()+ "</span>  \n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                + "                                                                            </table>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td valign=\"top\">\n"
                + "                                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"bordertopgrey\" width=\"40%\">&nbsp;<span class=\"boldtitle\">PAGE</span></td>\n"
                + "                                                                                    <td class=\"bordertoprightgrey\" width=\"60%\">&nbsp;<span class=\"normtitle\">" + page + " of " + totalPage + "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">REFERENCE NO</span></td>\n"
                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\"></span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">STATEMENT DATE</span></td>\n"
                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + AccountingPeriod.getCurrentTimeStamp() + "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                            </table>\n"
                + "                                                                        </td>\n"
                + "                                                                    </tr>\n"
                + "                                                                </table>\n"
                + "                                                            </td>\n"
                + "                                                        </tr>\n"
                + "\n"
                + "                                                        <tr>\n"
                + "                                                            <td>\n"
                + "                                                                <p>&nbsp;</p>\n"
                + "                                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                      <tr>\n"
                + "                                                                    <td width=\"2%\" class=\"bordertop\">&nbsp;<span class=\"intableheader_jv\">No</span></td>\n"
                + "                                                                    <td width=\"15%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Date</span></td>\n"
                + "                                                                    <td width=\"11%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Type</span></td>\n"
                + "                                                                    <td width=\"18%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Reference No</span></td>\n"
                + "                                                                    <td width=\"10%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Cheque</span></td>\n"
                + "                                                                    <td width=\"12%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Debit</span></td>\n"
                + "                                                                    <td width=\"12%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Credit</span></td>\n"
                + "                                                                    <td width=\"12%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Balance</span></td>\n"
                + "                                                                </tr>\n"
                + "\n"
                + "\n"
                + "\n";
     
        if(page > 1){
            report += "                                                                    <tr height=\"20px\">\n"
                    + "                                                                        <td class=\"borderleft\">&nbsp;<span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"left\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">B/F</span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.normalCredit(totalCFDebit) +"</span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.normalCredit(totalCFCredit) +"</span></td>\n"
                     + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.normalCredit(totalCFBalance) +"</span></td>\n"
                    + "                                                                    </tr>\n";
        }
        
        totalDebit+=totalCFDebit;
        totalCredit+=totalCFCredit;
               
        for (ArReportStatement c : slist) {
            i++;
            k++;
            
            totalDebit+=c.getDebit();
            totalCredit+=c.getCredit();
            //tot += Double.parseDouble(c.getAmount());

            report += "<tr height=\"20px\"><td class=\"borderleft\"><span class=\"fontjv_item\">"+ i +"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\"><span class=\"fontjv_item\">"+ AccountingPeriod.fullDateMonth(c.getDate()) +"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"left\"><span class=\"fontjv_item\">"+ c.getType()+"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"left\"><span class=\"fontjv_item\">"+ c.getRefno()+"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"left\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.currencyFormat( c.getDebit()) +"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.currencyFormat( c.getCredit()) +"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.normalCredit(totalDebit-totalCredit) +"</span></td>\n"
                    + "                                                                </tr>";

        }
        
         report += "                                                                    <tr height=\"20px\">\n"
                    + "                                                                        <td class=\"borderleft\">&nbsp;<span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"left\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                     + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                    </tr>\n";
        
          
        for (int j = 0; j < 25 - k; j++) {

            report += "                                                                    <tr height=\"20px\">\n"
                    + "                                                                        <td class=\"borderleft\">&nbsp;<span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"left\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                     + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                    </tr>\n";
        }
        
        if(page!=totalPage){
        report += "                                                                    <tr height=\"20px\">\n"
                    + "                                                                        <td class=\"borderbottomboldnoright\">&nbsp;<span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td  class=\"borderbottomboldright\" align=\"left\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderbottomboldright\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderbottomboldright\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderbottomboldright\" style=\"padding-right:10px;\" align=\"right\"><span class=\"fontjv_item\">C/F</span></td>\n"
                    + "                                                                        <td class=\"borderbottomboldright\" style=\"padding-right:10px;\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.currencyFormat(totalDebit) +" </span></td>\n"
                    + "                                                                        <td class=\"borderbottomboldright\" style=\"padding-right:10px;\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.normalCredit(totalCredit) +" </span></td>\n"
                     + "                                                                        <td class=\"borderrightendtotal\" style=\"padding-right:10px;\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.normalCredit((totalDebit)-(totalCredit)) +" </span></td>\n"
                    + "                                                                    </tr>\n";
        }
        
        if(page==totalPage){
        report += "                                                                    <tr height=\"20px\">\n"
                    + "                                                                        <td class=\"borderbottomboldnoright\">&nbsp;<span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td  class=\"borderbottomboldright\" align=\"left\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderbottomboldright\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderbottomboldright\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderbottomboldright\" style=\"padding-right:10px;\" align=\"right\"><span class=\"fontjv_item\"><strong>Total</strong></span></td>\n"
                    + "                                                                        <td class=\"borderbottomboldright\" style=\"padding-right:10px;\" align=\"right\"><span class=\"fontjv_item\"><strong>"+ GeneralTerm.currencyFormat(totalDebit) +" </strong></span></td>\n"
                    + "                                                                        <td class=\"borderbottomboldright\" style=\"padding-right:10px;\" align=\"right\"><span class=\"fontjv_item\"><strong>"+ GeneralTerm.normalCredit(totalCredit) +" </strong></span></td>\n"
                     + "                                                                        <td class=\"borderrightendtotal\" style=\"padding-right:10px;\" align=\"right\"><span class=\"fontjv_item\"><strong>"+ GeneralTerm.normalCredit((totalDebit)-(totalCredit)) +" </strong></span></td>\n"
                    + "                                                                    </tr>\n";
        }
        report += "<tr>\n"
                + "                                                                    <td class=\"borderbottombold\" colspan=\"10\"></td>\n"
                + "                                                                </tr>\n"
                + "                                                                </table>\n"
                + "                                                                <p>&nbsp;</p>\n"
                + "                                                            </td>\n"
                + "                                                        </tr>\n"
                + "                                                    </table>    \n"
                + "                                                </div>";

        fm.setOutput(report);
        fm.setTotalEachDebit(totalDebit-totalCFDebit);
        fm.setTotalEachCredit(totalCredit-totalCFCredit);
        fm.setTotalEachBalance((totalDebit-totalCFDebit)-(totalCredit-totalCFCredit));
        //fm.setRefer(v.getRefer());
        fm.setPage(totalPage);

        return fm;

    }
    
}
