/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.template.voucher;

import com.lcsb.fms.dao.financial.ap.VendorInvoiceDAO;
import com.lcsb.fms.dao.financial.ar.ArCreditNoteDAO;
import com.lcsb.fms.dao.financial.ar.ArDebitNoteDAO;
import com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO;
import com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO;
import com.lcsb.fms.dao.financial.cashbook.PaymentVoucherDAO;
import com.lcsb.fms.dao.financial.gl.CreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.DebitNoteDAO;
import com.lcsb.fms.dao.financial.gl.JournalVoucherDAO;
import com.lcsb.fms.model.financial.ap.VendorInvoice;
import com.lcsb.fms.model.financial.ar.ArCreditNote;
import com.lcsb.fms.model.financial.ar.ArDebitNote;
import com.lcsb.fms.model.financial.ar.SalesInvoice;
import com.lcsb.fms.model.financial.cashbook.OfficialReceipt;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucher;
import com.lcsb.fms.model.financial.gl.GLCreditNote;
import com.lcsb.fms.model.financial.gl.GLDebitNote;
import com.lcsb.fms.model.financial.gl.JournalVoucher;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class VoucherMaster {
    
    public static FormattedVoucher getMaster(String refer, LoginProfile log) throws Exception{
        
        FormattedVoucher fv = new FormattedVoucher();
        Module  mod = getType(log, refer);
        Logger.getLogger(VoucherMaster.class.getName()).log(Level.INFO, "-------" + mod.getModuleDesc());
        switch (mod.getModuleID()) {
            case "020204":
                {
                    PaymentVoucher v = (PaymentVoucher) PaymentVoucherDAO.getPYV(log, refer);
                    fv = PaymentVoucher_Template.printPV(v, log);
                    fv.setModuleid(mod.getModuleID());
                    break;
                }
            case "021009":
                {
                    PaymentVoucher v = (PaymentVoucher) PaymentVoucherDAO.getPYV(log, refer);
                    fv = PaymentVoucher_Template.printPV(v, log);
                    fv.setModuleid(mod.getModuleID());
                    break;
                }
            case "020203":
                {
                    OfficialReceipt v = (OfficialReceipt) OfficialReceiptDAO.getINV(log, refer);
                    fv = OfficialReceipt_Template.printPV(v, log);
                    fv.setModuleid(mod.getModuleID());
                    break;
                }
            case "020104":
                {
                    JournalVoucher v = (JournalVoucher) JournalVoucherDAO.getJV(log, refer);
                    fv = JournalVoucher_Template.printPV(v, log);
                    fv.setModuleid(mod.getModuleID());
                    break;
                }
            case "020903":
                {
                    SalesInvoice v = (SalesInvoice) SalesInvoiceDAO.getINV(log, refer);
                    fv = SalesInvoice_Template.printPV(v, log);
                    fv.setModuleid(mod.getModuleID());
                    break;
                }
            case "020108":
                {
                    GLDebitNote v = (GLDebitNote) DebitNoteDAO.getDNE(log, refer);
                    fv = DebitNote_Template.printPV(v, log);
                    fv.setModuleid(mod.getModuleID());
                    break;
                }
            case "020109":
                {
                    GLCreditNote v = (GLCreditNote) CreditNoteDAO.getCNE(log, refer);
                    fv = CreditNote_Template.printPV(v, log);
                    fv.setModuleid(mod.getModuleID());
                    break;
                }
            case "020909"://Sales Credit Note
                {
                    ArCreditNote v = (ArCreditNote) ArCreditNoteDAO.getMain(log, refer);
                    fv = ArCreditNote_Template.printPV(v, log);
                    fv.setModuleid(mod.getModuleID());
                    break;
                }
            case "020908"://Sales Debit Note
                {
                    ArDebitNote v = (ArDebitNote) ArDebitNoteDAO.getMain(log, refer);
                    fv = ArDebitNote_Template.printPV(v, log);
                    fv.setModuleid(mod.getModuleID());
                    break;
                }
            case "021008"://Invoice
                {
                    VendorInvoice v = (VendorInvoice) VendorInvoiceDAO.getPNV(log, refer);
                    fv = VendorInvoice_Template.printPV(v, log);
                    fv.setModuleid(mod.getModuleID());
                    break;
                }
            default:
                break;
        }
        
        return fv;
        
    }
    
    private static Module getType(LoginProfile log, String refer) throws Exception{
        
        Module mod = new Module();
        String abb = "";
        
        abb = refer.substring(0, 3);
        
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select *  from sec_module where abb LIKE ?");
        stmt.setString(1, "%"+abb+"%");
        rs = stmt.executeQuery();
        if (rs.next()) {
            
            mod.setModuleID(rs.getString("moduleid"));
            mod.setModuleDesc(rs.getString("moduledesc"));
            mod.setAbb(rs.getString("abb"));
            
        }
        
        return mod;
        
    }
    
}
