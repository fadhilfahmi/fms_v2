/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.template.voucher;

import com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO;
import com.lcsb.fms.dao.financial.tx.TaxOutputDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.cashbook.CbOfficialCek;
import com.lcsb.fms.model.financial.cashbook.OfficialCreditItem;
import com.lcsb.fms.model.financial.cashbook.OfficialReceipt;
import com.lcsb.fms.util.dao.EstateDAO;
import com.lcsb.fms.util.model.Estate;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class OfficialReceipt_Template {

    public static FormattedVoucher printPV(OfficialReceipt v, LoginProfile log) throws Exception {

        FormattedVoucher fv = new FormattedVoucher();
        int length = 10;
        int totPage = getCount(log, v.getRefer());
        int page = totPage / length;
        int modulo = totPage % length;
        int perPage = 0;
        String output = "";
        double totalEachWithoutTax = 0.0;
        double totalEachTax = 0.0;

        if (modulo > 0) {
            page = page + 1;
        }

        perPage = page * 10;

        Logger.getLogger(TaxOutputDAO.class.getName()).log(Level.INFO, "-------" + page);

        for (int j = 0; j < page; j++) {

            FormattedVoucher fm = getTemplate(v, log, j + 1, j * length, length, page, totalEachWithoutTax, totalEachTax);

            output += fm.getOutput();
            //totalEach += fm.getTotalEachPage();
            totalEachWithoutTax += fm.getTotalEachWithoutTax();
            totalEachTax += fm.getTotalEachPageTax();

        }

        fv.setOutput(output);
        fv.setPage(page);
        fv.setRefer(v.getRefer());

        return fv;
    }

    private static int getCount(LoginProfile log, String refno) throws Exception {

        int i = 0;

        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from cb_official_credit where voucherno = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            i = rs.getInt(1);
        }

        return i;
    }

    private static FormattedVoucher getTemplate(OfficialReceipt v, LoginProfile log, int page, int row, int length, int totalPage, double totalCFwithouttax, double totalCFtax) throws Exception {

        FormattedVoucher fm = new FormattedVoucher();

        int i = row;
        int k = 0;
        double tot = 0.0;
        
        double totalwithouttax = 0.0;
        double totaltax = 0.0;

        Estate est = EstateDAO.getEstateInfo(log, log.getEstateCode(), "estatecode");
        CbOfficialCek cek = (CbOfficialCek) OfficialReceiptDAO.getCheque(log, v.getRefer());

        List<OfficialCreditItem> slist = (List<OfficialCreditItem>) OfficialReceiptDAO.getAllINVItemByPage(log, v.getRefer(), row, length);

        String report = "<div class=\"page\">\n"
                + "                                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\">\n"
                + "                                                        <tr>\n"
                + "                                                            <td>\n"
                + "                                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td width=\"50%\">\n"
                + "                                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td width=\"10%\"><img src=\"" + est.getLogopath() + "\" width=\"25\" height=\"25\"></td>\n"
                + "                                                                                    <td width=\"90%\">&nbsp;<span class=\"comnamevc\"><strong>" + log.getEstateDescp() + "</strong></span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                            </table>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td align=\"right\"><strong>OFFICIAL RECEIPT</strong></td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td>\n"
                + "                                                                            <p class=\"medfont2\">&nbsp;</p>\n"
                + "                                                                            <p class=\"medfont3\">" + est.getAddress() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">" + est.getPostcode() + " " + est.getCity() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">" + est.getState() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">&nbsp;</p>\n"
                + "                                                                            <p class=\"medfont3\">Tel : " + est.getPhone() + ", Fax : " + est.getFax() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">&nbsp;</p>\n"
                + "                                                                            <p class=\"medfont3\">GST ID : " + est.getGstid() + "</p>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td valign=\"top\">\n"
                + "                                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td width=\"20%\">\n"
                + "                                                                                        <span class=\"boldtitle\">FROM</span>\n"
                + "                                                                                    </td>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"normtitle\">" + v.getPaidname() + "</span>  \n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"boldtitle\"></span>\n"
                + "                                                                                    </td>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"normtitle\">" + v.getPaidaddress() + ", " + v.getPaidpostcode() + "</span>  \n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"boldtitle\"></span>\n"
                + "                                                                                    </td>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"normtitle\">" + v.getPaidcity() + ", " + v.getPaidstate() + "</span>  \n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                + "                                                                            </table>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td valign=\"top\">\n"
                + "                                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"bordertopgrey\" width=\"40%\">&nbsp;<span class=\"boldtitle\">PAGE</span></td>\n"
                + "                                                                                    <td class=\"bordertoprightgrey\" width=\"60%\">&nbsp;<span class=\"normtitle\">" + page + " of " + totalPage + "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">REFERENCE NO</span></td>\n"
                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + v.getRefer() + "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">DATE</span></td>\n"
                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + AccountingPeriod.fullDateMonth(v.getDate()) + "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">TRX NO</span></td>\n"
                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + v.getVoucherno() + "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">PERIOD</span></td>\n"
                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + v.getPeriod() + " , " + v.getYear() + "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                            </table>\n"
                + "                                                                        </td>\n"
                + "                                                                    </tr>\n"
                + "                                                                </table>\n"
                + "                                                            </td>\n"
                + "                                                        </tr>\n"
                + "\n"
                + "                                                        <tr>\n"
                + "                                                            <td>\n"
                + "                                                                <p>&nbsp;</p>\n"
                + "                                                                <table width=\"100%\" border=\"0\" cellspacing=\"10\" cellpadding=\"10\">\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td width=\"3%\" class=\"bordertop\">&nbsp;<span class=\"intableheader_jv\">No</span></td>\n"
                + "                                                                        <td width=\"50%\" colspan=\"2\" class=\"bordertoprightgrey\" style=\"text-align: center;\">&nbsp;<span class=\"intableheader_jv\">Remarks</span></td>\n"
                + "                                                                        <td width=\"10%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Amount</span></td></tr>\n"
                + "\n"
                + "\n"
                + "\n"
                + "                                                                    <!--<tr height=\"20px\"><td class=\"borderleft\">&nbsp;<span class=\"fontjv_item\"></span></td>\n"
                + "                                                                    <td class=\"borderleft\">&nbsp;<span class=\"fontjv_item\"></span></td>\n"
                + "                                                                    <td class=\"borderrightend\" align=\"right\">&nbsp;<span class=\"fontjv_item\"></span>&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\" align=\"right\">&nbsp;<span class=\"fontjv_item\"></span>&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\" align=\"right\">&nbsp;<span class=\"fontjv_item\">b/f</span>&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\" align=\"right\">&nbsp;<span class=\"fontjv_item\">'+round_decimals(parseFloat(tot_debt),2)+'</span>&nbsp;</td> \n"
                + "                                                                    <td class=\"borderrightend\" align=\"right\">&nbsp;<span class=\"fontjv_item\">'+round_decimals(parseFloat(tot_cred),2)+'</span>&nbsp;</td></tr>-->\n"
                + "\n";
        for (OfficialCreditItem c : slist) {
            i++;
            k++;
            
            totalwithouttax += Double.parseDouble(c.getAmount());
            totaltax += c.getTaxamt();
            //totalqty += c.getQty();
            
            tot += Double.parseDouble(c.getAmount());
            report += "                                                                    <tr height=\"38px\">\n"
                    + "                                                                        <td class=\"borderleft\">&nbsp;<span class=\"fontjv_item\">" + i + "</span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" colspan=\"2\" align=\"left\"><span class=\"normtitle\">" + c.getRemarks() + "</span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"normtitle\">" + GeneralTerm.currencyFormat(Double.parseDouble(c.getAmount())) + "</span></td>\n"
                    + "                                                                    </tr>\n";
        }
        for (int j = 0; j < 10 - k; j++) {

            report += "                                                                    <tr height=\"38px\">\n"
                    + "                                                                        <td class=\"borderleft\">&nbsp;<span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" colspan=\"2\" align=\"left\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                    </tr>\n";
        }
        report += "                                                               <tr>\n"
                + "                                                                        <td class=\"borderleft\">&nbsp;</td>\n"
                + "                                                                        <td class=\"borderrightend\" colspan=\"2\" align=\"right\">&nbsp;</td>\n"
                + "                                                                        <td class=\"borderrightend\" align=\"right\">&nbsp;</td>\n"
                + "                                                                    </tr>     \n"
                + "                                                                    <tr>\n"
                + "                                                                        <td colspan=\"2\"  style=\"vertical-align:top;\" class=\"borderbottomboldnoright\">&nbsp;\n"
                + "                                                                <table border=\"0\" width=\"100%\" cellpadding=\"15\">\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td style=\"vertical-align:top;\" width=\"25%\"><span class=\"intableheader_jv\">&nbsp;&nbsp;Ringgit Malaysia</span></td>\n"
                + "                                                                        <td style=\"vertical-align:top;\">:&nbsp;&nbsp;</td>\n"
                + "                                                                        <td style=\"vertical-align:top;\"><span class=\"fontjv_item\">" + v.getRm() + "</span></td>\n"
//                + "                                                                    </tr>\n"
//                + "                                                                    <tr>\n"
//                + "                                                                        <td style=\"vertical-align:top;\"><span class=\"intableheader_jv\">&nbsp;&nbsp;Bank / Branch</span></td>\n"
//                + "                                                                        <td style=\"vertical-align:top;\">:</td>\n"
//                + "                                                                        <td style=\"vertical-align:top;\"><span class=\"fontjv_item\">&nbsp;&nbsp;" + BankDAO.getBankGeneral(log, v.getBankcode()).getName() + "</span></td>\n"
//                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td style=\"vertical-align:top;\"><span class=\"intableheader_jv\">&nbsp;&nbsp;"+ ((cek.getCekno()== null || cek.getCekno().equals("") || cek.getCekno().equals("null")) ? "Payment " : "Cheque No. ") +" </span></td>\n"
                + "                                                                        <td style=\"vertical-align:top;\">:</td>\n"
                + "                                                                        <td style=\"vertical-align:top;\"><span class=\"fontjv_item\">"+ ((cek.getCekno()== null || cek.getCekno().equals("") || cek.getCekno().equals("null")) ? v.getPaymentmode() : cek.getBank() + " " + cek.getCekno()) + "</span></td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td style=\"vertical-align:top;\"></td>\n"
                + "                                                                        <td style=\"vertical-align:top;\">&nbsp;</td>\n"
                + "                                                                        <td style=\"vertical-align:top;\"></td>\n"
//                + "                                                                    </tr>\n"
//                + "                                                                    <tr>\n"
//                + "                                                                        <td style=\"vertical-align:top;\"><span class=\"intableheader_jv\">&nbsp;&nbsp;"+ ((v.getChequeno()== null || v.getChequeno().equals("") || v.getChequeno().equals("null")) ? "Payment " : "Cheque No. ") +" </span></td>\n"
//                + "                                                                        <td style=\"vertical-align:top;\">:</td>\n"
//                + "                                                                        <td style=\"vertical-align:top;\"><span class=\"fontjv_item\">&nbsp;&nbsp;" + ((v.getChequeno()== null || v.getChequeno().equals("") || v.getChequeno().equals("null")) ? v.getPaymentmode() : v.getChequeno()) + "</span></td>\n"
//                + "                                                                    </tr>\n"
//                + "                                                                    <tr>\n"
//                + "                                                                        <td style=\"vertical-align:top;\"></td>\n"
//                + "                                                                        <td style=\"vertical-align:top;\">&nbsp;</td>\n"
//                + "                                                                        <td style=\"vertical-align:top;\"></td>\n"
//                + "                                                                    </tr>\n"
                + "                                                                </table>\n"
                + "                                                                          </td>\n"
                + "                                                                        <td width=\"15%\" align=\"right\" class=\"borderbottomboldright\">\n"
//                + "                                                                        <span class=\"totalamount_jv\" style=\"font-size:11px;\">&nbsp;&nbsp;<strong>Sub Total</strong></span>&nbsp;<br>\n"
//                + "                                                                        <span class=\"totalamount_jv\" style=\"font-size:11px;\">&nbsp;&nbsp;<strong>Tax</strong></span>&nbsp;<br>\n"
                + "                                                                        <span class=\"totalamount_jv\" style=\"font-size:11px;\">&nbsp;&nbsp;<strong>Total (Exclusive of GST)</strong></span>&nbsp;<br>\n"
                + "                                                                        <span class=\"totalamount_jv\" style=\"font-size:11px;\">&nbsp;&nbsp;<strong>Total GST 6%</strong></span>&nbsp;<br>\n"
                + "                                                                        <span class=\"totalamount_jv\" style=\"font-size:11px;\">&nbsp;&nbsp;<strong>Grand Total</strong></span>&nbsp;\n"
                + "                                                                         </td>\n"
                + "                                                                    <td align=\"right\" class=\"borderbottomboldright\">"
                + "                                                                          <p></p><span class=\"totalamount_jv\" style=\"font-size:11px;\">" + GeneralTerm.currencyFormat(totalwithouttax + totalCFwithouttax) + "<strong></strong></span>&nbsp;<br>"
                + "                                                                          <span class=\"totalamount_jv\" style=\"font-size:11px;\">" + GeneralTerm.currencyFormat(totaltax + totalCFtax) + "<strong></strong></span>&nbsp;<br>"
                + "                                                                          <span class=\"totalamount_jv\" style=\"font-size:11px;\">" + GeneralTerm.currencyFormat(totalwithouttax + totaltax + totalCFtax + totalCFwithouttax) + "<strong></strong></span>&nbsp;<p></p>"
                + "                                                                     </td>\n"
                //+ "                                                                        <td align=\"right\" class=\"borderbottomboldright\" height=\"30px\"><span class=\"fontjv_item\">" + GeneralTerm.currencyFormat(tot + totalCF) + "</span>&nbsp;</td>\n"
                + "                                                                    </tr>\n"
                + "                                                                </table>\n"
                + "                                                                <table border=\"0\" width=\"100%\" cellpadding=\"15\">\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td width=\"40%\" style=\"vertical-align:top;\" width=\"25%\">&nbsp;</td>\n"
                + "                                                                        <td width=\"20%\" style=\"vertical-align:top;\"></td>\n"
                + "                                                                        <td width=\"40%\" style=\"vertical-align:top;\"></span></td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td style=\"vertical-align:top;\" width=\"25%\">&nbsp;</td>\n"
                + "                                                                        <td style=\"vertical-align:top;\"></td>\n"
                + "                                                                        <td style=\"vertical-align:top;\"></span></td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td style=\"vertical-align:top;\" width=\"25%\">&nbsp;</td>\n"
                + "                                                                        <td style=\"vertical-align:top;\"></td>\n"
                + "                                                                        <td style=\"vertical-align:top;\"><span class=\"normtitle\">Please Note : This receipt is only valid upon cheque clearing.</span></td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td colspan=\"2\" style=\"vertical-align:top;\" width=\"25%\"><span class=\"normtitle\">Issued By : __________________________</span><br><br><span class=\"normtitle\">Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: __________________________</span></td>\n"
                + "                                                                        <td style=\"vertical-align:top;\"></td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td style=\"vertical-align:top;\" width=\"25%\">&nbsp;</td>\n"
                + "                                                                        <td style=\"vertical-align:top;\"></td>\n"
                + "                                                                        <td style=\"vertical-align:top;\"></span></td>\n"
                + "                                                                    </tr>\n"
                + "                                                                </table>\n"
                + "                                                            </td>\n"
                + "                                                        </tr>\n"
//                + "                                                        <tr>\n"
//                + "                                                            <td><br><br<br><br<br><br<br><br>\n"
//                + "                                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
//                + "                                                                    <tr>\n"
//                + "                                                                        <td width=\"33%\" >\n"
//                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
//                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
//                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;_________________________</p>\n"
//                + "                                                                            <p class=\"paytofontmiddle\">&nbsp;&nbsp;<strong>Prepared By</strong></p>\n"
//                + "                                                                        </td>\n"
//                + "                                                                        <td width=\"33%\">\n"
//                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
//                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
//                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;_________________________</p>\n"
//                + "                                                                            <p class=\"paytofontmiddle\">&nbsp;&nbsp;<strong>Checked By</strong></p>\n"
//                + "                                                                        </td>\n"
//                + "                                                                        <td width=\"33%\">\n"
//                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
//                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
//                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;_________________________</p>\n"
//                + "                                                                            <p class=\"paytofontmiddle\">&nbsp;&nbsp;<strong>Approved By</strong></p>\n"
//                + "                                                                        </td>\n"
//                + "                                                                    </tr>\n"
//                + "                                                                    <tr>\n"
//                + "                                                                        <td valign=\"top\">\n"
//                //+ "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Name : <strong>" + v.getPrename() + "</strong></p>\n"
//                //+ "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Date : <strong>" + AccountingPeriod.fullDateMonth(v.getPreparedate()) + "</strong></p>\n"
//                + "                                                                        </td>\n"
//                + "                                                                        <td  valign=\"top\">\n"
//                + "                                                                            <p class=\"paytofontmiddle\">&nbsp;&nbsp;</p>\n"
//                //+ "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Name : <strong>" + v.getCheckname()+ "</strong></p>\n"
//                //+ "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Date : <strong>" + AccountingPeriod.fullDateMonth(v.getApprovedate()) + "</strong></p>\n"
//                + "                                                                        </td>\n"
//                + "                                                                        <td  valign=\"top\">\n"
//                //+ "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Name : </p>\n"
//                //+ "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Date : </p>\n"
//                + "                                                                        </td>\n"
//                + "                                                                    </tr>\n"
//                + "                                                                </table>\n"
//                + "                                                            </td>\n"
//                + "                                                        </tr>\n"
                + "                                                    </table>    \n"
                + "                                                </div>";

        fm.setOutput(report);
        //fm.setTotalEachPage(tot);
        fm.setTotalEachWithoutTax(totalwithouttax);
        fm.setTotalEachPageTax(totaltax);
        fm.setRefer(v.getRefer());
        fm.setPage(totalPage);

        return fm;

    }

}
