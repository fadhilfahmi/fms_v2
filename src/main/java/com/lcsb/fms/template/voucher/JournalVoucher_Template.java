/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.template.voucher;

import com.lcsb.fms.dao.financial.gl.JournalVoucherDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.gl.JournalVoucher;
import com.lcsb.fms.model.financial.gl.JournalVoucherItem;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.dao.EstateDAO;
import com.lcsb.fms.util.model.Estate;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class JournalVoucher_Template {
    
    public static FormattedVoucher printPV(JournalVoucher v, LoginProfile log) throws Exception {

        FormattedVoucher fv = new FormattedVoucher();
        int length = 10;
        int totPage = getCount(log, v.getJVrefno());
        int page = totPage / length;
        int modulo = totPage % length;
        int perPage = 0;
        String output = "";
        double totalEachDebit = 0.0;
        double totalEachCredit = 0.0;

        if (modulo > 0) {
            page = page + 1;
        }

        perPage = page * 10;

        Logger.getLogger(JournalVoucherDAO.class.getName()).log(Level.INFO, "-------" + page);

        for (int j = 0; j < page; j++) {

            FormattedVoucher fm = getTemplate(v, log, j + 1, j * length, length, page, totalEachDebit, totalEachCredit);

            output += fm.getOutput();
            totalEachDebit += fm.getTotalEachPage();
            totalEachCredit += fm.getTotalEachPage1();

        }

        fv.setOutput(output);
        fv.setPage(page);
        fv.setRefer(v.getJVrefno());

        return fv;
    }

    private static int getCount(LoginProfile log, String refno) throws Exception {

        int i = 0;

        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from gl_jv_item where JVrefno = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            i = rs.getInt(1);
        }

        return i;
    }

    private static FormattedVoucher getTemplate(JournalVoucher v, LoginProfile log, int page, int row, int length, int totalPage, double totalCFDebit, double totalCFCredit) throws Exception {

        FormattedVoucher fm = new FormattedVoucher();

        int i = row;
        int k = 0;
        double totDebit = 0.0;
        double totCredit = 0.0;

        Estate est = EstateDAO.getEstateInfo(log, log.getEstateCode(), "estatecode");

        List<JournalVoucherItem> slist = (List<JournalVoucherItem>) JournalVoucherDAO.getAllJVItemByPage(log, v.getJVrefno(), row, length);

        String report = "<div class=\"page\">\n"
                + "                                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\">\n"
                + "                                                        <tr>\n"
                + "                                                            <td>\n"
                + "                                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td width=\"50%\">\n"
                + "                                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td width=\"10%\"><img src=\"" + est.getLogopath() + "\" width=\"25\" height=\"25\"></td>\n"
                + "                                                                                    <td width=\"90%\">&nbsp;<span class=\"comnamevc\"><strong>" + log.getEstateDescp() + "</strong></span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                            </table>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td align=\"right\"><strong>JOURNAL VOUCHER</strong></td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td>\n"
                + "                                                                            <p class=\"medfont2\">&nbsp;</p>\n"
                + "                                                                            <p class=\"medfont3\">" + est.getAddress() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">" + est.getPostcode() + " " + est.getCity() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">" + est.getState() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">&nbsp;</p>\n"
                + "                                                                            <p class=\"medfont3\">Tel : " + est.getPhone() + ", Fax : " + est.getFax() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">&nbsp;</p>\n"
                + "                                                                            <p class=\"medfont3\">GST ID : " + est.getGstid() + "</p>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>\n"
                + "                                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"bordertopgrey\" width=\"40%\">&nbsp;<span class=\"boldtitle\">PAGE</span></td>\n"
                + "                                                                                    <td class=\"bordertoprightgrey\" width=\"60%\">&nbsp;<span class=\"normtitle\">" + page + " of " + totalPage + "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">REFERENCE NO</span></td>\n"
                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + v.getJVrefno()+ "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">DATE</span></td>\n"
                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + AccountingPeriod.fullDateMonth(v.getJVdate()) + "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">TRX NO</span></td>\n"
                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + v.getJVno()+ "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">PERIOD</span></td>\n"
                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + v.getCurperiod()+ " , " + v.getYear() + "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                            </table>\n"
                + "                                                                      </td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td valign=\"top\">\n"
                + "                                                                        </td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td valign=\"top\">\n"
                + "                                                                        </td>\n"
                + "                                                                    </tr>\n"
                + "                                                                </table>\n"
                + "                                                            </td>\n"
                + "                                                        </tr>\n"
                + "\n"
                + "                                                        <tr>\n"
                + "                                                            <td>\n"
                + "                                                                <p>&nbsp;</p>\n"
                + "                                                                <p> <span class=\"normtitle\">"+  v.getReason()+"</span></p>\n"
                + "                                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                      <tr>\n"
                + "                                                                    <td width=\"2%\" class=\"bordertop\">&nbsp;<span class=\"intableheader_jv\">No</span></td>\n"
                + "                                                                    <td width=\"15%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Account Code</span></td>\n"
                + "                                                                    <td width=\"32%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Account Description</span></td>\n"
                + "                                                                    <td width=\"9%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Debit</span></td>\n"
                + "                                                                    <td width=\"9%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Credit</span></td>\n"
                + "                                                                </tr>\n"
                + "\n"
                + "\n"
                + "\n";
               
        for (JournalVoucherItem c : slist) {
            i++;
            k++;
            totDebit += c.getDebit();
            totCredit += c.getCredit();

            report += "<tr height=\"20px\">\n"
                    + "                                                                    <td class=\"borderleft\" style=\"vertical-align:top;\" align=\"left\"><span class=\"fontjv_item\">"+ i +"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" style=\"vertical-align:top;\" align=\"left\"><span class=\"fontjv_item\">"+ c.getActcode()+"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" style=\"vertical-align:top;\" align=\"left\"><span class=\"fontjv_item\">"+ c.getActdesc()+"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" style=\"vertical-align:top;\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.currencyFormat(c.getDebit()) +"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" style=\"vertical-align:top;\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.currencyFormat(c.getCredit()) +"</span></td>\n"
                    + "                                                                </tr>";

        }
        for (int j = 0; j < 19 - k; j++) {

            report += "                                                                    <tr height=\"20px\">\n"
                    + "                                                                        <td class=\"borderleft\">&nbsp;<span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"left\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                       <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">&nbsp;</span></td>\n" 
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">&nbsp;</span></td>\n"
                    + "                                                                    </tr>\n";
        }
        report += "<tr>\n"
                + "                                                                    <td colspan=\"9\"></td>\n"
                + "                                                                </tr>\n"
                + "                                                                <tr>\n"
                + "                                                                    <td colspan=\"9\"></td>\n"
                + "                                                                </tr>\n"
                + "                                                                <tr>\n"
                + "                                                                    <td class=\"borderbottombold\">&nbsp;</td>\n"
                + "                                                                    <td align=\"right\" class=\"borderbottomboldright\">&nbsp;</td>\n"
                + "                                                                    <td align=\"right\" class=\"borderbottomboldright\" valign=\"top\">&nbsp;<span class=\"boldtitle\">TOTAL</span>&nbsp;</td>\n"
                + "                                                                    </td>\n"
                + "                                                                    <td align=\"right\" class=\"borderbottomboldright\"><span class=\"fontjv_item\">" + GeneralTerm.currencyFormat(totDebit + totalCFDebit) + "<strong>\n"
                + "                                                                            </strong></span>&nbsp;</td>\n"
                + "                                                                    <td align=\"right\" class=\"borderbottomboldright\"><span class=\"fontjv_item\">" + GeneralTerm.currencyFormat(totCredit + totalCFCredit) + "<strong></strong></span>&nbsp;</td>\n" + "                                                                </tr>"
                + "                                                                </table>\n"
                + "                                                                <p>&nbsp;</p>\n"
                + "                                                            </td>\n"
                + "                                                        </tr>\n"
                + "                                                        <tr>\n"
                + "                                                            <td><br><br<br><br<br><br<br><br>\n"
                + "                                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td width=\"33%\" >\n"
                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;_________________________</p>\n"
                + "                                                                            <p class=\"paytofontmiddle\">&nbsp;&nbsp;<strong>Prepared By</strong></p>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td width=\"33%\">\n"
                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;_________________________</p>\n"
                + "                                                                            <p class=\"paytofontmiddle\">&nbsp;&nbsp;<strong>Checked By</strong></p>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td width=\"33%\">\n"
                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;_________________________</p>\n"
                + "                                                                            <p class=\"paytofontmiddle\">&nbsp;&nbsp;<strong>Approved By</strong></p>\n"
                + "                                                                        </td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td valign=\"top\">\n"
                //+ "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Name : <strong>" + v.getPrename() + "</strong></p>\n"
                //+ "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Date : <strong>" + AccountingPeriod.fullDateMonth(v.getPreparedate()) + "</strong></p>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td  valign=\"top\">\n"
                + "                                                                            <p class=\"paytofontmiddle\">&nbsp;&nbsp;</p>\n"
                //+ "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Name : <strong>" + v.getCheckname()+ "</strong></p>\n"
                //+ "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Date : <strong>" + AccountingPeriod.fullDateMonth(v.getApprovedate()) + "</strong></p>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td  valign=\"top\">\n"
                //+ "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Name : </p>\n"
                //+ "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Date : </p>\n"
                + "                                                                        </td>\n"
                + "                                                                    </tr>\n"
                + "                                                                </table>\n"
                + "                                                            </td>\n"
                + "                                                        </tr>\n"
                + "                                                    </table>    \n"
                + "                                                </div>";

        fm.setOutput(report);
        fm.setTotalEachPage(totDebit);
        fm.setTotalEachPage1(totCredit);
        fm.setRefer(v.getJVrefno());
        fm.setPage(totalPage);

        return fm;

    }
    
}
