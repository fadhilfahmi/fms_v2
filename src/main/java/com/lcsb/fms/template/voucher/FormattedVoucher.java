/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.template.voucher;

/**
 *
 * @author fadhilfahmi
 */
public class FormattedVoucher {

    private String output;
    private double totalEachPage;
    private double totalEachPage1;
    private double totalEachWithoutTax;
    private double totalEachPageTax;
    private double totalEachDebit;
    private double totalEachCredit;
    private double totalEachBalance;
    private String refer;
    private int page;
    private String moduleid;

    public double getTotalEachDebit() {
        return totalEachDebit;
    }

    public void setTotalEachDebit(double totalEachDebit) {
        this.totalEachDebit = totalEachDebit;
    }

    public double getTotalEachCredit() {
        return totalEachCredit;
    }

    public void setTotalEachCredit(double totalEachCredit) {
        this.totalEachCredit = totalEachCredit;
    }

    public double getTotalEachBalance() {
        return totalEachBalance;
    }

    public void setTotalEachBalance(double totalEachBalance) {
        this.totalEachBalance = totalEachBalance;
    }

    public double getTotalEachWithoutTax() {
        return totalEachWithoutTax;
    }

    public void setTotalEachWithoutTax(double totalEachWithoutTax) {
        this.totalEachWithoutTax = totalEachWithoutTax;
    }

    public double getTotalEachPageTax() {
        return totalEachPageTax;
    }

    public void setTotalEachPageTax(double totalEachPageTax) {
        this.totalEachPageTax = totalEachPageTax;
    }

    public double getTotalEachPage1() {
        return totalEachPage1;
    }

    public void setTotalEachPage1(double totalEachPage1) {
        this.totalEachPage1 = totalEachPage1;
    }

    public String getModuleid() {
        return moduleid;
    }

    public void setModuleid(String moduleid) {
        this.moduleid = moduleid;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public double getTotalEachPage() {
        return totalEachPage;
    }

    public void setTotalEachPage(double totalEachPage) {
        this.totalEachPage = totalEachPage;
    }

}
