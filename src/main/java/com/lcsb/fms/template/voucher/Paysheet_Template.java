/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.template.voucher;

import com.lcsb.fms.dao.financial.cashbook.PaymentVoucherDAO;
import com.lcsb.fms.dao.financial.employee.EmPayrollDAO;
import com.lcsb.fms.dao.financial.tx.TaxOutputDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucher;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem;
import com.lcsb.fms.model.financial.employee.EmPaySheetList;
import com.lcsb.fms.model.financial.employee.EmPayrollType;
import com.lcsb.fms.model.setup.company.staff.Staff;
import com.lcsb.fms.util.dao.BankDAO;
import com.lcsb.fms.util.dao.EstateDAO;
import com.lcsb.fms.util.model.Estate;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class Paysheet_Template {
    
    public static FormattedVoucher printT(String refer, LoginProfile log) throws Exception {

        FormattedVoucher fv = new FormattedVoucher();
        int length = 4;
        int totPage = getCount(log);
        int page = totPage / length;
        int modulo = totPage % length;
        int perPage = 0;
        String output = "";
        double totalEachWithoutTax = 0.0;
        double totalEachTax = 0.0;

        if (modulo > 0) {
            page = page + 1;
        }

        perPage = page * 4;

        Logger.getLogger(Paysheet_Template.class.getName()).log(Level.INFO, "-------" + page);

        for (int j = 0; j < page; j++) {

            FormattedVoucher fm = getTemplate(refer, log, j + 1, j * length, length, page, totalEachWithoutTax, totalEachTax);

            output += fm.getOutput();
            totalEachWithoutTax += fm.getTotalEachWithoutTax();
            totalEachTax += fm.getTotalEachPageTax();

        }

        fv.setOutput(output);
        fv.setPage(page);
        fv.setRefer(refer);

        return fv;
    }
    
    private static int getCount(LoginProfile log) throws Exception {

        int i = 0;

        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as tot from co_staff a, co_staff_designation b where a.staffid = b.staffid and b.worklocation = ?");
        stmt.setString(1, "Headquarters");
        rs = stmt.executeQuery();
        if (rs.next()) {
            i = rs.getInt(1);
        }

        return i;
    }
    
    public static FormattedVoucher getTemplate(String refer, LoginProfile log, int page, int row, int length, int totalPage, double totalCFwithouttax, double totalCFtax) throws Exception {

        FormattedVoucher fm = new FormattedVoucher();

        int i = row;
        int k = 0;
        double tot = 0.0;
        
        double totalwithouttax = 0.0;
        double totaltax = 0.0;

        Estate est = EstateDAO.getEstateInfo(log, log.getEstateCode(), "estatecode");
        
        EmPaySheetList x = EmPayrollDAO.getPaysheet(log, refer, row, length);

        //List<PaymentVoucherItem> slist = (List<PaymentVoucherItem>) PaymentVoucherDAO.getAllPYVItemByPages(log, v.getRefer(), row, length);

        String report = "<div class=\"page-landscape\">\n" +
"                                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
"                                                        <tr>\n" +
"                                                            <td colspan=\"3\">\n" +
"                                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
"                                                                    <tr>\n" +
"                                                                        <td width=\"50px\"><img src=\""+est.getLogopath()+"\" width=\"40px\" style=\"vertical-align:middle\" ></td>\n" +
"                                                                        <td><strong>"+ log.getEstateDescp()+"</strong><span class=\"pull-right\"></span>\n" +
"                                                                            <p class=\"font_inheader_normal\">"+est.getAddress()+"</p>\n" +
"                                                                            <p class=\"font_inheader_normal\">"+est.getPostcode()+", "+ est.getCity()+", "+ est.getState()+"</p>\n" +
"                                                                        </td>\n" +
"                                                                    </tr>\n" +
"                                                                </table>\n" +
"                                                            </td>\n" +
"                                                            <td >\n" +//NOTED
"                                                               <span class=\"pull-right\" style=\"font-size:12px;\">PAY SHEET <strong>"+ AccountingPeriod.getMonthofPeriod(x.getMaster().getPeriod()).toUpperCase() +" "+x.getMaster().getYear()+"</strong></span>\n" +//NOTED
"                                                            </td>\n" +//NOTED
"                                                        </tr>\n" +
"                                                        <tr>\n" +
"                                                            <td colspan=\"4\" class=\"border_bottom\"></td>\n" +
"                                                        </tr>\n" +
"                                                    </table>\n" +
"                                                    <br>\n" +
"                                                    <table  class=\"table-striped\" width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">\n" +
"                                                        <tr> \n" +
"                                                            <th class=\"smalltd\" colspan=\"5\">EARNING</th>\n" +
"\n" +
"                                                        </tr>\n";
                                                         for (EmPayrollType t : x.getListEarn()) {
report +=                                                         " <tr> \n" +
"                                                            <td class=\"smalltd\" width=\"12%\">"+t.getEarndesc()+"</td>\n" ;
                                                            for (Staff j : x.getStaffAll()) {
report +=                                                             "<td class=\"smalltd\" width=\"22%\"><span class=\"pull-right\">"+ GeneralTerm.currencyFormat(EmPayrollDAO.getEarningEachPersonInfo(log, refer, j.getStaffid(), t.getEarncode()).getAmount())+"</span></td>\n";
                                                            }
report +=                                                         "</tr>\n";
                                                         }
report +=                                                         "<tr> \n" +
"                                                            <td class=\"smalltd\" width=\"12%\">GROSS PAY</td>\n";
                                                             for (Staff j : x.getStaffAll()) {
report +=                                                            "<td class=\"smalltd\" width=\"22%\"><span class=\"pull-right\">"+GeneralTerm.currencyFormat(EmPayrollDAO.getAmountSum(log, refer, j.getStaffid(), "earning", "thismonth"))+"</span></td>\n";
                                                                } 
report +=                                                        "</tr>\n" +
"                                                        <tr> \n" +
"                                                            <td class=\"smalltd\" width=\"12%\">GROSS PAY TODATE</td>\n";
                                                            for (Staff j : x.getStaffAll()) {
report +=                                                             "<td class=\"smalltd\" width=\"22%\"><span class=\"pull-right\">"+ GeneralTerm.currencyFormat(EmPayrollDAO.getAmountSum(log, refer, j.getStaffid(), "earning", "todate"))+"</span></td>\n";
                                                                }
report +=                                                         "</tr>\n" +
"                                                        <tr> \n" +
"                                                            <th class=\"smalltd\" colspan=\"5\">DEDUCTION</th>\n" +
"                                                        </tr>\n";
                                                            for (EmPayrollType t : x.getListDeduct()) {
report +=                                                         "<tr> \n" +
"                                                            <td class=\"smalltd\" width=\"12%\">"+ t.getEarndesc()+"</td>\n";
                                                               for (Staff j : x.getStaffAll()) {
                                                                    double earn = EmPayrollDAO.getDeductionEachPersonInfo(log, refer, j.getStaffid(), t.getEarncode()).getAmount();
report += "                                                            <td class=\"smalltd\" width=\"22%\"><span class=\"pull-right\">"+ GeneralTerm.currencyFormat(earn)+"</span></td>\n";
                                                                }
report += "                                                        </tr>\n";
                                                        }
report += "                                                        <tr> \n" +
"                                                            <td class=\"smalltd\" width=\"12%\">TOTAL DEDUCTION</td>\n";
                                                             for (Staff j : x.getStaffAll()) {
report += "                                                            <td class=\"smalltd\" width=\"22%\"><span class=\"pull-right\">"+ GeneralTerm.currencyFormat(EmPayrollDAO.getAmountSum(log, refer, j.getStaffid(), "deduction", "thismonth")) +"</span></td>\n";
                                                                }
report += "                                                        </tr>\n" +
"                                                        <tr> \n" +
"                                                            <td class=\"smalltd\" width=\"12%\">NET PAY</td>\n";
                                                            for (Staff j : x.getStaffAll()) {
report += "                                                            <td class=\"smalltd\" width=\"22%\"><span class=\"pull-right\"><strong>"+ GeneralTerm.currencyFormat(EmPayrollDAO.getAmountSum(log, refer, j.getStaffid(), "earning", "thismonth") - EmPayrollDAO.getAmountSum(log, refer, j.getStaffid(), "deduction", "thismonth"))+"</strong></span></td>\n";
                                                                        }
report += "                                                        </tr>\n"+
"                                                        <tr> \n" +
"                                                            <th class=\"smalltd\" colspan=\"5\">EMPLOYER</th>\n" +
"\n" +
"                                                        </tr>\n";
                                                          for (EmPayrollType t : x.getListDeduct()) {
report += "                                                        <tr> \n" +
"                                                            <td class=\"smalltd\" width=\"12%\">"+ t.getEarndesc()+"</td>\n";
                                                                for (Staff j : x.getStaffAll()) {

                                                                    double earn = EmPayrollDAO.getDeductionEachPersonInfo(log, refer, j.getStaffid(), t.getEarncode()).getAmount();
report += "                                                            <td class=\"smalltd\" width=\"22%\"><span class=\"pull-right\">"+ GeneralTerm.currencyFormat(earn)+"</span></td>\n";
                                                                 } 
report += "                                                        </tr>\n";
                                                       } 
report += "                                                        <tr> \n" +
"                                                            <th class=\"smalltd\" width=\"12%\">STAFF ID</th>\n";
                                                                 for (Staff j : x.getStaffAll()) {
report += "                                                            <td class=\"smalltd\" width=\"22%\"><span class=\"pull-right\"><span class=\"pull-right\">"+ j.getStaffid()+"</span></span></td>\n";
                                                                    }
report += "                                                        </tr>\n" +
"                                                        <tr> \n" +
"                                                            <th class=\"smalltd\" width=\"12%\">STAFF NAME</th>\n";
                                                                for (Staff j : x.getStaffAll()) {
report += "                                                            <td class=\"smalltd\" width=\"22%\"><span class=\"pull-right\"><span class=\"pull-right\">"+ j.getName()+"</span></span></td>\n";
                                                                    }
report += "                                                        </tr>\n" +
"                                                        <tr> \n" +
"                                                            <th class=\"smalltd\" width=\"12%\">DESIGNATION</th>\n";
                                                                 for (Staff j : x.getStaffAll()) {
report += "                                                            <td class=\"smalltd\" width=\"22%\"><span class=\"pull-right\">"+ j.getPposition()+"</span></td>\n";
                                                                }
report += "                                                        </tr>\n" +   
"                                                        <tr> \n" +
"                                                            <th class=\"smalltd\" width=\"12%\">IC NO.</th>\n";
                                                                for (Staff j : x.getStaffAll()) {
report += "                                                            <td class=\"smalltd\" width=\"22%\"><span class=\"pull-right\">"+ j.getIc()+"</span></td>\n";
                                                                }
report += "                                                        </tr>\n" +
"                                                    </table> \n" +
"                                                </div>";

        fm.setOutput(report);
        fm.setTotalEachPage(tot);
        fm.setRefer(refer);
        fm.setPage(totalPage);

        return fm;

    }
    
}
