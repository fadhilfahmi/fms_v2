/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.template.voucher;

import com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO;
import com.lcsb.fms.dao.financial.tx.TaxOutputDAO;
import com.lcsb.fms.dao.setup.configuration.BuyerDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.ar.SalesInvoice;
import com.lcsb.fms.model.financial.ar.SalesInvoiceItem;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.dao.EstateDAO;
import com.lcsb.fms.util.model.Estate;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class SalesInvoice_Template {
    
    public static FormattedVoucher printPV(SalesInvoice v, LoginProfile log) throws Exception {

        FormattedVoucher fv = new FormattedVoucher();
        int length = 8;
        int totPage = getCount(log, v.getInvref());
        int page = totPage / length;
        int modulo = totPage % length;
        int perPage = 0;
        String output = "";
        double totalEachWithoutTax = 0.0;
        double totalEachTax = 0.0;

        if (modulo > 0) {
            page = page + 1;
        }

        perPage = page * 10;

        Logger.getLogger(TaxOutputDAO.class.getName()).log(Level.INFO, "-------" + page);

        for (int j = 0; j < page; j++) {

            FormattedVoucher fm = getTemplate(v, log, j + 1, j * length, length, page, totalEachWithoutTax, totalEachTax);

            output += fm.getOutput();
            totalEachWithoutTax += fm.getTotalEachWithoutTax();
            totalEachTax += fm.getTotalEachPageTax();

        }

        fv.setOutput(output);
        fv.setPage(page);
        fv.setRefer(v.getInvref());

        return fv;
    }

    private static int getCount(LoginProfile log, String refno) throws Exception {

        int i = 0;

        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from sl_inv_item where ivref = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            i = rs.getInt(1);
        }

        return i;
    }
    
    private static String textClaimer(){
        
        String text = "All cheque must be crossed A/C Payee Only 'NOT NEGOTIABLE' and made payable to : DOMINION SQUARE SDN BHD A/C NO : MBB 556011079541. The company deserves the right to charge interest on overdue accounts. E&O.E";
        
        return text;
    }

    private static FormattedVoucher getTemplate(SalesInvoice v, LoginProfile log, int page, int row, int length, int totalPage, double totalCFwithouttax, double totalCFtax) throws Exception {

        FormattedVoucher fm = new FormattedVoucher();

        int i = row;
        int k = 0;
        double totalwithouttax = 0.0;
        double totaltax = 0.0;
        double totalqty = 0.0;

        Estate est = EstateDAO.getEstateInfo(log, log.getEstateCode(), "estatecode");

        List<SalesInvoiceItem> slist = (List<SalesInvoiceItem>) SalesInvoiceDAO.getAllINVItemByPage(log, v.getInvref(), row, length);

        String report = "<div class=\"page\">\n"
                + "                                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\">\n"
                + "                                                        <tr>\n"
                + "                                                            <td>\n"
                + "                                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td width=\"50%\">\n"
                + "                                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td width=\"10%\"><img src=\"" + est.getLogopath() + "\" width=\"25\" height=\"25\"></td>\n"
                + "                                                                                    <td width=\"90%\">&nbsp;<span class=\"comnamevc\"><strong>" + log.getEstateDescp() + " </strong></span><font style=\"font-size:10px\">("+ est.getRegisterno() +")</font></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                            </table>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td align=\"right\"><strong>TAX INVOICE</strong></td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td>\n"
                + "                                                                            <p class=\"medfont2\">&nbsp;</p>\n"
                + "                                                                            <p class=\"medfont3\">" + est.getAddress() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">" + est.getPostcode() + " " + est.getCity() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">" + est.getState() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">&nbsp;</p>\n"
                + "                                                                            <p class=\"medfont3\">Tel : " + est.getPhone() + ", Fax : " + est.getFax() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">&nbsp;</p>\n"
                + "                                                                            <p class=\"medfont3\">GST ID : " + est.getGstid() + "</p>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td valign=\"top\">\n"
                + "                                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td width=\"20%\">\n"
                + "                                                                                        <span class=\"boldtitle\">To</span>\n"
                + "                                                                                    </td>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"normtitle\">" + v.getBname()+ "</span>  \n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"boldtitle\"></span>\n"
                + "                                                                                    </td>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"normtitle\">" + v.getBaddress()+ "</span>  \n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"boldtitle\"></span>\n"
                + "                                                                                    </td>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"normtitle\">" + v.getBcity()+ ", " + v.getBpostcode()+ "</span>  \n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"boldtitle\"></span>\n"
                + "                                                                                    </td>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"normtitle\">" + v.getBstate()+ "</span>  \n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
//                + "                                                                                <tr>\n"
//                + "                                                                                    <td>\n"
//                + "                                                                                        <span class=\"boldtitle\"></span>\n"
//                + "                                                                                    </td>\n"
//                + "                                                                                    <td>\n"
//                + "                                                                                        <span class=\"normtitle\">" + v.get + ", " + v.getPaidstate() + "</span>  \n"
//                + "                                                                                    </td>\n"
//                + "                                                                                </tr>\n"
                + "                                                                            </table>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td valign=\"top\">\n"
                + "                                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"bordertopgrey\" width=\"40%\">&nbsp;<span class=\"boldtitle\">PAGE</span></td>\n"
                + "                                                                                    <td class=\"bordertoprightgrey\" width=\"60%\">&nbsp;<span class=\"normtitle\">" + page + " of " + totalPage + "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">INVOICE NO</span></td>\n"
                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + v.getInvref()+ " - "+ v.getProdname() +"</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">INVOICE DATE</span></td>\n"
                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + AccountingPeriod.fullDateMonth(v.getInvdate()) + "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">DELIVERY NO</span></td>\n"
                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + ((v.getDono()== null) ? "N/A" : v.getDono()) + "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">ORDER NUMBER</span></td>\n"
                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + v.getOrderno()+ "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">TERMS</span></td>\n"
                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + BuyerDAO.getInfo(log, v.getBcode()).getPaymentterm() + "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                            </table>\n"
                + "                                                                        </td>\n"
                + "                                                                    </tr>\n"
                + "                                                                </table>\n"
                + "                                                            </td>\n"
                + "                                                        </tr>\n"
                + "\n"
                + "                                                        <tr>\n"
                + "                                                            <td>\n"
                + "                                                                <p>&nbsp;</p>\n"
                + "                                                                <p> <span class=\"normtitle\">"+ ((v.getProdcode().equals("03")) ? "" : v.getRemarks()) +"</span></p>\n"
                + "                                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                      <tr>\n"
                + "                                                                    <td width=\"2%\" class=\"bordertop\">&nbsp;<span class=\"intableheader_jv\">No</span></td>\n"
                + "                                                                    <td width=\"37%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Description</span></td>\n"
                + "                                                                    <td width=\"10%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Quantity</span></td>\n"
                + "                                                                    <td width=\"10%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Unit Price</span></td>\n"
                + "                                                                    <td width=\"10%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Discount</span></td>\n"
                + "                                                                    <td width=\"10%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Tax</span></td>\n"
                + "                                                                    <td width=\"10%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Total</span></td>\n"
                + "                                                                </tr>\n"
                + "\n"
                + "\n"
                + "\n";
        
        int spaceForSummary = 0;
        for (SalesInvoiceItem c : slist) {
            
            i++;
            k++;
            totalwithouttax += c.getAmount();
            totaltax += c.getTaxamt();
            totalqty += c.getQty();
            
            if(BuyerDAO.getInfo(log, v.getBcode()).getTaxinvoicetype().equals("Detail")){
            
                report += "<tr height=\"40px\">\n"
                    + "                                                                    <td class=\"borderleft\">&nbsp;<span class=\"fontjv_item\">"+ i +"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\">&nbsp;<span class=\"fontjv_item\">"+ c.getRemarks() +"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.currencyFormat(c.getQty())+"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.currencyFormat(Double.parseDouble(c.getUnitp()))+"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">0.00</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.currencyFormat(c.getTaxamt()) +"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.currencyFormat(c.getAmount()) +"</span></td>\n"
                    + "                                                                </tr>";
                
                
            }else if(k == slist.size() && BuyerDAO.getInfo(log, v.getBcode()).getTaxinvoicetype().equals("Summary")){
                
                spaceForSummary = 9;
                
                report += "<tr height=\"40px\">\n"
                    + "                                                                    <td class=\"borderleft\">&nbsp;<span class=\"fontjv_item\">1</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\"><span class=\"fontjv_item\">"+ v.getRemarks() +"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.currencyFormat(totalqty) +"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.currencyFormat(Double.parseDouble(c.getUnitp()))+"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">0.00</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.currencyFormat(totaltax) +"</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">"+ GeneralTerm.currencyFormat(totalwithouttax) +"</span></td>\n"
                    + "                                                                </tr>";
                
            }

        }
        
        int cntSpace = 8;
        if(page!=totalPage){
            cntSpace = 8;
        }
        for (int j = 0; j < cntSpace - k + spaceForSummary; j++) {

            report += "                                                                    <tr height=\"30px\">\n"
                    + "                                                                        <td class=\"borderleft\">&nbsp;<span class=\"fontjv_item\">&nbsp;</span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"left\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">&nbsp;</span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">&nbsp;</span></td>\n"
                    + "                                                                    </tr>\n";
        }
        report += "<tr>\n"
                + "                                                                    <td colspan=\"9\"></td>\n"
                + "                                                                </tr>\n"
                + "                                                                <tr>\n"
                + "                                                                    <td colspan=\"9\"></td>\n"
                + "                                                                </tr>\n"
                + "                                                                <tr>\n"
                + "                                                                    <td class=\"borderleft\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\"  valign=\"top\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\" align=\"right\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\" align=\"right\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\" align=\"right\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\" align=\"right\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\" align=\"right\">&nbsp;</td>\n"
                + "                                                                </tr>\n"
                + "                                                                <tr>\n"
                + "                                                                    <td class=\"borderleft\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\">&nbsp;</td>\n"
                + "                                                                </tr>\n"
                + "                                                                <tr>\n"
                + "                                                                    <td class=\"borderbottombold\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderbottomboldright\" valign=\"top\">&nbsp;</td>\n"
                + "                                                                    <td align=\"right\" class=\"borderbottomboldright\" colspan=\"4\">\n"
                + "                                                                        <span class=\"totalamount_jv\" style=\"font-size:11px;\">&nbsp;&nbsp;<strong>Total (Exclusive of GST)</strong></span>&nbsp;<br>\n"
                + "                                                                        <span class=\"totalamount_jv\" style=\"font-size:11px;\">&nbsp;&nbsp;<strong>Total GST 6%</strong></span>&nbsp;<br>\n"
                + "                                                                        <span class=\"totalamount_jv\" style=\"font-size:11px;\">&nbsp;&nbsp;<strong>Grand Total</strong></span>&nbsp;\n"
                + "                                                                    </td>\n"
                + "                                                                    <td align=\"right\" class=\"borderbottomboldright\">"
                +"                                                                          <span class=\"totalamount_jv\" style=\"font-size:11px;\">" + GeneralTerm.currencyFormat(totalwithouttax + totalCFwithouttax) + "<strong></strong></span>&nbsp;"
                +"                                                                          <span class=\"totalamount_jv\" style=\"font-size:11px;\">" + GeneralTerm.currencyFormat(totaltax + totalCFtax) + "<strong></strong></span>&nbsp;"
                +"                                                                          <span class=\"totalamount_jv\" style=\"font-size:11px;\">" + GeneralTerm.currencyFormat(totalwithouttax + totaltax + totalCFtax + totalCFwithouttax) + "<strong></strong></span>&nbsp;"
                +"                                                                     </td>\n" + "                                                                </tr>"
                
                + "                                                                </table>\n"
//                + "                                                                <table border=\"0\" width=\"100%\">\n"
//                + "                                                                    <tr>\n"
//                + "                                                                        <td width=\"25%\"><span class=\"intableheader_jv\">Ringgit Malaysia(in words)</span></td>\n"
//                + "                                                                        <td>:</td>\n"
//                + "                                                                        <td><span class=\"fontjv_item\">&nbsp;&nbsp;" + v.getRm() + "</span></td>\n"
//                + "                                                                    </tr>\n"
//                + "                                                                    <tr>\n"
//                + "                                                                        <td><span class=\"intableheader_jv\">Bank / Branch</span></td>\n"
//                + "                                                                        <td>:</td>\n"
//                + "                                                                        <td><span class=\"fontjv_item\">&nbsp;&nbsp;" + v.getBankcode() + " - " + v.getBankname() + "</span></td>\n"
//                + "                                                                    </tr>\n"
//                + "                                                                    <tr>\n"
//                + "                                                                        <td><span class=\"intableheader_jv\">Payment Mode</span></td>\n"
//                + "                                                                        <td>:</td><td><span class=\"fontjv_item\">&nbsp;&nbsp;</span></td>\n"
//                + "                                                                    </tr>  \n"
//                + "                                                                    <tr>\n"
//                + "                                                                        <td><span class=\"intableheader_jv\">Cheque No. </span></td>\n"
//                + "                                                                        <td>:</td>\n"
//                + "                                                                        <td><span class=\"fontjv_item\">&nbsp;&nbsp;</span></td>\n"
//                + "                                                                    </tr>\n"
//                + "                                                                </table>\n"
               + "                                                                <p>&nbsp;</p>\n"
                + "                                                            </td>\n"
                + "                                                        </tr>\n"
                + "                                                        <tr>\n"
                + "                                                            <td>&nbsp;<span class=\"boldtitle\">Year</span><span class=\"normtitle\"> - " + v.getYear() + "</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"boldtitle\">Period</span><span class=\"normtitle\"> - " + v.getPeriod() + "</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"boldtitle\">No</span><span class=\"normtitle\"> - " + v.getInvref()+ "</span></td>\n"
                + "                                                        </tr>\n";
                if(page==totalPage){
                report += "                                                        <tr>\n"
                + "                                                            <td>\n"
                + "                                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td width=\"50%\" class=\"bordertopgreyauth\">\n"
                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;_________________________</p>\n"
                + "                                                                            <p class=\"paytofontmiddle\">&nbsp;&nbsp;<strong>Approved By</strong></p>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td style=\"padding=10px\" width=\"50%\" class=\"bordertopgreyauthright\"> <div style=\"padding-left: 15px;padding-right: 15px;\"> \n"
                + "                                                                            <p class=\"paytofonttop\"></p>\n"
                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
                + "                                                                            <p class=\"paytofontmiddle\">"+textClaimer()+"</p>\n"
                + "                                                                        </div></td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td class=\"borderleftbottomgrey\" valign=\"top\">\n"
                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Name : </p>\n"
                + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Date : </p>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td class=\"borderleftbottomgreyright\" valign=\"top\">\n"
                + "                                                                        </td>\n"
                + "                                                                    </tr>\n"
                + "                                                                </table>\n"
                + "                                                            </td>\n"
                + "                                                        </tr>\n";
                        }
                report += "                                                    </table>    \n"
                + "                                                </div>";

        fm.setOutput(report);
        fm.setTotalEachWithoutTax(totalwithouttax);
        fm.setTotalEachPageTax(totaltax);
        fm.setRefer(v.getInvref());
        fm.setPage(totalPage);

        return fm;

    }
    
}
