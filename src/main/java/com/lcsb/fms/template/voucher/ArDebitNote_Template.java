/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.template.voucher;

import com.lcsb.fms.dao.financial.ar.ArCreditNoteDAO;
import com.lcsb.fms.dao.financial.ar.ArDebitNoteDAO;
import com.lcsb.fms.dao.financial.tx.TaxOutputDAO;
import com.lcsb.fms.dao.setup.configuration.BuyerDAO;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.ar.ArCreditNote;
import com.lcsb.fms.model.financial.ar.ArCreditNoteItem;
import com.lcsb.fms.model.financial.ar.ArDebitNote;
import com.lcsb.fms.model.financial.ar.ArDebitNoteItem;
import com.lcsb.fms.util.dao.EstateDAO;
import com.lcsb.fms.util.model.Estate;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class ArDebitNote_Template {
    
    public static FormattedVoucher printPV(ArDebitNote v, LoginProfile log) throws Exception {

        FormattedVoucher fv = new FormattedVoucher();
        int length = 10;
        int totPage = getCount(log, v.getNoteno());
        int page = totPage / length;
        int modulo = totPage % length;
        int perPage = 0;
        String output = "";
        double totalEachWithoutTax = 0.0;
        double totalEachTax = 0.0;

        if (modulo > 0) {
            page = page + 1;
        }

        perPage = page * 10;

        Logger.getLogger(TaxOutputDAO.class.getName()).log(Level.INFO, "-------" + page);

        for (int j = 0; j < page; j++) {

            FormattedVoucher fm = getTemplate(v, log, j + 1, j * length, length, page, totalEachWithoutTax, totalEachTax);

            output += fm.getOutput();
            totalEachWithoutTax += fm.getTotalEachWithoutTax();
            totalEachTax += fm.getTotalEachPageTax();

        }

        fv.setOutput(output);
        fv.setPage(page);
        fv.setRefer(v.getNoteno());

        return fv;
    }

    private static int getCount(LoginProfile log, String refno) throws Exception {

        int i = 0;

        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from ar_debitnote_item where noteno = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            i = rs.getInt(1);
        }

        return i;
    }

    private static FormattedVoucher getTemplate(ArDebitNote v, LoginProfile log, int page, int row, int length, int totalPage, double totalCFwithouttax, double totalCFtax) throws Exception {

        Connection con = log.getCon();
        FormattedVoucher fm = new FormattedVoucher();

        int i = row;
        int k = 0;
        double totalwithouttax = 0.0;
        double totaltax = 0.0;
        double totalqty = 0.0;

        Estate est = EstateDAO.getEstateInfo(log, log.getEstateCode(), "estatecode");

        List<ArDebitNoteItem> slist = (List<ArDebitNoteItem>) ArDebitNoteDAO.getAllItemByPage(log, v.getNoteno(), row, length);

        String report = "<div class=\"page\">\n"
                + "                                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\">\n"
                + "                                                        <tr>\n"
                + "                                                            <td>\n"
                + "                                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td width=\"50%\">\n"
                + "                                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td width=\"10%\"><img src=\"" + est.getLogopath() + "\" width=\"25\" height=\"25\"></td>\n"
                + "                                                                                    <td width=\"90%\">&nbsp;<span class=\"comnamevc\"><strong>" + log.getEstateDescp() + "</strong></span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                            </table>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td align=\"right\"><strong>DEBIT NOTE</strong></td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td>\n"
                + "                                                                            <p class=\"medfont2\">&nbsp;</p>\n"
                + "                                                                            <p class=\"medfont3\">" + est.getAddress() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">" + est.getPostcode() + " " + est.getCity() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">" + est.getState() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">&nbsp;</p>\n"
                + "                                                                            <p class=\"medfont3\">Tel : " + est.getPhone() + ", Fax : " + est.getFax() + "</p>\n"
                + "                                                                            <p class=\"medfont3\">&nbsp;</p>\n"
                + "                                                                            <p class=\"medfont3\">GST ID : " + est.getGstid() + "</p>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                    </tr>\n"
                + "                                                                    <tr>\n"
                + "                                                                        <td valign=\"top\">\n"
                + "                                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td width=\"20%\">\n"
                + "                                                                                        <span class=\"boldtitle\">To</span>\n"
                + "                                                                                    </td>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"normtitle\">" + v.getBuyername()+ "</span>  \n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"boldtitle\"></span>\n"
                + "                                                                                    </td>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"normtitle\">" + BuyerDAO.getInfo(log, v.getBuyercode()).getAddress() + "</span>  \n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"boldtitle\"></span>\n"
                + "                                                                                    </td>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"normtitle\">" + BuyerDAO.getInfo(log, v.getBuyercode()).getPostcode()+ ", " + BuyerDAO.getInfo(log, v.getBuyercode()).getCity()+ "</span>  \n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"boldtitle\"></span>\n"
                + "                                                                                    </td>\n"
                + "                                                                                    <td>\n"
                + "                                                                                        <span class=\"normtitle\">" + BuyerDAO.getInfo(log, v.getBuyercode()).getState()+ "</span>  \n"
                + "                                                                                    </td>\n"
                + "                                                                                </tr>\n"
                //                + "                                                                                <tr>\n"
                //                + "                                                                                    <td>\n"
                //                + "                                                                                        <span class=\"boldtitle\"></span>\n"
                //                + "                                                                                    </td>\n"
                //                + "                                                                                    <td>\n"
                //                + "                                                                                        <span class=\"normtitle\">" + v.get + ", " + v.getPaidstate() + "</span>  \n"
                //                + "                                                                                    </td>\n"
                //                + "                                                                                </tr>\n"
                + "                                                                            </table>\n"
                + "                                                                        </td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td>&nbsp;</td>\n"
                + "                                                                        <td valign=\"top\">\n"
                + "                                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"bordertopgrey\" width=\"40%\">&nbsp;<span class=\"boldtitle\">PAGE</span></td>\n"
                + "                                                                                    <td class=\"bordertoprightgrey\" width=\"60%\">&nbsp;<span class=\"normtitle\">" + page + " of " + totalPage + "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">REFERENCE NO</span></td>\n"
                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + v.getNoteno() + "</span></td>\n"
                + "                                                                                </tr>\n"
                + "                                                                                <tr>\n"
                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">DATE</span></td>\n"
                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + v.getNotedate() + "</span></td>\n"
                + "                                                                                </tr>\n"
                //                + "                                                                                <tr>\n"
                //                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">DELIVERY DATE NO</span></td>\n"
                //                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + /*v.getInvdate()*/" "+ "</span></td>\n"
                //                + "                                                                                </tr>\n"
                //                + "                                                                                <tr>\n"
                //                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">ORDER NUMBER</span></td>\n"
                //                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + v.getContractno()+ "</span></td>\n"
                //                + "                                                                                </tr>\n"
                //                + "                                                                                <tr>\n"
                //                + "                                                                                    <td align=\"left\" class=\"borderleftbottomgrey\">&nbsp;<span class=\"boldtitle\">TERMS</span></td>\n"
                //                + "                                                                                    <td class=\"borderleftbottomrightgrey\">&nbsp;<span class=\"normtitle\">" + BuyerDAO.getInfo(v.getBcode()).getPaymentterm() + " DAYS</span></td>\n"
                //                + "                                                                                </tr>\n"
                + "                                                                            </table>\n"
                + "                                                                        </td>\n"
                + "                                                                    </tr>\n"
                + "                                                                </table>\n"
                + "                                                            </td>\n"
                + "                                                        </tr>\n"
                + "\n"
                + "                                                        <tr>\n"
                + "                                                            <td>\n"
                + "                                                                <p>&nbsp;</p>\n"
                + "                                                                <p> <span class=\"normtitle\"></span></p>\n"
                + "                                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "                                                                      <tr>\n"
                + "                                                                    <td width=\"2%\" class=\"bordertop\">&nbsp;<span class=\"intableheader_jv\">No</span></td>\n"
                + "                                                                    <td width=\"37%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Description</span></td>\n"
                + "                                                                    <td width=\"10%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Quantity</span></td>\n"
                + "                                                                    <td width=\"10%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Unit Price</span></td>\n"
                + "                                                                    <td width=\"10%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Discount</span></td>\n"
                + "                                                                    <td width=\"10%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Tax</span></td>\n"
                + "                                                                    <td width=\"10%\" class=\"bordertoprightgrey\">&nbsp;<span class=\"intableheader_jv\">Total</span></td>\n"
                + "                                                                </tr>\n"
                + "\n"
                + "\n"
                + "\n";

        int spaceForSummary = 0;
        for (ArDebitNoteItem c : slist) {

            i++;
            k++;
            totalwithouttax += c.getAmount();
            totaltax += c.getTaxamt();
            totalqty += c.getQty();

            report += "<tr height=\"40px\">\n"
                    + "                                                                    <td class=\"borderleft\">&nbsp;<span class=\"fontjv_item\">" + i + "</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\">&nbsp;<span class=\"fontjv_item\">" + c.getRemarks()+ "</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">" + GeneralTerm.currencyFormat(c.getQty())+ "</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">" + GeneralTerm.currencyFormat(Double.parseDouble(c.getUnitp())) + "</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">0.00</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">" + GeneralTerm.currencyFormat(c.getTaxamt()) + "</span></td>\n"
                    + "                                                                    <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">" + GeneralTerm.currencyFormat(c.getAmount()) + "</span></td>\n"
                    + "                                                                </tr>";

        }

        int cntSpace = 9;
        if (page != totalPage) {
            cntSpace = 8;
        }
        for (int j = 0; j < cntSpace - k + spaceForSummary; j++) {

            report += "                                                                    <tr height=\"20px\">\n"
                    + "                                                                        <td class=\"borderleft\">&nbsp;<span class=\"fontjv_item\">&nbsp;</span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"left\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\"></span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">&nbsp;</span></td>\n"
                    + "                                                                        <td class=\"borderrightend\" align=\"right\"><span class=\"fontjv_item\">&nbsp;</span></td>\n"
                    + "                                                                    </tr>\n";
        }
        report += "<tr>\n"
                + "                                                                    <td colspan=\"9\"></td>\n"
                + "                                                                </tr>\n"
                + "                                                                <tr>\n"
                + "                                                                    <td colspan=\"9\"></td>\n"
                + "                                                                </tr>\n"
                + "                                                                <tr>\n"
                + "                                                                    <td class=\"borderleft\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\"  valign=\"top\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\" align=\"right\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\" align=\"right\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\" align=\"right\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\" align=\"right\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\" align=\"right\">&nbsp;</td>\n"
                + "                                                                </tr>\n"
                + "                                                                <tr>\n"
                + "                                                                    <td class=\"borderleft\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderrightend\">&nbsp;</td>\n"
                + "                                                                </tr>\n"
                + "                                                                <tr>\n"
                + "                                                                    <td class=\"borderbottombold\">&nbsp;</td>\n"
                + "                                                                    <td class=\"borderbottomboldright\" valign=\"top\">&nbsp;</td>\n"
                + "                                                                    <td align=\"right\" class=\"borderbottomboldright\" colspan=\"4\">\n"
                + "                                                                        <span class=\"totalamount_jv\" style=\"font-size:11px;\">&nbsp;&nbsp;<strong>Total (Exclusive of GST)</strong></span>&nbsp;<br>\n"
                + "                                                                        <span class=\"totalamount_jv\" style=\"font-size:11px;\">&nbsp;&nbsp;<strong>Total GST 6%</strong></span>&nbsp;<br>\n"
                + "                                                                        <span class=\"totalamount_jv\" style=\"font-size:11px;\">&nbsp;&nbsp;<strong>Grand Total</strong></span>&nbsp;\n"
                + "                                                                    </td>\n"
                + "                                                                    <td align=\"right\" class=\"borderbottomboldright\">"
                + "                                                                          <p></p><span class=\"totalamount_jv\" style=\"font-size:11px;\">" + GeneralTerm.currencyFormat(totalwithouttax + totalCFwithouttax) + "<strong></strong></span>&nbsp;<br>"
                + "                                                                          <span class=\"totalamount_jv\" style=\"font-size:11px;\">" + GeneralTerm.currencyFormat(totaltax + totalCFtax) + "<strong></strong></span>&nbsp;<br>"
                + "                                                                          <span class=\"totalamount_jv\" style=\"font-size:11px;\">" + GeneralTerm.currencyFormat(totalwithouttax + totaltax + totalCFtax + totalCFwithouttax) + "<strong></strong></span>&nbsp;<p></p>"
                + "                                                                     </td>\n" + "                                                                </tr>"
                + "                                                                </table>\n"
                //                + "                                                                <table border=\"0\" width=\"100%\">\n"
                //                + "                                                                    <tr>\n"
                //                + "                                                                        <td width=\"25%\"><span class=\"intableheader_jv\">Ringgit Malaysia(in words)</span></td>\n"
                //                + "                                                                        <td>:</td>\n"
                //                + "                                                                        <td><span class=\"fontjv_item\">&nbsp;&nbsp;" + v.getRm() + "</span></td>\n"
                //                + "                                                                    </tr>\n"
                //                + "                                                                    <tr>\n"
                //                + "                                                                        <td><span class=\"intableheader_jv\">Bank / Branch</span></td>\n"
                //                + "                                                                        <td>:</td>\n"
                //                + "                                                                        <td><span class=\"fontjv_item\">&nbsp;&nbsp;" + v.getBankcode() + " - " + v.getBankname() + "</span></td>\n"
                //                + "                                                                    </tr>\n"
                //                + "                                                                    <tr>\n"
                //                + "                                                                        <td><span class=\"intableheader_jv\">Payment Mode</span></td>\n"
                //                + "                                                                        <td>:</td><td><span class=\"fontjv_item\">&nbsp;&nbsp;</span></td>\n"
                //                + "                                                                    </tr>  \n"
                //                + "                                                                    <tr>\n"
                //                + "                                                                        <td><span class=\"intableheader_jv\">Cheque No. </span></td>\n"
                //                + "                                                                        <td>:</td>\n"
                //                + "                                                                        <td><span class=\"fontjv_item\">&nbsp;&nbsp;</span></td>\n"
                //                + "                                                                    </tr>\n"
                //                + "                                                                </table>\n"
                + "                                                                <p>&nbsp;</p>\n"
                + "                                                            </td>\n"
                + "                                                        </tr>\n"
                + "                                                        <tr>\n"
                + "                                                            <td>&nbsp;<span class=\"boldtitle\">Year</span><span class=\"normtitle\"> - " + v.getYear() + "</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"boldtitle\">Period</span><span class=\"normtitle\"> - " + v.getPeriod() + "</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"boldtitle\">No</span><span class=\"normtitle\"> - " + v.getNoteno() + "</span></td>\n"
                + "                                                        </tr>\n";
        if (page == totalPage) {
            report += "                                                        <tr>\n"
                    + "                                                            <td>\n"
                    + "                                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
                    + "                                                                    <tr>\n"
                    + "                                                                        <td width=\"50%\" class=\"bordertopgreyauth\">\n"
                    + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
                    + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
                    + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;_________________________</p>\n"
                    + "                                                                            <p class=\"paytofontmiddle\">&nbsp;&nbsp;<strong>Prepared By</strong></p>\n"
                    + "                                                                        </td>\n"
                    + "                                                                        <td width=\"50%\" class=\"bordertopgreyauthright\">\n"
                    + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
                    + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;</p>\n"
                    + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;_________________________</p>\n"
                    + "                                                                            <p class=\"paytofontmiddle\">&nbsp;&nbsp;<strong>Approved By</strong></p>\n"
                    + "                                                                        </td>\n"
                    + "                                                                    </tr>\n"
                    + "                                                                    <tr>\n"
                    + "                                                                        <td class=\"borderleftbottomgrey\" valign=\"top\">\n"
                    + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Name : <strong>" + v.getPname()+ "</strong></p>\n"
                    + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Date : <strong>" + v.getPdate()+ "</strong></p>\n"
                    + "                                                                        </td>\n"
                    + "                                                                        <td class=\"borderleftbottomgreyright\" valign=\"top\">\n"
                    + "                                                                            <p class=\"paytofontmiddle\">&nbsp;&nbsp;</p>\n"
                    + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Name : <strong></strong></p>\n"
                    + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Designation : <strong></strong></p>\n"
                    + "                                                                            <p class=\"paytofonttop\">&nbsp;&nbsp;Date : <strong></strong></p>\n"
                    + "                                                                        </td>\n"
                    + "                                                                    </tr>\n"
                    + "                                                                </table>\n"
                    + "                                                            </td>\n"
                    + "                                                        </tr>\n";
        }
        report += "                                                    </table>    \n"
                + "                                                </div>";

        fm.setOutput(report);
        fm.setTotalEachWithoutTax(totalwithouttax);
        fm.setTotalEachPageTax(totaltax);
        fm.setRefer(v.getNoteno());
        fm.setPage(totalPage);

        return fm;

    }
    
}
