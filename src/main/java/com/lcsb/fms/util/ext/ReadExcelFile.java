/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.util.ext;

import com.lcsb.fms.dao.financial.ap.VendorPaymentDAO;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFRow;

/**
 *
 * @author fadhilfahmi
 */
public class ReadExcelFile {

    public static void main() {
        try {

            InputStream input = new BufferedInputStream(
                    new FileInputStream("/Users/fadhilfahmi/Google Drive/fms_v1/target/fms_v1-1.0/uploadedFiles/WriteSheet.xls"));
            POIFSFileSystem fs = new POIFSFileSystem(input);
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);

            Iterator rows = sheet.rowIterator();
            while (rows.hasNext()) {
                HSSFRow row = (HSSFRow) rows.next();
                System.out.println("\n");
                Iterator cells = row.cellIterator();
                while (cells.hasNext()) {

                    HSSFCell cell = (HSSFCell) cells.next();
                    ////if(HSSFCell.CELL_TYPE_NUMERIC==cell.getCellType())
                    //System.out.print( cell.getNumericCellValue()+"     " );

                    //else
                    if (HSSFCell.CELL_TYPE_STRING == cell.getCellType()) //System.out.print( cell.getStringCellValue()+"     " );
                    {
                        Logger.getLogger(ReadExcelFile.class.getName()).log(Level.INFO, "-----------" + cell.getStringCellValue() + "     ");
                    }
//                    else
//                        if(HSSFCell.CELL_TYPE_BOOLEAN==cell.getCellType())
//                        System.out.print( cell.getBooleanCellValue()+"     " );
//                        else
//                            if(HSSFCell.CELL_TYPE_BLANK==cell.getCellType())
//                                System.out.print( "BLANK     " );
//                                else
//                            System.out.print("Unknown cell type");

                }

            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
