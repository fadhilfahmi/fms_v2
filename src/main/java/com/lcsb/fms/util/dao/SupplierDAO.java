/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.util.dao;

import com.lcsb.fms.model.financial.ap.VendorAssign;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Supplier;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ersmiv
 */
public class SupplierDAO {
    
    public static List<Supplier> getSupplier(LoginProfile log, String keyword) throws Exception {
        
        
        Statement stmt = null;
        List<Supplier> Com;
        Com = new ArrayList();
        
        String q = "";
        
        if(keyword!=null){
            q = "where code like '%"+keyword+"%' or companyname like '%"+keyword+"%'";
        }
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from supplier_info "+q+" order by code limit 10");
Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getResult(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    private static Supplier getResult(ResultSet rs) throws SQLException {
        Supplier comp= new Supplier();
        comp.setSupplierAddress(rs.getString("address"));
        comp.setSupplierCity(rs.getString("city"));
        comp.setSupplierCode(rs.getString("code"));
        comp.setSupplierName(rs.getString("companyname"));
        comp.setSupplierPostcode(rs.getString("postcode"));
        comp.setSupplierState(rs.getString("state"));
        comp.setGstid(rs.getString("gstid"));
     
     
        return comp;
    }
    
    public static List<VendorAssign> getSupplierAssign(LoginProfile log, String keyword) throws Exception {
        
        
        Statement stmt = null;
        List<VendorAssign> Com;
        Com = new ArrayList();
        
        String q = "";
        
        if(keyword!=null){
            q = "where suppcode like '%"+keyword+"%' or suppname like '%"+keyword+"%'";
        }
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from ap_vendor "+q+" order by suppcode limit 10");
Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getResultAssign(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    private static VendorAssign getResultAssign(ResultSet rs) throws SQLException {
        VendorAssign comp= new VendorAssign();
        comp.setAccode(rs.getString("accode"));
        comp.setAcdesc(rs.getString("acdesc"));
        comp.setCompanycode(rs.getString("companycode"));
        comp.setCompanyname(rs.getString("companyname"));
        comp.setSuppaddress(rs.getString("suppaddress"));
        comp.setSuppcode(rs.getString("suppcode"));
        comp.setSuppname(rs.getString("suppname"));
        comp.setGstid(rs.getString("gstid"));
        return comp;
    }
    
    public static VendorAssign getAssignSupplier(LoginProfile log, String refno) throws SQLException, Exception {
        VendorAssign c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from ap_vendor where suppcode=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getResultAssign(rs);
        }

        
        return c;
    }
    
    
    
}
