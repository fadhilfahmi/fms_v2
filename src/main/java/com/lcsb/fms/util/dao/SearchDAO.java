/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.fms.util.dao;

import com.lcsb.fms.util.model.Company;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Search;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class SearchDAO {
    
    public static List<Search> getAllResult(LoginProfile log, String table,String column, String keyword, String method, String code, String descp, String special) throws Exception {
        
        Logger.getLogger(SearchDAO.class.getName()).log(Level.INFO, "select---- "+special);

        Statement stmt = null;
        List<Search> Com;
        Com = new ArrayList();
        
        String q = "";
        if(method.equals("start")){
            q = "like '"+keyword+"%'";
        }else if(method.equals("contain")){
            q = "like '%"+keyword+"%'";
        }
        
        
        
        if(table.equals("chartofacccount")){
            q += " and active = 'Yes'";
        }
        
        boolean spec = true;
        if(special == null){
             spec = false;
        }
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select "+code+","+descp+" from "+table+" where "+column+" "+q+" order by "+code+" limit 10");
Logger.getLogger(CompanyDAO.class.getName()).log(Level.INFO, "select "+code+","+descp+" from "+table+" where "+column+" "+q+" order by "+code+" limit 10");
            while (rs.next()) {
                
                if(spec){
                    
                    if(special.substring(0, 3).equals("EMP")){
                        
                        boolean c = SearchDAO.filterCode(log, rs.getString(code), special);
                        if(!c){
                          
                            Com.add(getResult(rs, code, descp));
                        }else{
                            Logger.getLogger(SearchDAO.class.getName()).log(Level.INFO, "tak ade");
                        }
                        
                    }
                    
                }else{
                    Com.add(getResult(rs, code, descp));
                }
                
                
            }
            
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        stmt.close();
        return Com;
    }
    
    private static Search getResult(ResultSet rs, String code, String descp) throws SQLException {
        Search comp= new Search();
        comp.setCode(rs.getString(code));
        comp.setDecsp(rs.getString(descp));
        return comp;
    }
    
    private static boolean filterCode(LoginProfile log, String code, String refer) throws SQLException, Exception {
        String table = "";
        String column1 = "";
        String column2 = "";
        boolean c = false;
        
        if(refer.substring(0, 3).equals("EMP")){
            table = "em_payroll_type";
            column1 = "refer";
            column2 = "earncode";
        }
        
        ResultSet rs = null;
        try {

            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * from "+table+" where "+column1+" = ? and "+column2+" = ?");
            stmt.setString(1, refer);
            stmt.setString(2, code);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }
    
}
