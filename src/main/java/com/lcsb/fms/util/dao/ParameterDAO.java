/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.fms.util.dao;

import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import com.lcsb.fms.util.model.ModuleItem;
import com.lcsb.fms.util.model.Parameter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class ParameterDAO {
    
    public static List<Parameter> getAllParameter(LoginProfile log, String type) throws Exception {
        
        Statement stmt = null;
        List<Parameter> Param;
        Param = new ArrayList();
        
       
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from parameter where parameter = '"+type+"'  order by dorder");

            while (rs.next()) {
                Param.add(getParameter(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        

        return Param;
    }
    
    public static List<Parameter> searchParameter(LoginProfile log, String type, String keyword) throws Exception {
        
        Statement stmt = null;
        List<Parameter> Param;
        Param = new ArrayList();
        
       
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from parameter where parameter = '"+type+"' and value like '%"+keyword+"%'  order by dorder asc");

            while (rs.next()) {
                Param.add(getParameter(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        

        return Param;
    }
    
    public static Parameter getParameter(LoginProfile log, String type) throws SQLException, Exception {
        Parameter c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from parameter where parameter=?");
        stmt.setString(1, type);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getParameter(rs);
        }

        
        return c;
    }
    
    private static Parameter getParameter(ResultSet rs) throws SQLException {
        Parameter prm = new Parameter();
        prm.setParameter(rs.getString("parameter"));
        prm.setValue(rs.getString("value"));
        return prm;
    }
    
    public static String parameterList(LoginProfile log, String type, String selected) throws Exception {
        
        Statement stmt = null;
        String drop = "";
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from parameter where parameter = '"+type+"'  order by  dorder");

            while (rs.next()) {
                if(rs.getString("value").equals(selected) && !selected.equals("")){
                    drop += "<option value = \""+rs.getString("value")+"\" selected>"+rs.getString("value")+"</option>";
                }else{
                    drop += "<option value = \""+rs.getString("value")+"\">"+rs.getString("value")+"</option>";
                }    
                
            }
            
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
       
        return drop;
    }
    
   
}
