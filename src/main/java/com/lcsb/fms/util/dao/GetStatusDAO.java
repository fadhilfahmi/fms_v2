/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.util.dao;

import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.development.SecModule;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author fadhilfahmi
 */
public class GetStatusDAO {
    
    public static String getStatus(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String check = "";
        String app = "";
        String post = "";
        String status = "<span class=\"label label-primary\">Preparing</span>";
        ResultSet rs = null;
        
        SecModule sc = (SecModule) GeneralTerm.getType(log, refer.substring(0, 3));

        PreparedStatement stmt = log.getCon().prepareStatement("select "+sc.getCheck()+","+sc.getApprove()+","+sc.getPost()+" from "+sc.getMaintable()+" where "+sc.getReferidMaster()+" = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            check = rs.getString(1);
            app = rs.getString(2);
            post = rs.getString(3);
        }

        stmt.close();
        rs.close();

        if (!check.equals("")) {
            status = "Checked";
        }

        if (!app.equals("")) {
            status = "Approved";
        }

//        if(post.equals("posted")){
//            status = "<span class=\"label label-info\">Post</span>";
//        }
        if (post.equals("cancel")) {
            status = "Canceled";
        }

        return status;
    }
    
    public static boolean isCheck(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;
        SecModule sc = (SecModule) GeneralTerm.getType(log, refer.substring(0, 3));
        
        PreparedStatement stmt = log.getCon().prepareStatement("select "+ sc.getCheck() +" from "+ sc.getMaintable()+" where "+ sc.getReferidMaster()+" = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString(1);
        }

        if (!ck.equals("")) {
            cek = true;
        }

        stmt.close();
        rs.close();

        return cek;
    }
    
    public static boolean isApprove(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;

        SecModule sc = (SecModule) GeneralTerm.getType(log, refer.substring(0, 3));
        
        PreparedStatement stmt = log.getCon().prepareStatement("select "+ sc.getApprove()+" from "+ sc.getMaintable()+" where "+ sc.getReferidMaster()+" = ?");
        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString(1);
        }

        if (!ck.equals("")) {
            cek = true;
        }

        stmt.close();
        rs.close();

        return cek;
    }
    
}
