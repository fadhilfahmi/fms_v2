/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.util.dao;

import com.lcsb.fms.util.model.Buyer;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ersmiv
 */
public class BuyerDAO {
    
    public static List<Buyer> getBuyer(LoginProfile log, String keyword) throws Exception {
        
        
        Statement stmt = null;
        List<Buyer> Com;
        Com = new ArrayList();
        
        String q = "";
        
        if(keyword!=null){
            q = "where code like '%"+keyword+"%' or companyname like '%"+keyword+"%'";
        }
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select code,companyname,address,city,state,postcode,gstid,coa,coadescp from buyer_info "+q+" order by code limit 10");
Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getResult(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    public static List<Buyer> getBuyerMill(LoginProfile log, String keyword) throws Exception {
        
        
        Statement stmt = null;
        List<Buyer> Com;
        Com = new ArrayList();
        
        
        
        String q = "";
        
        if(keyword!=null){
            q = "where code like '%"+keyword+"%' or descp like '%"+keyword+"%'";
        }
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select code,descp from sl_contractor_info "+q+" order by code limit 15");
Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                Buyer by = new Buyer();
                by.setBuyerCode(rs.getString("code"));
                by.setBuyerName(rs.getString("descp"));
                
                Com.add(by);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    private static Buyer getResult(ResultSet rs) throws SQLException {
        Buyer comp= new Buyer();
        comp.setBuyerAddress(rs.getString("address"));
        comp.setBuyerCity(rs.getString("city"));
        comp.setBuyerCode(rs.getString("code"));
        comp.setBuyerName(rs.getString("companyname"));
        comp.setBuyerPostcode(rs.getString("postcode"));
        comp.setBuyerState(rs.getString("state"));
        comp.setGstID(rs.getString("gstid"));
        comp.setCoa(rs.getString("coa"));
        comp.setCoadescp(rs.getString("coadescp"));
       
        return comp;
    }
    
}
