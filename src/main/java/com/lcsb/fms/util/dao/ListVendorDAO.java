/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.util.dao;


import com.lcsb.fms.model.setup.configuration.Vendor;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class ListVendorDAO {
    
    public static List<Vendor> getAllVendor(LoginProfile log, String keyword) throws Exception {
        
        
        Statement stmt = null;
        List<Vendor> Com;
        Com = new ArrayList();
        
        String q = "";
        
        if(keyword!=null){
            q = "where code like '%"+keyword+"%' or vendor_name like '%"+keyword+"%'";
        }
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from vendor_info "+q+" order by code");
Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getResult(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    private static Vendor getResult(ResultSet rs) throws SQLException {
        Vendor nw= new Vendor();
        nw.setActive(rs.getString("active"));
        nw.setBank(rs.getString("bank"));
        nw.setBankaccount(rs.getString("bankaccount"));
        nw.setBankcode(rs.getString("bankcode"));
        nw.setBankdesc(rs.getString("bankdesc"));
        nw.setBumiputra(rs.getString("bumiputra"));
        nw.setCity(rs.getString("city"));
        nw.setCoa(rs.getString("coa"));
        nw.setCoadescp(rs.getString("coadescp"));
        nw.setCode(rs.getString("code"));
        nw.setCountry(rs.getString("country"));
        nw.setEmail(rs.getString("email"));
        nw.setFax(rs.getString("fax"));
        nw.setHp(rs.getString("hp"));
        nw.setPayment(rs.getString("payment"));
        nw.setPerson(rs.getString("person"));
        nw.setPhone(rs.getString("phone"));
        nw.setPosition(rs.getString("position"));
        nw.setPostcode(rs.getString("postcode"));
        nw.setRegisterNo(rs.getString("register_no"));
        nw.setRemarks(rs.getString("remarks"));
        nw.setState(rs.getString("state"));
        nw.setTitle(rs.getString("title"));
        nw.setUrl(rs.getString("url"));
        nw.setVendorAddress(rs.getString("vendor_address"));
        nw.setVendorName(rs.getString("vendor_name"));
        return nw;
    }
   
}
