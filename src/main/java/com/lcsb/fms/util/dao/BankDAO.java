/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.util.dao;

import com.lcsb.fms.dao.setup.company.ManagementCpDAO;
import com.lcsb.fms.model.setup.company.BankBranch;
import com.lcsb.fms.model.financial.cashbook.OfficialReceipt;
import com.lcsb.fms.model.setup.company.CeManageBank;
import com.lcsb.fms.model.setup.configuration.BankGeneral;
import com.lcsb.fms.util.model.Bank;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Tax;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class BankDAO {
    
    public static List<Bank> getAllBank(LoginProfile log, String keyword) throws Exception {
        
        Connection con = log.getCon();
        
        Statement stmt = null;
        List<Bank> Com;
        Com = new ArrayList();
        
        String q = "";
        
        if(keyword!=null){
            q = "where code like '%"+keyword+"%' or name like '%"+keyword+"%'";
        }
        
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from info_bank "+q+" order by code");
Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getResult(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    public static List<BankGeneral> getAllBankGeneral(LoginProfile log, String keyword) throws Exception {
        
        
        Statement stmt = null;
        List<BankGeneral> Com;
        Com = new ArrayList();
        
        String q = "";
        
        if(keyword!=null){
            q = "where code like '%"+keyword+"%' or name like '%"+keyword+"%'";
        }
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from info_bank_general "+q+" order by code");
Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getResultGeneral(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    public static BankGeneral getBankGeneral(LoginProfile log, String code) throws Exception {
        
       
        BankGeneral c = null;
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from info_bank_general where code = ?");
            stmt.setString(1, code);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getResultGeneral(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }
    
    private static BankGeneral getResultGeneral(ResultSet rs) throws SQLException {
        BankGeneral comp= new BankGeneral();
        comp.setAbb(rs.getString("abb"));
        comp.setAddress(rs.getString("address"));
        comp.setCity(rs.getString("city"));
        comp.setCode(rs.getString("code"));
        comp.setGstid(rs.getString("gstid"));
        comp.setName(rs.getString("name"));
        comp.setPostcode(rs.getString("postcode"));
        comp.setState(rs.getString("state"));
        return comp;
    }
    
    private static Bank getResult(ResultSet rs) throws SQLException {
        Bank comp= new Bank();
        comp.setAbbreviation(rs.getString("abbreviation"));
        comp.setCoa(rs.getString("coa"));
        comp.setCoadescp(rs.getString("coadescp"));
        comp.setCode(rs.getString("code"));
        comp.setName(rs.getString("name"));
        comp.setOverdue(rs.getString("overdue"));
        comp.setRemarks(rs.getString("remarks"));
        return comp;
    }
    
    public static List<BankBranch> getAllBranch(LoginProfile log, String keyword) throws Exception {
        
        
        Statement stmt = null;
        List<BankBranch> Com;
        Com = new ArrayList();
        
        String q = "";
        
        if(keyword!=null){
            q = "where branchcode like '%"+keyword+"%' or branchname like '%"+keyword+"%'";
        }
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from info_branch "+q+" order by branchcode");
Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getResultBranch(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    private static BankBranch getResultBranch(ResultSet rs) throws SQLException {
        BankBranch comp= new BankBranch();
        comp.setAddress(rs.getString("address"));
        comp.setBankcode(rs.getString("bankcode"));
        comp.setBankname(rs.getString("bankname"));
        comp.setBranchcode(rs.getString("branchcode"));
        comp.setBranchname(rs.getString("branchname"));
        comp.setCity(rs.getString("city"));
        comp.setEmail(rs.getString("email"));
        comp.setFax(rs.getString("fax"));
        comp.setHp(rs.getString("hp"));
        comp.setPerson(rs.getString("person"));
        comp.setPhone(rs.getString("phone"));
        comp.setPosition("position");
        comp.setPostcode("postcode");
        comp.setRemarks("remarks");
        comp.setState("state");
        comp.setTitle("title");
        comp.setUrl("url");
        return comp;
    }
    
//    public static Bank getInfo(String bankcode) throws SQLException, Exception {
//        Bank c = null;
//        ResultSet rs = null;
//        Connection conet = ConnectionUtil.getConnection();
//        
//        try {
//            PreparedStatement stmt = conet.prepareStatement("select * from ce_manage_bank where code=left(?,2)");
//            stmt.setString(1, bankcode);
//            
//            rs = stmt.executeQuery();
//            Logger.getLogger(BankDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
//            if (rs.next()) {
//                c = getResult(rs);
//            }
//            
//            stmt.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        
//        return c;
//    }
    
    public static CeManageBank getInfo(LoginProfile log, String bankcode) throws SQLException, Exception {
        CeManageBank c = null;
        ResultSet rs = null;
        
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from ce_manage_bank where code=?");
            stmt.setString(1, bankcode);
            
            rs = stmt.executeQuery();
            Logger.getLogger(BankDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            if (rs.next()) {
                c = ManagementCpDAO.getBank(rs);
            }
            
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        
        return c;
    }
    
    public static List<CeManageBank> getAllBankCA(LoginProfile log, String keyword) throws Exception {
        
        
        Statement stmt = null;
        List<CeManageBank> Com;
        Com = new ArrayList();
        
        String q = "";
        
        if(keyword!=null){
            q = "and code like '%"+keyword+"%' or name like '%"+keyword+"%'";
        }
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from ce_manage_bank WHERE ftype = 'CURRENT ACCOUNT' "+q+" order by code");
Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(ManagementCpDAO.getBank(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
}
