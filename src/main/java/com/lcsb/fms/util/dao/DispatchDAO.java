/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.util.dao;

import com.lcsb.fms.dao.setup.company.MillDAO;
import com.lcsb.fms.model.financial.ar.ArKernelRefinery;
import com.lcsb.fms.model.financial.ar.ArKernelRefineryPK;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class DispatchDAO {
    
    public static List<ArKernelRefinery> getDispatchNo(LoginProfile log, String table, String keyword) throws Exception {
        
        Connection con = log.getCon();
        
        Statement stmt = null;
        List<ArKernelRefinery> Com;
        Com = new ArrayList();
        
        String q = "";
        
        if(keyword!=null){
            q = "where dispatch_no like '%"+keyword+"%'";
        }
        
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from "+table+" "+q+" order by dispatch_no limit 10");
Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getResult(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        

        return Com;
    }
    
    private static ArKernelRefinery getResult(ResultSet rs) throws SQLException {
        ArKernelRefinery comp= new ArKernelRefinery();
        comp.setDispatchNo(rs.getString("dispatch_no"));
        comp.setArKernelRefineryPK(getResultPK(rs));
        comp.setMt(rs.getString("mt"));
        comp.setMtRefinery(rs.getDouble("mt_refinery"));
     
        return comp;
    }
    
    private static ArKernelRefineryPK getResultPK(ResultSet rs) throws SQLException {
        ArKernelRefineryPK comp= new ArKernelRefineryPK();
        comp.setDate(rs.getDate("date"));
        comp.setTrailer(rs.getString("trailer"));
     
     
        return comp;
    }
    
}
