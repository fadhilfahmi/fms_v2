/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.general;

import com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO;
import com.lcsb.fms.dao.financial.cashbook.BankReportDAO;
import com.lcsb.fms.dao.financial.cashbook.CashVoucherDAO;
import com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO;
import com.lcsb.fms.dao.financial.cashbook.PaymentVoucherDAO;
import com.lcsb.fms.dao.financial.employee.EmPayrollDAO;
import com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.JournalVoucherDAO;
import com.lcsb.fms.model.financial.ar.SalesInvoice;
import com.lcsb.fms.model.financial.cashbook.CashVoucher;
import com.lcsb.fms.model.financial.cashbook.OfficialReceipt;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucher;
import com.lcsb.fms.model.financial.employee.EmPayroll;
import com.lcsb.fms.model.financial.gl.JournalVoucher;
import com.lcsb.fms.ui.button.Button;
import com.lcsb.fms.util.dao.GetStatusDAO;
import com.lcsb.fms.util.dao.ModuleDAO;
import com.lcsb.fms.util.model.LoginProfile;
import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class DataTableDAO {

    public static DataTable getData(LoginProfile log, DataTable prm) throws Exception {

        DataTable dt = new DataTable();

        dt.setModule(ModuleDAO.getModule(log, prm.getModuleTypeID()));

        if (prm.getModuleTypeID().equals("020903")) {//Sales Invoice

            dt.setListTable(SalesInvoiceDAO.tableList());

            List<SalesInvoice> listx = (List<SalesInvoice>) SalesInvoiceDAO.getAll(log, prm);

            String renderRow = "";

            for (SalesInvoice j : listx) {
                renderRow += "<tr class=\"activerowy " + getCancelCSSClass(log, j.getInvref()) + "\" id=\"<%//= j.getId()%>\">"
                        + "<td class=\"tdrow\">" + j.getInvref() + "</td>"
                        + "<td class=\"tdrow\">&nbsp;" + j.getInvdate() + "</td>"
                        + "<td class=\"tdrow\">&nbsp;" + j.getBname() + "</td>"
                        + "<td class=\"tdrow\">&nbsp;" + j.getProdname() + "</td>"
                        + "<td class=\"tdrow\">&nbsp;<span class=\"pull-right\">" + GeneralTerm.currencyFormat(j.getAmountno()) + "</span></td>"
                        + "<td class=\"tdrow\" align=\"left\">&nbsp;" + GeneralTerm.getStatusLabel(GetStatusDAO.getStatus(log, j.getInvref())) + DebitCreditNoteDAO.checkDCNoteExist(log, j.getInvref()) + "</td>"
                        + "<td class=\"tdrow\" align=\"right\">&nbsp;"
                        + "<div class=\"btn-group btn-group-xs\" role=\"group\" aria-label=\"...\">";
                if (prm.getViewBy().equals("all")) {
                    renderRow += "<button id=\"<%//= j.getId()%>\" class=\"btn btn-default btn-xs edititem\" title=\"<%//= mod.getModuleID()%>\" name=\"edit_st\"><i class=\"fa fa-pencil\"></i></button>"
                            + " <button id=\"<%//= j.getId()%>\" class=\"btn btn-default btn-xs deleteitem\" title=\"<%//= mod.getModuleID()%>\"  name=\"delete_st\"><i class=\"fa fa-trash-o\"></i></button>";

                } else if (prm.getViewBy().equals("approval")) {
                    renderRow += "<button id=\"<%//= j.getId()%>\" class=\"btn btn-default btn-xs edititem\" title=\"<%//= mod.getModuleID()%>\" name=\"edit_st\"><i class=\"fa fa-pencil\"></i> Approve Now</button>";
                }else if (prm.getViewBy().equals("check")) {
                    renderRow += "<button id=\"<%//= j.getId()%>\" class=\"btn btn-default btn-xs edititem\" title=\"<%//= mod.getModuleID()%>\" name=\"edit_st\"><i class=\"fa fa-pencil\"></i> View</button>";
                }
                renderRow += "</div>"
                        + "</td>"
                        + "</tr>";
            }

            dt.setRenderData(renderRow);

        } else if (prm.getModuleTypeID().equals("020203")) {//Official Receipt

            dt.setListTable(OfficialReceiptDAO.tableList());

            List<OfficialReceipt> listx = (List<OfficialReceipt>) OfficialReceiptDAO.getAll(log, prm);

            String renderRow = "";

            for (OfficialReceipt j : listx) {

                renderRow += "<tr class=\"activerowy " + getCancelCSSClass(log, j.getRefer()) + "\" id=\"<%//= j.getId()%>\">"
                        + "<td class=\"tdrow\">" + j.getRefer() + "</td>"
                        + "<td class=\"tdrow\">&nbsp;" + j.getDate() + "</td>"
                        + "<td class=\"tdrow\">&nbsp;" + j.getPaidname() + "</td>"
                        + "<td class=\"tdrow\">&nbsp;<span class=\"pull-right\">" + GeneralTerm.currencyFormat(j.getAmount()) + "</span></td>"
                        + "<td class=\"tdrow\" align=\"left\">&nbsp;" + GeneralTerm.getStatusLabel(GetStatusDAO.getStatus(log, j.getRefer())) + DebitCreditNoteDAO.checkDCNoteExist(log, j.getRefer()) + "</td>"
                        + "<td class=\"tdrow\" align=\"right\">&nbsp;"
                        + "<div class=\"btn-group btn-group-xs\" role=\"group\" aria-label=\"...\">";

                renderRow += "<button id=\"" + j.getRefer() + "\" type=\"button\" class=\"btn btn-default btn-xs replicate\" title=\"" + OfficialReceiptDAO.getModule().getModuleID() + "\"><i class=\"fa fa-clone\" aria-hidden=\"true\"></i></button>"
                        + Button.viewPrintedButton(OfficialReceiptDAO.getModule().getModuleID(), j.getRefer())
                        + " <button id=\"" + j.getRefer() + "\" class=\"btn btn-danger btn-xs deletemain\" title=\"" + OfficialReceiptDAO.getModule().getModuleID() + "\"  name=\"delete_st\"><i class=\"fa fa-trash-o\"></i></button>";

                renderRow += "</div>"
                        + "</td>"
                        + "</tr>";
            }

            dt.setRenderData(renderRow);

        } else if (prm.getModuleTypeID().equals("020204")) {//Payment Voucher

            dt.setListTable(PaymentVoucherDAO.tableList());

            List<PaymentVoucher> listx = (List<PaymentVoucher>) PaymentVoucherDAO.getAll(log, prm);

            String renderRow = "";

            for (PaymentVoucher j : listx) {

                String addClass = "";
                if (GetStatusDAO.getStatus(log, j.getRefer()) == "Canceled") {
                    addClass = "cancelrow";
                }

                renderRow += "<tr class=\"activerowy " + addClass + "\" id=\"<%//= j.getId()%>\">"
                        + "<td class=\"tdrow\">" + j.getRefer() + "</td>"
                        + "<td class=\"tdrow\">&nbsp;" + j.getTarikh() + "</td>"
                        + "<td class=\"tdrow\">&nbsp;" + j.getPaidname() + "</td>"
                        + "<td class=\"tdrow\">&nbsp;<span class=\"pull-right\">" + GeneralTerm.currencyFormat(j.getAmount()) + "</span></td>"
                        + "<td class=\"tdrow\" align=\"left\">&nbsp;" + GeneralTerm.getStatusLabel(GetStatusDAO.getStatus(log, j.getRefer())) + DebitCreditNoteDAO.checkDCNoteExist(log, j.getRefer()) + "</td>"
                        + "<td class=\"tdrow\" align=\"right\">&nbsp;"
                        + "<div class=\"btn-group btn-group-xs\" role=\"group\" aria-label=\"...\">";

                renderRow += "<button id=\"" + j.getRefer() + "\" type=\"button\" class=\"btn btn-default btn-xs replicate\" title=\"" + PaymentVoucherDAO.getModule().getModuleID() + "\"><i class=\"fa fa-clone\" aria-hidden=\"true\"></i></button>"
                        + Button.viewPrintedButton(PaymentVoucherDAO.getModule().getModuleID(), j.getRefer())
                        + " <button id=\"" + j.getRefer() + "\" class=\"btn btn-danger btn-xs deletemain\" title=\"" + PaymentVoucherDAO.getModule().getModuleID() + "\"  name=\"delete_st\"><i class=\"fa fa-trash-o\"></i></button>";

                renderRow += "</div>"
                        + "</td>"
                        + "</tr>";
            }

            dt.setRenderData(renderRow);

        } else if (prm.getModuleTypeID().equals("020104")) {//Journal Voucher

            dt.setListTable(JournalVoucherDAO.tableList());

            List<JournalVoucher> listx = (List<JournalVoucher>) JournalVoucherDAO.getAll(log, prm);

            String renderRow = "";

            for (JournalVoucher j : listx) {

                String addClass = "";
                if (GetStatusDAO.getStatus(log, j.getJVrefno()) == "Canceled") {
                    addClass = "cancelrow";
                }

                renderRow += "<tr class=\"activerowy " + addClass + "\" id=\"<%//= j.getId()%>\">"
                        + "<td class=\"tdrow\">" + j.getJVrefno() + "</td>"
                        + "<td class=\"tdrow\">&nbsp;" + j.getJVdate()+ "</td>"
                        + "<td class=\"tdrow\">&nbsp;<span class=\"pull-right\">" + GeneralTerm.currencyFormat(Double.parseDouble(j.getTodebit())) + "</span></td>"
                        + "<td class=\"tdrow\">&nbsp;<span class=\"pull-right\">" + GeneralTerm.currencyFormat(Double.parseDouble(j.getTocredit())) + "</span></td>"
                        + "<td class=\"tdrow\" align=\"left\">&nbsp;" + GeneralTerm.getStatusLabel(GetStatusDAO.getStatus(log, j.getJVrefno())) + DebitCreditNoteDAO.checkDCNoteExist(log, j.getJVrefno()) + "</td>"
                        + "<td class=\"tdrow\" align=\"right\">&nbsp;"
                        + "<div class=\"btn-group btn-group-xs\" role=\"group\" aria-label=\"...\">";

                renderRow += "<button id=\"" + j.getJVrefno() + "\" type=\"button\" class=\"btn btn-default btn-xs replicate\" title=\"" + JournalVoucherDAO.getModule().getModuleID() + "\"><i class=\"fa fa-clone\" aria-hidden=\"true\"></i></button>"
                        + Button.viewPrintedButton(JournalVoucherDAO.getModule().getModuleID(), j.getJVrefno())
                        + " <button id=\"" + j.getJVrefno() + "\" class=\"btn btn-danger btn-xs deletemain\" title=\"" + JournalVoucherDAO.getModule().getModuleID() + "\"  name=\"delete_st\"><i class=\"fa fa-trash-o\"></i></button>";

                renderRow += "</div>"
                        + "</td>"
                        + "</tr>";
            }

            dt.setRenderData(renderRow);

        } else if (prm.getModuleTypeID().equals("020309")) {//Payroll

            dt.setListTable(EmPayrollDAO.tableList());

            List<EmPayroll> listx = (List<EmPayroll>) EmPayrollDAO.getAll(log, prm);

            String renderRow = "";

            for (EmPayroll j : listx) {

                String addClass = "";
//                if (GetStatusDAO.getStatus(log, j.getRefer()) == "Canceled") {
//                    addClass = "cancelrow";
//                }

                renderRow += "<tr class=\"activerowy " + addClass + "\" id=\"<%//= j.getId()%>\">"
                        + "<td class=\"tdrow\">" + j.getRefer() + "</td>"
                        + "<td class=\"tdrow\">&nbsp;" + j.getDate()+ "</td>"
                        + "<td class=\"tdrow\">&nbsp;" + j.getYear()+ "</td>"
                        + "<td class=\"tdrow\">&nbsp;" + j.getPeriod()+ "</td>"
                        + "<td class=\"tdrow\"><div class=\"progress\">\n" +
"  <div class=\"progress-bar\" role=\"progressbar\" style=\"width: 25%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n" +
"</div></td>"
                        + "<td class=\"tdrow\" align=\"right\">&nbsp;"
                        + "<div class=\"btn-group btn-group-xs\" role=\"group\" aria-label=\"...\">";

                renderRow += "<button id=\"" + j.getRefer() + "\" type=\"button\" class=\"btn btn-default btn-xs replicate\" title=\"" + EmPayrollDAO.getModule().getModuleID() + "\"><i class=\"fa fa-clone\" aria-hidden=\"true\"></i></button>"
                        + Button.viewPrintedButton(EmPayrollDAO.getModule().getModuleID(), j.getRefer())
                        + " <button id=\"" + j.getRefer() + "\" class=\"btn btn-danger btn-xs deletemain\" title=\"" + EmPayrollDAO.getModule().getModuleID() + "\"  name=\"delete_st\"><i class=\"fa fa-trash-o\"></i></button>";

                renderRow += "</div>"
                        + "</td>"
                        + "</tr>";
            }

            dt.setRenderData(renderRow);

        }else if (prm.getModuleTypeID().equals("020202")) {//Cash Voucher

            dt.setListTable(CashVoucherDAO.tableList());

            List<CashVoucher> listx = (List<CashVoucher>) CashVoucherDAO.getAll(log, prm);

            String renderRow = "";

            for (CashVoucher j : listx) {

                String addClass = "";
                if (GetStatusDAO.getStatus(log, j.getRefer()) == "Canceled") {
                    addClass = "cancelrow";
                }

                renderRow += "<tr class=\"activerowy " + addClass + "\" id=\"<%//= j.getId()%>\">"
                        + "<td class=\"tdrow\">" + j.getRefer() + "</td>"
                        + "<td class=\"tdrow\">&nbsp;" + j.getVoucherdate()+ "</td>"
                        + "<td class=\"tdrow\">&nbsp;" + j.getPayto()+ "</td>"
                        + "<td class=\"tdrow\">&nbsp;<span class=\"pull-right\">" + GeneralTerm.currencyFormat(j.getTotal()) + "</span></td>"
                        + "<td class=\"tdrow\" align=\"left\">&nbsp;" + GeneralTerm.getStatusLabel(GetStatusDAO.getStatus(log, j.getRefer())) + DebitCreditNoteDAO.checkDCNoteExist(log, j.getRefer()) + "</td>"
                        + "<td class=\"tdrow\" align=\"right\">&nbsp;"
                        + "<div class=\"btn-group btn-group-xs\" role=\"group\" aria-label=\"...\">";

                renderRow += "<button id=\"" + j.getRefer() + "\" type=\"button\" class=\"btn btn-default btn-xs replicate\" title=\"" + CashVoucherDAO.getModule().getModuleID() + "\"><i class=\"fa fa-clone\" aria-hidden=\"true\"></i></button>"
                        + Button.viewPrintedButton(CashVoucherDAO.getModule().getModuleID(), j.getRefer())
                        + " <button id=\"" + j.getRefer() + "\" class=\"btn btn-danger btn-xs deletemain\" title=\"" + CashVoucherDAO.getModule().getModuleID() + "\"  name=\"delete_st\"><i class=\"fa fa-trash-o\"></i></button>";

                renderRow += "</div>"
                        + "</td>"
                        + "</tr>";
            }

            dt.setRenderData(renderRow);

        } else if (prm.getModuleTypeID().equals("020207")) {

            dt.setListTable(BankReportDAO.tableList());

//            List<SalesInvoice> listx = (List<SalesInvoice>) SalesInvoiceDAO.getAll();
//
//            String renderRow = "";
//
//            for (SalesInvoice j : listx) {
//                renderRow += "<tr class=\"activerowy\" id=\"<%//= j.getId()%>\">"
//            + "<td class=\"tdrow\">" + j.getInvref() + "</td>"
//            + "<td class=\"tdrow\">&nbsp;" + j.getInvdate() + "</td>"
//            + "<td class=\"tdrow\">&nbsp;" + j.getBname() + "</td>"
//            + "<td class=\"tdrow\">&nbsp;" + j.getProdname() + "</td>"
//            + "<td class=\"tdrow\" align=\"left\">&nbsp;" + GeneralTerm.getStatusLabel(SalesInvoiceDAO.getStatus(j.getInvref())) + DebitCreditNoteDAO.checkDCNoteExist(j.getInvref()) +"</td>"
//            + "<td class=\"tdrow\" align=\"right\">&nbsp;"
//                + "<div class=\"btn-group btn-group-xs\" role=\"group\" aria-label=\"...\">"
//                    + "<button id=\"<%//= j.getId()%>\" class=\"btn btn-default btn-xs edititem\" title=\"<%//= mod.getModuleID()%>\" name=\"edit_st\"><i class=\"fa fa-pencil\"></i></button>"
//                   + " <button id=\"<%//= j.getId()%>\" class=\"btn btn-default btn-xs deleteitem\" title=\"<%//= mod.getModuleID()%>\"  name=\"delete_st\"><i class=\"fa fa-trash-o\"></i></button>"
//                + "</div>"
//            + "</td>"
//        + "</tr>";
//            }
//            
//            dt.setRenderData(renderRow);
        }

        return dt;
    }

    private static String getCancelCSSClass(LoginProfile log, String refer) throws Exception {

        String addClass = "";
        if (GetStatusDAO.getStatus(log, refer) == "Canceled") {
            addClass = "cancelrow";
        }
        return addClass;

    }

}
