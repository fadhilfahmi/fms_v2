/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.development;

import com.lcsb.fms.dao.financial.ap.VendorInvoiceDAO;
import com.lcsb.fms.dao.financial.gl.DistributeDAO;
import com.lcsb.fms.financial.gl.post.Distribute;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.util.dao.ModuleDAO;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class PostTransDAO {

    public static ArrayList<String> listModule(LoginProfile log) {

        ArrayList<String> r = new ArrayList<String>();

        try {
            ResultSet rs = null;

            PreparedStatement stmt = log.getCon().prepareStatement("select * from sec_module where abb is not null");
            //stmt.setString(1, id);
            rs = stmt.executeQuery();
            while (rs.next()) {
                r.add(rs.getString("moduleid"));
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return r;

    }

    public static int totalUnpost(LoginProfile log, String moduleid) throws Exception {

        int i = 0;

        try {
            ResultSet rs = null;

            PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from " + ModuleDAO.getModule(log, moduleid).getMainTable() + " where " + ModuleDAO.getModule(log, moduleid).getApprove() + " = ''");
            //stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                i = rs.getInt("cnt");
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return i;
    }

    public static int postAll(LoginProfile log) throws Exception {

        int u = 0;

        ArrayList<String> list = PostTransDAO.listModule(log);

        for (int i = 0; i < list.size(); i++) {

            if (PostTransDAO.totalUnpost(log, list.get(i)) > 0) {

                try {
                    
                    Module mod = ModuleDAO.getModule(log, list.get(i));
                    ResultSet rs = null;

                    PreparedStatement stmt = log.getCon().prepareStatement("select " + mod.getReferID_Master()+ " from " + mod.getMainTable() + " where " + mod.getApprove() + " = ''");
                    //stmt.setString(1, id);
                    rs = stmt.executeQuery();
                    while (rs.next()) {
                        Distribute dist = DistributeDAO.distributeNow(rs.getString(mod.getReferID_Master()), mod.getPosttype(), log);
                        
                        if(dist.isSuccess()){
                            i++;
                            Logger.getLogger(PostTransDAO.class.getName()).log(Level.INFO, "------ "+mod.getModuleDesc()+" not successfull posted - "+rs.getString(mod.getReferID_Master()));
                        }else{
                            PostTransDAO.updateApprovalStatus(log, rs.getString(mod.getReferID_Master()), mod.getModuleID());
                            Logger.getLogger(PostTransDAO.class.getName()).log(Level.INFO, "------ "+mod.getModuleDesc()+" successfull posted - "+rs.getString(mod.getReferID_Master()));
                        }
                    }

                    rs.close();
                    stmt.close();

                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }

        }

        //1-loop all module and check the unpost transaction
        //2-filter the unpost transaction only
        //3-post the transaction using postDAO
        //4-return the status

        return u;

    }
    
    private static void updateApprovalStatus(LoginProfile log, String voucherno, String moduleid) throws Exception, SQLException{
        
        Module mod = ModuleDAO.getModule(log, moduleid);
        
        String query = "";
            query = "UPDATE "+mod.getMainTable()+" SET "+mod.getApprove()+" = '"+log.getUserID()+"' where "+mod.getReferID_Master()+" = '"+voucherno+"'";
        
        
        PreparedStatement ps = log.getCon().prepareStatement(query);
        Logger.getLogger(DistributeDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
        ps.executeUpdate();
        ps.close();
        
    }

}
