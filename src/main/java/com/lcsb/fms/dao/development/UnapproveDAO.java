/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.development;

import com.lcsb.fms.dao.financial.tx.TaxOutputDAO;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.development.SecModule;
import com.lcsb.fms.model.development.Unapp;
import com.lcsb.fms.model.development.Unapprove;
import com.lcsb.fms.model.financial.gl.GeneralLedger;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class UnapproveDAO {

    public static Unapprove getMaster(LoginProfile log, String refer) throws Exception {

        Unapprove un = new Unapprove();

        un.setListGL(getGlListing(log, refer));

        SecModule sc = GeneralTerm.getType(log, refer.substring(0, 3));

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM " + sc.getMaintable() + " WHERE " + sc.getReferidMaster() + "=?");
        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {

            un.setApproveStatus(rs.getString(sc.getApprove()));
            un.setCheckStatus(rs.getString(sc.getCheck()));
            un.setPostStatus(getStatus(log, sc, refer));

        }

        return un;
    }

    private static List<GeneralLedger> getGlListing(LoginProfile log, String refer) throws Exception {

        Statement stmt = null;
        List<GeneralLedger> gl;
        gl = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from gl_posting_distribute where NoVoucher = '" + refer + "' order by COAcode");

            while (rs.next()) {
                gl.add(getGL(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return gl;
    }

    private static GeneralLedger getGL(ResultSet rs) throws SQLException {
        GeneralLedger gl = new GeneralLedger();

        gl.setCOACode(rs.getString("coacode"));
        gl.setCOADesc(rs.getString("cOADesc"));
        gl.setTarikh(rs.getDate("tarikh"));
        gl.setDebit(rs.getDouble("debit"));
        gl.setCredit(rs.getDouble("credit"));
        gl.setNoVoucher(rs.getString("noVoucher"));

        return gl;
    }

    private static String getStatus(LoginProfile log, SecModule sc, String refer) throws Exception {

        boolean cek = false;
        String check = "";
        String app = "";
        String post = "";
        String status = "<span class=\"label label-primary\">Preparing</span>";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select " + sc.getCheck() + "," + sc.getApprove() + "," + sc.getPost() + " from " + sc.getMaintable() + " where " + sc.getReferidMaster() + " = ?");
        Logger.getLogger(UnapproveDAO.class.getName()).log(Level.INFO, "-------" + "select " + sc.getCheck() + "," + sc.getApprove() + "," + sc.getPost() + " from " + sc.getMaintable() + " where " + sc.getReferidMaster() + " = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            check = rs.getString(1);
            app = rs.getString(2);
            post = rs.getString(3);
        }

        stmt.close();
        rs.close();

        if (!check.equals("")) {
            status = "Checked";
        }

        if (!app.equals("")) {
            status = "Approved";
        }

//        if(post.equals("posted")){
//            status = "<span class=\"label label-info\">Post</span>";
//        }
        if (post.equals("Cancel")) {
            status = "Posted";
        }

        return status;
    }

    public static String unapproveNow(LoginProfile log, String refer) throws Exception {
        String i = "0";
        
        int inc = 0;

        String deleteQuery_2 = "delete from gl_posting_distribute where NoVoucher = ?";
        try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
            ps_2.setString(1, refer);
           ps_2.executeUpdate();
            inc++;
        }
        
        try{
            
            SecModule sc = GeneralTerm.getType(log, refer.substring(0,3));
            
            String q = ("UPDATE "+ sc.getMaintable() +" SET "+ sc.getApprove() +" = '', "+ sc.getCheck() +" = '', "+ sc.getPost() +" = 'No' WHERE "+ sc.getReferidMaster() +" = ?");
            
            Logger.getLogger(UnapproveDAO.class.getName()).log(Level.INFO, "-------" + q);
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, refer);
            
            ps.executeUpdate();
            ps.close();
            
            inc++;
            
           
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        i = String.valueOf(inc);
        
        if (!hasDebitCreditNote(log, refer).equals("None") && i.equals("2")) {

            i = hasDebitCreditNote(log, refer);

        }

        return i;

    }

    public static String voucherSupported(LoginProfile log) throws Exception {

        String support = "";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from sec_module where maintable <> '' and referid_master <> '' and approve <> '' and 'check' <> '' and post <> ''");

        rs = stmt.executeQuery();
        int i = 0;
        while (rs.next()) {
            i++;
            if (i == 1) {
                support += rs.getString("abb");
            } else {
                support += ", " + rs.getString("abb");
            }

        }

        return support;
    }

    private static String hasDebitCreditNote(LoginProfile log, String refer) throws Exception {

        String has = "None";

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from gl_debitnote where refernoteno = '" + refer + "'");

        rs = stmt.executeQuery();
        while (rs.next()) {

            has = rs.getString("noteno");

        }

        ResultSet rs1 = null;
        PreparedStatement stmt1 = log.getCon().prepareStatement("select * from gl_creditnote where refernoteno = '" + refer + "'");

        rs1 = stmt1.executeQuery();
        while (rs1.next()) {

            has = rs1.getString("noteno");

        }

        rs.close();
        rs1.close();
        stmt.close();
        stmt1.close();

        return has;

    }

}
