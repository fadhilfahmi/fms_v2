/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.tx;

import com.lcsb.fms.dao.administration.om.UserDAO;
import com.lcsb.fms.dao.financial.cashbook.PaymentVoucherDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.model.financial.tx.TxPeriod;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.dao.DataDAO;
import com.lcsb.fms.util.dao.ModuleDAO;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author fadhilfahmi
 */
public class TaxMasterDAO {

    public static TxPeriod getTaxPeriod(LoginProfile log) throws SQLException, Exception {

        TxPeriod tx = new TxPeriod();

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from tx_return_period where current='Yes'");
        rs = stmt.executeQuery();
        if (rs.next()) {
            tx = getResult(rs);
        }

        return tx;

    }

    public static TxPeriod getPreviousTaxPeriod(LoginProfile log) throws Exception {
        TxPeriod cur = (TxPeriod) TaxMasterDAO.getTaxPeriod(log);
        TxPeriod pre = new TxPeriod();

        if (cur.getPeriod() == 1) {
            pre.setPeriod(12);
            pre.setYear(cur.getYear() - 1);
        } else {
            pre.setPeriod(cur.getPeriod() - 1);
            pre.setYear(cur.getYear());
        }

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from tx_return_period where year='" + pre.getYear() + "' and period = '" + pre.getPeriod() + "'");
        rs = stmt.executeQuery();
        if (rs.next()) {
            pre = getResult(rs);
        }

        return pre;
    }

    public static TxPeriod getNextTaxPeriod(LoginProfile log) throws Exception {
        TxPeriod cur = (TxPeriod) TaxMasterDAO.getTaxPeriod(log);
        TxPeriod nxt = new TxPeriod();

        if (cur.getPeriod() == 12) {
            nxt.setPeriod(1);
            nxt.setYear(cur.getYear() + 1);
        } else {
            nxt.setPeriod(cur.getPeriod() + 1);
            nxt.setYear(cur.getYear());
        }

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from tx_return_period where year='" + nxt.getYear() + "' and period = '" + nxt.getPeriod() + "'");
        rs = stmt.executeQuery();
        if (rs.next()) {
            nxt = getResult(rs);
        }

        return nxt;
    }

    private static TxPeriod getResult(ResultSet rs) throws SQLException {

        TxPeriod tx = new TxPeriod();

        tx.setClosedate(rs.getDate("closedate"));
        tx.setCurrent(rs.getString("current"));
        tx.setEndyear(rs.getDate("endyear"));
        tx.setPeriod(rs.getInt("period"));
        tx.setStartyear(rs.getDate("startyear"));
        tx.setStatus(rs.getString("status"));
        tx.setTaxdue(rs.getDate("taxdue"));
        tx.setTaxend(rs.getDate("taxend"));
        tx.setTaxreturnperiod(rs.getString("taxreturnperiod"));
        tx.setTaxstart(rs.getDate("taxstart"));
        tx.setYear(rs.getInt("year"));

        return tx;

    }

    public static int confirmClose(LoginProfile log) throws SQLException, Exception {

        Connection con = log.getCon();
        int success = 1;
        TxPeriod cur = (TxPeriod) TaxMasterDAO.getTaxPeriod(log);
        TxPeriod pre = (TxPeriod) TaxMasterDAO.getPreviousTaxPeriod(log);
        String nextYear = String.valueOf(cur.getYear() + 1);
        String theYear = "";

        int nextPeriod = 0;

        nextPeriod = cur.getPeriod() + 1;
        theYear = String.valueOf(cur.getYear());

        try {
            String q1 = ("UPDATE tx_return_period SET current='No'");
            String q2 = ("UPDATE tx_return_period SET closedate='" + AccountingPeriod.getCurrentTimeStamp() + "' where concat(year,period)=concat('" + cur.getYear() + "','" + cur.getPeriod() + "')");
            String q3 = ("UPDATE tx_return_period SET current='Yes' where concat(year,period)=concat('" + theYear + "','" + nextPeriod + "')");
            PreparedStatement ps1 = con.prepareStatement(q1);
            PreparedStatement ps2 = con.prepareStatement(q2);
            PreparedStatement ps3 = con.prepareStatement(q3);

            ps1.executeUpdate();
            ps2.executeUpdate();
            ps3.executeUpdate();

            ps1.close();
            ps2.close();
            ps3.close();
        } catch (SQLException e) {
            e.printStackTrace();
            success = 0;
        }

        return success;

    }

    public static JSONArray confirmUndo(LoginProfile log) throws Exception {

        JSONArray array = new JSONArray();

        try {

            JSONObject jo = new JSONObject();
            
            jo.put("deleteGst03", TaxMasterDAO.deleteTxGST03(log));
            jo.put("deleteReconcile", TaxMasterDAO.deleteTxReconcile(log));
            jo.put("updateInput", updateInputTransaction(log, "tx_input"));
            jo.put("updateOutput", updateInputTransaction(log, "tx_output"));
            jo.put("deleteOutput", TaxMasterDAO.deleteTxOutput(log));
            jo.put("deleteInput", TaxMasterDAO.deleteTxInput(log));
            array.add(jo);
            jo = null;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return array;

    }

    public static int confirmUndoOld(LoginProfile log) throws SQLException, Exception {

        int i = 0;

        //begin process undo
        i += TaxMasterDAO.deleteTxGST03(log);
        i += TaxMasterDAO.deleteTxReconcile(log);

        i += updateInputTransaction(log, "tx_input");
        i += updateInputTransaction(log, "tx_output");

        i += TaxMasterDAO.deleteTxOutput(log);
        i += TaxMasterDAO.deleteTxInput(log);

        return i;

    }

    private static int deleteTxGST03(LoginProfile log) throws SQLException, Exception {

        int i = 0;

        try {
            String referno = getCurrentRefer(log, "tx_gst03");
            DataDAO.deleteData(log, referno, "tx_gst03", "refer");
            DataDAO.deleteData(log, referno, "tx_gst03_refer", "refer");
            i = 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i;

    }

    private static int deleteTxReconcile(LoginProfile log) throws SQLException, Exception {

        int i = 0;

        try {
            String referno = getCurrentRefer(log, "tx_reconcile_master");
            DataDAO.deleteData(log, referno, "tx_reconcile_master", "refno");
            DataDAO.deleteData(log, referno, "tx_reconcile", "refer");
            i = 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return i;

    }

    private static int deleteTxOutput(LoginProfile log) throws SQLException, Exception {

        int i = 0;

        try {
            String referno = getCurrentRefer(log, "tx_output");
            DataDAO.deleteData(log, referno, "tx_output", "refno");
            DataDAO.deleteData(log, referno, "tx_output_detail", "refer");
            i = 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return i;

    }

    private static int deleteTxInput(LoginProfile log) throws SQLException, Exception {

        int i = 0;

//        String refer = "";
//        ResultSet rs = null;
//        PreparedStatement stmt = log.getCon().prepareStatement("select * from tx_input where year = ? and period = ?");
//        stmt.setString(1, AccountingPeriod.getCurYear(log));
//        stmt.setString(2, AccountingPeriod.getCurPeriod(log));
//        rs = stmt.executeQuery();
//        if (rs.next()) {
//            refer = rs.getString("refer");
//        }
        try {
            String referno = getCurrentRefer(log, "tx_input");
            DataDAO.deleteData(log, referno, "tx_input", "refno");
            DataDAO.deleteData(log, referno, "tx_input_detail", "refer");
            i = 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return i;

    }

    private static int updateInputTransaction(LoginProfile log, String table) throws SQLException, Exception {

        int i = 0;

        String refer = getCurrentRefer(log, table);

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select distinct(voucherno) from " + table + "_detail where refer = ?");
        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        while (rs.next()) {

            String voucherno = rs.getString(1);
            //String abb = voucherno.substring(0, 3);
            Module mod = (Module) ModuleDAO.getModuleFromReferenceNo(log, voucherno);
            updateIO(log, mod, voucherno);

            i = 1;

        }

        return i;

    }

    public static void updateIO(LoginProfile log, Module mod, String refer) throws Exception {

        String query = "UPDATE " + mod.getMainTable() + " SET postgst = 'No' WHERE " + mod.getReferID_Master() + " = '" + refer + "' AND postgst = 'reconcile'";

        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }

    private static String getCurrentRefer(LoginProfile log, String table) throws SQLException, Exception {

        String refer = "";
        String colRefer = "refno";

        if (table.equals("tx_gst03")) {

            colRefer = "refer";
        }

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from " + table + " where year = ? and period = ?");
        stmt.setInt(1, TaxMasterDAO.getTaxPeriod(log).getYear());
        stmt.setInt(2, TaxMasterDAO.getTaxPeriod(log).getPeriod());
        //Logger.getLogger(PaymentVoucherDAO.class.getName()).log(Level.INFO, String.valueOf(stmt));
        rs = stmt.executeQuery();
        if (rs.next()) {
            refer = rs.getString(colRefer);
        }

        return refer;

    }

}
