/* 
 * To change this license header, choose License Headers in Project Properties. 
 * To change this template file, choose Tools | Templates 
 * and open the template in the editor. 
 */ 
package com.lcsb.fms.dao.financial.ar; 
 
import com.lcsb.fms.model.financial.ar.ArCPORefinery; 
import com.lcsb.fms.model.financial.ar.SalesInvoiceItem; 
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable; 
import com.lcsb.fms.util.model.LoginProfile; 
import com.lcsb.fms.util.model.Module; 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet; 
import java.sql.SQLException; 
import java.sql.Statement; 
import java.util.ArrayList; 
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger;
 
/** 
 * 
 * @author fadhilfahmi 
 */ 
public class ArDispatchDAO { 
     
    public static Module getModule() { 
 
        Module mod = new Module(); 
        mod.setModuleID("020904"); 
        mod.setModuleDesc("Dispatch Note"); 
        mod.setMainTable("qc_cpo_grade"); 
        mod.setReferID_Master("id"); 
        return mod; 
 
    } 
 
    public static List<ListTable> tableList() { 
 
        String column_names[] = {"id", "date", "contract","dispatch_no", "trailer"}; 
        String title_name[] = {"#", "Date", "Contract","Dispatch No", "Trailer"}; 
 
        List<ListTable> lt = new ArrayList(); 
 
        for (int j = 0; j < column_names.length; j++) { 
            ListTable i = new ListTable(); 
            i.setColumnName(column_names[j]); 
            i.setTitleName(title_name[j]); 
            lt.add(i); 
        } 
 
        return lt; 
    } 
     
    public static List<ArCPORefinery> getAllRefinery(LoginProfile log, String type, String location) throws Exception { 
 
        Statement stmt = null; 
        List<ArCPORefinery> CVi; 
        CVi = new ArrayList(); 
         
        String table = ""; 
         
        switch (type) { 
            case "cpo": 
                table = "qc_cpo_grade"; 
                break; 
            case "kernel": 
                table = "qc_kernel_grade"; 
                break; 
            case "shell": 
                table = "qc_shell_dispatch"; 
                break; 
            case "sludge": 
                table = "qc_sludge_dispatch"; 
                break; 
            default: 
                break; 
        } 
 
        try {
            Connection con;
            
            if(!location.equals("local")){
                con = ConnectionUtil.getMillConnection("2411");
            }else{
                con = log.getCon();
            }
            stmt = con.createStatement(); 
            ResultSet rs = stmt.executeQuery("select * from "+table+" where date REGEXP '2016|2017'"); 
 
            while (rs.next()) { 
                CVi.add(SalesInvoiceDAO.getListCPO(rs)); 
            } 
        } catch (SQLException e) { 
            e.printStackTrace(); 
        } 
 
        return CVi; 
    }
    
    public static ArCPORefinery getRefinery(LoginProfile log, String type, String location, String disno) throws Exception { 
 
        Statement stmt = null; 
        ArCPORefinery i = new ArCPORefinery(); 
         
        
 
        try {
            Connection con;
            
            if(!location.equals("local")){
                con = ConnectionUtil.getMillConnection("2411");
            }else{
                con = log.getCon();
            }
            stmt = con.createStatement(); 
            ResultSet rs = stmt.executeQuery("select * from "+getTableByType(type)+" where dispatch_no = '"+disno+"'"); 
 
            while (rs.next()) { 
                i = SalesInvoiceDAO.getListCPO(rs); 
            } 
        } catch (SQLException e) { 
            e.printStackTrace(); 
        } 
 
        return i; 
    } 
     
     
    public static void updateRefinery(LoginProfile log, ArCPORefinery item, String olddisno, String type) throws Exception {

        try {
            String q = ("UPDATE "+getTableByType(type)+" set dispatch_no = ?,contract = ?,driver = ?,mt = ?,trailer = ?,mt_refinery = ? WHERE dispatch_no = '" + olddisno + "'");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getDispatchNo());
            ps.setString(2, item.getContract());
            ps.setString(3, item.getDriver());
            ps.setString(4, item.getMt());
            ps.setString(5, item.getTrailer());
            ps.setDouble(6, item.getMtRefinery());

            Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    } 
    
    private static String getTableByType(String type){
        
         String table = ""; 
         
        switch (type) { 
            case "cpo": 
                table = "qc_cpo_grade"; 
                break; 
            case "kernel": 
                table = "qc_kernel_grade"; 
                break; 
            case "shell": 
                table = "qc_shell_dispatch"; 
                break; 
            case "sludge": 
                table = "qc_sludge_dispatch"; 
                break; 
            default: 
                break; 
        }
        
        return table;
        
    }
    
    public static boolean checkExistRefinery(LoginProfile log, String type, String disno) throws Exception { 
        
        boolean c = false;
        Statement stmt = null; 
        ArCPORefinery i = new ArCPORefinery(); 
         
        try {
            stmt = log.getCon().createStatement(); 
            ResultSet rs = stmt.executeQuery("select * from "+getTableByType(type)+" where dispatch_no = '"+disno+"'"); 
 
            if (rs.next()) { 
                c = true;
            } 
        } catch (SQLException e) { 
            e.printStackTrace(); 
        } 
 
        return c; 
    } 
     
} 