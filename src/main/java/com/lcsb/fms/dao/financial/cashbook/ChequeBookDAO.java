/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.fms.dao.financial.cashbook;

import com.lcsb.fms.model.financial.cashbook.CbCekbook;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class ChequeBookDAO {
    
    public static String module_id(){
         return "020201";
         
    }
    
    public static void saveData(LoginProfile log, CbCekbook item) throws Exception{
        //String no, String bankcode, String bankname, Date tarikh, String startcek, int nocek, String endcek, String active)
        String q = ("insert into cb_cekbook(no,bankcode,bankname,tarikh,startcek,nocek,endcek,active) values (?,?,?,?,?,?,?,?)");
        String lastNo = ChequeBookDAO.getLastNo(log);
        PreparedStatement ps = log.getCon().prepareStatement(q);
        ps.setString(1, lastNo);
        ps.setString(2, item.getBankcode());
        ps.setString(3, item.getBankname());
        ps.setString(4, item.getTarikh());
        ps.setString(5, item.getStartcek());
        ps.setInt(6, item.getNocek());
        ps.setString(7, item.getEndcek());
        ps.setString(8, item.getActive());
        
        ps.executeUpdate();
        
        
        ChequeBookDAO.generateCheque(log, item.getStartcek(), item.getNocek(),lastNo, item.getBankcode());
        
        
    }
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
         
        String deleteQuery_2 = "delete from cb_cekbook where no = ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, no);
        ps_2.executeUpdate();
        ps_2.close();
        
        
    }
    
    public static void updateItem(LoginProfile log, CbCekbook item) throws Exception{
        
        try{
            String q = ("UPDATE cb_cekbook set bankcode = ?,bankname = ?,tarikh = ?,startcek = ?,nocek = ?,endcek = ?,active = ? WHERE no = '"+item.getNo()+"'");
            Logger.getLogger(ChequeBookDAO.class.getName()).log(Level.INFO, q);
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getBankcode());
            ps.setString(2, item.getBankname());
            ps.setString(3, item.getTarikh());
            ps.setString(4, item.getStartcek());
            ps.setInt(5, item.getNocek());
            ps.setString(6, item.getEndcek());
            ps.setString(7, item.getActive());
           
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    private static String getLastNo(LoginProfile log) throws SQLException, Exception{
        
        ResultSet rs = null;
        String no = "";
        
        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(lpad(max(no)+1,4,'0'),'0001') as no from cb_cekbook");
        
        rs = stmt.executeQuery();
        if (rs.next()) {
            no = rs.getString("no"); 
        }
        
        rs.close();
        stmt.close();
        return no;
        
    }
    
    public static CbCekbook getInfo(LoginProfile log, String no) throws SQLException, Exception {
        CbCekbook c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_cekbook where no=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static CbCekbook getInfo(ResultSet rs) throws SQLException {
        CbCekbook g = new CbCekbook();
        g.setActive(rs.getString("active"));
        g.setBankcode(rs.getString("bankcode"));
        g.setBankname(rs.getString("bankname"));
        g.setEndcek(rs.getString("endcek"));
        g.setNo(rs.getString("no"));
        g.setNocek(Integer.parseInt(rs.getString("nocek")));
        g.setStartcek(rs.getString("startcek"));
        g.setTarikh(rs.getString("tarikh"));
        return g;
    }
    
    private static void generateCheque(LoginProfile log, String startcek, int nocek, String no, String bankcode) throws SQLException, Exception{
        
        
        int cuba;
        int h=Integer.parseInt(startcek);
        int hlength = startcek.length();
        int r=nocek;
        int cubalength;
        int p;
        String chqno = "";
        for ( p=0;p<r;p++)
        {
         cuba = h+p;
         cubalength = String.valueOf(cuba).length();  
         chqno = "";
         for (int i = 0; i < hlength - cubalength; i++) {
                chqno = chqno + "0";
         }
         chqno = chqno + String.valueOf(cuba);

        
        
        String q = ("insert into cb_cekbook_cek(bankcode,nocekbook,nocek) values (?,?,?)");
        PreparedStatement ps = log.getCon().prepareStatement(q);
        ps.setString(1, bankcode);
        ps.setString(2, no);
        ps.setString(3, chqno);
        Logger.getLogger(ChequeBookDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
        ps.executeUpdate();
        
        ps.close();

        } 
        
    } 
    
}
