/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.dao.financial.gl;

import com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO;
import static com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO.getORMaster;
import com.lcsb.fms.dao.financial.tx.TaxGst03DAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.general.DataTable;
import com.lcsb.fms.model.financial.cashbook.ORReceiptMaster;
import com.lcsb.fms.model.financial.cashbook.OfficialCreditItem;
import com.lcsb.fms.model.financial.gl.JournalVoucher;
import com.lcsb.fms.model.financial.gl.JournalVoucherItem;
import com.lcsb.fms.model.financial.gl.JournalVoucherMaster;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ConnectionModel;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class JournalVoucherDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("020104");
        mod.setModuleDesc("Journal Voucher");
        mod.setMainTable("gl_jv");
        mod.setReferID_Master("JVrefno");
        return mod;

    }

    public static List<ListTable> tableList() {

        String column_names[] = {"JVrefno", "JVdate", "todebit", "tocredit", "checkbyid", "approvebyid", "postflag"};
        String title_name[] = {"Refer", "Date", "Debit", "Credit", "Check Id", "App id", "Post"};
        boolean boolview[] = {true, true, true, true, false, false, false};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            i.setBoolView(boolview[j]);
            lt.add(i);
        }

        return lt;
    }

    public static void deleteExistJournal(LoginProfile log, String JVno) throws SQLException, Exception {

        Connection con = log.getCon();

        String deleteQuery_1 = "delete from gl_jv where JVrefno = ?";
        String deleteQuery_2 = "delete from gl_jv_item where JVrefno = ?";
        PreparedStatement ps_1 = con.prepareStatement(deleteQuery_1);
        PreparedStatement ps_2 = con.prepareStatement(deleteQuery_2);
        ps_1.setString(1, JVno);
        ps_2.setString(1, JVno);
        ps_1.executeUpdate();
        ps_2.executeUpdate();
        ps_1.close();
        ps_2.close();

        DebitNoteDAO.updateEstDN(log, JVno);

        if (JVno.substring(0, 3).equals("JTX")) {
            TaxGst03DAO.updateTxGst03(log, JVno);
        }

        LiveGLDAO.deleteFromGL(log, JVno);

    }

    public static void deleteJournalItem(LoginProfile log, String Jvid, String JVrefno) throws SQLException, Exception {

        String deleteQuery_2 = "delete from gl_jv_item where Jvid = ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, Jvid);
        ps_2.executeUpdate();
        ps_2.close();

        updateAmount(log, JVrefno);
        LiveGLDAO.reloadGL(log, JVrefno, "JV");

    }

    public static String saveJournalMain(LoginProfile log, JournalVoucher item) throws Exception {

        if (item.getJVtype() == null) {
            Logger.getLogger(JournalVoucherDAO.class.getName()).log(Level.INFO, "--------************---no-----" + String.valueOf(item.getJVtype()));
        } else {
            Logger.getLogger(JournalVoucherDAO.class.getName()).log(Level.INFO, "--------************---yes-----" + String.valueOf(item.getJVtype()));
        }

        Connection con = log.getCon();

        String JVrefno = "";

        try {

            if (item.getJVtype() == null) {
                JVrefno = AutoGenerate.getReferenceNox(log, "JVrefno", "gl_jv", "JVN", item.getEstatecode(), item.getYear(), item.getCurperiod());
            } else {
                JVrefno = AutoGenerate.getReferenceNox(log, "JVrefno", "gl_jv", item.getJVtype(), item.getEstatecode(), item.getYear(), item.getCurperiod());
            }

            String q = ("insert into gl_jv(JVrefno,JVdate,JVno,reason,preparedbyid,preparedbyname,preparedate,year,curperiod,estatecode,estatename,todebit,tocredit) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, JVrefno);
            ps.setString(2, item.getJVdate());
            ps.setString(3, AutoGenerate.getVoucherNo(log, "gl_jv", "JVno"));
            ps.setString(4, item.getReason());
            ps.setString(5, item.getPreparedbyid());
            ps.setString(6, item.getPreparedbyname());
            ps.setString(7, item.getPreparedate());
            ps.setString(8, item.getYear());
            ps.setString(9, item.getCurperiod());
            ps.setString(10, item.getEstatecode());
            ps.setString(11, item.getEstatename());
            ps.setString(12, item.getTodebit());
            ps.setString(13, item.getTocredit());
            ps.executeUpdate();

            ps.close();

            LiveGLDAO.reloadGL(log, JVrefno, "JV");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return JVrefno;

    }

    public static void saveJournalItem(LoginProfile log, JournalVoucherItem item) throws Exception {

        try {
            String q = ("insert into gl_jv_item(Jvid,JVrefno,remark,actcode,actdesc,loccode,locdesc,loclevel,satype,sacode,sadesc,debit,credit,taxcode,taxdescp,taxrate,gstid,amtbeforetax) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, AutoGenerate.getReferAddBack(log, "gl_jv_item", "Jvid", item.getJVrefno(), "JVrefno"));
            ps.setString(2, item.getJVrefno());
            ps.setString(3, item.getRemark());
            ps.setString(4, item.getActcode());
            ps.setString(5, item.getActdesc());
            ps.setString(6, item.getLoccode());
            ps.setString(7, item.getLocdesc());
            ps.setString(8, item.getLoclevel());
            ps.setString(9, item.getSatype());
            ps.setString(10, item.getSacode());
            ps.setString(11, item.getSadesc());
            ps.setDouble(12, item.getDebit());
            ps.setDouble(13, item.getCredit());
            ps.setString(14, item.getTaxcode());
            ps.setString(15, item.getTaxdescp());
            ps.setString(16, item.getTaxrate());
            ps.setString(17, item.getGstid());
            ps.setDouble(18, item.getAmtbeforetax());

            ps.executeUpdate();
            Logger.getLogger(JournalVoucherDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            updateAmount(log, item.getJVrefno());
            ps.close();

            LiveGLDAO.reloadGL(log, item.getJVrefno(), "JV");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateJournalItem(LoginProfile log, JournalVoucherItem item, String Jvid) throws Exception {

        try {
            //String q = ("insert into gl_jv_item(Jvid,JVrefno,remark,actcode,actdesc,loccode,locdesc,loclevel,satype,sacode,sadesc,debit,credit,taxcode,taxdescp,taxrate,gstid,amtbeforetax) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            String q = ("UPDATE gl_jv_item set remark = ?,actcode = ?,actdesc = ?,loccode = ?,locdesc = ?,loclevel = ?,satype = ?,sacode = ?,sadesc = ?,debit = ?, credit = ?,taxcode = ?,taxdescp = ?,taxrate = ?,gstid = ?,amtbeforetax = ? WHERE Jvid = '" + Jvid + "'");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getRemark());
            ps.setString(2, item.getActcode());
            ps.setString(3, item.getActdesc());
            ps.setString(4, item.getLoccode());
            ps.setString(5, item.getLocdesc());
            ps.setString(6, item.getLoclevel());
            ps.setString(7, item.getSatype());
            ps.setString(8, item.getSacode());
            ps.setString(9, item.getSadesc());
            ps.setDouble(10, item.getDebit());
            ps.setDouble(11, item.getCredit());
            ps.setString(12, item.getTaxcode());
            ps.setString(13, item.getTaxdescp());
            ps.setString(14, item.getTaxrate());
            ps.setString(15, item.getGstid());
            ps.setDouble(16, item.getAmtbeforetax());

            ps.executeUpdate();
            Logger.getLogger(JournalVoucherDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            updateAmount(log, item.getJVrefno());
            ps.close();

            LiveGLDAO.reloadGL(log, item.getJVrefno(), "JV");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static String getJVno(LoginProfile log, String abb) throws Exception {

        ResultSet rs = null;
        String JVno = "";
        String year = AccountingPeriod.getCurYear(log);
        String curperiod = AccountingPeriod.getCurPeriod(log);
        //String estatecode = LoginProfile.getEstateCode();
        String estatecode = "9911";

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat(?,'" + estatecode + "',substring('" + year + "',3,2),lpad('" + curperiod + "',2,'0'),lpad((max(substring(JVrefno,14,4))+1),4,'0')),concat('JPY','" + estatecode + "',substring('" + year + "',3,2),lpad('" + curperiod + "',2,'0'),'0001')) as refer from gl_jv where JVrefno like concat(?,'%') and year='" + year + "' and curperiod='" + curperiod + "' and estatecode like concat('" + estatecode + "','%')");
        stmt.setString(1, abb);
        stmt.setString(2, abb);

        rs = stmt.executeQuery();
        if (rs.next()) {
            JVno = rs.getString(1);

        }

        rs.close();
        stmt.close();
        return JVno;

    }

    public static String getJVoucherNo(LoginProfile log) throws Exception {

        ResultSet rs = null;
        String JVno = "";

        PreparedStatement stmt = log.getCon().prepareStatement("SELECT ifnull(concat(lpad((max(JVno)+1),5,'0')),concat('0001')) as new FROM `gl_jv`");

        rs = stmt.executeQuery();
        if (rs.next()) {
            JVno = rs.getString(1);
        }

        rs.close();
        stmt.close();
        return JVno;

    }

    public static String getJVid(LoginProfile log, String JVrefno) throws Exception, SQLException {

        ResultSet rs = null;
        String JVno = "";

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat('" + JVrefno + "',lpad((max(right(jvid,4))+1),4,'0')),concat('" + JVrefno + "','0001')) as njvno from gl_jv_item where jvrefno='" + JVrefno + "'");

        rs = stmt.executeQuery();
        if (rs.next()) {
            JVno = rs.getString(1);
        }

        rs.close();
        stmt.close();
        return JVno;

    }

    public static Double getSum(LoginProfile log, String column, String JVno) throws Exception {
        Double x = 0.00;
        Statement stment = log.getCon().createStatement();
        ResultSet setent = stment.executeQuery(" select sum(" + column + ") as sumamount from gl_jv_item where JVrefno = '" + JVno + "'");

        while (setent.next()) {
            x += setent.getDouble("sumamount");
        }

        stment.close();
        setent.close();
        return x;
    }

    public static JournalVoucherItem getJVBal(LoginProfile log, String JVno) throws Exception {

        JournalVoucherItem jv = new JournalVoucherItem();

        Double debit = getSum(log, "debit", JVno);
        Double credit = getSum(log, "credit", JVno);
        jv.setJVrefno(JVno);
        Logger.getLogger(JournalVoucherDAO.class.getName()).log(Level.INFO, String.valueOf(debit) + "**" + String.valueOf(credit));
        if (debit.equals(credit)) {
            Logger.getLogger(JournalVoucherDAO.class.getName()).log(Level.INFO, String.valueOf(debit) + "--" + String.valueOf(credit));
            jv.setDebit(0.00);
            jv.setCredit(0.00);
        } else if (debit > credit) {
            Logger.getLogger(JournalVoucherDAO.class.getName()).log(Level.INFO, String.valueOf(debit) + "@@" + String.valueOf(credit));
            jv.setCredit(debit - credit);
            jv.setDebit(0.00);
        } else if (debit < credit) {
            Logger.getLogger(JournalVoucherDAO.class.getName()).log(Level.INFO, String.valueOf(debit) + "@@" + String.valueOf(credit));
            jv.setCredit(0.00);
            jv.setDebit(credit - debit);
        }

        if (debit > credit) {
            jv.setBalance(debit - credit);
        } else if (debit < credit) {
            jv.setBalance(credit - debit);
        }

        return jv;
    }

    private static void updateAmount(LoginProfile log, String JVrefno) throws Exception {

        String query = "UPDATE gl_jv SET todebit = '" + getSum(log, "debit", JVrefno) + "',tocredit = '" + getSum(log, "credit", JVrefno) + "' where JVrefno = '" + JVrefno + "'";

        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }

    public static List<JournalVoucherItem> getAllJVItem(LoginProfile log, String JVrefno) throws Exception {

        Statement stmt = null;
        List<JournalVoucherItem> JVi;
        JVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from gl_jv_item where JVrefno = '" + JVrefno + "' order by debit asc");

            while (rs.next()) {
                JVi.add(getJVitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return JVi;
    }

    public static List<JournalVoucherItem> getAllJVItemByPage(LoginProfile log, String JVrefno, int row, int limit) throws Exception {

        Statement stmt = null;
        List<JournalVoucherItem> JVi;
        JVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from gl_jv_item where JVrefno = '" + JVrefno + "' order by Jvid asc limit " + row + "," + limit);

            while (rs.next()) {
                JVi.add(getJVitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return JVi;
    }

    public static JournalVoucherItem getJVitem(LoginProfile log, String Jvid) throws SQLException, Exception {
        JournalVoucherItem c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from gl_jv_item where Jvid=?");
        stmt.setString(1, Jvid);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getJVitem(rs);
        }

        return c;
    }

    private static JournalVoucherItem getJVitem(ResultSet rs) throws SQLException {
        JournalVoucherItem jv = new JournalVoucherItem();
        jv.setActcode(rs.getString("actcode"));
        jv.setActdesc(rs.getString("actdesc"));
        jv.setAmtbeforetax(rs.getDouble("amtbeforetax"));
        jv.setCredit(rs.getDouble("credit"));
        jv.setDebit(rs.getDouble("debit"));
        jv.setGstid(rs.getString("gstid"));
        jv.setJVrefno(rs.getString("JVrefno"));
        jv.setJvid(rs.getString("Jvid"));
        jv.setLoccode(rs.getString("loccode"));
        jv.setLocdesc(rs.getString("locdesc"));
        jv.setLoclevel(rs.getString("loclevel"));
        jv.setRemark(rs.getString("remark"));
        jv.setSacode(rs.getString("sacode"));
        jv.setSadesc(rs.getString("sadesc"));
        jv.setSatype(rs.getString("satype"));
        jv.setTaxcode(rs.getString("taxcode"));
        jv.setTaxdescp(rs.getString("taxdescp"));
        jv.setTaxrate(rs.getString("taxrate"));
        return jv;
    }

    public static List<JournalVoucher> getAllJV(LoginProfile log, String JVrefno) throws Exception {

        Statement stmt = null;
        List<JournalVoucher> JV;
        JV = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from gl_jv where JVrefno = '" + JVrefno + "' order by JVrefno");

            while (rs.next()) {
                JV.add(getJV(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return JV;
    }

    public static JournalVoucher getJV(LoginProfile log, String JVrefno) throws SQLException, Exception {
        JournalVoucher c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from gl_jv where JVrefno=?");
        stmt.setString(1, JVrefno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getJV(rs);
        }

        return c;
    }

    private static JournalVoucher getJV(ResultSet rs) throws SQLException {
        JournalVoucher jv = new JournalVoucher();
        jv.setJVrefno(rs.getString("JVrefno"));
        jv.setCurperiod(rs.getString("curperiod"));
        jv.setEstatecode(rs.getString("estatecode"));
        jv.setEstatename(rs.getString("estatename"));
        jv.setJVdate(rs.getString("jvdate"));
        jv.setJVno(rs.getString("JVno"));
        jv.setPreparedate(rs.getString("preparedate"));
        jv.setPreparedbyid(rs.getString("preparedbyid"));
        jv.setPreparedbyname(rs.getString("preparedbyname"));
        jv.setReason(rs.getString("reason"));
        jv.setReflexcb(rs.getString("reflexcb"));
        jv.setTocredit(rs.getString("tocredit"));
        jv.setTodebit(rs.getString("todebit"));
        jv.setYear(rs.getString("year"));
        jv.setCheckbyid(rs.getString("checkbyid"));
        jv.setCheckbyname(rs.getString("checkbyname"));
        jv.setCheckdate(rs.getString("checkdate"));
        jv.setApprovebyid(rs.getString("approvebyid"));
        jv.setApprovebydesig(rs.getString("approvebydesig"));
        jv.setApprovebyname(rs.getString("approvebyname"));
        jv.setApprovedate(rs.getString("approvedate"));
        return jv;
    }

    public static void checkVoucher(LoginProfile log, String refer, String staff_id, String staff_name) throws Exception {

        String query = "UPDATE gl_jv SET checkbyid = '" + staff_id + "',checkbyname = '" + staff_name + "',checkdate='" + AccountingPeriod.getCurrentTimeStamp() + "' where JVrefno = '" + refer + "'";

        try {
            PreparedStatement ps = log.getCon().prepareStatement(query);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void approveVoucher(LoginProfile log, String refer, String staff_id, String staff_name, String position) throws Exception {

        String query = "UPDATE gl_jv SET approvebyid = '" + staff_id + "',approvebyname = '" + staff_name + "',approvedate='" + AccountingPeriod.getCurrentTimeStamp() + "', approvebydesig = '" + position + "' where JVrefno = '" + refer + "'";

        try {
            PreparedStatement ps = log.getCon().prepareStatement(query);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static String getStatus(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String check = "";
        String app = "";
        String post = "";
        String status = "<span class=\"label label-primary\">Preparing</span>";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select checkbyid,approvebyid,postflag from gl_jv where JVrefno = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            check = rs.getString(1);
            app = rs.getString(2);
            post = rs.getString(3);
        }

        stmt.close();
        rs.close();

        if (!check.equals("")) {
            status = "Checked";
        }

        if (!app.equals("")) {
            status = "Approved";
        }

//        if(post.equals("posted")){
//            status = "<span class=\"label label-info\">Post</span>";
//        }
        if (post.equals("Cancel")) {
            status = "Posted";
        }

        return status;
    }

    public static boolean isCheck(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select checkbyid from gl_jv where JVrefno = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString(1);
        }

        if (!ck.equals("")) {
            cek = true;
        }

        stmt.close();
        rs.close();

        return cek;
    }

    public static boolean isApprove(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select approvebyid from gl_jv where JVrefno = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString(1);
        }

        if (!ck.equals("")) {
            cek = true;
        }

        stmt.close();
        rs.close();

        return cek;
    }

    public static void updateMain(LoginProfile log, JournalVoucher m) throws Exception {

        try {

            String q = ("UPDATE gl_jv set curperiod = ?,JVdate = ?,reason = ?,reflexcb = ?,year = ? WHERE JVrefno = '" + m.getJVrefno() + "'");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, m.getCurperiod());
            ps.setString(2, m.getJVdate());
            ps.setString(3, m.getReason());
            ps.setString(4, m.getReflexcb());
            ps.setString(5, m.getYear());
            ps.executeUpdate();

            //Logger.getLogger(OfficialReceiptDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

            LiveGLDAO.reloadGL(log, m.getJVrefno(), "JV");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static List<JournalVoucher> getAll(LoginProfile log, DataTable prm) throws Exception, SQLException {

        Statement stmt = null;
        List<JournalVoucher> CV;
        CV = new ArrayList();

        String q = "";
        if (prm.getViewBy().equals("today")) {
            q = " WHERE JVdate = '" + AccountingPeriod.getCurrentTimeStamp() + "'";
        } else if (prm.getViewBy().equals("thismonth")) {
            q = " WHERE curperiod = '" + AccountingPeriod.getCurPeriod(log) + "' and year = '" + AccountingPeriod.getCurYear(log) + "'";
        } else if (prm.getViewBy().equals("byperiod")) {
            q = " WHERE curperiod = '" + prm.getPeriod() + "' and year = '" + prm.getYear() + "'";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from gl_jv " + q + " order by JVrefno");

            while (rs.next()) {
                CV.add(getJV(rs));
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }
    
    public static String replicateData(LoginProfile log, String refno, String replicateDate, String replicateYear, String replicatePeriod) throws Exception {
        String refer = "";

        JournalVoucherMaster pm = (JournalVoucherMaster) getMaster(log, refno);
        pm.getMaster().setCurperiod(replicatePeriod);
        pm.getMaster().setYear(replicateYear);
        pm.getMaster().setPreparedbyid(log.getUserID());
        pm.getMaster().setPreparedbyname(log.getFullname());
        pm.getMaster().setPreparedate(AccountingPeriod.getCurrentTimeStamp());
        pm.getMaster().setJVdate(replicateDate);

        refer = JournalVoucherDAO.saveJournalMain(log, pm.getMaster());

        List<JournalVoucherItem> listx = (List<JournalVoucherItem>) pm.getListItem();
        for (JournalVoucherItem j : listx) {

            j.setJVrefno(refer);
            JournalVoucherDAO.saveJournalItem(log, j);

        }

        return refer;
    }
    
    public static JournalVoucherMaster getMaster(LoginProfile log, String refno) throws Exception {

        JournalVoucherMaster pm = new JournalVoucherMaster();

        pm.setMaster(getJV(log, refno));
        pm.setListItem(getAllJVItem(log, refno));

        return pm;

    }
    
    public static boolean hasDCNote(LoginProfile log, String refno) throws Exception {

        boolean s = false;

        JournalVoucherMaster pm = (JournalVoucherMaster) getMaster(log, refno);

        List<JournalVoucherItem> listx = (List<JournalVoucherItem>) pm.getListItem();
        for (JournalVoucherItem j : listx) {
            
            if(!s){
                s = DebitCreditNoteDAO.checkSuspence(log, j.getActcode());
            }

            

        }

        return s;
    }
    
    public static boolean isComplete(LoginProfile log, String refno) throws Exception {

        boolean s = true;
        JournalVoucherMaster pm = (JournalVoucherMaster) getMaster(log, refno);

        JournalVoucher p = (JournalVoucher) pm.getMaster();

        if (Double.parseDouble(p.getTocredit()) <= 0.0) {
            s = false;
        }else if (Double.parseDouble(p.getTodebit()) <= 0.0) {
            s = false;
        } /*else if (p.get().equals("") || p.getBcode() == null) {
            s = false;
        }*/

        return s;
    }

}
