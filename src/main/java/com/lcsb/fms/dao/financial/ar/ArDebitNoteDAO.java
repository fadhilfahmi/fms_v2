/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.ar;

import com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.LiveGLDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.ar.ArDebitNote;
import com.lcsb.fms.model.financial.ar.ArDebitNoteItem;
import com.lcsb.fms.model.financial.ar.ArDebitNoteMaster;
import com.lcsb.fms.util.dao.TaxDAO;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class ArDebitNoteDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("020908");
        mod.setModuleDesc("Sales Debit Note");
        mod.setMainTable("ar_debitnote");
        mod.setReferID_Master("noteno");
        return mod;

    }

    public static List<ListTable> tableList() {

        String column_names[] = {"noteno", "notedate", "buyername", "total", "checkid", "appid", "postflag"};
        String title_name[] = {"Note No.", "Date", "Company Name", "Amount(RM)", "Check ID", " Approve ID", "Post"};
        boolean boolview[] = {true, true, true, true, false, false, false};
        //int colWidth[] = {};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            i.setBoolView(boolview[j]);
            lt.add(i);
        }

        return lt;
    }

    public static String saveMain(LoginProfile log, ArDebitNote item) throws Exception {

        String newrefer = AutoGenerate.getReferenceNox(log, "noteno", "ar_debitnote", "DNR", item.getEstcode(), item.getYear(), item.getPeriod());

        try {
            String q = ("insert into ar_debitnote(noteno,buyercode,buyername,estcode,estname,pdate,period,pid,pname,remark,year,total,notedate) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, newrefer);
            ps.setString(2, item.getBuyercode());
            ps.setString(3, item.getBuyername());
            ps.setString(4, item.getEstcode());
            ps.setString(5, item.getEstname());
            ps.setString(6, item.getPdate());
            ps.setString(7, item.getPeriod());
            ps.setString(8, item.getPid());
            ps.setString(9, item.getPname());
            ps.setString(10, item.getRemark());
            ps.setString(11, item.getYear());
            ps.setDouble(12, item.getTotal());
            ps.setDate(13, (item.getNotedate() != null) ? new java.sql.Date(item.getNotedate().getTime()) : null);

            Logger.getLogger(ArDebitNoteDAO.class.getName()).log(Level.INFO, "-------{0}", String.valueOf(ps));
            ps.executeUpdate();

            ps.close();

            LiveGLDAO.reloadGL(log, newrefer, "DNR");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newrefer;

    }

    public static ArDebitNote getMain(LoginProfile log, String refno) throws SQLException, Exception {
        ArDebitNote c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from ar_debitnote where noteno=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getMain(rs);
        }

        return c;
    }

    public static ArDebitNote getMain(ResultSet rs) throws SQLException {

        ArDebitNote i = new ArDebitNote();

        i.setBuyercode(rs.getString("buyercode"));
        i.setBuyername(rs.getString("buyername"));
        i.setEstcode(rs.getString("estcode"));
        i.setEstname(rs.getString("estname"));
        i.setNotedate(rs.getDate("notedate"));
        i.setNoteno(rs.getString("noteno"));
        i.setPdate(rs.getString("pdate"));
        i.setPeriod(rs.getString("period"));
        i.setPid(rs.getString("pid"));
        i.setPname(rs.getString("pname"));
        i.setPostflag(rs.getString("postflag"));
        i.setRemark(rs.getString("remark"));
        i.setTotal(rs.getDouble("total"));
        i.setYear(rs.getString("year"));

        return i;
    }

    public static String getStatus(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String check = "";
        String app = "";
        String post = "";
        String status = "<span class=\"label label-primary\">Preparing</span>";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select checkid,appid,postflag from ar_debitnote where noteno = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            check = rs.getString(1);
            app = rs.getString(2);
            post = rs.getString(3);
        }

        stmt.close();
        rs.close();

        if (!check.equals("")) {
            status = "Checked";
        }

        if (!app.equals("")) {
            status = "Approved";
        }

//        if(post.equals("posted")){
//            status = "<span class=\"label label-info\">Post</span>";
//        }
        if (post.equals("Cancel")) {
            status = "Posted";
        }

        return status;
    }

    public static boolean isCheck(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select checkid from ar_debitnote where noteno = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString(1);
        }

        if (!ck.equals("")) {
            cek = true;
        }

        stmt.close();
        rs.close();

        return cek;
    }

    public static boolean isApprove(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select appid from ar_debitnote where noteno = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString(1);
        }

        if (!ck.equals("")) {
            cek = true;
        }

        stmt.close();
        rs.close();

        return cek;
    }

    public static Double getBalance(LoginProfile log, String column, String id) throws Exception {
        Double x = 0.00;
        Double y = 0.00;
        Statement stment = log.getCon().createStatement();
        ResultSet setent = stment.executeQuery(" select sum(" + column + ") as sumamount from cb_official_credit where voucherno = '" + id + "'");

        while (setent.next()) {
            x += setent.getDouble("sumamount");
        }

        setent = stment.executeQuery(" select amount from cb_official where refer = '" + id + "'");

        if (setent.next()) {
            y = setent.getDouble("amount");
        }

        stment.close();
        setent.close();
        return y - x;
    }

    public static void saveItem(LoginProfile log, ArDebitNoteItem item) throws Exception {

        try {
            String q = ("insert into ar_debitnote_item(noteno,prodcode,prodname,coacode,coaname,qty,unitp,amount,unitm,remarks,loclevel,loccode,locdesc,taxcode,taxrate,taxamt,taxcoacode,taxcoadescp,satype,sacode,sadesc,refer) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getNoteno());
            ps.setString(2, item.getProdcode());
            ps.setString(3, item.getProdname());
            ps.setString(4, item.getCoacode());
            ps.setString(5, item.getCoaname());
            ps.setDouble(6, item.getQty());
            ps.setString(7, item.getUnitp());
            ps.setDouble(8, item.getAmount());
            ps.setString(9, item.getUnitm());
            ps.setString(10, item.getRemarks());
            ps.setString(11, item.getLoclevel());
            ps.setString(12, item.getLoccode());
            ps.setString(13, item.getLocdesc());
            ps.setString(14, item.getTaxcode());
            ps.setDouble(15, item.getTaxrate());
            ps.setDouble(16, item.getTaxamt());
            ps.setString(17, TaxDAO.getTax(log, item.getTaxcode()).getTaxcoacode());
            ps.setString(18, TaxDAO.getTax(log, item.getTaxcode()).getTaxcoadescp());
            ps.setString(19, item.getSatype());
            ps.setString(20, item.getSacode());
            ps.setString(21, item.getSadesc());
            ps.setString(22, AutoGenerate.getReferAddBack(log, "ar_debitnote_item", "refer", item.getNoteno(), "noteno"));

            ps.executeUpdate();

            ArDebitNoteDAO.updateAmount(log, item.getNoteno(), GeneralTerm.twoDecimalDouble(item.getAmount()) + GeneralTerm.twoDecimalDouble(item.getTaxamt()));

            Logger.getLogger(ArDebitNoteDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

            LiveGLDAO.reloadGL(log, item.getNoteno(), "DNR");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static List<ArDebitNoteItem> getAllItemByPage(LoginProfile log, String refno, int row, int limit) throws Exception {

        Statement stmt = null;
        List<ArDebitNoteItem> CVi;
        CVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from ar_debitnote_item where noteno = '" + refno + "' order by refer limit " + row + "," + limit);

            while (rs.next()) {
                CVi.add(getRefer(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<ArDebitNoteItem> getAllItem(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<ArDebitNoteItem> i;
        i = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from ar_debitnote_item where noteno = '" + refno + "' order by refer");

            while (rs.next()) {
                i.add(getRefer(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return i;
    }

    public static ArDebitNoteItem getRefer(ResultSet rs) throws SQLException {
        ArDebitNoteItem i = new ArDebitNoteItem();

        i.setAmount(rs.getDouble("amount"));
        i.setCoacode(rs.getString("coacode"));
        i.setCoaname(rs.getString("coaname"));
        i.setNoteno(rs.getString("noteno"));
        i.setLoccode(rs.getString("loccode"));
        i.setLoclevel(rs.getString("loclevel"));
        i.setLocdesc(rs.getString("locdesc"));
        i.setProdcode(rs.getString("prodcode"));
        i.setProdname(rs.getString("prodname"));
        i.setQty(rs.getDouble("qty"));
        i.setUnitm(rs.getString("unitm"));
        i.setUnitp(rs.getString("unitp"));
        i.setRefer(rs.getString("refer"));
        i.setRemarks(rs.getString("remarks"));
        i.setSacode(rs.getString("sacode"));
        i.setSadesc(rs.getString("sadesc"));
        i.setSatype(rs.getString("satype"));
        i.setTaxamt(rs.getDouble("taxamt"));
        i.setTaxcoacode(rs.getString("taxcoacode"));
        i.setTaxcoadescp(rs.getString("taxcoadescp"));
        i.setTaxcode(rs.getString("taxcode"));
        i.setTaxrate(rs.getDouble("taxrate"));

        return i;
    }

    public static ArDebitNoteItem getItem(LoginProfile log, String refno) throws SQLException, Exception {
        ArDebitNoteItem c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from ar_debitnote_item where refer=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getRefer(rs);
        }

        return c;
    }

    public static void updateItem(LoginProfile log, ArDebitNoteItem item, String id) throws Exception {

        try {
            String q = ("UPDATE ar_debitnote_item set prodcode = ?,prodname = ?,coacode = ?,coaname = ?,qty = ?,unitp = ?,amount = ?,unitm = ?,remarks = ?,loclevel = ?,loccode = ?,locdesc = ?,taxcode = ?,taxrate = ?,taxamt = ?,taxcoacode = ?,taxcoadescp = ?,satype = ?,sacode = ?,sadesc = ? WHERE refer = '" + id + "'");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getProdcode());
            ps.setString(2, item.getProdname());
            ps.setString(3, item.getCoacode());
            ps.setString(4, item.getCoaname());
            ps.setDouble(5, item.getQty());
            ps.setString(6, item.getUnitp());
            ps.setDouble(7, item.getAmount());
            ps.setString(8, item.getUnitm());
            ps.setString(9, item.getRemarks());
            ps.setString(10, item.getLoclevel());
            ps.setString(11, item.getLoccode());
            ps.setString(12, item.getLocdesc());
            ps.setString(13, item.getTaxcode());
            ps.setDouble(14, item.getTaxrate());
            ps.setDouble(15, item.getTaxamt());
            ps.setString(16, TaxDAO.getTax(log, item.getTaxcode()).getTaxcoacode());
            ps.setString(17, TaxDAO.getTax(log, item.getTaxcode()).getTaxcoadescp());
            ps.setString(18, item.getSatype());
            ps.setString(19, item.getSacode());
            ps.setString(20, item.getSadesc());

            Logger.getLogger(ArDebitNoteDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();

            ps.close();

            LiveGLDAO.reloadGL(log, item.getNoteno(), "DNR");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteItem(LoginProfile log, String refer) throws SQLException, Exception {

        String deleteQuery_2 = "delete from ar_debitnote_item where refer= ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, refer);
        ps_2.executeUpdate();
        ps_2.close();

        LiveGLDAO.reloadGL(log, refer, "DNR");

    }

    public static void updateMain(LoginProfile log, ArDebitNote m, String id) throws Exception {

        try {
            String q = ("UPDATE ar_debitnote set noteno = ?,buyercode = ?,buyername = ?,estcode = ?,estname = ?,period = ?,remark = ?,year = ?,total = ?,notedate = ? WHERE noteno = '" + id + "'");
            //amtbeforetax=?
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, m.getNoteno());
            ps.setString(2, m.getBuyercode());
            ps.setString(3, m.getBuyername());
            ps.setString(4, m.getEstcode());
            ps.setString(5, m.getEstname());
            ps.setString(6, m.getPeriod());
            ps.setString(7, m.getRemark());
            ps.setString(8, m.getYear());
            ps.setDouble(9, m.getTotal());
            ps.setDate(10, (m.getNotedate() != null) ? new java.sql.Date(m.getNotedate().getTime()) : null);
            Logger.getLogger(ArDebitNoteDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();

            ps.close();

            LiveGLDAO.reloadGL(log, m.getNoteno(), "DNR");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteExist(LoginProfile log, String no) throws SQLException, Exception {

        String deleteQuery_1 = "delete from ar_debitnote where noteno = ?";
        String deleteQuery_2 = "delete from ar_debitnote_item where noteno = ?";
        PreparedStatement ps_1 = log.getCon().prepareStatement(deleteQuery_1);
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_1.setString(1, no);
        ps_2.setString(1, no);
        ps_1.executeUpdate();
        ps_2.executeUpdate();
        ps_1.close();
        ps_2.close();

        LiveGLDAO.deleteFromGL(log, no);

    }

    public static double getDebit(LoginProfile log, String refno) throws SQLException, Exception {
        double amount = 0;
        ResultSet rs = null;
        ResultSet rs1 = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select total from ar_debitnote where noteno=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amount = rs.getDouble(1);
        }

        return amount;
    }

    public static double getCredit(LoginProfile log, String refno) throws SQLException, Exception {
        double amt = 0;
        double amttax = 0;
        double totalCredit = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select sum(amount) as amt,sum(taxamt) as amttax from ar_debitnote_item where noteno=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amt = rs.getDouble(1);
            amttax = rs.getDouble(2);
        }

        totalCredit = amt + amttax;

        return totalCredit;
    }

    public static void checkVoucher(LoginProfile log, String refer, String staff_id, String staff_name) throws Exception {

        String query = "UPDATE ar_debitnote SET checkid = '" + staff_id + "',checkname = '" + staff_name + "',checkdate='" + AccountingPeriod.getCurrentTimeStamp() + "' where noteno = '" + refer + "'";

        try {
            PreparedStatement ps = log.getCon().prepareStatement(query);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void approveVoucher(LoginProfile log, String refer, String staff_id, String staff_name, String position) throws Exception {

        String query = "UPDATE ar_debitnote SET appid = '" + staff_id + "',appname = '" + staff_name + "',appdate='" + AccountingPeriod.getCurrentTimeStamp() + "', appdesign = '" + position + "' where noteno = '" + refer + "'";

        try {
            PreparedStatement ps = log.getCon().prepareStatement(query);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static List<ArDebitNote> getAllRDN(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<ArDebitNote> i;
        i = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from ar_debitnote");

            while (rs.next()) {
                i.add(getMain(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return i;
    }

    public static List<ArDebitNoteItem> getAllTax(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<ArDebitNoteItem> CVi;
        CVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs;
            rs = stmt.executeQuery("select * from ar_debitnote_item where noteno = '" + refno + "' and taxamt>0 order by refer");

            while (rs.next()) {
                CVi.add(ArDebitNoteDAO.getRefer(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    private static void updateAmount(LoginProfile log, String refer, double amount) throws Exception {

        String query = "UPDATE ar_debitnote SET total = '" + amount + "' where noteno = '" + refer + "'";

        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }

    public static List<ArDebitNote> getAll(LoginProfile log, String type) throws Exception, SQLException {

        Statement stmt = null;
        List<ArDebitNote> CV;
        CV = new ArrayList();

        String q = "";
        if (type.equals("approval")) {
            q = " WHERE appid = '' AND checkid <> ''";
        } else if (type.equals("check")) {
            q = " WHERE appid = '' AND checkid = ''";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from ar_debitnote " + q + " order by noteno");

            while (rs.next()) {
                CV.add(ArDebitNoteDAO.getMain(rs));
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static ArDebitNoteMaster getMaster(LoginProfile log, String refno) throws Exception {

        ArDebitNoteMaster pm = new ArDebitNoteMaster();

        pm.setaMain(ArDebitNoteDAO.getMain(log, refno));
        pm.setaList(ArDebitNoteDAO.getAllItem(log, refno));

        return pm;

    }

    public static boolean isComplete(LoginProfile log, String refno) throws Exception {

        boolean s = true;
        ArDebitNoteMaster pm = (ArDebitNoteMaster) getMaster(log, refno);

        ArDebitNote p = (ArDebitNote) pm.getaMain();

        if (p.getTotal() <= 0.0) {
            s = false;
        } else if (p.getBuyercode().equals("") || p.getBuyercode() == null) {
            s = false;
        } else if (p.getRemark().equals("") || p.getRemark() == null) {
            s = false;
        }

        return s;
    }

    public static boolean hasDCNote(LoginProfile log, String refno) throws Exception {

        boolean s = false;

        ArDebitNoteMaster pm = (ArDebitNoteMaster) getMaster(log, refno);

        List<ArDebitNoteItem> listx = (List<ArDebitNoteItem>) pm.getaList();
        for (ArDebitNoteItem j : listx) {

            s = DebitCreditNoteDAO.checkSuspence(log, j.getCoacode());

        }

        return s;
    }

}
