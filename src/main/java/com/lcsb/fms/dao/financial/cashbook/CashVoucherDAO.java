/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.dao.financial.cashbook;

import com.lcsb.fms.dao.financial.gl.JournalVoucherDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.general.DataTable;
import com.lcsb.fms.model.financial.cashbook.CashVoucher;
import com.lcsb.fms.model.financial.cashbook.CashVoucherAccount;
import com.lcsb.fms.model.financial.cashbook.CbCashVoucherReceipt;
import com.lcsb.fms.model.financial.cashbook.CbCashVoucherReceiptPK;
import com.lcsb.fms.model.financial.gl.JournalVoucher;
import com.lcsb.fms.model.financial.gl.JournalVoucherItem;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class CashVoucherDAO {
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("020202");
        mod.setModuleDesc("Cash Voucher");
        mod.setMainTable("cb_cashvoucher");
        mod.setReferID_Master("refer");
        return mod;
         
    }
    
    public static List<ListTable> tableList() {
        
        String column_names[] = {"refer", "voucherdate", "payto", "total", "checkid", "appid", "post"};
        String title_name[] = {"Reference No.", "Date", "Payee", "Amount (RM)", "Check ID", " Approve ID", "Post"};
        boolean boolview[] = {true, true, true, true, false, false, false};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            i.setBoolView(boolview[j]);
            lt.add(i);
        }
        
        return lt;
    }
    
    public static void deleteExist(LoginProfile log, String no) throws SQLException, Exception{
         
        Connection con = log.getCon();
        
        String deleteQuery_1 = "delete from cb_cashvoucher where refer = ?";
        String deleteQuery_2 = "delete from cb_cashvoucher_account where refer = ?";
        PreparedStatement ps_1 = con.prepareStatement(deleteQuery_1);
        PreparedStatement ps_2 = con.prepareStatement(deleteQuery_2);
        ps_1.setString(1, no);
        ps_2.setString(1, no);
        ps_1.executeUpdate();
        ps_2.executeUpdate();
        ps_1.close();
        ps_2.close();
        
        
    }
     
     public static void deleteItem(LoginProfile log, String novoucher, String refer) throws SQLException, Exception{
         
        
        
        String deleteQuery_2 = "delete from cb_cashvoucher_account where novoucher = ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, novoucher);
        ps_2.executeUpdate();
        ps_2.close();
        
        //updateAmount(refer);
        
        
    }
    
    public static void saveMain(LoginProfile log, CashVoucher item) throws Exception{
        
        try{
            String q = ("insert into cb_cashvoucher(refer,voucherid,year,period,payto,total,amount,reason,receivetype,receiveid,receivebyname,estatecode,prepareid,preparedate,preparename,voucherdate) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, AutoGenerate.getReferenceNo(log, "refer", "cb_cashvoucher", "CVN", log.getEstateCode()));
            ps.setString(2, AutoGenerate.getVoucherNo(log, "cb_cashvoucher", "voucherid"));
            ps.setString(3, item.getYear());
            ps.setString(4, item.getPeriod());
            ps.setString(5, item.getPayto());
            ps.setDouble(6, item.getTotal());
            ps.setString(7, item.getAmount());
            ps.setString(8, item.getReason());
            ps.setString(9, item.getReceivetype());
            ps.setString(10, item.getReceiveid());
            ps.setString(11, item.getReceivebyname());
            ps.setString(12, log.getEstateCode());
            ps.setString(13, log.getUserID());
            ps.setString(14, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(15, log.getFullname());
            ps.setString(16, item.getVoucherdate());
            ps.executeUpdate();
            
            Logger.getLogger(CashVoucherDAO.class.getName()).log(Level.INFO, String.valueOf(ps));

            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
    
    public static void updateMain(LoginProfile log, CashVoucher item, String refer) throws Exception{
        
        try{
            String q = ("UPDATE cb_cashvoucher set year = ?,period = ?,payto = ?,total = ?,amount = ?,reason = ?,receivetype = ?,receiveid = ?,receivebyname = ?,estatecode = ?,prepareid = ?,preparedate = ?,preparename = ?,voucherdate = ? where refer = '"+refer+"'");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getYear());
            ps.setString(2, item.getPeriod());
            ps.setString(3, item.getPayto());
            ps.setDouble(4, item.getTotal());
            ps.setString(5, item.getAmount());
            ps.setString(6, item.getReason());
            ps.setString(7, item.getReceivetype());
            ps.setString(8, item.getReceiveid());
            ps.setString(9, item.getReceivebyname());
            ps.setString(10, item.getEstatecode());
            ps.setString(11, item.getPrepareid());
            ps.setString(12, item.getPreparedate());
            ps.setString(13, item.getPreparename());
            ps.setString(14, item.getVoucherdate());
            ps.executeUpdate();

            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
     
    public static void saveItem(LoginProfile log, CashVoucherAccount item) throws Exception{
        
        try{
            String q = ("insert into cb_cashvoucher_account(novoucher,refer,debit,loclevel,loccode,locname,coacode,coadescp,remarks,satype,sacode,sadesc,taxcode,taxdescp,taxrate,taxamt,amtbeforetax) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, AutoGenerate.getReferAddBack(log, "cb_cashvoucher_account", "novoucher", item.getRefer(), "refer"));
            ps.setString(2, item.getRefer());
            ps.setString(3, item.getDebit());
            ps.setString(4, item.getLoclevel());
            ps.setString(5, item.getLoccode());
            ps.setString(6, item.getLocname());
            ps.setString(7, item.getCoacode());
            ps.setString(8, item.getCoadescp());
            ps.setString(9, item.getRemarks());
            ps.setString(10, item.getSatype());
            ps.setString(11, item.getSacode());
            ps.setString(12, item.getSadesc());
            ps.setString(13, item.getTaxcode());
            ps.setString(14, item.getTaxdescp());
            ps.setDouble(15, item.getTaxrate());
            ps.setDouble(16, item.getTaxamt());
            ps.setDouble(17, item.getAmtbeforetax());
           
            ps.executeUpdate();
            Logger.getLogger(CashVoucherDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void saveReceipt(LoginProfile log, CbCashVoucherReceipt item) throws Exception{
        
        try{
            String q = ("insert into cb_receipt(gstid, novoucer, remarks, trade, tarikh, total, noreceipt, refer) values (?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getGstid());
            ps.setString(2, item.getNovoucer());
            ps.setString(3, item.getRemarks());
            ps.setString(4, item.getTrade());
            ps.setDate(5, (item.getTarikh()!=null)?new java.sql.Date(item.getTarikh().getTime()):null);
            ps.setDouble(6, item.getTotal());
            ps.setString(7, item.getCbCashVoucherReceiptPK().getNoreceipt());
            ps.setString(8, item.getCbCashVoucherReceiptPK().getRefer());
           
            ps.executeUpdate();
            Logger.getLogger(CashVoucherDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, CashVoucherAccount item, String id) throws Exception{
        
        
        try{
            //String q = ("insert into gl_jv_item(Jvid,JVrefno,remark,actcode,actdesc,loccode,locdesc,loclevel,satype,sacode,sadesc,debit,credit,taxcode,taxdescp,taxrate,gstid,amtbeforetax) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            String q = ("UPDATE cb_cashvoucher_account set debit = ?,loclevel = ?,loccode = ?,locname = ?,coacode = ?,coadescp = ?,remarks = ?,satype = ?,sacode = ?,sadesc = ?, taxcode = ?,taxdescp = ?,taxrate = ?,taxamt = ?,amtbeforetax = ? WHERE novoucher = '"+id+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getDebit());
            ps.setString(2, item.getLoclevel());
            ps.setString(3, item.getLoccode());
            ps.setString(4, item.getLocname());
            ps.setString(5, item.getCoacode());
            ps.setString(6, item.getCoadescp());
            ps.setString(7, item.getRemarks());
            ps.setString(8, item.getSatype());
            ps.setString(9, item.getSacode());
            ps.setString(10, item.getSadesc());
            ps.setString(11, item.getTaxcode());
            ps.setString(12, item.getTaxdescp());
            ps.setDouble(13, item.getTaxrate());
            ps.setDouble(14, item.getTaxamt());
            ps.setDouble(15, item.getAmtbeforetax());
           
            ps.executeUpdate();
            Logger.getLogger(CashVoucherDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static String getCVno(LoginProfile log, String abb) throws Exception{
        
        ResultSet rs = null;
        String CVno = "";
        String year = AccountingPeriod.getCurYear(log);
        String curperiod = AccountingPeriod.getCurPeriod(log);
        //String estatecode = LoginProfile.getEstateCode();
        String estatecode = "9911";
        
        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat(?,'"+ estatecode +"',substring('"+ year +"',3,2),lpad('"+ curperiod +"',2,'0'),lpad((max(substring(refer,14,4))+1),4,'0')),concat('JPY','"+ estatecode +"',substring('"+ year +"',3,2),lpad('"+ curperiod +"',2,'0'),'0001')) as refer from cb_cashvoucher where refer like concat(?,'%') and year='"+ year +"' and period='"+ curperiod +"' and estatecode like concat('"+ estatecode +"','%')");
        stmt.setString(1, abb);
        stmt.setString(2, abb);
        
        rs = stmt.executeQuery();
        if (rs.next()) {
            CVno = rs.getString(1);
            
            
        }
        
        rs.close();
        stmt.close();
        return CVno;
        
    }
    
    public static String getCVoucherNo(LoginProfile log) throws Exception{
        
        ResultSet rs = null;
        String CVno = "";
        
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT ifnull(concat(lpad((max(voucherid)+1),5,'0')),concat('0001')) as new FROM `cb_cashvoucher`");
        
        rs = stmt.executeQuery();
        if (rs.next()) {
            CVno = rs.getString(1); 
        }
        
        rs.close();
        stmt.close();
        return CVno;
        
    }
    
    public static String getCVid(LoginProfile log, String refno) throws Exception, SQLException{
        
        ResultSet rs = null;
        String JVno = "";
        
        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat('"+refno+"',lpad((max(right(novoucher,4))+1),4,'0')),concat('"+refno+"','0001')) as njvno from cb_cashvoucher_account where refer='"+refno+"'");
        
        rs = stmt.executeQuery();
        if (rs.next()) {
            JVno = rs.getString(1); 
        }
        
        rs.close();
        stmt.close();
        return JVno;
        
    }
   
    public static Double getSum(LoginProfile log, String column, String id)throws Exception {
        
        Double x = 0.00;
        Statement stment = log.getCon().createStatement();   
        ResultSet setent =stment.executeQuery(" select sum("+column+") as sumamount from cb_cashvoucher_account where refer = '"+id+"'");
       
        while(setent.next()) {
            x+=setent.getDouble("sumamount");
        }
        
        stment.close();
        setent.close();
        return x;
    }
    
    public static Double getBalance(LoginProfile log, String column, String id)throws Exception {
        
        Double x = 0.00;
        Double y = 0.00;
        Statement stment = log.getCon().createStatement();   
        ResultSet setent =stment.executeQuery(" select sum("+column+") as sumamount from cb_cashvoucher_account where refer = '"+id+"'");
       
        while(setent.next()) {
            x+=setent.getDouble("sumamount");
        }
        
          
        setent =stment.executeQuery(" select total from cb_cashvoucher where refer = '"+id+"'");
       
        if(setent.next()) {
            y=setent.getDouble("total");
        }
        
        stment.close();
        setent.close();
        return y-x;
    }
    
    public static CashVoucherAccount getItem(LoginProfile log, String id)throws Exception {
        
        CashVoucherAccount v = new CashVoucherAccount();
        
        v.setRefer(id);
        v.setAmtbeforetax(getBalance(log, "debit",id));
       
        return v;
    }
    
   
    
    private static void updateAmount(LoginProfile log, String JVrefno) throws Exception{
        
        String query = "UPDATE gl_jv SET todebit = '"+getSum(log, "debit",JVrefno)+"',tocredit = '"+getSum(log, "credit",JVrefno)+"' where JVrefno = '"+JVrefno+"'";
        
        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }
    
    public static List<CashVoucherAccount> getAllCVItem(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<CashVoucherAccount> CVi;
        CVi = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_cashvoucher_account where refer = '"+refno+"' order by refer");

            while (rs.next()) {
                CVi.add(getCVitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static List<CbCashVoucherReceipt> getAllReceipt(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<CbCashVoucherReceipt> CVi;
        CVi = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_receipt where refer = '"+refno+"' order by refer");

            while (rs.next()) {
                CVi.add(getReceipt(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static CashVoucherAccount getCVitem(LoginProfile log, String id) throws SQLException, Exception {
        CashVoucherAccount c = null;
        ResultSet rs = null;
        try{
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_cashvoucher_account where novoucher=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getCVitem(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        
        return c;
    }
    
    private static CashVoucherAccount getCVitem(ResultSet rs) throws SQLException {
        CashVoucherAccount cv = new CashVoucherAccount();
        cv.setAmtbeforetax(rs.getDouble("amtbeforetax"));
        cv.setCoadescp(rs.getString("coadescp"));
        cv.setDebit(rs.getString("debit"));
        cv.setLoclevel(rs.getString("loclevel"));
        cv.setLocname(rs.getString("locname"));
        cv.setRefer(rs.getString("refer"));
        cv.setRemarks(rs.getString("remarks"));
        cv.setSacode(rs.getString("sacode"));
        cv.setSadesc(rs.getString("sadesc"));
        cv.setSatype(rs.getString("satype"));
        cv.setTaxamt(rs.getDouble("taxamt"));
        cv.setTaxcoacode(rs.getString("taxcoacode"));
        cv.setTaxcoadescp(rs.getString("taxcoadescp"));
        cv.setTaxcode(rs.getString("taxcode"));
        cv.setTaxdescp(rs.getString("taxdescp"));
        cv.setTaxrate(rs.getDouble("taxrate"));
        cv.setNovoucher(rs.getString("novoucher"));
        cv.setLoccode(rs.getString("loccode"));
        cv.setCoacode(rs.getString("coacode"));
        
        return cv;
    }
    private static CbCashVoucherReceipt getReceipt(ResultSet rs) throws SQLException {
        CbCashVoucherReceipt cv = new CbCashVoucherReceipt();
        CbCashVoucherReceiptPK cvPK = new CbCashVoucherReceiptPK();
        
        cvPK.setNoreceipt(rs.getString("noreceipt"));
        cvPK.setRefer(rs.getString("refer"));
        
        cv.setCbCashVoucherReceiptPK(cvPK);
        cv.setGstid(rs.getString("gstid"));
        cv.setNovoucer(rs.getString("novoucer"));
        cv.setRemarks(rs.getString("remarks"));
        cv.setTarikh(rs.getDate("tarikh"));
        cv.setTotal(rs.getDouble("total"));
        cv.setTrade(rs.getString("trade"));
        
        return cv;
    }
    
    public static List<CashVoucher> getAllCV(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<CashVoucher> CV;
        CV = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_cashvoucher where refer = '"+refno+"' order by refer");

            while (rs.next()) {
                CV.add(getCV(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }
    
    public static CashVoucher getCV(LoginProfile log, String refno) throws SQLException, Exception {
        CashVoucher c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_cashvoucher where refer=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getCV(rs);
        }

        
        return c;
    }
    
    private static CashVoucher getCV(ResultSet rs) throws SQLException {
        CashVoucher cv = new CashVoucher();
        cv.setAmount(rs.getString("amount"));
        cv.setAmounttotaldebit(rs.getString("amounttotaldebit"));
        cv.setAppdesign(rs.getString("appdesign"));
        cv.setAppid(rs.getString("appid"));
        cv.setAppdesign(rs.getString("appdesign"));
        cv.setApprobyname(rs.getString("approbyname"));
        cv.setApprovedate(rs.getString("approvedate"));
        cv.setCheckdate(rs.getString("checkdate"));
        cv.setCheckid(rs.getString("checkid"));
        cv.setCheckname(rs.getString("checkname"));
        cv.setEstatecode(rs.getString("estatecode"));
        cv.setGstdate(rs.getString("gstdate"));
        cv.setPaymentflag(rs.getString("paymentflag"));
        cv.setPayto(rs.getString("payto"));
        cv.setPeriod(rs.getString("period"));
        cv.setPettycode(rs.getString("pettycode"));
        cv.setPost(rs.getString("post"));
        cv.setPostdate(rs.getString("postdate"));
        cv.setPostflag(rs.getString("postflag"));
        cv.setPostgst(rs.getString("postgst"));
        cv.setPreparedate(rs.getString("preparedate"));
        cv.setPrepareid(rs.getString("prepareid"));
        cv.setPreparename(rs.getString("preparename"));
        cv.setRacoacode(rs.getString("racoacode"));
        cv.setRacoadesc(rs.getString("racoadesc"));
        cv.setReason(rs.getString("reason"));
        cv.setReceiptno(rs.getString("receiptno"));
        cv.setReceivebyname(rs.getString("receivebyname"));
        cv.setReceiveid(rs.getString("receiveid"));
        cv.setReceivetype(rs.getString("receivetype"));
        cv.setRefer(rs.getString("refer"));
        cv.setTotal(rs.getDouble("total"));
        cv.setVoucherdate(rs.getString("voucherdate"));
        cv.setVoucherid(rs.getString("voucherid"));
        cv.setYear(rs.getString("year"));
        
        return cv;
    }
    
    
    public static List<CashVoucher> getAll(LoginProfile log, DataTable prm) throws Exception, SQLException {

        Statement stmt = null;
        List<CashVoucher> CV;
        CV = new ArrayList();

        String q = "";
        if (prm.getViewBy().equals("today")) {
            q = " WHERE voucherdate = '" + AccountingPeriod.getCurrentTimeStamp() + "'";
        } else if (prm.getViewBy().equals("thismonth")) {
            q = " WHERE period = '" + AccountingPeriod.getCurPeriod(log) + "' and year = '" + AccountingPeriod.getCurYear(log) + "'";
        } else if (prm.getViewBy().equals("byperiod")) {
            q = " WHERE period = '" + prm.getPeriod() + "' and year = '" + prm.getYear() + "'";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_cashvoucher " + q + " order by refer");

            while (rs.next()) {
                CV.add(getCV(rs));
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }
}
