/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.employee;

import com.lcsb.fms.dao.setup.company.StaffDAO;
import com.lcsb.fms.dao.setup.configuration.DeductionDAO;
import com.lcsb.fms.dao.setup.configuration.EarningDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.general.DataTable;
import com.lcsb.fms.model.financial.employee.EmPaySheetList;
import com.lcsb.fms.model.financial.employee.EmPayroll;
import com.lcsb.fms.model.financial.employee.EmPayrollDeduction;
import com.lcsb.fms.model.financial.employee.EmPayrollEarning;
import com.lcsb.fms.model.financial.employee.EmPayrollMaster;
import com.lcsb.fms.model.financial.employee.EmPayrollPCB;
import com.lcsb.fms.model.financial.employee.EmPayrollPCBforP;
import com.lcsb.fms.model.financial.employee.EmPayrollStaffParam;
import com.lcsb.fms.model.financial.employee.EmPayrollType;
import com.lcsb.fms.model.setup.company.staff.Staff;
import com.lcsb.fms.model.setup.configuration.TaxTable;
import com.lcsb.fms.util.dao.DataDAO;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class EmPayrollDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("020309");
        mod.setModuleDesc("Payroll");
        mod.setMainTable("em_payroll_master");
        mod.setReferID_Master("refer");
        return mod;

    }

    public static List<ListTable> tableList() {

        String column_names[] = {"refer", "date", "year", "period", "status","postflag","checkid","approveid"};
        String title_name[] = {"Refer", "Date", "Year", "Period", "status","postflag","check","approve"};
        boolean boolview[] = {true, true, true, true, false,false,false,false};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            i.setBoolView(boolview[j]);
            lt.add(i);
        }

        return lt;
    }

    public static List<EmPayroll> getAll(LoginProfile log, DataTable prm) throws Exception, SQLException {

        Statement stmt = null;
        List<EmPayroll> CV;
        CV = new ArrayList();

        String q = "";
        if (prm.getViewBy().equals("today")) {
            q = " WHERE date = '" + AccountingPeriod.getCurrentTimeStamp() + "'";
        } else if (prm.getViewBy().equals("thismonth")) {
            q = " WHERE period = '" + AccountingPeriod.getCurPeriod(log) + "' and year = '" + AccountingPeriod.getCurYear(log) + "'";
        } else if (prm.getViewBy().equals("byperiod")) {
            q = " WHERE period = '" + prm.getPeriod() + "' and year = '" + prm.getYear() + "'";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from em_payroll_master " + q + " order by refer");

            while (rs.next()) {
                CV.add(getEMP(rs));
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static List<EmPayrollEarning> getAllEarningTemp(LoginProfile log, String refer) throws Exception, SQLException {

        List<EmPayrollEarning> CV;
        CV = new ArrayList();

        ResultSet rs = null;

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM em_payroll_earning_temp WHERE refer = ?");
            stmt.setString(1, refer);
            rs = stmt.executeQuery();

            while (rs.next()) {
                CV.add(EmPayrollDAO.getEMPEarning(rs));
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }
    
    public static List<EmPayrollDeduction> getAllDeductionTemp(LoginProfile log, String refer) throws Exception, SQLException {

        List<EmPayrollDeduction> CV;
        CV = new ArrayList();

        ResultSet rs = null;

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM em_payroll_deduction_temp WHERE refer = ?");
            stmt.setString(1, refer);
            rs = stmt.executeQuery();

            while (rs.next()) {
                CV.add(EmPayrollDAO.getEMPDeduction(rs));
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static EmPayroll getEMP(ResultSet rs) throws SQLException {
        EmPayroll cv = new EmPayroll();
        cv.setDate(rs.getString("date"));
        cv.setPeriod(rs.getString("period"));
        cv.setPredate(rs.getString("predate"));
        cv.setPreid(rs.getString("preid"));
        cv.setPrename(rs.getString("prename"));
        cv.setPreposition(rs.getString("preposition"));
        cv.setRefer(rs.getString("refer"));
        cv.setStatus(rs.getString("status"));
        cv.setYear(rs.getString("year"));

        return cv;
    }

    public static String saveMain(LoginProfile log, EmPayroll item) throws Exception {

        String newrefer = AutoGenerate.getReferenceNox(log, "refer", "em_payroll_master", "EMP", item.getCompanycode(), String.valueOf(item.getYear()), String.valueOf(item.getPeriod()));

        try {
            String q = ("insert into em_payroll_master(refer,date,period,predate,preid,prename,preposition,status,year,companycode,companydescp) values (?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, newrefer);
            ps.setString(2, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(3, item.getPeriod());
            ps.setString(4, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(5, log.getUserID());
            ps.setString(6, log.getFullname());
            ps.setString(7, log.getPosition());
            ps.setString(8, "Preparing");
            ps.setString(9, item.getYear());
            ps.setString(10, item.getCompanycode());
            ps.setString(11, item.getCompanydescp());

            Logger.getLogger(EmPayrollDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(ps));
            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newrefer;

    }

    public static EmPayrollStaffParam getParamList(LoginProfile log, String staffid, String code, String type) throws SQLException {

        EmPayrollStaffParam e = new EmPayrollStaffParam();
        
        String table = "";
        String codex = "";
        if(type.equals("Earning")){
            table = "em_payroll_earning_temp";
            codex = "earncode";
        }else if(type.equals("Deduction")){
            table = "em_payroll_deduction_temp";
            codex = "deductcode";
        }

        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select amount, guna from "+table+" where staffid= ? and "+codex+" = ?");

        stmt.setString(1, staffid);
        stmt.setString(2, code);
        rs = stmt.executeQuery();
        if (rs.next()) {
            e.setAmount(rs.getDouble(1));
            e.setApplicable(rs.getString(2));
        }else{
            e.setApplicable("No");
        }
        
        stmt.close();
        rs.close();

        return e;
    }

    public static boolean checkExist(LoginProfile log, String refer, String staffid, String code) throws SQLException {

        boolean c = false;

        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select * from em_payroll_earning where staffid= ? and earncode = ?");

        stmt.setString(1, staffid);
        stmt.setString(2, code);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = true;
        }

        stmt.close();
        rs.close();

        return c;
    }

    public static void saveType(LoginProfile log, String refer, String code, String type) throws Exception {

        try {
            String descp = "";
            
            if(type.equals("Earning")){
                descp = EarningDAO.getInfo(log, code).getDescp();
            }else if(type.equals("Deduction")){
                descp = DeductionDAO.getInfo(log, code).getDescp();
            }

            String q = ("INSERT INTO em_payroll_type(refer,earncode,earndesc,status,type) values (?,?,?,?,?)");

            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, refer);
                ps.setString(2, code);
                ps.setString(3, descp);
                ps.setString(4, "Preparing");
                ps.setString(5, type);
                ps.executeUpdate();

            }
            
            if(type.equals("Earning")){
                EmPayrollDAO.saveStaffBaseEarnType(log, refer, code);
            }
            else if(type.equals("Deduction")){
                EmPayrollDAO.saveStaffBaseDeductionType(log, refer, code);
            }           

            

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void saveStaffBaseEarnType(LoginProfile log, String refer, String code) throws Exception {

        try {

            List<Staff> listAll = (List<Staff>) StaffDAO.getAllHQStaff(log);

            for (Staff j : listAll) {


                EmPayrollEarning ep = new EmPayrollEarning();

                if (EmPayrollDAO.checkExist(log, refer, j.getStaffid(), code)) {
                    

                    ep = EmPayrollDAO.getLastSavedEarning(log, j.getStaffid(), code);
                    Logger.getLogger(EmPayrollDAO.class.getName()).log(Level.INFO, "4----dah ada seblum ni---"+ep.getAmount());

                } else {

                    ep.setAmount(0.00);
                    ep.setBasicflag("");
                    ep.setDaymth("");
                    ep.setEarncode(code);
                    ep.setEarndesc(EarningDAO.getInfo(log, code).getDescp());
                    ep.setEarntype(EarningDAO.getInfo(log, code).getType());
                    ep.setEstatecode(log.getEstateCode());
                    ep.setEstatename(log.getEstateDescp());
                    ep.setPeriod(AccountingPeriod.getCurPeriod(log));
                    //ep.setPosition(((StaffDAO.getDesignation(log, j.getStaffid()).getDesignation()== null) ? "" : StaffDAO.getDesignation(log, j.getStaffid()).getDesignation()));
                    ep.setRefer(refer);
                    ep.setStaffid(j.getStaffid());
                    ep.setStaffname(j.getName());
                    ep.setStafftype(j.getWorkertype());
                    ep.setYear(AccountingPeriod.getCurYear(log));
                    
                    Logger.getLogger(EmPayrollDAO.class.getName()).log(Level.INFO, "4---belum ada----" + ep.getStaffid());


                }

                //refer,year,period,staffid,earncode,earndesc,amount,staffname,epf,socso,basicflag,loclevel,daymth,flag,position,earntype,minwday,estatecode,estatename,stafftype,salary,guna,includeinp
                String q = ("INSERT INTO em_payroll_earning_temp(refer,year,period,staffid,earncode,earndesc,amount,staffname,epf,socso,basicflag,loclevel,daymth,flag,position,earntype,minwday,estatecode,estatename,stafftype,salary) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

                try (PreparedStatement ps = log.getCon().prepareStatement(q)) {

                    ps.setString(1, refer);
                    ps.setString(2, AccountingPeriod.getCurYear(log));
                    ps.setString(3, AccountingPeriod.getCurPeriod(log));
                    ps.setString(4, j.getStaffid());
                    ps.setString(5, ep.getEarncode());
                    ps.setString(6, ep.getEarndesc());
                    ps.setDouble(7, ep.getAmount());
                    ps.setString(8, ep.getStaffname());
                    ps.setString(9, ep.getEpf());
                    ps.setString(10, ep.getSocso());
                    ps.setString(11, ep.getBasicflag());
                    ps.setString(12, ep.getLoclevel());
                    ps.setString(13, ep.getDaymth());
                    ps.setString(14, ep.getFlag());
                    ps.setString(15, ep.getPosition());
                    ps.setString(16, ep.getEarntype());
                    ps.setString(17, ep.getMinwday());
                    ps.setString(18, ep.getEstatecode());
                    ps.setString(19, ep.getEstatename());
                    ps.setString(20, ep.getStafftype());
                    ps.setString(21, ep.getSalary());
                    ps.executeUpdate();
                    Logger.getLogger(EmPayrollDAO.class.getName()).log(Level.INFO, "0-----"+ps);
                    ps.close();

                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
    public static void saveStaffBaseDeductionType(LoginProfile log, String refer, String code) throws Exception {

        try {

            List<Staff> listAll = (List<Staff>) StaffDAO.getAllHQStaff(log);

            for (Staff j : listAll) {


                EmPayrollDeduction ep = new EmPayrollDeduction();

                if (EmPayrollDAO.checkExist(log, refer, j.getStaffid(), code)) {

                    ep = EmPayrollDAO.getLastSavedDeduction(log, j.getStaffid(), code);

                } else {

                    ep.setAmount(0.00);
                    ep.setBasicflag("");
                    ep.setDaymth("");
                    ep.setDeductcode(code);
                    ep.setDeductdesc(DeductionDAO.getInfo(log, code).getDescp());
                    ep.setDeducttype(DeductionDAO.getInfo(log, code).getType());
                    ep.setEstatecode(log.getEstateCode());
                    ep.setEstatename(log.getEstateDescp());
                    ep.setPeriod(AccountingPeriod.getCurPeriod(log));
                    //ep.setPosition(((StaffDAO.getDesignation(log, j.getStaffid()).getDesignation()== null) ? "" : StaffDAO.getDesignation(log, j.getStaffid()).getDesignation()));
                    ep.setRefer(refer);
                    ep.setStaffid(j.getStaffid());
                    ep.setStaffname(j.getName());
                    ep.setStafftype(j.getWorkertype());
                    ep.setYear(AccountingPeriod.getCurYear(log));


                }

                //refer,year,period,staffid,earncode,earndesc,amount,staffname,epf,socso,basicflag,loclevel,daymth,flag,position,earntype,minwday,estatecode,estatename,stafftype,salary,guna,includeinp
                String q = ("INSERT INTO em_payroll_deduction_temp(refer,year,period,staffid,deductcode,deductdesc,amount,staffname,epf,socso,basicflag,loclevel,daymth,flag,position,deducttype,minwday,estatecode,estatename,stafftype,salary) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

                try (PreparedStatement ps = log.getCon().prepareStatement(q)) {

                    ps.setString(1, refer);
                    ps.setString(2, AccountingPeriod.getCurYear(log));
                    ps.setString(3, AccountingPeriod.getCurPeriod(log));
                    ps.setString(4, j.getStaffid());
                    ps.setString(5, ep.getDeductcode());
                    ps.setString(6, ep.getDeductdesc());
                    ps.setDouble(7, ep.getAmount());
                    ps.setString(8, ep.getStaffname());
                    ps.setString(9, ep.getEpf());
                    ps.setString(10, ep.getSocso());
                    ps.setString(11, ep.getBasicflag());
                    ps.setString(12, ep.getLoclevel());
                    ps.setString(13, ep.getDaymth());
                    ps.setString(14, ep.getFlag());
                    ps.setString(15, ep.getPosition());
                    ps.setString(16, ep.getDeducttype());
                    ps.setString(17, ep.getMinwday());
                    ps.setString(18, ep.getEstatecode());
                    ps.setString(19, ep.getEstatename());
                    ps.setString(20, ep.getStafftype());
                    ps.setString(21, ep.getSalary());
                    ps.executeUpdate();

                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
    public static EmPayroll getPayroll(LoginProfile log, String refer) throws Exception, SQLException {

        Statement stmt = null;
        EmPayroll CV = new EmPayroll();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from em_payroll_master where refer = '" + refer + "'");

            while (rs.next()) {
                CV = getEMP(rs);
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static EmPayrollEarning getLastSavedEarning(LoginProfile log, String staffid, String code) throws Exception, SQLException {

        Statement stmt = null;
        EmPayrollEarning CV = new EmPayrollEarning();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from em_payroll_earning where earncode = '" + code + "' and staffid = '" + staffid + "' and id = (select max(id) from em_payroll_earning  where earncode = '" + code + "' and staffid = '" + staffid + "') order by earncode");

            while (rs.next()) {
                CV = getEMPEarning(rs);
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }
    
    
    public static EmPayrollDeduction getLastSavedDeduction(LoginProfile log, String staffid, String code) throws Exception, SQLException {

        Statement stmt = null;
        EmPayrollDeduction CV = new EmPayrollDeduction();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from em_payroll_deduction where deductcode = '" + code + "' and staffid = '" + staffid + "' and id = (select max(id) from em_payroll_deduction  where deductcode = '" + code + "' and staffid = '" + staffid + "') order by deductcode");

            while (rs.next()) {
                CV = getEMPDeduction(rs);
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static List<EmPayrollType> getSavedType(LoginProfile log, String refer, String type) throws Exception, SQLException {

        Statement stmt = null;
        List<EmPayrollType> CV;
        CV = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from em_payroll_type where refer = '" + refer + "' and type = '"+ type +"' order by earncode");

            while (rs.next()) {
                CV.add(getEMPEarningType(rs));
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static EmPayrollType getEMPEarningType(ResultSet rs) throws SQLException {
        EmPayrollType cv = new EmPayrollType();
        cv.setRefer(rs.getString("refer"));
        cv.setEarncode(rs.getString("earncode"));
        cv.setEarndesc(rs.getString("earndesc"));
        cv.setStatus(rs.getString("status"));
        cv.setType(rs.getString("type"));
        return cv;
    }

    public static int deleteEarning(LoginProfile log, String refer, String code) throws Exception {

        int x = 0;

        try {
            DataDAO.deleteDataTripleReferral(log, refer, code, "Earning", "em_payroll_type", "refer", "earncode", "type");
            DataDAO.deleteDataDoubleReferral(log, refer, code, "em_payroll_earning_temp", "refer", "earncode");
            DataDAO.deleteDataDoubleReferral(log, refer, code, "em_payroll_earning", "refer", "earncode");
            x = 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return x;

    }
    
    public static int deleteDeduction(LoginProfile log, String refer, String code) throws Exception {

        int x = 0;

        try {
            DataDAO.deleteDataTripleReferral(log, refer, code, "Deduction", "em_payroll_type", "refer", "earncode", "type");
            DataDAO.deleteDataDoubleReferral(log, refer, code, "em_payroll_deduction_temp", "refer", "deductcode");
            DataDAO.deleteDataDoubleReferral(log, refer, code, "em_payroll_deduction", "refer", "deductcode");
            x = 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return x;

    }

    public static EmPayrollEarning getEMPEarning(ResultSet rs) throws SQLException {

        EmPayrollEarning cv = new EmPayrollEarning();
        cv.setAmount(rs.getDouble("amount"));
        cv.setBasicflag(rs.getString("basicflag"));
        cv.setDaymth(rs.getString("daymth"));
        cv.setEarncode(rs.getString("earncode"));
        cv.setEarndesc(rs.getString("earndesc"));
        cv.setEarntype(rs.getString("earntype"));
        cv.setEpf(rs.getString("epf"));
        cv.setEstatecode(rs.getString("estatecode"));
        cv.setEstatename(rs.getString("estatename"));
        cv.setFlag(rs.getString("flag"));
        cv.setGuna(rs.getString("guna"));
        cv.setIncludeinp(rs.getString("includeinp"));
        cv.setLoclevel(rs.getString("loclevel"));
        cv.setMinwday(rs.getString("minwday"));
        cv.setPeriod(rs.getString("period"));
        cv.setPosition(rs.getString("position"));
        cv.setRefer(rs.getString("refer"));
        cv.setSalary(rs.getString("salary"));
        cv.setSocso(rs.getString("socso"));
        cv.setStaffid(rs.getString("staffid"));
        cv.setStaffname(rs.getString("staffname"));
        cv.setStafftype(rs.getString("stafftype"));
        cv.setYear(rs.getString("year"));

        return cv;
    }
    
    public static EmPayrollDeduction getEMPDeduction(ResultSet rs) throws SQLException {

        EmPayrollDeduction cv = new EmPayrollDeduction();
        cv.setAmount(rs.getDouble("amount"));
        cv.setBasicflag(rs.getString("basicflag"));
        cv.setDaymth(rs.getString("daymth"));
        cv.setDeductcode(rs.getString("deductcode"));
        cv.setDeductdesc(rs.getString("deductdesc"));
        cv.setDeducttype(rs.getString("deducttype"));
        cv.setEpf(rs.getString("epf"));
        cv.setEstatecode(rs.getString("estatecode"));
        cv.setEstatename(rs.getString("estatename"));
        cv.setFlag(rs.getString("flag"));
        cv.setGuna(rs.getString("guna"));
        cv.setIncludeinp(rs.getString("includeinp"));
        cv.setLoclevel(rs.getString("loclevel"));
        cv.setMinwday(rs.getString("minwday"));
        cv.setPeriod(rs.getString("period"));
        cv.setPosition(rs.getString("position"));
        cv.setRefer(rs.getString("refer"));
        cv.setSalary(rs.getString("salary"));
        cv.setSocso(rs.getString("socso"));
        cv.setStaffid(rs.getString("staffid"));
        cv.setStaffname(rs.getString("staffname"));
        cv.setStafftype(rs.getString("stafftype"));
        cv.setYear(rs.getString("year"));

        return cv;
    }

    public static double updateChangedAmount(LoginProfile log, String refer, String staffid, String code, double amount, String type) throws Exception {

        double i = 0.0;
        
        String table = "";
        String codex = "";
        String applicable = "No";
        
        if(type.equals("Earning")){
            table = "em_payroll_earning_temp";
            codex = "earncode";
        }else if(type.equals("Deduction")){
            table = "em_payroll_deduction_temp";
            codex = "deductcode";
        }
        
        if(amount > 0){
            applicable = "Yes";
        }
        
        String q = ("UPDATE "+table+" SET amount = ?, guna = ? WHERE refer = '" + refer + "' AND staffid = '" + staffid + "' AND "+codex+" = '" + code + "'");
        try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
            ps.setDouble(1, amount);
            ps.setString(2, applicable);
            ps.executeUpdate();
            Logger.getLogger(EmPayrollDAO.class.getName()).log(Level.INFO, "5-------" + String.valueOf(ps));
            ps.close();
            
            i = amount;
        }

        return i;

    }
    
    public static int confirmSlip(LoginProfile log, String refer, String staffid) throws Exception {

        int i = 0;
        
        String table = "";
        
        
        String q = ("INSERT INTO em_payroll_staff(refer,year,period,staffid,payslip) values (?,?,?,?,?)");
        try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
            ps.setString(1, refer);
            ps.setString(2, EmPayrollDAO.getPayroll(log, refer).getYear());
            ps.setString(3, EmPayrollDAO.getPayroll(log, refer).getPeriod());
            ps.setString(4, staffid);
            ps.setBoolean(5, true);
            ps.executeUpdate();
            Logger.getLogger(EmPayrollDAO.class.getName()).log(Level.INFO, "5-------" + String.valueOf(ps));
            ps.close();
            
            i = 1;
        }

        return i;

    }

    public static int saveAllEarning(LoginProfile log, String refer) throws Exception {

        int i = 0;

        List<EmPayrollEarning> listAll = (List<EmPayrollEarning>) EmPayrollDAO.getAllEarningTemp(log, refer);

        for (EmPayrollEarning j : listAll) {
            
            if(j.getAmount() > 0){
                
                DataDAO.deleteDataTripleReferral(log, refer, j.getEarncode(), j.getStaffid(), "em_payroll_earning", "refer", "earncode", "staffid");
                EmPayrollDAO.saveEarning(log, "em_payroll_earning", j);
                
                i = 1;
            }

        }

        return i;

    }
    
    public static int saveAllDeduction(LoginProfile log, String refer) throws Exception {

        int i = 0;

        List<EmPayrollDeduction> listAll = (List<EmPayrollDeduction>) EmPayrollDAO.getAllDeductionTemp(log, refer);

        for (EmPayrollDeduction j : listAll) {
            
            if(j.getAmount() > 0){
                
                DataDAO.deleteDataTripleReferral(log, refer, j.getDeductcode(), j.getStaffid(), "em_payroll_deduction", "refer", "deductcode", "staffid");
                EmPayrollDAO.saveDeduction(log, "em_payroll_deduction", j);
                
                i = 1;
            }

        }

        return i;

    }
    
    private static void saveEarning(LoginProfile log, String table, EmPayrollEarning ep) throws SQLException{
        
        String q = ("INSERT INTO "+table+"(refer,year,period,staffid,earncode,earndesc,amount,staffname,epf,socso,basicflag,loclevel,daymth,flag,position,earntype,minwday,estatecode,estatename,stafftype,salary) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

                try (PreparedStatement ps = log.getCon().prepareStatement(q)) {

                    Logger.getLogger(EmPayrollDAO.class.getName()).log(Level.INFO, "4-------" + ep.getStaffid());
                    ps.setString(1, ep.getRefer());
                    ps.setString(2, ep.getYear());
                    ps.setString(3, ep.getPeriod());
                    ps.setString(4, ep.getStaffid());
                    ps.setString(5, ep.getEarncode());
                    ps.setString(6, ep.getEarndesc());
                    ps.setDouble(7, ep.getAmount());
                    ps.setString(8, ep.getStaffname());
                    ps.setString(9, ep.getEpf());
                    ps.setString(10, ep.getSocso());
                    ps.setString(11, ep.getBasicflag());
                    ps.setString(12, ep.getLoclevel());
                    ps.setString(13, ep.getDaymth());
                    ps.setString(14, ep.getFlag());
                    ps.setString(15, ep.getPosition());
                    ps.setString(16, ep.getEarntype());
                    ps.setString(17, ep.getMinwday());
                    ps.setString(18, ep.getEstatecode());
                    ps.setString(19, ep.getEstatename());
                    ps.setString(20, ep.getStafftype());
                    ps.setString(21, ep.getSalary());
                    ps.executeUpdate();

                }
        
    }
    
    private static void saveDeduction(LoginProfile log, String table, EmPayrollDeduction ep) throws SQLException{
        
        String q = ("INSERT INTO "+table+"(refer,year,period,staffid,deductcode,deductdesc,amount,staffname,epf,socso,basicflag,loclevel,daymth,flag,position,deducttype,minwday,estatecode,estatename,stafftype,salary) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

                try (PreparedStatement ps = log.getCon().prepareStatement(q)) {

                    Logger.getLogger(EmPayrollDAO.class.getName()).log(Level.INFO, "4-------" + ep.getStaffid());
                    ps.setString(1, ep.getRefer());
                    ps.setString(2, ep.getYear());
                    ps.setString(3, ep.getPeriod());
                    ps.setString(4, ep.getStaffid());
                    ps.setString(5, ep.getDeductcode());
                    ps.setString(6, ep.getDeductdesc());
                    ps.setDouble(7, ep.getAmount());
                    ps.setString(8, ep.getStaffname());
                    ps.setString(9, ep.getEpf());
                    ps.setString(10, ep.getSocso());
                    ps.setString(11, ep.getBasicflag());
                    ps.setString(12, ep.getLoclevel());
                    ps.setString(13, ep.getDaymth());
                    ps.setString(14, ep.getFlag());
                    ps.setString(15, ep.getPosition());
                    ps.setString(16, ep.getDeducttype());
                    ps.setString(17, ep.getMinwday());
                    ps.setString(18, ep.getEstatecode());
                    ps.setString(19, ep.getEstatename());
                    ps.setString(20, ep.getStafftype());
                    ps.setString(21, ep.getSalary());
                    ps.executeUpdate();

                }
        
    }
    
    public static List<EmPayrollDeduction> getDeductionEachPerson(LoginProfile log, String refer, String staffid) throws Exception, SQLException {

        List<EmPayrollDeduction> CV;
        CV = new ArrayList();

        ResultSet rs = null;

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM em_payroll_deduction WHERE refer = ? and staffid = ?");
            stmt.setString(1, refer);
            stmt.setString(2, staffid);
            rs = stmt.executeQuery();
            Logger.getLogger(EmPayrollDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(stmt));

            while (rs.next()) {
                CV.add(EmPayrollDAO.getEMPDeduction(rs));
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }
    public static List<EmPayrollEarning> getEarningEachPerson(LoginProfile log, String refer, String staffid) throws Exception, SQLException {

        List<EmPayrollEarning> CV;
        CV = new ArrayList();

        ResultSet rs = null;

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM em_payroll_earning WHERE refer = ? and staffid = ?");
            stmt.setString(1, refer);
            stmt.setString(2, staffid);
            rs = stmt.executeQuery();
            Logger.getLogger(EmPayrollDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(stmt));

            while (rs.next()) {
                CV.add(EmPayrollDAO.getEMPEarning(rs));
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }
    
    public static EmPayrollEarning getEarningEachPersonInfo(LoginProfile log, String refer, String staffid, String code) throws Exception, SQLException {

        EmPayrollEarning em =  new EmPayrollEarning();

        ResultSet rs = null;

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM em_payroll_earning WHERE refer = ? and staffid = ? and earncode = ?");
            stmt.setString(1, refer);
            stmt.setString(2, staffid);
            stmt.setString(3, code);
            rs = stmt.executeQuery();

            if (rs.next()) {
               em = EmPayrollDAO.getEMPEarning(rs);
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return em;
    }
    
    public static EmPayrollDeduction getDeductionEachPersonInfo(LoginProfile log, String refer, String staffid, String code) throws Exception, SQLException {

        EmPayrollDeduction em =  new EmPayrollDeduction();

        ResultSet rs = null;

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM em_payroll_deduction WHERE refer = ? and staffid = ? and deductcode = ?");
            stmt.setString(1, refer);
            stmt.setString(2, staffid);
            stmt.setString(3, code);
            rs = stmt.executeQuery();

            if (rs.next()) {
               em = EmPayrollDAO.getEMPDeduction(rs);
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return em;
    }
    
    public static EmPayrollMaster getPayrollMaster(LoginProfile log, String refer, String staffid) throws Exception{
        
        EmPayrollMaster em = new EmPayrollMaster();
        
        em.setGetStaff(StaffDAO.getHQStaff(log, staffid));
        em.setEmPayroll(EmPayrollDAO.getPayroll(log, refer));
        em.setListEarn(EmPayrollDAO.getEarningEachPerson(log, refer, staffid));
        em.setListDeduct(EmPayrollDAO.getDeductionEachPerson(log, refer, staffid));
        em.setConfirmSlip(EmPayrollDAO.getConfirmSlipStatus(log, refer, staffid));
        
        return em;
        
    }
    
    public static double getAmountSum(LoginProfile log, String refer, String staffid, String type, String range) throws Exception, SQLException {

        double amt = 0.0;
        
        String table = "";
        if(type.equals("earning")){
            table = "em_payroll_earning";
        }else if(type.equals("deduction")){
            table = "em_payroll_deduction";
        }
        
        String q = "";
        
        if(range.equals("todate")){
            q = "year = '"+AccountingPeriod.getCurYear(log)+"'";
        }else if(range.equals("thismonth")){
            q = "refer = '"+refer+"'";
        }

        ResultSet rs = null;

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT sum(amount) as amt FROM "+table+" WHERE "+q+" and staffid = ?");
            stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            Logger.getLogger(EmPayrollDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(stmt));

            while (rs.next()) {
                amt = rs.getDouble(1);
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return amt;
    }
    
    public static EmPaySheetList getPaysheet(LoginProfile log, String refer, int start, int limit) throws Exception{
        
        EmPaySheetList em = new EmPaySheetList();
        
        em.setStaffName(EmPayrollDAO.getList(log, "co_staff", "name", start, limit));
        em.setStaffID(EmPayrollDAO.getList(log, "co_staff", "staffid", start, limit));
        em.setStaffAll(EmPayrollDAO.getAllHQStaff(log, start, limit));
        em.setPageCount(EmPayrollDAO.countPagePaysheet(log,limit));
        em.setListEarn(EmPayrollDAO.getListPayrollType(log, "Earning", refer));
        em.setListDeduct(EmPayrollDAO.getListPayrollType(log, "Deduction", refer));
        em.setMaster(EmPayrollDAO.getPayroll(log, refer));
        
        return em;
        
    }
    
    public static List<String> getList(LoginProfile log, String table,String column, int start, int limit) throws Exception, SQLException {

        Statement stmt = null;
        List<String> CV;
        CV = new ArrayList();

        

        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from "+table+" limit "+start+","+limit+"");

            while (rs.next()) {
                CV.add(rs.getString(column));
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    

        return CV;
    }
    
    public static List<Staff> getAllHQStaff(LoginProfile log, int start, int limit) {

        ResultSet rs = null;
        List<Staff> CVi;
        CVi = new ArrayList();
        Staff c = new Staff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff a, co_staff_designation b where a.staffid = b.staffid and b.worklocation = 'Headquarters' order by a.name  limit "+start+","+limit+"");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                
                Staff st = (Staff) StaffDAO.getInfo(rs);
                st.setPposition(rs.getString("b.designation"));
                CVi.add(st);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static int countPagePaysheet(LoginProfile log, int limit) throws SQLException {

        int c = 0;

        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as tot from co_staff a, co_staff_designation b where a.staffid = b.staffid and b.worklocation = ?");

        stmt.setString(1, "Headquarters");
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = rs.getInt(1);
        }

        stmt.close();
        rs.close();
        
        int page = c / limit;
        int modulo = c % limit;
        int perPage = 0;

        if (modulo > 0) {
            page = page + 1;
        }

        return page;
    }
    
    public static List<EmPayrollType> getListPayrollType(LoginProfile log, String type, String refer) throws Exception, SQLException {

        ResultSet rs = null;
        List<EmPayrollType> CVi;
        CVi = new ArrayList();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from em_payroll_type where refer = ? and type = ?");
            stmt.setString(1, refer);
            stmt.setString(2, type);
            rs = stmt.executeQuery();
            while (rs.next()) {
                
                CVi.add(getEMPEarningType(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static boolean getConfirmSlipStatus(LoginProfile log, String refer, String staffid) throws Exception, SQLException {

        boolean b = false;
        ResultSet rs = null;

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM em_payroll_staff WHERE refer = ? and staffid = ?");
            stmt.setString(1, refer);
            stmt.setString(2, staffid);
            rs = stmt.executeQuery();
            Logger.getLogger(EmPayrollDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(stmt));

            if (rs.next()) {
                b = true;
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return b;
    }
    
    public static double calculateTax(LoginProfile log, String refer, String staffid) throws Exception{
        
        double amt = 0.0;
        EmPayrollPCB em = new EmPayrollPCB();
        EmPayrollPCBforP emP = new EmPayrollPCBforP();
        
        emP.setY(EmPayrollDAO.getCollectiveSalary(log, staffid));
        emP.setK(EmPayrollDAO.getCollectiveEPF(log, staffid));
        emP.setY1(EmPayrollDAO.getAllEarning(log, refer, staffid));
        emP.setK1(EmPayrollDAO.getDeductionEachPersonInfo(log, refer, staffid, "D0003").getAmount());
        emP.setY2(EmPayrollDAO.getAllEarning(log, refer, staffid));
        emP.setK2(EmPayrollDAO.getDeductionEachPersonInfo(log, refer, staffid, "D0003").getAmount());
        emP.setD(9000);
        emP.setS(EmPayrollDAO.getSpouseDeduction(log, staffid));
        emP.setN(12-Integer.parseInt(AccountingPeriod.getCurPeriod(log)));
        emP.setQ(2000);
        emP.setC(StaffDAO.getAllChild(log, staffid, true).size());
        
        double P1 = ((emP.getY() - emP.getK()) + (emP.getY1() - emP.getK1()) + ((emP.getY2() - emP.getK2()) * emP.getN()) + (emP.getYt() - emP.getKt()));
        double P2 = (emP.getD() + emP.getS() + emP.getDu() + emP.getSu() + (emP.getQ()*emP.getC()) + emP.getELP() + emP.getLP1());
        
        Logger.getLogger(EmPayrollDAO.class.getName()).log(Level.INFO, "---P1----(("+emP.getY() +"-"+ emP.getK()+") + ("+emP.getY1() +"-"+ emP.getK1()+") + (("+emP.getY2() +"-"+ emP.getK2()+") x "+ emP.getN()+") + ("+emP.getYt() +"-"+ emP.getKt()+"))");
        
        Logger.getLogger(EmPayrollDAO.class.getName()).log(Level.INFO, "---P2----("+emP.getD() +"+"+ emP.getS() +"+"+ emP.getDu() +"+"+ emP.getSu() +"+ ("+emP.getQ()+" x "+emP.getC()+") + "+ emP.getELP() +"+"+ emP.getLP1()+")");
                
        em.setP(P1 - P2);
        em.setM(amt);
        em.setR(amt);
        em.setB(amt);
        em.setZ(amt);
        em.setX(amt);
        em.setN(amt);
        
        
        return P1-P2;
    }
    
    private static double getCollectiveSalary(LoginProfile log, String staffid) throws Exception, SQLException {
        
        double b = 0.0;
        ResultSet rs = null;

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT sum(grosspay) as amt FROM em_payroll_staff WHERE staffid = ? and year = ?");
            stmt.setString(1, staffid);
            stmt.setString(2, AccountingPeriod.getCurYear(log));
            rs = stmt.executeQuery();
            Logger.getLogger(EmPayrollDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(stmt));

            if (rs.next()) {
                b = rs.getDouble(1);
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return b; 
        
    }
    
    private static double getCollectiveEPF(LoginProfile log, String staffid) throws Exception{
        
        double b = 0.0;
        ResultSet rs = null;

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT sum(amount) as amt FROM em_payroll_deduction WHERE staffid = ? and year = ? and period <> ? and deductcode = ?");
            stmt.setString(1, staffid);
            stmt.setString(2, AccountingPeriod.getCurYear(log));
            stmt.setString(3, AccountingPeriod.getCurPeriod(log));
            stmt.setString(4, "D0003");
            rs = stmt.executeQuery();
            Logger.getLogger(EmPayrollDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(stmt));

            if (rs.next()) {
                b = rs.getDouble(1);
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return b; 
        
    }
    
    private static double getAllEarning(LoginProfile log, String refer, String staffid) throws Exception{
        
        double b = 0.0;
        ResultSet rs = null;

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT sum(amount) as amt FROM em_payroll_earning WHERE staffid = ? and refer = ? and earncode in ('E0001','E0002','E0007')");
            stmt.setString(1, staffid);
            stmt.setString(2, refer);
            rs = stmt.executeQuery();
            Logger.getLogger(EmPayrollDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(stmt));

            if (rs.next()) {
                b = rs.getDouble(1);
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return b; 
        
    }
    
    private static double getSpouseDeduction(LoginProfile log, String staffid){
        double b = 0.0;
        ResultSet rs = null;
        
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT working FROM co_staff_spouse WHERE staffid = ?");
            stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            Logger.getLogger(EmPayrollDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(stmt));

            if (rs.next()) {
                if(rs.getString(1).equals("No")){
                    b = 4000.00;
                }
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return b;
        
    }
    
    public static TaxTable getRs(ResultSet rs) throws SQLException{
        
        TaxTable t = new TaxTable();
        
        t.setB13(rs.getDouble("B13"));
        t.setB2(rs.getDouble("B2"));
        t.setId(rs.getInt("id"));
        t.setM(rs.getDouble("M"));
        t.setPend(rs.getDouble("Pend"));
        t.setPstart(rs.getDouble("Pstart"));
        t.setR(rs.getDouble("R"));
        
        return t;
        
    }

}
