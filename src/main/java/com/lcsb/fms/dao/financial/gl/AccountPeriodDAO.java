/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.dao.financial.gl;

import com.lcsb.fms.model.financial.gl.AccountPeriod;
import com.lcsb.fms.model.financial.gl.Period;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class AccountPeriodDAO {
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("020101");
        mod.setModuleDesc("Accounting Period");
        mod.setMainTable("gl_altaccountingperiod");
        mod.setReferID_Master("id");
        return mod;
         
    }
    
    public static void saveData(LoginProfile log, AccountPeriod item) throws Exception{
        //List<ErrorPost> er = new ArrayList();
        
        int i = 0;
        List<Period> listAll = (List<Period>) item.getListPeriod();
        for (Period all : listAll) {
            i++;
            try{
                String q = ("insert into gl_altaccountingperiod(period,year,startperiod,endperiod,current,startyear,endyear,month,closedate) values (?,?,?,?,?,?,?,?,?)");
                PreparedStatement ps = log.getCon().prepareStatement(q);
                ps.setInt(1, all.getPeriod());
                ps.setString(2, item.getYear());
                ps.setString(3, all.getStartperiod());
                ps.setString(4, all.getEndperiod());
                ps.setString(5, "No");
                ps.setString(6, item.getStartyear());
                ps.setString(7, item.getEndyear());
                ps.setInt(8, all.getMonth());
                ps.setString(9, "0000-00-00");

                ps.executeUpdate();

                ps.close();
            
            } catch (SQLException e) {
                e.printStackTrace();
            }
            
        }
    }
    
    public static void deleteItem(LoginProfile log, String id) throws SQLException, Exception{
        
        String deleteQuery_2 = "delete from gl_altaccountingperiod where id = ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, id);
        ps_2.executeUpdate();
        ps_2.close();
        
        
    }
    
    public static void updateItem(LoginProfile log, AccountPeriod item) throws Exception{
        
        try{
            String q = ("UPDATE gl_altaccountingperiod set period = ?,year = ?,startperiod = ?,endperiod = ?,current = ?,startyear = ?,endyear = ?, month = ?, closedate = ? WHERE id = '"+item.getId()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setInt(1, item.getPeriod());
            ps.setString(2, item.getYear());
            ps.setString(3, item.getStartperiod());
            ps.setString(4, item.getEndperiod());
            ps.setString(5, item.getCurrent());
            ps.setString(6, item.getStartyear());
            ps.setString(7, item.getEndyear());
            ps.setString(8, item.getMonth());
            ps.setString(9, item.getClosedate());
           
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static AccountPeriod getInfo(LoginProfile log, String no) throws SQLException, Exception {
        AccountPeriod c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from gl_altaccountingperiod where id=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        rs.close();
        stmt.close();
        return c;
    }
    
    private static AccountPeriod getInfo(ResultSet rs) throws SQLException {
        AccountPeriod g = new AccountPeriod();
        g.setClosedate(rs.getString("closedate"));
        g.setCurrent(rs.getString("current"));
        g.setEndperiod(rs.getString("endperiod"));
        g.setEndyear(rs.getString("endyear"));
        g.setId(Integer.parseInt(rs.getString("id")));
        g.setMonth(rs.getString("month"));
        g.setPeriod(Integer.parseInt(rs.getString("period")));
        return g;
    }
    
    public static AccountPeriod getCurPeriod(LoginProfile log)throws SQLException, Exception {
        
        AccountPeriod p = new AccountPeriod();
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from gl_altaccountingperiod where current='Yes'");
        rs = stmt.executeQuery();
        if (rs.next()) {
            p.setStartperiod(rs.getString("startperiod"));
            p.setEndperiod(rs.getString("endperiod"));
            p.setYear(rs.getString("year"));
            p.setPeriod(rs.getInt("period"));
        }
        rs.close();
        stmt.close();
        return p;
    }
    
    public static AccountPeriod getPreviousPeriod(LoginProfile log)throws Exception {
        AccountPeriod cur = (AccountPeriod) AccountPeriodDAO.getCurPeriod(log);
        AccountPeriod pre = new AccountPeriod();
        
        if(cur.getPeriod()==1){
            pre.setPeriod(12);
            pre.setYear(String.valueOf(Integer.parseInt(cur.getYear())-1));
        }else{
            pre.setPeriod(cur.getPeriod()-1);
            pre.setYear(cur.getYear());
        }
        
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from gl_altaccountingperiod where year='"+pre.getYear()+"' and period = '"+pre.getPeriod()+"'");
        rs = stmt.executeQuery();
        if (rs.next()) {
            pre.setStartperiod(rs.getString("startperiod"));
            pre.setEndperiod(rs.getString("endperiod"));
            pre.setClosedate(rs.getString("closedate"));
        }
        
        rs.close();
        stmt.close();
        
        return pre;
    }
    
    public static int setNewPeriod(LoginProfile log, String year, String period) throws Exception{
        
        int success = 0;
        
        try{
            String q = ("UPDATE gl_altaccountingperiod set current = ?");
            String q1 = ("UPDATE gl_altaccountingperiod set current = ? WHERE period = '"+period+"' and year = '"+year+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, "No");
            
            PreparedStatement ps1 = log.getCon().prepareStatement(q1);
            ps1.setString(1, "Yes");
           
            ps.executeUpdate();
            ps.close();
            ps1.executeUpdate();
            ps1.close();
            
            success = 1;

        } catch (SQLException e) {
            e.printStackTrace();
            success = 0;
        }
        
        return success;
    }
     
    
}
