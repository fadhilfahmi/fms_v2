/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.tx;

import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.financial.tx.TxInput;
import com.lcsb.fms.model.financial.tx.TxInputDetail;
import com.lcsb.fms.model.financial.tx.TxInputDetailPK;
import com.lcsb.fms.model.financial.tx.TxVoucher;
import com.lcsb.fms.model.financial.tx.TxVoucherItem;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class TaxInputDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("021102");
        mod.setModuleDesc("Input Tax");
        mod.setMainTable("tx_input");
        mod.setReferID_Master("refno");
        return mod;

    }

    public static List<ListTable> tableList() {

        String column_names[] = {"refno", "name", "taxperiodfrom", "taxperiodto", "taxreturndate", "lockedby", "recon_refno"};
        String title_name[] = {"Refer", "Name", "From", "To", "Return", "App id", "Post"};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }

        return lt;
    }

   

    public static String saveMain(TxInput item, LoginProfile log) throws Exception {

        String newrefer = AutoGenerate.getReferenceNox(log, "refno", "tx_input", "TXI", log.getEstateCode(), String.valueOf(item.getYear()), String.valueOf(item.getPeriod()));

        try {
            String q = ("insert into tx_input(refno,period,year,loccode,locdesc,name,preparedate,prepareid,taxperiodfrom,taxperiodto,taxreturndate) values (?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, newrefer);
            ps.setInt(2, item.getPeriod());
            ps.setInt(3, item.getYear());
            ps.setString(4, log.getEstateCode());
            ps.setString(5, log.getEstateDescp());
            ps.setString(6, item.getName());
            ps.setDate(7, (item.getPreparedate() != null) ? new java.sql.Date(item.getPreparedate().getTime()) : null);
            ps.setString(8, log.getUserID());
            ps.setDate(9, (item.getTaxperiodfrom() != null) ? new java.sql.Date(item.getTaxperiodfrom().getTime()) : null);
            ps.setDate(10, (item.getTaxperiodto() != null) ? new java.sql.Date(item.getTaxperiodto().getTime()) : null);
            ps.setDate(11, (item.getTaxreturndate() != null) ? new java.sql.Date(item.getTaxreturndate().getTime()) : null);

            Logger.getLogger(TaxInputDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(ps));
            ps.executeUpdate();

            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newrefer;

    }

    public static List<TxVoucher> getAllVoucher() {

        List<TxVoucher> t;
        t = new ArrayList();

        TxVoucher ti = new TxVoucher();

        ti.setTrans("add");
        ti.setType("Invoice");
        ti.setDb("ap_inv");
        ti.setAbb("PNV");
        ti.setQuery("select b.invrefno as queue, date as Tarikh, a.invrefno as novoucher,  b.taxcode, c.descp, b.amount as amount,b.amount as amt ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, b.sacode, b.sadesc, a.estatecode as estatecode, a.estatename as estatename, left(a.invrefno,3) as kodVoucher,a.suppcode as compcode, a.suppname as compname, b.remark as remark   from ap_inv a inner join ap_inv_detail b on a.invrefno=b.no inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='PURCHASE' and (postflag='posted' or postflag='2') where a.postgst='No'");
        
        ti.setQuery1("select b.invrefno as queue, date as Tarikh, a.invrefno as novoucher,  b.taxcode, c.descp, b.amount as amount,b.amount as amt ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, b.sacode, b.sadesc, a.estatecode as estatecode, a.estatename as estatename, left(a.invrefno,3) as kodVoucher,a.suppcode as compcode, a.suppname as compname, b.remark as remark   from ap_inv a inner join ap_inv_detail b on a.invrefno=b.no inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='PURCHASE' and (postflag='posted' or postflag='2') where b.invrefno = ?");

        t.add(ti);
        
        TxVoucher ti1 = new TxVoucher();
        
        ti1.setTrans("add");
        ti1.setType("Payment Voucher");
        ti1.setDb("cb_payvoucher");
        ti1.setAbb("PVN");
        ti1.setQuery("select b.refer as queue, a.Tarikh, a.refer as novoucher, b.taxcode, c.descp, b.amtbeforetax as amount  ,b.taxamt as taxamt,b.amount as amt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, b.sacode as sacode, b.sadesc as sadesc, a.estatecode as estatecode, a.estatename as estatename, left(a.refer,3) as kodVoucher, a.paidcode as compcode, a.paidname as compname, b.remarks as remark  from cb_payvoucher a inner join cb_payvaucher_debit b on a.refer=b.refer inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='PURCHASE' where a.postgst='No' and post='posted'");
        
        ti1.setQuery1("select b.refer as queue, a.Tarikh, a.refer as novoucher, b.taxcode, c.descp, b.amtbeforetax as amount  ,b.taxamt as taxamt,b.amount as amt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, b.sacode as sacode, b.sadesc as sadesc, a.estatecode as estatecode, a.estatename as estatename, left(a.refer,3) as kodVoucher, a.paidcode as compcode, a.paidname as compname, b.remarks as remark  from cb_payvoucher a inner join cb_payvaucher_debit b on a.refer=b.refer inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='PURCHASE' where b.refer = ?");

        t.add(ti1);
        
        TxVoucher ti2 = new TxVoucher();
        
        ti2.setTrans("add");
        ti2.setType("Cash Voucher");
        ti2.setDb("cb_payvoucher");
        ti2.setAbb("CV");
        ti2.setQuery("select b.novoucher as queue, a.voucherdate as Tarikh, a.refer as novoucher, b.taxcode, c.descp, b.amtbeforetax as amount ,b.taxamt as taxamt,b.debit as amt, b.taxrate, b.taxcoacode,b.taxcoadescp, d.gstid, b.sacode, b.sadesc, b.loccode as estatecode, b.locname as estatename, left(a.refer,3) as kodVoucher , '00' as compcode, d.trade as compname, b.remarks as remark from cb_cashvoucher a inner join cb_receipt d on a.refer=d.refer inner join cb_cashvoucher_account b on a.refer=b.refer inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='PURCHASE' where a.postgst='No' and a.post='posted' group by b.novoucher");
        
        ti2.setQuery1("select b.novoucher as queue, a.voucherdate as Tarikh, a.refer as novoucher, b.taxcode, c.descp, b.amtbeforetax as amount ,b.taxamt as taxamt,b.debit as amt, b.taxrate, b.taxcoacode,b.taxcoadescp, d.gstid, b.sacode, b.sadesc, b.loccode as estatecode, b.locname as estatename, left(a.refer,3) as kodVoucher , '00' as compcode, d.trade as compname, b.remarks as remark from cb_cashvoucher a inner join cb_receipt d on a.refer=d.refer inner join cb_cashvoucher_account b on a.refer=b.refer inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='PURCHASE' where b.novoucher = ? group by b.novoucher");

        t.add(ti2);
        
        TxVoucher ti3 = new TxVoucher();
        
        ti3.setTrans("add");
        ti3.setType("Journal Voucher");
        ti3.setDb("gl_jv");
        ti3.setAbb("JVN");
        ti3.setQuery("select b.jvid as queue,a.jvdate as Tarikh, a.jvrefno as novoucher, b.taxcode, c.descp, b.amtbeforetax as amount ,(b.debit-b.credit) as taxamt,(b.debit-b.credit) as amt, b.taxrate, b.actcode,b.actdesc, b.gstid, b.sacode, b.sadesc, a.estatecode as estatecode, a.estatename as estatename, b.actcode as taxcoacode, b.actdesc as taxcoadescp, left(a.jvrefno,3) as kodVoucher, '00' as compcode, 'Not Applicable' as compname, b.remark as remark from gl_jv a inner join gl_jv_item b on a.jvrefno=b.jvrefno inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='PURCHASE' where a.postgst='No' and a.postflag='posted'");
        ti3.setQuery1("select b.jvid as queue,a.jvdate as Tarikh, a.jvrefno as novoucher, b.taxcode, c.descp, b.amtbeforetax as amount ,(b.debit-b.credit) as taxamt,(b.debit-b.credit) as amt, b.taxrate, b.actcode,b.actdesc, b.gstid, b.sacode, b.sadesc, a.estatecode as estatecode, a.estatename as estatename, b.actcode as taxcoacode, b.actdesc as taxcoadescp, left(a.jvrefno,3) as kodVoucher, '00' as compcode, 'Not Applicable' as compname, b.remark as remark from gl_jv a inner join gl_jv_item b on a.jvrefno=b.jvrefno inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='PURCHASE' where b.jvid = ?");

        t.add(ti3);
        
//        TxVoucher ti4 = new TxVoucher();
//        
//        ti4.setTrans("add");
//        ti4.setType("Contract");
//        ti4.setDb("co_big_f10");
//        ti4.setAbb("CPT");
//        ti4.setQuery("select b.refno as queue, a.date as Tarikh, a.refer as novoucher, b.taxcode, b.taxdescp as descp, b.amtmonth as amount, b.amtmonth as amt, b.taxamt as taxamt, b.taxrate,a.gstid,a.estatecode as estatecode, a.estatename as estatename, b.taxcoacode, b.taxcoadescp, d.sacode, d.sadesc, left(a.refer,3) as kodVoucher, a.code as compcode, a.nama as compname, b.remarks as remark  from co_big_f10 a inner join co_big_f10_work b on a.refer=b.no inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='PURCHASE' inner join co_stb_post d on a.refer=d.no where a.postgst='No' and a.post='posted' group by b.refer");
//        ti4.setQuery1("select b.refno as queue, a.date as Tarikh, a.refer as novoucher, b.taxcode, b.taxdescp as descp, b.amtmonth as amount, b.amtmonth as amt, b.taxamt as taxamt, b.taxrate,a.gstid,a.estatecode as estatecode, a.estatename as estatename, b.taxcoacode, b.taxcoadescp, d.sacode, d.sadesc, left(a.refer,3) as kodVoucher, a.code as compcode, a.nama as compname, b.remarks as remark  from co_big_f10 a inner join co_big_f10_work b on a.refer=b.no inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='PURCHASE' inner join co_stb_post d on a.refer=d.no where b.refno = ? group by b.refer");
//
//        t.add(ti4);
        
//        TxVoucher ti5 = new TxVoucher();
//        
//        ti5.setTypecode("04");
//        ti5.setType("Sales Credit Note");
//        ti5.setDb("ar_creditnote");
//        ti5.setQuery("select b.refer as queue, a.notedate as Tarikh, a.noteno as novoucher,  b.taxcode, c.descp, b.amount as amount ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, b.sacode, b.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.noteno,3) as kodVoucher, a.buyercode as compcode, a.buyername as compname, b.remarks as remark from ar_creditnote a inner join ar_creditnote_item b on a.noteno=b.noteno inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and a.postflag='posted'");
//        
//        ti5.setQuery1("select b.refer as queue, a.notedate as Tarikh, a.noteno as novoucher,  b.taxcode, c.descp, b.amount as amount ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, b.sacode, b.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.noteno,3) as kodVoucher, a.buyercode as compcode, a.buyername as compname, b.remarks as remark from ar_creditnote a inner join ar_creditnote_item b on a.noteno=b.noteno inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='PURCHASE' where b.refer = ?");
//
//        t.add(ti5);

        return t;

    }

    private static List<TxVoucherItem> getAllVoucherItem(LoginProfile log, TxVoucher t) throws Exception {
        
         List<TxVoucherItem> txi;
        txi = new ArrayList();

        TxVoucherItem tx = new TxVoucherItem();
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement(t.getQuery());
        //stmt.setString(1, sessionid);
        rs = stmt.executeQuery();
        while (rs.next()) {

            txi.add(getResult1(rs));
        }

        return txi;

    }
    
    private static TxVoucherItem getVoucherItem(LoginProfile log, String type, String refno) throws Exception {
        
        List<TxVoucher> tt = (List<TxVoucher>) getAllVoucher();
        TxVoucher ob = new TxVoucher();  
        
        for (int i = 0; i < tt.size(); i++) {
            if (tt.get(i).getType().equals(type)) {
              ob =  tt.get(i);
            }
        }
        
        TxVoucherItem tx = new TxVoucherItem();
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement(ob.getQuery1());
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        Logger.getLogger(TaxInputDAO.class.getName()).log(Level.INFO, "----$$--"+ String.valueOf(refno));
        while (rs.next()) {

            tx = getResult1(rs);
            tx.setType(type);
        }

        return tx;

    }

    private static TxVoucherItem getResult1(ResultSet rs) throws SQLException {

        TxVoucherItem t = new TxVoucherItem();

        t.setCompcode(rs.getString("compcode"));
        t.setCompname(rs.getString("compname"));
        t.setDate(rs.getDate("Tarikh"));
        t.setEstatecode(rs.getString("estatecode"));
        t.setEstatename(rs.getString("estatename"));
        t.setGstid(rs.getString("gstid"));
        t.setNoVoucher(rs.getString("noVoucher"));
        t.setKodVoucher2("kodVoucher");
        t.setQueue(rs.getString("queue"));
        t.setRemark(rs.getString("remark"));
        t.setTaxDescp(rs.getString("descp"));
        t.setTaxamt(rs.getDouble("taxamt"));
        t.setTaxcoacode(rs.getString("taxcoacode"));
        t.setTaxcoadescp(rs.getString("taxcoadescp"));
        t.setTaxrate(rs.getDouble("taxrate"));
        t.setTaxcode(rs.getString("taxcode"));
        t.setAmt(rs.getDouble("amt"));
     

        return t;

    }

    public static List<TxInputDetail> getAllTaxDetail(LoginProfile log, TxVoucher tv) throws Exception {

        List<TxInputDetail> txi;
        txi = new ArrayList();

        List<TxVoucherItem> listVoucher = (List<TxVoucherItem>) getAllVoucherItem(log, tv);

        for (TxVoucherItem j : listVoucher) {


            TxInputDetail ti = new TxInputDetail();
            TxInputDetailPK tp = new TxInputDetailPK();
            
            tp.setRefer("");
            tp.setRefno("");
            tp.setTaxcode(j.getTaxcode());
            tp.setVoucherno(j.getNoVoucher());

            ti.setAmount(j.getAmt());
            ti.setAmountCt(0);
            ti.setAmountDt(0);
            //ti.setBaddebt(baddebt);
            ti.setCompcode(j.getCompcode());
            ti.setCompname(j.getCompname());
            ti.setDate(j.getDate());
            ti.setEstcode(j.getEstatecode());
            ti.setEstname(j.getEstatename());
            ti.setGstid(j.getGstid());
            ti.setPeriod(0);
            ti.setRemarks(j.getRemark());
            ti.setTaxamt(j.getTaxamt());
            ti.setTaxamtCt(0);
            ti.setTaxamtDt(0);
            ti.setTaxcoacode(j.getTaxcoacode());
            ti.setTaxcoadescp(j.getTaxcoadescp());
            ti.setTaxdescp(j.getTaxDescp());
            //ti.setTaxperiodfrom(taxperiodfrom);
            //ti.setTaxperiodto(taxperiodto);
            ti.setTaxrate(j.getTaxrate());
            ti.setTrxtype(tv.getType());
            ti.setTxInputDetailPK(tp);
            ti.setVoucherrefer(j.getQueue());
            
            //ti.setYear(0);

            txi.add(ti);

        }

        return txi;
    }
    
    public static TxInputDetail getSelectedTaxDetail(LoginProfile log, String refer, String type) throws Exception {

        
            TxInputDetail ti = new TxInputDetail();
            TxInputDetailPK tp = new TxInputDetailPK();
            
            TxVoucherItem j = getVoucherItem(log, type,refer);
            
            tp.setRefer("");
            tp.setRefno("");
            tp.setTaxcode(j.getTaxcode());
            tp.setVoucherno(j.getNoVoucher());

            ti.setAmount(j.getAmt());
            ti.setAmountCt(0);
            ti.setAmountDt(0);
            //ti.setBaddebt(baddebt);
            ti.setCompcode(j.getCompcode());
            ti.setCompname(j.getCompname());
            ti.setDate(j.getDate());
            ti.setEstcode(j.getEstatecode());
            ti.setEstname(j.getEstatename());
            ti.setGstid(j.getGstid());
            ti.setPeriod(0);
            ti.setRemarks(j.getRemark());
            ti.setTaxamt(j.getTaxamt());
            ti.setTaxcoacode(j.getTaxcoacode());
            ti.setTaxcoadescp(j.getTaxcoadescp());
            ti.setTaxdescp(j.getTaxDescp());
            ti.setTaxrate(j.getTaxrate());
            ti.setTrxtype(j.getType());
            
            ti.setTxInputDetailPK(tp);
            ti.setVoucherrefer(j.getQueue());
           
                
                ti.setTaxamtDt(j.getTaxamt());
                ti.setAmountDt(j.getAmt());
		
		if (ti.getTaxamtDt() < 0) {
			ti.setTaxamtCt(-1*ti.getTaxamtDt());
			ti.setTaxamtDt(0);
			ti.setTaxamt(ti.getTaxamtCt());
			
			ti.setAmountCt(-1*ti.getAmountDt());
			ti.setAmountDt(0);
			ti.setAmount(ti.getAmountCt());
                        
		} 


        return ti;
    }
    
    public static TxInput getInput(LoginProfile log, String refno) throws SQLException, Exception {
        TxInput c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from tx_input where refno=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInput(rs);
        }

        
        return c;
    }
    
    public static TxInput getInput(ResultSet rs) throws SQLException{
        
        TxInput t = new TxInput();
        
        t.setApproveby(rs.getString("approveby"));
        t.setApprovedate(rs.getDate("approvedate"));
        t.setApprovedesignation(rs.getString("approvedesignation"));
        t.setApproveid(rs.getString("approveid"));
        t.setCheckedby(rs.getString("checkedby"));
        t.setCheckeddate(rs.getDate("checkeddate"));
        t.setCheckedid(rs.getString("checkedid"));
        t.setLoccode(rs.getString("loccode"));
        t.setLocdesc(rs.getString("locdesc"));
        t.setLockedby(rs.getString("lockedby"));
        t.setLockeddate(rs.getDate("lockeddate"));
        t.setName(rs.getString("name"));
        t.setPeriod(rs.getInt("period"));
        t.setPrepareby(rs.getString("prepareby"));
        t.setPreparedate(rs.getDate("preparedate"));
        t.setPrepareid(rs.getString("prepareid"));
        t.setReconRefno(rs.getString("recon_refno"));
        t.setRefno(rs.getString("refno"));
        t.setTaxperiodfrom(rs.getDate("taxperiodfrom"));
        t.setTaxperiodto(rs.getDate("taxperiodto"));
        t.setTaxreturndate(rs.getDate("taxreturndate"));
        t.setTotalAmt(rs.getDouble("total_amt"));
        t.setTotalTaxamt(rs.getDouble("total_taxamt"));
        t.setYear(rs.getInt("year"));
        
        return t;
    }
    
    public static String getStatus(LoginProfile log, String refer) throws Exception{
        
        boolean cek = false;
        String check = "";
        String app = "";
        String post = "";
        String status = "<span class=\"label label-primary\">Preparing</span>";
        ResultSet rs = null;
        
        PreparedStatement stmt = log.getCon().prepareStatement("select checkedid,approveid from tx_input where refno = ?");
        
        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            check = rs.getString(1);
            app = rs.getString(2);
        }
        
        stmt.close();
        rs.close();
        
        if(!check.equals("")){
            status = "Checked";
        }
        
        if(!app.equals("")){
            status = "Approved";
        }
        
        
        return status;
    }
    
    public static boolean isCheck(LoginProfile log, String refer) throws Exception{
        
        boolean cek = false;
        String ck = "";
        ResultSet rs = null;
        
        PreparedStatement stmt = log.getCon().prepareStatement("select checkedid from tx_input where refno = ?");
        
        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString(1);
        }
        
        if(!ck.equals("")){
            cek = true;
        }
        
        stmt.close();
        rs.close();
        
        return cek;
    }
    
    public static boolean isApprove(LoginProfile log, String refer) throws Exception{
        
        boolean cek = false;
        String ck = "";
        ResultSet rs = null;
        
        PreparedStatement stmt = log.getCon().prepareStatement("select approveid from tx_input where refno = ?");
        
        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString(1);
        }
        
        if(!ck.equals("")){
            cek = true;
        }
        
        stmt.close();
        rs.close();
        
        return cek;
    }
    
    public static boolean checkExistDetail(LoginProfile log, String refno) throws Exception{
        
        boolean b = false;
        ResultSet rs = null;
        
        PreparedStatement stmt = log.getCon().prepareStatement("select * from tx_input_detail where refer = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            b = true;
        }
        
        return b;
    }
    
    public static void saveDetail(LoginProfile log, TxInputDetail item, String refno) throws Exception {


        try {
            String q = ("insert into tx_input_detail(refno,period,year,compcode,compname,estcode,estname,gstid,remarks,taxamt,taxcoacode,taxcoadescp,taxdescp,trxtype,voucherrefer,amount,taxperiodfrom,taxperiodto,amount_ct,amount_dt,taxamt_ct,taxamt_dt,taxrate,date,refer,taxcode,voucherno) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, AutoGenerate.getReferAddBack(log, "tx_input_detail", "refno", refno, "refer"));
            ps.setInt(2, TaxInputDAO.getInput(log, refno).getPeriod());
            ps.setInt(3, TaxInputDAO.getInput(log, refno).getYear());
            ps.setString(4, item.getCompcode());
            ps.setString(5, item.getCompname());
            ps.setString(6, item.getEstcode());
            ps.setString(7, item.getEstname());
            ps.setString(8, item.getGstid());
            ps.setString(9, item.getRemarks());
            ps.setDouble(10, item.getTaxamt());
            ps.setString(11, item.getTaxcoacode());
            ps.setString(12, item.getTaxcoadescp());
            ps.setString(13, item.getTaxdescp());
            ps.setString(14, item.getTrxtype());
            ps.setString(15, item.getVoucherrefer());
            ps.setDouble(16, item.getAmount());
            ps.setDate(17, (TaxInputDAO.getInput(log, refno).getTaxperiodfrom()!= null) ? new java.sql.Date(TaxInputDAO.getInput(log, refno).getTaxperiodfrom().getTime()) : null);
            ps.setDate(18, (TaxInputDAO.getInput(log, refno).getTaxperiodto()!= null) ? new java.sql.Date(TaxInputDAO.getInput(log, refno).getTaxperiodto().getTime()) : null);
            ps.setDouble(19, item.getAmountCt());
            ps.setDouble(20, item.getAmountDt());
            ps.setDouble(21, item.getTaxamtCt());
            ps.setDouble(22, item.getTaxamtDt());
            ps.setDouble(23, item.getTaxrate());
            ps.setDate(24, (item.getDate()!= null) ? new java.sql.Date(item.getDate().getTime()) : null);
            ps.setString(25, refno);
            ps.setString(26, item.getTxInputDetailPK().getTaxcode());
            ps.setString(27, item.getTxInputDetailPK().getVoucherno());
            //Logger.getLogger(TaxInputDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(ps)Logger);
            ps.executeUpdate();

            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
    
    public static List<TxInputDetail> getAllTaxDetailSaved(LoginProfile log, String refer, String type) throws Exception {
        
        
        Statement stmt = null;
        List<TxInputDetail> CVi;
        CVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs;
            Logger.getLogger(TaxInputDAO.class.getName()).log(Level.INFO, "-------" + "select * from tx_input_detail where refer = '" + refer + "' and voucherno like '"+type+"%'");
            rs = stmt.executeQuery("select * from tx_input_detail where refer = '" + refer + "' and voucherno like '"+type+"%'");

            while (rs.next()) {
                CVi.add(getResultTaxDetail(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    private static TxInputDetail getResultTaxDetail(ResultSet rs) throws SQLException {
        TxInputDetail cv = new TxInputDetail();
        TxInputDetailPK pk = new TxInputDetailPK();
        
        pk.setRefer(rs.getString("refer"));
        pk.setRefno(rs.getString("refno"));
        pk.setTaxcode(rs.getString("taxcode"));
        pk.setVoucherno(rs.getString("voucherno"));

        cv.setAmount(rs.getDouble("amount"));
        cv.setAmountCt(rs.getDouble("amount_ct"));
        cv.setAmountDt(rs.getDouble("amount_dt"));
        cv.setBaddebt(rs.getString("baddebt"));
        cv.setCompcode(rs.getString("compcode"));
        cv.setCompname(rs.getString("compname"));
        cv.setDate(rs.getDate("date"));
        cv.setEstcode(rs.getString("estcode"));
        cv.setEstname(rs.getString("estname"));
        cv.setGstid(rs.getString("gstid"));
        cv.setPeriod(rs.getInt("period"));
        cv.setRemarks(rs.getString("remarks"));
        cv.setTaxamt(rs.getDouble("taxamt"));
        cv.setTaxamtCt(rs.getDouble("taxamt_ct"));
        cv.setTaxamtDt(rs.getDouble("taxamt_dt"));
        cv.setTaxcoacode(rs.getString("taxcoacode"));
        cv.setTaxcoadescp(rs.getString("taxcoadescp"));
        cv.setTaxdescp(rs.getString("taxdescp"));
        cv.setTaxperiodfrom(rs.getDate("taxperiodfrom"));
        cv.setTaxperiodto(rs.getDate("taxperiodto"));
        cv.setTaxrate(rs.getDouble("taxrate"));
        cv.setTrxtype(rs.getString("trxtype"));
        cv.setTxInputDetailPK(pk);
        cv.setVoucherrefer(rs.getString("voucherrefer"));
        cv.setYear(rs.getInt("year"));

        return cv;
    }
    
    public static void deleteTaxDetail(LoginProfile log, String refer) throws SQLException, Exception {
        
        String deleteQuery_2 = "delete from tx_input_detail where refer= ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, refer);
        ps_2.executeUpdate();
        ps_2.close();
        
        TaxInputDAO.updateTaxStatus(log, refer, "");
        
        
    }
    
    public static void updateTaxStatus(LoginProfile log, String refer, String status) throws Exception {
        
        String query = "UPDATE tx_input SET lockedby = ? where refno = '" + refer + "'";
        
        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.setString(1, status);
        ps.executeUpdate();
        ps.close();
        
    }

}
