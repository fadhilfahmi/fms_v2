/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.ar;

import com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.model.financial.ar.ArCreditNote;
import com.lcsb.fms.model.financial.ar.ArDebitNote;
import com.lcsb.fms.model.financial.ar.ArGLParam;
import com.lcsb.fms.model.financial.ar.ArReportAging;
import com.lcsb.fms.model.financial.ar.ArReportAgingParam;
import com.lcsb.fms.model.financial.ar.ArReportStatement;
import com.lcsb.fms.model.financial.ar.SalesInvoice;
import com.lcsb.fms.model.financial.cashbook.ORInvoice;
import com.lcsb.fms.model.financial.cashbook.OfficialReceipt;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;

/**
 *
 * @author fadhilfahmi
 */
public class ArReportDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("020910");
        mod.setModuleDesc("Sales Report");
        mod.setMainTable("");
        mod.setReferID_Master("");
        return mod;

    }

    public static List<ArReportStatement> getAllStatement(LoginProfile log, ArGLParam prm, int row, int limit, boolean report) throws Exception, SQLException {

        Statement stmt = null;
        List<ArReportStatement> CV;
        CV = new ArrayList();

        if (!report) {
            runStatement(log, prm, row, limit);
        }

        String q = "";

        if (limit != 0) {
            q = "limit " + row + "," + limit;
        }

        try {
            stmt = log.getCon().createStatement();
            //ResultSet rs = stmt.executeQuery("SELECT * FROM gl_posting_distribute WHERE sacode='" + prm.getBuyercode()+ "' AND (tarikh BETWEEN '"+prm.getStartDate()+"' AND '"+prm.getEndDate()+"') order by tarikh asc "+q);
            ResultSet rs = stmt.executeQuery("SELECT * FROM ar_report_statement order by date,id asc " + q);

            while (rs.next()) {

                ArReportStatement ar = new ArReportStatement();
                ar.setBalance(0.0);
                ar.setChequeno(rs.getString("chequeno"));
                ar.setCredit(rs.getDouble("credit"));
                ar.setDebit(rs.getDouble("debit"));
                ar.setRefno(rs.getString("refno"));
                ar.setType(rs.getString("type"));
                ar.setDate(rs.getString("date"));
                CV.add(ar);

            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static void runStatement(LoginProfile log, ArGLParam prm, int row, int limit) throws Exception, SQLException {

        Statement stmt = null;
        List<ArReportStatement> addrpt;
        addrpt = new ArrayList();

        deleteExist(log);
        getClosingBalance(log, prm);
        getInvoice(log, prm, row, limit);
        getOfficialReceipt(log, prm, row, limit);
        getDebitNote(log, prm, row, limit);
        getCreditNote(log, prm, row, limit);

    }

    private static void getClosingBalance(LoginProfile log, ArGLParam prm) throws SQLException, Exception {

        String year = prm.getStartDate().substring(0, 4);

        Logger.getLogger(ArReportDAO.class.getName()).log(Level.INFO, "-------op year----" + year);
        
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from gl_openingbalance where sacode=? and year=? and period=? and coacode = ?");
        stmt.setString(1, prm.getBuyercode());
        stmt.setString(2, year);
        stmt.setString(3, AccountingPeriod.getPeriodByDate(prm.getStartDate()));
        stmt.setString(4, "122101");
        Logger.getLogger(ArReportDAO.class.getName()).log(Level.INFO, "-----------" + stmt);
        rs = stmt.executeQuery();
        if (rs.next()) {

            ArReportStatement ar = new ArReportStatement();
            ar.setBalance(0.0);
            ar.setChequeno("N/A");
            ar.setDebit(rs.getDouble("debit"));
            ar.setCredit(rs.getDouble("credit"));
            ar.setRefno("Opening Balance");
            ar.setType("OP");
            ar.setDate(prm.getStartDate());
            saveReport(log, ar);
        }

        rs.close();

    }
    
    private static double getOpeningBalanceforAging(LoginProfile log, ArReportAgingParam prm) throws SQLException, Exception {

        double ob = 0.0;
        
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from gl_openingbalance where sacode=? and year=? and period=? and coacode = ?");
        stmt.setString(1, prm.getBuyerCode());
        stmt.setString(2, "2016");
        stmt.setString(3, "9");
        stmt.setString(4, "122101");
        Logger.getLogger(ArReportDAO.class.getName()).log(Level.INFO, "-----------" + stmt);
        rs = stmt.executeQuery();
        if (rs.next()) {
            
            double debit = rs.getDouble("debit");
            double credit = rs.getDouble("credit");
            
            if(debit > 0){
                ob = debit;
            }
            
            if(credit > 0){
                ob = credit * -1;
            }

        }

        rs.close();
        
        return ob;

    }
    
//    private static double getBalanceFromOpeningToDate(LoginProfile log, ArGLParam prm, int row, int limit) throws SQLException{
//        
//        double bal = 0.0;
//        
//        getInvoice(log, prm, row, limit);
//        getOfficialReceipt(log, prm, row, limit);
//        getDebitNote(log, prm, row, limit);
//        getCreditNote(log, prm, row, limit);
//        
//        ResultSet rs = null;
//        PreparedStatement stmt = log.getCon().prepareStatement("select * from gl_openingbalance where sacode=? and year=? and period=?");
//        stmt.setString(1, prm.getBuyercode());
//        stmt.setString(2, year);
//        stmt.setString(3, AccountingPeriod.getPeriodByDate(prm.getStartDate()));
//        Logger.getLogger(ArReportDAO.class.getName()).log(Level.INFO, "-----------" + stmt);
//        rs = stmt.executeQuery();
//        if (rs.next()) {
//
//            ArReportStatement ar = new ArReportStatement();
//            ar.setBalance(0.0);
//            ar.setChequeno("N/A");
//            ar.setDebit(rs.getDouble("debit"));
//            ar.setCredit(rs.getDouble("credit"));
//            ar.setRefno("Opening Balance");
//            ar.setType("OP");
//            ar.setDate(prm.getStartDate());
//            saveReport(log, ar);
//        }
//
//        rs.close();
//        
//        return bal;
//        
//    }

    private static void getInvoice(LoginProfile log, ArGLParam prm, int row, int limit) throws Exception, SQLException {

        Statement stmt = null;
        List<ArReportStatement> CV;
        CV = new ArrayList();

        String q = "";

        if (limit != 0) {
            q = "limit " + row + "," + limit;
        }

        try {
            stmt = log.getCon().createStatement();
            //ResultSet rs = stmt.executeQuery("SELECT * FROM gl_posting_distribute WHERE sacode='" + prm.getBuyercode()+ "' AND (tarikh BETWEEN '"+prm.getStartDate()+"' AND '"+prm.getEndDate()+"') order by tarikh asc "+q);
            ResultSet rs = stmt.executeQuery("SELECT * FROM sl_inv WHERE postflag = 'posted' AND bcode='" + prm.getBuyercode() + "' AND (invdate BETWEEN '" + prm.getStartDate() + "' AND '" + prm.getEndDate() + "') order by invdate asc " + q);

            while (rs.next()) {

                SalesInvoice opt = (SalesInvoice) SalesInvoiceDAO.getINV(rs);

                ArReportStatement ar = new ArReportStatement();
                ar.setBalance(0.0);
                ar.setChequeno("N/A");
                ar.setCredit(0.0);
                ar.setDebit(opt.getAmountno());
                ar.setRefno(opt.getInvref());
                ar.setType("AR");
                ar.setDate(opt.getInvdate());
                saveReport(log, ar);

            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static void getOfficialReceipt(LoginProfile log, ArGLParam prm, int row, int limit) throws Exception, SQLException {

        Statement stmt = null;
        List<ArReportStatement> CV;
        CV = new ArrayList();

        String q = "";

        if (limit != 0) {
            q = "limit " + row + "," + limit;
        }

        try {
            stmt = log.getCon().createStatement();
            //ResultSet rs = stmt.executeQuery("SELECT * FROM gl_posting_distribute WHERE sacode='" + prm.getBuyercode()+ "' AND (tarikh BETWEEN '"+prm.getStartDate()+"' AND '"+prm.getEndDate()+"') order by tarikh asc "+q);
            ResultSet rs = stmt.executeQuery("SELECT * FROM cb_official WHERE post = 'posted' AND  paidtype = 'Buyer' and paidcode='" + prm.getBuyercode() + "' AND (date BETWEEN '" + prm.getStartDate() + "' AND '" + prm.getEndDate() + "') order by date asc " + q);

            while (rs.next()) {

                OfficialReceipt opt = (OfficialReceipt) OfficialReceiptDAO.getINV(rs);

                ArReportStatement ar = new ArReportStatement();
                ar.setBalance(0.0);
                ar.setChequeno("N/A");
                ar.setCredit(opt.getAmount());
                ar.setDebit(0.0);
                ar.setRefno(opt.getRefer());
                ar.setType("CB");
                ar.setDate(opt.getDate());
                saveReport(log, ar);

            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static void getDebitNote(LoginProfile log, ArGLParam prm, int row, int limit) throws Exception, SQLException {

        Statement stmt = null;
        List<ArReportStatement> CV;
        CV = new ArrayList();

        String q = "";

        if (limit != 0) {
            q = "limit " + row + "," + limit;
        }

        try {
            stmt = log.getCon().createStatement();
            //ResultSet rs = stmt.executeQuery("SELECT * FROM gl_posting_distribute WHERE sacode='" + prm.getBuyercode()+ "' AND (tarikh BETWEEN '"+prm.getStartDate()+"' AND '"+prm.getEndDate()+"') order by tarikh asc "+q);
            ResultSet rs = stmt.executeQuery("SELECT * FROM ar_debitnote WHERE postflag = 'posted' AND buyercode='" + prm.getBuyercode() + "' AND (notedate BETWEEN '" + prm.getStartDate() + "' AND '" + prm.getEndDate() + "') order by notedate asc " + q);

            while (rs.next()) {

                ArDebitNote opt = (ArDebitNote) ArDebitNoteDAO.getMain(rs);

                ArReportStatement ar = new ArReportStatement();
                ar.setBalance(0.0);
                ar.setChequeno("N/A");
                ar.setCredit(0.0);
                ar.setDebit(opt.getTotal());
                ar.setRefno(opt.getNoteno());
                ar.setType("AR");
                ar.setDate(AccountingPeriod.convertDatetoString(opt.getNotedate()));
                saveReport(log, ar);

            }

            rs.close();
            stmt.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void getCreditNote(LoginProfile log, ArGLParam prm, int row, int limit) throws Exception, SQLException {

        Statement stmt = null;
        List<ArReportStatement> CV;
        CV = new ArrayList();

        String q = "";

        if (limit != 0) {
            q = "limit " + row + "," + limit;
        }

        try {
            stmt = log.getCon().createStatement();
            //ResultSet rs = stmt.executeQuery("SELECT * FROM gl_posting_distribute WHERE sacode='" + prm.getBuyercode()+ "' AND (tarikh BETWEEN '"+prm.getStartDate()+"' AND '"+prm.getEndDate()+"') order by tarikh asc "+q);
            ResultSet rs = stmt.executeQuery("SELECT * FROM ar_creditnote WHERE postflag = 'posted' AND buyercode='" + prm.getBuyercode() + "' AND (notedate BETWEEN '" + prm.getStartDate() + "' AND '" + prm.getEndDate() + "') order by notedate asc " + q);

            while (rs.next()) {

                ArCreditNote opt = (ArCreditNote) ArCreditNoteDAO.getMain(rs);

                ArReportStatement ar = new ArReportStatement();
                ar.setBalance(0.0);
                ar.setChequeno("N/A");
                ar.setCredit(opt.getTotal());
                ar.setDebit(0.0);
                ar.setRefno(opt.getNoteno());
                ar.setType("AR");
                ar.setDate(AccountingPeriod.convertDatetoString(opt.getNotedate()));
                saveReport(log, ar);

            }

            rs.close();
            stmt.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void saveReport(LoginProfile log, ArReportStatement item) throws Exception {

        try {
            String q = ("insert into ar_report_statement(date,type,refno,chequeno,debit,credit,balance) values (?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getDate());
            ps.setString(2, item.getType());
            ps.setString(3, item.getRefno());
            ps.setString(4, item.getChequeno());
            ps.setDouble(5, item.getDebit());
            ps.setDouble(6, item.getCredit());
            ps.setDouble(7, item.getBalance());
            ps.executeUpdate();

            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();

        }

    }

    public static void deleteExist(LoginProfile log) throws SQLException, Exception {

        String deleteQuery_1 = "delete from ar_report_statement";
        PreparedStatement ps_1 = log.getCon().prepareStatement(deleteQuery_1);
        ps_1.executeUpdate();
        ps_1.close();

    }

    public static List<ArReportAging> getAllAging(LoginProfile log, ArReportAgingParam prm, int row, int limit, boolean report) throws Exception, SQLException {

        Statement stmt = null;
        Statement stmt1 = null;
        List<ArReportAging> CV;
        CV = new ArrayList();

        if (!report) {
            //runStatement(prm, row, limit);
        }

        String q = "";

        if (limit != 0) {
            q = "limit " + row + "," + limit;
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM sl_inv where postflag = 'posted' group by bcode order by invdate asc " + q);

            while (rs.next()) {

                ArReportAging ar = new ArReportAging();
                ar.setBuyerCode(rs.getString("bcode"));
                ar.setBuyerName(rs.getString("bname"));

                double day1 = 0.0;
                double day2 = 0.0;
                double day3 = 0.0;
                double day4 = 0.0;
                double day5 = 0.0;

                stmt1 = log.getCon().createStatement();
                ResultSet rs1 = stmt1.executeQuery("SELECT * FROM sl_inv WHERE bcode = '" + rs.getString("bcode") + "' AND invdate <= '" + prm.getDate() + "' order by invdate asc " + q);

                while (rs1.next()) {
                    if (!checkORExist(log, rs1.getString("invref"))) {

                        Logger.getLogger(ArReportDAO.class.getName()).log(Level.INFO, "-------takde or----" + rs1.getString("invref"));
                        Logger.getLogger(ArReportDAO.class.getName()).log(Level.INFO, "invdate--" + rs1.getString("invdate")+"  paramdate--" + prm.getDate());
                        int days = getDayCount(rs1.getString("invdate"), prm.getDate());
                        double unpaid = rs1.getDouble("amountno") - rs1.getDouble("paid");

                        if (days <= 30) {
                            day1 += unpaid;
                        } else if (days > 30 && days < 61) {
                            day2 += unpaid;
                        } else if (days > 60 && days < 91) {
                            day3 += unpaid;
                        } else if (days > 90 && days < 121) {
                            day4 += unpaid;
                        } else if (days > 120) {
                            day5 += unpaid;
                        }

                    } else {

                        List<ORInvoice> listx = (List<ORInvoice>) checkDateFromOR(log, rs1.getString("invref"));//get list OR for invoice

                        for (ORInvoice j : listx) {

                            String date = OfficialReceiptDAO.getINV(log, j.getOrnno()).getDate();// get OR date

                            Date ornDate = AccountingPeriod.convertStringtoDate(date);
                            Date untilDate = AccountingPeriod.convertStringtoDate(prm.getDate());

                            if (ornDate.after(untilDate)) {//check OR date if exceed report parameter date

                                int days = getDayCount(rs1.getString("invdate"), prm.getDate());
                                double unpaid = rs1.getDouble("amountno") - j.getAmount();

                                Logger.getLogger(ArReportDAO.class.getName()).log(Level.INFO, "-------ada or----" + j.getOrnno() + "--tarikh or--" + ornDate + "--day--" + days);

                                if (days <= 30) {
                                    day1 += unpaid;
                                } else if (days > 30 && days < 61) {
                                    day2 += unpaid;
                                } else if (days > 60 && days < 91) {
                                    day3 += unpaid;
                                } else if (days > 90 && days < 121) {
                                    day4 += unpaid;
                                } else if (days > 120) {
                                    day5 += unpaid;
                                }

                            } else if (ornDate.before(untilDate)) {

//                                if (j.getAmount() != rs1.getDouble("amountno")) {
//
//                                    int days = getDayCount(rs1.getString("invdate"), prm.getDate());
//                                    double unpaid = rs1.getDouble("amountno") - j.getAmount();
//
//                                    if (days <= 30) {
//                                        day1 += unpaid;
//                                    } else if (days > 30 && days < 61) {
//                                        day2 += unpaid;
//                                    } else if (days > 60 && days < 91) {
//                                        day3 += unpaid;
//                                    } else if (days > 90 && days < 121) {
//                                        day4 += unpaid;
//                                    } else if (days > 120) {
//                                        day5 += unpaid;
//                                    }
//                                }
                            }

                        }

                    }

                    //ar.setTotal(rs1.getDouble("amountno") - rs1.getDouble("paid"));
                }

                ar.setDayCount1(day1);
                ar.setDayCount2(day2);
                ar.setDayCount3(day3);
                ar.setDayCount4(day4);
                ar.setDayCount5(day5);

                ar.setTotal(day1 + day2 + day3 + day4 + day5);

                CV.add(ar);

            }

            rs.close();
            stmt.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return CV;
    }

    //Logger.getLogger(ArReportDAO.class.getName()).log(Level.INFO, "-------ssss----" + rs.getString("bcode") + " - "+ rs1.getString("invdate") + " - " + AccountingPeriod.getCurrentTimeStamp() + "-" + getDayCount(rs1.getString("invdate"), AccountingPeriod.getCurrentTimeStamp()));
    private static int getDayCount(String startdate, String enddate) throws ParseException {

        DateTime dateTime1 = new DateTime(AccountingPeriod.convertStringtoDate(startdate));
        DateTime dateTime2 = new DateTime(AccountingPeriod.convertStringtoDate(enddate));
        int days = Days.daysBetween(dateTime1, dateTime2).getDays();
        return days;

    }

    private static boolean checkORExist(LoginProfile log, String invno) throws Exception {

        boolean c = false;
        ResultSet rs = null;
        String ornno = "";
        int i = 0;

        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_official_invoice where invno=?");
        stmt.setString(1, invno);
        rs = stmt.executeQuery();
        while (rs.next()) {
            i++;
            ornno = rs.getString("ornno");

        }

        if (i > 0) {
            c = true;
        }

        return c;

    }

    private static List<ORInvoice> checkDateFromOR(LoginProfile log, String invno) throws Exception {

        List<ORInvoice> ar = new ArrayList();

        boolean c = false;
        ResultSet rs = null;
        String ornno = "";

        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_official_invoice where invno= ? ");
        stmt.setString(1, invno);
        rs = stmt.executeQuery();
        while (rs.next()) {

            ORInvoice a = new ORInvoice();

            a.setAmount(rs.getDouble("amount"));
            a.setInvno(rs.getString("invno"));
            a.setOrnno(rs.getString("ornno"));

            ar.add(a);

        }

        return ar;

    }

}
