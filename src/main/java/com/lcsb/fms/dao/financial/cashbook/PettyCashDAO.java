/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.fms.dao.financial.cashbook;

import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.model.financial.cashbook.PettyCash;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.dao.DataDAO;
import com.lcsb.fms.util.model.Data;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class PettyCashDAO {
    
    
    
    public static void runPettyBook(LoginProfile log, int curyear, int curperiod) throws Exception{
        
        PettyCash.setCurrentPeriod(String.valueOf(curperiod));
        PettyCash.setCurrentYear(String.valueOf(curyear));
        PettyCash.setStartPeriod(AccountingPeriod.getStartofPeriod(log, PettyCash.getCurrentYear(), PettyCash.getCurrentPeriod()));
        PettyCash.setEndPeriod(AccountingPeriod.getEndofPeriod(log,PettyCash.getCurrentYear(), PettyCash.getCurrentPeriod()));
        deleteOldPettyCash(log);
        setPettyCOA(log);
        runPV(log);
        runJV(log);
        runCV(log);
        runOR(log);
        
    }
    
    private static void setCurrentYear(String y, String p){
        PettyCash.setCurrentPeriod(p);
        PettyCash.setCurrentYear(y);
    }
    
    private static void alignPeriod(int curyear,int period) throws Exception {
        int prePeriod = period - 1;
        
        if(prePeriod==0){
            curyear = curyear - 1;
        }
        
        PettyCash.setPeriod(String.valueOf(prePeriod));
        PettyCash.setYear(String.valueOf(curyear));
        
    }
   
    private static void deleteOldPettyCash(LoginProfile log) throws Exception{
        DataDAO.deleteAllData(log, "cb_cashtemp");
    }
    
    private static void setPettyCOA(LoginProfile log) throws Exception{
        String output = "";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from ce_manage where code = left('',2)");
        
        Logger.getLogger(PettyCashDAO.class.getName()).log(Level.INFO, String.valueOf(stmt));
        rs = stmt.executeQuery();
        if (rs.next()) {
            output = rs.getString("coa");
        }
        Logger.getLogger(PettyCashDAO.class.getName()).log(Level.INFO, "the output>>>>>>>>>"+output);
        PettyCash.setPettyCode(output);
    }
    
    private static void setOpeningBalance(){
        //ResultSet sod=sww.executeQuery("select * from gl_openingbalance where period = '"+period+"' and year = '"+thn+"' and coacode = '"+petty+"'");
	//if(sod.next())
	
	///		carry = sod.getDouble("debit")-sod.getDouble("credit");
//double hasil = carry;
    }
    
    private static void runPV(LoginProfile log) throws Exception{
        
        
        ResultSet rs = null;
        Connection con = log.getCon();
        PreparedStatement stmt = con.prepareStatement("select refer,tarikh,remarks from cb_payvoucher where (tarikh between '"+PettyCash.getStartPeriod()+"' and '"+PettyCash.getEndPeriod()+"') and post<>'cancel' order by tarikh");
        //stmt.setString(1, referenceno);
        
        Logger.getLogger(PettyCashDAO.class.getName()).log(Level.INFO, String.valueOf(stmt));
        rs = stmt.executeQuery();
        while (rs.next()) {
            //output = rs.getString(1);
            String refer = rs.getString(1);
            String tarikh = rs.getString(2);
            String remarks = rs.getString(3);
            String listPvRemark = "";
            String listPvCoa = "";
            
            Data dt = new Data();
            
            ResultSet rs1 = null;
            PreparedStatement stmt1 = con.prepareStatement("select * from cb_payvaucher_debit where voucer = ?");
            stmt1.setString(1, refer);
            
            rs1 = stmt1.executeQuery();
            while (rs1.next()) {
                String pvCoa = rs1.getString("coacode");
                String pvAmount = rs1.getString("amount");
                String pvRemark = rs1.getString("remarks");
                
                Logger.getLogger(PettyCashDAO.class.getName()).log(Level.INFO, refer+"--"+String.valueOf(pvCoa)+"---"+PettyCash.getPettyCode());
                
                if(pvCoa.equalsIgnoreCase(PettyCash.getPettyCode())){
                    
                    listPvCoa += pvCoa + ", ";
                    listPvRemark += pvRemark + ", ";
                    
                    insertData(log, PettyCash.getCurrentYear(),PettyCash.getCurrentPeriod(), tarikh, refer, listPvRemark, listPvCoa, pvAmount, "R");
                    
                }
            }
        }
    }
    
    private static void runJV(LoginProfile log) throws Exception{
        
        
        ResultSet rs = null;
        Connection con = log.getCon();
        PreparedStatement stmt = con.prepareStatement("select JVrefno,jvdate from gl_jv where (jvdate between '"+PettyCash.getStartPeriod()+"' and '"+PettyCash.getEndPeriod()+"') and postflag<>'cancel' and reflexcb='yes' order by jvdate");
        //stmt.setString(1, referenceno);
        
        Logger.getLogger(PettyCashDAO.class.getName()).log(Level.INFO, String.valueOf(stmt));
        rs = stmt.executeQuery();
        while (rs.next()) {
            //output = rs.getString(1);
            String refer = rs.getString(1);
            String tarikh = rs.getString(2);
            String remarks = "";
            String listPvRemark = "";
            String listPvCoa = "";
            
            Data dt = new Data();
            
            ResultSet rs1 = null;
            PreparedStatement stmt1 = con.prepareStatement("select * from gl_jv_item where JVrefno = ?");
            stmt1.setString(1, refer);
            
            rs1 = stmt1.executeQuery();
            while (rs1.next()) {
                String pvCoa = rs1.getString("actcode");
                String pvRemark = rs1.getString("remark");
                String pvamount = "";
		String symbolx="";
                
                 double amtx=0;
                    if(rs1.getDouble("debit")>0){
                        amtx=rs1.getDouble("debit");
                        pvamount = rs1.getString("debit");
                        symbolx="R";
                    }
                    else if(rs1.getDouble("credit")>0){
                        amtx=rs1.getDouble("credit");
                        pvamount = rs1.getString("credit");					 	
                        symbolx="E";
                    }
                Logger.getLogger(PettyCashDAO.class.getName()).log(Level.INFO, refer+"--"+String.valueOf(pvCoa)+"---"+PettyCash.getPettyCode());
                
                if(pvCoa.equalsIgnoreCase(PettyCash.getPettyCode())){
                    
                    listPvCoa += pvCoa + ", ";
                    listPvRemark += pvRemark + ", ";
                    
                    insertData(log, PettyCash.getCurrentYear(),PettyCash.getCurrentPeriod(), tarikh, refer, listPvRemark, listPvCoa, pvamount, symbolx);
                    
                }
            }
        }
    }
    
    private static void runCV(LoginProfile log) throws Exception{
        
        
        ResultSet rs = null;
        Connection con = log.getCon();
        PreparedStatement stmt = con.prepareStatement("select refer,voucherdate,total from cb_cashvoucher where (voucherdate between '"+PettyCash.getStartPeriod()+"' and '"+PettyCash.getEndPeriod()+"') and post<>'cancel' order by voucherdate");
        //stmt.setString(1, referenceno);
        
        Logger.getLogger(PettyCashDAO.class.getName()).log(Level.INFO, String.valueOf(stmt));
        rs = stmt.executeQuery();
        while (rs.next()) {
            //output = rs.getString(1);
            String refer = rs.getString(1);
            String tarikh = rs.getString(2);
            String cvAmount = rs.getString(3);
            String remarks = "";
            String listPvRemark = "";
            String listPvCoa = "";
            Logger.getLogger(PettyCashDAO.class.getName()).log(Level.INFO, refer+"--"+listPvRemark);
            Data dt = new Data();
            
            ResultSet rs1 = null;
            PreparedStatement stmt1 = con.prepareStatement("select * from cb_cashvoucher_account where refer = ?");
            stmt1.setString(1, refer);
            
            rs1 = stmt1.executeQuery();
            while (rs1.next()) {
                String pvCoa = rs1.getString("coacode");
                String pvCoaDescp = rs1.getString("coadescp");
                
                
                
                
                if(!pvCoa.equalsIgnoreCase("")){
                    
                    listPvCoa += pvCoa + ", ";
                    listPvRemark += pvCoaDescp + ", ";
                    
                   
                    
                }
            }
             insertData(log, PettyCash.getCurrentYear(),PettyCash.getCurrentPeriod(), tarikh, refer, listPvRemark, listPvCoa, cvAmount, "E");
        }
    }
    
    private static void runOR(LoginProfile log) throws Exception{
        
        
        ResultSet rs = null;
        Connection con = log.getCon();
        PreparedStatement stmt = con.prepareStatement("select refer,date from cb_official where (date between '"+PettyCash.getStartPeriod()+"' and '"+PettyCash.getEndPeriod()+"') and post<>'cancel' and bankname='PETTY CASH' order by date");
        //stmt.setString(1, referenceno);
        
        Logger.getLogger(PettyCashDAO.class.getName()).log(Level.INFO, String.valueOf(stmt));
        rs = stmt.executeQuery();
        while (rs.next()) {
            //output = rs.getString(1);
            String refer = rs.getString(1);
            String tarikh = rs.getString(2);
            String listPvRemark = "";
            String listPvCoa = "";
            
            Data dt = new Data();
            
            ResultSet rs1 = null;
            PreparedStatement stmt1 = con.prepareStatement("select * from cb_official_credit where voucherno = ?");
            stmt1.setString(1, refer);
            
            rs1 = stmt1.executeQuery();
            while (rs1.next()) {
                String pvCoa = rs1.getString("coacode");
                String pvAmount = rs1.getString("amount");
                String pvRemark = rs1.getString("remarks");
                
                Logger.getLogger(PettyCashDAO.class.getName()).log(Level.INFO, refer+"--"+String.valueOf(pvCoa)+"---"+PettyCash.getPettyCode());
                
                //if(pvCoa.equalsIgnoreCase(PettyCash.getPettyCode())){
                    
                listPvCoa += pvCoa + ", ";
                listPvRemark += pvRemark + ", ";
                    
                insertData(log, PettyCash.getCurrentYear(),PettyCash.getCurrentPeriod(), tarikh, refer, listPvRemark, listPvCoa, pvAmount, "R");
                    
                //}
            }
        }
    }
    
    private static void insertData(LoginProfile log, String y, String p, String date, String ID, String remarks, String listCoa, String pvAmount, String status) throws Exception{
        PreparedStatement ps = log.getCon().prepareStatement("INSERT INTO cb_cashtemp(year,period,pvdate,pvid,pvremarks,listcoa,pvamount,status) VALUES (?,?,?,?,?,?,?,?)");
        ps.setString(1, y);
        ps.setString(2, p);
        ps.setString(3, date);
        ps.setString(4, ID);
        ps.setString(5, remarks);
        ps.setString(6, listCoa);
        ps.setString(7, pvAmount);
        ps.setString(8, status);
        ps.executeUpdate();
        Logger.getLogger(PettyCashDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
        ps.close();
        
    }
    
    public static List<PettyCash> getAllPettyCash(LoginProfile log, String period, String year) throws Exception {

        Statement stmt = null;
        List<PettyCash> Pc;
        Pc = new ArrayList();
         Logger.getLogger(PettyCashDAO.class.getName()).log(Level.INFO, "select * from cb_cashtemp where period ='"+period+"' and year='"+year+"' order by pvdate, status desc,pvid");
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_cashtemp where period ='"+period+"' and year='"+year+"' order by pvdate, status desc,pvid");

            while (rs.next()) {
                Pc.add(getPettyCash(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        

        return Pc;
    }
    
    public static PettyCash getPettyCash(LoginProfile log, String code) throws SQLException, Exception {
        PettyCash c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_cashvoucher where refer=?");
        stmt.setString(1, code);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getPettyCash(rs);
        }

        
        return c;
    }
    
    private static PettyCash getPettyCash(ResultSet rs) throws SQLException {
        PettyCash pc = new PettyCash();
        pc.setPcYear(rs.getString("year"));
        pc.setPcPeriod(rs.getString("period"));
        pc.setPcPvDate(rs.getString("pvdate"));
        pc.setPcPvid(rs.getString("pvid"));
        pc.setPcPvremarks(rs.getString("pvremarks"));
        pc.setPvListcoa(rs.getString("listcoa"));
        pc.setPcPvamount(rs.getString("pvamount"));
        pc.setPvStatus(rs.getString("status"));
        
        return pc;
    }
    
   
    
}
