/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.fms.dao.financial.gl;


import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.model.financial.gl.Prepayment;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.Company;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class PrepaymentDAO {
     public static List<Prepayment> getListJournal(LoginProfile log) throws Exception {
        Logger.getLogger(PrepaymentDAO.class.getName()).log(Level.INFO, "ssssssssssssssssssssssssssssssssssssssssssss");
        Connection con = log.getCon();
        Statement stmt = null;
        List<Prepayment> Prep;
        Prep = new ArrayList();
        
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT *,sum(t1.amt) as cnt FROM gl_prepay_detail as t1, gl_prepay as t2 where t1.refer=t2.refer and t1.year<='"+AccountingPeriod.getCurYear(log)+"' and t1.period<="+AccountingPeriod.getCurPeriod(log)+" and postref='' and t2.estatecode like concat('','%') and t2.status<>'Not Active' group by t2.refer order by actcode");

            while (rs.next()) {
                Prep.add(getJournal(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        

        return Prep;
    }
    
    public static Prepayment getJournal(ResultSet rs) throws SQLException {
        Prepayment prep = new Prepayment();
        prep.setRefer(rs.getString("t1.refer"));
        prep.setCnt(rs.getString("cnt"));
        prep.setActcode(rs.getString("actcode"));
        prep.setActdesc(rs.getString("actdesc"));
        prep.setEstatecode(rs.getString("estatecode"));
        prep.setEstatename(rs.getString("estatename"));
        prep.setEstlevel(rs.getString("estlevel"));
        prep.setCtcode(rs.getString("ctcode"));
        prep.setCttype(rs.getString("cttype"));
        prep.setCtdesc(rs.getString("ctdesc"));
        prep.setActcode1(rs.getString("actcode1"));
        prep.setActdesc1(rs.getString("actdesc1"));
        prep.setLoccode(rs.getString("loccode"));
        prep.setLocdesc(rs.getString("locdesc"));
        prep.setLoclevel(rs.getString("loclevel"));
        prep.setDtctcode(rs.getString("dtctcode"));
        prep.setDtctdesc(rs.getString("dtctdesc"));
        prep.setDtcttype(rs.getString("dtcttype"));
        prep.setDtsatype(rs.getString("dtsatype"));
        prep.setDtsadesc(rs.getString("dtsadesc"));
        prep.setDtsacode(rs.getString("dtsacode"));
        prep.setSatype(rs.getString("satype"));
        prep.setSacode(rs.getString("sacode"));
        prep.setSadesc(rs.getString("sadesc"));
        prep.setRemark(rs.getString("remark"));
        return prep;
    }
    
    public static void saveJournal(){
        /*Connection con = ConnectionUtil.getConnection();
        String q = ("insert into sec_usrlog(userid,workerid,workername,estatecode,estatename,logintime,loginstatus,sessionid) values (?,?,?,?,?,?,?,?)");
        PreparedStatement ps = con.prepareStatement(q);
        ps.setString(1, LoginProfile.getUserName());
        ps.setString(2, LoginProfile.getUserID());
        ps.setString(3, LoginDAO.getStaffName());
        ps.setString(4, LoginProfile.getEstateCode());
        ps.setString(5, LoginProfile.getEstateDescp());
        ps.setString(6, String.valueOf(GeneralTerm.getCurrentDateTime()));
        ps.setString(7, "Success");
        ps.setString(8, sess);
        ps.executeUpdate();
        ps.close();*/
        
    }
    
    
}
