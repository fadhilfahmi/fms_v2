/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.gl;

import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.model.financial.gl.GLAccCredit;
import com.lcsb.fms.model.financial.gl.GLCreditNote;
import com.lcsb.fms.model.financial.gl.GLItemCreditNote;
import com.lcsb.fms.model.financial.gl.JournalVoucher;
import com.lcsb.fms.model.financial.gl.JournalVoucherItem;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class CreditNoteDAO {
    
    public static Module getModule(String type) {

        Module mod = new Module();

        if (type.equals("HQ")) {

            mod.setModuleID("020109");
            mod.setModuleDesc("Credit Note");
            mod.setMainTable("gl_creditnote");
            mod.setReferID_Master("noteno");

        } else if (type.equals("mill")) {

            mod.setModuleID("020120");
            mod.setModuleDesc("Credit Note Receive");
            mod.setMainTable("est_creditnote");
            mod.setReferID_Master("noteno");

        }

        return mod;

    }
    public static List<ListTable> tableList(String type) {
        
        List<ListTable> lt = new ArrayList();
    
        if (type.equals("HQ")) {

            String column_names[] = {"noteno", "notedate", "receivercode", "receivername", "total", "approveid", "postflag"};
            String title_name[] = {"Note No.", "Date", "Receiver Code", "Receiver Name", "Amount (RM)", "Check ID", " Approve ID", "Post"};

            

            for (int j = 0; j < column_names.length; j++) {
                ListTable i = new ListTable();
                i.setColumnName(column_names[j]);
                i.setTitleName(title_name[j]);
                lt.add(i);
            }
        }else if (type.equals("mill")) {
            String column_names[] = {"noteno", "notedate", "receivercode", "receivername", "total", "JVno", "postflag"};
            String title_name[] = {"Note No.", "Date", "Receiver Code", "Receiver Name", "Amount (RM)", "Check ID", " Approve ID", "Post"};

            

            for (int j = 0; j < column_names.length; j++) {
                ListTable i = new ListTable();
                i.setColumnName(column_names[j]);
                i.setTitleName(title_name[j]);
                lt.add(i);
            }
        }

        return lt;
    }
    
    public static GLCreditNote getCNE(LoginProfile log, String refno) throws SQLException, Exception {
        GLCreditNote c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from gl_creditnote where noteno=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getCNE(rs);
        }

        
        return c;
    }
    
    public static GLCreditNote getCNEest(LoginProfile log, String refno) throws SQLException, Exception {
        GLCreditNote c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from est_creditnote where noteno=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getCNEest(rs);
        }

        return c;
    }
    
    private static GLCreditNote getCNEest(ResultSet rs) throws SQLException {

        GLCreditNote i = new GLCreditNote();

        i.setAcccode(rs.getString("acccode"));
        i.setAccdesc(rs.getString("accdesc"));
        i.setApprovedate(rs.getString("approvedate"));
        i.setApprovedesig(rs.getString("approvedesig"));
        i.setApproveid(rs.getString("approveid"));
        i.setApprovename(rs.getString("approvename"));
        i.setEstcode(rs.getString("estcode"));
        i.setEstname(rs.getString("estname"));
        i.setNotedate(rs.getDate("notedate"));
        i.setNoteno(rs.getString("noteno"));
        i.setPeriod(rs.getString("period"));
        i.setPostdate(rs.getDate("postdate"));
        i.setPostflag(rs.getString("postflag"));
        i.setPreparedate(rs.getString("preparedate"));
        i.setPrepareid(rs.getString("prepareid"));
        i.setPreparename(rs.getString("preparename"));
        i.setReceivercode(rs.getString("receivercode"));
        i.setReceivername(rs.getString("receivername"));
        i.setRemark(rs.getString("remark"));
        i.setSacode(rs.getString("sacode"));
        i.setSadesc(rs.getString("sadesc"));
        i.setSatype(rs.getString("satype"));
        i.setTotal(rs.getDouble("total"));
        i.setYear(rs.getString("year"));
        i.setJVno(rs.getString("JVno"));

        return i;
    }
    
    private static GLCreditNote getCNEglEst(ResultSet rs) throws SQLException {

        GLCreditNote i = new GLCreditNote();

        i.setAcccode(rs.getString("acccode"));
        i.setAccdesc(rs.getString("accdesc"));
        i.setApprovedate(rs.getString("approvedate"));
        i.setApprovedesig(rs.getString("approvedesig"));
        i.setApproveid(rs.getString("approveid"));
        i.setApprovename(rs.getString("approvename"));
        i.setEstcode(rs.getString("estcode"));
        i.setEstname(rs.getString("estname"));
        i.setNotedate(rs.getDate("notedate"));
        i.setNoteno(rs.getString("noteno"));
        i.setPeriod(rs.getString("period"));
        i.setPostdate(rs.getDate("postdate"));
        i.setPostflag(rs.getString("postflag"));
        i.setPreparedate(rs.getString("preparedate"));
        i.setPrepareid(rs.getString("prepareid"));
        i.setPreparename(rs.getString("preparename"));
        i.setReceivercode(rs.getString("receivercode"));
        i.setReceivername(rs.getString("receivername"));
        i.setRemark(rs.getString("remark"));
        i.setSacode(rs.getString("sacode"));
        i.setSadesc(rs.getString("sadesc"));
        i.setSatype(rs.getString("satype"));
        i.setTotal(rs.getDouble("total"));
        i.setYear(rs.getString("year"));

        return i;
    }
    
    private static GLCreditNote getCNE(ResultSet rs) throws SQLException {
        
        GLCreditNote i = new GLCreditNote();
     
        i.setAcccode(rs.getString("acccode"));
        i.setAccdesc(rs.getString("accdesc"));
        i.setApprovedate(rs.getString("approvedate"));
        i.setApprovedesig(rs.getString("approvedesig"));
        i.setApproveid(rs.getString("approveid"));
        i.setApprovename(rs.getString("approvename"));
        i.setEstcode(rs.getString("estcode"));
        i.setEstname(rs.getString("estname"));
        i.setNotedate(rs.getDate("notedate"));
        i.setNoteno(rs.getString("noteno"));
        i.setPeriod(rs.getString("period"));
        i.setPostdate(rs.getDate("postdate"));
        i.setPostflag(rs.getString("postflag"));
        i.setPreparedate(rs.getString("preparedate"));
        i.setPrepareid(rs.getString("prepareid"));
        i.setPreparename(rs.getString("preparename"));
        i.setReceivercode(rs.getString("receivercode"));
        i.setReceivername(rs.getString("receivername"));
        i.setRemark(rs.getString("remark"));
        i.setSacode(rs.getString("sacode"));
        i.setSadesc(rs.getString("sadesc"));
        i.setSatype(rs.getString("satype"));
        i.setTotal(rs.getDouble("total"));
        i.setYear(rs.getString("year"));
        i.setRefernoteno(rs.getString("refernoteno"));
        
        return i;
    }
    
    public static String getStatus(LoginProfile log, String refer) throws Exception{
        
        boolean cek = false;
        String app = "";
        String post = "";
        String status = "<span class=\"label label-primary\">Preparing</span>";
        ResultSet rs = null;
        
        PreparedStatement stmt = log.getCon().prepareStatement("select approveid,postflag from gl_creditnote where noteno = ?");
        
        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            app = rs.getString(1);
            post = rs.getString(2);
        }
        
        stmt.close();
        rs.close();
        
        
        if(!app.equals("")){
            status = "Approved";
        }
        
        if(post.equals("Cancel")){
            status = "Posted";
        }
        
        return status;
    }
    
    public static GLItemCreditNote getCNEitem(LoginProfile log, String id) throws SQLException, Exception {
        GLItemCreditNote c = null;
        ResultSet rs = null;
        try{
            PreparedStatement stmt = log.getCon().prepareStatement("select * from gl_itemcreditnote where noteno=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getCNEitem(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        
        return c;
    }
    
    private static GLItemCreditNote getCNEitem(ResultSet rs) throws SQLException {
        GLItemCreditNote i = new GLItemCreditNote();
        
        i.setAmount(rs.getDouble("amount"));
        i.setDate(rs.getDate("date"));
        i.setDescp(rs.getString("descp"));
        i.setNoteno(rs.getString("noteno"));
        i.setQuantity(rs.getDouble("quantity"));
        i.setRefer(rs.getInt("refer"));
        i.setReference(rs.getString("reference"));
        i.setUnitprice(rs.getDouble("unitprice"));
        
        return i;
    }
    
    public static boolean isApprove(LoginProfile log, String refer) throws Exception{
        
        boolean cek = false;
        String ck = "";
        ResultSet rs = null;
        
        PreparedStatement stmt = log.getCon().prepareStatement("select approveid from gl_creditnote where noteno = ?");
        
        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString(1);
        }
        
        if(!ck.equals("")){
            cek = true;
        }
        
        stmt.close();
        rs.close();
        
        return cek;
    }
    
    public static List<GLItemCreditNote> getAllCNEItem(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<GLItemCreditNote> CVi;
        CVi = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from gl_itemcreditnote where noteno = '"+refno+"' order by refer");

            while (rs.next()) {
                CVi.add(getCNEitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static List<GLItemCreditNote> getAllCNEItemByPage(LoginProfile log, String refno, int row, int limit) throws Exception {

        Statement stmt = null;
        List<GLItemCreditNote> CVi;
        CVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from gl_itemcreditnote where noteno = '" + refno + "' order by refer limit "+row+","+limit);

            while (rs.next()) {
                CVi.add(getCNEitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static GLAccCredit getAccCredit(LoginProfile log, String id) throws SQLException, Exception {
        GLAccCredit c = null;
        ResultSet rs = null;
        
        try{
            PreparedStatement stmt = log.getCon().prepareStatement("select * from gl_acccredit where noteno=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getAccCredit(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        
        return c;
    }
    
    private static GLAccCredit getAccCredit(ResultSet rs) throws SQLException {
        GLAccCredit i = new GLAccCredit();
        
        i.setAcccode(rs.getString("acccode"));
        i.setAccdesc(rs.getString("accdesc"));
        i.setAmount(rs.getDouble("amount"));
        i.setLoccode(rs.getString("loccode"));
        i.setLocdesc(rs.getString("locdesc"));
        i.setLoclevel(rs.getString("loclevel"));
        i.setNoteno(rs.getString("noteno"));
        i.setRefer(rs.getInt("refer"));
        i.setRemark(rs.getString("remark"));
        i.setSacode(rs.getString("sacode"));
        i.setSadesc(rs.getString("sadesc"));
        i.setSatype(rs.getString("satype"));
        
        return i;
    }
    
    public static void approveVoucher(LoginProfile log, String refer, String staff_id, String staff_name, String position) throws Exception{
        
        String query = "UPDATE gl_creditnote SET approveid = '"+staff_id+"',approvename = '"+staff_name+"',approvedate='"+AccountingPeriod.getCurrentTimeStamp()+"', approvedesig = '"+position+"' where noteno = '"+refer+"'";
        
        Logger.getLogger(DebitNoteDAO.class.getName()).log(Level.INFO, "-------mmmmmmmmmmmmmmmmmmmmmmmmm----{0}", query);
        
        try {
            try (PreparedStatement ps = log.getCon().prepareStatement(query)) {
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static double getDebit(LoginProfile log, String refno) throws SQLException, Exception {
        double amount = 0;
        ResultSet rs = null;
        ResultSet rs1 = null;
        
        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(sum(amount),0) as sumamount from gl_itemcreditnote where noteno=?");
        
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amount = rs.getDouble(1);
        }

        return amount;
    }

    public static double getCredit(LoginProfile log, String refno) throws SQLException, Exception {
        double amt = 0;
        ResultSet rs = null;
        
        PreparedStatement stmt = log.getCon().prepareStatement("select amount from gl_acccredit where noteno=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amt = rs.getDouble(1);
        }

        return amt;
    }
    
    public static void deleteExist(LoginProfile log, String no) throws SQLException, Exception{
        Connection con = log.getCon();
        
        String deleteQuery_1 = "delete from gl_creditnote where noteno = ?";
        String deleteQuery_2 = "delete from gl_acccredit where noteno = ?";
        String deleteQuery_3 = "delete from gl_itemcreditnote where noteno = ?";
        PreparedStatement ps_1 = con.prepareStatement(deleteQuery_1);
        PreparedStatement ps_2 = con.prepareStatement(deleteQuery_2);
        PreparedStatement ps_3 = con.prepareStatement(deleteQuery_3);
        ps_1.setString(1, no);
        ps_2.setString(1, no);
        ps_3.setString(1, no);
        ps_1.executeUpdate();
        ps_2.executeUpdate();
        ps_3.executeUpdate();
        ps_1.close();
        ps_2.close();
        ps_3.close();
        
        LiveGLDAO.deleteFromGL(log, no);
    }
    
    public static String getStatusEst(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String app = "";
        String status = "<span class=\"label label-primary\">JV Ready</span>";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select JVno from est_creditnote where noteno = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            app = rs.getString(1);
        }

        stmt.close();
        rs.close();

        if (!app.equals("")) {
            status = "JV Created";
        }

        
        return status;
    }
    
    public static GLItemCreditNote getCNEitemEst(LoginProfile log, String id) throws SQLException, Exception {
        GLItemCreditNote c = null;
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from est_itemcreditnote where noteno=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getCNEitem(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }
    
    public static List<GLItemCreditNote> getAllCNEItemEst(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<GLItemCreditNote> CVi;
        CVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from est_itemcreditnote where noteno = '" + refno + "' order by refer");

            while (rs.next()) {
                CVi.add(getCNEitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static GLAccCredit getAccCreditEst(LoginProfile log, String id) throws SQLException, Exception {
        GLAccCredit c = null;
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from est_acccredit where noteno=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getAccCredit(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }
    
    public static String generateJV(String noteno, LoginProfile log) throws Exception{
        
        Connection con = log.getCon();
        String JVno = "";
        
        JournalVoucher jv = new JournalVoucher();
        
        jv.setCurperiod(AccountingPeriod.getCurPeriod(log));
        jv.setEstatecode(log.getEstateCode());
        jv.setEstatename(log.getEstateDescp());
        jv.setJVdate(AccountingPeriod.getCurrentTimeStamp());
        jv.setJVno("");
        jv.setJVrefno("");
        jv.setPreparedate(AccountingPeriod.getCurrentTimeStamp());
        jv.setPreparedbyid(log.getUserID());
        jv.setPreparedbyname(log.getFullname());
        jv.setReason(CreditNoteDAO.getCNEest(log, noteno).getRemark());
        jv.setReflexcb("No");
        jv.setTocredit(String.valueOf(CreditNoteDAO.getCNEest(log, noteno).getTotal()));
        jv.setTodebit(String.valueOf(CreditNoteDAO.getCNEest(log, noteno).getTotal()));
        jv.setYear(AccountingPeriod.getCurYear(log));
        
        String JVrefno = JournalVoucherDAO.saveJournalMain(log, jv);
        
        JournalVoucherItem jvi = new JournalVoucherItem();
        
        //Debit - Target
        jvi.setActcode(CreditNoteDAO.getAccCreditEst(log, noteno).getAcccode());
        jvi.setActdesc(CreditNoteDAO.getAccCreditEst(log, noteno).getAccdesc());
        jvi.setAmtbeforetax(0.0);
        jvi.setBalance(0.0);
        jvi.setCredit(CreditNoteDAO.getAccCreditEst(log, noteno).getAmount());
        jvi.setDebit(0.00);
        jvi.setGstid("");
        jvi.setJVrefno(JVrefno);
        jvi.setJvid("");
        jvi.setLoccode(log.getEstateCode());
        jvi.setLocdesc(log.getEstateDescp());
        jvi.setLoclevel("Company");
        jvi.setRemark(CreditNoteDAO.getCNEest(log, noteno).getRemark());
        jvi.setSacode("00");
        jvi.setSadesc("Not Applicable");
        jvi.setSatype("None");
        jvi.setTaxcode("None");
        jvi.setTaxdescp("Not Applicable");
        jvi.setTaxrate("0");
        
        JournalVoucherDAO.saveJournalItem(log, jvi);
        
        jvi.setActcode(CreditNoteDAO.getCNEest(log, noteno).getAcccode());
        jvi.setActdesc(CreditNoteDAO.getCNEest(log, noteno).getAccdesc());
        jvi.setAmtbeforetax(0.0);
        jvi.setBalance(0.0);
        jvi.setCredit(0.00);
        jvi.setDebit(CreditNoteDAO.getCNEest(log, noteno).getTotal());
        jvi.setGstid("");
        jvi.setJVrefno(JVrefno);
        jvi.setJvid("");
        jvi.setLoccode(log.getEstateCode());
        jvi.setLocdesc(log.getEstateDescp());
        jvi.setLoclevel("Company");
        jvi.setRemark(CreditNoteDAO.getCNEest(log, noteno).getRemark());
        jvi.setSacode("00");
        jvi.setSadesc("Not Applicable");
        jvi.setSatype("None");
        jvi.setTaxcode("None");
        jvi.setTaxdescp("Not Applicable");
        jvi.setTaxrate("0");
        
        JournalVoucherDAO.saveJournalItem(log, jvi);
        
        String query = "UPDATE est_creditnote SET JVno = '"+JVrefno+"' WHERE noteno = '"+noteno+"'";
        
        PreparedStatement ps = con.prepareStatement(query);
        ps.executeUpdate();
        ps.close();
        
        return JVno;
    }
    
    public static void updateEstCN(LoginProfile log, String JVrefno) throws Exception{
        
        String query = "UPDATE est_creditnote SET JVno = '' WHERE JVno = '"+JVrefno+"'";
        
        try (PreparedStatement ps = log.getCon().prepareStatement(query)) {
            ps.executeUpdate();
        }
        
    }
    
    public static int retrieveData(LoginProfile log, String no, String year, String period) throws Exception {

        Connection con = ConnectionUtil.createMillConnection(no);

        int i = 0;

        try {
            saveEstCN(log, con, year, period);
            i = 1;
        } catch (Exception e) {
            e.printStackTrace();
            i = 0;
        }

        con.close();
        return i;
    }

    private static void saveEstCN(LoginProfile log, Connection con, String year, String period) throws Exception {

        List<GLCreditNote> slist = (List<GLCreditNote>) getAllCNote(log, con, year, period);
        for (GLCreditNote c : slist) {
            
            deleteExistEst(log, c.getNoteno());
            
            saveMainCN(log, c);
            
            List<GLItemCreditNote> tlist = (List<GLItemCreditNote>) getAllCNItem(log, con, c.getNoteno());
            for (GLItemCreditNote d : tlist) {
                
                saveItemCredit(log, d);
                
            }
            
            List<GLAccCredit> ulist = (List<GLAccCredit>) getAllCNAcc(log, con, c.getNoteno());
            for (GLAccCredit e : ulist) {
                
                saveAccCredit(log, e);
                
            }

        }

    }
    
    public static List<GLCreditNote> getAllCNote(LoginProfile log, Connection con, String year, String period) throws Exception {

        Statement stmt = null;
        List<GLCreditNote> CVi;
        CVi = new ArrayList();

        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from gl_creditnote where year = '" + year + "' and period = '" + period + "' and postflag='Posted' order by noteno");

            while (rs.next()) {
                CVi.add(CreditNoteDAO.getCNEglEst(rs));
                
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static List<GLItemCreditNote> getAllCNItem(LoginProfile log, Connection con, String noteno) throws Exception {

        Statement stmt = null;
        List<GLItemCreditNote> CVi;
        CVi = new ArrayList();

        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from gl_itemcreditnote where noteno = '" + noteno + "'");

            while (rs.next()) {
                CVi.add(CreditNoteDAO.getCNEitem(rs));
                
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static List<GLAccCredit> getAllCNAcc(LoginProfile log, Connection con, String noteno) throws Exception {

        Statement stmt = null;
        List<GLAccCredit> CVi;
        CVi = new ArrayList();

        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from gl_acccredit where noteno = '" + noteno + "'");

            while (rs.next()) {
                CVi.add(CreditNoteDAO.getAccCredit(rs));
                
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static void saveMainCN(LoginProfile log, GLCreditNote item) throws Exception, SQLException {

        try {

            String q = ("insert into est_creditnote(acccode,accdesc,approvedate,approvedesig,approveid,approvename,estcode,estname,notedate,noteno,period,postflag,preparedate,prepareid,preparename,receivercode,receivername,remark,topost,year,postdate,total) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getAcccode());
            //ps.setDate(2, (item.getNotedate() != null) ? new java.sql.Date(item.getNotedate().getTime()) : null);
            ps.setString(2, item.getAccdesc());
            ps.setString(3, item.getApprovedate());
            ps.setString(4, item.getApprovedesig());
            ps.setString(5, item.getApproveid());
            ps.setString(6, item.getApprovename());
            ps.setString(7, item.getEstcode());
            ps.setString(8, item.getEstname());
            ps.setDate(9, (item.getNotedate() != null) ? new java.sql.Date(item.getNotedate().getTime()) : null);
            ps.setString(10, item.getNoteno());
            ps.setString(11, item.getPeriod());
            ps.setString(12, item.getPostflag());
            ps.setString(13, item.getPreparedate());
            ps.setString(14, item.getPrepareid());
            ps.setString(15, item.getPreparename());
            ps.setString(16, item.getReceivercode());
            ps.setString(17, item.getReceivername());
            ps.setString(18, item.getRemark());
            ps.setString(19, item.getTopost());
            ps.setString(20, item.getYear());
            ps.setDate(21, (item.getPostdate() != null) ? new java.sql.Date(item.getPostdate().getTime()) : null);
            ps.setDouble(22, item.getTotal());

            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void saveAccCredit(LoginProfile log, GLAccCredit item) throws Exception, SQLException {
       

        try {

            String q = ("insert into est_acccredit(noteno,acccode,accdesc,loclevel,loccode,locdesc,amount,remark) values (?,?,?,?,?,?,?,?)");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getNoteno());
            ps.setString(2, item.getAcccode());
            ps.setString(3, item.getAccdesc());
            ps.setString(4, item.getLoclevel());
            ps.setString(5, item.getLoccode());
            ps.setString(6, item.getLocdesc());
            ps.setDouble(7, item.getAmount());
            ps.setString(8, item.getRemark());

            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void saveItemCredit(LoginProfile log, GLItemCreditNote item) throws Exception, SQLException {

        try {

            String q = ("insert into est_itemcreditnote(noteno,descp,amount,reference,date,quantity,unitprice) values (?,?,?,?,?,?,?)");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getNoteno());
            ps.setString(2, item.getDescp());
            ps.setDouble(3, item.getAmount());
            ps.setString(4, item.getReference());
            ps.setDate(5, (item.getDate() != null) ? new java.sql.Date(item.getDate().getTime()) : null);
            ps.setDouble(6, item.getQuantity());
            ps.setDouble(7, item.getUnitprice());

            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
    public static void deleteExistEst(LoginProfile log, String no) throws SQLException, Exception {
        Connection con = log.getCon();

        String deleteQuery_1 = "delete from est_creditnote where noteno = ?";
        String deleteQuery_2 = "delete from est_acccredit where noteno = ?";
        String deleteQuery_3 = "delete from est_itemcreditnote where noteno = ?";
        PreparedStatement ps_1 = con.prepareStatement(deleteQuery_1);
        PreparedStatement ps_2 = con.prepareStatement(deleteQuery_2);
        PreparedStatement ps_3 = con.prepareStatement(deleteQuery_3);
        ps_1.setString(1, no);
        ps_2.setString(1, no);
        ps_3.setString(1, no);
        ps_1.executeUpdate();
        ps_2.executeUpdate();
        ps_3.executeUpdate();
        ps_1.close();
        ps_2.close();
        ps_3.close();

    }
    
}
