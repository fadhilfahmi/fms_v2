/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.tx;

import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.financial.tx.TxOutput;
import com.lcsb.fms.model.financial.tx.TxOutputDetail;
import com.lcsb.fms.model.financial.tx.TxOutputDetailPK;
import com.lcsb.fms.model.financial.tx.TxVoucher;
import com.lcsb.fms.model.financial.tx.TxVoucherItem;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class TaxOutputDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("021103");
        mod.setModuleDesc("Output Tax");
        mod.setMainTable("tx_output");
        mod.setReferID_Master("refno");
        return mod;

    }

    public static List<ListTable> tableList() {

        String column_names[] = {"refno", "name", "taxperiodfrom", "taxperiodto", "taxreturndate", "lockedby", "recon_refno"};
        String title_name[] = {"Refer", "Name", "From", "To", "Return", "App id", "Post"};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }

        return lt;
    }

    public static String saveMain(TxOutput item, LoginProfile log) throws Exception {
        
         String newrefer = AutoGenerate.getReferenceNox(log, "refno", "tx_input", "TXO", log.getEstateCode(), String.valueOf(item.getYear()), String.valueOf(item.getPeriod()));


        try {
            String q = ("insert into tx_output(refno,period,year,loccode,locdesc,name,preparedate,prepareid,taxperiodfrom,taxperiodto,taxreturndate) values (?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, newrefer);
            ps.setInt(2, item.getPeriod());
            ps.setInt(3, item.getYear());
            ps.setString(4, log.getEstateCode());
            ps.setString(5, log.getEstateDescp());
            ps.setString(6, item.getName());
            ps.setDate(7, (item.getPreparedate() != null) ? new java.sql.Date(item.getPreparedate().getTime()) : null);
            ps.setString(8, log.getUserID());
            ps.setDate(9, (item.getTaxperiodfrom() != null) ? new java.sql.Date(item.getTaxperiodfrom().getTime()) : null);
            ps.setDate(10, (item.getTaxperiodto() != null) ? new java.sql.Date(item.getTaxperiodto().getTime()) : null);
            ps.setDate(11, (item.getTaxreturndate() != null) ? new java.sql.Date(item.getTaxreturndate().getTime()) : null);

            Logger.getLogger(TaxOutputDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(ps));
            ps.executeUpdate();

            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newrefer;

    }

    public static List<TxVoucher> getAllVoucher() throws Exception {

        List<TxVoucher> t;
        t = new ArrayList();

        TxVoucher ti = new TxVoucher();

        ti.setTypecode("01");
        ti.setTrans("add");
        ti.setType("Sales Invoice");
        ti.setDb("sl_inv");
        ti.setAbb("INV");
        ti.setQuery("select b.refer as queue, invdate as Tarikh, a.invref as novoucher,  b.taxcode, c.descp, b.amount as amount ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, b.sacode, b.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.invref,3) as kodVoucher, a.bcode as compcode, a.bname as compname, b.remarks as remark from sl_inv a inner join sl_inv_item b on a.invref=b.ivref inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and a.postflag='posted'");

        ti.setQuery1("select b.refer as queue, invdate as Tarikh, a.invref as novoucher,  b.taxcode, c.descp, b.amount as amount ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, b.sacode, b.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.invref,3) as kodVoucher, a.bcode as compcode, a.bname as compname, b.remarks as remark from sl_inv a inner join sl_inv_item b on a.invref=b.ivref inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where b.refer = ?");

        t.add(ti);

        TxVoucher ti1 = new TxVoucher();

        ti1.setTypecode("02");
        ti1.setTrans("add");
        ti1.setType("Official Receipt");
        ti1.setDb("cb_official");
        ti1.setAbb("ORN");
        ti1.setQuery("select b.refer as queue, a.date as Tarikh, a.refer as novoucher, b.taxcode, c.descp, b.amount as amount ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, b.sacode, b.sadesc, a.estatecode as estatecode, a.estatename as estatename, left(a.refer,3) as kodVoucher,a.paidcode as compcode,  a.paidname as compname, b.remarks as remark  from cb_official a inner join cb_official_credit b on a.refer=b.voucherno inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and a.post='posted'");

        ti1.setQuery1("select b.refer as queue, a.date as Tarikh, a.refer as novoucher, b.taxcode, c.descp, b.amount as amount ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, b.sacode, b.sadesc, a.estatecode as estatecode, a.estatename as estatename, left(a.refer,3) as kodVoucher,a.paidcode as compcode,  a.paidname as compname, b.remarks as remark  from cb_official a inner join cb_official_credit b on a.refer=b.voucherno inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where b.refer = ?");

        t.add(ti1);

        TxVoucher ti2 = new TxVoucher();

        ti2.setTypecode("03");
        ti2.setTrans("add");
        ti2.setType("Journal Voucher");
        ti2.setDb("gl_jv");
        ti2.setAbb("JVN");
        ti2.setQuery("select b.jvid as queue, a.jvdate as Tarikh, a.jvrefno as novoucher, b.taxcode, c.descp, b.amtbeforetax as amount ,b.credit-b.debit as taxamt, b.taxrate, b.actcode,b.actdesc, b.gstid, b.sacode, b.sadesc, a.estatecode as estatecode, a.estatename as estatename, taxcoacode, taxcoadescp, left(a.jvrefno,3) as kodVoucher, '00' as compcode, 'Not Applicable' as compname, b.remark as remark from gl_jv a inner join gl_jv_item b on a.jvrefno=b.jvrefno inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and a.postflag='posted'");

        ti2.setQuery1("select b.jvid as queue, a.jvdate as Tarikh, a.jvrefno as novoucher, b.taxcode, c.descp, b.amtbeforetax as amount ,b.credit-b.debit as taxamt, b.taxrate, b.actcode,b.actdesc, b.gstid, b.sacode, b.sadesc, a.estatecode as estatecode, a.estatename as estatename, taxcoacode, taxcoadescp, left(a.jvrefno,3) as kodVoucher, '00' as compcode, 'Not Applicable' as compname, b.remark as remark from gl_jv a inner join gl_jv_item b on a.jvrefno=b.jvrefno inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where b.jvid = ?");

        t.add(ti2);

        TxVoucher ti3 = new TxVoucher();

        ti3.setTypecode("04");
        ti3.setTrans("add");
        ti3.setType("Sales Debit Note");
        ti3.setDb("ar_debitnote");
        ti3.setAbb("DNR");
        ti3.setQuery("select b.refer as queue, a.notedate as Tarikh, a.noteno as novoucher,  b.taxcode, c.descp, b.amount as amount ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, b.sacode, b.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.noteno,3) as kodVoucher, a.buyercode as compcode, a.buyername as compname, b.remarks as remark from ar_debitnote a inner join ar_debitnote_item b on a.noteno=b.noteno inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and a.postflag='posted'");

        ti3.setQuery1("select b.refer as queue, a.notedate as Tarikh, a.noteno as novoucher,  b.taxcode, c.descp, b.amount as amount ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, b.sacode, b.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.noteno,3) as kodVoucher, a.buyercode as compcode, a.buyername as compname, b.remarks as remark from ar_debitnote a inner join ar_debitnote_item b on a.noteno=b.noteno inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where b.refer = ?");

        t.add(ti3);

        //ti4 is missing!
        TxVoucher ti5 = new TxVoucher();

        ti5.setTypecode("05");
        ti5.setTrans("substract");
        ti5.setType("Sales Credit Note");
        ti5.setDb("ar_creditnote");
        ti5.setAbb("CNR");
        ti5.setQuery("select b.refer as queue, a.notedate as Tarikh, a.noteno as novoucher,  b.taxcode, c.descp, b.amount as amount ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, b.sacode, b.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.noteno,3) as kodVoucher, a.buyercode as compcode, a.buyername as compname, b.remarks as remark from ar_creditnote a inner join ar_creditnote_item b on a.noteno=b.noteno inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and a.postflag='posted'");

        ti5.setQuery1("select b.refer as queue, a.notedate as Tarikh, a.noteno as novoucher,  b.taxcode, c.descp, b.amount as amount ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, b.sacode, b.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.noteno,3) as kodVoucher, a.buyercode as compcode, a.buyername as compname, b.remarks as remark from ar_creditnote a inner join ar_creditnote_item b on a.noteno=b.noteno inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where b.refer = ?");

        t.add(ti5);

        return t;

    }

    private static List<TxVoucherItem> getAllVoucherItem(LoginProfile log, TxVoucher t) throws Exception {

        List<TxVoucherItem> txi;
        txi = new ArrayList();
        try {
            TxVoucherItem tx = new TxVoucherItem();
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement(t.getQuery());
            //stmt.setString(1, sessionid);
            rs = stmt.executeQuery();
            while (rs.next()) {

                txi.add(getResult1(rs));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            Logger.getLogger(TaxOutputDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(e));
        }

        return txi;

    }

    private static TxVoucherItem getVoucherItem(LoginProfile log, String type, String refno) throws Exception {

        List<TxVoucher> tt = (List<TxVoucher>) getAllVoucher();
        TxVoucher ob = new TxVoucher();

        for (int i = 0; i < tt.size(); i++) {
            if (tt.get(i).getType().equals(type)) {
                ob = tt.get(i);
            }
        }


        TxVoucherItem tx = new TxVoucherItem();
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement(ob.getQuery1());
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        Logger.getLogger(TaxInputDAO.class.getName()).log(Level.INFO, "----$$--" + String.valueOf(stmt));
        while (rs.next()) {

            tx = getResult1(rs);
            tx.setType(type);
        }

        return tx;

    }

    private static TxVoucherItem getResult1(ResultSet rs) throws SQLException {

        TxVoucherItem t = new TxVoucherItem();

        t.setCompcode(rs.getString("compcode"));
        t.setCompname(rs.getString("compname"));
        t.setDate(rs.getDate("Tarikh"));
        t.setEstatecode(rs.getString("estatecode"));
        t.setEstatename(rs.getString("estatename"));
        t.setGstid(rs.getString("gstid"));
        t.setNoVoucher(rs.getString("noVoucher"));
        t.setKodVoucher2("kodVoucher");
        t.setQueue(rs.getString("queue"));
        t.setRemark(rs.getString("remark"));
        t.setTaxDescp(rs.getString("descp"));
        t.setTaxamt(rs.getDouble("taxamt"));
        t.setTaxcoacode(rs.getString("taxcoacode"));
        t.setTaxcoadescp(rs.getString("taxcoadescp"));
        t.setTaxrate(rs.getDouble("taxrate"));
        t.setTaxcode(rs.getString("taxcode"));
        t.setAmt(rs.getDouble("amount"));

        return t;

    }

    public static List<TxOutputDetail> getAllTaxDetail(LoginProfile log, TxVoucher tv) throws Exception {

        List<TxOutputDetail> txi;
        txi = new ArrayList();

        List<TxVoucherItem> listVoucher = (List<TxVoucherItem>) getAllVoucherItem(log, tv);

        for (TxVoucherItem j : listVoucher) {

            TxOutputDetail ti = new TxOutputDetail();
            TxOutputDetailPK tp = new TxOutputDetailPK();

            tp.setRefer("");
            tp.setRefno("");
            tp.setTaxcode(j.getTaxcode());
            tp.setVoucherno(j.getNoVoucher());

            ti.setAmount(j.getAmt());
            ti.setAmountCt(0);
            ti.setAmountDt(0);
            //ti.setBaddebt(baddebt);
            ti.setCompcode(j.getCompcode());
            ti.setCompname(j.getCompname());
            ti.setDate(j.getDate());
            ti.setEstcode(j.getEstatecode());
            ti.setEstname(j.getEstatename());
            ti.setGstid(j.getGstid());
            ti.setPeriod(0);
            ti.setRemarks(j.getRemark());
            ti.setTaxamt(j.getTaxamt());
            ti.setTaxamtCt(0);
            ti.setTaxamtDt(0);
            ti.setTaxcoacode(j.getTaxcoacode());
            ti.setTaxcoadescp(j.getTaxcoadescp());
            ti.setTaxdescp(j.getTaxDescp());
            //ti.setTaxperiodfrom(taxperiodfrom);
            //ti.setTaxperiodto(taxperiodto);
            ti.setTaxrate(j.getTaxrate());
            ti.setTrxtype(tv.getType());
            ti.setTxOutputDetailPK(tp);
            ti.setVoucherrefer(j.getQueue());

            //ti.setYear(0);
            txi.add(ti);

        }

        return txi;
    }

    public static TxOutputDetail getSelectedTaxDetail(LoginProfile log, String refer, String type) throws Exception {

        TxOutputDetail ti = new TxOutputDetail();
        TxOutputDetailPK tp = new TxOutputDetailPK();

        TxVoucherItem j = getVoucherItem(log, type, refer);

        tp.setRefer("");
        tp.setRefno("");
        tp.setTaxcode(j.getTaxcode());
        tp.setVoucherno(j.getNoVoucher());

        ti.setAmount(j.getAmt());
        ti.setAmountCt(0);
        ti.setAmountDt(0);
        //ti.setBaddebt(baddebt);
        ti.setCompcode(j.getCompcode());
        ti.setCompname(j.getCompname());
        ti.setDate(j.getDate());
        ti.setEstcode(j.getEstatecode());
        ti.setEstname(j.getEstatename());
        ti.setGstid(j.getGstid());
        ti.setPeriod(0);
        ti.setRemarks(j.getRemark());
        ti.setTaxamt(j.getTaxamt());
        ti.setTaxcoacode(j.getTaxcoacode());
        ti.setTaxcoadescp(j.getTaxcoadescp());
        ti.setTaxdescp(j.getTaxDescp());
        ti.setTaxrate(j.getTaxrate());
        ti.setTrxtype(j.getType());

        ti.setTxOutputDetailPK(tp);
        ti.setVoucherrefer(j.getQueue());

        //ti.setTaxamtCt(j.getTaxamt());
        //ti.setAmountCt(j.getAmt());
        if (TaxReconcileDAO.getMinusOrAdd(j.getQueue()).equals("-")) {
            //j.setTaxamt(-1 * j.getTaxamt());

            ti.setTaxamtDt(j.getTaxamt());
            ti.setTaxamtCt(0);
            //ti.setTaxamt(j.getTaxamt());

            ti.setAmountDt(j.getAmt());
            ti.setAmountCt(0);
            //ti.setAmount(j.getAmt());

        } else {
            ti.setTaxamtCt(j.getTaxamt());
            ti.setTaxamtDt(0);
            //ti.setTaxamt(j.getTaxamt());

            ti.setAmountCt(j.getAmt());
            ti.setAmountDt(0);

        }

//        if (ti.getTaxamtCt() < 0) {
//            ti.setTaxamtDt(-1 * ti.getTaxamtCt());
//            ti.setTaxamtCt(0);
//            ti.setTaxamt(ti.getTaxamtCt());
//
//            ti.setAmountDt(-1 * ti.getAmountCt());
//            ti.setAmountCt(0);
//            ti.setAmount(ti.getAmountDt());
//
//        }
        return ti;
    }

    public static TxOutput getOutput(LoginProfile log, String refno) throws SQLException, Exception {
        TxOutput c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from tx_output where refno=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getOutput(rs);
        }

        return c;
    }

    public static TxOutput getOutput(ResultSet rs) throws SQLException {

        TxOutput t = new TxOutput();

        t.setApproveby(rs.getString("approveby"));
        t.setApprovedate(rs.getDate("approvedate"));
        t.setApprovedesignation(rs.getString("approvedesignation"));
        t.setApproveid(rs.getString("approveid"));
        t.setCheckedby(rs.getString("checkedby"));
        t.setCheckeddate(rs.getDate("checkeddate"));
        t.setCheckedid(rs.getString("checkedid"));
        t.setLoccode(rs.getString("loccode"));
        t.setLocdesc(rs.getString("locdesc"));
        t.setLockedby(rs.getString("lockedby"));
        t.setLockeddate(rs.getDate("lockeddate"));
        t.setName(rs.getString("name"));
        t.setPeriod(rs.getInt("period"));
        t.setPrepareby(rs.getString("prepareby"));
        t.setPreparedate(rs.getDate("preparedate"));
        t.setPrepareid(rs.getString("prepareid"));
        t.setReconRefno(rs.getString("recon_refno"));
        t.setRefno(rs.getString("refno"));
        t.setTaxperiodfrom(rs.getDate("taxperiodfrom"));
        t.setTaxperiodto(rs.getDate("taxperiodto"));
        t.setTaxreturndate(rs.getDate("taxreturndate"));
        t.setTotalAmt(rs.getDouble("total_amt"));
        t.setTotalTaxamt(rs.getDouble("total_taxamt"));
        t.setYear(rs.getInt("year"));

        return t;
    }

    public static String getStatus(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String check = "";
        String app = "";
        String post = "";
        String status = "<span class=\"label label-primary\">Preparing</span>";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select checkedid,approveid from tx_output where refno = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            check = rs.getString(1);
            app = rs.getString(2);
        }

        stmt.close();
        rs.close();

        if (!check.equals("")) {
            status = "Checked";
        }

        if (!app.equals("")) {
            status = "Approved";
        }

        return status;
    }

    public static boolean isCheck(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select checkedid from tx_output where refno = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString(1);
        }

        if (!ck.equals("")) {
            cek = true;
        }

        stmt.close();
        rs.close();

        return cek;
    }

    public static boolean isApprove(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select approveid from tx_output where refno = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString(1);
        }

        if (!ck.equals("")) {
            cek = true;
        }

        stmt.close();
        rs.close();

        return cek;
    }

    public static boolean checkExistDetail(LoginProfile log, String refno) throws Exception {

        boolean b = false;
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select * from tx_output_detail where refer = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            b = true;
        }

        return b;
    }

    public static void saveDetail(LoginProfile log, TxOutputDetail item, String refno) throws Exception {


        try {
            String q = ("insert into tx_output_detail(refno,period,year,compcode,compname,estcode,estname,gstid,remarks,taxamt,taxcoacode,taxcoadescp,taxdescp,trxtype,voucherrefer,amount,taxperiodfrom,taxperiodto,amount_ct,amount_dt,taxamt_ct,taxamt_dt,taxrate,date,refer,taxcode,voucherno) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, AutoGenerate.getReferAddBack(log,"tx_output_detail", "refno", refno, "refer"));
            ps.setInt(2, TaxOutputDAO.getOutput(log, refno).getPeriod());
            ps.setInt(3, TaxOutputDAO.getOutput(log, refno).getYear());
            ps.setString(4, item.getCompcode());
            ps.setString(5, item.getCompname());
            ps.setString(6, item.getEstcode());
            ps.setString(7, item.getEstname());
            ps.setString(8, item.getGstid());
            ps.setString(9, item.getRemarks());
            ps.setDouble(10, item.getTaxamt());
            ps.setString(11, item.getTaxcoacode());
            ps.setString(12, item.getTaxcoadescp());
            ps.setString(13, item.getTaxdescp());
            ps.setString(14, item.getTrxtype());
            ps.setString(15, item.getVoucherrefer());
            ps.setDouble(16, item.getAmount());
            ps.setDate(17, (TaxOutputDAO.getOutput(log, refno).getTaxperiodfrom() != null) ? new java.sql.Date(TaxOutputDAO.getOutput(log, refno).getTaxperiodfrom().getTime()) : null);
            ps.setDate(18, (TaxOutputDAO.getOutput(log, refno).getTaxperiodto() != null) ? new java.sql.Date(TaxOutputDAO.getOutput(log, refno).getTaxperiodto().getTime()) : null);
            ps.setDouble(19, item.getAmountCt());
            ps.setDouble(20, item.getAmountDt());
            ps.setDouble(21, item.getTaxamtCt());
            ps.setDouble(22, item.getTaxamtDt());
            ps.setDouble(23, item.getTaxrate());
            ps.setDate(24, (item.getDate() != null) ? new java.sql.Date(item.getDate().getTime()) : null);
            ps.setString(25, refno);
            ps.setString(26, item.getTxOutputDetailPK().getTaxcode());
            ps.setString(27, item.getTxOutputDetailPK().getVoucherno());
            Logger.getLogger(TaxInputDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(ps));
            ps.executeUpdate();

            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
    public static List<TxOutputDetail> getAllTaxDetailSaved(LoginProfile log, String refer, String type) throws Exception {
        
        
        Statement stmt = null;
        List<TxOutputDetail> CVi;
        CVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs;
            Logger.getLogger(TaxInputDAO.class.getName()).log(Level.INFO, "-------" + "select * from tx_output_detail where refer = '" + refer + "' and voucherno like '"+type+"%'");
            rs = stmt.executeQuery("select * from tx_output_detail where refer = '" + refer + "' and voucherno like '"+type+"%'");

            while (rs.next()) {
                CVi.add(getResultTaxDetail(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    private static TxOutputDetail getResultTaxDetail(ResultSet rs) throws SQLException {
        TxOutputDetail cv = new TxOutputDetail();
        TxOutputDetailPK pk = new TxOutputDetailPK();
        
        pk.setRefer(rs.getString("refer"));
        pk.setRefno(rs.getString("refno"));
        pk.setTaxcode(rs.getString("taxcode"));
        pk.setVoucherno(rs.getString("voucherno"));

        cv.setAmount(rs.getDouble("amount"));
        cv.setAmountCt(rs.getDouble("amount_ct"));
        cv.setAmountDt(rs.getDouble("amount_dt"));
        cv.setBaddebt(rs.getString("baddebt"));
        cv.setCompcode(rs.getString("compcode"));
        cv.setCompname(rs.getString("compname"));
        cv.setDate(rs.getDate("date"));
        cv.setEstcode(rs.getString("estcode"));
        cv.setEstname(rs.getString("estname"));
        cv.setGstid(rs.getString("gstid"));
        cv.setPeriod(rs.getInt("period"));
        cv.setRemarks(rs.getString("remarks"));
        cv.setTaxamt(rs.getDouble("taxamt"));
        cv.setTaxamtCt(rs.getDouble("taxamt_ct"));
        cv.setTaxamtDt(rs.getDouble("taxamt_dt"));
        cv.setTaxcoacode(rs.getString("taxcoacode"));
        cv.setTaxcoadescp(rs.getString("taxcoadescp"));
        cv.setTaxdescp(rs.getString("taxdescp"));
        cv.setTaxperiodfrom(rs.getDate("taxperiodfrom"));
        cv.setTaxperiodto(rs.getDate("taxperiodto"));
        cv.setTaxrate(rs.getDouble("taxrate"));
        cv.setTrxtype(rs.getString("trxtype"));
        cv.setTxOutputDetailPK(pk);
        cv.setVoucherrefer(rs.getString("voucherrefer"));
        cv.setYear(rs.getInt("year"));

        return cv;
    }
    
    public static void deleteTaxDetail(LoginProfile log, String refer) throws SQLException, Exception {
        
        String deleteQuery_2 = "delete from tx_output_detail where refer= ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, refer);
        ps_2.executeUpdate();
        ps_2.close();
        
        TaxOutputDAO.updateTaxStatus(log, refer, "");
        
        
    }
    
    public static void updateTaxStatus(LoginProfile log, String refer, String status) throws Exception {
        
        String query = "UPDATE tx_output SET lockedby = ? where refno = '" + refer + "'";
        
        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.setString(1, status);
        ps.executeUpdate();
        ps.close();
        
    }

}
