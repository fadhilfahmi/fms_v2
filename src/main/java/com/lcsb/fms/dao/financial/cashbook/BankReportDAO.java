/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.dao.financial.cashbook;

import com.lcsb.fms.dao.financial.ap.VendorPaymentDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.model.financial.cashbook.BankReconcilation;
import com.lcsb.fms.model.financial.cashbook.BankReconcilationPK;
import com.lcsb.fms.model.financial.cashbook.BankReport;
import com.lcsb.fms.model.financial.cashbook.BankReportParam;
import com.lcsb.fms.model.financial.cashbook.OfficialReceipt;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucher;
import com.lcsb.fms.model.financial.cashbook.BankReconcile;
import com.lcsb.fms.model.financial.cashbook.BankReconcilePK;
import com.lcsb.fms.model.financial.cashbook.BankReconcileReportMaster;
import com.lcsb.fms.model.financial.cashbook.ItemDetail;
import com.lcsb.fms.model.financial.cashbook.ItemMain;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class BankReportDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("020207");
        mod.setModuleDesc("Bank Report");
        mod.setMainTable("cb_payvoucher");
        mod.setReferID_Master("refer");
        return mod;

    }

    public static List<ListTable> tableList() {

        //String column_names[] = {"invref", "invdate", "bname", "prodname", "cid", "aid", "postflag"};
        String title_name[] = {"Bil", "Date", "Voucher No", "Paid By/Paid To", "Paymode", "Cheque No", "Ampunt", "Date Clear", "Amount Clear", "Varians", "Remarks"};
        boolean boolview[] = {true, true, true, true, true, true, true, true, true, true, true};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < title_name.length; j++) {
            ListTable i = new ListTable();
            //i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            i.setBoolView(boolview[j]);
            lt.add(i);
        }

        return lt;
    }

    public static List<BankReconcilation> getAll(LoginProfile log, BankReportParam p) throws Exception, SQLException {

        Statement stmt = null;
        List<BankReconcilation> CV;
        CV = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from bank_reconcilation where bankcode='" + p.getBankcode() + "' and year=" + p.getYear() + " and period=" + p.getPeriod() + " order by voucherdate,voucherno asc");

            while (rs.next()) {

                BankReconcilation opt = new BankReconcilation();

                opt = BankReportDAO.getRsBankReport(rs);

                if (rs.getString("depan").equals("PVN")) {
                    opt.setLabelTrans("<i class=\"fa fa-plus-circle\" aria-hidden=\"true\"></i>");
                } else if (rs.getString("depan").equals("ORN")) {
                    opt.setLabelTrans("<i class=\"fa fa-minus-circle\" aria-hidden=\"true\"></i>");
                }

                if (rs.getString("paymode").equals("Cheque")) {
                    opt.setLabelPaymode("primary");
                } else if (rs.getString("paymode").equals("Bank Charges")) {
                    opt.setLabelPaymode("default");
                }

                if (rs.getString("status").equals("Clear")) {
                    opt.setLabelStatus("<span class=\"label label-success\">Clear</span>");
                } else if (rs.getString("status").equals("Overdue")) {
                    opt.setLabelStatus("<span class=\"label label-danger\">Overdue</span>");
                } else if (rs.getString("status").equals("Outstanding")) {
                    opt.setLabelStatus("<span class=\"label label-info\">Oustanding</span>");
                } else if (rs.getString("status").equals("Bounce")) {
                    opt.setLabelStatus("<span class=\"label label-warning\">Bounce</span>");
                } else if (rs.getString("status").equals("None")) {
                    opt.setLabelStatus("<span class=\"label label-default\">None</span>");
                }

//                if(rs.getDate("dateclear") == null){
//                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//                    Date date = formatter.parse("0000-00-00");
//                    opt.setDateclear(date);
//                }
                CV.add(opt);

            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static void runBankReport(LoginProfile log, BankReportParam p) throws Exception {

        runPV(log, p);
        runPVCharges(log, p);
        runOR(log, p);
        runORHibah(log, p);

        if (p.getCarry() != null) {
            runCarryForward(log, p);
        }

    }

    private static void runPV(LoginProfile log, BankReportParam p) throws Exception {

        PaymentVoucher c = null;

        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_payvoucher where bankcode='" + p.getBankcode() + "' and year='" + p.getYear() + "' and period='" + p.getPeriod() + "' and post<>'cancel' and flag='NO' and flagbank='NO' group by refer");
        //stmt.setString(1, referenceno);
        int cnt = 0;

        rs = stmt.executeQuery();
        while (rs.next()) {

            c = VendorPaymentDAO.getPYV(rs);

            BankReconcilation br = new BankReconcilation();
            BankReconcilationPK brP = new BankReconcilationPK();

            brP.setDepan("PVN");
            brP.setPeriod(c.getPeriod());
            brP.setRefer(c.getRefer());
            brP.setVoucherno(c.getRefer());
            brP.setYear(c.getYear());

            br.setBankReconcilationPK(brP);
            br.setEstatecode(c.getEstatecode());
            br.setBankcode(c.getBankcode());
            br.setBankname(c.getBankname());
            br.setVoucherdate(c.getTarikh());
            br.setPaid(c.getPaidname());
            br.setPaymode(c.getPaymentmode());
            br.setCek(c.getCekno());
            br.setAmountpv(c.getAmount());

            insertData(log, br);
            BankReportDAO.updatePV(log, c.getRefer());

            cnt++;
        }

        Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, String.valueOf(cnt) + " Payment Voucher saved into bank reconciliation");
    }

    private static void runPVCharges(LoginProfile log, BankReportParam p) throws Exception {

        PaymentVoucher c = null;

        ResultSet rs = null;
        Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, "select * from cb_payvoucher where bankcode='" + p.getBankcode() + "' and concat(year,lpad(period,2,'0')) > concat('" + p.getYear() + "',lpad('" + p.getPeriod() + "',2,'0')) and post<>'cancel' and flagbank='YES'  group by refer");
       //PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_payvoucher where bankcode='" + p.getBankcode() + "' and concat(year,lpad(period,2,'0')) > concat('" + p.getYear() + "',lpad('" + p.getPeriod() + "',2,'0')) and post<>'cancel' and flagbank='YES'  group by refer");
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_payvoucher where bankcode='" + p.getBankcode() + "' and year = '" + p.getYear() + "' and period = '" + p.getPeriod() + "' and post<>'cancel' and flagbank='YES'  group by refer");
        //stmt.setString(1, referenceno);

        int cnt = 1;
        //Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, String.valueOf(stmt));
        rs = stmt.executeQuery();
        while (rs.next()) {

            c = VendorPaymentDAO.getPYV(rs);

            BankReconcilation br = new BankReconcilation();
            BankReconcilationPK brP = new BankReconcilationPK();

            brP.setDepan("PVN");
            brP.setPeriod(c.getPeriod());
            brP.setRefer(c.getRefer());
            brP.setVoucherno(c.getRefer());
            brP.setYear(c.getYear());

            br.setBankReconcilationPK(brP);
            br.setEstatecode(c.getEstatecode());
            br.setBankcode(c.getBankcode());
            br.setBankname(c.getBankname());
            br.setVoucherdate(c.getTarikh());
            br.setPaid(c.getPaidname());
            br.setPaymode(c.getPaymentmode());
            br.setCek(c.getCekno());
            br.setAmountpv(c.getAmount());
            br.setBankcharges("YES");

            insertDataCharges(log, br, "");
            BankReportDAO.updatePVCharges(log, c.getRefer());

            cnt++;

        }

        Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, String.valueOf(cnt) + " Payment Voucher Charges saved into bank reconciliation");
    }

    private static void insertData(LoginProfile log, BankReconcilation c) throws Exception {

        BankReconcilationPK pk = c.getBankReconcilationPK();
        PreparedStatement ps = log.getCon().prepareStatement("insert into bank_reconcilation (refer,depan,estatecode,bankcode,bankname,year,period,voucherdate,voucherno,paid,paymode,cek,amountpv) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
        ps.setString(1, pk.getRefer());
        ps.setString(2, pk.getDepan());
        ps.setString(3, c.getEstatecode());
        ps.setString(4, c.getBankcode());
        ps.setString(5, c.getBankname());
        ps.setString(6, pk.getYear());
        ps.setString(7, pk.getPeriod());
        ps.setString(8, c.getVoucherdate());
        ps.setString(9, pk.getVoucherno());
        ps.setString(10, c.getPaid());
        ps.setString(11, c.getPaymode());
        ps.setString(12, c.getCek());
        ps.setDouble(13, c.getAmountpv());

        ps.executeUpdate();
        Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
        ps.close();

    }

    private static void insertDataCharges(LoginProfile log, BankReconcilation c, String from) throws Exception {

        Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, " -@@@@@@@@@@@@- " + from);

        BankReconcilationPK pk = c.getBankReconcilationPK();

        PreparedStatement ps = log.getCon().prepareStatement("insert into bank_reconcilation (bankcharges,refer,depan,estatecode,bankcode,bankname,year,period,voucherdate,voucherno,paid,paymode,cek,amountpv) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        ps.setString(1, c.getBankcharges());
        ps.setString(2, pk.getRefer());
        ps.setString(3, pk.getDepan());
        ps.setString(4, c.getEstatecode());
        ps.setString(5, c.getBankcode());
        ps.setString(6, c.getBankname());
        ps.setString(7, pk.getYear());
        ps.setString(8, pk.getPeriod());
        ps.setString(9, c.getVoucherdate());
        ps.setString(10, pk.getRefer());
        ps.setString(11, c.getPaid());
        ps.setString(12, c.getPaymode());
        ps.setString(13, c.getCek());
        ps.setDouble(14, c.getAmountpv());

        ps.executeUpdate();
        Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
        ps.close();

    }

    private static void insertDataCarryForward(LoginProfile log, BankReconcilation c) throws Exception {

        BankReconcilationPK pk = c.getBankReconcilationPK();
        PreparedStatement ps = log.getCon().prepareStatement("insert into bank_reconcilation (voucherdate,voucherno,cek,amountpv,status,remarks,dateclear,amountclear,varians,bankcode,bankname,year,period,paid,paymode,createdvoucher,estatecode,depan,refer,flag,bankcharges,coacode,coadesc,paidcode) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        ps.setString(1, c.getVoucherdate());
        ps.setString(2, pk.getVoucherno());
        ps.setString(3, c.getCek());
        ps.setDouble(4, c.getAmountpv());
        ps.setString(5, c.getStatus());
        ps.setString(6, c.getRemarks());
        ps.setString(7, c.getDateclear());
        ps.setDouble(8, c.getAmountclear());
        ps.setDouble(9, c.getVarians());
        ps.setString(10, c.getBankcode());
        ps.setString(11, c.getBankname());
        ps.setString(12, pk.getYear());
        ps.setString(13, pk.getPeriod());
        ps.setString(14, c.getPaid());
        ps.setString(15, c.getPaymode());
        ps.setString(16, c.getCreatedvoucher());
        ps.setString(17, c.getEstatecode());
        ps.setString(18, pk.getDepan());
        ps.setString(19, pk.getRefer());
        ps.setString(20, c.getFlag());
        ps.setString(21, c.getBankcharges());
        ps.setString(22, c.getCoacode());
        ps.setString(23, c.getCoadesc());
        ps.setString(24, c.getPaidcode());

        ps.executeUpdate();
        Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
        ps.close();

    }

    public static void updatePV(LoginProfile log, String pvno) throws Exception {

        String update = "update cb_payvoucher set flag = 'YES'  where refer =?";
        PreparedStatement ps = log.getCon().prepareStatement(update);
        ps.setString(1, pvno);
        ps.executeUpdate();
        ps.close();

    }

    public static void updatePVCharges(LoginProfile log, String pvno) throws Exception {

        String update = "update cb_payvoucher set flag = 'YES' , flagbank='NO' where refer =?";
        PreparedStatement ps = log.getCon().prepareStatement(update);
        ps.setString(1, pvno);
        ps.executeUpdate();
        ps.close();

    }

    public static void updateOR(LoginProfile log, String orno) throws Exception {

        String update = "update cb_official set flag = 'YES'  where refer =?";
        PreparedStatement ps = log.getCon().prepareStatement(update);
        ps.setString(1, orno);
        ps.executeUpdate();
        ps.close();

    }

    public static void updateORHibah(LoginProfile log, String orno) throws Exception {

        String update = "update cb_official set flag = 'YES' , flagbank='NO' where refer =?";
        PreparedStatement ps = log.getCon().prepareStatement(update);
        ps.setString(1, orno);
        ps.executeUpdate();
        ps.close();

    }

    public static List<BankReport> getAllBankBook(LoginProfile log, String period, String year) throws Exception {

        Statement stmt = null;
        List<BankReport> Pc;
        Pc = new ArrayList();
        Logger.getLogger(PettyCashDAO.class.getName()).log(Level.INFO, "select * from cb_tempbankbalance where period ='" + period + "' and year='" + year + "' order by pvdate, status desc,pvid");

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_tempbankbalance where period ='" + period + "' and year='" + year + "' order by pvdate, status desc,pvid");

            while (rs.next()) {
                Pc.add(getBankBook(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Pc;
    }

    public static BankReport getBankBook(LoginProfile log, String code) throws SQLException, Exception {
        BankReport c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_cashvoucher where refer=?");
        stmt.setString(1, code);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getBankBook(rs);
        }

        return c;
    }

    private static BankReport getBankBook(ResultSet rs) throws SQLException {
        BankReport bb = new BankReport();
        //bb.setPvdate(rs.getString("pvdate"));
        // bb.setPvid(rs.getString("pvid"));
        //bb.setPvremarks(rs.getString("pvremarks"));
        // bb.setListcoa(rs.getString("listcoa"));
        // bb.setPvamount(rs.getString("pvamount"));
        // bb.setListcek(rs.getString("listcek"));
        // bb.setStatus(rs.getString("status"));
        // bb.setSacode(rs.getString("sacode"));

        return bb;
    }

    private static void runOR(LoginProfile log, BankReportParam p) throws Exception {

        OfficialReceipt c = null;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_official where bankcode='" + p.getBankcode() + "' and year=" + p.getYear() + " and period=" + p.getPeriod() + " and post<>'cancel' and flag='NO' and flagbank='NO' order by voucherno");
        //stmt.setString(1, referenceno);
        int cnt = 0;

        rs = stmt.executeQuery();
        while (rs.next()) {

            c = OfficialReceiptDAO.getINV(rs);

            String cek = "";
            ResultSet rs1 = null;
            PreparedStatement stmt1 = log.getCon().prepareStatement("select * from cb_official_cek where voucherno='" + c.getRefer() + "'");

            rs1 = stmt1.executeQuery();
            while (rs1.next()) {

                BankReconcilation br = new BankReconcilation();
                BankReconcilationPK brP = new BankReconcilationPK();

                brP.setDepan("ORN");
                brP.setPeriod(String.valueOf(c.getPeriod()));
                brP.setRefer(rs.getString("refer"));
                brP.setVoucherno(c.getRefer());
                brP.setYear(String.valueOf(c.getYear()));

                br.setBankReconcilationPK(brP);
                br.setEstatecode(c.getEstatecode());
                br.setBankcode(c.getBankcode());
                br.setBankname(c.getBankname());
                br.setVoucherdate(c.getDate());
                br.setPaid(c.getPaidname());
                br.setPaymode(c.getPaidtype());
                br.setCek(rs1.getString("cekno"));
                br.setAmountpv(rs1.getDouble("amount"));

                cek = rs1.getString("voucherno");

                insertData(log, br);
                updateOR(log, c.getRefer());
                //insertData(c,"PVN");
                //BankReportDAO.updatePV(c.getRefer());
                //cnt++;
            }

            if (!c.getRefer().equalsIgnoreCase(cek)) {

                BankReconcilation br = new BankReconcilation();
                BankReconcilationPK brP = new BankReconcilationPK();

                brP.setDepan("ORN");
                brP.setPeriod(String.valueOf(c.getPeriod()));
                brP.setRefer(c.getRefer());
                brP.setVoucherno(c.getRefer());
                brP.setYear(String.valueOf(c.getYear()));

                br.setBankReconcilationPK(brP);
                br.setEstatecode(c.getEstatecode());
                br.setBankcode(c.getBankcode());
                br.setBankname(c.getBankname());
                br.setVoucherdate(c.getDate());
                br.setPaid(c.getPaidname());
                br.setPaymode(c.getPaidtype());
                br.setCek("");
                br.setAmountpv(c.getAmount());

                insertData(log, br);
                updateOR(log, c.getRefer());

            }

            Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, String.valueOf(cnt) + " Official Receipt saved into bank reconciliation");
        }
    }

    private static void runORHibah(LoginProfile log, BankReportParam p) throws Exception {

        OfficialReceipt c = null;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_official where bankcode='" + p.getBankcode() + "' and stmtyear=" + p.getYear() + " and stmtperiod=" + p.getPeriod() + " and post<>'cancel' and flagbank='YES' order by voucherno desc");
        //stmt.setString(1, referenceno);
        int cnt = 0;

        rs = stmt.executeQuery();
        while (rs.next()) {

            c = OfficialReceiptDAO.getINV(rs);

            String cek = "";
            ResultSet rs1 = null;
            PreparedStatement stmt1 = log.getCon().prepareStatement("select * from cb_official_cek where voucherno='" + c.getRefer() + "'");

            rs1 = stmt1.executeQuery();
            while (rs1.next()) {

                BankReconcilation br = new BankReconcilation();
                BankReconcilationPK brP = new BankReconcilationPK();

                brP.setDepan("ORN");
                brP.setPeriod(String.valueOf(c.getPeriod()));
                brP.setRefer(rs.getString("refer"));
                brP.setVoucherno(c.getRefer());
                brP.setYear(String.valueOf(c.getYear()));

                br.setBankReconcilationPK(brP);
                br.setEstatecode(c.getEstatecode());
                br.setBankcode(c.getBankcode());
                br.setBankname(c.getBankname());
                br.setVoucherdate(c.getDate());
                br.setPaid(c.getPaidname());
                br.setPaymode(c.getPaidtype());
                br.setCek(rs1.getString("cekno"));
                br.setAmountpv(rs1.getDouble("amount"));
                br.setBankcharges("YES");

                cek = rs1.getString("voucherno");

                Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, " run here " + cek);

                insertDataCharges(log, br, "from cek");
                updateORHibah(log, c.getRefer());

                //insertData(c,"PVN");
                //BankReportDAO.updatePV(c.getRefer());
                //cnt++;
            }

            Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, c.getRefer() + " -- " + cek);
            if ((!c.getRefer().equalsIgnoreCase(cek))) {

                BankReconcilation br = new BankReconcilation();
                BankReconcilationPK brP = new BankReconcilationPK();

                brP.setDepan("ORN");
                brP.setPeriod(String.valueOf(c.getPeriod()));
                brP.setRefer(c.getRefer());
                brP.setVoucherno(c.getRefer());
                brP.setYear(String.valueOf(c.getYear()));

                br.setBankReconcilationPK(brP);
                br.setEstatecode(c.getEstatecode());
                br.setBankcode(c.getBankcode());
                br.setBankname(c.getBankname());
                br.setVoucherdate(c.getDate());
                br.setPaid(c.getPaidname());
                br.setPaymode(c.getPaidtype());
                br.setCek("");
                br.setAmountpv(c.getAmount());
                br.setBankcharges("YES");

                insertDataCharges(log, br, "from not cek");
                updateORHibah(log, c.getRefer());

            }

            Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, String.valueOf(cnt) + " Official Receipt saved into bank reconciliation");
        }
    }

    private static void runCarryForward(LoginProfile log, BankReportParam p) throws Exception {

        PaymentVoucher c = null;

        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select voucherdate,voucherno,cek,amountpv,if((status='bounce' and flag='yes') or (status='overdue' and flag='yes'),'Clear',status) as status,remarks,dateclear,amountclear,varians,bankcode,bankname,if(period=12,t1.year+1,t1.year) as yearx,if(period=12,1,period+1) as periodx,paid,paymode,createdvoucher,estatecode,depan,refer,flag,bankcharges,coacode,coadesc,paidcode from bank_reconcilation as t1  where bankcode='" + p.getBankcode() + "' and year=" + p.getYear() + " and period=" + p.getPeriod() + " and ((status<>'bank charges' and status<> 'hibah' and status <> 'clear' and status <> '') or ((status='Bank Charges' or status='hibah') and concat(substring('" + p.getYear() + "',3,2),lpad('" + p.getPeriod() + "',2,'0')) <= concat(substring(voucherno,8,2),substring(voucherno,10,2))))");
        //stmt.setString(1, referenceno);
        int cnt = 0;

        rs = stmt.executeQuery();
        while (rs.next()) {

            BankReconcilation br = getRsBankReport(rs);

            BankReportDAO.insertDataCarryForward(log, br);

            cnt++;
        }

        Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, String.valueOf(cnt) + " Carry Forward saved into bank reconciliation");
    }

    private static BankReconcilation getRsBankReport(ResultSet rs) throws SQLException, Exception {

        BankReconcilation br = new BankReconcilation();
        BankReconcilationPK brP = new BankReconcilationPK();

        brP.setDepan(rs.getString("depan"));
        brP.setPeriod(rs.getString("period"));
        brP.setRefer(rs.getString("refer"));
        brP.setVoucherno(rs.getString("voucherno"));
        brP.setYear(rs.getString("year"));

        br.setBankReconcilationPK(brP);

        br.setAmountclear(rs.getDouble("amountclear"));
        br.setAmountpv(rs.getDouble("amountpv"));
        br.setBankcharges(rs.getString("bankcharges"));
        br.setBankcode(rs.getString("bankcode"));
        br.setBankname(rs.getString("bankname"));
        br.setCek(rs.getString("cek"));
        br.setCoacode(rs.getString("coacode"));
        br.setCoadesc(rs.getString("coadesc"));
        br.setCreatedvoucher(rs.getString("createdvoucher"));
        br.setDateclear(rs.getString("dateclear"));
        br.setEstatecode(rs.getString("estatecode"));
        br.setFlag(rs.getString("flag"));
        br.setPaid(rs.getString("paid"));
        br.setPaidcode(rs.getString("paidcode"));
        br.setPaymode(rs.getString("paymode"));
        br.setRemarks(rs.getString("remarks"));
        br.setStatus(rs.getString("status"));
        br.setVarians(rs.getDouble("varians"));
        br.setVoucherdate(rs.getString("voucherdate"));

        return br;
    }

    public static void updateBankStatus(LoginProfile log, BankReconcilation br, BankReportParam prm) throws Exception {

        try {
            
            Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, "+++++++** "+br.getDateclear());

            String update = "update bank_reconcilation set amountclear = ?,dateclear = ?,status = ? where bankcode=? and voucherno =? and concat(year,period)=concat(?,?)";
            PreparedStatement ps = log.getCon().prepareStatement(update);
            ps.setDouble(1, br.getAmountclear());
            ps.setString(2, br.getDateclear()== "" ? "0000-00-00" : br.getDateclear());
            ps.setString(3, br.getStatus() == null ? "None" : br.getStatus());
            ps.setString(4, prm.getBankcode());
            ps.setString(5, br.getBankReconcilationPK().getRefer());
            ps.setString(6, prm.getYear());
            ps.setString(7, prm.getPeriod());
            ps.executeUpdate();
            
            Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, "+++++++** "+ps);
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateBankStatusBeforeSave(LoginProfile log, BankReportParam prm) throws Exception {

        Connection con = log.getCon();

        try {

            String deleteQuery_1 = "delete from bank_reconcilation where bankcode='" + prm.getBankcode() + "' and concat(year,lpad(period,2,'0'))>concat(" + prm.getYear() + ",lpad(" + prm.getPeriod() + ",2,'0')) ";
            String update_1 = "update cb_official set flag = 'NO' where bankcode='" + prm.getBankcode() + "' and year='" + prm.getYear() + "'";
            String update_2 = "update cb_payvoucher set flag = 'NO' where bankcode='" + prm.getBankcode() + "' and year='" + prm.getYear() + "'";
            String update_3 = "update cb_payvoucher set flagbank = 'YES' where paymentmode='Bank Charges' ";
            String update_4 = "update cb_official set flagbank = 'YES' where receivemode='Bank Charges'";
            String update_5 = "update cb_official set flagbank = 'YES' where receivemode='Hibah' ";

            PreparedStatement ps_1 = con.prepareStatement(deleteQuery_1);
            PreparedStatement ps_2 = con.prepareStatement(update_1);
            PreparedStatement ps_3 = con.prepareStatement(update_2);
            PreparedStatement ps_4 = con.prepareStatement(update_3);
            PreparedStatement ps_5 = con.prepareStatement(update_4);
            PreparedStatement ps_6 = con.prepareStatement(update_5);

            ps_1.close();
            ps_2.close();
            ps_3.close();
            ps_4.close();
            ps_5.close();
            ps_6.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static ItemMain getReconcileList(LoginProfile log, String type, BankReportParam prm) throws Exception {

        ItemMain im = new ItemMain();

        Statement stmt = null;
        List<ItemDetail> CVi;
        CVi = new ArrayList();

        String q = "";
        double totalItem = 0.0;

        im.setType(type);

        switch (type) {
            case "outstanding-issued-cheque":
                im.setTitle("(-) List of Outstanding Issued Cheque");
                //q = "select voucherdate as date, paid, cek as chequeno, amountpv as amount from bank_reconcilation where depan = 'PVN' and status='Outstanding' and bankcode ='"+bcode+"' and year="+y+" and period="+p+" order by voucherdate,cek";
                q = "select voucherdate as date, paid, cek as chequeno, amountpv as amount from bank_reconcilation where depan = 'PVN' and status='Outstanding' and bankcode ='" + prm.getBankcode() + "' and year=" + prm.getYear() + " and period=" + prm.getPeriod() + " order by voucherdate,cek";
                
                im.setTotalCurrent(BankReportDAO.getBankReconcile(log, prm).getB());
                break;
            case "outstanding-received-cheque":
                im.setTitle("(+) List of Outstanding Received Cheque");
                //q = "select voucherdate as date, paid, cek as chequeno, amountpv as amount from bank_reconcilation where depan = 'ORN' and status='Outstanding' and bankcode ='"+bcode+"'  and year ="+y+" and period ="+p+" order by voucherdate";
                q = "select voucherdate as date, paid, cek as chequeno, amountpv as amount from bank_reconcilation where depan = 'ORN' and status='Outstanding' and bankcode ='" + prm.getBankcode() + "'  and year =" + prm.getYear() + " and period =" + prm.getPeriod() + " order by voucherdate";
                
                im.setTotalCurrent(BankReportDAO.getBankReconcile(log, prm).getC());
                break;
//            case "bankcharges-expense":
//                im.setTitle("(+) Bank Charges - Expense");
//                q = "select tarikh as date, paidname as paid, cekno as chequeno, amount from cb_payvoucher where paymentmode='Bank Charges'  and bankcode ='" + prm.getBankcode() + "' and post<>'cancel'  and concat('" + prm.getYear() + "',lpad('" + prm.getPeriod() + "',2,'0')) <= concat(year,lpad(period,2,'0'))   and  concat('" + prm.getYear() + "',lpad('" + prm.getPeriod() + "',2,'0')) >= concat(stmtyear,lpad(stmtperiod,2,'0')) order by tarikh";
//                
//                im.setTotalCurrent(BankReportDAO.getBankReconcile(log, prm).getD());
//                break;
//            case "bankcharges-receive":
//                im.setTitle("(-) Bank Charges - Receive");
//                q = "select  date, paidname as paid, amount from cb_official where receivemode='Bank Charges' and bankcode ='" + prm.getBankcode() + "' and post<>'cancel'  and concat('" + prm.getYear() + "',lpad('" + prm.getPeriod() + "',2,'0')) <= concat(year,lpad(period,2,'0'))   and  concat('" + prm.getYear() + "',lpad('" + prm.getPeriod() + "',2,'0')) >= concat(stmtyear,lpad(stmtperiod,2,'0')) order by date";
//                
//                im.setTotalCurrent(BankReportDAO.getBankReconcile(log, prm).getE());
//                break;
//            case "hibah":
//                im.setTitle("(-) Hibah");
//                q = "select date, paidname as paid, amount, chequeno from cb_official where receivemode='Hibah' and bankcode ='" + prm.getBankcode() + "' and post<>'cancel'  and concat('" + prm.getYear() + "',lpad('" + prm.getPeriod() + "',2,'0')) <= concat(year,lpad(period,2,'0'))   and  concat('" + prm.getYear() + "',lpad('" + prm.getPeriod() + "',2,'0')) >= concat(stmtyear,lpad(stmtperiod,2,'0')) order by date";
//                
//                im.setTotalCurrent(BankReportDAO.getBankReconcile(log, prm).getF());
//                
//                Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, String.valueOf(q));
//                break;
            default:
                break;
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery(q);

            while (rs.next()) {

                totalItem += rs.getDouble("amount");

                ItemDetail i = new ItemDetail();
                i.setAmount(rs.getDouble("amount"));
                i.setChequeno(rs.getString("chequeno"));
                i.setDate(rs.getString("date"));
                i.setPaid(rs.getString("paid"));

                CVi.add(i);
            }

            im.setTotalItem(totalItem);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        im.setListItemDetail(CVi);

        return im;
    }
    
    public static boolean isReconcile(LoginProfile log, BankReportParam prm) throws SQLException, Exception {

        boolean c = false;
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_bankreconcil where codebank ='" + prm.getBankcode() + "' and year =" + prm.getYear() + " and period =" + prm.getPeriod());
            //stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static BankReconcile getBankReconcile(LoginProfile log, BankReportParam prm) throws SQLException, Exception {

        BankReconcile br = new BankReconcile();
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_bankreconcil where codebank = ? and year = ? and period = ?");
            stmt.setString(1, prm.getBankcode());
            stmt.setString(2, prm.getYear());
            stmt.setString(3, prm.getPeriod());
            
            Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, String.valueOf(stmt));
            rs = stmt.executeQuery();
            if (rs.next()) {
                br = getResult(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return br;
    }
    
    private static BankReconcile getResult(ResultSet rs) throws SQLException {
        
        BankReconcile br= new BankReconcile();
        BankReconcilePK brPK = new BankReconcilePK();
        
        br.setA(rs.getDouble("a"));
        br.setAdate(rs.getString("adate"));
        br.setAdesignation(rs.getString("adesignation"));
        br.setAid(rs.getString("aid"));
        br.setAname(rs.getString("aname"));
        br.setB(rs.getDouble("b"));
        br.setC(rs.getDouble("c"));
        br.setCdate(rs.getString("cdate"));
        br.setCid(rs.getString("cid"));
        br.setCname(rs.getString("cname"));
        brPK.setCodebank(rs.getString("codebank"));
        br.setD(rs.getDouble("d"));
        br.setE(rs.getDouble("e"));
        br.setEstatecode(rs.getString("estatecode"));
        br.setEstatename(rs.getString("estatename"));
        br.setF(rs.getDouble("f"));
        br.setPdate(rs.getString("pdate"));
        brPK.setPeriod(rs.getString("period"));
        br.setPid(rs.getString("pid"));
        br.setPname(rs.getString("pname"));
        brPK.setYear(rs.getString("year"));
        
        br.setBankReconcilePK(brPK);
        
        return br;
    }

    public static double getBankBookBalance(LoginProfile log, BankReportParam prm) throws SQLException, Exception {

        double c = 0.0;
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_bankreconcil where codebank ='" + prm.getBankcode() + "' and year =" + prm.getYear() + " and period =" + prm.getPeriod());
            //stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = rs.getDouble("f");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static BankReconcileReportMaster getReconcileMaster(LoginProfile log, BankReportParam prm) throws Exception {

        BankReconcileReportMaster rm = new BankReconcileReportMaster();
        
        rm.setDoReconcile(BankReportDAO.isReconcile(log, prm));

        List<ItemMain> im = new ArrayList();

        im.add(BankReportDAO.getReconcileList(log, "outstanding-issued-cheque", prm));
        im.add(BankReportDAO.getReconcileList(log, "outstanding-received-cheque", prm));
//        im.add(BankReportDAO.getReconcileList(log, "bankcharges-expense", prm));
//        im.add(BankReportDAO.getReconcileList(log, "bankcharges-receive", prm));
//        im.add(BankReportDAO.getReconcileList(log, "hibah", prm));

        rm.setListItemMain(im);
        
        rm.setBankReconcile(BankReportDAO.getBankReconcile(log, prm));

        return rm;

    }
    
    public static void updateBankReconcileMaster(LoginProfile log, BankReconcile br) throws Exception{
        
        if(BankReportDAO.checkReconcileExist(log, br)){
            BankReportDAO.updateBankReconcile(log, br);
        }else{
            BankReportDAO.saveBankReconcile(log, br);
        }
    }

    public static void saveBankReconcile(LoginProfile log, BankReconcile br) throws SQLException, Exception {

        try {
            
            BankReconcilePK brPK = br.getBankReconcilePK();
            
            PreparedStatement ps = log.getCon().prepareStatement("insert into cb_bankreconcil (estatecode,estatename,codebank,year,period,a,b,c,d,e,f,pid,pname,pdate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            ps.setString(1, br.getEstatecode());
            ps.setString(2, br.getEstatename());
            ps.setString(3, brPK.getCodebank());
            ps.setString(4, brPK.getYear());
            ps.setString(5, brPK.getPeriod());
            ps.setDouble(6, br.getA());
            ps.setDouble(7, br.getB());
            ps.setDouble(8, br.getC());
            ps.setDouble(9, br.getD());
            ps.setDouble(10, br.getE());
            ps.setDouble(11, br.getF());
            ps.setString(12, log.getUserID());
            ps.setString(13, log.getFullname());
            ps.setString(14, AccountingPeriod.getCurrentTimeStamp());

            ps.executeUpdate();
            Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateBankReconcile(LoginProfile log, BankReconcile br) throws Exception {

        try {
            
            BankReconcilePK brPK = br.getBankReconcilePK();

            String update = "update cb_bankreconcil set a = ?,b = ?,c = ?,d = ?,e = ?,f = ?,cid= ?,cname= ?,cdate= ?,aid= ?,aname=?,adate=?,adesignation=? where codebank = '"+brPK.getCodebank()+"' and year = '"+brPK.getYear()+"' and period = '"+brPK.getPeriod()+"' and estatecode = '"+br.getEstatecode()+"'";
            PreparedStatement ps = log.getCon().prepareStatement(update);
            ps.setDouble(1, br.getA());
            ps.setDouble(2, br.getB());
            ps.setDouble(3, br.getC());
            ps.setDouble(4, br.getD());
            ps.setDouble(5, br.getE());
            ps.setDouble(6, br.getF());
            ps.setString(7, br.getCid());
            ps.setString(8, br.getCname());
            ps.setString(9, br.getCdate());
            ps.setString(10, br.getAid());
            ps.setString(11, br.getAname());
            ps.setString(12, br.getAdate());
            ps.setString(13, br.getAdesignation());
            ps.executeUpdate();
            
            Logger.getLogger(BankReportDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
    public static boolean checkReconcileExist(LoginProfile log, BankReconcile br) throws SQLException, Exception {
        boolean c = false;
        ResultSet rs = null;
        BankReconcilePK brPK = br.getBankReconcilePK();
        
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_bankreconcil where codebank=? and year = ? and period = ?");
        stmt.setString(1, brPK.getCodebank());
        stmt.setString(2, brPK.getYear());
        stmt.setString(3, brPK.getPeriod());
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = true;
        }

        return c;
    }

    
}
