/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.dao.financial.gl;

import com.lcsb.fms.dao.financial.ap.VendorInvoiceDAO;
import com.lcsb.fms.dao.financial.ar.ArCreditNoteDAO;
import com.lcsb.fms.dao.financial.ar.ArDebitNoteDAO;
import com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO;
import com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO;
import com.lcsb.fms.dao.financial.cashbook.PaymentVoucherDAO;
import com.lcsb.fms.dao.setup.configuration.BuyerDAO;
import com.lcsb.fms.financial.gl.post.ErrorPost;
import com.lcsb.fms.financial.gl.post.Item;
import com.lcsb.fms.financial.gl.post.Master;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.ap.VendorInvoice;
import com.lcsb.fms.model.financial.ap.VendorInvoiceDetail;
import com.lcsb.fms.model.financial.ar.ArCreditNote;
import com.lcsb.fms.model.financial.ar.ArCreditNoteItem;
import com.lcsb.fms.model.financial.ar.ArDebitNote;
import com.lcsb.fms.model.financial.ar.ArDebitNoteItem;
import com.lcsb.fms.model.financial.ar.SalesInvoice;
import com.lcsb.fms.model.financial.ar.SalesInvoiceItem;
import com.lcsb.fms.model.financial.cashbook.OfficialCreditItem;
import com.lcsb.fms.model.financial.cashbook.OfficialReceipt;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucher;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem;
import com.lcsb.fms.model.financial.gl.GLAccCredit;
import com.lcsb.fms.model.financial.gl.GLAccDebit;
import com.lcsb.fms.model.financial.gl.GLCreditNote;
import com.lcsb.fms.model.financial.gl.GLDebitNote;
import com.lcsb.fms.model.financial.gl.JournalVoucher;
import com.lcsb.fms.model.financial.gl.JournalVoucherItem;
import com.lcsb.fms.util.dao.BankDAO;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class PostDAO {

    public static Master getInfo(LoginProfile log, String voucherno, String vtype) throws SQLException, Exception {

        List<ErrorPost> er = new ArrayList();
        Master mt = new Master();

        //mt.setYear(AccountingPeriod.getCurYear());
        //mt.setPeriod(AccountingPeriod.getCurPeriod());
        mt.setSource(vtype);
        mt.setNovoucher(voucherno);

        switch (vtype) {
            case "JV": {
                //Journal Voucher
                JournalVoucher all = (JournalVoucher) JournalVoucherDAO.getJV(log, voucherno);
                mt.setTarikh(all.getJVdate());
                mt.setEstatecode(all.getEstatecode());
                mt.setEstatename(all.getEstatename());
                mt.setDebit(all.getTodebit());
                mt.setCredit(all.getTocredit());
                mt.setYear(all.getYear());
                mt.setPeriod(all.getCurperiod());
                er.add(PostDAO.checkVtype(vtype));
                er.add(PostDAO.checkBalance(Double.parseDouble(all.getTodebit()), Double.parseDouble(all.getTocredit())));
                er.add(PostDAO.checkValue(Double.parseDouble(all.getTodebit()), Double.parseDouble(all.getTocredit())));
                //er.add(PostDAO.checkPeriod(log, all.getCurperiod()));
                break;
            }
            case "SNV": {
                //Sales Invoice
                SalesInvoice all = (SalesInvoice) SalesInvoiceDAO.getINV(log, voucherno);
                mt.setTarikh(all.getInvdate());
                mt.setEstatecode(all.getEstcode());
                mt.setEstatename(all.getEstname());
                mt.setDebit(String.valueOf(SalesInvoiceDAO.getDebit(log, all.getInvref())));
                mt.setCredit(String.valueOf(SalesInvoiceDAO.getCredit(log, all.getInvref())));
                mt.setYear(String.valueOf(all.getYear()));
                mt.setPeriod(String.valueOf(all.getPeriod()));
                er.add(PostDAO.checkVtype(vtype));
                er.add(PostDAO.checkBalance(SalesInvoiceDAO.getDebit(log, all.getInvref()), SalesInvoiceDAO.getCredit(log, all.getInvref())));
                er.add(PostDAO.checkValue(SalesInvoiceDAO.getDebit(log, all.getInvref()), SalesInvoiceDAO.getCredit(log, all.getInvref())));
                //er.add(PostDAO.checkPeriod(log, String.valueOf(all.getPeriod())));
                break;
            }
            case "PV": {
                //Payment Voucher
                PaymentVoucher all = (PaymentVoucher) PaymentVoucherDAO.getPYV(log, voucherno);
                mt.setTarikh(all.getTarikh());
                mt.setEstatecode(all.getEstatecode());
                mt.setEstatename(all.getEstatename());
                mt.setDebit(String.valueOf(PaymentVoucherDAO.getDebit(log, all.getRefer())));
                mt.setCredit(String.valueOf(PaymentVoucherDAO.getCredit(log, all.getRefer())));
                mt.setYear(String.valueOf(all.getYear()));
                mt.setPeriod(String.valueOf(all.getPeriod()));
                er.add(PostDAO.checkVtype(vtype));
                er.add(PostDAO.checkBalance(PaymentVoucherDAO.getDebit(log, all.getRefer()), PaymentVoucherDAO.getCredit(log, all.getRefer())));
                er.add(PostDAO.checkValue(PaymentVoucherDAO.getDebit(log, all.getRefer()), PaymentVoucherDAO.getCredit(log, all.getRefer())));
                //er.add(PostDAO.checkPeriod(log, String.valueOf(all.getPeriod())));
                break;
            }
            case "OR": {
                OfficialReceipt all = (OfficialReceipt) OfficialReceiptDAO.getINV(log, voucherno);
                mt.setTarikh(all.getDate());
                mt.setEstatecode(all.getEstatecode());
                mt.setEstatename(all.getEstatename());
                mt.setDebit(String.valueOf(OfficialReceiptDAO.getDebit(log, all.getRefer())));
                mt.setCredit(String.valueOf(OfficialReceiptDAO.getCredit(log, all.getRefer())));
                mt.setYear(String.valueOf(all.getYear()));
                mt.setPeriod(String.valueOf(all.getPeriod()));
                er.add(PostDAO.checkVtype(vtype));
                er.add(PostDAO.checkBalance(OfficialReceiptDAO.getDebit(log, all.getRefer()), OfficialReceiptDAO.getCredit(log, all.getRefer())));
                er.add(PostDAO.checkValue(OfficialReceiptDAO.getDebit(log, all.getRefer()), OfficialReceiptDAO.getCredit(log, all.getRefer())));
                //er.add(PostDAO.checkPeriod(log, String.valueOf(all.getPeriod())));
                break;
            }
            case "PNV": {
                VendorInvoice all = (VendorInvoice) VendorInvoiceDAO.getPNV(log, voucherno);
                mt.setTarikh(all.getDate());
                mt.setEstatecode(all.getEstatecode());
                mt.setEstatename(all.getEstatename());
                mt.setDebit(String.valueOf(VendorInvoiceDAO.getDebit(log, all.getInvrefno())));
                mt.setCredit(String.valueOf(VendorInvoiceDAO.getCredit(log, all.getInvrefno())));
                mt.setYear(String.valueOf(all.getYear()));
                mt.setPeriod(String.valueOf(all.getPeriod()));
                er.add(PostDAO.checkVtype(vtype));
                er.add(PostDAO.checkBalance(VendorInvoiceDAO.getDebit(log, all.getInvrefno()), VendorInvoiceDAO.getCredit(log, all.getInvrefno())));
                er.add(PostDAO.checkValue(VendorInvoiceDAO.getDebit(log, all.getInvrefno()), VendorInvoiceDAO.getCredit(log, all.getInvrefno())));
                //er.add(PostDAO.checkPeriod(log, String.valueOf(all.getPeriod())));
                break;
            }
            case "DN": {
                GLDebitNote all = (GLDebitNote) DebitNoteDAO.getDNE(log, voucherno);
                mt.setTarikh(String.valueOf(all.getNotedate()));
                mt.setEstatecode(all.getEstcode());
                mt.setEstatename(all.getEstname());
                mt.setDebit(String.valueOf(DebitNoteDAO.getDebit(log, all.getNoteno())));
                mt.setCredit(String.valueOf(DebitNoteDAO.getCredit(log, all.getNoteno())));
                mt.setYear(String.valueOf(all.getYear()));
                mt.setPeriod(String.valueOf(all.getPeriod()));
                er.add(PostDAO.checkVtype(vtype));
                er.add(PostDAO.checkBalance(DebitNoteDAO.getDebit(log, all.getNoteno()), DebitNoteDAO.getCredit(log, all.getNoteno())));
                er.add(PostDAO.checkValue(DebitNoteDAO.getDebit(log, all.getNoteno()), DebitNoteDAO.getCredit(log, all.getNoteno())));
                er.add(PostDAO.checkPeriod(log, String.valueOf(all.getPeriod())));
                break;
            }
            case "CN": {
                GLCreditNote all = (GLCreditNote) CreditNoteDAO.getCNE(log, voucherno);
                mt.setTarikh(String.valueOf(all.getNotedate()));
                mt.setEstatecode(all.getEstcode());
                mt.setEstatename(all.getEstname());
                mt.setDebit(String.valueOf(CreditNoteDAO.getDebit(log, all.getNoteno())));
                mt.setCredit(String.valueOf(CreditNoteDAO.getCredit(log, all.getNoteno())));
                mt.setYear(String.valueOf(all.getYear()));
                mt.setPeriod(String.valueOf(all.getPeriod()));
                er.add(PostDAO.checkVtype(vtype));
                er.add(PostDAO.checkBalance(CreditNoteDAO.getDebit(log, all.getNoteno()), CreditNoteDAO.getCredit(log, all.getNoteno())));
                er.add(PostDAO.checkValue(CreditNoteDAO.getDebit(log, all.getNoteno()), CreditNoteDAO.getCredit(log, all.getNoteno())));
                er.add(PostDAO.checkPeriod(log, String.valueOf(all.getPeriod())));
                break;
            }
            case "DNR": {
                ArDebitNote all = (ArDebitNote) ArDebitNoteDAO.getMain(log, voucherno);
                mt.setTarikh(String.valueOf(all.getNotedate()));
                mt.setEstatecode(all.getEstcode());
                mt.setEstatename(all.getEstname());
                mt.setDebit(String.valueOf(ArDebitNoteDAO.getDebit(log, all.getNoteno())));
                mt.setCredit(String.valueOf(ArDebitNoteDAO.getCredit(log, all.getNoteno())));
                mt.setYear(String.valueOf(all.getYear()));
                mt.setPeriod(String.valueOf(all.getPeriod()));
                er.add(PostDAO.checkVtype(vtype));
                er.add(PostDAO.checkBalance(ArDebitNoteDAO.getDebit(log, all.getNoteno()), ArDebitNoteDAO.getCredit(log, all.getNoteno())));
                er.add(PostDAO.checkValue(ArDebitNoteDAO.getDebit(log, all.getNoteno()), ArDebitNoteDAO.getCredit(log, all.getNoteno())));
                //er.add(PostDAO.checkPeriod(log, String.valueOf(all.getPeriod())));
                break;
            }
            case "CNR": {
                ArCreditNote all = (ArCreditNote) ArCreditNoteDAO.getMain(log, voucherno);
                mt.setTarikh(String.valueOf(all.getNotedate()));
                mt.setEstatecode(all.getEstcode());
                mt.setEstatename(all.getEstname());
                mt.setDebit(String.valueOf(ArCreditNoteDAO.getDebit(log, all.getNoteno())));
                mt.setCredit(String.valueOf(ArCreditNoteDAO.getCredit(log, all.getNoteno())));
                mt.setYear(String.valueOf(all.getYear()));
                mt.setPeriod(String.valueOf(all.getPeriod()));
                er.add(PostDAO.checkVtype(vtype));
                er.add(PostDAO.checkBalance(ArCreditNoteDAO.getDebit(log, all.getNoteno()), ArCreditNoteDAO.getCredit(log, all.getNoteno())));
                er.add(PostDAO.checkValue(ArCreditNoteDAO.getDebit(log, all.getNoteno()), ArCreditNoteDAO.getCredit(log, all.getNoteno())));
                //er.add(PostDAO.checkPeriod(log, String.valueOf(all.getPeriod())));
                break;
            }
            default:
                break;
        }
        //er.add(PostDAO.checkInfo(mt));
        mt.setListItem(PostDAO.getList(log, voucherno, vtype));
        mt.setVtype(vtype);
        mt.setListError(er);

        return mt;
    }

    public static List<Item> getList(LoginProfile log, String voucherno, String vtype) throws SQLException, Exception {

        List<Item> it = new ArrayList();

        switch (vtype) {
            case "JV": {
                JournalVoucher jv = (JournalVoucher) JournalVoucherDAO.getJV(log, voucherno);
                List<JournalVoucherItem> listAll = (List<JournalVoucherItem>) JournalVoucherDAO.getAllJVItem(log, voucherno);
                for (JournalVoucherItem all : listAll) {

                    Item n = new Item();
                    n.setCoacode(all.getActcode());
                    n.setCoadesc(all.getActdesc());
                    n.setCredit(all.getCredit());
                    n.setDebit(all.getDebit());
                    n.setJid(all.getJvid());
                    n.setLoccode(all.getLoccode());
                    n.setLocdesc(all.getLocdesc());
                    n.setLoclevel(all.getLoclevel());
                    n.setNovoucher(all.getJVrefno());
                    n.setRemark(all.getRemark());
                    n.setSacode(all.getSacode());
                    n.setSadesc(all.getSadesc());
                    n.setSatype(all.getSatype());
                    n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                    n.setYear(jv.getYear());
                    n.setPeriod(jv.getCurperiod());
                    n.setSource(sourcePost("JV"));
                    it.add(n);
                }
                break;
            }
            case "SNV": {
                SalesInvoice sl = (SalesInvoice) SalesInvoiceDAO.getINV(log, voucherno);
                Item m = new Item();
                m.setCoacode(sl.getCoacode());
                m.setCoadesc(sl.getCoadesc());
                m.setCredit(0);
                m.setDebit(sl.getAmountno());
                m.setLoccode(sl.getLoccode());
                m.setLocdesc(sl.getLocdesc());
                m.setLoclevel(sl.getLoclevel());
                m.setNovoucher(sl.getInvref());
                m.setRemark(sl.getRemarks());
                m.setSacode(sl.getSacode());
                m.setSadesc(sl.getSadesc());
                m.setSatype(sl.getSatype());
                m.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                m.setYear(String.valueOf(sl.getYear()));
                m.setPeriod(String.valueOf(sl.getPeriod()));
                m.setSource(sourcePost(vtype));
                it.add(m);
                List<SalesInvoiceItem> listAll = (List<SalesInvoiceItem>) SalesInvoiceDAO.getAllINVItem(log, voucherno);
                for (SalesInvoiceItem all : listAll) {

                    Item n = new Item();
                    n.setCoacode(all.getCoacode());
                    n.setCoadesc(all.getCoaname());
                    n.setCredit(all.getAmount());
                    n.setDebit(0);
                    n.setJid(all.getRefer());
                    n.setLoccode(all.getLoccode());
                    n.setLocdesc(all.getLocdesc());
                    n.setLoclevel(all.getLoclevel());
                    n.setNovoucher(all.getIvref());
                    n.setRemark(all.getRemarks());
                    n.setSacode(all.getSacode());
                    n.setSadesc(all.getSadesc());
                    n.setSatype(all.getSatype());
                    n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                    n.setYear(String.valueOf(sl.getYear()));
                    n.setPeriod(String.valueOf(sl.getPeriod()));
                    n.setSource(sourcePost(vtype));
                    it.add(n);
                }

//                    for (SalesInvoiceItem all : listAll) {
//                        
//                        Item n = new Item();
//                        n.setCoacode(all.getCoacode());
//                        n.setCoadesc(all.getCoaname());
//                        n.setCredit(all.getTaxamt());
//                        n.setDebit(0);
//                        n.setJid(all.getRefer());
//                        n.setLoccode(all.getLoccode());
//                        n.setLocdesc(all.getLocdesc());
//                        n.setLoclevel(all.getLoclevel());
//                        n.setNovoucher(all.getIvref());
//                        n.setRemark(all.getRemarks());
//                        n.setSacode(all.getSacode());
//                        n.setSadesc(all.getSadesc());
//                        n.setSatype(all.getSatype());
//                        n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
//                        n.setYear(AccountingPeriod.getCurYear());
//                        n.setPeriod(AccountingPeriod.getCurPeriod());
//                        n.setSource(sourcePost("SL"));
//                        it.add(n);
//                    }
                List<SalesInvoiceItem> listTax = (List<SalesInvoiceItem>) SalesInvoiceDAO.getAllTax(log, voucherno);
                for (SalesInvoiceItem all : listTax) {

                    Item n = new Item();
                    n.setCoacode(all.getTaxcoacode());
                    n.setCoadesc(all.getTaxcoadescp());
                    n.setCredit(all.getTaxamt());
                    n.setDebit(0.00);
                    n.setJid(all.getRefer());
                    n.setLoccode(all.getLoccode());
                    n.setLocdesc(all.getLocdesc());
                    n.setLoclevel(all.getLoclevel());
                    n.setNovoucher(all.getIvref());
                    n.setRemark(all.getRemarks());
                    n.setSacode("00");
                    n.setSadesc("Not Applicable");
                    n.setSatype("None");
                    n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                    n.setYear(String.valueOf(sl.getYear()));
                    n.setPeriod(String.valueOf(sl.getPeriod()));
                    n.setSource(sourcePost(vtype));
                    it.add(n);
                }

                break;
            }
            case "OR": {
                OfficialReceipt or = (OfficialReceipt) OfficialReceiptDAO.getINV(log, voucherno);
                Item m = new Item();
                m.setCoacode(BankDAO.getInfo(log, or.getBankcode()).getCoacode());
                m.setCoadesc(BankDAO.getInfo(log, or.getBankcode()).getCoadesc());
                m.setCredit(0);
                m.setDebit(or.getAmount());
                m.setLoccode(or.getEstatecode());
                m.setLocdesc(or.getEstatename());
                m.setLoclevel("Company");
                m.setNovoucher(or.getRefer());
                m.setRemark(or.getRemarks());
                m.setSacode("00");
                m.setSadesc("Not Applicable");
                m.setSatype("None");
                m.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                m.setYear(String.valueOf(or.getYear()));
                m.setPeriod(String.valueOf(or.getPeriod()));
                m.setSource(sourcePost("OR"));
                it.add(m);
                List<OfficialCreditItem> listAll = (List<OfficialCreditItem>) OfficialReceiptDAO.getAllINVItem(log, voucherno);
                for (OfficialCreditItem all : listAll) {

                    Item n = new Item();
                    n.setCoacode(all.getCoacode());
                    n.setCoadesc(all.getCoadescp());
                    n.setCredit(Double.parseDouble(all.getAmount()));
                    n.setDebit(0);
                    n.setJid(all.getRefer());
                    n.setLoccode(all.getLoccode());
                    n.setLocdesc(all.getLocname());
                    n.setLoclevel(all.getLoclevel());
                    n.setNovoucher(all.getVoucherno());
                    n.setRemark(all.getRemarks());
                    n.setSacode(all.getSacode());
                    n.setSadesc(all.getSadesc());
                    n.setSatype(all.getSatype());
                    n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                    n.setYear(String.valueOf(or.getYear()));
                    n.setPeriod(String.valueOf(or.getPeriod()));
                    n.setSource(sourcePost("OR"));
                    it.add(n);
                }
                List<OfficialCreditItem> listTax = (List<OfficialCreditItem>) OfficialReceiptDAO.getAllORTax(log, voucherno);
                for (OfficialCreditItem all : listTax) {

                    Item n = new Item();
                    n.setCoacode(all.getTaxcoacode());
                    n.setCoadesc(all.getTaxcoadescp());
                    n.setCredit(all.getTaxamt());
                    n.setDebit(0.00);
                    n.setJid(all.getRefer());
                    n.setLoccode(all.getLoccode());
                    n.setLocdesc(all.getLocname());
                    n.setLoclevel(all.getLoclevel());
                    n.setNovoucher(all.getVoucherno());
                    n.setRemark(all.getRemarks());
                    n.setSacode("00");
                    n.setSadesc("Not Applicable");
                    n.setSatype("None");
                    n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                    n.setYear(String.valueOf(or.getYear()));
                    n.setPeriod(String.valueOf(or.getPeriod()));
                    n.setSource(sourcePost("OR"));
                    it.add(n);
                }
                break;
            }
            case "PV": {
                //-----------credit-----------------------------
                PaymentVoucher lt = (PaymentVoucher) PaymentVoucherDAO.getPYV(log, voucherno);
                Item m = new Item();
                m.setCoacode(BankDAO.getInfo(log, lt.getBankcode()).getCoacode());
                m.setCoadesc(BankDAO.getInfo(log, lt.getBankcode()).getCoadesc());
                m.setCredit(lt.getAmount());
                m.setDebit(0);
                m.setLoccode(lt.getEstatecode());
                m.setLocdesc(lt.getEstatename());
                m.setLoclevel("Company");
                m.setNovoucher(lt.getRefer());
                m.setRemark(lt.getRemarks());
                m.setSacode("00");
                m.setSadesc("Not Applicable");
                m.setSatype("None");
                m.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                m.setYear(lt.getYear());
                m.setPeriod(lt.getPeriod());
                m.setSource(sourcePost("PV"));
                it.add(m);
                List<PaymentVoucherItem> listAll = (List<PaymentVoucherItem>) PaymentVoucherDAO.getAllPYVItem(log, voucherno);
                for (PaymentVoucherItem all : listAll) {

                    Item n = new Item();
                    n.setCoacode(all.getCoacode());
                    n.setCoadesc(all.getCoadescp());
                    n.setCredit(0.00);
                    n.setDebit(all.getAmount());
                    n.setJid(all.getRefer());
                    n.setLoccode(all.getLoccode());
                    n.setLocdesc(all.getLocname());
                    n.setLoclevel(all.getLoclevel());
                    n.setNovoucher(all.getRefer());
                    n.setRemark(all.getRemarks());
                    n.setSacode(all.getSacode());
                    n.setSadesc(all.getSadesc());
                    n.setSatype(all.getSatype());
                    n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                    n.setYear(lt.getYear());
                    n.setPeriod(lt.getPeriod());
                    n.setSource(sourcePost("PV"));
                    it.add(n);
                }
                List<PaymentVoucherItem> listTax = (List<PaymentVoucherItem>) PaymentVoucherDAO.getAllPVTax(log, voucherno);
                for (PaymentVoucherItem all : listTax) {

                    Item n = new Item();
                    n.setCoacode(all.getTaxcoacode());
                    n.setCoadesc(all.getTaxcoadescp());
                    n.setCredit(0.00);
                    n.setDebit(all.getTaxamt());
                    n.setJid(all.getRefer());
                    n.setLoccode(all.getLoccode());
                    n.setLocdesc(all.getLocname());
                    n.setLoclevel(all.getLoclevel());
                    n.setNovoucher(all.getRefer());
                    n.setRemark(all.getRemarks());
                    n.setSacode("00");
                    n.setSadesc("Not Applicable");
                    n.setSatype("None");
                    n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                    n.setYear(lt.getYear());
                    n.setPeriod(lt.getPeriod());
                    n.setSource(sourcePost("PV"));
                    it.add(n);
                }
                double diffAmt = lt.getAmount() - PaymentVoucherDAO.getRoundingAmount(log, voucherno);
                if (diffAmt > 0 && diffAmt < 1) {
                    Logger.getLogger(PostDAO.class.getName()).log(Level.INFO, "-------^^" + String.valueOf(diffAmt));
                    Item n = new Item();
                    n.setCoacode(lt.getRacoacode());
                    n.setCoadesc(lt.getRacoadesc());
                    n.setCredit(0.00);
                    n.setDebit(diffAmt);
                    n.setJid(voucherno);
                    n.setLoccode(lt.getEstatecode());
                    n.setLocdesc(lt.getEstatename());
                    n.setLoclevel("Company");
                    n.setNovoucher(voucherno);
                    n.setRemark(lt.getRemarks());
                    n.setSacode("00");
                    n.setSadesc("Not Applicable");
                    n.setSatype("None");
                    n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                    n.setYear(lt.getYear());
                    n.setPeriod(lt.getPeriod());
                    n.setSource(sourcePost("PV"));

                    it.add(n);

                } else if (diffAmt < 0) {

                    Logger.getLogger(PostDAO.class.getName()).log(Level.INFO, "^^" + String.valueOf(diffAmt));

                    Item n = new Item();
                    n.setCoacode(lt.getRacoacode());
                    n.setCoadesc(lt.getRacoadesc());
                    n.setCredit(diffAmt * -1.0D);
                    n.setDebit(0.00);
                    n.setJid(voucherno);
                    n.setLoccode(lt.getEstatecode());
                    n.setLocdesc(lt.getEstatename());
                    n.setLoclevel("Company");
                    n.setNovoucher(voucherno);
                    n.setRemark(lt.getRemarks());
                    n.setSacode("00");
                    n.setSadesc("Not Applicable");
                    n.setSatype("None");
                    n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                    n.setYear(lt.getYear());
                    n.setPeriod(lt.getPeriod());
                    n.setSource(sourcePost("PV"));

                    it.add(n);

                }
                break;
            }
            case "PNV": {
                //-----------credit-----------------------------
                VendorInvoice lt = (VendorInvoice) VendorInvoiceDAO.getPNV(log, voucherno);
                Item m = new Item();
                m.setCoacode(lt.getAccode());
                m.setCoadesc(lt.getAcdesc());
                m.setCredit(lt.getTotalamount());
                m.setDebit(0);
                m.setLoccode(lt.getEstatecode());
                m.setLocdesc(lt.getEstatename());
                m.setLoclevel("Company");
                m.setNovoucher(lt.getInvrefno());
                m.setRemark(lt.getRemark());
                m.setSacode(lt.getSacode());
                m.setSadesc(lt.getSadesc());
                m.setSatype(lt.getSatype());
                m.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                m.setYear(lt.getYear());
                m.setPeriod(lt.getPeriod());
                m.setSource(sourcePost("AP"));
                it.add(m);

                List<VendorInvoiceDetail> listAll = (List<VendorInvoiceDetail>) VendorInvoiceDAO.getAllPNVItem(log, voucherno);
                for (VendorInvoiceDetail all : listAll) {
                    if (lt.getInvtype().equals("Services")) {

                        Item n = new Item();
                        n.setCoacode(all.getAccode());
                        n.setCoadesc(all.getAcdesc());
                        n.setCredit(0.00);
                        n.setDebit(all.getAmount());
                        n.setJid(all.getNo());
                        n.setLoccode(all.getLoccode());
                        n.setLocdesc(all.getLocdesc());
                        n.setLoclevel(all.getLoclevel());
                        n.setNovoucher(all.getNo());
                        n.setRemark(all.getRemark());
                        n.setSacode(all.getSacode());
                        n.setSadesc(all.getSadesc());
                        n.setSatype(all.getSatype());
                        n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                        n.setYear(lt.getYear());
                        n.setPeriod(lt.getPeriod());
                        n.setSource(sourcePost("AP"));
                        it.add(n);

                    }
                }

                List<VendorInvoiceDetail> listTax = (List<VendorInvoiceDetail>) VendorInvoiceDAO.getAllTax(log, voucherno);
                for (VendorInvoiceDetail all : listTax) {
                    if (lt.getInvtype().equals("Services") && all.getTaxamt() > 0) {

                        Item n = new Item();
                        n.setCoacode(all.getTaxcoacode());
                        n.setCoadesc(all.getTaxcoadescp());
                        n.setCredit(0.00);
                        n.setDebit(all.getTaxamt());
                        n.setJid(all.getInvrefno());
                        n.setLoccode(all.getLoccode());
                        n.setLocdesc(all.getLocdesc());
                        n.setLoclevel(all.getLoclevel());
                        n.setNovoucher(all.getNo());
                        n.setRemark("GST APPLIED");
                        n.setSacode("00");
                        n.setSadesc("Not Applicable");
                        n.setSatype("None");
                        n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                        n.setYear(lt.getYear());
                        n.setPeriod(lt.getPeriod());
                        n.setSource(sourcePost("AP"));
                        it.add(n);

                    } else if (!lt.getInvtype().equals("Services") && all.getAmount() > 0) {

                        Item n = new Item();
                        n.setCoacode(all.getTaxcoacode());
                        n.setCoadesc(all.getTaxcoadescp());
                        n.setCredit(0.00);
                        n.setDebit(all.getTaxamt());
                        n.setJid(all.getInvrefno());
                        n.setLoccode(all.getLoccode());
                        n.setLocdesc(all.getLocdesc());
                        n.setLoclevel(all.getLoclevel());
                        n.setNovoucher(all.getNo());
                        n.setRemark(all.getRemark());
                        n.setSacode("00");
                        n.setSadesc("Not Applicable");
                        n.setSatype("None");
                        n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                        n.setYear(lt.getYear());
                        n.setPeriod(lt.getPeriod());
                        n.setSource(sourcePost("AP"));
                        it.add(n);

                    }
                }

                double diffAmt = lt.getTotalamount() - VendorInvoiceDAO.getRoundingAmount(log, voucherno);
                if (diffAmt > 0 && diffAmt < 1) {
                    Logger.getLogger(PostDAO.class.getName()).log(Level.INFO, "-------^^" + String.valueOf(diffAmt));
                    Item n = new Item();
                    n.setCoacode(lt.getRacoacode());
                    n.setCoadesc(lt.getRacoadesc());
                    n.setCredit(0.00);
                    n.setDebit(diffAmt);
                    n.setJid(voucherno);
                    n.setLoccode(lt.getEstatecode());
                    n.setLocdesc(lt.getEstatename());
                    n.setLoclevel("Company");
                    n.setNovoucher(voucherno);
                    n.setRemark(lt.getRemark());
                    n.setSacode("00");
                    n.setSadesc("Not Applicable");
                    n.setSatype("None");
                    n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                    n.setYear(lt.getYear());
                    n.setPeriod(lt.getPeriod());
                    n.setSource(sourcePost("AP"));

                    it.add(n);

                } else if (diffAmt < 0) {

                    Logger.getLogger(PostDAO.class.getName()).log(Level.INFO, "^^" + String.valueOf(diffAmt));

                    Item n = new Item();
                    n.setCoacode(lt.getRacoacode());
                    n.setCoadesc(lt.getRacoadesc());
                    n.setCredit(diffAmt * -1.0D);
                    n.setDebit(0.00);
                    n.setJid(voucherno);
                    n.setLoccode(lt.getEstatecode());
                    n.setLocdesc(lt.getEstatename());
                    n.setLoclevel("Company");
                    n.setNovoucher(voucherno);
                    n.setRemark(lt.getRemark());
                    n.setSacode("00");
                    n.setSadesc("Not Applicable");
                    n.setSatype("None");
                    n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                    n.setYear(lt.getYear());
                    n.setPeriod(lt.getPeriod());
                    n.setSource(sourcePost("AP"));

                    it.add(n);

                }
                break;
            }
            case "DN": {
                GLDebitNote dn = (GLDebitNote) DebitNoteDAO.getDNE(log, voucherno);
                Item m = new Item();
                m.setCoacode(dn.getAcccode());
                m.setCoadesc(dn.getAccdesc());
                m.setCredit(0);
                m.setDebit(DebitNoteDAO.getDebit(log, voucherno));
                m.setLoccode(dn.getEstcode());
                m.setLocdesc(dn.getEstname());
                m.setLoclevel("Company");
                m.setNovoucher(voucherno);
                m.setRemark(dn.getRemark());
                m.setSacode("00");
                m.setSadesc("Not Applicable");
                m.setSatype("None");
                m.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                m.setYear(dn.getYear());
                m.setPeriod(dn.getPeriod());
                m.setSource(sourcePost("DN"));
                it.add(m);

                GLAccDebit ad = (GLAccDebit) DebitNoteDAO.getAccDebit(log, voucherno);
                Item n = new Item();
                n.setCoacode(ad.getAcccode());
                n.setCoadesc(ad.getAccdesc());
                n.setCredit(DebitNoteDAO.getCredit(log, voucherno));
                n.setDebit(0);
                n.setLoccode(ad.getLoccode());
                n.setLocdesc(ad.getLocdesc());
                n.setLoclevel(ad.getLoclevel());
                n.setNovoucher(voucherno);
                n.setRemark(ad.getRemark());
                n.setSacode(ad.getSacode());
                n.setSadesc(ad.getSadesc());
                n.setSatype(ad.getSatype());
                n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                n.setYear(dn.getYear());
                n.setPeriod(dn.getPeriod());
                n.setSource(sourcePost("DN"));
                it.add(n);
            }
            break;
            case "CN": {
                GLCreditNote dn = (GLCreditNote) CreditNoteDAO.getCNE(log, voucherno);
                Item m = new Item();
                m.setCoacode(dn.getAcccode());
                m.setCoadesc(dn.getAccdesc());
                m.setCredit(CreditNoteDAO.getDebit(log, voucherno));
                m.setDebit(0);
                m.setLoccode(dn.getEstcode());
                m.setLocdesc(dn.getEstname());
                m.setLoclevel("Company");
                m.setNovoucher(voucherno);
                m.setRemark(dn.getRemark());
                m.setSacode("00");
                m.setSadesc("Not Applicable");
                m.setSatype("None");
                m.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                m.setYear(dn.getYear());
                m.setPeriod(dn.getPeriod());
                m.setSource(sourcePost("CN"));
                it.add(m);

                GLAccCredit ad = (GLAccCredit) CreditNoteDAO.getAccCredit(log, voucherno);
                Item n = new Item();
                n.setCoacode(ad.getAcccode());
                n.setCoadesc(ad.getAccdesc());
                n.setCredit(0);
                n.setDebit(CreditNoteDAO.getCredit(log, voucherno));
                n.setLoccode(ad.getLoccode());
                n.setLocdesc(ad.getLocdesc());
                n.setLoclevel(ad.getLoclevel());
                n.setNovoucher(voucherno);
                n.setRemark(ad.getRemark());
                n.setSacode(ad.getSacode());
                n.setSadesc(ad.getSadesc());
                n.setSatype(ad.getSatype());
                n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                n.setYear(dn.getYear());
                n.setPeriod(dn.getPeriod());
                n.setSource(sourcePost("CN"));
                it.add(n);
            }
            break;
            case "DNR": {
                ArDebitNote dn = (ArDebitNote) ArDebitNoteDAO.getMain(log, voucherno);
                Item m = new Item();
                m.setCoacode(BuyerDAO.getInfo(log, dn.getBuyercode()).getCoa());
                m.setCoadesc(BuyerDAO.getInfo(log, dn.getBuyercode()).getCoadescp());
                m.setCredit(0.00);
                m.setDebit(dn.getTotal());
                m.setLoccode(dn.getEstcode());
                m.setLocdesc(dn.getEstname());
                m.setLoclevel("Company");
                m.setNovoucher(dn.getNoteno());
                m.setRemark(dn.getRemark());
                m.setSacode(dn.getBuyercode());
                m.setSadesc(dn.getBuyername());
                m.setSatype("Buyer");
                m.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                m.setYear(dn.getYear());
                m.setPeriod(dn.getPeriod());
                m.setSource(sourcePost(vtype));
                it.add(m);
                List<ArDebitNoteItem> listAll = (List<ArDebitNoteItem>) ArDebitNoteDAO.getAllItem(log, voucherno);
                for (ArDebitNoteItem all : listAll) {

                    Item n = new Item();
                    n.setCoacode(all.getCoacode());
                    n.setCoadesc(all.getCoaname());
                    n.setCredit(all.getAmount());
                    n.setDebit(0);
                    n.setJid(all.getRefer());
                    n.setLoccode(all.getLoccode());
                    n.setLocdesc(all.getLocdesc());
                    n.setLoclevel(all.getLoclevel());
                    n.setNovoucher(all.getNoteno());
                    n.setRemark(all.getRemarks());
                    n.setSacode(all.getSacode());
                    n.setSadesc(all.getSadesc());
                    n.setSatype(all.getSatype());
                    n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                    n.setYear(dn.getYear());
                    n.setPeriod(dn.getPeriod());
                    n.setSource(sourcePost(vtype));
                    it.add(n);
                }

                List<ArDebitNoteItem> listTax = (List<ArDebitNoteItem>) ArDebitNoteDAO.getAllTax(log, voucherno);
                for (ArDebitNoteItem all : listTax) {

                    Item n = new Item();
                    n.setCoacode(all.getTaxcoacode());
                    n.setCoadesc(all.getTaxcoadescp());
                    n.setCredit(all.getTaxamt());
                    n.setDebit(0.00);
                    n.setJid(all.getRefer());
                    n.setLoccode(all.getLoccode());
                    n.setLocdesc(all.getLocdesc());
                    n.setLoclevel(all.getLoclevel());
                    n.setNovoucher(all.getNoteno());
                    n.setRemark(all.getRemarks());
                    n.setSacode("00");
                    n.setSadesc("Not Applicable");
                    n.setSatype("None");
                    n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                    n.setYear(dn.getYear());
                    n.setPeriod(dn.getPeriod());
                    n.setSource(sourcePost(vtype));
                    it.add(n);
                }
                break;
            }
            case "CNR": {
                ArCreditNote dn = (ArCreditNote) ArCreditNoteDAO.getMain(log, voucherno);
                Item m = new Item();
                m.setCoacode(BuyerDAO.getInfo(log, dn.getBuyercode()).getCoa());
                m.setCoadesc(BuyerDAO.getInfo(log, dn.getBuyercode()).getCoadescp());
                m.setCredit(dn.getTotal());
                m.setDebit(0.00);
                m.setLoccode(dn.getEstcode());
                m.setLocdesc(dn.getEstname());
                m.setLoclevel("Company");
                m.setNovoucher(dn.getNoteno());
                m.setRemark(dn.getRemark());
                m.setSacode(dn.getBuyercode());
                m.setSadesc(dn.getBuyername());
                m.setSatype("Buyer");
                m.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                m.setYear(dn.getYear());
                m.setPeriod(dn.getPeriod());
                m.setSource(sourcePost(vtype));
                it.add(m);
                List<ArCreditNoteItem> listAll = (List<ArCreditNoteItem>) ArCreditNoteDAO.getAllItem(log, voucherno);
                for (ArCreditNoteItem all : listAll) {

                    Item n = new Item();
                    n.setCoacode(all.getCoacode());
                    n.setCoadesc(all.getCoaname());
                    n.setCredit(0.00);
                    n.setDebit(all.getAmount());
                    n.setJid(all.getRefer());
                    n.setLoccode(all.getLoccode());
                    n.setLocdesc(all.getLocdesc());
                    n.setLoclevel(all.getLoclevel());
                    n.setNovoucher(all.getNoteno());
                    n.setRemark(all.getRemarks());
                    n.setSacode(all.getSacode());
                    n.setSadesc(all.getSadesc());
                    n.setSatype(all.getSatype());
                    n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                    n.setYear(dn.getYear());
                    n.setPeriod(dn.getPeriod());
                    n.setSource(sourcePost(vtype));
                    it.add(n);
                }

                List<ArCreditNoteItem> listTax = (List<ArCreditNoteItem>) ArCreditNoteDAO.getAllTax(log, voucherno);
                for (ArCreditNoteItem all : listTax) {

                    Item n = new Item();
                    n.setCoacode(all.getTaxcoacode());
                    n.setCoadesc(all.getTaxcoadescp());
                    n.setCredit(0.00);
                    n.setDebit(all.getTaxamt());
                    n.setJid(all.getRefer());
                    n.setLoccode(all.getLoccode());
                    n.setLocdesc(all.getLocdesc());
                    n.setLoclevel(all.getLoclevel());
                    n.setNovoucher(all.getNoteno());
                    n.setRemark(all.getRemarks());
                    n.setSacode("00");
                    n.setSadesc("Not Applicable");
                    n.setSatype("None");
                    n.setTarikh(AccountingPeriod.getCurrentTimeStamp());
                    n.setYear(dn.getYear());
                    n.setPeriod(dn.getPeriod());
                    n.setSource(sourcePost(vtype));
                    it.add(n);
                }
                break;
            }

            default:
                break;
        }

        return it;
    }

    public static String sourcePost(String vtype) {

        String source = "";

        if (vtype.equalsIgnoreCase("JV") || vtype.equalsIgnoreCase("CN") || vtype.equalsIgnoreCase("DN") || vtype.equalsIgnoreCase("AA") || vtype.equalsIgnoreCase("DT") || vtype.equalsIgnoreCase("NT")) {
            source = "GL";
        } else if (vtype.equalsIgnoreCase("GRN") || vtype.equalsIgnoreCase("SIV")) {
            source = "ST";
        } else if (vtype.equalsIgnoreCase("PNV")) {
            source = "PH";
        } else if (vtype.equalsIgnoreCase("F10")) {
            source = "CO";
        } else if (vtype.equalsIgnoreCase("CC")) {
            source = "CC";
        } else if (vtype.equalsIgnoreCase("SNV") || vtype.equalsIgnoreCase("DNR") || vtype.equalsIgnoreCase("CNR")) {
            source = "AR";
        } else {
            source = "CB";
        }

        return source;

    }

    public static ErrorPost checkBalance(Double debit, Double credit) {
        Logger.getLogger(JournalVoucherDAO.class.getName()).log(Level.INFO, String.valueOf(debit) + "@@" + String.valueOf(credit));
        ErrorPost er = new ErrorPost();

        if (GeneralTerm.currencyFormat(debit).equals(GeneralTerm.currencyFormat(credit))) {
            er.setError(false);
            er.setErrorDesc("Transaction balance");
            er.setErrorID("Post-a-2");
            er.setErrorItem("");
        } else {
            er.setError(true);
            er.setErrorDesc("Transaction not balance, Please check your voucher");
            er.setErrorID("Post-a-2");
            er.setErrorItem("");
        }

        return er;
    }

    private static ErrorPost checkValue(Double debit, Double credit) {

        ErrorPost er = new ErrorPost();
        Logger.getLogger(JournalVoucherDAO.class.getName()).log(Level.INFO, String.valueOf(debit) + "^^" + String.valueOf(credit));
        if ((debit == 0.0) && (credit == 0.0)) {
            er.setError(true);
            er.setErrorDesc("Transaction has no values");
            er.setErrorID("Post-a-3");
            er.setErrorItem("");
        } else {
            er.setError(false);
            er.setErrorDesc("Transaction has value");
            er.setErrorID("Post-a-3");
            er.setErrorItem("");
        }

        return er;
    }

    private static ErrorPost checkVtype(String vtype) {

        ErrorPost er = new ErrorPost();
        if (vtype == null || vtype.equals("")) {
            er.setError(true);
            er.setErrorID("Post-a-1");
            er.setErrorDesc("Please Supply Voucher Type");
            er.setErrorItem("Voucher Type : " + vtype);
        } else {
            er.setError(false);
            er.setErrorDesc("Voucher type is supplied");
            er.setErrorID("Post-a-1");
            er.setErrorItem("");
        }

        return er;
    }

    public static ErrorPost checkPeriod(LoginProfile log, String period) throws Exception {

        ErrorPost er = new ErrorPost();
        if (!period.equals(AccountingPeriod.getCurPeriod(log))) {
            er.setError(true);
            er.setErrorID("Post-a-4");
            er.setErrorDesc("Voucher not in current period");
            er.setErrorItem("");
        } else {
            er.setError(false);
            er.setErrorDesc("Voucher is in current period");
            er.setErrorID("Post-a-4");
            er.setErrorItem("");
        }

        return er;
    }

    private static ErrorPost checkInfo(Item mt) throws Exception {

        ErrorPost er = new ErrorPost();
        boolean exist = false;
        String field = "";
        if (mt.getCoacode().equals("") || mt.getCoacode() == null) {
            exist = true;
            field += "Account Code,";
        }
        if (mt.getCoadesc().equals("") || mt.getCoadesc() == null) {
            exist = true;
            field += "Account Name,";
        }
        if (mt.getLoccode().equals("") || mt.getLoccode() == null) {
            exist = true;
            field += "Location Code,";
        }
        if (mt.getLocdesc().equals("") || mt.getLocdesc() == null) {
            exist = true;
            field += "Location Name,";
        }
        if (mt.getLoclevel().equals("") || mt.getLoclevel() == null) {
            exist = true;
            field += "Location Level,";
        }
        if (mt.getSacode().equals("") || mt.getSacode() == null) {
            exist = true;
            field += "Sub Account Code,";
        }
        if (mt.getSadesc().equals("") || mt.getSadesc() == null) {
            exist = true;
            field += "Sub Account Name,";
        }
        if (mt.getSatype().equals("") || mt.getSatype() == null) {
            exist = true;
            field += "Sub Account Type,";
        }
        if (exist) {
            er.setError(true);
            er.setErrorID("Post-a-5");
            er.setErrorDesc("Please Check Your Journal Detail");
            er.setErrorItem(field);
        } else {
            er.setError(false);
            er.setErrorDesc("Journal Detail is complete");
            er.setErrorID("Post-a-5");
            er.setErrorItem(field);
        }

        return er;
    }

    public static boolean checkReadyToPost(LoginProfile log, String referno, String vtype) throws Exception {

        Master post = (Master) PostDAO.getInfo(log, referno, vtype);

        boolean doError = false;
        List<ErrorPost> checkError = (List<ErrorPost>) post.getListError();
        for (ErrorPost f : checkError) {
            if (f.getError()) {
                doError = true;
            }
        }

        return doError;

    }

    public static boolean isInPeriod(LoginProfile log, String referno, String vtype) throws Exception {

        Master post = (Master) PostDAO.getInfo(log, referno, vtype);

        boolean doSame = false;

        if (post.getYear().equals(AccountingPeriod.getCurYear(log)) && post.getPeriod().equals(AccountingPeriod.getCurPeriod(log))) {
            doSame = true;
        }

        return doSame;

    }

}
