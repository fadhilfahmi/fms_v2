/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.ap;

import com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.LiveGLDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.financial.ap.VendorInvoice;
import com.lcsb.fms.model.financial.ap.VendorInvoiceDetail;
import com.lcsb.fms.model.financial.ap.VendorInvoiceMaster;
import com.lcsb.fms.model.financial.ap.VendorInvoiceRefer;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class VendorInvoiceDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("021008");
        mod.setModuleDesc("Vendor Invoice");
        mod.setMainTable("ap_inv");
        mod.setReferID_Master("invrefno");
        return mod;

    }

    public static List<ListTable> tableList() {

        String column_names[] = {"invrefno", "date", "suppname", "totalamount", "checkid", "appid", "postflag"};
        String title_name[] = {"Reference No.", "Date", "Company Name", "Amount(RM)", "Check ID", " Approve ID", "Post"};
        boolean boolview[] = {true, true, true,true, false, false, false};
        //int colWidth[] = {};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            i.setBoolView(boolview[j]);
            lt.add(i);
        }

        return lt;
    }

    public static void deleteExist(LoginProfile log, String no) throws SQLException, Exception {

        String deleteQuery_1 = "delete from ap_inv where invrefno = ?";
        String deleteQuery_2 = "delete from ap_inv_detail where no = ?";
        String deleteQuery_3 = "delete from ap_inv_refer where referid = ?";

        PreparedStatement ps_1 = log.getCon().prepareStatement(deleteQuery_1);
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        PreparedStatement ps_3 = log.getCon().prepareStatement(deleteQuery_3);
        ps_1.setString(1, no);
        ps_2.setString(1, no);
        ps_3.setString(1, no);
        ps_1.executeUpdate();
        ps_2.executeUpdate();
        ps_3.executeUpdate();
        ps_1.close();
        ps_2.close();
        ps_3.close();
        
        LiveGLDAO.deleteFromGL(log, no);

    }

    public static void deleteItem(LoginProfile log, String novoucher, String refer) throws SQLException, Exception {


        String deleteQuery_2 = "delete from ap_inv_detail where invrefno= ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, novoucher);
        ps_2.executeUpdate();
        ps_2.close();
        
        LiveGLDAO.reloadGL(log, refer, "PNV");
        //updateAmount(refer);
    }

    public static String saveMain(LoginProfile log, VendorInvoice item) throws Exception {
        

        String newrefer = AutoGenerate.getReferenceNox(log, "invrefno", "ap_inv", "PNV", item.getEstatecode(), item.getYear(), item.getPeriod());
        try {
            //ps.setString(1, AutoGenerate.get2digitNo("info_bank", "code"));
            String q = ("insert into ap_inv(invrefno,invno,year,period,date,estatecode,estatename,suppcode,suppname,suppaddress,remark,preid,prename,predate,accode,acdesc,porefno,nogrn,noinv,invtype,totalamount,satype,sacode,sadesc,gstid,racoacode,racoadesc) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, newrefer);
            ps.setString(2, AutoGenerate.get4digitNo(log, "ap_inv", "invno"));
            ps.setString(3, item.getYear());
            ps.setString(4, item.getPeriod());
            ps.setString(5, item.getDate());
            ps.setString(6, item.getEstatecode());
            ps.setString(7, item.getEstatename());
            ps.setString(8, item.getSuppcode());
            ps.setString(9, item.getSuppname());
            ps.setString(10, item.getSuppaddress());
            ps.setString(11, item.getRemark());
            ps.setString(12, item.getPreid());
            ps.setString(13, item.getPrename());
            ps.setString(14, item.getPredate());
            ps.setString(15, item.getAccode());
            ps.setString(16, item.getAcdesc());
            ps.setString(17, item.getPorefno());
            ps.setString(18, item.getNogrn());
            ps.setString(19, item.getNoinv());
            ps.setString(20, item.getInvtype());
            ps.setDouble(21, item.getTotalamount());
            ps.setString(22, "Supplier");
            ps.setString(23, item.getSuppcode());
            ps.setString(24, item.getSuppname());
            ps.setString(25, item.getGstid());
            ps.setString(26, item.getRacoacode());
            ps.setString(27, item.getRacoadesc());

            ps.executeUpdate();
            Logger.getLogger(VendorInvoiceDAO.class.getName()).log(Level.INFO, "-----------" + String.valueOf(ps));
            ps.close();
            
            LiveGLDAO.reloadGL(log, newrefer, "PNV");
            
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newrefer;

    }

    public static void saveItem(LoginProfile log, VendorInvoiceDetail item) throws Exception {

        try {
            String q = ("insert into ap_inv_detail(invrefno,no,matcode,matdesc,quantity,unitprice,unitmeasure,amount,remark,loclevel,loccode,locdesc,accode,acdesc,varian,pounitmeasure,pounitprice,poquantity,poamount,taxrate,taxamt,taxcode,taxdescp,taxcoacode,taxcoadescp,satype,sacode,sadesc) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, AutoGenerate.getReferAddBack(log, "ap_inv_detail", "invrefno", item.getNo(), "no"));
            ps.setString(2, item.getNo());
            ps.setString(3, item.getMatcode());
            ps.setString(4, item.getMatdesc());
            ps.setString(5, item.getQuantity());
            ps.setString(6, item.getUnitprice());
            ps.setString(7, item.getUnitmeasure());
            ps.setDouble(8, item.getAmount());
            ps.setString(9, item.getRemark());
            ps.setString(10, item.getLoclevel());
            ps.setString(11, item.getLoccode());
            ps.setString(12, item.getLocdesc());
            ps.setString(13, item.getAccode());
            ps.setString(14, item.getAcdesc());
            ps.setString(15, item.getVarian());
            ps.setString(16, item.getPounitmeasure());
            ps.setString(17, item.getPounitprice());
            ps.setString(18, item.getPoquantity());
            ps.setDouble(19, 0.0);
            ps.setDouble(20, item.getTaxrate());
            ps.setDouble(21, item.getTaxamt());
            ps.setString(22, item.getTaxcode());
            ps.setString(23, item.getTaxdescp());
            ps.setString(24, item.getTaxcoacode());
            ps.setString(25, item.getTaxcoadescp());
            ps.setString(26, item.getSatype());
            ps.setString(27, item.getSacode());
            ps.setString(28, item.getSadesc());

            ps.executeUpdate();
            Logger.getLogger(VendorInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();
            
            LiveGLDAO.reloadGL(log, item.getNo(), "PNV");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateMain(LoginProfile log, VendorInvoice m, String id) throws Exception {


        try {
            //String q = ("insert into gl_jv_item(Jvid,JVrefno,remark,actcode,actdesc,loccode,locdesc,loclevel,satype,sacode,sadesc,debit,credit,taxcode,taxdescp,taxrate,gstid,amtbeforetax) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            String q = ("UPDATE ap_inv set year = ?,period = ?,date = ?,estatecode = ?,estatename = ?,suppcode = ?,suppname = ?,suppaddress = ?,remark = ?,accode = ?,acdesc = ?,porefno = ?,nogrn = ?,noinv = ?,invtype = ?,totalamount = ?,satype = ?,sacode = ?,sadesc = ?,gstid = ?,racoacode = ?,racoadesc = ? WHERE invrefno = '" + id + "'");
            //amtbeforetax=?
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, m.getYear());
            ps.setString(2, m.getPeriod());
            ps.setString(3, m.getDate());
            ps.setString(4, m.getEstatecode());
            ps.setString(5, m.getEstatename());
            ps.setString(6, m.getSuppcode());
            ps.setString(7, m.getSuppname());
            ps.setString(8, m.getSuppaddress());
            ps.setString(9, m.getRemark());
            ps.setString(10, m.getAccode());
            ps.setString(11, m.getAcdesc());
            ps.setString(12, m.getPorefno());
            ps.setString(13, m.getNogrn());
            ps.setString(14, m.getNoinv());
            ps.setString(15, m.getInvtype());
            ps.setDouble(16, m.getTotalamount());
            ps.setString(17, "Supplier");
            ps.setString(18, m.getSuppcode());
            ps.setString(19, m.getSuppname());
            ps.setString(20, m.getGstid());
            ps.setString(21, m.getRacoacode());
            ps.setString(22, m.getRacoadesc());

            Logger.getLogger(VendorInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();

            ps.close();
            
            LiveGLDAO.reloadGL(log, id, "PNV");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateItem(LoginProfile log, VendorInvoiceDetail item, String id) throws Exception {

        try {
            String q = ("UPDATE ap_inv_detail set matcode = ?,matdesc = ?,quantity = ?,unitprice = ?,unitmeasure = ?,amount = ?,remark = ?,loclevel = ?,loccode = ?,locdesc = ?,accode = ?,acdesc = ?,varian = ?,taxrate = ?,taxamt = ?,taxcode = ?,taxdescp = ?,taxcoacode = ?,taxcoadescp = ?,satype = ?,sacode = ?,sadesc = ? WHERE invrefno = '" + id + "'");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getMatcode());
            ps.setString(2, item.getMatdesc());
            ps.setString(3, item.getQuantity());
            ps.setString(4, item.getUnitprice());
            ps.setString(5, item.getUnitmeasure());
            ps.setDouble(6, item.getAmount());
            ps.setString(7, item.getRemark());
            ps.setString(8, item.getLoclevel());
            ps.setString(9, item.getLoccode());
            ps.setString(10, item.getLocdesc());
            ps.setString(11, item.getAccode());
            ps.setString(12, item.getAcdesc());
            ps.setString(13, item.getVarian());
            ps.setDouble(14, item.getTaxrate());
            ps.setDouble(15, item.getTaxamt());
            ps.setString(16, item.getTaxcode());
            ps.setString(17, item.getTaxdescp());
            ps.setString(18, item.getTaxcoacode());
            ps.setString(19, item.getTaxcoadescp());
            ps.setString(20, item.getSatype());
            ps.setString(21, item.getSacode());
            ps.setString(22, item.getSatype());
            Logger.getLogger(VendorInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();

            ps.close();
            
            LiveGLDAO.reloadGL(log, id, "PNV");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static String getCVno(LoginProfile log, String abb) throws Exception {

        ResultSet rs = null;
        String CVno = "";
        String year = AccountingPeriod.getCurYear(log);
        String curperiod = AccountingPeriod.getCurPeriod(log);
        //String estatecode = LoginProfile.getEstateCode();
        String estatecode = "9911";

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat(?,'" + estatecode + "',substring('" + year + "',3,2),lpad('" + curperiod + "',2,'0'),lpad((max(substring(refer,14,4))+1),4,'0')),concat('JPY','" + estatecode + "',substring('" + year + "',3,2),lpad('" + curperiod + "',2,'0'),'0001')) as refer from cb_cashvoucher where refer like concat(?,'%') and year='" + year + "' and period='" + curperiod + "' and estatecode like concat('" + estatecode + "','%')");
        stmt.setString(1, abb);
        stmt.setString(2, abb);

        rs = stmt.executeQuery();
        if (rs.next()) {
            CVno = rs.getString(1);

        }

        rs.close();
        stmt.close();
        return CVno;

    }

    public static String getCVoucherNo(LoginProfile log) throws Exception {

        ResultSet rs = null;
        String CVno = "";

        PreparedStatement stmt = log.getCon().prepareStatement("SELECT ifnull(concat(lpad((max(voucherid)+1),5,'0')),concat('0001')) as new FROM `cb_cashvoucher`");

        rs = stmt.executeQuery();
        if (rs.next()) {
            CVno = rs.getString(1);
        }

        rs.close();
        stmt.close();
        return CVno;

    }

    public static String getCVid(LoginProfile log, String refno) throws Exception, SQLException {

        ResultSet rs = null;
        String JVno = "";

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat('" + refno + "',lpad((max(right(novoucher,4))+1),4,'0')),concat('" + refno + "','0001')) as njvno from cb_cashvoucher_account where refer='" + refno + "'");

        rs = stmt.executeQuery();
        if (rs.next()) {
            JVno = rs.getString(1);
        }

        rs.close();
        stmt.close();
        return JVno;

    }

    public static Double getSum(LoginProfile log, String column, String id) throws Exception {
        Double x = 0.00;
        Statement stment = log.getCon().createStatement();
        ResultSet setent = stment.executeQuery(" select sum(" + column + ") as sumamount from cb_cashvoucher_account where refer = '" + id + "'");

        while (setent.next()) {
            x += setent.getDouble("sumamount");
        }

        stment.close();
        setent.close();
        return x;
    }

    public static Double getBalance(LoginProfile log, String column, String id) throws Exception {
        Double x = 0.00;
        Double y = 0.00;
        Statement stment = log.getCon().createStatement();
        ResultSet setent = stment.executeQuery(" select sum(" + column + ") as sumamount from cb_cashvoucher_account where refer = '" + id + "'");

        while (setent.next()) {
            x += setent.getDouble("sumamount");
        }

        setent = stment.executeQuery(" select total from cb_cashvoucher where refer = '" + id + "'");

        if (setent.next()) {
            y = setent.getDouble("total");
        }

        stment.close();
        setent.close();
        return y - x;
    }

    public static VendorInvoiceDetail getItem(String id) throws Exception {

        VendorInvoiceDetail v = new VendorInvoiceDetail();

        //v.setVoucer(id);
        //v.setAmtbeforetax(getBalance("debit",id));
        return v;
    }

    private static void updateAmount(LoginProfile log, String JVrefno) throws Exception {
        
        String query = "UPDATE gl_jv SET todebit = '" + getSum(log, "debit", JVrefno) + "',tocredit = '" + getSum(log, "credit", JVrefno) + "' where JVrefno = '" + JVrefno + "'";

        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }

    public static List<VendorInvoiceDetail> getAllPNVItem(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<VendorInvoiceDetail> CVi;
        CVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from ap_inv_detail where no = '" + refno + "' order by no");

            while (rs.next()) {
                CVi.add(getPNVitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static VendorInvoiceDetail getPNVitem(LoginProfile log, String id) throws SQLException, Exception {
        VendorInvoiceDetail c = null;
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from ap_inv_detail where invrefno=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getPNVitem(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    private static VendorInvoiceDetail getPNVitem(ResultSet rs) throws SQLException {
        VendorInvoiceDetail cv = new VendorInvoiceDetail();

        cv.setAccode(rs.getString("accode"));
        cv.setAcdesc(rs.getString("acdesc"));
        cv.setAmount(rs.getDouble("amount"));
        cv.setBil(rs.getLong("bil"));
        cv.setConversion(rs.getString("conversion"));
        cv.setInvrefno(rs.getString("invrefno"));
        cv.setLoccode(rs.getString("loccode"));
        cv.setLocdesc(rs.getString("locdesc"));
        cv.setLoclevel(rs.getString("loclevel"));
        cv.setMatcode(rs.getString("matcode"));
        cv.setMatdesc(rs.getString("matdesc"));
        cv.setNo(rs.getString("no"));
        cv.setPoamount(rs.getDouble("poamount"));
        cv.setPoquantity(rs.getString("poquantity"));
        cv.setPounitmeasure(rs.getString("pounitmeasure"));
        cv.setPounitprice(rs.getString("pounitprice"));
        cv.setQuantity(rs.getString("quantity"));
        cv.setRemark(rs.getString("remark"));
        cv.setSacode(rs.getString("sacode"));
        cv.setSadesc(rs.getString("sadesc"));
        cv.setSatype(rs.getString("satype"));
        cv.setTaxamt(rs.getDouble("taxamt"));
        cv.setTaxcoacode(rs.getString("taxcoacode"));
        cv.setTaxcoadescp(rs.getString("taxcoadescp"));
        cv.setTaxcode(rs.getString("taxcode"));
        cv.setTaxdescp(rs.getString("taxdescp"));
        cv.setTaxrate(rs.getDouble("taxrate"));
        cv.setUnitmeasure(rs.getString("unitmeasure"));
        cv.setUnitprice(rs.getString("unitprice"));
        cv.setVarian(rs.getString("varian"));

        return cv;
    }

    public static List<VendorInvoice> getAllPNV(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<VendorInvoice> CV;
        CV = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from ap_inv where invrefno = '" + refno + "' order by invrefno");

            while (rs.next()) {
                CV.add(getPNV(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static VendorInvoice getPNV(LoginProfile log, String refno) throws SQLException, Exception {
        VendorInvoice c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from ap_inv where invrefno=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getPNV(rs);
        }

        return c;
    }

    private static VendorInvoice getPNV(ResultSet rs) throws SQLException {

        VendorInvoice cv = new VendorInvoice();

        cv.setAccode(rs.getString("accode"));
        cv.setAcdesc(rs.getString("acdesc"));
        cv.setAppdate(rs.getString("appdate"));
        cv.setAppdesig(rs.getString("appdesig"));
        cv.setAppid(rs.getString("appid"));
        cv.setAppname(rs.getString("appname"));
        cv.setCheckdate(rs.getString("checkdate"));
        cv.setCheckdesig(rs.getString("checkdesig"));
        cv.setCheckid(rs.getString("checkid"));
        cv.setCheckname(rs.getString("checkname"));
        cv.setDate(rs.getString("date"));
        cv.setInvno(rs.getString("invno"));
        cv.setInvrefno(rs.getString("invrefno"));
        cv.setInvtype(rs.getString("invtype"));
        cv.setNogrn(rs.getString("nogrn"));
        cv.setNoinv(rs.getString("noinv"));
        cv.setPaid(rs.getDouble("paid"));
        cv.setPeriod(rs.getString("period"));
        cv.setPorefno(rs.getString("porefno"));
        cv.setPostdate(rs.getString("postdate"));
        cv.setPostflag(rs.getString("postflag"));
        cv.setPredate(rs.getString("predate"));
        cv.setPreid(rs.getString("preid"));
        cv.setPrename(rs.getString("prename"));
        cv.setRacoacode(rs.getString("racoacode"));
        cv.setRacoadesc(rs.getString("racoadesc"));
        cv.setRemark(rs.getString("remark"));
        cv.setSuppaddress(rs.getString("suppaddress"));
        cv.setSuppcode(rs.getString("suppcode"));
        cv.setSuppname(rs.getString("suppname"));
        cv.setTotalamount(rs.getDouble("totalamount"));
        cv.setYear(rs.getString("year"));
        cv.setEstatecode(rs.getString("estatecode"));
        cv.setEstatename(rs.getString("estatename"));
        cv.setGstid(rs.getString("gstid"));
        cv.setSacode(rs.getString("sacode"));
        cv.setSadesc(rs.getString("sadesc"));
        cv.setSatype(rs.getString("satype"));

        return cv;
    }

    public static double getDebit(LoginProfile log, String refno) throws SQLException, Exception {
        double amount = 0;
        ResultSet rs = null;
        ResultSet rs1 = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select sum(amount+taxamt) as amt from ap_inv_detail where no=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amount = rs.getDouble("amt");
        }

        return amount;
    }

    public static double getCredit(LoginProfile log, String refno) throws SQLException, Exception {
        double amt = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from ap_inv  where invrefno = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amt = rs.getDouble("totalamount");
        }

        return amt;
    }

    public static int getCheckCounter(LoginProfile log) throws Exception {

        int cnt = 0;
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from sl_inv where year = ? and period = ? and cid = ''");

        stmt.setString(1, AccountingPeriod.getCurYear(log));
        stmt.setString(2, AccountingPeriod.getCurPeriod(log));
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }

        return cnt;
    }

    public static int getApproveCounter(LoginProfile log) throws Exception {

        int cnt = 0;
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from sl_inv where year = ? and period = ? and cid <> ? and pid = ?");

        stmt.setString(1, AccountingPeriod.getCurYear(log));
        stmt.setString(2, AccountingPeriod.getCurPeriod(log));
        stmt.setString(3, "");
        stmt.setString(4, "");
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }

        return cnt;
    }

    public static void checkVoucher(LoginProfile log, String refer, String staff_id, String staff_name) throws Exception {
        
        String query = "UPDATE ap_inv SET checkid = '" + staff_id + "',checkname = '" + staff_name + "',checkdate='" + AccountingPeriod.getCurrentTimeStamp() + "' where invrefno = '" + refer + "'";

        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }

    public static void approveVoucher(LoginProfile log, String refer, String staff_id, String staff_name, String position) throws Exception {
        
        String query = "UPDATE ap_inv SET appid = '" + staff_id + "',appname = '" + staff_name + "',appdate='" + AccountingPeriod.getCurrentTimeStamp() + "', appdesig = '" + position + "' where invrefno = '" + refer + "'";

        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }

    public static String getStatus(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String check = "";
        String app = "";
        String post = "";
        String status = "<span class=\"label label-primary\">Preparing</span>";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select checkid,appid,postflag from ap_inv where invrefno = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            check = rs.getString("checkid");
            app = rs.getString("appid");
            post = rs.getString("postflag");
        }

        stmt.close();
        rs.close();

        if (!check.equals("")) {
            status = "Checked";
        }

        if (!app.equals("")) {
            status = "Approved";
        }

//        if(post.equals("posted")){
//            status = "<span class=\"label label-info\">Post</span>";
//        }
        if (post.equals("Cancel")) {
            status = "Posted";
        }

        return status;
    }

    public static boolean isCheck(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select checkid from ap_inv where invrefno = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString("checkid");
        }

        if (!ck.equals("")) {
            cek = true;
        }

        stmt.close();
        rs.close();

        return cek;
    }

    public static boolean isApprove(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select appid from ap_inv where invrefno = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString("appid");
        }

        if (!ck.equals("")) {
            cek = true;
        }

        stmt.close();
        rs.close();

        return cek;
    }

    public static List<VendorInvoiceRefer> getAllRefer(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<VendorInvoiceRefer> pvr;
        pvr = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from ap_inv_refer where refer = '" + refno + "' order by referid");

            while (rs.next()) {
                pvr.add(getRefer(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return pvr;
    }

    private static VendorInvoiceRefer getRefer(ResultSet rs) throws SQLException {
        VendorInvoiceRefer vr = new VendorInvoiceRefer();

        vr.setAmount(rs.getDouble("amount"));
        vr.setNo(rs.getString("no"));
        vr.setRefer(rs.getString("refer"));
        vr.setReferid(rs.getString("referid"));
        vr.setRemark(rs.getString("remark"));
        vr.setTarikh(rs.getString("tarikh"));
        vr.setType(rs.getString("type"));

        return vr;
    }

    public static void saveRefer(LoginProfile log, VendorInvoiceRefer item) throws Exception {

        try {
            String q = ("insert into ap_inv_refer(amount,no,refer,referid,remark,tarikh,type) values (?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setDouble(1, item.getAmount());
            ps.setString(2, item.getNo());
            ps.setString(3, item.getRefer());
            ps.setString(4, AutoGenerate.getReferAddBack(log, "ap_inv_refer", "referid", item.getRefer(), "refer"));
            ps.setString(5, item.getRemark());
            ps.setString(6, item.getTarikh());
            ps.setString(7, item.getType());

            ps.executeUpdate();
            Logger.getLogger(VendorInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateRefer(LoginProfile log, VendorInvoiceRefer item, String id) throws Exception {

        try {
            String q = ("UPDATE ap_inv_refer set amount = ?,no = ?,refer = ?,referid = ?,remark = ?,tarikh = ?,type = ? WHERE referid = '" + id + "'");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setDouble(1, item.getAmount());
            ps.setString(2, item.getNo());
            ps.setString(3, item.getRefer());
            ps.setString(4, item.getReferid());
            ps.setString(5, item.getRemark());
            ps.setString(6, item.getTarikh());
            ps.setString(7, item.getType());

            ps.executeUpdate();
            Logger.getLogger(VendorInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static VendorInvoiceRefer getReferEach(LoginProfile log, String id) throws SQLException, Exception {
        VendorInvoiceRefer c = new VendorInvoiceRefer();
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from ap_inv_refer where referid=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getRefer(rs);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

//        if(c == null){
//                
//        }
        return c;
    }

    public static void deleteRefer(LoginProfile log, String novoucher, String refer) throws SQLException, Exception {

        try {
            String deleteQuery_2 = "delete from ap_inv_refer where referid = ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, novoucher);
            Logger.getLogger(VendorInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps_2));
            ps_2.executeUpdate();
            ps_2.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        //updateAmount(refer);

    }

    public static double getRoundingAmount(LoginProfile log, String id) throws SQLException, Exception {
        double total = 0;
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select sum(amount+taxamt) as amt from ap_inv_detail where no=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                total = rs.getDouble("amt");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return total;
    }

    public static List<VendorInvoiceDetail> getAllTax(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<VendorInvoiceDetail> CVi;
        CVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs;
            rs = stmt.executeQuery("select * from ap_inv_detail where no = '" + refno + "' and taxamt<>0 order by invrefno");

            while (rs.next()) {
                CVi.add(VendorInvoiceDAO.getPNVitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<VendorInvoice> getNoPaidPNV(LoginProfile log, String code) throws Exception {

        Statement stmt = null;
        List<VendorInvoice> CV;
        CV = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from ap_inv where totalamount > paid and suppcode = '" + code + "' and postflag = 'posted' order by invrefno desc");

            while (rs.next()) {
                
                VendorInvoice vi = getPNV(rs);
                //if(rs.getDouble("totalamount") > rs.getDouble("paid")){
                
                vi.setTotalamount(rs.getDouble("totalamount") - rs.getDouble("paid"));
                CV.add(vi);
                
               
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }
    
    public static List<VendorInvoiceDetail> getAllPNVItemByPage(LoginProfile log, String refno, int row, int limit) throws Exception {

        Statement stmt = null;
        List<VendorInvoiceDetail> CVi;
        CVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from ap_inv_detail where no = '" + refno + "' order by invrefno asc limit " + row + "," + limit);

            while (rs.next()) {
                CVi.add(getPNVitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static VendorInvoiceMaster getMaster(LoginProfile log, String refno) throws Exception {
        
        VendorInvoiceMaster pm = new VendorInvoiceMaster();
        
        pm.setApMain(VendorInvoiceDAO.getPNV(log, refno));
        pm.setApList(VendorInvoiceDAO.getAllPNVItem(log, refno));
        
        return pm;
        
    }
    
    public static boolean isComplete(LoginProfile log, String refno) throws Exception{
        
        boolean s = true;
        VendorInvoiceMaster pm = (VendorInvoiceMaster) getMaster(log, refno);
        
        VendorInvoice p = (VendorInvoice) pm.getApMain();
        
        if(p.getTotalamount()<= 0.0){
            s = false;
        }else if(p.getSuppcode().equals("") || p.getSuppcode() == null){
            s = false;
        }else if(p.getAccode().equals("") || p.getAccode() == null){
            s = false;
        }else if(p.getRemark().equals("") || p.getRemark()== null){
            s = false;
        }
        
        return s;
    }
    
    public static boolean hasDCNote(LoginProfile log, String refno) throws Exception{
        
        boolean s = false;
        
        VendorInvoiceMaster pm = (VendorInvoiceMaster) getMaster(log, refno);
        
        List<VendorInvoiceDetail> listx = (List<VendorInvoiceDetail>) pm.getApList();
        for (VendorInvoiceDetail j : listx) {
            
            s = DebitCreditNoteDAO.checkSuspence(log, j.getAccode());
            
        }
        
        return s;
    }
    
    public static String replicateData(LoginProfile log, String refno, String replicateDate, String replicateYear, String replicatePeriod) throws Exception {
        String refer = "";
        
        VendorInvoiceMaster pm = (VendorInvoiceMaster) getMaster(log, refno);
        pm.getApMain().setPeriod(replicatePeriod);
        pm.getApMain().setYear(replicateYear);
        pm.getApMain().setDate(replicateDate);
        //pm.getPvMain().setPaymentmode("None");
        //pm.getPvMain().setBankcode("");
        //pm.getPvMain().setBankname("");
        pm.getApMain().setPreid(log.getUserID());
        pm.getApMain().setPrename(log.getFullname());
        pm.getApMain().setPredate(AccountingPeriod.getCurrentTimeStamp());
        
        refer = VendorInvoiceDAO.saveMain(log, pm.getApMain());
        
        List<VendorInvoiceDetail> listx = (List<VendorInvoiceDetail>) pm.getApList();
        for (VendorInvoiceDetail j : listx) {
            
            j.setNo(refer);
            VendorInvoiceDAO.saveItem(log, j);
            
        }
        
        return refer;
    }

}
