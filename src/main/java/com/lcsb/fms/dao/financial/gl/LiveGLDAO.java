/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.gl;

import com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO;
import static com.lcsb.fms.dao.financial.gl.PostDAO.sourcePost;
import com.lcsb.fms.financial.gl.post.Item;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.cashbook.OfficialCreditItem;
import com.lcsb.fms.model.financial.cashbook.OfficialReceipt;
import com.lcsb.fms.report.financial.gl.GListDAO;
import com.lcsb.fms.report.financial.gl.GListDetail;
import com.lcsb.fms.report.financial.gl.GListHeader;
import com.lcsb.fms.report.financial.gl.GListMaster;
import com.lcsb.fms.report.financial.gl.GListParam;
import com.lcsb.fms.util.dao.BankDAO;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class LiveGLDAO {
    
    public static void reloadGL(LoginProfile log, String voucherno, String type) throws Exception {
        
        if(voucherno.length() > 15){
            voucherno = voucherno.substring(0, 15);
        }

        List<Item> it = PostDAO.getList(log, voucherno, type);

        LiveGLDAO.executeLiveDistribution(log, it, voucherno);

    }

    private static void executeLiveDistribution(LoginProfile log, List<Item> listItem, String refer) throws Exception {

        try {
            LiveGLDAO.deleteFromGL(log, refer);
            boolean hasDeleted = false;
            String tempRefer = "";

            for (Item it : listItem) {

//                if(!hasDeleted && !tempRefer.equals(it.getNovoucher())){
//                    LiveGLDAO.deleteForUpdate(log, it.getNovoucher());
//                    tempRefer = it.getNovoucher();
//                    hasDeleted = true;
//                }
                

                String q = ("INSERT INTO gl_posting_temp(tarikh,year,period,coacode,coadesc,loclevel,loccode,locdesc,satype,sacode,sadesc,remark,novoucher,source,debit,credit,postdate) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                PreparedStatement ps = log.getCon().prepareStatement(q);
                ps.setString(1, it.getTarikh());
                ps.setString(2, it.getYear());
                ps.setString(3, it.getPeriod());
                ps.setString(4, it.getCoacode());
                ps.setString(5, it.getCoadesc());
                ps.setString(6, it.getLoclevel());
                ps.setString(7, it.getLoccode());
                ps.setString(8, it.getLocdesc());
                ps.setString(9, it.getSatype());
                ps.setString(10, it.getSacode());
                ps.setString(11, it.getSadesc());
                ps.setString(12, it.getRemark());
                ps.setString(13, it.getNovoucher());
                ps.setString(14, it.getSource());
                ps.setDouble(15, it.getDebit());
                ps.setDouble(16, it.getCredit());
                ps.setString(17, AccountingPeriod.getCurrentTimeStamp());
                Logger.getLogger(DistributeDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
                ps.executeUpdate();

                ps.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteFromGL(LoginProfile log, String refer) throws SQLException, Exception {

        String deleteQuery_2 = "delete from gl_posting_temp where NoVoucher LIKE ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        
        
        ps_2.setString(1, refer + "%");
        ps_2.executeUpdate();
        Logger.getLogger(LiveGLDAO.class.getName()).log(Level.INFO, String.valueOf(ps_2));
        ps_2.close();

        //updateAmount(refer);
    }

    public static void deleteForUpdate(LoginProfile log, String refer) throws SQLException, Exception {

        String deleteQuery_2 = "delete from gl_posting_temp where NoVoucher = ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, refer);
        ps_2.executeUpdate();
        ps_2.close();

        //updateAmount(refer);
    }
    
    public static GListMaster generateGListing(LoginProfile log, String refer) throws Exception{
        
        GListMaster gl = new GListMaster();
        
        gl.setListCore(getListCode(log, refer));
        gl.setTotalDebit(getTotal(log, "debit", refer));
        gl.setTotalCredit(getTotal(log, "credit", refer));
        gl.setGltitle("GENERAL LEDGER LISTING");
        gl.setGlcodetocode("FROM ACCOUNT ");
        
        return gl;
        
    }

    public static List<GListHeader> getListCode(LoginProfile log, String refer) throws Exception {

        Connection con = log.getCon();
        List<GListHeader> lt = new ArrayList();

       

        String query = "SELECT coacode,coadesc,satype,sacode,sadesc from gl_posting_temp where NoVoucher = '"+refer+"'  group by coacode,satype,sacode";
        
        Logger.getLogger(GListDAO.class.getName()).log(Level.INFO, "-----------"+query);
        
        ResultSet rs = null;
        PreparedStatement stmt = con.prepareStatement(query);
        //stmt.setString(1, refno);
        rs = stmt.executeQuery();
        while (rs.next()) {

            GListHeader cv = new GListHeader();

            cv.setCoacode(rs.getString("coacode"));
            cv.setCoadescp(rs.getString("coadesc"));
            cv.setSacode(rs.getString("sacode"));
            cv.setSadesc(rs.getString("sadesc"));
            cv.setSatype(rs.getString("satype"));
            cv.setBalance(0.00);
            cv.setListDetail(getListDetail(log, refer, cv.getCoacode(), cv.getSatype(), cv.getSacode(), cv.getBalance()));
            
            lt.add(cv);

        }

        stmt.close();
        rs.close();

        return lt;
    }
    
    public static List<GListDetail> getListDetail(LoginProfile log, String refer, String code, String satype, String sacode, double initbalance) throws Exception {

        Connection con = log.getCon();
        List<GListDetail> lt = new ArrayList();

        String query = "select year,period,tarikh,source,remark,novoucher,ifnull(sum(debit),0) as debit,ifnull(sum(credit),0) as credit from gl_posting_temp where coacode='"+ code +"' and satype='"+ satype +"' and sacode='"+ sacode +"'  and NoVoucher = '"+ refer +"'  group by novoucher,coacode,sacode,remark  order by period,year,tarikh"; 
        
        Logger.getLogger(GListDAO.class.getName()).log(Level.INFO, " ---- " +query);
        double netchange = 0.00;
        ResultSet rs = null;
        PreparedStatement stmt = con.prepareStatement(query);
        //stmt.setString(1, refno);
        rs = stmt.executeQuery();
        while (rs.next()) {

            GListDetail cv = new GListDetail();
            
            if(rs.getDouble("debit")==0){
                cv.setDebit("");
            }else{
                cv.setDebit(GeneralTerm.normalCredit(rs.getDouble("debit")));
            }
            
            if(rs.getDouble("credit")==0){
                cv.setCredit("");
            }else{
                cv.setCredit(GeneralTerm.normalCredit(rs.getDouble("credit")));
            }

            cv.setNovoucher(rs.getString("novoucher"));
            cv.setPeriod(rs.getString("period"));
            cv.setRemark(rs.getString("remark"));
            cv.setSource(rs.getString("source"));
            cv.setTarikh(rs.getString("tarikh"));
            cv.setYear(rs.getString("year"));
            netchange+=(rs.getDouble("debit")-rs.getDouble("credit"));
            if(rs.isLast()){
               cv.setNetchange(GeneralTerm.normalCredit(netchange)); 
               cv.setLastbalance(GeneralTerm.normalCredit(netchange + initbalance));
               cv.setStar("<i class=\"fa fa-flag-checkered\" aria-hidden=\"true\"></i>");
            }else{
                cv.setNetchange("");
                cv.setLastbalance("");
                cv.setStar("");
            }
            
            
            lt.add(cv);

        }

        stmt.close();
        rs.close();

        return lt;
    }
    
    private static double getBalance(LoginProfile log, String coacode, String satype, String sacode) throws SQLException, Exception {

        double bal = 0.00;

        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(sum(debit-credit),0) as jum from gl_openingbalance where coacode='" + coacode + "' and satype='" + satype + "' and sacode='" + sacode + "'");
        //stmt.setString(1, refno);
        rs = stmt.executeQuery();
        while (rs.next()) {

            bal = rs.getDouble("jum");
        }

        stmt.close();
        rs.close();

        return bal;

    }
    
    private static double getTotal(LoginProfile log, String type, String refer) throws Exception{
        
        
        double bal = 0.00;

        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(sum("+type+"),0) as "+type+",ifnull(sum("+type+"),0) as "+type+" from gl_posting_temp where NoVoucher = '"+refer+"'");
        //stmt.setString(1, refno);
        rs = stmt.executeQuery();
        while (rs.next()) {

            bal = rs.getDouble(type);
        }
        
        stmt.close();
        rs.close();
        
        return bal;
        
    }

}
