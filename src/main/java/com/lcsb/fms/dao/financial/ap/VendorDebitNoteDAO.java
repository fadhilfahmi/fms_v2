/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.ap;

import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.financial.ap.VendorDebitNote;
import com.lcsb.fms.model.financial.ap.VendorDebitNoteItem;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class VendorDebitNoteDAO {
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("021006");
        mod.setModuleDesc("Vendor Debit Note");
        mod.setMainTable("ap_debitnote");
        mod.setReferID_Master("noteno");
        return mod;
         
    }
    public static List<ListTable> tableList(){
        
        String column_names[] = {"noteno","notedate","receivercode","receivername","approveid","postflag"};
        String title_name[] = {"Debit Note No.","Date","Receiver Code","Receiver Name"," Approve ID","Post"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
    
    public static void deleteExist(LoginProfile log, String no) throws SQLException, Exception{
        
        String deleteQuery_1 = "delete from ap_debitnote where noteno = ?";
        String deleteQuery_2 = "delete from ap_debitnote_item where noteno = ?";
        PreparedStatement ps_1 = log.getCon().prepareStatement(deleteQuery_1);
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_1.setString(1, no);
        ps_2.setString(1, no);
        ps_1.executeUpdate();
        ps_2.executeUpdate();
        ps_1.close();
        ps_2.close();
        
        
    }
     
     public static void deleteItem(LoginProfile log, String novoucher, String refer) throws SQLException, Exception{
         
        
        String deleteQuery_2 = "delete from ap_debitnote_item where noteno= ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, novoucher);
        ps_2.executeUpdate();
        ps_2.close();
        
        updateAmount(log, refer);
        
        
    }
    
     public static void saveMain(LoginProfile log, VendorDebitNote item) throws Exception{
        Logger.getLogger(VendorInvoiceDAO.class.getName()).log(Level.INFO, "-----------"+String.valueOf(item));
        try{
             //ps.setString(1, AutoGenerate.get2digitNo("info_bank", "code"));
            String q = ("insert into ap_debitnote(noteno,notedate,year,period,acccode,accdesc,estcode,estname,remark,total,postflag,prepareid,preparename,preparedate,receivercode,receivername,satype,sacode,sadesc) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, AutoGenerate.getReferenceNo(log, "noteno", "ap_debitnote", "DBN", item.getEstcode()));
            ps.setString(2, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(3, item.getYear());
            ps.setString(4, item.getPeriod());
            ps.setString(5, item.getAcccode());
            ps.setString(6, item.getAccdesc());
            ps.setString(7, item.getEstcode());
            ps.setString(8, item.getEstname());
            ps.setString(9, item.getRemark());
            ps.setDouble(10, item.getTotal());
            ps.setString(11, item.getPostflag());
            ps.setString(12, item.getPrepareid());
            ps.setString(13, item.getPreparename());
            ps.setString(14, item.getPreparedate());
            ps.setString(15, item.getReceivercode());
            ps.setString(16, item.getReceivername());
            ps.setString(17, item.getSatype());
            ps.setString(18, item.getSacode());
            ps.setString(19, item.getSadesc());
            
            ps.executeUpdate();

            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
    
    public static void saveItem(LoginProfile log, VendorDebitNoteItem item) throws Exception{
        Logger.getLogger(VendorDebitNoteDAO.class.getName()).log(Level.INFO, "-------mmmmmmmmmmmmmmmmmmmmmmmmm----");
        
        try{
            String q = ("insert into ap_debitnote_item(noteno,descp,quantity,unitprice,amount,date,refer,reference) values (?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getNoteno());
            ps.setString(2, item.getDescp());
            ps.setDouble(3, item.getQuantity());
            ps.setDouble(4, item.getUnitprice());
            ps.setDouble(5, item.getAmount());
            ps.setString(6, item.getDate());
            ps.setString(7, AutoGenerate.getReferAddBack(log, "ap_debitnote_item", "refer", item.getNoteno(), "noteno"));
            ps.setString(8, item.getReference());
           
            ps.executeUpdate();
            Logger.getLogger(VendorDebitNoteDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

       } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void updateMain(LoginProfile log, VendorDebitNote item, String id) throws Exception{
        
        
        try{
            //String q = ("insert into gl_jv_item(Jvid,JVrefno,remark,actcode,actdesc,loccode,locdesc,loclevel,satype,sacode,sadesc,debit,credit,taxcode,taxdescp,taxrate,gstid,amtbeforetax) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            String q = ("UPDATE ap_debitnote set noteno = ?,notedate = ?,year = ?,period = ?,acccode = ?,accdesc = ?,estcode = ?,estname = ?,remark = ?,total = ?,postflag = ?,prepareid = ?,preparename = ?,preparedate = ?,receivercode = ?,receivername = ?,satype = ?,sacode = ?,sadesc = ? WHERE noteno = '"+id+"'");
            //amtbeforetax=?
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getNoteno());
            ps.setString(2, item.getNotedate());
            ps.setString(3, item.getYear());
            ps.setString(4, item.getPeriod());
            ps.setString(5, item.getAcccode());
            ps.setString(6, item.getAccdesc());
            ps.setString(7, item.getEstcode());
            ps.setString(8, item.getEstname());
            ps.setString(9, item.getRemark());
            ps.setDouble(10, item.getTotal());
            ps.setString(11, item.getPostflag());
            ps.setString(12, item.getPrepareid());
            ps.setString(13, item.getPreparename());
            ps.setString(14, item.getPreparedate());
            ps.setString(15, item.getReceivercode());
            ps.setString(16, item.getReceivername());
            ps.setString(17, item.getSatype());
            ps.setString(18, item.getSacode());
            ps.setString(19, item.getSadesc());
            
            Logger.getLogger(VendorDebitNoteDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, VendorDebitNoteItem item, String id) throws Exception{
        
        
        try{
            String q = ("UPDATE ap_debitnote_item set matcode = ?,matdesc = ?,quantity = ?,unitprice = ?,unitmeasure = ?,amount = ?,remark = ?,loclevel = ?,loccode = ?,locdesc = ?,accode = ?,acdesc = ?,varian = ?,pounitmeasure = ?,pounitprice = ?,poquantity = ?,poamount = ?,taxrate = ?,taxamt = ?,taxcode = ?,taxdescp = ?,taxcoacode = ?,taxcoadescp = ?,satype = ?,sacode = ?,sadesc = ? WHERE invrefno = '"+id+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getNoteno());
            ps.setString(2, item.getDescp());
            ps.setDouble(3, item.getQuantity());
            ps.setDouble(4, item.getUnitprice());
            ps.setDouble(5, item.getAmount());
            ps.setString(6, item.getDate());
            ps.setInt(7, item.getRefer());
            ps.setString(8, item.getReference());
            Logger.getLogger(VendorInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static String getCVno(LoginProfile log, String abb) throws Exception{
        
        ResultSet rs = null;
        String CVno = "";
        String year = AccountingPeriod.getCurYear(log);
        String curperiod = AccountingPeriod.getCurPeriod(log);
        //String estatecode = LoginProfile.getEstateCode();
        String estatecode = "9911";
        
        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat(?,'"+ estatecode +"',substring('"+ year +"',3,2),lpad('"+ curperiod +"',2,'0'),lpad((max(substring(refer,14,4))+1),4,'0')),concat('JPY','"+ estatecode +"',substring('"+ year +"',3,2),lpad('"+ curperiod +"',2,'0'),'0001')) as refer from cb_cashvoucher where refer like concat(?,'%') and year='"+ year +"' and period='"+ curperiod +"' and estatecode like concat('"+ estatecode +"','%')");
        stmt.setString(1, abb);
        stmt.setString(2, abb);
        
        rs = stmt.executeQuery();
        if (rs.next()) {
            CVno = rs.getString(1);
            
            
        }
        
        rs.close();
        stmt.close();
        return CVno;
        
    }
    
    public static String getCVoucherNo(LoginProfile log) throws Exception{
        
        ResultSet rs = null;
        String CVno = "";
        
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT ifnull(concat(lpad((max(voucherid)+1),5,'0')),concat('0001')) as new FROM `cb_cashvoucher`");
        
        rs = stmt.executeQuery();
        if (rs.next()) {
            CVno = rs.getString(1); 
        }
        
        rs.close();
        stmt.close();
        return CVno;
        
    }
    
    public static String getCVid(LoginProfile log, String refno) throws Exception, SQLException{
        
        ResultSet rs = null;
        String JVno = "";
        
        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat('"+refno+"',lpad((max(right(novoucher,4))+1),4,'0')),concat('"+refno+"','0001')) as njvno from cb_cashvoucher_account where refer='"+refno+"'");
        
        rs = stmt.executeQuery();
        if (rs.next()) {
            JVno = rs.getString(1); 
        }
        
        rs.close();
        stmt.close();
        return JVno;
        
    }
   
    public static Double getSum(LoginProfile log, String column, String id)throws Exception {
        Double x = 0.00;
        Statement stment = log.getCon().createStatement();   
        ResultSet setent =stment.executeQuery(" select sum("+column+") as sumamount from cb_cashvoucher_account where refer = '"+id+"'");
       
        while(setent.next()) {
            x+=setent.getDouble("sumamount");
        }
        
        stment.close();
        setent.close();
        return x;
    }
    
    public static Double getBalance(LoginProfile log, String column, String id)throws Exception {
        Double x = 0.00;
        Double y = 0.00;
        Statement stment = log.getCon().createStatement();   
        ResultSet setent =stment.executeQuery(" select sum("+column+") as sumamount from cb_cashvoucher_account where refer = '"+id+"'");
       
        while(setent.next()) {
            x+=setent.getDouble("sumamount");
        }
        
          
        setent =stment.executeQuery(" select total from cb_cashvoucher where refer = '"+id+"'");
       
        if(setent.next()) {
            y=setent.getDouble("total");
        }
        
        stment.close();
        setent.close();
        return y-x;
    }
    
    public static VendorDebitNoteItem getItem(String id)throws Exception {
        
        VendorDebitNoteItem v = new VendorDebitNoteItem();
        
        //v.setVoucer(id);
        //v.setAmtbeforetax(getBalance("debit",id));
       
        return v;
    }
    
   
    
    private static void updateAmount(LoginProfile log, String JVrefno) throws Exception{
        String query = "UPDATE gl_jv SET todebit = '"+getSum(log, "debit",JVrefno)+"',tocredit = '"+getSum(log, "credit",JVrefno)+"' where JVrefno = '"+JVrefno+"'";
        
        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }
    
    public static List<VendorDebitNoteItem> getAllDBNItem(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<VendorDebitNoteItem> CVi;
        CVi = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from ap_inv_detail where no = '"+refno+"' order by no");

            while (rs.next()) {
                CVi.add(getDBNitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static VendorDebitNoteItem getPNVitem(LoginProfile log, String id) throws SQLException, Exception {
        VendorDebitNoteItem c = null;
        ResultSet rs = null;
        try{
            PreparedStatement stmt = log.getCon().prepareStatement("select * from ap_inv_detail where invrefno=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getDBNitem(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        
        return c;
    }
    
    private static VendorDebitNoteItem getDBNitem(ResultSet rs) throws SQLException {
        VendorDebitNoteItem gt = new VendorDebitNoteItem();
        
        gt.setAmount(rs.getDouble("amount"));
        gt.setDate(rs.getString("date"));
        gt.setDescp(rs.getString("descp"));
        gt.setNoteno(rs.getString("noteno"));
        gt.setQuantity(rs.getDouble("quantity"));
        gt.setRefer(rs.getInt("refer"));
        gt.setReference(rs.getString("reference"));
        gt.setUnitprice(rs.getDouble("unitprice"));
        
        return gt;
    }
    
    public static List<VendorDebitNote> getAllDBN(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<VendorDebitNote> CV;
        CV = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from ap_inv where invrefno = '"+refno+"' order by invrefno");

            while (rs.next()) {
                CV.add(getDBN(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }
    
    public static VendorDebitNote getDBN(LoginProfile log, String refno) throws SQLException, Exception {
        VendorDebitNote c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from ap_inv where invrefno=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getDBN(rs);
        }

        
        return c;
    }
    
    private static VendorDebitNote getDBN(ResultSet rs) throws SQLException {
        
        VendorDebitNote gt = new VendorDebitNote();
        
        gt.setAcccode(rs.getString("acccode"));
        gt.setAccdesc(rs.getString("accdesc"));
        gt.setApprovedate(rs.getString("approvedate"));
        gt.setApprovedesig(rs.getString("approvedesig"));
        gt.setApproveid(rs.getString("approveid"));
        gt.setApprovename(rs.getString("approveid"));
        gt.setEstcode(rs.getString("estcode"));
        gt.setEstname(rs.getString("estname"));
        gt.setNotedate(rs.getString("notedate"));
        gt.setNoteno(rs.getString("noteno"));
        gt.setPeriod(rs.getString("period"));
        gt.setPostdate(rs.getString("postdate"));
        gt.setPostflag(rs.getString("postflag"));
        gt.setPreparedate(rs.getString("preparedate"));
        gt.setPrepareid(rs.getString("prepareid"));
        gt.setPreparename(rs.getString("preparename"));
        gt.setReceivercode(rs.getString("receivercode"));
        gt.setReceivername(rs.getString("receivername"));
        gt.setRemark(rs.getString("remark"));
        gt.setSacode(rs.getString("sacode"));
        gt.setSadesc(rs.getString("sadesc"));
        gt.setSatype(rs.getString("satype"));
        gt.setTopost(rs.getString("topost"));
        gt.setTotal(rs.getDouble("total"));
        gt.setYear(rs.getString("year"));
     
        return gt;
    }
    
    public static double getDebit(LoginProfile log, String refno) throws SQLException, Exception {
        double amount = 0;
        ResultSet rs = null;
        ResultSet rs1 = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select sum(amount) as amt from cb_payvaucher_debit where voucer=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amount = rs.getDouble("amt");
        }
        
        return amount;
    }
    
    public static double getCredit(LoginProfile log, String refno) throws SQLException, Exception {
        double amt = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_payvoucher  where refer = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amt = rs.getDouble("amount");
        }
        
        
        return amt;
    }
    
    public static int getCheckCounter(LoginProfile log) throws Exception{
        
        int cnt = 0;
        ResultSet rs = null;
        
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from sl_inv where year = ? and period = ? and cid = ''");
        
        stmt.setString(1, AccountingPeriod.getCurYear(log));
        stmt.setString(2, AccountingPeriod.getCurPeriod(log));
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }
        
        return cnt;
    }
    
    public static int getApproveCounter(LoginProfile log) throws Exception{
        
        int cnt = 0;
        ResultSet rs = null;
        
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from sl_inv where year = ? and period = ? and cid <> ? and pid = ?");
        
        stmt.setString(1, AccountingPeriod.getCurYear(log));
        stmt.setString(2, AccountingPeriod.getCurPeriod(log));
        stmt.setString(3, "");
        stmt.setString(4, "");
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }
        
        return cnt;
    }
    
    public static void checkVoucher(LoginProfile log, String refer, String staff_id, String staff_name) throws Exception{
        
        String query = "UPDATE sl_inv SET cid = '"+staff_id+"',cname = '"+staff_name+"',cdate='"+AccountingPeriod.getCurrentTimeStamp()+"' where invref = '"+refer+"'";
        
        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }
    
    public static void approveVoucher(LoginProfile log, String refer, String staff_id, String staff_name, String position) throws Exception{
        String query = "UPDATE sl_inv SET aid = '"+staff_id+"',aname = '"+staff_name+"',adate='"+AccountingPeriod.getCurrentTimeStamp()+"', adesignation = '"+position+"' where invref = '"+refer+"'";
        
        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }
}
