/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.tx;

import com.lcsb.fms.dao.financial.gl.CreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.JournalVoucherDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.gl.JournalVoucher;
import com.lcsb.fms.model.financial.gl.JournalVoucherItem;
import com.lcsb.fms.model.financial.tx.TxGst03;
import com.lcsb.fms.model.financial.tx.TxGst03Refer;
import com.lcsb.fms.model.financial.tx.TxReconcileMaster;
import com.lcsb.fms.model.financial.tx.TxVoucher;
import com.lcsb.fms.model.financial.tx.TxVoucherItem;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.dao.TaxDAO;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class TaxGst03DAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("021105");
        mod.setModuleDesc("Tax Return - GST 03");
        mod.setMainTable("tx_gst03");
        mod.setReferID_Master("refer");
        return mod;

    }

    public static List<ListTable> tableList() {

        String column_names[] = {"refer", "year", "taxperiod", "date", "taxdue", "pay", "claim", "appid"};
        String title_name[] = {"Refer", "Year", "Tax Period", "Date", "Return Period", "Payable", "Claimable", "Approve"};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }

        return lt;
    }

    public static List<TxReconcileMaster> getAllReconcile(LoginProfile log) throws Exception {
        List<TxReconcileMaster> txi;
        txi = new ArrayList();

        TxReconcileMaster tx = new TxReconcileMaster();
        ResultSet rs = null;

        int year = TaxMasterDAO.getTaxPeriod(log).getYear();
        int period = TaxMasterDAO.getTaxPeriod(log).getPeriod();

        PreparedStatement stmt = log.getCon().prepareStatement("select * from tx_reconcile_master where year='" + year + "' and period='" + period + "' and refno not in (select reconrefer from tx_gst03_refer where taxperiod='" + period + "' and taxyear='" + year + "')");
        //stmt.setString(1, sessionid);
        rs = stmt.executeQuery();
        while (rs.next()) {

            txi.add(TaxReconcileDAO.getRecon(rs));
        }

        return txi;

    }

    public static double getAmount(LoginProfile log, String y, String p, String tx, String fld) throws Exception {

        double rtn = 0.00;
        Statement stm = log.getCon().createStatement();
        ResultSet st = stm.executeQuery("select sum(" + fld + ") as amt from tx_reconcile where year='" + y + "' and period='" + p + "' and taxcode in (" + tx + ") and refer not in (select reconrefer from tx_gst03_refer where taxperiod='" + p + "' and taxyear='" + y + "')");
        while (st.next()) {
            rtn += st.getDouble("amt");
        }
        return GeneralTerm.twoDecimalDouble(rtn);

    }

    public static double getAmount2(LoginProfile log, String y, String p, String tx, String fld) throws Exception {

        double rtn = 0.00;
        Statement stm = log.getCon().createStatement();
        ResultSet st = stm.executeQuery("select sum(" + fld + ") as amt from tx_reconcile where year='" + y + "' and period='" + p + "' and taxcode in (" + tx + ") and baddebt='Yes' and refer not in (select reconrefer from tx_gst03_refer where taxperiod='" + p + "' and taxyear='" + y + "')");
        while (st.next()) {
            rtn += st.getDouble("amt");
        }
        return GeneralTerm.twoDecimalDouble(rtn);

    }

    public static void saveGst03(TxGst03 item, LoginProfile log, String refer) throws Exception {


        Logger.getLogger(TaxGst03DAO.class.getName()).log(Level.INFO, "------0000000000000-");
        try {
            String q = ("insert into tx_gst03(refer,year,taxyear,taxperiod,period,date,gstid,amend,preid,prename,predesign,c1,c2,c3,c4,b5,c6,c7,c8,c9,c10,c11,c12,c13,c15,c17,c19,c21,c23,c24,i14,i16,i18,i20,i22,taxstart,taxend,taxdue,estcode,loccode,prcnt1,prcnt2,prcnt3,prcnt4,prcnt5,prcnt6,locname,loclevel,pay,claim,coacode,coadescp,satype,sacode,sadesc,predate) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, refer);
                ps.setString(2, item.getYear());
                ps.setInt(3, TaxMasterDAO.getTaxPeriod(log).getYear());
                ps.setInt(4, TaxMasterDAO.getTaxPeriod(log).getPeriod());
                ps.setInt(5, item.getPeriod());
                ps.setDate(6, (AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()) != null) ? new java.sql.Date(AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()).getTime()) : null);
                ps.setString(7, item.getGstid());
                ps.setString(8, item.getAmend());
                ps.setString(9, log.getUserID());
                ps.setString(10, log.getFullname());
                ps.setString(11, log.getPosition());
                ps.setBigDecimal(12, item.getC1());
                ps.setBigDecimal(13, item.getC2());
                ps.setBigDecimal(14, item.getC3());
                ps.setBigDecimal(15, item.getC4());
                ps.setString(16, item.getB5());
                ps.setBigDecimal(17, item.getC6());
                ps.setBigDecimal(18, item.getC7());
                ps.setBigDecimal(19, item.getC8());
                ps.setBigDecimal(20, item.getC9());
                ps.setBigDecimal(21, item.getC10());
                ps.setBigDecimal(22, item.getC11());
                ps.setBigDecimal(23, item.getC12());
                ps.setBigDecimal(24, item.getC13());
                ps.setBigDecimal(25, item.getC15());
                ps.setBigDecimal(26, item.getC17());
                ps.setBigDecimal(27, item.getC19());
                ps.setBigDecimal(28, item.getC21());
                ps.setBigDecimal(29, item.getC23());
                ps.setBigDecimal(30, item.getC24());
                ps.setString(31, item.getI14());
                ps.setString(32, item.getI16());
                ps.setString(33, item.getI18());
                ps.setString(34, item.getI20());
                ps.setString(35, item.getI22());
                ps.setDate(36, (item.getTaxstart() != null) ? new java.sql.Date(item.getTaxstart().getTime()) : null);
                ps.setDate(37, (item.getTaxend() != null) ? new java.sql.Date(item.getTaxend().getTime()) : null);
                ps.setDate(38, (item.getTaxdue() != null) ? new java.sql.Date(item.getTaxdue().getTime()) : null);
                ps.setString(39, log.getEstateCode());
                ps.setString(40, log.getEstateCode());
                ps.setBigDecimal(41, item.getPrcnt1());
                ps.setBigDecimal(42, item.getPrcnt2());
                ps.setBigDecimal(43, item.getPrcnt3());
                ps.setBigDecimal(44, item.getPrcnt4());
                ps.setBigDecimal(45, item.getPrcnt5());
                ps.setBigDecimal(46, item.getPrcnt6());
                ps.setString(47, log.getEstateDescp());
                ps.setString(48, "Company");
                ps.setBigDecimal(49, item.getPay());
                ps.setBigDecimal(50, item.getClaim());
                ps.setString(51, item.getCoacode());
                ps.setString(52, item.getCoadescp());
                ps.setString(53, item.getSatype());
                ps.setString(54, item.getSacode());
                ps.setString(55, item.getSadesc());
                ps.setDate(56, (AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()) != null) ? new java.sql.Date(AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()).getTime()) : null);

                Logger.getLogger(TaxGst03DAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(ps));
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void saveGst03Refer(LoginProfile log, TxGst03Refer item) throws Exception {


        try {
            String q = ("insert into tx_gst03_refer (refer,reconrefer,taxdebit,taxcredit,taxstart,taxend,taxyear,taxperiod) values (?,?,?,?,?,?,?,?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, item.getRefer());
                ps.setString(2, AutoGenerate.getReferAddBack(log, "tx_gst03_refer", "reconrefer", item.getRefer(), "refer"));
                ps.setDouble(3, item.getTaxdebit());
                ps.setDouble(4, item.getTaxcredit());
                ps.setDate(5, (item.getTaxstart() != null) ? new java.sql.Date(item.getTaxstart().getTime()) : null);
                ps.setDate(6, (item.getTaxend() != null) ? new java.sql.Date(item.getTaxend().getTime()) : null);
                ps.setString(7, item.getTaxyear());
                ps.setString(8, item.getTaxperiod());

                Logger.getLogger(TaxGst03DAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(ps));
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static TxGst03 getGst03(LoginProfile log, String refno) throws SQLException, Exception {
        TxGst03 c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from tx_gst03 where refer=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getGst03(rs);
        }

        return c;
    }

    public static TxGst03 getGst03(ResultSet rs) throws SQLException {

        TxGst03 tg = new TxGst03();

        tg.setRefer(rs.getString("refer"));
        tg.setAmend(rs.getString("amend"));
        tg.setB5(rs.getString("b5"));
        tg.setC1(rs.getBigDecimal("c1"));
        tg.setC10(rs.getBigDecimal("c10"));
        tg.setC11(rs.getBigDecimal("c11"));
        tg.setC12(rs.getBigDecimal("c12"));
        tg.setC13(rs.getBigDecimal("c13"));
        tg.setC15(rs.getBigDecimal("c15"));
        tg.setC17(rs.getBigDecimal("c17"));
        tg.setC19(rs.getBigDecimal("c19"));
        tg.setC2(rs.getBigDecimal("c2"));
        tg.setC21(rs.getBigDecimal("c21"));
        tg.setC23(rs.getBigDecimal("c23"));
        tg.setC24(rs.getBigDecimal("c24"));
        tg.setC3(rs.getBigDecimal("c3"));
        tg.setC4(rs.getBigDecimal("c4"));
        tg.setC6(rs.getBigDecimal("c6"));
        tg.setC7(rs.getBigDecimal("c7"));
        tg.setC8(rs.getBigDecimal("c8"));
        tg.setC9(rs.getBigDecimal("c9"));
        tg.setClaim(rs.getBigDecimal("claim"));
        tg.setCoacode(rs.getString("coacode"));
        tg.setCoadescp(rs.getString("coadescp"));
        tg.setEstcode(rs.getString("estcode"));
        tg.setGstid(rs.getString("gstid"));
        tg.setI14(rs.getString("i14"));
        tg.setI16(rs.getString("i16"));
        tg.setI18(rs.getString("i18"));
        tg.setI20(rs.getString("i20"));
        tg.setI22(rs.getString("i22"));
        tg.setLoccode(rs.getString("loccode"));
        tg.setLoclevel(rs.getString("loclevel"));
        tg.setLocname(rs.getString("locname"));
        tg.setPay(rs.getBigDecimal("pay"));
        tg.setPeriod(rs.getInt("period"));
        tg.setPrcnt1(rs.getBigDecimal("prcnt1"));
        tg.setPrcnt2(rs.getBigDecimal("prcnt2"));
        tg.setPrcnt3(rs.getBigDecimal("prcnt3"));
        tg.setPrcnt4(rs.getBigDecimal("prcnt4"));
        tg.setPrcnt5(rs.getBigDecimal("prcnt5"));
        tg.setPrcnt6(rs.getBigDecimal("prcnt6"));
        tg.setSacode(rs.getString("sacode"));
        tg.setSadesc(rs.getString("sadesc"));
        tg.setSatype(rs.getString("satype"));
        tg.setTaxdue(rs.getDate("taxdue"));
        tg.setTaxend(rs.getDate("taxend"));
        tg.setTaxperiod(rs.getString("taxperiod"));
        tg.setTaxstart(rs.getDate("taxstart"));
        tg.setTaxyear(rs.getString("taxyear"));
        tg.setYear(rs.getString("year"));
        tg.setGenjvref(rs.getString("genjvref"));

        return tg;
    }

    public static String getStatus(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String jv = "";
        String app = "";
        String status = "<span class=\"label label-primary\">Preparing</span>";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select genjvref,appid from tx_gst03 where refer = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            jv = rs.getString(1);
            app = rs.getString(2);
        }

        stmt.close();
        rs.close();

        if (!jv.equals("")) {
            status = "JV Created";
        }
        
        if (!app.equals("")) {
            status = "Approved";
        }

        return status;
    }
    
    public static boolean isApprove(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select appid from tx_gst03 where refer = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString(1);
        }

        if (!ck.equals("")) {
            cek = true;
        }

        stmt.close();
        rs.close();

        return cek;
    }

    public static List<TxGst03Refer> getGst03ReferAll(LoginProfile log, String refno) throws Exception {

        List<TxGst03Refer> txi;
        txi = new ArrayList();

        TxGst03Refer tx = new TxGst03Refer();
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM tx_gst03_refer WHERE refer = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        while (rs.next()) {

            txi.add(getInput(rs));
        }

        return txi;

    }

    private static TxGst03Refer getInput(ResultSet rs) throws SQLException {

        TxGst03Refer tr = new TxGst03Refer();

        tr.setRefer(rs.getString("refer"));
        tr.setTaxcredit(rs.getDouble("taxcredit"));
        tr.setTaxdebit(rs.getDouble("taxdebit"));
        tr.setTaxend(rs.getDate("taxend"));
        tr.setTaxperiod(rs.getString("taxperiod"));
        tr.setTaxstart(rs.getDate("taxstart"));
        tr.setTaxyear(rs.getString("taxyear"));
        tr.setReconrefer(rs.getString("reconrefer"));

        return tr;
    }

    public static String generateJV(String refer, LoginProfile log) throws Exception {

        String JVno = "";

        JournalVoucher jv = new JournalVoucher();

        TxGst03 tg = TaxGst03DAO.getGst03(log, refer);

        double dt_amount = tg.getC2().doubleValue();
        double ct_amount = tg.getC4().doubleValue();
        double balcon = dt_amount - ct_amount;

        jv.setCurperiod(AccountingPeriod.getCurPeriod(log));
        jv.setEstatecode(log.getEstateCode());
        jv.setEstatename(log.getEstateDescp());
        jv.setJVdate(AccountingPeriod.getCurrentTimeStamp());
        jv.setJVno("");
        jv.setJVrefno("");
        jv.setPreparedate(AccountingPeriod.getCurrentTimeStamp());
        jv.setPreparedbyid(log.getUserID());
        jv.setPreparedbyname(log.getFullname());
        jv.setReason("TAX CLEARING POSTING FOR TAXABLE PERIOD FROM " + TaxGst03DAO.getGst03(log, refer).getTaxstart() + " TO " + TaxGst03DAO.getGst03(log, refer).getTaxend());
        jv.setReflexcb("No");
        jv.setTocredit(String.valueOf(ct_amount));
        jv.setTodebit(String.valueOf(dt_amount));
        jv.setYear(AccountingPeriod.getCurYear(log));
        jv.setJVtype("JTX");

        String JVrefno = JournalVoucherDAO.saveJournalMain(log, jv);

        JournalVoucherItem jvi = new JournalVoucherItem();

        if (balcon < 0) {//gst clearing account

            jvi.setActcode(tg.getCoacode());
            jvi.setActdesc(tg.getCoadescp());
            jvi.setAmtbeforetax(0.0);
            jvi.setBalance(0.0);
            jvi.setCredit(0.00);
            jvi.setDebit(balcon * (-1));
            jvi.setGstid("");
            jvi.setJVrefno(JVrefno);
            jvi.setJvid("");
            jvi.setLoccode(log.getEstateCode());
            jvi.setLocdesc(log.getEstateDescp());
            jvi.setLoclevel("Company");
            jvi.setRemark("GST CLEARING ACCOUNT");
            jvi.setSacode("00");
            jvi.setSadesc("Not Applicable");
            jvi.setSatype("None");
            jvi.setTaxcode("None");
            jvi.setTaxdescp("Not Applicable");
            jvi.setTaxrate("0");

            JournalVoucherDAO.saveJournalItem(log, jvi);

        } else {

            jvi.setActcode(tg.getCoacode());
            jvi.setActdesc(tg.getCoadescp());
            jvi.setAmtbeforetax(0.0);
            jvi.setBalance(0.0);
            jvi.setCredit(balcon);
            jvi.setDebit(0.00);
            jvi.setGstid("");
            jvi.setJVrefno(JVrefno);
            jvi.setJvid("");
            jvi.setLoccode(log.getEstateCode());
            jvi.setLocdesc(log.getEstateDescp());
            jvi.setLoclevel("Company");
            jvi.setRemark("GST CLEARING ACCOUNT");
            jvi.setSacode("00");
            jvi.setSadesc("Not Applicable");
            jvi.setSatype("None");
            jvi.setTaxcode("None");
            jvi.setTaxdescp("Not Applicable");
            jvi.setTaxrate("0");

            JournalVoucherDAO.saveJournalItem(log, jvi);

        }

        if (dt_amount > 0) {//Debit - Output

            jvi.setActcode(TaxDAO.getTax(log, "SR").getTaxcoacode());
            jvi.setActdesc(TaxDAO.getTax(log, "SR").getTaxcoadescp());
            jvi.setAmtbeforetax(0.0);
            jvi.setBalance(0.0);
            jvi.setCredit(0.00);
            jvi.setDebit(dt_amount);
            jvi.setGstid("");
            jvi.setJVrefno(JVrefno);
            jvi.setJvid("");
            jvi.setLoccode(log.getEstateCode());
            jvi.setLocdesc(log.getEstateDescp());
            jvi.setLoclevel("Company");
            jvi.setRemark("GST OUTPUT TAX");
            jvi.setSacode("00");
            jvi.setSadesc("Not Applicable");
            jvi.setSatype("None");
            jvi.setTaxcode("None");
            jvi.setTaxdescp("Not Applicable");
            jvi.setTaxrate("0");

            JournalVoucherDAO.saveJournalItem(log, jvi);
        }

        if (ct_amount > 0) {

            jvi.setActcode(TaxDAO.getTax(log, "TX").getTaxcoacode());
            jvi.setActdesc(TaxDAO.getTax(log, "TX").getTaxcoadescp());
            jvi.setAmtbeforetax(0.0);
            jvi.setBalance(0.0);
            jvi.setCredit(ct_amount);
            jvi.setDebit(0.00);
            jvi.setGstid("");
            jvi.setJVrefno(JVrefno);
            jvi.setJvid("");
            jvi.setLoccode(log.getEstateCode());
            jvi.setLocdesc(log.getEstateDescp());
            jvi.setLoclevel("Company");
            jvi.setRemark("GST INPUT TAX");
            jvi.setSacode("00");
            jvi.setSadesc("Not Applicable");
            jvi.setSatype("None");
            jvi.setTaxcode("None");
            jvi.setTaxdescp("Not Applicable");
            jvi.setTaxrate("0");

            JournalVoucherDAO.saveJournalItem(log, jvi);
        }
        String query = "UPDATE tx_gst03 SET genjvref = '" + JVrefno + "' WHERE refer = '" + refer + "'";

        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();

        return JVno;
    }
    
    public static void deleteExist(LoginProfile log, String no) throws SQLException, Exception {
        Connection con = log.getCon();
        
        TxGst03 tg = (TxGst03) TaxGst03DAO.getGst03(log, no);

        String deleteQuery_1 = "delete from tx_gst03 where refer = ?";
        String deleteQuery_2 = "delete from tx_gst03_refer where refer = ?";
        PreparedStatement ps_1 = con.prepareStatement(deleteQuery_1);
        PreparedStatement ps_2 = con.prepareStatement(deleteQuery_2);
        ps_1.setString(1, no);
        ps_2.setString(1, no);
        ps_1.executeUpdate();
        ps_2.executeUpdate();
        ps_1.close();
        ps_2.close();
        
        //JournalVoucherDAO.deleteExistJournal(tg.getGenjvref());
        
    }
    
    public static void updateTxGst03(LoginProfile log, String JVrefno) throws Exception{
        
        String query = "UPDATE tx_gst03 SET genjvref = '' WHERE genjvref = '"+JVrefno+"'";
        
        try (PreparedStatement ps = log.getCon().prepareStatement(query)) {
            ps.executeUpdate();
        }
        
    }
    
    public static void approveVoucher(String refer, LoginProfile log) throws Exception {
        
        String query = "UPDATE tx_gst03 SET appid = '" + log.getUserID() + "',appname = '" + log.getFullname() + "',appdate='" + AccountingPeriod.getCurrentTimeStamp() + "', appdesign = '" + log.getPosition() + "' where refer = '" + refer + "'";

        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }
    
}
