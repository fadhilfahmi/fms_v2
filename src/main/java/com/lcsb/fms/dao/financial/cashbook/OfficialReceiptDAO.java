/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.cashbook;

import com.lcsb.fms.dao.financial.ar.ArDebitNoteDAO;
import com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO;
import com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.LiveGLDAO;
import com.lcsb.fms.dao.financial.gl.PostDAO;
import com.lcsb.fms.dao.setup.configuration.BuyerDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.general.DataTable;
import com.lcsb.fms.general.EnglishNumberToWords;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.ar.ArDebitNote;
import com.lcsb.fms.model.financial.ar.SalesInvoice;
import com.lcsb.fms.model.financial.cashbook.CbOfficialCek;
import com.lcsb.fms.model.financial.cashbook.ORPayer;
import com.lcsb.fms.model.financial.cashbook.ORReceiptMaster;
import com.lcsb.fms.model.financial.cashbook.ORTempCredit;
import com.lcsb.fms.model.financial.cashbook.ORTempInvoiceAmount;
import com.lcsb.fms.model.financial.cashbook.OfficialReceipt;
import com.lcsb.fms.model.financial.cashbook.OfficialCreditItem;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherType;
import com.lcsb.fms.model.setup.company.CeManageBank;
import com.lcsb.fms.ui.button.Button;
import com.lcsb.fms.ui.button.ButtonGroupList;
import com.lcsb.fms.util.dao.BankDAO;
import com.lcsb.fms.util.dao.EstateDAO;
import com.lcsb.fms.util.dao.ModuleDAO;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class OfficialReceiptDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("020203");
        mod.setModuleDesc("Official Receipt");
        mod.setMainTable("cb_official");
        mod.setReferID_Master("refer");
        return mod;

    }

    public static List<ListTable> tableList() {

        String column_names[] = {"refer", "date", "paidname", "amount", "checkid", "appid", "post"};
        String title_name[] = {"Refer", "Date", "Paid Name", "Amount", "Check Id", "App id", "Post"};
        boolean boolview[] = {true, true, true, true, false, false, false};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            i.setBoolView(boolview[j]);
            lt.add(i);
        }

        return lt;
    }

    public static void deleteExist(LoginProfile log, String no) throws SQLException, Exception {
        Connection con = log.getCon();

        String deleteQuery_1 = "delete from cb_official where refer = ?";
        String deleteQuery_2 = "delete from cb_official_credit where voucherno = ?";
        String deleteQuery_3 = "delete from cb_official_cek where voucherno = ?";
        PreparedStatement ps_1 = con.prepareStatement(deleteQuery_1);
        PreparedStatement ps_2 = con.prepareStatement(deleteQuery_2);
        PreparedStatement ps_3 = con.prepareStatement(deleteQuery_3);
        ps_1.setString(1, no);
        ps_2.setString(1, no);
        ps_3.setString(1, no);
        ps_1.executeUpdate();
        ps_2.executeUpdate();
        ps_3.executeUpdate();
        ps_1.close();
        ps_2.close();
        ps_3.close();

        updateInvoiceIfORDeleted(log, no);
        LiveGLDAO.deleteFromGL(log, no);

    }

    private static void updateInvoiceIfORDeleted(LoginProfile log, String refno) throws Exception {

        String oldcekno = "";
        ResultSet rs = null;
        ResultSet rs1 = null;
        Connection conet = log.getCon();
        PreparedStatement stmt = conet.prepareStatement("select * from cb_official_invoice where ornno = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();

        while (rs.next()) {

            String q = ("UPDATE sl_inv set paid = (paid - " + rs.getString("amount") + ")  where invref = '" + rs.getString("invno") + "'");
            PreparedStatement ps = conet.prepareStatement(q);
            //ps.setString(1, pvno);
            ps.executeUpdate();
            ps.close();
        }

        String deleteQuery = "delete from cb_official_invoice where ornno = ?";
        PreparedStatement ps = conet.prepareStatement(deleteQuery);
        ps.setString(1, refno);
        ps.executeUpdate();
        ps.close();

        rs.close();

    }

    public static void deleteItem(LoginProfile log, String novoucher, String refer) throws SQLException, Exception {

        updateInvoiceIfORDeletedEach(log, novoucher);
        updateORAmountIfDebitNoteDeleted(log, novoucher);

        String deleteQuery_2 = "delete from cb_official_credit where refer = ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, novoucher);
        ps_2.executeUpdate();
        ps_2.close();

        LiveGLDAO.reloadGL(log, refer, "OR");

    }

    private static void updateORAmountIfDebitNoteDeleted(LoginProfile log, String refno) throws Exception, SQLException {

        String remark = getINVitem(log, refno).getRemarks();
        double amounttoDeduct = Double.parseDouble(getINVitem(log, refno).getAmount());
        double newamount = OfficialReceiptDAO.getINV(log, refno.substring(0, 15)).getAmount() - amounttoDeduct;

        if (remark.indexOf("DNR") != -1) {
            String q = ("UPDATE cb_official set amount = ?, rm = ?  where refer = '" + refno.substring(0, 15) + "'");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setDouble(1, newamount);
            ps.setString(2, EnglishNumberToWords.toConvert(newamount));
            ps.executeUpdate();
            ps.close();

        }

    }

    private static void updateInvoiceIfORDeletedEach(LoginProfile log, String refno) throws Exception, SQLException {

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_official_invoice where ornrefno = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();

        if (rs.next()) {

            String q = ("UPDATE sl_inv set paid = (paid - " + rs.getString("amount") + ")  where invref = '" + rs.getString("invno") + "'");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            //ps.setString(1, pvno);
            ps.executeUpdate();
            ps.close();
        }

        String deleteQuery = "delete from cb_official_invoice where ornrefno = ?";
        PreparedStatement ps = log.getCon().prepareStatement(deleteQuery);
        ps.setString(1, refno);
        ps.executeUpdate();
        ps.close();

        rs.close();

    }

    public static void deleteCheque(LoginProfile log, String novoucher, String refer) throws SQLException, Exception {

        String deleteQuery_2 = "delete from cb_official_cek where refer = ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, novoucher);
        ps_2.executeUpdate();
        ps_2.close();

        //updateAmount(refer);
    }

    public static String saveMain(LoginProfile log, OfficialReceipt item) throws Exception {

        String newrefer = AutoGenerate.getReferenceNox(log, "refer", "cb_official", "ORN", item.getEstatecode(), String.valueOf(item.getYear()), String.valueOf(item.getPeriod()));

        try {
            String q = ("insert into cb_official(refer,voucherno,date,paidtype,paidcode,paidname,paidaddress,paidcity,paidstate,paidpostcode,gstid,remarks,amount,rm,receivemode,racoacode,racoadesc,bankcode,bankname,year,period,estatecode,estatename,invno,preid,prename,preposition,preparedate,flagbank,stmtyear,stmtperiod, paymentmode, chequeno) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, newrefer);
            ps.setInt(2, Integer.parseInt(AutoGenerate.get4digitNo(log, "cb_official", "voucherno")));
            ps.setString(3, item.getDate());
            ps.setString(4, item.getPaidtype());
            ps.setString(5, item.getPaidcode());
            ps.setString(6, item.getPaidname());
            ps.setString(7, item.getPaidaddress());
            ps.setString(8, item.getPaidcity());
            ps.setString(9, item.getPaidstate());
            ps.setString(10, item.getPaidpostcode());
            ps.setString(11, item.getGstid());
            ps.setString(12, item.getRemarks());
            ps.setDouble(13, item.getAmount());
            ps.setString(14, item.getRm());
            ps.setString(15, item.getReceivemode());
            ps.setString(16, item.getRacoacode());
            ps.setString(17, item.getRacoadesc());
            ps.setString(18, item.getBankcode());
            ps.setString(19, item.getBankname());
            ps.setInt(20, item.getYear());
            ps.setInt(21, item.getPeriod());
            ps.setString(22, item.getEstatecode());
            ps.setString(23, item.getEstatename());
            ps.setString(24, item.getInvno());
            ps.setString(25, item.getPreid());
            ps.setString(26, item.getPrename());
            ps.setString(27, item.getPreposition());
            ps.setString(28, item.getPreparedate());
            ps.setString(29, item.getFlagbank());
            ps.setString(30, item.getStmtyear());
            ps.setString(31, item.getStmtperiod());
            ps.setString(32, item.getPaymentmode());
            ps.setString(33, item.getChequeno());

            Logger.getLogger(OfficialReceiptDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(ps));
            ps.executeUpdate();

            ps.close();

            LiveGLDAO.reloadGL(log, newrefer, "OR");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newrefer;

    }

    public static void saveItem(LoginProfile log, OfficialCreditItem item) throws Exception {

        try {
            String newrefer = AutoGenerate.getReferAddBack(log, "cb_official_credit", "refer", item.getRefer(), "voucherno");
            String q = ("insert into cb_official_credit(refer,loclevel,loccode,locname,coacode,coadescp,satype,sacode,sadesc,remarks,amount,taxdescp,taxcode,taxrate,taxamt,voucherno,amtbeforetax,taxcoacode,taxcoadescp) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, newrefer);
            ps.setString(2, item.getLoclevel());
            ps.setString(3, item.getLoccode());
            ps.setString(4, item.getLocname());
            ps.setString(5, item.getCoacode());
            ps.setString(6, item.getCoadescp());
            ps.setString(7, item.getSatype());
            ps.setString(8, item.getSacode());
            ps.setString(9, item.getSadesc());
            ps.setString(10, item.getRemarks());
            ps.setString(11, item.getAmount());
            ps.setString(12, item.getTaxdescp());
            ps.setString(13, item.getTaxcode());
            ps.setDouble(14, item.getTaxrate());
            ps.setDouble(15, item.getTaxamt());
            ps.setString(16, item.getRefer());
            ps.setDouble(17, Double.parseDouble(item.getAmount()) - item.getTaxamt());
            ps.setString(18, item.getTaxcoacode());
            ps.setString(19, item.getTaxcoadescp());

            ps.executeUpdate();
            Logger.getLogger(OfficialReceiptDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

            LiveGLDAO.reloadGL(log, item.getRefer(), "OR");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void saveItemInvoice(LoginProfile log, OfficialCreditItem item) throws Exception, SQLException {

        try {
            String q = ("insert into cb_official_credit(refer,loclevel,loccode,locname,coacode,coadescp,satype,sacode,sadesc,remarks,amount,taxdescp,taxcode,taxrate,taxamt,voucherno,amtbeforetax) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, item.getRefer());
            ps.setString(2, item.getLoclevel());
            ps.setString(3, item.getLoccode());
            ps.setString(4, item.getLocname());
            ps.setString(5, item.getCoacode());
            ps.setString(6, item.getCoadescp());
            ps.setString(7, item.getSatype());
            ps.setString(8, item.getSacode());
            ps.setString(9, item.getSadesc());
            ps.setString(10, item.getRemarks());
            ps.setString(11, item.getAmount());
            ps.setString(12, item.getTaxdescp());
            ps.setString(13, item.getTaxcode());
            ps.setDouble(14, item.getTaxrate());
            ps.setDouble(15, item.getTaxamt());
            ps.setString(16, item.getVoucherno());
            ps.setDouble(17, Double.parseDouble(item.getAmount()) - item.getTaxamt());

            ps.executeUpdate();
            Logger.getLogger(OfficialReceiptDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

            LiveGLDAO.reloadGL(log, item.getVoucherno(), "OR");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void saveCheque(LoginProfile log, CbOfficialCek item) throws Exception {

        try {
            String q = ("insert into cb_official_cek(voucherno,cekno,date,bank,branch,refer,amount) values (?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, item.getVoucherno());
            ps.setString(2, item.getCekno());
            ps.setString(3, item.getDate());
            ps.setString(4, item.getBank());
            ps.setString(5, item.getBranch());
            ps.setString(6, AutoGenerate.getReferAddBack(log, "cb_official_cek", "refer", item.getVoucherno(), "voucherno"));
            ps.setDouble(7, item.getAmount());

            ps.executeUpdate();
            Logger.getLogger(OfficialReceiptDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateCheque(LoginProfile log, CbOfficialCek item, String id) throws Exception {

        try {
            String q = ("UPDATE cb_official_cek set voucherno = ?,cekno= ?,date= ?,bank= ?,branch= ?,refer= ?,amount= ? WHERE refer = '" + id + "'");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, item.getVoucherno());
            ps.setString(2, item.getCekno());
            ps.setString(3, item.getDate());
            ps.setString(4, item.getBank());
            ps.setString(5, item.getBranch());
            ps.setString(6, item.getRefer());
            ps.setDouble(7, item.getAmount());

            ps.executeUpdate();
            Logger.getLogger(OfficialReceiptDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateMain(LoginProfile log, OfficialReceipt m, String id) throws Exception {

        try {
            //String q = ("insert into gl_jv_item(Jvid,JVrefno,remark,actcode,actdesc,loccode,locdesc,loclevel,satype,sacode,sadesc,debit,credit,taxcode,taxdescp,taxrate,gstid,amtbeforetax) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            String q = ("UPDATE cb_official set refer = ?,voucherno = ?,date = ?,paidtype = ?,paidcode = ?,paidname = ?,paidaddress = ?,paidcity = ?,paidstate = ?,paidpostcode = ?,gstid = ?,remarks = ?,amount = ?,rm = ?,receivemode = ?,racoacode = ?,racoadesc = ?,bankcode = ?,bankname = ?,year = ?,period = ?,estatecode = ?,estatename = ?, paymentmode = ?, chequeno = ? WHERE refer = '" + id + "'");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, m.getRefer());
            ps.setInt(2, m.getVoucherno());
            ps.setString(3, m.getDate());
            ps.setString(4, m.getPaidtype());
            ps.setString(5, m.getPaidcode());
            ps.setString(6, m.getPaidname());
            ps.setString(7, m.getPaidaddress());
            ps.setString(8, m.getPaidcity());
            ps.setString(9, m.getPaidstate());
            ps.setString(10, m.getPaidpostcode());
            ps.setString(11, m.getGstid());
            ps.setString(12, m.getRemarks());
            ps.setDouble(13, m.getAmount());
            ps.setString(14, m.getRm());
            ps.setString(15, m.getReceivemode());
            ps.setString(16, m.getRacoacode());
            ps.setString(17, m.getRacoadesc());
            ps.setString(18, m.getBankcode());
            ps.setString(19, m.getBankname());
            ps.setInt(20, m.getYear());
            ps.setInt(21, m.getPeriod());
            ps.setString(22, m.getEstatecode());
            ps.setString(23, m.getEstatename());
            ps.setString(24, m.getPaymentmode());
            ps.setString(25, m.getChequeno());
            Logger.getLogger(OfficialReceiptDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();

            //Logger.getLogger(OfficialReceiptDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

            LiveGLDAO.reloadGL(log, id, "OR");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateItem(LoginProfile log, OfficialCreditItem item, String id) throws Exception {

        try {
            String q = ("UPDATE cb_official_credit set  refer = ?,loclevel = ?,loccode = ?,locname = ?,coacode = ?,coadescp = ?,satype = ?,sacode = ?,sadesc = ?,remarks = ?,amount = ?,taxdescp = ?,taxcode = ?,taxrate = ?,taxamt = ?,voucherno = ?,amtbeforetax = ? WHERE refer = '" + id + "'");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getRefer());
            ps.setString(2, item.getLoclevel());
            ps.setString(3, item.getLoccode());
            ps.setString(4, item.getLocname());
            ps.setString(5, item.getCoacode());
            ps.setString(6, item.getCoadescp());
            ps.setString(7, item.getSatype());
            ps.setString(8, item.getSacode());
            ps.setString(9, item.getSadesc());
            ps.setString(10, item.getRemarks());
            ps.setString(11, item.getAmount());
            ps.setString(12, item.getTaxdescp());
            ps.setString(13, item.getTaxcode());
            ps.setDouble(14, item.getTaxrate());
            ps.setDouble(15, item.getTaxamt());
            ps.setString(16, item.getVoucherno());
            ps.setDouble(17, Double.parseDouble(item.getAmount()) - item.getTaxamt());

            Logger.getLogger(OfficialReceiptDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();

            ps.close();

            LiveGLDAO.reloadGL(log, item.getVoucherno(), "OR");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static String getCVno(LoginProfile log, String abb) throws Exception {

        ResultSet rs = null;
        String CVno = "";
        String year = AccountingPeriod.getCurYear(log);
        String curperiod = AccountingPeriod.getCurPeriod(log);
        //String estatecode = LoginProfile.getEstateCode();
        String estatecode = "9911";

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat(?,'" + estatecode + "',substring('" + year + "',3,2),lpad('" + curperiod + "',2,'0'),lpad((max(substring(refer,14,4))+1),4,'0')),concat('JPY','" + estatecode + "',substring('" + year + "',3,2),lpad('" + curperiod + "',2,'0'),'0001')) as refer from cb_cashvoucher where refer like concat(?,'%') and year='" + year + "' and period='" + curperiod + "' and estatecode like concat('" + estatecode + "','%')");
        stmt.setString(1, abb);
        stmt.setString(2, abb);

        rs = stmt.executeQuery();
        if (rs.next()) {
            CVno = rs.getString(1);

        }

        rs.close();
        stmt.close();
        return CVno;

    }

    public static String getCVoucherNo(LoginProfile log) throws Exception {

        ResultSet rs = null;
        String CVno = "";

        PreparedStatement stmt = log.getCon().prepareStatement("SELECT ifnull(concat(lpad((max(voucherid)+1),5,'0')),concat('0001')) as new FROM `cb_cashvoucher`");

        rs = stmt.executeQuery();
        if (rs.next()) {
            CVno = rs.getString(1);
        }

        rs.close();
        stmt.close();
        return CVno;

    }

    public static String getCVid(LoginProfile log, String refno) throws Exception, SQLException {

        ResultSet rs = null;
        String JVno = "";

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat('" + refno + "',lpad((max(right(novoucher,4))+1),4,'0')),concat('" + refno + "','0001')) as njvno from cb_cashvoucher_account where refer='" + refno + "'");

        rs = stmt.executeQuery();
        if (rs.next()) {
            JVno = rs.getString(1);
        }

        rs.close();
        stmt.close();
        return JVno;

    }

    public static Double getSum(LoginProfile log, String column, String id) throws Exception {
        Double x = 0.00;
        Statement stment = log.getCon().createStatement();
        ResultSet setent = stment.executeQuery(" select sum(" + column + ") as sumamount from cb_official_credit where voucherno = '" + id + "'");

        while (setent.next()) {
            x += setent.getDouble("sumamount");
        }

        stment.close();
        setent.close();
        return x;
    }

    public static Double getBalance(LoginProfile log, String column, String id) throws Exception {

        Double x = 0.00;
        Double y = 0.00;
        Statement stment = log.getCon().createStatement();
        ResultSet setent = stment.executeQuery(" select sum(" + column + ") as sumamount from cb_official_credit where voucherno = '" + id + "'");

        while (setent.next()) {
            x += setent.getDouble("sumamount");
        }

        setent = stment.executeQuery(" select amount from cb_official where refer = '" + id + "'");

        if (setent.next()) {
            y = setent.getDouble("amount");
        }

        stment.close();
        setent.close();
        return y - x;
    }

    public static OfficialCreditItem getItem(String id) throws Exception {

        OfficialCreditItem v = new OfficialCreditItem();

        v.setRefer(id);
        //v.setAmtbeforetax(getBalance("debit",id));

        return v;
    }

    private static void updateAmount(LoginProfile log, String JVrefno) throws Exception {

        String query = "UPDATE gl_jv SET todebit = '" + getSum(log, "debit", JVrefno) + "',tocredit = '" + getSum(log, "credit", JVrefno) + "' where JVrefno = '" + JVrefno + "'";

        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }

    public static List<OfficialCreditItem> getAllINVItem(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<OfficialCreditItem> CVi;
        CVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_official_credit where voucherno = '" + refno + "' order by voucherno");

            while (rs.next()) {
                CVi.add(getINVitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<OfficialCreditItem> getAllINVItemByPage(LoginProfile log, String refno, int row, int limit) throws Exception {

        Statement stmt = null;
        List<OfficialCreditItem> CVi;
        CVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_official_credit where voucherno = '" + refno + "' order by voucherno limit " + row + "," + limit);

            while (rs.next()) {
                CVi.add(getINVitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<CbOfficialCek> getAllCheque(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<CbOfficialCek> CVi;
        CVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_official_cek where voucherno = '" + refno + "' order by voucherno");

            while (rs.next()) {
                CVi.add(getCheque(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static OfficialCreditItem getINVitem(LoginProfile log, String id) throws SQLException, Exception {
        OfficialCreditItem c = null;
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_official_credit where refer=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getINVitem(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    private static OfficialCreditItem getINVitem(ResultSet rs) throws SQLException {
        OfficialCreditItem cv = new OfficialCreditItem();

        cv.setAmount(rs.getString("amount"));
        cv.setCoacode(rs.getString("coacode"));
        cv.setCoadescp(rs.getString("coadescp"));
        cv.setLoccode(rs.getString("loccode"));
        cv.setLoclevel(rs.getString("loclevel"));
        cv.setLocname(rs.getString("locname"));
        cv.setRefer(rs.getString("refer"));
        cv.setRemarks(rs.getString("remarks"));
        cv.setSacode(rs.getString("sacode"));
        cv.setSadesc(rs.getString("sadesc"));
        cv.setSatype(rs.getString("satype"));
        cv.setTaxamt(rs.getDouble("taxamt"));
        cv.setTaxcode(rs.getString("taxcode"));
        cv.setTaxdescp(rs.getString("taxdescp"));
        cv.setTaxrate(rs.getDouble("taxrate"));
        cv.setVoucherno(rs.getString("voucherno"));
        cv.setAmtbeforetax(rs.getDouble("amtbeforetax"));
        cv.setTaxcoacode(rs.getString("taxcoacode"));
        cv.setTaxcoadescp(rs.getString("taxcoadescp"));

        return cv;
    }

    public static CbOfficialCek getCheque(LoginProfile log, String id) throws SQLException, Exception {
        CbOfficialCek c = new CbOfficialCek();
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_official_cek where voucherno=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getCheque(rs);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

//        if(c == null){
//                
//        }
        return c;
    }

    public static CbOfficialCek getChequeEach(LoginProfile log, String id) throws SQLException, Exception {
        CbOfficialCek c = new CbOfficialCek();
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_official_cek where refer=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getCheque(rs);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

//        if(c == null){
//                
//        }
        return c;
    }

    private static CbOfficialCek getCheque(ResultSet rs) throws SQLException {
        CbOfficialCek cv = new CbOfficialCek();

        cv.setAmount(rs.getDouble("amount"));
        cv.setBank(rs.getString("bank"));
        cv.setBranch(rs.getString("branch"));
        cv.setCekno(rs.getString("cekno"));
        cv.setDate(rs.getString("date"));
        cv.setRefer(rs.getString("refer"));
        cv.setVoucherno(rs.getString("voucherno"));

        return cv;
    }

    public static List<OfficialReceipt> getAllCV(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<OfficialReceipt> CV;
        CV = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_cashvoucher where refer = '" + refno + "' order by refer");

            while (rs.next()) {
                CV.add(getINV(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static OfficialReceipt getINV(LoginProfile log, String refno) throws SQLException, Exception {
        OfficialReceipt c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_official where refer=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getINV(rs);
        }

        return c;
    }

    public static OfficialReceipt getINV(ResultSet rs) throws SQLException {
        OfficialReceipt cv = new OfficialReceipt();
        cv.setAmount(Double.parseDouble(rs.getString("amount")));
        cv.setBankcode(rs.getString("bankcode"));
        cv.setBankname(rs.getString("bankname"));
        cv.setDate(rs.getString("date"));
        cv.setEstatecode(rs.getString("estatecode"));
        cv.setEstatename(rs.getString("estatename"));
        cv.setGstid(rs.getString("gstid"));
        cv.setPaidaddress(rs.getString("paidaddress"));
        cv.setPaidcity(rs.getString("paidcity"));
        cv.setPaidcode(rs.getString("paidcode"));
        cv.setPaidname(rs.getString("paidname"));
        cv.setPaidpostcode(rs.getString("paidpostcode"));
        cv.setPaidstate(rs.getString("paidstate"));
        cv.setPaidtype(rs.getString("paidtype"));
        cv.setPeriod(rs.getInt("period"));
        cv.setRacoacode(rs.getString("racoacode"));
        cv.setRacoadesc(rs.getString("racoadesc"));
        cv.setReceivemode(rs.getString("receivemode"));
        cv.setRefer(rs.getString("refer"));
        cv.setRemarks(rs.getString("remarks"));
        cv.setRm(rs.getString("rm"));
        cv.setVoucherno(rs.getInt("voucherno"));
        cv.setYear(rs.getInt("year"));
        cv.setAppid(rs.getString("appid"));
        cv.setAppname(rs.getString("appname"));
        cv.setAppposition(rs.getString("appposition"));
        cv.setApprovedate(rs.getString("approvedate"));
        cv.setCheckid(rs.getString("checkid"));
        cv.setCheckdate(rs.getString("checkdate"));
        cv.setCheckname(rs.getString("checkname"));
        cv.setCheckposition(rs.getString("checkposition"));
        cv.setInvno(rs.getString("invno"));
        cv.setFlagbank(rs.getString("flagbank"));
        cv.setStmtperiod(rs.getString("stmtperiod"));
        cv.setStmtyear(rs.getString("stmtyear"));
        cv.setPaymentmode(rs.getString("paymentmode"));
        cv.setChequeno(rs.getString("chequeno"));

        return cv;
    }

    public static double getDebit(LoginProfile log, String refno) throws SQLException, Exception {
        double amount = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select amount from cb_official where refer=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amount = rs.getDouble("amount");
        }

        rs.close();
        stmt.close();

        return amount;
    }

    public static double getCredit(LoginProfile log, String refno) throws SQLException, Exception {
        double amt = 0;
        double amttax = 0;
        double totalCredit = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select sum(amount+taxamt) as amt from cb_official_credit where voucherno=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amt = rs.getDouble("amt");
        }

        return amt;
    }

    public static int getCheckCounter(LoginProfile log) throws Exception {

        int cnt = 0;
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from cb_official where year = ? and period = ? and cid = ''");

        stmt.setString(1, AccountingPeriod.getCurYear(log));
        stmt.setString(2, AccountingPeriod.getCurPeriod(log));
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }

        return cnt;
    }

    public static int getApproveCounter(LoginProfile log) throws Exception {

        int cnt = 0;
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from cb_official where year = ? and period = ? and cid <> ? and pid = ?");

        stmt.setString(1, AccountingPeriod.getCurYear(log));
        stmt.setString(2, AccountingPeriod.getCurPeriod(log));
        stmt.setString(3, "");
        stmt.setString(4, "");
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }

        return cnt;
    }

    public static void checkVoucher(LoginProfile log, String refer, String staff_id, String staff_name) throws Exception {

        String query = "UPDATE cb_official SET checkid = '" + staff_id + "',checkname = '" + staff_name + "',checkdate='" + AccountingPeriod.getCurrentTimeStamp() + "' where refer = '" + refer + "'";

        try {
            PreparedStatement ps = log.getCon().prepareStatement(query);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void approveVoucher(LoginProfile log, String refer, String staff_id, String staff_name, String position) throws Exception {

        String query = "UPDATE cb_official SET appid = '" + staff_id + "',appname = '" + staff_name + "',approvedate='" + AccountingPeriod.getCurrentTimeStamp() + "', appposition = '" + position + "' where refer = '" + refer + "'";

        try {
            PreparedStatement ps = log.getCon().prepareStatement(query);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static Object getOfficialReceipt(String refno) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static String getCheckApprove(LoginProfile log, String function, String refno) throws Exception {

        ResultSet rs = null;
        String func = "disabled";

        PreparedStatement stmt = log.getCon().prepareStatement("SELECT " + function + " as func FROM `cb_official` where refer = '" + refno + "'");

        rs = stmt.executeQuery();
        if (rs.next()) {
            if (rs.getString(1).equals("")) {
                func = "";
            }
        }

        rs.close();
        stmt.close();
        return func;

    }

    public static List<SalesInvoice> getSalesInvoice(LoginProfile log, String keyword) throws Exception {

        Statement stmt = null;
        List<SalesInvoice> Com;
        Com = new ArrayList();

        String q = "";

        if (keyword != null) {
            q = "where invref like '%" + keyword + "%' or bname like '%" + keyword + "%'";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from sl_inv  " + q + " order by invref");
            while (rs.next()) {

                Com.add(SalesInvoiceDAO.getINV(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }

    public static String getStatus(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String check = "";
        String app = "";
        String post = "";
        String status = "<span class=\"label label-primary\">Preparing</span>";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select checkid,appid,post from cb_official where refer = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            check = rs.getString("checkid");
            app = rs.getString("appid");
            post = rs.getString("post");
        }

        stmt.close();
        rs.close();

        if (!check.equals("")) {
            status = "Checked";
        }

        if (!app.equals("")) {
            status = "Approved";
        }

//        if(post.equals("posted")){
//            status = "<span class=\"label label-info\">Post</span>";
//        }
        if (post.equals("cancel")) {
            status = "Canceled";
        }

        return status;
    }

    public static boolean isCheck(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select checkid from cb_official where refer = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString("checkid");
        }

        if (!ck.equals("")) {
            cek = true;
        }

        stmt.close();
        rs.close();

        return cek;
    }

    public static boolean isApprove(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select appid from cb_official where refer = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString("appid");
        }

        if (!ck.equals("")) {
            cek = true;
        }

        stmt.close();
        rs.close();

        return cek;
    }

    public static OfficialCreditItem getORTax(LoginProfile log, String id) throws SQLException, Exception {
        OfficialCreditItem c = null;
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_official_credit where refer=? and taxamt>0");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getINVitem(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static List<OfficialCreditItem> getAllORTax(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<OfficialCreditItem> CVi;
        CVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs;
            rs = stmt.executeQuery("select * from cb_official_credit where voucherno = '" + refno + "' and taxamt>0 order by refer");

            while (rs.next()) {
                CVi.add(getINVitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<ORPayer> getPayerHasInvoice(LoginProfile log, String keyword) throws Exception {

        Statement stmt = null;
        List<ORPayer> Com;
        Com = new ArrayList();

        String q = "";

        if (keyword != null) {
            q = " and bcode like '%" + keyword + "%' or bname like '%" + keyword + "%'";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select distinct(bcode) as code,bname as name from sl_inv where amountno > paid and postflag='posted' " + q + " order by bcode");
            while (rs.next()) {

                ORPayer vp = new ORPayer();

                vp.setPayercode(rs.getString("code"));
                vp.setPayername(rs.getString("name"));
                vp.setTotalInvoice(getTotalInvoices(log, rs.getString("code")));

                Com.add(vp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }

    private static int getTotalInvoices(LoginProfile log, String code) throws Exception {

        int cnt = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from sl_inv where bcode=? and amountno > paid and postflag='posted'");
        stmt.setString(1, code);
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }

        return cnt;
    }

    public static int saveIntoTempInvoice(LoginProfile log, List<ORTempInvoiceAmount> listT) throws Exception {

        int i = 0;
        try {

            String deleteQuery_2 = "delete from cb_payvoucher_temp where sessionid = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, listT.get(0).getSessionid());
                ps_2.executeUpdate();
            }

            for (ORTempInvoiceAmount m : listT) {

                String q = ("INSERT INTO cb_official_amounttopay(sessionid,invrefno,amounttopay,buyercode) values (?,?,?,?)");

                try (PreparedStatement ps = log.getCon().prepareStatement(q)) {

                    ps.setString(1, m.getSessionid());
                    ps.setString(2, m.getInvrefno());
                    ps.setDouble(3, m.getAmounttopay());
                    ps.setString(4, m.getBuyercode());

                    Logger.getLogger(OfficialReceiptDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
                    ps.executeUpdate();

                    i = 1;
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
            i = 0;
        }

        return i;

    }

    public static int saveIntoTempCredit(LoginProfile log, List<ORTempCredit> listT) throws Exception {

        int i = 0;
        try {

            String deleteQuery_2 = "delete from cb_official_credit_temp where sessionid = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, listT.get(0).getSessionid());
                ps_2.executeUpdate();
            }

            for (ORTempCredit m : listT) {

                String q = ("INSERT INTO cb_official_credit_temp(sessionid,referno,amounttopay,partycode,frommodule) values (?,?,?,?,?)");

                try (PreparedStatement ps = log.getCon().prepareStatement(q)) {

                    ps.setString(1, m.getSessionid());
                    ps.setString(2, m.getReferno());
                    ps.setDouble(3, m.getAmounttopay());
                    ps.setString(4, m.getPartycode());
                    ps.setString(5, m.getFrommodule());

                    Logger.getLogger(OfficialReceiptDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
                    ps.executeUpdate();

                    i = 1;
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
            i = 0;
        }

        return i;

    }

    public static String getRemark(LoginProfile log, String id) throws Exception {

        String remark = "Receipt for Invoice No : ";
        String inv = "";

        List<ORTempInvoiceAmount> listAll = (List<ORTempInvoiceAmount>) getInvoice(log, id);
        int x = listAll.size();
        int i = 0;
        for (ORTempInvoiceAmount all : listAll) {
            i++;

            if (i == listAll.size()) {
                inv = inv + all.getInvrefno();
            } else {
                inv = inv + all.getInvrefno() + ", ";
            }

        }

        return remark + inv;
    }

    public static Double getSumInvoice(LoginProfile log, String id) throws Exception {

        Double x = 0.00;
        Statement stment = log.getCon().createStatement();
        ResultSet setent = stment.executeQuery(" select sum(amounttopay) as sumamount from cb_official_amounttopay where sessionid = '" + id + "'");

        if (setent.next()) {
            x = setent.getDouble("sumamount");
        }

        stment.close();
        setent.close();
        return x;
    }

    public static List<ORTempInvoiceAmount> getInvoice(LoginProfile log, String id) throws Exception {

        List<ORTempInvoiceAmount> Com;
        Com = new ArrayList();

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_official_amounttopay where sessionid=? order by invrefno asc");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            while (rs.next()) {

                ORTempInvoiceAmount vp = new ORTempInvoiceAmount();

                vp.setInvrefno(rs.getString("invrefno"));
                vp.setAmounttopay(rs.getDouble("amounttopay"));
                vp.setPortion(rs.getString("portion"));
                vp.setBuyercode(rs.getString("buyercode"));

                Com.add(vp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }

    public static List<ORTempCredit> getTempCredit(LoginProfile log, String id) throws Exception {

        List<ORTempCredit> Com;
        Com = new ArrayList();

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_official_credit_temp where sessionid=? order by referno asc");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            while (rs.next()) {

                ORTempCredit vp = new ORTempCredit();

                vp.setReferno(rs.getString("referno"));
                vp.setAmounttopay(rs.getDouble("amounttopay"));
                vp.setFrommodule(rs.getString("frommodule"));
                vp.setPartycode(rs.getString("partycode"));

                Com.add(vp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }

    public static ORReceiptMaster getAllInfo(LoginProfile log, String sessionID, String companycode, String companyname) throws Exception {

        ORReceiptMaster mt = new ORReceiptMaster();
        OfficialReceipt pv = new OfficialReceipt();
        List<OfficialCreditItem> l = new ArrayList();

        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_official_temp_master where sessionid=?");
            stmt.setString(1, sessionID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                pv = getINV(rs);
            }

            mt.setMaster(pv);

            List<ORTempInvoiceAmount> listx = (List<ORTempInvoiceAmount>) getInvoice(log, sessionID);
            for (ORTempInvoiceAmount j : listx) {

                OfficialCreditItem cv = new OfficialCreditItem();

                cv.setAmount(String.valueOf(j.getAmounttopay()));
                cv.setCoacode(BuyerDAO.getInfo(log, j.getBuyercode()).getCoa());
                cv.setCoadescp(BuyerDAO.getInfo(log, j.getBuyercode()).getCoadescp());
                cv.setLoccode(companycode);
                cv.setLoclevel("Company");
                cv.setLocname(companyname);
                cv.setRefer("");
                cv.setRemarks("INVOICE NO : " + j.getInvrefno());
                cv.setSacode(j.getBuyercode());
                cv.setSadesc(BuyerDAO.getInfo(log, j.getBuyercode()).getCompanyname());
                cv.setSatype("Buyer");
                cv.setTaxcode("00");
                cv.setTaxdescp("None");
                cv.setTaxrate(0.00);
                cv.setVoucherno("");
                cv.setAmtbeforetax(0.00);
                cv.setInvrefno(j.getInvrefno());

                l.add(cv);

            }

            mt.setListInvoice(l);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return mt;
    }

    public static ORReceiptMaster getAllInfoGeneral(LoginProfile log, String sessionID, String companycode, String companyname) throws Exception {

        ORReceiptMaster mt = new ORReceiptMaster();
        OfficialReceipt pv = new OfficialReceipt();
        List<OfficialCreditItem> l = new ArrayList();

        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_official_temp_master where sessionid=?");
            stmt.setString(1, sessionID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                pv = getINV(rs);
            }

            mt.setMaster(pv);

            List<ORTempCredit> listx = (List<ORTempCredit>) getTempCredit(log, sessionID);
            for (ORTempCredit j : listx) {
                //Logger.getLogger(OfficialReceiptDAO.class.getName()).log(Level.INFO, j.getBuyercode()+"----===---"+BuyerDAO.getInfo(j.getBuyercode()).getCoa()+"==="+BuyerDAO.getInfo(j.getBuyercode()).getCoadescp());

                OfficialCreditItem cv = new OfficialCreditItem();

                PaymentVoucherType pvt = (PaymentVoucherType) getTypeDetail(log, j.getFrommodule(), j.getPartycode());

                cv.setAmount(String.valueOf(j.getAmounttopay()));
                cv.setCoacode(pvt.getCoacode());
                cv.setCoadescp(pvt.getCoadesc());
                cv.setLoccode(companycode);
                cv.setLoclevel("Company");
                cv.setLocname(companyname);
                cv.setRefer("");
                cv.setRemarks(pvt.getRemark());
                cv.setSacode(pvt.getSacode());
                cv.setSadesc(pvt.getSadesc());
                cv.setSatype(pvt.getSatype());
                cv.setTaxcode("00");
                cv.setTaxdescp("None");
                cv.setTaxrate(0.00);
                cv.setVoucherno("");
                cv.setAmtbeforetax(0.00);

                l.add(cv);

            }

            mt.setListInvoice(l);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return mt;
    }

    public static int saveIntoTempMaster(LoginProfile log, OfficialReceipt item) throws Exception {

        int i = 0;
        //String newrefer = AutoGenerate.getReferenceNo("refer", "cb_payvoucher", "PVN", item.getEstatecode());

        String deleteQuery_2 = "delete from cb_official_temp_master where sessionid = ?";
        try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
            ps_2.setString(1, item.getSessionID());
            ps_2.executeUpdate();
        }

        try {
            //ps.setString(1, AutoGenerate.get2digitNo("info_bank", "code"));
            String q = ("insert into cb_official_temp_master(refer,voucherno,date,paidtype,paidcode,paidname,paidaddress,paidcity,paidstate,paidpostcode,gstid,remarks,amount,rm,receivemode,racoacode,racoadesc,bankcode,bankname,year,period,estatecode,estatename,invno,sessionid,flagbank,stmtyear,stmtperiod) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, "ORN");
            ps.setInt(2, 0);
            ps.setString(3, item.getDate());
            ps.setString(4, item.getPaidtype());
            ps.setString(5, item.getPaidcode());
            ps.setString(6, item.getPaidname());
            ps.setString(7, item.getPaidaddress());
            ps.setString(8, item.getPaidcity());
            ps.setString(9, item.getPaidstate());
            ps.setString(10, item.getPaidpostcode());
            ps.setString(11, item.getGstid());
            ps.setString(12, item.getRemarks());
            ps.setDouble(13, item.getAmount());
            ps.setString(14, item.getRm());
            ps.setString(15, item.getReceivemode());
            ps.setString(16, item.getRacoacode());
            ps.setString(17, item.getRacoadesc());
            ps.setString(18, item.getBankcode());
            ps.setString(19, item.getBankname());
            ps.setInt(20, item.getYear());
            ps.setInt(21, item.getPeriod());
            ps.setString(22, item.getEstatecode());
            ps.setString(23, item.getEstatename());
            ps.setString(24, item.getInvno());
            ps.setString(25, item.getSessionID());
            ps.setString(26, item.getFlagbank());
            ps.setString(27, item.getStmtyear());
            ps.setString(28, item.getStmtperiod());

            ps.executeUpdate();

            ps.close();

            i = 1;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return i;

    }

    public static String generateReceipt(String sessionID, LoginProfile log) throws Exception {

        ORReceiptMaster am = (ORReceiptMaster) OfficialReceiptDAO.getAllInfo(log, sessionID, log.getEstateCode(), log.getEstateDescp());

        OfficialReceipt pv = new OfficialReceipt();
        OfficialReceipt p = am.getMaster();

        String newrefer = AutoGenerate.getReferenceNox(log, "refer", "cb_official", "ORN", p.getEstatecode(), String.valueOf(p.getYear()), String.valueOf(p.getPeriod()));
        p.setRefer(newrefer);
        p.setPreid(log.getUserID());
        p.setPrename(log.getFullname());
        p.setPreposition(log.getPosition());
        p.setPreparedate(AccountingPeriod.getCurrentTimeStamp());
        p.setVoucherno(Integer.parseInt(AutoGenerate.get4digitNo(log, "cb_official", "voucherno")));

        OfficialReceiptDAO.saveMain(log, p);

        List<OfficialCreditItem> listx = (List<OfficialCreditItem>) am.getListInvoice();
        for (OfficialCreditItem j : listx) {

            String addBack = AutoGenerate.getReferAddBack(log, "cb_official_credit", "refer", newrefer, "voucherno");
            j.setRefer(addBack);
            j.setVoucherno(newrefer);
            Logger.getLogger(OfficialReceiptDAO.class.getName()).log(Level.INFO, "----===---@@@@@@@@@@@@@@@******--------" + addBack);
            OfficialReceiptDAO.saveItemInvoice(log, j);
            updateInvoice(log, j.getInvrefno(), Double.parseDouble(j.getAmount()));
            saveReceiptInvoiceReference(log, newrefer, j.getInvrefno(), addBack, Double.parseDouble(j.getAmount()));

        }

        return newrefer;
    }

    public static String generateReceiptGeneral(String sessionID, LoginProfile log) throws Exception {

        ORReceiptMaster am = (ORReceiptMaster) OfficialReceiptDAO.getAllInfoGeneral(log, sessionID, log.getEstateCode(), log.getEstateDescp());

        OfficialReceipt pv = new OfficialReceipt();
        OfficialReceipt p = am.getMaster();

        String newrefer = AutoGenerate.getReferenceNo(log, "refer", "cb_official", "ORN", p.getEstatecode());
        p.setRefer(newrefer);
        p.setPreid(log.getUserID());
        p.setPrename(log.getFullname());
        p.setPreposition(log.getPosition());
        p.setPreparedate(AccountingPeriod.getCurrentTimeStamp());
        p.setVoucherno(Integer.parseInt(AutoGenerate.get4digitNo(log, "cb_official", "voucherno")));

        OfficialReceiptDAO.saveMain(log, p);

        List<OfficialCreditItem> listx = (List<OfficialCreditItem>) am.getListInvoice();
        for (OfficialCreditItem j : listx) {

            String addBack = AutoGenerate.getReferAddBack(log, "cb_official_credit", "refer", newrefer, "voucherno");
            j.setRefer(addBack);
            j.setVoucherno(newrefer);
            Logger.getLogger(OfficialReceiptDAO.class.getName()).log(Level.INFO, "----===---@@@@@@@@@@@@@@@=======--------" + addBack);
            OfficialReceiptDAO.saveItemInvoice(log, j);

        }

        return newrefer;
    }

    private static void updateInvoice(LoginProfile log, String invrefno, double amount) throws SQLException, Exception {

        ResultSet rs = null;

        double amountinv = 0.0;
        double paid = 0.00;

        PreparedStatement stmt = log.getCon().prepareStatement("select * from sl_inv where invref=?");
        stmt.setString(1, invrefno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amountinv = rs.getDouble("amountno");
            paid = rs.getDouble("paid");
        }
        
        Logger.getLogger(OfficialReceiptDAO.class.getName()).log(Level.INFO, "----===--!!!!!!--------" + amountinv + " ----- "+ amount);

        if (GeneralTerm.twoDecimalDouble(amountinv) >= amount) {

            String query = "UPDATE sl_inv SET paid = ? where invref = '" + invrefno + "'";
            Logger.getLogger(OfficialReceiptDAO.class.getName()).log(Level.INFO, "----===--********--------" + query);
            try (PreparedStatement ps = log.getCon().prepareStatement(query)) {
                ps.setDouble(1, amount + paid);
                ps.executeUpdate();
            }

        }

    }

    private static void saveReceiptInvoiceReference(LoginProfile log, String ornno, String invno, String ornrefno, double amount) throws SQLException, Exception {

        ResultSet rs = null;

        String q = ("insert into cb_official_invoice(ornno,invno,ornrefno,amount) values (?,?,?,?)");
        PreparedStatement ps = log.getCon().prepareStatement(q);
        ps.setString(1, ornno);
        ps.setString(2, invno);
        ps.setString(3, ornrefno);
        ps.setDouble(4, amount);

        ps.executeUpdate();

        ps.close();

    }

    private static void updateORAmount(LoginProfile log, String ornno, double amount) throws SQLException, Exception {

        ResultSet rs = null;

        String q = ("UPDATE cb_official SET amount = ?, rm = ? WHERE refer = '" + ornno + "'");
        PreparedStatement ps = log.getCon().prepareStatement(q);
        ps.setDouble(1, OfficialReceiptDAO.getINV(log, ornno).getAmount() + amount);
        ps.setString(2, EnglishNumberToWords.toConvert(OfficialReceiptDAO.getINV(log, ornno).getAmount() + amount));

        ps.executeUpdate();
        ps.close();

    }

    public static int generateFromDebitNote(LoginProfile log, String refno, String noteno) throws Exception {

        int i = 0;

        ArDebitNote f = (ArDebitNote) ArDebitNoteDAO.getMain(log, noteno);

        OfficialCreditItem o = new OfficialCreditItem();

        o.setAmount(String.valueOf(f.getTotal()));
        o.setCoacode(BuyerDAO.getInfo(log, f.getBuyercode()).getCoa());
        o.setCoadescp(BuyerDAO.getInfo(log, f.getBuyercode()).getCoadescp());
        o.setLoccode(f.getEstcode());
        o.setLoclevel("Company");
        o.setLocname(f.getEstname());
        o.setRemarks("DEBIT NO : " + f.getNoteno());
        o.setSacode(f.getBuyercode());
        o.setSadesc(f.getBuyername());
        o.setSatype("Buyer");
        o.setRefer(refno);

        OfficialReceiptDAO.saveItem(log, o);
        updateORAmount(log, refno, f.getTotal());

        i = 1;

        return i;

    }

    private static PaymentVoucherType getTypeDetail(LoginProfile log, String type, String code) throws Exception {

        PaymentVoucherType pvt = new PaymentVoucherType();

        if (type.equals("020207")) {

            CeManageBank sp = (CeManageBank) BankDAO.getInfo(log, code);

            pvt.setCoacode(EstateDAO.getEstateInfo(log, "2411", "estatecode").getHqsuspence());
            pvt.setCoadesc(EstateDAO.getEstateInfo(log, "2411", "estatecode").getHqsuspencedescp());
            pvt.setCode(code);
            pvt.setName(sp.getName());
            pvt.setSacode("00");
            pvt.setSadesc("Not Applicable");
            pvt.setSatype("None");
            pvt.setType("Bank");
            pvt.setRemark("Hibah");
        }

        return pvt;

    }

    public static ORReceiptMaster getORMaster(LoginProfile log, String refno) throws Exception {

        ORReceiptMaster pm = new ORReceiptMaster();

        pm.setMaster(getINV(log, refno));
        pm.setListInvoice(getAllINVItem(log, refno));

        return pm;

    }

    public static String replicateData(LoginProfile log, String refno, String replicateDate, String replicateYear, String replicatePeriod) throws Exception {
        String refer = "";

        ORReceiptMaster pm = (ORReceiptMaster) getORMaster(log, refno);
        pm.getMaster().setPeriod(Integer.parseInt(replicatePeriod));
        pm.getMaster().setYear(Integer.parseInt(replicateYear));
        pm.getMaster().setPaymentmode("None");
        pm.getMaster().setPreid(log.getUserID());
        pm.getMaster().setPrename(log.getFullname());
        pm.getMaster().setPreparedate(AccountingPeriod.getCurrentTimeStamp());
        pm.getMaster().setDate(replicateDate);
        pm.getMaster().setPreposition(log.getPosition());

        refer = OfficialReceiptDAO.saveMain(log, pm.getMaster());

        List<OfficialCreditItem> listx = (List<OfficialCreditItem>) pm.getListInvoice();
        for (OfficialCreditItem j : listx) {

            j.setRefer(refer);
            OfficialReceiptDAO.saveItem(log, j);

        }

        return refer;
    }

    public static boolean isComplete(LoginProfile log, String refno) throws Exception {

        boolean s = true;
        ORReceiptMaster pm = (ORReceiptMaster) getORMaster(log, refno);

        OfficialReceipt p = (OfficialReceipt) pm.getMaster();

        if (p.getBankcode().equals("") || p.getBankcode() == null) {
            s = false;
        } else if (p.getAmount() <= 0.0) {
            s = false;
        } else if (p.getPaidtype().equals("") || p.getPaidtype() == null) {
            s = false;
        } else if (p.getPaidcode().equals("") || p.getPaidcode() == null) {
            s = false;
        }

        return s;
    }

    public static boolean hasDCNote(LoginProfile log, String refno) throws Exception {

        boolean s = false;

        ORReceiptMaster pm = (ORReceiptMaster) getORMaster(log, refno);

        List<OfficialCreditItem> listx = (List<OfficialCreditItem>) pm.getListInvoice();
        for (OfficialCreditItem j : listx) {

            s = DebitCreditNoteDAO.checkSuspence(log, j.getCoacode());

        }

        return s;
    }

    public static void cancelThis(LoginProfile log, String refer) throws SQLException, Exception {

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_official_invoice where ornno=?");
        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        while (rs.next()) {
            updatePaidAmount(log, rs.getString("invno"), rs.getDouble("amount"));
        }

        updateCancel(log, refer);
        LiveGLDAO.deleteFromGL(log, refer);

    }

    private static void updatePaidAmount(LoginProfile log, String invno, double amount) throws SQLException, Exception {

        ResultSet rs = null;

        String q = ("UPDATE sl_inv SET paid = (paid - '" + amount + "') WHERE invref = '" + invno + "'");
        PreparedStatement ps = log.getCon().prepareStatement(q);
        //ps.setDouble(1, OfficialReceiptDAO.getINV(log, ornno).getAmount() + amount);

        ps.executeUpdate();
        ps.close();

    }

    private static void updateCancel(LoginProfile log, String refer) throws SQLException, Exception {

        String q = ("UPDATE cb_official SET post = ? WHERE refer = '" + refer + "'");
        PreparedStatement ps = log.getCon().prepareStatement(q);
        ps.setString(1, "cancel");

        ps.executeUpdate();
        ps.close();

    }

    public static List<OfficialReceipt> getAll(LoginProfile log, DataTable prm) throws Exception, SQLException {

        Statement stmt = null;
        List<OfficialReceipt> CV;
        CV = new ArrayList();

        String q = "";
        if (prm.getViewBy().equals("today")) {
            q = " WHERE date = '" + AccountingPeriod.getCurrentTimeStamp() + "'";
        } else if (prm.getViewBy().equals("thismonth")) {
            q = " WHERE period = '" + AccountingPeriod.getCurPeriod(log) + "' and year = '" + AccountingPeriod.getCurYear(log) + "'";
        } else if (prm.getViewBy().equals("byperiod")) {
            q = " WHERE period = '" + prm.getPeriod() + "' and year = '" + prm.getYear() + "'";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_official " + q + " order by refer");

            while (rs.next()) {
                CV.add(getINV(rs));
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

}
