/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.dao.financial.gl;

import com.lcsb.fms.dao.financial.tx.TaxMasterDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.model.financial.gl.AccountPeriod;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.dao.ModuleDAO;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Dell
 */
public class CnOPeriodDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("020110");
        mod.setModuleDesc("Close & Open Period");
        mod.setMainTable("chartofacccount");
        mod.setReferID_Master("code");
        return mod;

    }

    public static int finalizingAccount(LoginProfile log) throws Exception {

        int success = 1;

        String nextYear = "";
        String nextPeriod = "";
        String currPeriod = AccountingPeriod.getCurPeriod(log);
        String currYear = AccountingPeriod.getCurYear(log);

        int successCount = 0;
        int notAavailable = 0;
        int failCount = 0;

        if (Integer.parseInt(currPeriod) == 12) {
            nextPeriod = "1";
            nextYear = String.valueOf(Integer.parseInt(currYear) + 1);
        } else if (Integer.parseInt(currPeriod) < 12) {
            nextPeriod = String.valueOf(Integer.parseInt(currPeriod) + 1);
            nextYear = currYear;
        }

        Connection con = log.getCon();
        try {
            Statement stmt = con.createStatement();

            stmt.addBatch("delete from gl_openingbalance_detail where concat(year,period)=concat('" + currYear + "','" + currPeriod + "')");//0
            stmt.addBatch("delete from gl_openingbalance where concat(year,period)=concat('" + nextYear + "','" + nextPeriod + "')");//1
            stmt.addBatch("delete from gl_closingbalance where concat(year,period)=concat('" + currYear + "','" + currPeriod + "')");//2
            stmt.addBatch("delete from gl_closingbalance where concat(year,period)=concat('" + currYear + "','" + currPeriod + "')");//3
            stmt.addBatch("update gl_posting_distribute set gcflag='posted' where  novoucher like 'jvg%' and gcflag='NO'");//4	
            stmt.addBatch("delete from gl_balanceopening");//5
            stmt.addBatch("delete from gl_balanceopening_temp");//6
            stmt.addBatch("delete from gl_balanceclosing");//7
            stmt.addBatch("delete from gl_balancedetail");//8
            stmt.addBatch("delete from gl_balancedistribute");//9
            stmt.addBatch("insert into gl_balancedetail(itemid,year,period,coacode,coadesc,applevel,apptype,loccode,locdesc,stagecode,stagedesc,debit,credit,satype,sacode,sadesc) select concat(coacode,year,period,loccode,stgcode,satype,sacode),year,period,coacode,coadesc,loclevel,t2.apptype,loccode,locdesc,stgcode,stgdesc, ifnull(round(sum(debit),3),0) as sdebit,ifnull(round(sum(credit),3),0) as scredit,satype,sacode,sadesc from gl_posting_distribute left join chartofacccount as t2 on coacode=t2.code where concat(year,period)=concat('" + currYear + "','" + currPeriod + "') group by coacode,loclevel,loccode,stgcode,satype,sacode");//10
            stmt.addBatch("delete from gl_balancedetail where round(debit,2)=round(credit,2)");//11
            stmt.addBatch("insert into gl_balanceclosing   select * from  gl_openingbalance where concat(year,period)=concat('" + currYear + "','" + currPeriod + "')");//12
            stmt.addBatch("update gl_balanceclosing  as t1 left join gl_balancedetail as t2 on t1.itemid=t2.itemid set t1.debit=round(ifnull(t1.debit,0)+ifnull(t2.debit,0),2),t1.credit=round(ifnull(t1.credit,0)+ifnull(t2.credit,0),2) where t1.itemid is not null");//13
            stmt.addBatch("insert into gl_balanceclosing(itemid,year,period,coacode,coadesc,applevel,apptype,loccode,locdesc,stagecode,stagedesc,debit,credit,satype,sacode,sadesc)   select t1.itemid,t1.year,t1.period,t1.coacode,t1.coadesc,t1.applevel,t1.apptype,t1.loccode,t1.locdesc,t1.stagecode,t1.stagedesc,t1.debit,t1.credit,t1.satype,t1.sacode,t1.sadesc from  gl_balancedetail as t1 left join gl_balanceclosing as t2 on t1.itemid=t2.itemid where t2.itemid is null");//14
            stmt.addBatch("DELETE FROM gl_balanceclosing WHERE round(DEBIT,2)=round(CREDIT,2)");//15
            stmt.addBatch("DELETE FROM gl_balanceopening_temp");//16
            stmt.addBatch("insert into gl_balanceopening_temp (itemid,year,period,coacode,coadesc,applevel,apptype,loccode,locdesc,stagecode,stagedesc,debit,credit,satype,sacode,sadesc) select itemid,year,period,coacode,coadesc,applevel,apptype,loccode,locdesc,stagecode,stagedesc,if(debit>credit,round(debit-credit,2),0),if(credit>debit,round(credit-debit,2),0),satype,sacode,sadesc from gl_balanceclosing where debit <> 0 and credit<>0 ");//17
            stmt.addBatch("DELETE FROM gl_balanceclosing WHERE debit <> 0 and credit<>0 ");//18
            stmt.addBatch("insert into gl_balanceclosing select * from gl_balanceopening_temp");//19
            stmt.addBatch("insert into gl_balanceopening   select * from  gl_balanceclosing ");//20
            stmt.addBatch("DELETE FROM gl_balanceopening_temp");//21
            stmt.addBatch("insert into gl_balanceopening_temp  (itemid,year,period,coacode,coadesc,applevel,apptype,loccode,locdesc,stagecode,stagedesc,debit,credit,satype,sacode,sadesc)  select t1.itemid,t1.year,t1.period,t1.coacode,t1.coadesc,t1.applevel,t1.apptype,t1.loccode,t1.locdesc,t1.stagecode,t1.stagedesc,sum(t1.debit),sum(t1.credit),t1.satype,t1.sacode,t1.sadesc from  gl_balanceopening as t1 group by t1.year,t1.period,t1.coacode,t1.loccode,t1.stagecode,t1.satype,t1.sacode");//22
            stmt.addBatch("DELETE FROM gl_balanceopening");//23
            stmt.addBatch("insert into gl_balanceopening   select * from  gl_balanceopening_temp ");//24
            stmt.addBatch("delete from gl_balanceopening where round(debit,2)=round(credit,2)");//25	
            stmt.addBatch("DELETE FROM gl_balanceopening_temp");//26
            stmt.addBatch("insert into gl_balanceopening_temp (itemid,year,period,coacode,coadesc,applevel,apptype,loccode,locdesc,stagecode,stagedesc,debit,credit,satype,sacode,sadesc) select itemid,year,period,coacode,coadesc,applevel,apptype,loccode,locdesc,stagecode,stagedesc,if(debit>credit,round(debit-credit,2),0),if(credit>debit,round(credit-debit,2),0),satype,sacode,sadesc from gl_balanceopening where debit <> 0 and credit<>0 ");//27
            stmt.addBatch("DELETE FROM gl_balanceopening WHERE debit <> 0 and credit<>0 ");//28
            stmt.addBatch("insert into gl_balanceopening select * from gl_balanceopening_temp ");//29
            stmt.addBatch("update gl_balanceopening set itemid=concat(coacode,'" + nextYear + "','" + nextPeriod + "',loccode,stagecode,satype,sacode),year=" + nextYear + ",period=" + nextPeriod);//30
            stmt.addBatch("insert into gl_openingbalance   select * from  gl_balanceopening ");//31
            stmt.addBatch("insert into gl_closingbalance   select * from  gl_balanceclosing ");//32
            stmt.addBatch("insert into gl_openingbalance_detail   select * from  gl_balancedetail ");//33
            stmt.addBatch("DELETE FROM gl_openingbalance WHERE round(debit,2)=ROUND(credit,2)");//34
            stmt.addBatch("DELETE FROM gl_openingbalance_detail WHERE round(debit,2)=round(credit,2)");//35
            stmt.addBatch("DELETE FROM gl_closingbalance WHERE round(debit,2)=round(credit,2)");//36

            int[] recordsAffected = stmt.executeBatch();
            stmt.clearBatch();
            stmt.close();

        } catch (BatchUpdateException buex) {
            buex.printStackTrace();
            //LogUtil.error(buex);
            int[] updateCounts = buex.getUpdateCounts();
            for (int i = 0; i < updateCounts.length; i++) {
                if (updateCounts[i] >= 0) {
                    successCount++;

                } else if (updateCounts[i] == Statement.SUCCESS_NO_INFO) {
                    notAavailable++;

                } else if (updateCounts[i] == Statement.EXECUTE_FAILED) {
                    failCount++;

                }
                if (updateCounts[i] == Statement.EXECUTE_FAILED) {
                    failCount++;
                    Logger.getLogger(CnOPeriodDAO.class.getName()).log(Level.INFO, "Failed to execute record at:" + i);
                    success = 0;
                }

            }
        } finally {
            Logger.getLogger(CnOPeriodDAO.class.getName()).log(Level.INFO, "Number of affected rows before Batch Error :: " + successCount);
            Logger.getLogger(CnOPeriodDAO.class.getName()).log(Level.INFO, "Number of affected rows not available:" + notAavailable);
            Logger.getLogger(CnOPeriodDAO.class.getName()).log(Level.INFO, "Failed Count in Batch because of Error:" + failCount);

        }
        return success;
    }

    public static int confirmClose(LoginProfile log) throws SQLException, Exception {

        Connection con = log.getCon();
        int success = 1;
        AccountPeriod cur = (AccountPeriod) AccountPeriodDAO.getCurPeriod(log);
        AccountPeriod pre = (AccountPeriod) AccountPeriodDAO.getPreviousPeriod(log);
        String nextYear = String.valueOf(Integer.parseInt(cur.getYear()) + 1);
        String theYear = "";

        int nextPeriod = 0;

        if (cur.getPeriod() == 12) {
            try {
                generateOneYearPeriod(log, nextYear);
            } catch (SQLException e) {
                e.printStackTrace();
                success = 0;
            }

            theYear = nextYear;
            nextPeriod = 1;

        } else {
            nextPeriod = cur.getPeriod() + 1;
            theYear = cur.getYear();
        }

        try {
            String q1 = ("UPDATE gl_altaccountingperiod SET current='No'");
            String q2 = ("UPDATE gl_altaccountingperiod SET closedate='" + AccountingPeriod.getCurrentTimeStamp() + "' where concat(year,period)=concat('" + cur.getYear() + "','" + cur.getPeriod() + "')");
            String q3 = ("UPDATE gl_altaccountingperiod SET current='Yes' where concat(year,period)=concat('" + theYear + "','" + nextPeriod + "')");
            PreparedStatement ps1 = con.prepareStatement(q1);
            PreparedStatement ps2 = con.prepareStatement(q2);
            PreparedStatement ps3 = con.prepareStatement(q3);

            ps1.executeUpdate();
            ps2.executeUpdate();
            ps3.executeUpdate();

            ps1.close();
            ps2.close();
            ps3.close();
        } catch (SQLException e) {
            e.printStackTrace();
            success = 0;
        }

        return success;

    }

    private static void generateOneYearPeriod(LoginProfile log, String nextYear) throws Exception {

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from gl_altaccountingperiod where year='" + nextYear + "'");
        rs = stmt.executeQuery();
        if (rs.next()) {
            if (rs.getInt("cnt") == 0) {
                for (int i = 1; i <= 12; i++) {
                    Statement updap = log.getCon().createStatement();
                    java.util.Calendar kk = java.util.Calendar.getInstance();
                    kk.set(Integer.parseInt(nextYear), i - 1, 1);
                    int ll = kk.getActualMaximum(java.util.Calendar.DAY_OF_MONTH);
                    String q = "INSERT INTO gl_altaccountingperiod (year,period,month,startyear,endyear,startperiod,endperiod) VALUES (?,?,?,?,?,?,?)";
                    PreparedStatement ps = log.getCon().prepareStatement(q);
                    ps.setString(1, nextYear);
                    ps.setString(2, String.valueOf(i));
                    ps.setString(3, String.valueOf(i));
                    ps.setString(4, nextYear + "-01-01");
                    ps.setString(5, nextYear + "-12-31");
                    ps.setString(6, nextYear + "-" + String.valueOf(i) + "-01");
                    ps.setString(7, nextYear + "-" + String.valueOf(i) + "-" + String.valueOf(ll));

                    ps.executeUpdate();
                    ps.close();
                }
            }
        }

    }

    public static String getModuleList(LoginProfile log) throws SQLException {

        String render = "<br><table class=\"table table-bordered table-striped\"><tbody>";
        //render += "<thead><tr><th>Module ID</th><th>Module Name</th><th>Status</th></tr></thead>";

        String modtype = "";

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM gl_cp_module WHERE status= ? ORDER BY moduleid ASC");
        stmt.setBoolean(1, true);
        rs = stmt.executeQuery();
        while (rs.next()) {

            if (!modtype.equals(rs.getString("moduletypeid"))) {
                render += "<tr><th colspan=\"3\">" + rs.getString("moduletypedesc") + "</th></tr>";
            }
            modtype = rs.getString("moduletypeid");

            render += "<tr class=\"trclass\" id=\"tr"+rs.getString("moduleid")+"\"><td>" + rs.getString("moduleid") + "</td>";
            render += "<td>" + rs.getString("moduledesc") + "</td>";
            render += "<td width=\"20%\"><div class=\"statusdiv\" id=\"div"+rs.getString("moduleid")+"\">&nbsp;</div></td>";
            render += "</tr>";

        }

        render += "</tbody></table>";

        return render;

    }

    public static JSONArray getModuleArray(LoginProfile log) throws Exception {

        JSONArray array = new JSONArray();


        try {

            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM gl_cp_module WHERE status= ? ORDER BY moduleid ASC");
            stmt.setBoolean(1, true);
            rs = stmt.executeQuery();
            while (rs.next()) {
                
                array.add(rs.getString("moduleid"));

            } 

            rs.close();
            

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return array;

    }

    public static int checkTransactionForModule(LoginProfile log, String moduleid) throws SQLException, Exception {
        
        Module mod = (Module) ModuleDAO.getModule(log, moduleid);
        
        String period = "period";
        if(moduleid.equals("020104")){
            period = "curperiod";
        }
        
        int cnt = 0;
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from " + mod.getMainTable() + " where year = ? and "+period+" = ? and " + mod.getApprove() + " = ? and "+mod.getPost()+" <> ?");

        stmt.setString(1, AccountingPeriod.getCurYear(log));
        stmt.setString(2, AccountingPeriod.getCurPeriod(log));
        stmt.setString(3, "");
        stmt.setString(4, "cancel");
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }
        
        rs.close();

        return cnt;

    }

}
