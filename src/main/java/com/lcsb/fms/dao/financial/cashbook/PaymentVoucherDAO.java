package com.lcsb.fms.dao.financial.cashbook;

import com.lcsb.fms.controller.PathController;
import com.lcsb.fms.dao.financial.ap.VendorPaymentDAO;
import com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.LiveGLDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.general.DataTable;
import com.lcsb.fms.model.financial.ap.ApPaymentMaster;
import com.lcsb.fms.model.financial.ap.ApTempInvoiceAmount;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucher;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherMaster;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherTempDebitAccount;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherType;
import com.lcsb.fms.model.setup.company.CeManageBank;
import com.lcsb.fms.util.dao.BankDAO;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.dao.EstateDAO;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
/*
            ps.setString(1, item.getEstatecode());
            ps.setString(2, item.getEstatename());
            ps.setString(3, item.getEstateaddress());
            ps.setString(4, item.getEstatecity());
            ps.setString(5, item.getEstatestate());
            ps.setString(6, item.getEstatepostcode());
            ps.setString(7, item.getEstatephone());
            ps.setString(8, item.getEstatefax());
            ps.setString(9, item.getYear());
            ps.setString(10, item.getPeriod());
            ps.setString(11, item.getBankcode());
            ps.setString(12, item.getBankname());
            ps.setString(13, item.getVoucherno());
            ps.setString(14, item.getTarikh());
            ps.setString(15, item.getPaidtype());
            ps.setString(16, item.getPaidcode());
            ps.setString(17, item.getPaidname());
            ps.setString(18, item.getPaidaddress());
            ps.setString(19, item.getPaidcity());
            ps.setString(20, item.getPaidstate());
            ps.setString(21, item.getPaidpostcode());
            ps.setString(22, item.getPost());
            ps.setString(23, item.getTarikhpost());
            ps.setString(24, item.getRemarks());
            ps.setString(25, item.getAmount());
            ps.setString(26, item.getPaymentmode());
            ps.setString(27, item.getCekno());
            ps.setString(28, item.getAppid());
            ps.setString(29, item.getAppname());
            ps.setString(30, item.getAppposition());
            ps.setString(31, item.getPreid());
            ps.setString(32, item.getPrename());
            ps.setString(33, item.getPreposition());
            ps.setString(34, item.getFlag());
            ps.setString(35, item.getRefer());
            ps.setString(36, item.getFlagbank());
            ps.setString(37, item.getRm());
            ps.setString(38, item.getCheckid());
            ps.setString(39, item.getCheckname());
            ps.setString(40, item.getCheckposition());
            ps.setString(41, item.getStmtyear());
            ps.setString(42, item.getStmtperiod());
            ps.setString(43, item.getNoagree());
            ps.setString(44, item.getPaymentflag());
            ps.setString(45, item.getAdvancetype());
            ps.setString(46, item.getPreparedate());
            ps.setString(47, item.getCheckdate());
            ps.setString(48, item.getApprovedate());
            ps.setString(49, item.getAdvance());
            ps.setString(50, item.getPostgst());
            ps.setString(51, item.getGstid());
            ps.setString(52, item.getGstdate());
            ps.setString(53, item.getRacoacode());
            ps.setString(54, item.getRacoadesc());
 */
public class PaymentVoucherDAO {
    
    public static Module getModule() {
        
        Module mod = new Module();
        mod.setModuleID("020204");
        mod.setModuleDesc("Payment Voucher");
        mod.setMainTable("cb_payvoucher");
        mod.setReferID_Master("refer");
        return mod;
        
    }
    
    public static List<ListTable> tableList() {
        
        String column_names[] = {"refer", "tarikh", "paidname", "amount", "checkid", "appid", "post"};
        String title_name[] = {"Reference No.", "Date", "Payee", "Amount (RM)", "Check ID", " Approve ID", "Post"};
        boolean boolview[] = {true, true, true, true, false, false, false};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            i.setBoolView(boolview[j]);
            lt.add(i);
        }
        
        return lt;
    }
    
    public static void deleteExist(LoginProfile log, String no) throws SQLException, Exception {
        Connection con = log.getCon();
        
        String deleteQuery_1 = "delete from cb_payvoucher where refer = ?";
        String deleteQuery_2 = "delete from cb_payvaucher_debit where voucer = ?";
        PreparedStatement ps_1 = con.prepareStatement(deleteQuery_1);
        PreparedStatement ps_2 = con.prepareStatement(deleteQuery_2);
        ps_1.setString(1, no);
        ps_2.setString(1, no);
        ps_1.executeUpdate();
        ps_2.executeUpdate();
        ps_1.close();
        ps_2.close();
        
        LiveGLDAO.reloadGL(log, no, "PV");
        
    }
    
    public static void deleteItem(LoginProfile log, String novoucher, String refer) throws SQLException, Exception {
        
        String deleteQuery_2 = "delete from cb_payvaucher_debit where voucer= ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, novoucher);
        ps_2.executeUpdate();
        ps_2.close();
        
        updateAmount(log, refer);
        LiveGLDAO.reloadGL(log, refer, "PV");
        
    }
    
    public static String saveMain(LoginProfile log, PaymentVoucher item) throws Exception {
        
        String newrefer = "";
        
        if(item.getRefer() == null || item.getRefer().equals("null") || item.getRefer().equals("")){
            newrefer = AutoGenerate.getReferenceNox(log, "refer", "cb_payvoucher", "PVN", item.getEstatecode(), item.getYear(), item.getPeriod());
        }else{
            newrefer = item.getRefer();
        }
        
        try {
            //ps.setString(1, AutoGenerate.get2digitNo("info_bank", "code"));
            String q = ("insert into cb_payvoucher(refer,tarikh,paidtype,paidcode,paidname,paidaddress,paidcity,paidstate,paidpostcode,gstid,remarks,racoacode,racoadesc,amount,rm,paymentmode,bankcode,bankname,cekno,year,period,estatecode,estatename,voucherno,preid,prename,preposition,preparedate,flagbank) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, newrefer);
            //ps.setString(1, item.getRefer());
            ps.setString(2, item.getTarikh());
            ps.setString(3, item.getPaidtype());
            ps.setString(4, item.getPaidcode());
            ps.setString(5, item.getPaidname());
            ps.setString(6, item.getPaidaddress());
            ps.setString(7, item.getPaidcity());
            ps.setString(8, item.getPaidstate());
            ps.setString(9, item.getPaidpostcode());
            ps.setString(10, item.getGstid());
            ps.setString(11, item.getRemarks());
            ps.setString(12, item.getRacoacode());
            ps.setString(13, item.getRacoadesc());
            ps.setDouble(14, item.getAmount());
            ps.setString(15, item.getRm());
            ps.setString(16, item.getPaymentmode());
            ps.setString(17, item.getBankcode());
            ps.setString(18, item.getBankname());
            ps.setString(19, item.getCekno());
            ps.setString(20, item.getYear());
            ps.setString(21, item.getPeriod());
            ps.setString(22, item.getEstatecode());
            ps.setString(23, item.getEstatename());
            ps.setString(24, AutoGenerate.getVoucherNo(log, "cb_payvoucher", "voucherno"));
            ps.setString(25, item.getPreid());
            ps.setString(26, item.getPrename());
            ps.setString(27, item.getPreposition());
            ps.setString(28, item.getPreparedate());
            ps.setString(29, item.getFlagbank());
            
            Logger.getLogger(PaymentVoucherDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            
            ps.executeUpdate();
            
            
            
            ps.close();
            
            if (item.getPaymentmode().equals("Cheque")) {
                updateChequeBook(log, item, newrefer);
            }
            
            LiveGLDAO.reloadGL(log, newrefer, "PV");
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return newrefer;
        
    }
    
    public static void updateChequeBook(LoginProfile log, PaymentVoucher cb, String refer) throws Exception {
        
        String query = "UPDATE cb_cekbook_cek SET payno = ?, paycode = ?, payname = ?,paydate = ?, payamount = ?, remarks = ?, flag = ? where nocek = '" + cb.getCekno() + "'";
        
        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.setString(1, refer);
        ps.setString(2, cb.getPaidcode());
        ps.setString(3, cb.getPaidname());
        ps.setString(4, cb.getTarikh());
        ps.setDouble(5, cb.getAmount());
        ps.setString(6, cb.getRemarks());
        ps.setString(7, "Yes");
        ps.executeUpdate();
        ps.close();
        
    }
    
    public static void saveItem(LoginProfile log, PaymentVoucherItem item) throws Exception {
        
        try {
            String q = ("insert into cb_payvaucher_debit(coacode,coadescp,loclevel,loccode,locname,remarks,amount,refer,voucer,satype,sacode,sadesc,taxcode,taxdescp,taxrate,taxamt) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCoacode());
            ps.setString(2, item.getCoadescp());
            ps.setString(3, item.getLoclevel());
            ps.setString(4, item.getLoccode());
            ps.setString(5, item.getLocname());
            ps.setString(6, item.getRemarks());
            ps.setDouble(7, item.getAmount());
            ps.setString(8, item.getRefer());
            
            ps.setString(9, AutoGenerate.getReferAddBack(log, "cb_payvaucher_debit", "voucer", item.getRefer(), "refer"));
            Logger.getLogger(PaymentVoucherDAO.class.getName()).log(Level.INFO, "--------Mark #1 voucer---" + item.getVoucer());
            Logger.getLogger(PaymentVoucherDAO.class.getName()).log(Level.INFO, "--------Mark #2 refer---" + item.getRefer());
            ps.setString(10, item.getSatype());
            ps.setString(11, item.getSacode());
            ps.setString(12, item.getSadesc());
            ps.setString(13, item.getTaxcode());
            ps.setString(14, item.getTaxdescp());
            ps.setDouble(15, item.getTaxrate());
            ps.setDouble(16, item.getTaxamt());
            
            ps.executeUpdate();
            Logger.getLogger(PaymentVoucherDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();
            
            LiveGLDAO.reloadGL(log, item.getRefer(), "PV");
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
    
    public static void updateMain(LoginProfile log, PaymentVoucher m, String id) throws Exception {
        
        try {
            //String q = ("insert into gl_jv_item(Jvid,JVrefno,remark,actcode,actdesc,loccode,locdesc,loclevel,satype,sacode,sadesc,debit,credit,taxcode,taxdescp,taxrate,gstid,amtbeforetax) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            String q = ("UPDATE cb_payvoucher set refer=? ,tarikh=?,paidtype=?,paidcode=?,paidname=?,paidaddress=?,paidcity=?,paidstate=?,paidpostcode=?,gstid=?,remarks=?,racoacode=?,racoadesc=?,amount=?,rm=?,paymentmode=?,bankcode=?,bankname=?,cekno=?,year=?,period=?,estatecode=?,estatename=?,voucherno=? WHERE refer = '" + id + "'");
            //amtbeforetax=?
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, m.getRefer());
            ps.setString(2, m.getTarikh());
            ps.setString(3, m.getPaidtype());
            ps.setString(4, m.getPaidcode());
            ps.setString(5, m.getPaidname());
            ps.setString(6, m.getPaidaddress());
            ps.setString(7, m.getPaidcity());
            ps.setString(8, m.getPaidstate());
            ps.setString(9, m.getPaidpostcode());
            ps.setString(10, m.getGstid());
            ps.setString(11, m.getRemarks());
            ps.setString(12, m.getRacoacode());
            ps.setString(13, m.getRacoadesc());
            ps.setDouble(14, m.getAmount());
            ps.setString(15, m.getRm());
            Logger.getLogger(PaymentVoucherDAO.class.getName()).log(Level.INFO, "--------Mark #0 before generator--- refer---" + m.getRm());
            ps.setString(16, m.getPaymentmode());
            ps.setString(17, m.getBankcode());
            ps.setString(18, m.getBankname());
            ps.setString(19, m.getCekno());
            ps.setString(20, m.getYear());
            ps.setString(21, m.getPeriod());
            ps.setString(22, m.getEstatecode());
            ps.setString(23, m.getEstatename());
            ps.setString(24, m.getVoucherno());
            Logger.getLogger(PaymentVoucherDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();

            //Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();
            
            LiveGLDAO.reloadGL(log, m.getRefer(), "PV");
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
    
    public static void updateItem(LoginProfile log, PaymentVoucherItem item, String id) throws Exception {
        
        try {
            String q = ("UPDATE cb_payvaucher_debit set coacode=?,coadescp=?,loclevel=?,loccode=?,locname=?,remarks=?,amount=?,satype=?,sacode=?,sadesc=?,taxcode=?,taxdescp=?,taxrate=?,taxamt=? WHERE voucer = '" + id + "'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCoacode());
            ps.setString(2, item.getCoadescp());
            ps.setString(3, item.getLoclevel());
            ps.setString(4, item.getLoccode());
            ps.setString(5, item.getLocname());
            ps.setString(6, item.getRemarks());
            ps.setDouble(7, item.getAmount());
            ps.setString(8, item.getRefer());
            ps.setString(9, item.getVoucer());
            Logger.getLogger(PaymentVoucherDAO.class.getName()).log(Level.INFO, "--------Mark #1 voucer---" + item.getVoucer());
            
            Logger.getLogger(PaymentVoucherDAO.class.getName()).log(Level.INFO, "--------Mark #2 refer---" + item.getRefer());
            ps.setString(8, item.getSatype());
            ps.setString(9, item.getSacode());
            ps.setString(10, item.getSadesc());
            ps.setString(11, item.getTaxcode());
            ps.setString(12, item.getTaxdescp());
            ps.setDouble(13, item.getTaxrate());
            ps.setDouble(14, item.getTaxamt());
            
            Logger.getLogger(PaymentVoucherDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            
            ps.close();
            
            LiveGLDAO.reloadGL(log, item.getRefer(), "PV");
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
    
    public static String getCVno(LoginProfile log, String abb) throws Exception {
        
        ResultSet rs = null;
        String CVno = "";
        String year = AccountingPeriod.getCurYear(log);
        String curperiod = AccountingPeriod.getCurPeriod(log);
        //String estatecode = LoginProfile.getEstateCode();
        String estatecode = "9911";
        
        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat(?,'" + estatecode + "',substring('" + year + "',3,2),lpad('" + curperiod + "',2,'0'),lpad((max(substring(refer,14,4))+1),4,'0')),concat('JPY','" + estatecode + "',substring('" + year + "',3,2),lpad('" + curperiod + "',2,'0'),'0001')) as refer from cb_cashvoucher where refer like concat(?,'%') and year='" + year + "' and period='" + curperiod + "' and estatecode like concat('" + estatecode + "','%')");
        stmt.setString(1, abb);
        stmt.setString(2, abb);
        
        rs = stmt.executeQuery();
        if (rs.next()) {
            CVno = rs.getString(1);
            
        }
        
        rs.close();
        stmt.close();
        return CVno;
        
    }
    
    public static String getCVoucherNo(LoginProfile log) throws Exception {
        
        ResultSet rs = null;
        String CVno = "";
        
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT ifnull(concat(lpad((max(voucherid)+1),5,'0')),concat('0001')) as new FROM `cb_cashvoucher`");
        
        rs = stmt.executeQuery();
        if (rs.next()) {
            CVno = rs.getString(1);
        }
        
        rs.close();
        stmt.close();
        return CVno;
        
    }
    
    public static String getCVid(LoginProfile log, String refno) throws Exception, SQLException {
        
        ResultSet rs = null;
        String JVno = "";
        
        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat('" + refno + "',lpad((max(right(novoucher,4))+1),4,'0')),concat('" + refno + "','0001')) as njvno from cb_cashvoucher_account where refer='" + refno + "'");
        
        rs = stmt.executeQuery();
        if (rs.next()) {
            JVno = rs.getString(1);
        }
        
        rs.close();
        stmt.close();
        return JVno;
        
    }
    
    public static Double getSum(LoginProfile log, String column, String id) throws Exception {
        
        Double x = 0.00;
        Statement stment = log.getCon().createStatement();
        ResultSet setent = stment.executeQuery(" select sum(" + column + ") as sumamount from cb_cashvoucher_account where refer = '" + id + "'");
        
        while (setent.next()) {
            x += setent.getDouble("sumamount");
        }
        
        stment.close();
        setent.close();
        return x;
    }
    
    public static PaymentVoucherItem getItem(String id) throws Exception {
        
        PaymentVoucherItem v = new PaymentVoucherItem();
        
        v.setVoucer(id);
        //v.setAmtbeforetax(getBalance("debit",id));

        return v;
    }
    
    private static void updateAmount(LoginProfile log, String JVrefno) throws Exception {
        
        String query = "UPDATE gl_jv SET todebit = '" + getSum(log, "debit", JVrefno) + "',tocredit = '" + getSum(log, "credit", JVrefno) + "' where JVrefno = '" + JVrefno + "'";
        
        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }
    
    public static List<PaymentVoucherItem> getAllPYVItem(LoginProfile log, String refno) throws Exception {
        
        Statement stmt = null;
        List<PaymentVoucherItem> CVi;
        CVi = new ArrayList();
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_payvaucher_debit where refer = '" + refno + "' order by voucer");
            
            while (rs.next()) {
                CVi.add(getPYVitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return CVi;
    }
    
    public static List<PaymentVoucherItem> getAllPYVItemByPages(LoginProfile log, String refno, int row, int limit) throws Exception {
        
        Statement stmt = null;
        List<PaymentVoucherItem> CVi;
        CVi = new ArrayList();
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_payvaucher_debit where refer = '" + refno + "'  order by voucer limit " + row + "," + limit);
            
            while (rs.next()) {
                CVi.add(getPYVitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return CVi;
    }
    
    public static List<PaymentVoucherItem> getAllPVTax(LoginProfile log, String refno) throws Exception {
        
        Statement stmt = null;
        List<PaymentVoucherItem> CVi;
        CVi = new ArrayList();
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs;
            rs = stmt.executeQuery("select * from cb_payvaucher_debit where refer = '" + refno + "' and taxamt<>0 order by voucer");
            
            while (rs.next()) {
                CVi.add(getPYVitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return CVi;
    }
    
    public static PaymentVoucherItem getPYVitem(LoginProfile log, String id) throws SQLException, Exception {
        PaymentVoucherItem c = null;
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_payvaucher_debit where voucer=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getPYVitem(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return c;
    }
    
    public static double getRoundingAmount(LoginProfile log, String id) throws SQLException, Exception {
        double total = 0;
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select sum(amount+taxamt) as amt from cb_payvaucher_debit where refer=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                total = rs.getDouble("amt");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return total;
    }
    
    private static PaymentVoucherItem getPYVitem(ResultSet rs) throws SQLException {
        PaymentVoucherItem cv = new PaymentVoucherItem();
        
        cv.setCoacode(rs.getString("coacode"));
        cv.setCoadescp(rs.getString("coadescp"));
        cv.setLoclevel(rs.getString("loclevel"));
        cv.setLoccode(rs.getString("loccode"));
        cv.setLocname(rs.getString("locname"));
        cv.setChargetype(rs.getString("chargetype"));
        cv.setChargecode(rs.getString("chargedescp"));
        cv.setRemarks(rs.getString("remarks"));
        cv.setAmount(Double.parseDouble(rs.getString("amount")));
        cv.setVoucer(rs.getString("voucer"));
        cv.setRefer(rs.getString("refer"));
        cv.setSatype(rs.getString("satype"));
        cv.setSacode(rs.getString("sacode"));
        cv.setSadesc(rs.getString("sadesc"));
        cv.setTaxcode(rs.getString("taxcode"));
        cv.setTaxdescp(rs.getString("taxdescp"));
        cv.setTaxcoacode(rs.getString("taxcoacode"));
        cv.setTaxcoadescp(rs.getString("taxcoadescp"));
        cv.setTaxrate(rs.getDouble("taxrate"));
        cv.setTaxamt(rs.getDouble("taxamt"));
        cv.setAmtbeforetax(rs.getDouble("amtbeforetax"));
        
        return cv;
    }
    
    public static List<PaymentVoucher> getAllPYV(LoginProfile log, String refno) throws Exception {
        
        Statement stmt = null;
        List<PaymentVoucher> CV;
        CV = new ArrayList();
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_payvoucher where refer = '" + refno + "' order by refer");
            
            while (rs.next()) {
                CV.add(getPYV(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return CV;
    }
    
    public static PaymentVoucher getPYV(LoginProfile log, String refno) throws SQLException, Exception {
        PaymentVoucher c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_payvoucher where refer=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getPYV(rs);
        }
        
        return c;
    }
    
    private static PaymentVoucher getPYV(ResultSet rs) throws SQLException {
        
        PaymentVoucher cv = new PaymentVoucher();
        
        cv.setEstatecode(rs.getString("estatecode"));
        cv.setEstatename(rs.getString("estatename"));
        cv.setEstateaddress(rs.getString("estateaddress"));
        cv.setEstatecity(rs.getString("estatecity"));
        cv.setEstatestate(rs.getString("estatestate"));
        cv.setEstatepostcode(rs.getString("estatepostcode"));
        cv.setEstatephone(rs.getString("estatephone"));
        cv.setEstatefax(rs.getString("estatefax"));
        cv.setYear(rs.getString("year"));
        cv.setPeriod(rs.getString("period"));
        cv.setBankcode(rs.getString("bankcode"));
        cv.setBankname(rs.getString("bankname"));
        cv.setVoucherno(rs.getString("voucherno"));
        cv.setTarikh(rs.getString("tarikh"));
        cv.setPaidtype(rs.getString("paidtype"));
        cv.setPaidcode(rs.getString("paidcode"));
        cv.setPaidname(rs.getString("paidname"));
        cv.setPaidaddress(rs.getString("paidaddress"));
        cv.setPaidcity(rs.getString("paidcity"));
        cv.setPaidstate(rs.getString("paidstate"));
        cv.setPaidpostcode(rs.getString("paidpostcode"));
        cv.setPost(rs.getString("post"));
        cv.setTarikhpost(rs.getString("tarikhpost"));
        cv.setRemarks(rs.getString("remarks"));
        cv.setAmount(rs.getDouble("amount"));
        cv.setPaymentmode(rs.getString("paymentmode"));
        cv.setCekno(rs.getString("cekno"));
        cv.setAppid(rs.getString("appid"));
        cv.setAppname(rs.getString("appname"));
        cv.setAppposition(rs.getString("appposition"));
        cv.setPreid(rs.getString("preid"));
        cv.setPrename(rs.getString("prename"));
        cv.setPreposition(rs.getString("preposition"));
        cv.setFlag(rs.getString("flag"));
        cv.setRefer(rs.getString("refer"));
        cv.setFlagbank(rs.getString("flagbank"));
        cv.setRm(rs.getString("rm"));
        cv.setCheckid(rs.getString("checkid"));
        cv.setCheckname(rs.getString("checkname"));
        cv.setCheckposition(rs.getString("checkposition"));
        cv.setStmtyear(rs.getString("stmtyear"));
        cv.setStmtperiod(rs.getString("stmtperiod"));
        cv.setNoagree(rs.getString("noagree"));
        cv.setPaymentflag(rs.getString("paymentflag"));
        cv.setAdvancetype(rs.getString("advancetype"));
        cv.setPreparedate(rs.getString("preparedate"));
        cv.setCheckdate(rs.getString("checkdate"));
        cv.setApprovedate(rs.getString("approvedate"));
        cv.setAdvance(rs.getString("advance"));
        cv.setPostgst(rs.getString("postgst"));
        cv.setGstid(rs.getString("gstid"));
        cv.setGstdate(rs.getString("gstdate"));
        cv.setRacoacode(rs.getString("racoacode"));
        cv.setRacoadesc(rs.getString("racoadesc"));
        
        return cv;
    }
    
    public static double getDebit(LoginProfile log, String refno) throws SQLException, Exception {
        double amount = 0;
        ResultSet rs = null;
        ResultSet rs1 = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select sum(amount+taxamt) as amt from cb_payvaucher_debit where refer=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amount = rs.getDouble("amt");
        }
        
        return amount;
    }
    
    public static double getCredit(LoginProfile log, String refno) throws SQLException, Exception {
        double amt = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_payvoucher  where refer = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amt = rs.getDouble("amount");
        }
        
        return amt;
    }
    
    public static int getCheckCounter(LoginProfile log) throws Exception {
        
        int cnt = 0;
        ResultSet rs = null;
        
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from sl_inv where year = ? and period = ? and cid = ''");
        
        stmt.setString(1, AccountingPeriod.getCurYear(log));
        stmt.setString(2, AccountingPeriod.getCurPeriod(log));
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }
        
        return cnt;
    }
    
    public static int getApproveCounter(LoginProfile log) throws Exception {
        
        int cnt = 0;
        ResultSet rs = null;
        
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from sl_inv where year = ? and period = ? and cid <> ? and pid = ?");
        
        stmt.setString(1, AccountingPeriod.getCurYear(log));
        stmt.setString(2, AccountingPeriod.getCurPeriod(log));
        stmt.setString(3, "");
        stmt.setString(4, "");
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }
        
        return cnt;
    }
    
    public static void checkVoucher(LoginProfile log, String refer, String staff_id, String staff_name) throws Exception {
        
        String query = "UPDATE cb_payvoucher SET checkid = '" + staff_id + "',checkname = '" + staff_name + "',checkdate='" + AccountingPeriod.getCurrentTimeStamp() + "' where refer = '" + refer + "'";
        
        try {
            PreparedStatement ps = log.getCon().prepareStatement(query);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
    
    public static void approveVoucher(LoginProfile log, String refer, String staff_id, String staff_name, String position) throws Exception {
        
        String query = "UPDATE cb_payvoucher SET appid = '" + staff_id + "',appname = '" + staff_name + "',approvedate='" + AccountingPeriod.getCurrentTimeStamp() + "', appposition = '" + position + "' where refer = '" + refer + "'";
        
        try {
            PreparedStatement ps = log.getCon().prepareStatement(query);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
    
    public static boolean isCheck(LoginProfile log, String refer) throws Exception {
        
        boolean cek = false;
        String ck = "";
        ResultSet rs = null;
        
        PreparedStatement stmt = log.getCon().prepareStatement("select checkid from cb_payvoucher where refer = ?");
        
        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString("checkid");
        }
        
        if (!ck.equals("")) {
            cek = true;
        }
        
        stmt.close();
        rs.close();
        
        return cek;
    }
    
    public static boolean isApprove(LoginProfile log, String refer) throws Exception {
        
        boolean cek = false;
        String ck = "";
        ResultSet rs = null;
        
        PreparedStatement stmt = log.getCon().prepareStatement("select appid from cb_payvoucher where refer = ?");
        
        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString("appid");
        }
        
        if (!ck.equals("")) {
            cek = true;
        }
        
        stmt.close();
        rs.close();
        
        return cek;
    }
    
    public static String getStatus(LoginProfile log, String refer) throws Exception {
        
        boolean cek = false;
        String check = "";
        String app = "";
        String post = "";
        String status = "<span class=\"label label-primary\">Preparing</span>";
        ResultSet rs = null;
        
        PreparedStatement stmt = log.getCon().prepareStatement("select checkid,appid,post from cb_payvoucher where refer = ?");
        
        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            check = rs.getString("checkid");
            app = rs.getString("appid");
            post = rs.getString("post");
        }
        
        stmt.close();
        rs.close();
        
        if (!check.equals("")) {
            status = "Checked";
        }
        
        if (!app.equals("")) {
            status = "Approved";
        }

//        if(post.equals("posted")){
//            status = "<span class=\"label label-info\">Post</span>";
//        }
        if (post.equals("Cancel")) {
            status = "Posted";
        }
        
        return status;
    }
    
    public static Double getBalance(LoginProfile log, String column, String id) throws Exception {
        
        Double x = 0.00;
        Double y = 0.00;
        Statement stment = log.getCon().createStatement();
        ResultSet setent = stment.executeQuery(" select sum(" + column + ") as sumamount from cb_payvaucher_debit where refer = '" + id + "'");
        
        while (setent.next()) {
            x += setent.getDouble("sumamount");
        }
        
        setent = stment.executeQuery(" select amount from cb_payvoucher where refer = '" + id + "'");
        
        if (setent.next()) {
            y = setent.getDouble("amount");
        }
        
        stment.close();
        setent.close();
        return y - x;
    }
    
    public static int saveIntoTempMaster(LoginProfile log, PaymentVoucher item) throws Exception {
        
        int i = 0;
        //String newrefer = AutoGenerate.getReferenceNo("refer", "cb_payvoucher", "PVN", item.getEstatecode());

        String deleteQuery_2 = "delete from cb_payvoucher_temp_master where sessionid = ?";
        try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
            ps_2.setString(1, item.getSessionID());
            ps_2.executeUpdate();
        }
        
        try {
            //ps.setString(1, AutoGenerate.get2digitNo("info_bank", "code"));
            String q = ("insert into cb_payvoucher_temp_master(refer,tarikh,paidtype,paidcode,paidname,paidaddress,paidcity,paidstate,paidpostcode,gstid,remarks,racoacode,racoadesc,amount,rm,paymentmode,bankcode,bankname,cekno,year,period,estatecode,estatename,voucherno,sessionid,flagbank) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, "PVN");
            //ps.setString(1, item.getRefer());
            ps.setString(2, item.getTarikh());
            ps.setString(3, item.getPaidtype());
            ps.setString(4, item.getPaidcode());
            ps.setString(5, item.getPaidname());
            ps.setString(6, item.getPaidaddress());
            ps.setString(7, item.getPaidcity());
            ps.setString(8, item.getPaidstate());
            ps.setString(9, item.getPaidpostcode());
            ps.setString(10, item.getGstid());
            ps.setString(11, item.getRemarks());
            ps.setString(12, item.getRacoacode());
            ps.setString(13, item.getRacoadesc());
            ps.setDouble(14, item.getAmount());
            ps.setString(15, item.getRm());
            ps.setString(16, item.getPaymentmode());
            ps.setString(17, item.getBankcode());
            ps.setString(18, item.getBankname());
            ps.setString(19, item.getCekno());
            ps.setString(20, item.getYear());
            ps.setString(21, item.getPeriod());
            ps.setString(22, item.getEstatecode());
            ps.setString(23, item.getEstatename());
            ps.setString(24, "");
            ps.setString(25, item.getSessionID());
            ps.setString(26, item.getFlagbank());
            
            ps.executeUpdate();
            
            ps.close();
            
            i = 1;
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return i;
        
    }
    
    public static int saveDebitAccIntoTemp(LoginProfile log, List<PaymentVoucherTempDebitAccount> listT) throws Exception {
        
        int i = 0;
        try {
            
            String deleteQuery_2 = "delete from cb_payvoucher_debit_temp where sessionid = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, listT.get(0).getSessionid());
                ps_2.executeUpdate();
            }
            
            for (PaymentVoucherTempDebitAccount m : listT) {
                
                String q = ("INSERT INTO cb_payvoucher_debit_temp(sessionid,referno,amounttopay,partycode,frommodule) values (?,?,?,?,?)");
                
                try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                    
                    ps.setString(1, m.getSessionid());
                    ps.setString(2, "-");
                    ps.setDouble(3, m.getAmounttopay());
                    ps.setString(4, m.getPartyCode());
                    ps.setString(5, m.getFrommodule());
                    
                    Logger.getLogger(VendorPaymentDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
                    ps.executeUpdate();
                    
                    i = 1;
                }
                
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
            i = 0;
        }
        
        return i;
        
    }
    
    public static ApPaymentMaster getAllInfo(LoginProfile log, String sessionID, String companycode, String companyname) throws Exception {
        
        ApPaymentMaster mt = new ApPaymentMaster();
        PaymentVoucher pv = new PaymentVoucher();
        List<PaymentVoucherItem> l = new ArrayList();
        
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_payvoucher_temp_master where sessionid=?");
            stmt.setString(1, sessionID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                pv = getPYV(rs);
            }
            
            mt.setMaster(pv);
            
            List<PaymentVoucherTempDebitAccount> listx = (List<PaymentVoucherTempDebitAccount>) getDebitAccount(log, sessionID);
            for (PaymentVoucherTempDebitAccount j : listx) {
                
                PaymentVoucherItem pi = new PaymentVoucherItem();
                
                PaymentVoucherType pvt = (PaymentVoucherType) PaymentVoucherDAO.getTypeDetail(log, j.getFrommodule(), j.getPartyCode());
                
                pi.setAmount(j.getAmounttopay());
                pi.setCoacode(pvt.getCoacode());
                pi.setCoadescp(pvt.getCoadesc());
                pi.setLoccode(companycode);
                pi.setLoclevel("Company");
                pi.setLocname(companyname);
                pi.setRemarks(pvt.getRemark());
                pi.setSacode(pvt.getSacode());
                pi.setSadesc(pvt.getSadesc());
                pi.setSatype(pvt.getSatype());
                pi.setTaxamt(0.00);
                pi.setTaxcoacode("00");
                pi.setTaxcoadescp("None");
                pi.setTaxcode("00");
                pi.setTaxdescp("None");
                pi.setTaxrate(0.00);
                
                l.add(pi);
                
            }
            
            mt.setListInvoice(l);
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return mt;
    }
    
    public static List<PaymentVoucherTempDebitAccount> getDebitAccount(LoginProfile log, String id) throws Exception {
        
        List<PaymentVoucherTempDebitAccount> Com;
        Com = new ArrayList();
        
        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_payvoucher_debit_temp where sessionid=? order by referno asc");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            while (rs.next()) {
                
                PaymentVoucherTempDebitAccount vp = new PaymentVoucherTempDebitAccount();

                //vp.setInvrefno(rs.getString("invrefno"));
                vp.setAmounttopay(rs.getDouble("amounttopay"));
                vp.setFrommodule(rs.getString("frommodule"));
                vp.setPartyCode(rs.getString("partycode"));
                
                Com.add(vp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return Com;
    }
    
    public static String generateVoucher(LoginProfile log, String sessionID, String companycode, String companyname) throws Exception {
        
        ApPaymentMaster am = (ApPaymentMaster) PaymentVoucherDAO.getAllInfo(log, sessionID, companycode, companyname);
        
        PaymentVoucher pv = new PaymentVoucher();
        PaymentVoucher p = am.getMaster();
        
        String newrefer = AutoGenerate.getReferenceNox(log, "refer", "cb_payvoucher", "PVN", p.getEstatecode(), p.getYear(), p.getPeriod());
        //String newrefer = AutoGenerate.getReferenceNo(log, "refer", "cb_payvoucher", "PVN", p.getEstatecode());
        p.setRefer(newrefer);
        p.setVoucherno(AutoGenerate.getVoucherNo(log, "cb_payvoucher", "voucherno"));
        
        PaymentVoucherDAO.saveMain(log, p);
        
        List<PaymentVoucherItem> listx = (List<PaymentVoucherItem>) am.getListInvoice();
        for (PaymentVoucherItem j : listx) {
            
            String addBack = AutoGenerate.getReferAddBack(log, "cb_payvaucher_debit", "voucer", newrefer, "refer");
            j.setVoucer(addBack);
            j.setRefer(newrefer);
            PaymentVoucherDAO.saveItem(log, j);
            
        }
        
        return newrefer;
    }
    
    private static PaymentVoucherType getTypeDetail(LoginProfile log, String type, String code) throws Exception {
        
        PaymentVoucherType pvt = new PaymentVoucherType();
        
        if (type.equals("020207")) {
            
            CeManageBank sp = (CeManageBank) BankDAO.getInfo(log, code);
            
            pvt.setCoacode(EstateDAO.getEstateInfo(log, "2411", "estatecode").getHqsuspence());
            pvt.setCoadesc(EstateDAO.getEstateInfo(log, "2411", "estatecode").getHqsuspencedescp());
            pvt.setCode(code);
            pvt.setName(sp.getName());
            pvt.setSacode("00");
            pvt.setSadesc("Not Applicable");
            pvt.setSatype("None");
            pvt.setType("Bank");
            pvt.setRemark("Bank Charges");
        }
        
        return pvt;
        
    }
    
    public static PaymentVoucherMaster getPVMaster(LoginProfile log, String refno) throws Exception {
        
        PaymentVoucherMaster pm = new PaymentVoucherMaster();
        
        pm.setPvMain(getPYV(log, refno));
        pm.setPvList(getAllPYVItem(log, refno));
        
        return pm;
        
    }
    
    public static String replicateData(LoginProfile log, String refno, String replicateDate, String replicateYear, String replicatePeriod) throws Exception {
        String refer = "";
        
        PaymentVoucherMaster pm = (PaymentVoucherMaster) getPVMaster(log, refno);
        pm.getPvMain().setPeriod(replicatePeriod);
        pm.getPvMain().setYear(replicateYear);
        pm.getPvMain().setTarikh(replicateDate);
        pm.getPvMain().setRefer("");
        //pm.getPvMain().setPaymentmode("None");
        pm.getPvMain().setCekno("");
        //pm.getPvMain().setBankcode("");
        //pm.getPvMain().setBankname("");
        pm.getPvMain().setPreid(log.getUserID());
        pm.getPvMain().setPrename(log.getFullname());
        pm.getPvMain().setPreparedate(AccountingPeriod.getCurrentTimeStamp());
        pm.getPvMain().setPreposition(log.getPosition());
        
        refer = PaymentVoucherDAO.saveMain(log, pm.getPvMain());
        
        List<PaymentVoucherItem> listx = (List<PaymentVoucherItem>) pm.getPvList();
        for (PaymentVoucherItem j : listx) {
            
            j.setRefer(refer);
            PaymentVoucherDAO.saveItem(log, j);
            
        }
        
        return refer;
    }
    
    public static boolean isComplete(LoginProfile log, String refno) throws Exception{
        
        boolean s = true;
        PaymentVoucherMaster pm = (PaymentVoucherMaster) getPVMaster(log, refno);
        
        PaymentVoucher p = (PaymentVoucher) pm.getPvMain();
        
        if(p.getBankcode().equals("") || p.getBankcode() == null){
            s = false;
        }else if(p.getAmount() <= 0.0){
            s = false;
        }else if(p.getPaymentmode().equals("None")){
            s = false;
        }else if(p.getPaidtype().equals("") || p.getPaidtype() == null){
            s = false;
        }else if(p.getPaidcode().equals("") || p.getPaidcode() == null){
            s = false;
        }else if(p.getPaymentmode().equals("Cheque") && (p.getCekno().equals("") || p.getCekno() == null)){
            s = false;
        }
        
        return s;
    }
    
    public static boolean hasDCNote(LoginProfile log, String refno) throws Exception{
        
        boolean s = false;
        
        PaymentVoucherMaster pm = (PaymentVoucherMaster) getPVMaster(log, refno);
        
        List<PaymentVoucherItem> listx = (List<PaymentVoucherItem>) pm.getPvList();
        for (PaymentVoucherItem j : listx) {
            
            s = DebitCreditNoteDAO.checkSuspence(log, j.getCoacode());
            
        }
        
        return s;
    }
    
    public static List<PaymentVoucher> getAll(LoginProfile log, DataTable prm) throws Exception, SQLException {

        Statement stmt = null;
        List<PaymentVoucher> CV;
        CV = new ArrayList();

        String q = "";
        if (prm.getViewBy().equals("today")) {
            q = " WHERE tarikh = '" + AccountingPeriod.getCurrentTimeStamp() + "'";
        } else if (prm.getViewBy().equals("thismonth")) {
            q = " WHERE period = '" + AccountingPeriod.getCurPeriod(log) + "' and year = '" + AccountingPeriod.getCurYear(log) + "'";
        } else if (prm.getViewBy().equals("byperiod")) {
            q = " WHERE period = '" + prm.getPeriod() + "' and year = '" + prm.getYear() + "'";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_payvoucher " + q + " order by refer");

            while (rs.next()) {
                CV.add(getPYV(rs));
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }
}
