/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.ar;

import com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.LiveGLDAO;
import com.lcsb.fms.dao.setup.company.MillDAO;
import com.lcsb.fms.dao.setup.configuration.BuyerDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.general.DataTable;
import com.lcsb.fms.general.DateAndTime;
import com.lcsb.fms.general.EnglishNumberToWords;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.ar.ArCPORefinery;
import com.lcsb.fms.model.financial.ar.ArContractAgree;
import com.lcsb.fms.model.financial.ar.ArContractInfo;
import com.lcsb.fms.model.financial.ar.ArFFB;
import com.lcsb.fms.model.financial.ar.ArPrepareInvoice;
import com.lcsb.fms.model.financial.ar.ArTempMaster;
import com.lcsb.fms.model.financial.ar.ArTempRefinery;
import com.lcsb.fms.model.financial.ar.SalesInvoice;
import com.lcsb.fms.model.financial.ar.SalesInvoiceItem;
import com.lcsb.fms.model.financial.ar.SalesInvoiceMaster;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.dao.DataDAO;
import com.lcsb.fms.util.dao.EstateDAO;
import com.lcsb.fms.util.dao.TaxDAO;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import com.lcsb.fms.util.model.Tax;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class SalesInvoiceDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("020903");
        mod.setModuleDesc("Sales Invoice");
        mod.setMainTable("sl_inv");
        mod.setReferID_Master("invref");
        return mod;

    }

    public static List<ListTable> tableList() {

        String column_names[] = {"invref", "invdate", "bname", "prodname", "amountno", "cid", "aid", "postflag"};
        String title_name[] = {"Reference No.", "Date", "Buyer Name", "Commodity", "Amount", "Check ID", " Approve ID", "Post"};
        boolean boolview[] = {true, true, true, true, true, false, false, false};
        //int colWidth[] = {};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            i.setBoolView(boolview[j]);
            lt.add(i);
        }

        return lt;
    }

    public static void deleteExist(LoginProfile log, String no) throws SQLException, Exception {
        Connection con = log.getCon();

        DataDAO.deleteData(log, no, "sl_inv", "invref");
        DataDAO.deleteData(log, no, "sl_inv_item", "ivref");

        LiveGLDAO.deleteFromGL(log, no);

    }

    public static void deleteItem(LoginProfile log, String novoucher, String refer) throws SQLException, Exception {

        SalesInvoiceDAO.updateAmount(log, refer, SalesInvoiceDAO.getINV(log, refer).getAmountno() - (SalesInvoiceDAO.getINVitem(log, novoucher).getAmount() + SalesInvoiceDAO.getINVitem(log, novoucher).getTaxamt()));
        DataDAO.deleteData(log, novoucher, "sl_inv_item", "refer");
        LiveGLDAO.reloadGL(log, refer, "SNV");

    }

    public static String saveMain(LoginProfile log, SalesInvoice item) throws Exception {

        String newrefer = AutoGenerate.getReferenceNox(log, "invref", "sl_inv", "INV", item.getEstcode(), String.valueOf(item.getYear()), String.valueOf(item.getPeriod()));

        try {
            String q = ("insert into sl_inv(invno,invref,invdate,loclevel,loccode,locdesc,btype,bcode,bname,baddress,bpostcode,bstate,amountno,amountstr,remarks,year,period,estcode,estname,pid,pname,pdate,coacode,coadesc,dono,gstid,satype,sacode,sadesc,contractno,prodcode,prodname,oldinvno,bcity,orderno) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, AutoGenerate.getVoucherNo(log, "sl_inv", "invno"));
            ps.setString(2, newrefer);
            ps.setString(3, item.getInvdate());
            ps.setString(4, item.getLoclevel());
            ps.setString(5, item.getLoccode());
            ps.setString(6, item.getLocdesc());
            ps.setString(7, item.getBtype());
            ps.setString(8, item.getBcode());
            ps.setString(9, item.getBname());
            ps.setString(10, item.getBaddress());
            ps.setString(11, item.getBpostcode());
            ps.setString(12, item.getBstate());
            ps.setDouble(13, item.getAmountno());
            ps.setString(14, item.getAmountstr());
            ps.setString(15, item.getRemarks());
            ps.setInt(16, item.getYear());
            ps.setInt(17, item.getPeriod());
            ps.setString(18, item.getEstcode());
            ps.setString(19, item.getEstname());
            ps.setString(20, item.getPid());
            ps.setString(21, item.getPname());
            ps.setString(22, item.getPdate());
            ps.setString(23, item.getCoacode());
            ps.setString(24, item.getCoadesc());
            ps.setString(25, item.getDono());
            ps.setString(26, item.getGstid());
            ps.setString(27, item.getSatype());
            ps.setString(28, item.getSacode());
            ps.setString(29, item.getSadesc());
            ps.setString(30, item.getContractno());
            ps.setString(31, item.getProdcode());
            ps.setString(32, item.getProdname());
            ps.setString(33, item.getOldinvno());
            ps.setString(34, item.getBcity());
            ps.setString(35, item.getOrderno());
            ps.executeUpdate();

            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newrefer;

    }

    public static void saveItem(LoginProfile log, SalesInvoiceItem item) throws Exception {

        try {
            String q = ("insert into sl_inv_item(ivref,prodcode,prodname,coacode,coaname,qty,unitp,amount,unitm,remarks,loclevel,loccode,locdesc,taxcode,taxrate,taxamt,taxcoacode,taxcoadescp,ivno,satype,sacode,sadesc,refer,dispatchno) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getIvref());
            ps.setString(2, item.getProdcode());
            ps.setString(3, item.getProdname());
            ps.setString(4, item.getCoacode());
            ps.setString(5, item.getCoaname());
            ps.setDouble(6, item.getQty());
            ps.setString(7, item.getUnitp());
            ps.setDouble(8, item.getAmount());
            ps.setString(9, item.getUnitm());
            ps.setString(10, item.getRemarks());
            ps.setString(11, item.getLoclevel());
            ps.setString(12, item.getLoccode());
            ps.setString(13, item.getLocdesc());
            ps.setString(14, item.getTaxcode());
            ps.setDouble(15, item.getTaxrate());
            ps.setDouble(16, item.getTaxamt());
            ps.setString(17, item.getTaxcoacode());
            ps.setString(18, item.getTaxcoadescp());
            ps.setString(19, AutoGenerate.getVoucherNo(log, "sl_inv_item", "ivno"));
            ps.setString(20, item.getSatype());
            ps.setString(21, item.getSacode());
            ps.setString(22, item.getSadesc());
            ps.setString(23, AutoGenerate.getReferAddBack(log, "sl_inv_item", "refer", item.getIvref(), "ivref"));
            ps.setString(24, item.getDispatchno());

            ps.executeUpdate();
            Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();
            
            SalesInvoiceDAO.updateAmount(log, item.getIvref(), getAllINVItemAmount(log, item.getIvref()));

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateMain(LoginProfile log, SalesInvoice m, String id) throws Exception {

        try {
            //String q = ("insert into gl_jv_item(Jvid,JVrefno,remark,actcode,actdesc,loccode,locdesc,loclevel,satype,sacode,sadesc,debit,credit,taxcode,taxdescp,taxrate,gstid,amtbeforetax) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            String q = ("UPDATE sl_inv set invdate = ?,loclevel = ?,loccode = ?,locdesc = ?,btype = ?,bcode = ?,bname = ?,baddress = ?,bpostcode = ?,bstate = ?,amountno = ?,amountstr = ?,remarks = ?,year = ?,period = ?,estcode = ?,estname = ?,coacode = ?,coadesc = ?,dono = ?,gstid = ?,satype = ?,sacode = ?,sadesc = ?, racoacode = ?, racoadesc = ?, orderno = ? WHERE invref = '" + id + "'");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, m.getInvdate());
            ps.setString(2, m.getLoclevel());
            ps.setString(3, m.getLoccode());
            ps.setString(4, m.getLocdesc());
            ps.setString(5, m.getBtype());
            ps.setString(6, m.getBcode());
            ps.setString(7, m.getBname());
            ps.setString(8, m.getBaddress());
            ps.setString(9, m.getBpostcode());
            ps.setString(10, m.getBstate());
            ps.setDouble(11, m.getAmountno());
            ps.setString(12, m.getAmountstr());
            ps.setString(13, m.getRemarks());
            ps.setInt(14, m.getYear());
            ps.setInt(15, m.getPeriod());
            ps.setString(16, m.getEstcode());
            ps.setString(17, m.getEstname());
            ps.setString(18, m.getCoacode());
            ps.setString(19, m.getCoadesc());
            ps.setString(20, m.getDono());
            ps.setString(21, m.getGstid());
            ps.setString(22, m.getSatype());
            ps.setString(23, m.getSacode());
            ps.setString(24, m.getSadesc());
            ps.setString(25, m.getRacoacode());
            ps.setString(26, m.getRacoadesc());
            ps.setString(27, m.getOrderno());
            Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();

            //Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

            LiveGLDAO.reloadGL(log, id, "SNV");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateItem(LoginProfile log, SalesInvoiceItem item, String id) throws Exception {

        try {
            String q = ("UPDATE sl_inv_item set prodcode = ?,prodname = ?,coacode = ?,coaname = ?,qty = ?,unitp = ?,amount = ?,unitm = ?,remarks = ?,loclevel = ?,loccode = ?,locdesc = ?,taxcode = ?,taxrate = ?,taxamt = ?,taxcoacode = ?,taxcoadescp = ?,satype = ?,sacode = ?,sadesc = ?, dispatchno = ? WHERE refer = '" + id + "'");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getProdcode());
            ps.setString(2, item.getProdname());
            ps.setString(3, item.getCoacode());
            ps.setString(4, item.getCoaname());
            ps.setDouble(5, item.getQty());
            ps.setString(6, item.getUnitp());
            ps.setDouble(7, item.getAmount());
            ps.setString(8, item.getUnitm());
            ps.setString(9, item.getRemarks());
            ps.setString(10, item.getLoclevel());
            ps.setString(11, item.getLoccode());
            ps.setString(12, item.getLocdesc());
            ps.setString(13, item.getTaxcode());
            ps.setDouble(14, item.getTaxrate());
            ps.setDouble(15, item.getTaxamt());
            ps.setString(16, item.getTaxcoacode());
            ps.setString(17, item.getTaxcoadescp());
            ps.setString(18, item.getSatype());
            ps.setString(19, item.getSacode());
            ps.setString(20, item.getSadesc());
            ps.setString(21, item.getDispatchno());

            Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();

            ps.close();

            LiveGLDAO.reloadGL(log, id, "SNV");
            SalesInvoiceDAO.updateAmount(log, id.substring(0, 15), getAllINVItemAmount(log, id.substring(0, 15)));

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static double getAllINVItemAmount(LoginProfile log, String refno) {

        Statement stmt = null;
        double amt = 0.0;

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from sl_inv_item where ivref = '" + refno + "'");

            while (rs.next()) {
                amt += rs.getDouble("amount") + rs.getDouble("taxamt");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return amt;

    }

    public static String getCVno(LoginProfile log, String abb) throws Exception {

        ResultSet rs = null;
        String CVno = "";
        String year = AccountingPeriod.getCurYear(log);
        String curperiod = AccountingPeriod.getCurPeriod(log);
        //String estatecode = LoginProfile.getEstateCode();
        String estatecode = "9911";

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat(?,'" + estatecode + "',substring('" + year + "',3,2),lpad('" + curperiod + "',2,'0'),lpad((max(substring(refer,14,4))+1),4,'0')),concat('JPY','" + estatecode + "',substring('" + year + "',3,2),lpad('" + curperiod + "',2,'0'),'0001')) as refer from cb_cashvoucher where refer like concat(?,'%') and year='" + year + "' and period='" + curperiod + "' and estatecode like concat('" + estatecode + "','%')");
        stmt.setString(1, abb);
        stmt.setString(2, abb);

        rs = stmt.executeQuery();
        if (rs.next()) {
            CVno = rs.getString(1);

        }

        rs.close();
        stmt.close();
        return CVno;

    }

    public static String getCVoucherNo(LoginProfile log) throws Exception {

        ResultSet rs = null;
        String CVno = "";

        PreparedStatement stmt = log.getCon().prepareStatement("SELECT ifnull(concat(lpad((max(voucherid)+1),5,'0')),concat('0001')) as new FROM `cb_cashvoucher`");

        rs = stmt.executeQuery();
        if (rs.next()) {
            CVno = rs.getString(1);
        }

        rs.close();
        stmt.close();
        return CVno;

    }

    public static String getCVid(LoginProfile log, String refno) throws Exception, SQLException {

        ResultSet rs = null;
        String JVno = "";

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat('" + refno + "',lpad((max(right(novoucher,4))+1),4,'0')),concat('" + refno + "','0001')) as njvno from cb_cashvoucher_account where refer='" + refno + "'");

        rs = stmt.executeQuery();
        if (rs.next()) {
            JVno = rs.getString(1);
        }

        rs.close();
        stmt.close();
        return JVno;

    }

    public static Double getSum(LoginProfile log, String column, String id) throws Exception {

        Double x = 0.00;
        Statement stment = log.getCon().createStatement();
        ResultSet setent = stment.executeQuery(" select sum(" + column + ") as sumamount from cb_cashvoucher_account where refer = '" + id + "'");

        while (setent.next()) {
            x += setent.getDouble("sumamount");
        }

        stment.close();
        setent.close();
        return x;
    }

    public static Double getBalance(LoginProfile log, String column, String id) throws Exception {

        Double x = 0.00;
        Double y = 0.00;
        Statement stment = log.getCon().createStatement();
        ResultSet setent = stment.executeQuery(" select sum(" + column + ") as sumamount from cb_cashvoucher_account where refer = '" + id + "'");

        while (setent.next()) {
            x += setent.getDouble("sumamount");
        }

        setent = stment.executeQuery(" select total from cb_cashvoucher where refer = '" + id + "'");

        if (setent.next()) {
            y = setent.getDouble("total");
        }

        stment.close();
        setent.close();
        return y - x;
    }

    public static SalesInvoiceItem getItem(String id) throws Exception {

        SalesInvoiceItem v = new SalesInvoiceItem();

        v.setRefer(id);
        //v.setAmtbeforetax(getBalance("debit",id));

        return v;
    }

    private static void updateAmount(LoginProfile log, String refer, double amount) throws Exception {

        String query = "UPDATE sl_inv SET amountno = '" + amount + "', amountstr = '"+ EnglishNumberToWords.toConvert(amount) +"' where invref = '" + refer + "'";

        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }

    private static void updateInvrefFBB(LoginProfile log, String refer, String sessionid) throws Exception {

        String query = "UPDATE sl_inv_ffb SET invref = '" + refer + "' where sessionid = '" + sessionid + "'";

        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }

    public static void updatePrice(LoginProfile log, double price, String contractno) throws Exception {

        String query = "UPDATE sl_contract_aggree SET price = '" + price + "' where no = '" + contractno + "'";

        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }

    public static int setNewPrice(LoginProfile log, double price, String refno) throws Exception {

        int i = 0;
        double total = 0.0;

        List<SalesInvoiceItem> listAll = (List<SalesInvoiceItem>) SalesInvoiceDAO.getAllINVItem(log, refno);

        for (SalesInvoiceItem j : listAll) {

            total += SalesInvoiceDAO.calculateWithNewPrice(log, j, price);

        }

        calculateGrandTotalWithNewPrice(log, refno, total);
            
        return i = 1;
    }

    public static double calculateWithNewPrice(LoginProfile log, SalesInvoiceItem si, double price) throws Exception {
        
        double total = 0.0;
        
        String q = ("UPDATE sl_inv_item SET unitp = ?, amount = ?, taxamt = ? WHERE refer = '" + si.getRefer() + "'");
        try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
            ps.setDouble(1, price);
            ps.setDouble(2, si.getQty() * price);
            ps.setDouble(3, si.getTaxrate() * (si.getQty() * price) / 100);
            ps.executeUpdate();
            Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();
            
            total = (si.getQty() * price) + (si.getTaxrate() * (si.getQty() * price) / 100);
        }
        
        return total;

    }
    
    public static void calculateGrandTotalWithNewPrice(LoginProfile log, String refer, double total) throws Exception {

        String q = ("UPDATE sl_inv SET  amountno = ?, amountstr = ? WHERE invref = '" + refer + "'");
        try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
            ps.setDouble(1, total);
            ps.setString(2, EnglishNumberToWords.toConvert(total));
            ps.executeUpdate();
            Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();
        }

    }

    public static List<SalesInvoiceItem> getAllINVItem(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<SalesInvoiceItem> CVi;
        CVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from sl_inv_item where ivref = '" + refno + "' order by dispatchno asc");

            while (rs.next()) {
                CVi.add(getINVitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static SalesInvoiceItem getINVitem(LoginProfile log, String id) throws SQLException, Exception {
        SalesInvoiceItem c = null;
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from sl_inv_item where refer=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getINVitem(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    private static SalesInvoiceItem getINVitem(ResultSet rs) throws SQLException {
        SalesInvoiceItem cv = new SalesInvoiceItem();

        cv.setAmount(rs.getDouble("amount"));
        cv.setCoacode(rs.getString("coacode"));
        cv.setCoaname(rs.getString("coaname"));
        cv.setIvno(rs.getString("ivno"));
        cv.setIvref(rs.getString("ivref"));
        cv.setLoccode(rs.getString("loccode"));
        cv.setLoclevel(rs.getString("loclevel"));
        cv.setLocdesc(rs.getString("locdesc"));
        cv.setProdcode(rs.getString("prodcode"));
        cv.setProdname(rs.getString("prodname"));
        cv.setQty(rs.getDouble("qty"));
        cv.setUnitm(rs.getString("unitm"));
        cv.setUnitp(rs.getString("unitp"));
        cv.setRefer(rs.getString("refer"));
        cv.setRemarks(rs.getString("remarks"));
        cv.setSacode(rs.getString("sacode"));
        cv.setSadesc(rs.getString("sadesc"));
        cv.setSatype(rs.getString("satype"));
        cv.setTaxamt(rs.getDouble("taxamt"));
        cv.setTaxcoacode(rs.getString("taxcoacode"));
        cv.setTaxcoadescp(rs.getString("taxcoadescp"));
        cv.setTaxcode(rs.getString("taxcode"));
        cv.setTaxrate(rs.getDouble("taxrate"));
        cv.setDispatchno(rs.getString("dispatchno"));

        return cv;
    }

    public static List<SalesInvoice> getAllSI(LoginProfile log, String code) throws Exception, SQLException {

        Statement stmt = null;
        List<SalesInvoice> CV;
        CV = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from sl_inv where prodcode = '" + code + "' order by invref");

            while (rs.next()) {
                CV.add(getINV(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static List<SalesInvoice> getAll(LoginProfile log, DataTable prm) throws Exception, SQLException {

        Statement stmt = null;
        List<SalesInvoice> CV;
        CV = new ArrayList();

        String q = "";
        if (prm.getViewBy().equals("today")) {
            q = " WHERE invdate = '" + AccountingPeriod.getCurrentTimeStamp() + "'";
        } else if (prm.getViewBy().equals("thismonth")) {
            q = " WHERE period = '" + AccountingPeriod.getCurPeriod(log) + "' and year = '" + AccountingPeriod.getCurYear(log) + "'";
        } else if (prm.getViewBy().equals("byperiod")) {
            q = " WHERE period = '" + prm.getPeriod() + "' and year = '" + prm.getYear() + "'";
        } else if (prm.getViewBy().equals("approval")) {
            q = " WHERE aid = '' AND cid <> ''";
        } else if (prm.getViewBy().equals("check")) {
            q = " WHERE aid = '' AND cid = ''";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from sl_inv " + q + " order by invref");

            while (rs.next()) {
                CV.add(getINV(rs));
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static SalesInvoice getINV(LoginProfile log, String refno) throws SQLException, Exception {
        SalesInvoice c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from sl_inv where invref=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getINV(rs);
        }

        return c;
    }

    public static SalesInvoice getINV(ResultSet rs) throws SQLException {
        SalesInvoice cv = new SalesInvoice();
        cv.setAdate(rs.getString("adate"));
        cv.setAdesignation(rs.getString("adesignation"));
        cv.setAid(rs.getString("aid"));
        cv.setAmountno(rs.getDouble("amountno"));
        cv.setAmountstr(rs.getString("amountstr"));
        cv.setAname(rs.getString("aname"));
        cv.setBaddebt(rs.getString("baddebt"));
        cv.setBaddress(rs.getString("baddress"));
        cv.setBcode(rs.getString("bcode"));
        cv.setBname(rs.getString("bname"));
        cv.setBpostcode(rs.getString("bpostcode"));
        cv.setBstate(rs.getString("bstate"));
        cv.setBtype(rs.getString("btype"));
        cv.setCdate(rs.getString("cdate"));
        cv.setCid(rs.getString("cid"));
        cv.setCname(rs.getString("cname"));
        cv.setCoacode(rs.getString("coacode"));
        cv.setCoadesc(rs.getString("coadesc"));
        cv.setDono(rs.getString("dono"));
        cv.setEstcode(rs.getString("estcode"));
        cv.setEstname(rs.getString("estname"));
        cv.setGstdate(rs.getString("gstdate"));
        cv.setInvdate(rs.getString("invdate"));
        cv.setInvno(rs.getString("invno"));
        cv.setInvref(rs.getString("invref"));
        cv.setLoccode(rs.getString("loccode"));
        cv.setLocdesc(rs.getString("locdesc"));
        cv.setLoclevel(rs.getString("loclevel"));
        cv.setPdate(rs.getString("pdate"));
        cv.setPeriod(rs.getInt("period"));
        cv.setPid(rs.getString("pid"));
        cv.setPname(rs.getString("pname"));
        cv.setPostdate(rs.getString("postdate"));
        cv.setPostflag(rs.getString("postflag"));
        cv.setPostgst(rs.getString("postgst"));
        cv.setRacoacode(rs.getString("racoacode"));
        cv.setRacoadesc(rs.getString("racoadesc"));
        cv.setReceiveamt(rs.getDouble("receiveamt"));
        cv.setRemarks(rs.getString("remarks"));
        cv.setSacode(rs.getString("sacode"));
        cv.setSadesc(rs.getString("sadesc"));
        cv.setSatype(rs.getString("satype"));
        cv.setYear(rs.getInt("year"));
        cv.setGstid(rs.getString("gstid"));
        cv.setProdcode(rs.getString("prodcode"));
        cv.setProdname(rs.getString("prodname"));
        cv.setBcity(rs.getString("bcity"));
        cv.setContractno(rs.getString("contractno"));
        cv.setOrderno(rs.getString("orderno"));

        return cv;
    }

    public static double getDebit(LoginProfile log, String refno) throws SQLException, Exception {
        double amount = 0;
        String btype = "";
        try {
            ResultSet rs = null;
            ResultSet rs1 = null;
            PreparedStatement stmt = log.getCon().prepareStatement("select btype,amountno from sl_inv where invref=?");
            stmt.setString(1, refno);
            rs = stmt.executeQuery();
            if (rs.next()) {
                amount = rs.getDouble("amountno");
                btype = rs.getString("btype");
            }

            if (btype.equals("Stock And Store")) {

                PreparedStatement stmt1 = log.getCon().prepareStatement("select sum(amount) as amt,sum(taxamt) as tax from sl_inv_item where ivref=?");
                stmt1.setString(1, refno);
                rs1 = stmt.executeQuery();
                if (rs.next()) {
                    amount = rs.getDouble("tax");
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            amount = 0.0;
        }

        return amount;
    }

    public static double getCredit(LoginProfile log, String refno) throws SQLException, Exception {
        BigDecimal amt = new BigDecimal(0);
        BigDecimal amttax = new BigDecimal(0);
        BigDecimal totalCredit = new BigDecimal(0);

        double d = 0.0;
        try {

            ResultSet rs = null;

            PreparedStatement stmt = log.getCon().prepareStatement("select sum(amount) as amt,sum(taxamt) as amttax from sl_inv_item where ivref=?");
            stmt.setString(1, refno);
            rs = stmt.executeQuery();
            if (rs != null) {
                if (rs.next()) {
                    amt = rs.getBigDecimal("amt");
                    amttax = rs.getBigDecimal("amttax");
                    totalCredit = amt.add(amttax);
                    d = totalCredit.doubleValue();
                }
            }
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
            d = 0.0;
        }

        //totalCredit = amt + amttax;
        return d;
    }

    public static int getCheckCounter(LoginProfile log) throws Exception {

        int cnt = 0;
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from sl_inv where year = ? and period = ? and cid = ''");

        stmt.setString(1, AccountingPeriod.getCurYear(log));
        stmt.setString(2, AccountingPeriod.getCurPeriod(log));
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }

        return cnt;
    }

    public static int getApproveCounter(LoginProfile log) throws Exception {

        int cnt = 0;
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from sl_inv where year = ? and period = ? and cid <> ? and pid = ?");

        stmt.setString(1, AccountingPeriod.getCurYear(log));
        stmt.setString(2, AccountingPeriod.getCurPeriod(log));
        stmt.setString(3, "");
        stmt.setString(4, "");
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }

        return cnt;
    }

    public static void checkVoucher(LoginProfile log, String refer, String staff_id, String staff_name) throws Exception {

        String query = "UPDATE sl_inv SET cid = '" + staff_id + "',cname = '" + staff_name + "',cdate='" + AccountingPeriod.getCurrentTimeStamp() + "' where invref = '" + refer + "'";

        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }

    public static void approveVoucher(LoginProfile log, String refer, String staff_id, String staff_name, String position) throws Exception {

        String query = "UPDATE sl_inv SET aid = '" + staff_id + "',aname = '" + staff_name + "',adate='" + AccountingPeriod.getCurrentTimeStamp() + "', adesignation = '" + position + "' where invref = '" + refer + "'";

        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }

    public static String getStatus(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String check = "";
        String app = "";
        String post = "";
        String status = "<span class=\"label label-primary\">Preparing</span>";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select cid,aid,postflag from sl_inv where invref = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            check = rs.getString(1);
            app = rs.getString(2);
            post = rs.getString(3);
        }

        stmt.close();
        rs.close();

        if (!check.equals("")) {
            status = "Checked";
        }

        if (!app.equals("")) {
            status = "Approved";
        }

//        if(post.equals("posted")){
//            status = "<span class=\"label label-info\">Post</span>";
//        }
        if (post.equals("Cancel")) {
            status = "Posted";
        }

        return status;
    }

    public static boolean isCheck(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select cid from sl_inv where invref = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString(1);
        }

        if (!ck.equals("")) {
            cek = true;
        }

        stmt.close();
        rs.close();

        return cek;
    }

    public static boolean isApprove(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select aid from sl_inv where invref = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString(1);
        }

        if (!ck.equals("")) {
            cek = true;
        }

        stmt.close();
        rs.close();

        return cek;
    }

    public static List<ArCPORefinery> getListProd(LoginProfile log, String refno, String millCode, String prod) throws Exception {

        String agreeOrigin = refno.substring(0, 3);

//        if (agreeOrigin.equals("SAG")) {
//            con = ConnectionUtil.getConnection();
//        } else {
//            con = ConnectionUtil.createMillConnection(millCode);
//        }
        Statement stmt = null;
        List<ArCPORefinery> CVi;
        CVi = new ArrayList();

        String table = "";
        if (prod.equals("01")) {
            table = "qc_cpo_grade";
        } else if (prod.equals("02")) {
            table = "qc_kernel_grade";
        } else if (prod.equals("06")) {
            table = "qc_shell_dispatch";
        } else if (prod.equals("04")) {
            table = "qc_sludge_dispatch";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from " + table + " where contract = '" + refno + "' order by date");

            while (rs.next()) {
                CVi.add(getListCPO(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

//        if (!agreeOrigin.equals("SAG")) {
//            con.close();
//        }
        return CVi;
    }

    public static ArCPORefinery getListCPO(ResultSet rs) throws SQLException {
        ArCPORefinery cv = new ArCPORefinery();
        cv.setMt(rs.getString("mt"));
        cv.setMtRefinery(rs.getDouble("mt_refinery"));
        cv.setDate(rs.getDate("date"));
        cv.setTrailer(rs.getString("trailer"));
        cv.setDispatchNo(rs.getString("dispatch_no"));
        cv.setContract(rs.getString("contract"));
        cv.setDriver(rs.getString("driver"));

        return cv;
    }

    public static int saveIntoTempMaster(LoginProfile log, ArTempMaster m) throws Exception {

        int i = 0;
        try {

            String deleteQuery_2 = "delete from sl_inv_temp_master where contractno = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, m.getContractno());
                ps_2.executeUpdate();
            }

            String q = ("INSERT INTO sl_inv_temp_master(address,amountstring,buyercode,buyername,city,coacode,coadescp,companycode,companyname,contractno,date,gstid,period,postcode,racoacode,racoadescp,remark,state,year,totalamount,loccode,locdesc,loclevel,unitprice,pid,pname,pdate,prodcode,prodname,sessionid,oldinvno,orderno) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, m.getAddress());
                ps.setString(2, m.getAmountstring());
                ps.setString(3, m.getBuyercode());
                ps.setString(4, m.getBuyername());
                ps.setString(5, m.getCity());
                ps.setString(6, m.getCoacode());
                ps.setString(7, m.getCoadescp());
                ps.setString(8, m.getCompanycode());
                ps.setString(9, m.getCompanyname());
                ps.setString(10, m.getContractno());
                ps.setString(11, m.getDate());
                ps.setString(12, m.getGstid());
                ps.setString(13, m.getPeriod());
                ps.setString(14, m.getPostcode());
                ps.setString(15, m.getRacoacode());
                ps.setString(16, m.getRacoadescp());
                ps.setString(17, m.getRemark());
                ps.setString(18, m.getState());
                ps.setString(19, m.getYear());
                ps.setDouble(20, m.getTotalamount());
                ps.setString(21, m.getLoccode());
                ps.setString(22, m.getLocdesc());
                ps.setString(23, m.getLoclevel());
                ps.setDouble(24, m.getUnitprice());
                ps.setString(25, m.getPid());
                ps.setString(26, m.getPname());
                ps.setString(27, m.getPdate());
                ps.setString(28, m.getProdcode());
                ps.setString(29, m.getProdname());
                ps.setString(30, m.getSessionid());
                ps.setString(31, m.getOldinvno());
                ps.setString(32, m.getOrderno());
                Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
                ps.executeUpdate();
                Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, "----------saved-----------");

                i = 1;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            Logger.getLogger(ConnectionUtil.class.getName()).log(Level.SEVERE, null, e);
            i = 0;
        }

        return i;

    }

    public static int saveIntoTempRefinery(LoginProfile log, List<ArTempRefinery> listT) throws Exception {

        int i = 0;
        try {

            String deleteQuery_2 = "delete from sl_inv_temp_refinery where contractno = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, listT.get(0).getContractno());
                ps_2.executeUpdate();
            }

            for (ArTempRefinery m : listT) {

                String q = ("INSERT INTO sl_inv_temp_refinery(contractno,date,dispatchno,trailer,mt_mill,mt_refinery,sessionid) values (?,?,?,?,?,?,?)");

                try (PreparedStatement ps = log.getCon().prepareStatement(q)) {

                    ps.setString(1, m.getContractno());
                    ps.setString(2, m.getDate());
                    ps.setString(3, m.getDispatchno());
                    ps.setString(4, m.getTrailer());
                    ps.setDouble(5, m.getMtMill());
                    ps.setDouble(6, m.getMtRefinery());
                    ps.setString(7, m.getSessionid());

                    Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
                    ps.executeUpdate();

                    i = 1;
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
            i = 0;
        }

        return i;

    }

    public static int saveFromAddDispatchNo(LoginProfile log, List<ArTempRefinery> listT, String refer, double price) throws Exception {

        int i = 0;
        try {

            SalesInvoice am = (SalesInvoice) SalesInvoiceDAO.getINV(log, refer);
            Tax tx = (Tax) TaxDAO.getTax(log, "SR");
            double totalPrice = 0.0;

            for (ArTempRefinery j : listT) {

                SalesInvoiceItem si = new SalesInvoiceItem();

                si.setAmount(GeneralTerm.twoDecimalDouble(GeneralTerm.twoDecimalDouble(j.getMtRefinery()) * GeneralTerm.twoDecimalDouble(price)));
                si.setCoacode(EstateDAO.getEstateInfo(log, "2411", "estatecode").getHqsuspence());
                si.setCoaname(EstateDAO.getEstateInfo(log, "2411", "estatecode").getHqsuspencedescp());
                si.setIvref(refer);
                si.setLoccode(am.getLoccode());
                si.setLocdesc(am.getLocdesc());
                si.setLoclevel(am.getLoclevel());
                si.setProdcode(am.getProdcode());
                si.setProdname(am.getProdname());
                si.setQty(j.getMtRefinery());
                si.setRemarks(j.getDate() + " " + j.getTrailer() + " DO.NO : " + j.getDispatchno());
                si.setSacode("00");
                si.setSadesc("Not Applicable");
                si.setSatype("None");
                si.setTaxamt(GeneralTerm.twoDecimalDouble(GeneralTerm.twoDecimalDouble(tx.getTaxrate()) * (GeneralTerm.twoDecimalDouble(j.getMtRefinery()) * GeneralTerm.twoDecimalDouble(price)) / 100));
                si.setTaxcoacode(tx.getTaxcoacode());
                si.setTaxcoadescp(tx.getTaxcoadescp());
                si.setTaxcode(tx.getTaxcode());
                si.setTaxrate(tx.getTaxrate());
                si.setUnitm("Mt");
                si.setUnitp(String.valueOf(price));
                si.setDispatchno(j.getDispatchno());

                totalPrice += si.getAmount() + si.getTaxamt();

                SalesInvoiceDAO.saveItem(log, si);

            }

            updateAmount(log, refer, totalPrice + SalesInvoiceDAO.getINV(log, refer).getAmountno());

            i = 1;
        } catch (SQLException e) {
            e.printStackTrace();
            i = 0;
        }

        return i;

    }

    public static ArTempMaster getAllInfo(LoginProfile log, String sessionid) throws Exception {

        ArTempMaster am = new ArTempMaster();

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from sl_inv_temp_master where sessionid=?");
        stmt.setString(1, sessionid);
        rs = stmt.executeQuery();
        if (rs.next()) {
            am = getMasterInfo(rs);
        }

        if (am.getProdcode().equals("03")) {

            ArFFB ar = new ArFFB();

            ResultSet rs1 = null;
            PreparedStatement stmt1 = log.getCon().prepareStatement("select * from sl_inv_ffb where sessionid=?");
            stmt1.setString(1, sessionid);
            rs1 = stmt1.executeQuery();
            while (rs1.next()) {

                ar = getFFB(log, rs1);
            }

            am.setListFFB(ar);

        } else {

            List<ArTempRefinery> listar = new ArrayList();

            ResultSet rs1 = null;
            PreparedStatement stmt1 = log.getCon().prepareStatement("select * from sl_inv_temp_refinery where sessionid=? order by dispatchno asc");
            stmt1.setString(1, sessionid);
            rs1 = stmt1.executeQuery();
            while (rs1.next()) {

                ArTempRefinery ar = new ArTempRefinery();
                ar = getRefineryInfo(rs1);
                listar.add(ar);
            }

            am.setListDispatch(listar);

        }

        return am;
    }

    public static ArTempMaster getMasterInfo(ResultSet rs) throws SQLException {
        ArTempMaster am = new ArTempMaster();
        am.setAddress(rs.getString("address"));
        am.setAmountstring(rs.getString("amountstring"));
        am.setBuyercode(rs.getString("buyercode"));
        am.setBuyername(rs.getString("buyername"));
        am.setCity(rs.getString("city"));
        am.setCoacode(rs.getString("coacode"));
        am.setCoadescp(rs.getString("coadescp"));
        am.setCompanycode(rs.getString("companycode"));
        am.setCompanyname(rs.getString("companyname"));
        am.setContractno(rs.getString("contractno"));
        am.setDate(rs.getString("date"));
        am.setGstid(rs.getString("gstid"));
        am.setLoccode(rs.getString("loccode"));
        am.setLocdesc(rs.getString("locdesc"));
        am.setLoclevel(rs.getString("loclevel"));
        am.setPeriod(rs.getString("period"));
        am.setPostcode(rs.getString("postcode"));
        am.setRacoacode(rs.getString("racoacode"));
        am.setRacoadescp(rs.getString("racoadescp"));
        am.setRemark(rs.getString("remark"));
        am.setState(rs.getString("state"));
        am.setTotalamount(rs.getDouble("totalamount"));
        am.setYear(rs.getString("year"));
        am.setUnitprice(rs.getDouble("unitprice"));
        am.setPid(rs.getString("pid"));
        am.setPname(rs.getString("pname"));
        am.setPdate(rs.getString("pdate"));
        am.setProdcode(rs.getString("prodcode"));
        am.setProdname(rs.getString("prodname"));
        am.setOldinvno(rs.getString("oldinvno"));
        am.setOrderno(rs.getString("orderno"));

        return am;
    }

    public static ArTempRefinery getRefineryInfo(ResultSet rs) throws SQLException {
        ArTempRefinery am = new ArTempRefinery();
        am.setContractno(rs.getString("contractno"));
        am.setDate(rs.getString("date"));
        am.setDispatchno(rs.getString("dispatchno"));
        am.setMtMill(rs.getDouble("mt_mill"));
        am.setMtRefinery(rs.getDouble("mt_refinery"));
        am.setTrailer(rs.getString("trailer"));

        return am;
    }

    public static String generateInvoice(LoginProfile log, String sessionid) throws Exception {

        ArTempMaster am = (ArTempMaster) SalesInvoiceDAO.getAllInfo(log, sessionid);

        SalesInvoice ma = new SalesInvoice();

        ma.setAmountno(am.getTotalamount());
        ma.setAmountstr(am.getAmountstring());
        ma.setBaddress(am.getAddress());
        ma.setBcode(am.getBuyercode());
        ma.setBname(am.getBuyername());
        ma.setBcity(am.getCity());
        ma.setBpostcode(am.getPostcode());
        ma.setBstate(am.getState());
        ma.setBtype("");
        ma.setCoacode(am.getCoacode());
        ma.setCoadesc(am.getCoadescp());
        ma.setContractno(am.getContractno());
        ma.setEstcode(am.getCompanycode());
        ma.setEstname(am.getCompanyname());
        ma.setGstid(am.getGstid());
        ma.setInvdate(am.getDate());
        ma.setLoccode(am.getLoccode());
        ma.setLocdesc(am.getLocdesc());
        ma.setLoclevel(am.getLoclevel());
        ma.setPdate(am.getPdate());
        ma.setPeriod(Integer.parseInt(am.getPeriod()));
        ma.setPid(am.getPid());
        ma.setPname(am.getPname());
        ma.setRacoacode(am.getRacoacode());
        ma.setRacoadesc(am.getRacoadescp());
        ma.setRemarks(am.getRemark());
        ma.setSacode(am.getBuyercode());
        ma.setSadesc(am.getBuyername());
        ma.setSatype("Buyer");
        ma.setYear(Integer.parseInt(am.getYear()));
        ma.setProdcode(am.getProdcode());
        ma.setProdname(am.getProdname());
        ma.setOldinvno(am.getOldinvno());
        ma.setOrderno(am.getOrderno());

        String refer = SalesInvoiceDAO.saveMain(log, ma);

        Tax tx = (Tax) TaxDAO.getTax(log, "SR");

        if (am.getProdcode().equals("03")) {

            ArFFB j = (ArFFB) am.getListFFB();

            SalesInvoiceItem si = new SalesInvoiceItem();

            si.setAmount(j.getTotal());
            si.setCoacode(EstateDAO.getEstateInfo(log, "2411", "estatecode").getHqsuspence());
            si.setCoaname(EstateDAO.getEstateInfo(log, "2411", "estatecode").getHqsuspencedescp());
            si.setIvref(refer);
            si.setLoccode(am.getLoccode());
            si.setLocdesc(am.getLocdesc());
            si.setLoclevel(am.getLoclevel());
            si.setProdcode(am.getProdcode());
            si.setProdname(am.getProdname());
            si.setQty(j.getFfb());
            si.setRemarks(am.getRemark());
            si.setSacode("00");
            si.setSadesc("Not Applicable");
            si.setSatype("None");
            si.setTaxamt(j.getGst());
            si.setTaxcoacode(tx.getTaxcoacode());
            si.setTaxcoadescp(tx.getTaxcoadescp());
            si.setTaxcode(tx.getTaxcode());
            si.setTaxrate(tx.getTaxrate());
            si.setUnitm("Mt");
            si.setUnitp(GeneralTerm.currencyFormat(j.getPriceperton()));

            SalesInvoiceDAO.updateInvrefFBB(log, refer, j.getSessionid());

            SalesInvoiceDAO.saveItem(log, si);

        } else {

            List<ArTempRefinery> listItem = (List<ArTempRefinery>) am.getListDispatch();

            double totalAmount = 0.0;

            for (ArTempRefinery j : listItem) {

                //totalAmount += (GeneralTerm.PrecisionDouble(j.getMtRefinery() * am.getUnitprice()) + (GeneralTerm.twoDecimalDouble(GeneralTerm.twoDecimalDouble(tx.getTaxrate()) * (GeneralTerm.twoDecimalDouble(j.getMtRefinery()) * GeneralTerm.twoDecimalDouble(am.getUnitprice())) / 100));
                totalAmount += (j.getMtRefinery() * am.getUnitprice()) + (tx.getTaxrate() * (j.getMtRefinery() * am.getUnitprice()) / 100);
                SalesInvoiceItem si = new SalesInvoiceItem();

                Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, "---" + j.getMtRefinery() + "---" + am.getUnitprice() + "--" + j.getMtRefinery() * am.getUnitprice() + "--" + GeneralTerm.PrecisionDouble(j.getMtRefinery() * am.getUnitprice()));

                //si.setAmount(GeneralTerm.twoDecimalDouble(GeneralTerm.twoDecimalDouble(j.getMtRefinery()) * GeneralTerm.twoDecimalDouble(am.getUnitprice())));
                si.setAmount(j.getMtRefinery() * am.getUnitprice());

                si.setCoacode(EstateDAO.getEstateInfo(log, "2411", "estatecode").getHqsuspence());
                si.setCoaname(EstateDAO.getEstateInfo(log, "2411", "estatecode").getHqsuspencedescp());
                si.setIvref(refer);
                si.setLoccode(am.getLoccode());
                si.setLocdesc(am.getLocdesc());
                si.setLoclevel(am.getLoclevel());
                si.setProdcode(am.getProdcode());
                si.setProdname(am.getProdname());
                si.setQty(j.getMtRefinery());
                si.setRemarks(j.getDate() + " " + j.getTrailer() + " DO.NO : " + j.getDispatchno());
                si.setSacode("00");
                si.setSadesc("Not Applicable");
                si.setSatype("None");
                si.setTaxamt(tx.getTaxrate() * (j.getMtRefinery() * am.getUnitprice()) / 100);
                //si.setTaxamt(GeneralTerm.twoDecimalDouble(GeneralTerm.twoDecimalDouble(tx.getTaxrate()) * (GeneralTerm.twoDecimalDouble(j.getMtRefinery()) * GeneralTerm.twoDecimalDouble(am.getUnitprice())) / 100));
                si.setTaxcoacode(tx.getTaxcoacode());
                si.setTaxcoadescp(tx.getTaxcoadescp());
                si.setTaxcode(tx.getTaxcode());
                si.setTaxrate(tx.getTaxrate());
                si.setUnitm("Mt");
                si.setUnitp(String.valueOf(am.getUnitprice()));
                si.setDispatchno(j.getDispatchno());

                SalesInvoiceDAO.saveItem(log, si);

            }

            SalesInvoiceDAO.updateAmount(log, refer, totalAmount);

        }

        LiveGLDAO.reloadGL(log, refer, "SNV");

        return refer;

    }

    public static double getTotalExGST(LoginProfile log, String refno) throws SQLException, Exception {
        double amt = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select sum(amount) as amt from sl_inv_item where ivref=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amt = rs.getDouble("amt");
        }

        return amt;
    }

    public static double getTotalGST(LoginProfile log, String refno) throws SQLException, Exception {
        double amt = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select sum(taxamt) as amt from sl_inv_item where ivref=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amt = rs.getDouble("amt");
        }

        return amt;
    }

    public static double getTotalInGST(LoginProfile log, String refno) throws SQLException, Exception {
        double amt = 0;
        double amttax = 0;
        double totalCredit = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select sum(amount) as amt, sum(taxamt) as tax from sl_inv_item where ivref=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amt = rs.getDouble("amt");
            amttax = rs.getDouble("tax");
        }

        totalCredit = amt + amttax;

        return totalCredit;
    }

    public static SalesInvoice getNoPaidINVEach(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        SalesInvoice CV = new SalesInvoice();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from sl_inv where amountno > paid and invref = '" + refno + "' and postflag = 'posted'  order by invref desc");

            if (rs.next()) {

                CV = getINV(rs);
                //if(rs.getDouble("totalamount") > rs.getDouble("paid")){

                CV.setAmountno(rs.getDouble("amountno") - rs.getDouble("paid"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static List<SalesInvoice> getNoPaidINV(LoginProfile log, String code) throws Exception {

        Statement stmt = null;
        List<SalesInvoice> CV;
        CV = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from sl_inv where amountno > paid and bcode = '" + code + "' and postflag = 'posted' order by invref desc");

            while (rs.next()) {

                SalesInvoice vi = getINV(rs);
                //if(rs.getDouble("totalamount") > rs.getDouble("paid")){

                vi.setAmountno(rs.getDouble("amountno") - rs.getDouble("paid"));
                vi.setOldinvno(rs.getString("oldinvno"));
                CV.add(vi);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static List<SalesInvoiceItem> getAllTax(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<SalesInvoiceItem> CVi;
        CVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs;
            rs = stmt.executeQuery("select * from sl_inv_item where ivref = '" + refno + "' and taxamt>0 order by refer");

            while (rs.next()) {
                CVi.add(getINVitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static String getProdCode(String moduleID) {

        String prodCode = "";

        if (moduleID.equals("020904")) {
            prodCode = "01";
        } else if (moduleID.equals("020905")) {
            prodCode = "02";
        } else if (moduleID.equals("020906")) {
            prodCode = "03";
        } else if (moduleID.equals("020907")) {
            prodCode = "04";
        }

        return prodCode;
    }

    public static String getRemark(LoginProfile log, String prodCode, String contractno, Date date, String year, String month, String estatecode) throws Exception {

        String remark = "";
        String type = "";

        if (prodCode.equals("01")) {
            type = "CPO";
            remark = "BEING " + type + " SUPPLIED TO YOU FOR CONTRACT NO. : " + contractno + ", DATED : " + date + ".";
        } else if (prodCode.equals("02")) {
            type = "PALM KERNEL";
            remark = "BEING " + type + " SUPPLIED TO YOU FOR CONTRACT NO. : " + contractno + ", DATED : " + date + ".";
        } else if (prodCode.equals("03")) {
            type = "FFB";
            remark = "FFB SUPPLIED TO YOU FOR THE MONTH " + month.toUpperCase() + " " + year + " FROM " + EstateDAO.getEstateInfo(log, estatecode, "estatecode").getEstatedescp() + ".";
        } else if (prodCode.equals("04")) {
            type = "SLUDGE OIL";
            remark = "BEING " + type + " SUPPLIED TO YOU FOR CONTRACT NO. : " + contractno + ", DATED : " + date + ".";
        } else if (prodCode.equals("05")) {
            type = "SHELL";
            remark = "BEING " + type + " SUPPLIED TO YOU FOR THE MONTH OF " + month.toUpperCase() + " " + year + ".";
        }

        //remark = "BEING "+type+" SUPPLIED TO YOU FOR CONTRACT NO. : "+ contractno + ", DATED : " + date + ".";
        return remark;

    }

    public static String generateDispatchNo(LoginProfile log, String prodCode) throws Exception {

        String table = "";
        String column = "";
        String newCode = "";

        if (prodCode.equals("01")) {

        } else if (prodCode.equals("06")) {
            table = "qc_shell_dispatch";
            column = "dispatch_no";
        }

        newCode = AutoGenerate.getVoucherNo(log, table, column);

        return newCode;

    }

    public static int saveFFB(LoginProfile log, ArFFB item, String sessionid) throws Exception {

        String deleteQuery_2 = "delete from sl_inv_ffb where sessionid = ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, sessionid);
        ps_2.executeUpdate();
        ps_2.close();

        int i = 0;
        Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, "-------mmmmmmmmmmmmmmmmmmmmmmmmm----");

        try {
            String q = ("insert into sl_inv_ffb(cess,discount,externalffb,ffb,ker,millcost,mpobpricecpo,mpobpricepk,oer,transport,winfall,sessionid,year,month) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setDouble(1, item.getCess());
            ps.setDouble(2, item.getDiscount());
            ps.setDouble(3, item.getExternalffb());
            ps.setDouble(4, item.getFfb());
            ps.setDouble(5, item.getKer());
            ps.setDouble(6, item.getMillcost());
            ps.setDouble(7, item.getMpobpricecpo());
            ps.setDouble(8, item.getMpobpricepk());
            ps.setDouble(9, item.getOer());
            ps.setDouble(10, item.getTransport());
            ps.setDouble(11, item.getWinfall());
            ps.setString(12, sessionid);
            ps.setString(13, item.getYear());
            ps.setString(14, item.getMonth());

            ps.executeUpdate();
            Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();
            i = 1;

        } catch (SQLException e) {
            e.printStackTrace();
            i = 0;
        }

        return i;
    }

    public static ArFFB getFFB(LoginProfile log, String sessionid) throws SQLException, Exception {
        ArFFB c = null;
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from sl_inv_ffb where sessionid=?");
            stmt.setString(1, sessionid);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getFFB(log, rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static ArFFB getFFBPrint(LoginProfile log, String refer) throws SQLException, Exception {
        ArFFB c = null;
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from sl_inv_ffb where invref=?");
            stmt.setString(1, refer);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getFFB(log, rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    private static ArFFB getFFB(LoginProfile log, ResultSet rs) throws SQLException, Exception {
        ArFFB cv = new ArFFB();

        cv.setCess(rs.getDouble("cess"));
        cv.setDiscount(rs.getDouble("discount"));
        cv.setExternalffb(rs.getDouble("externalffb"));
        cv.setFfb(rs.getDouble("ffb"));
        cv.setKer(rs.getDouble("ker"));
        cv.setMillcost(rs.getDouble("millcost"));
        cv.setMpobpricecpo(rs.getDouble("mpobpricecpo"));
        cv.setMpobpricepk(rs.getDouble("mpobpricepk"));
        cv.setOer(rs.getDouble("oer"));
        cv.setSessionid(rs.getString("sessionid"));
        cv.setTransport(rs.getDouble("transport"));
        cv.setWinfall(rs.getDouble("winfall"));
        cv.setYear(rs.getString("year"));
        cv.setMonth(rs.getString("month"));
        cv.setInvref(rs.getString("invref"));

        cv.setNettcpo(cv.getMpobpricecpo() - cv.getTransport() - cv.getCess() - cv.getDiscount());
        cv.setCostcpo(cv.getOer() * cv.getNettcpo() / 100);
        cv.setCostpk(cv.getKer() * cv.getMpobpricepk() / 100);
        Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, "--------" + cv.getCostcpo() + "-*-" + cv.getCostpk() + "---------");
        cv.setTotalcost(GeneralTerm.twoDecimalDouble(cv.getCostcpo()) + GeneralTerm.twoDecimalDouble(cv.getCostpk()));
        cv.setPriceperton(cv.getTotalcost() - cv.getMillcost());
        cv.setTotal(cv.getFfb() * GeneralTerm.twoDecimalDouble(cv.getPriceperton()));
        cv.setGst(TaxDAO.getTax(log, "SR").getTaxrate() * cv.getTotal() / 100);
        cv.setNettamount(cv.getTotal() + cv.getGst());

        return cv;
    }

    public static ArPrepareInvoice getInvoice(String sessionid, String contractno, LoginProfile log, double amount, String prodcode, String buyercode) throws Exception {

        ArPrepareInvoice ar = new ArPrepareInvoice();
        ArFFB a = (ArFFB) SalesInvoiceDAO.getFFB(log, sessionid);

        ar.setAddress(BuyerDAO.getLocalCode(log, buyercode).getAddress());

        ar.setBuyercode(BuyerDAO.getLocalCode(log, buyercode).getCode());
        ar.setBuyername(BuyerDAO.getLocalCode(log, buyercode).getCompanyname());
        ar.setCity(BuyerDAO.getLocalCode(log, buyercode).getCity());
        ar.setCoacode(BuyerDAO.getLocalCode(log, buyercode).getCoa());
        ar.setCoadesc(BuyerDAO.getLocalCode(log, buyercode).getCoadescp());
        ar.setCompanycode(log.getEstateCode());
        ar.setCompanyname(log.getEstateDescp());

        ar.setGstid(BuyerDAO.getLocalCode(log, buyercode).getGstid());
        ar.setLoccode(log.getEstateCode());
        ar.setLocdesc(log.getEstateDescp());
        ar.setLoclevel("Company");
        ar.setPostcode(BuyerDAO.getLocalCode(log, buyercode).getPostcode());

        ar.setRm(EnglishNumberToWords.toConvert(amount));
        ar.setState(BuyerDAO.getLocalCode(log, buyercode).getState());

        if (prodcode.equals("03")) {

            ar.setContractNo("NONE");
            ar.setRemark(getRemark(log, prodcode, "", AccountingPeriod.convertStringtoDate("0000-00-00"), a.getYear(), a.getMonth(), log.getEstateCode()));
            ar.setUnitprice(SalesInvoiceDAO.getFFB(log, sessionid).getPriceperton());
            ar.setAmount(GeneralTerm.amountFormattoSave(GeneralTerm.currencyFormat(a.getNettamount())));

        } else {

            ArContractAgree v = (ArContractAgree) ArContractDAO.getEachAgreeUsingContractNo(log, contractno);
            ArContractInfo vi = (ArContractInfo) ArContractDAO.getInfo(log, v.getBuyercode());

            ar.setContractNo(contractno);
            ar.setRemark(getRemark(log, prodcode, v.getNo(), v.getDate(), "", "", ""));
            ar.setUnitprice(v.getPrice());
            //ar.setAmount(GeneralTerm.amountFormattoSave(GeneralTerm.currencyFormat(ArContractDAO.getTotalAmount(amount, v.getPrice()))));
            ar.setAmount(GeneralTerm.PrecisionDouble(ArContractDAO.getTotalAmount(amount, v.getPrice())));

        }

        return ar;

    }

    public static ArTempMaster getAllInfoFFB(LoginProfile log, String sessionid) throws Exception {

        ArTempMaster am = new ArTempMaster();

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from sl_inv_temp_master where sessionid=?");
        stmt.setString(1, sessionid);
        rs = stmt.executeQuery();
        if (rs.next()) {
            am = getMasterInfo(rs);
        }

        ArFFB ar = new ArFFB();

        ResultSet rs1 = null;
        PreparedStatement stmt1 = log.getCon().prepareStatement("select * from sl_inv_ffb where sessionid=?");
        stmt1.setString(1, sessionid);
        rs1 = stmt1.executeQuery();
        while (rs1.next()) {

            ar = getFFB(log, rs1);
        }

        am.setListFFB(ar);

        return am;
    }

    public static String syncRefinery(LoginProfile log) throws Exception {

        try {
            Connection con = log.getCon();
            Connection con_remote = ConnectionUtil.createMillConnection(MillDAO.getDefaultMill(log).getCode());

            Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, "Calculating differences... " + MillDAO.getDefaultMill(log).getCode());
            int cpo_diff = getCountRefinery(log, "cpo", con_remote) - getCountRefinery(log, "cpo", con);
            int kernel_diff = getCountRefinery(log, "kernel", con_remote) - getCountRefinery(log, "kernel", con);

            if (cpo_diff > 0) {
                saveNewRefinery(log, cpo_diff, "cpo", con_remote);
                Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, "Save CPO DO, Total : " + cpo_diff);
            }

            if (kernel_diff > 0) {
                saveNewRefinery(log, kernel_diff, "kernel", con_remote);
                Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, "Save PK DO, Total : " + kernel_diff);
            }

            updateLastSync(log);

            ArContractDAO.saveNewAgreementListAll(log, con_remote);

            con_remote.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return getLastSync(log);

    }

    public static int updateRefineryAmount(LoginProfile log, String disno, double amount, String prod) throws Exception {

        int i = 0;
        String table = "";
        if (prod.equals("01")) {
            table = "qc_cpo_grade";
        } else if (prod.equals("02")) {
            table = "qc_kernel_grade";
        } else if (prod.equals("04")) {
            table = "qc_sludge_dispatch";
        }

        String q = ("UPDATE " + table + " SET mt_refinery = ? WHERE dispatch_no = '" + disno + "'");
        try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
            ps.setDouble(1, amount);
            ps.executeUpdate();
            Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();
            i = 1;
        }

        return i;

    }

    private static void updateLastSync(LoginProfile log) throws Exception {

        String q = ("insert into ar_agree_lastsync(datesync,timesync,syncby_id,syncby_name) values (?,?,?,?)");
        PreparedStatement ps = log.getCon().prepareStatement(q);
        ps.setString(1, AccountingPeriod.getCurrentTimeStamp());
        ps.setString(2, AccountingPeriod.getCurrentTime());
        ps.setString(3, log.getUserID());
        ps.setString(4, log.getFullname());
        ps.executeUpdate();

        ps.close();

    }

    public static String getLastSync(LoginProfile log) throws SQLException, Exception {
        String lastsync = "";
        ResultSet rs = null;
        try {

            PreparedStatement stmt = log.getCon().prepareStatement("SELECT CONCAT(datesync,' ',timesync) as sync FROM ar_agree_lastsync WHERE id = (select max(id) from ar_agree_lastsync)");
            rs = stmt.executeQuery();
            if (rs.next()) {
                lastsync = rs.getString("sync");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lastsync;
    }

    private static int getCountRefinery(LoginProfile log, String type, Connection con) throws SQLException {

        String table = "";

        switch (type) {
            case "cpo":
                table = "qc_cpo_grade";
                break;
            case "kernel":
                table = "qc_kernel_grade";
                break;
            case "shell":
                table = "qc_shell_dispatch";
                break;
            case "sludge":
                table = "qc_sludge_dispatch";
                break;
            default:
                break;
        }

        int cnt = 0;

        try {

            ResultSet rs = null;
            PreparedStatement stmt = con.prepareStatement("select count(*) as cnt from " + table + " WHERE date >= '2016-01-01'");
            rs = stmt.executeQuery();
            if (rs.next()) {
                cnt = rs.getInt("cnt");
            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return cnt;

    }

    private static void saveNewRefinery(LoginProfile log, int total, String type, Connection con_remote) throws Exception {

        Connection con = log.getCon();
        ResultSet rs = null;

        if (type.equals("cpo")) {

            PreparedStatement stmt = con_remote.prepareStatement("SELECT * FROM (SELECT * FROM qc_cpo_grade ORDER BY id DESC LIMIT " + total + ") sub ORDER BY id ASC;");
            rs = stmt.executeQuery();
            while (rs.next()) {

                String q = ("insert into qc_cpo_grade(transporter,mt,code,date,lot,contract,trailer,cid,cname,cdate,aid,aname,adesign,adate,day,mt_refinery,pl3,dispatch_no,driver,dispatch_no_refinery,date_refinery,id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                PreparedStatement ps = con.prepareStatement(q);
                ps.setString(1, rs.getString("transporter"));
                ps.setString(2, rs.getString("mt"));
                ps.setString(3, rs.getString("code"));
                ps.setString(4, rs.getString("date"));
                ps.setString(5, rs.getString("lot"));
                ps.setString(6, rs.getString("contract"));
                ps.setString(7, rs.getString("trailer"));
                ps.setString(8, rs.getString("cid"));
                ps.setString(9, rs.getString("cname"));
                ps.setString(10, rs.getString("cdate"));
                ps.setString(11, rs.getString("aid"));
                ps.setString(12, rs.getString("aname"));
                ps.setString(13, rs.getString("adesign"));
                ps.setString(14, rs.getString("adate"));
                ps.setString(15, rs.getString("day"));
                ps.setString(16, rs.getString("mt_refinery"));
                ps.setString(17, rs.getString("pl3"));
                ps.setString(18, rs.getString("dispatch_no"));
                ps.setString(19, rs.getString("driver"));
                ps.setString(20, rs.getString("dispatch_no_refinery"));
                ps.setString(21, rs.getString("date_refinery"));
                ps.setString(22, rs.getString("id"));
                ps.executeUpdate();

                ps.close();
            }
            stmt.close();

        } else if (type.equals("kernel")) {

            PreparedStatement stmt = con_remote.prepareStatement("SELECT * FROM (SELECT * FROM qc_kernel_grade ORDER BY id DESC LIMIT " + total + ") sub ORDER BY id ASC;");
            rs = stmt.executeQuery();
            while (rs.next()) {

                String q = ("insert into qc_kernel_grade(mt,code,date,lot,contract,trailer,aid,adate,aname,cid,cname,adesign,cdesign,cdate,lori,mt_refinery,dispatch_no,driver,transporter,day,date_refinery,dispatch_no_refinery,id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                PreparedStatement ps = con.prepareStatement(q);
                ps.setString(1, rs.getString("mt"));
                ps.setString(2, rs.getString("code"));
                ps.setString(3, rs.getString("date"));
                ps.setString(4, rs.getString("lot"));
                ps.setString(5, rs.getString("contract"));
                ps.setString(6, rs.getString("trailer"));
                ps.setString(7, rs.getString("aid"));
                ps.setString(8, rs.getString("adate"));
                ps.setString(9, rs.getString("aname"));
                ps.setString(10, rs.getString("cid"));
                ps.setString(11, rs.getString("cname"));
                ps.setString(12, rs.getString("adesign"));
                ps.setString(13, rs.getString("cdesign"));
                ps.setString(14, rs.getString("cdate"));
                ps.setString(15, rs.getString("lori"));
                ps.setString(16, rs.getString("mt_refinery"));
                ps.setString(17, rs.getString("dispatch_no"));
                ps.setString(18, rs.getString("driver"));
                ps.setString(19, rs.getString("transporter"));
                ps.setString(20, rs.getString("day"));
                ps.setString(21, rs.getString("date_refinery"));
                ps.setString(22, rs.getString("dispatch_no_refinery"));
                ps.setString(23, rs.getString("id"));
                ps.executeUpdate();

                ps.close();
            }
            stmt.close();
        }

        rs.close();

    }

    private static boolean checkExistDispatch(LoginProfile log, String code) throws Exception {

        boolean exist = false;
        String checkcode = "";
        int x = 0;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from sl_contract_aggree where no=?");
        stmt.setString(1, code);
        rs = stmt.executeQuery();
        if (rs.next()) {
            checkcode = rs.getString("no");
            x++;
        }

        if (x > 0) {
            exist = true;
        }

        return exist;
    }

    public static List<SalesInvoiceItem> getAllINVItemByPage(LoginProfile log, String refno, int row, int limit) throws Exception {

        Statement stmt = null;
        List<SalesInvoiceItem> CVi;
        CVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from sl_inv_item where ivref = '" + refno + "' order by dispatchno asc limit " + row + "," + limit);

            while (rs.next()) {
                CVi.add(getINVitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static SalesInvoiceMaster getSIMaster(LoginProfile log, String refno) throws Exception {

        SalesInvoiceMaster pm = new SalesInvoiceMaster();

        pm.setSiMain(SalesInvoiceDAO.getINV(log, refno));
        pm.setSiList(SalesInvoiceDAO.getAllINVItem(log, refno));

        return pm;

    }

    public static boolean isComplete(LoginProfile log, String refno) throws Exception {

        boolean s = true;
        SalesInvoiceMaster pm = (SalesInvoiceMaster) getSIMaster(log, refno);

        SalesInvoice p = (SalesInvoice) pm.getSiMain();

        if (p.getAmountno() <= 0.0) {
            s = false;
        } else if (p.getBcode().equals("") || p.getBcode() == null) {
            s = false;
        }

        return s;
    }

    public static boolean hasDCNote(LoginProfile log, String refno) throws Exception {

        boolean s = false;

        SalesInvoiceMaster pm = (SalesInvoiceMaster) getSIMaster(log, refno);

        List<SalesInvoiceItem> listx = (List<SalesInvoiceItem>) pm.getSiList();
        for (SalesInvoiceItem j : listx) {

            s = DebitCreditNoteDAO.checkSuspence(log, j.getCoacode());

        }

        return s;
    }

    public static void cancelThis(LoginProfile log, String refer) throws SQLException, Exception {

        updateCancel(log, refer);
        LiveGLDAO.deleteFromGL(log, refer);

    }

    private static void updateCancel(LoginProfile log, String refer) throws SQLException, Exception {

        String q = ("UPDATE sl_inv SET postflag = ? WHERE invref = '" + refer + "'");
        PreparedStatement ps = log.getCon().prepareStatement(q);
        ps.setString(1, "cancel");

        ps.executeUpdate();
        ps.close();

    }

}
