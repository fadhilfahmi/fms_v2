/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.dao.financial.cashbook;

import com.lcsb.fms.dao.financial.AccDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.model.financial.cashbook.BankBook;
import com.lcsb.fms.model.financial.cashbook.BankBookMaster;
import com.lcsb.fms.model.financial.cashbook.BankBookParam;
import com.lcsb.fms.model.financial.cashbook.PettyCash;
import com.lcsb.fms.model.setup.company.Bank;
import com.lcsb.fms.model.setup.configuration.ChartofAccount;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.dao.DataDAO;
import com.lcsb.fms.util.model.Data;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class BankBookDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("020206");
        mod.setModuleDesc("Bank Book");
        mod.setMainTable("cb_payvoucher");
        mod.setReferID_Master("refer");
        return mod;

    }

    public static void runBankBook(LoginProfile log, BankBookParam b) throws Exception {

        deleteOldBankBook(log);
        BankBook.setCurrentPeriod(String.valueOf(b.getPeriod()));
        BankBook.setCurrentYear(String.valueOf(b.getYear()));
        BankBook.setStartPeriod(AccountingPeriod.getStartofPeriod(log, b.getYear(), b.getPeriod()));
        BankBook.setEndPeriod(AccountingPeriod.getEndofPeriod(log, b.getYear(), b.getPeriod()));

        runOR(log, b.getBankcode());
        runPV(log, b.getBankcode());

    }

    private static void  deleteOldBankBook(LoginProfile log) throws Exception {
        DataDAO.deleteAllData(log, "cb_tempbankbalance");
    }

    private static ChartofAccount getBankCOA(LoginProfile log, String code) throws Exception {
        
        ChartofAccount coa = new ChartofAccount();
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from ce_manage_bank where code  = '" + code + "'");
        rs = stmt.executeQuery();
        if (rs.next()) {
            coa.setCode(rs.getString("coacode"));
            coa.setDescp(rs.getString("coadesc"));
        }

        return coa;
    }

    private static void getPersonInfo(LoginProfile log) throws Exception {
        String output = "";
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from ce_manage_list where managecode = left('',2) and cek='yes' and active='yes'");

        rs = stmt.executeQuery();
        if (rs.next()) {
            BankBook.setStaffid(rs.getString("staffid"));
            BankBook.setName(rs.getString("name"));
            BankBook.setPposition(rs.getString("pposition"));
        }

    }

    private static void runOR(LoginProfile log, String bcode) throws Exception {

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_official where period=" + BankBook.getCurrentPeriod() + " and year=" + BankBook.getCurrentYear() + " and bankcode ='" + bcode + "' and post<>'cancel' order by date");
        //stmt.setString(1, referenceno);

        Logger.getLogger(BankBookDAO.class.getName()).log(Level.INFO, String.valueOf(stmt));
        rs = stmt.executeQuery();
        while (rs.next()) {
            //output = rs.getString(1);
            String refer = rs.getString("refer");
            String tarikh = rs.getString("date");
            String pvremarks = rs.getString("paidname");
            String pvamount = rs.getString("amount");
            String bankcode = rs.getString("bankcode");
            String listPvRemark = "";
            String listPvCoa = "";

            String listcek = "";
            String listcoa = "";
            String listsa = "";

            Data dt = new Data();

            ResultSet rs1 = null;
            PreparedStatement stmt1 = log.getCon().prepareStatement("select * from cb_official_cek where voucherno = ?");
            stmt1.setString(1, refer);

            rs1 = stmt1.executeQuery();
            while (rs1.next()) {

                String pvcekno = rs1.getString("cekno");
                if (!pvcekno.equalsIgnoreCase("")) {
                    listcek += pvcekno + ",";
                }

                //insertData(PettyCash.getCurrentYear(),PettyCash.getCurrentPeriod(), tarikh, refer, listPvRemark, listPvCoa, pvAmount, "R");
            }

            ResultSet rs2 = null;
            PreparedStatement stmt2 = log.getCon().prepareStatement("select * from cb_official_credit where voucherno = ?");
            stmt2.setString(1, refer);

            rs2 = stmt2.executeQuery();
            while (rs2.next()) {

                String pvcoa = rs2.getString("coacode");
                String listsa1 = rs2.getString("sacode");
                if (!pvcoa.equalsIgnoreCase("")) {
                    listcoa += pvcoa + ",";
                }
                if (!listsa1.equalsIgnoreCase("")) {
                    listsa += listsa1 + ",";
                }

                

            }
            
            insertData(log, BankBook.getCurrentYear(), BankBook.getCurrentPeriod(), bcode, tarikh, refer, pvremarks, listcoa, pvamount, listcek, "R", listsa);
        }
    }

    private static void runPV(LoginProfile log, String bcode) throws Exception {
        try {

            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_payvoucher where period=" + BankBook.getCurrentPeriod() + " and year=" + BankBook.getCurrentYear()+ " and bankcode ='" + bcode + "' and post<>'cancel' order by tarikh");
            //stmt.setString(1, referenceno);

            Logger.getLogger(BankBookDAO.class.getName()).log(Level.INFO, String.valueOf(stmt));
            rs = stmt.executeQuery();
            while (rs.next()) {
                //output = rs.getString(1);

                String refer = rs.getString("refer");
                String tarikh = rs.getString("tarikh");
                String pvremarks = rs.getString("paidname");
                String pvamount = rs.getString("amount");
                String bankcode = rs.getString("bankcode");
                String listPvRemark = "";
                String listPvCoa = "";

                String listcek = "";
                String listcoa = "";
                String listsa = "";

                Data dt = new Data();

                String coa = "";
                String sacoa = "";

                ResultSet rs1 = null;
                PreparedStatement stmt1 = log.getCon().prepareStatement("select * from cb_payvaucher_debit where voucer = ?");
                stmt1.setString(1, refer);

                rs1 = stmt1.executeQuery();
                while (rs1.next()) {

                    String coa1 = rs1.getString("coacode");
                    String sacoa1 = rs1.getString("sacode");
                    if (!coa1.equalsIgnoreCase("")) {

                        coa += coa1 + ",";
                        //data.addElement(coa);
                    }
                    if (!sacoa1.equalsIgnoreCase("")) {

                        sacoa += sacoa1 + ",";
                        //data.addElement(sacoa);
                    }

                    //insertData(PettyCash.getCurrentYear(),PettyCash.getCurrentPeriod(), tarikh, refer, listPvRemark, listPvCoa, pvAmount, "R");
                }

                insertData(log, BankBook.getCurrentYear(), BankBook.getCurrentPeriod(), bcode, tarikh, refer, pvremarks, coa, pvamount, listcek, "E", sacoa);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void insertData(LoginProfile log, String y, String p, String code, String date, String ID, String remarks, String listCoa, String pvAmount, String listcek, String status, String listsa) throws Exception {

        PreparedStatement ps = log.getCon().prepareStatement("insert into cb_tempbankbalance(year,period,bank,pvdate,pvid,pvremarks,listcoa,pvamount,listcek,status,sacode) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
        ps.setString(1, y);
        ps.setString(2, p);
        ps.setString(3, code);
        ps.setString(4, date);
        ps.setString(5, ID);
        ps.setString(6, remarks);
        ps.setString(7, listCoa);
        ps.setString(8, pvAmount);
        ps.setString(9, listcek);
        ps.setString(10, status);
        ps.setString(11, listsa);

        ps.executeUpdate();
        //Logger.getLogger(BankBookDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
        ps.close();

    }

    public static List<BankBook> getAllBankBook(LoginProfile log, BankBookParam prm) throws Exception {

        Statement stmt = null;
        List<BankBook> Pc;
        Pc = new ArrayList();
        Logger.getLogger(PettyCashDAO.class.getName()).log(Level.INFO, "select * from cb_tempbankbalance where period ='" + prm.getPeriod() + "' and year='" + prm.getYear() + "' order by pvdate, status desc,pvid");

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_tempbankbalance where period ='" + prm.getPeriod() + "' and year='" + prm.getYear() + "' order by pvdate, status desc,pvid");

            while (rs.next()) {
                Pc.add(getBankBook(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Pc;
    }

    public static BankBook getBankBook(LoginProfile log, String code) throws SQLException, Exception {
        BankBook c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_cashvoucher where refer=?");
        stmt.setString(1, code);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getBankBook(rs);
        }

        return c;
    }

    private static BankBook getBankBook(ResultSet rs) throws SQLException {
        BankBook bb = new BankBook();
        bb.setPvdate(rs.getString("pvdate"));
        bb.setPvid(rs.getString("pvid"));
        bb.setPvremarks(rs.getString("pvremarks"));
        bb.setListcoa(rs.getString("listcoa"));
        bb.setPvamount(rs.getDouble("pvamount"));
        bb.setListcek(rs.getString("listcek"));
        bb.setStatus(rs.getString("status"));
        bb.setSacode(rs.getString("sacode"));

        return bb;
    }
    
    public static BankBookMaster getMaster(LoginProfile log, BankBookParam prm) throws Exception {

        BankBookMaster rm = new BankBookMaster();
        Logger.getLogger(BankBookDAO.class.getName()).log(Level.INFO, "----------"+BankBookDAO.getBankCOA(log, prm.getBankcode()).getCode());
        rm.setListBankBook(getAllBankBook(log, prm));
        rm.setBfAmount(AccDAO.getOpenCloseBalance(log, prm.getYear(), prm.getPeriod(), BankBookDAO.getBankCOA(log, prm.getBankcode()).getCode()).getOpeningBalance());
        rm.setCfAmount(AccDAO.getOpenCloseBalance(log, prm.getYear(), prm.getPeriod(), BankBookDAO.getBankCOA(log, prm.getBankcode()).getCode()).getClosingBalance());

        return rm;

    }

}
//Logger.getLogger(PettyCashDAO.class.getName()).log(Level.INFO, String.valueOf(stmt));
