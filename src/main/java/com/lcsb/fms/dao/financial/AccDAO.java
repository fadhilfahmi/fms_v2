/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial;

import com.lcsb.fms.model.financial.OpenCloseBalance;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class AccDAO {
    
    public static OpenCloseBalance getOpenCloseBalance(LoginProfile log, String year, String period, String code) throws SQLException{
        
        OpenCloseBalance oc = new OpenCloseBalance();
        
        oc.setOpeningBalance(AccDAO.getOpenCloseBalanceAmount(log, year, period, code, "gl_openingbalance"));
        oc.setClosingBalance(AccDAO.getOpenCloseBalanceAmount(log, year, period, code, "gl_closingbalance"));

        return oc;
        
    }
    
    private static double getOpenCloseBalanceAmount(LoginProfile log, String year, String period, String code, String table) throws SQLException{
        
        double amt = 0.0;
        
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from "+ table +" where year = ? and  period = ? and coacode = ?");
        stmt.setString(1, year);
        stmt.setString(2, period);
        stmt.setString(3, code);
        
        rs = stmt.executeQuery();
        if (rs.next()) {
            
            
            if(rs.getDouble("debit") > 0){
                amt = rs.getDouble("debit");
            }
            if(rs.getDouble("credit") > 0){
                amt = rs.getDouble("credit");
            }
            
            Logger.getLogger(AccDAO.class.getName()).log(Level.INFO, String.valueOf(rs.getDouble("debit")+"$$"+rs.getDouble("credit")));
            
            
        }

        return amt;
        
    }
    
}
