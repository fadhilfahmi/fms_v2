/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.ap;

import com.lcsb.fms.dao.financial.gl.LiveGLDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.financial.ap.ApPaymentMaster;
import com.lcsb.fms.model.financial.ap.ApTempInvoiceAmount;
import com.lcsb.fms.model.financial.ap.VendorPayee;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucher;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherRefer;
import com.lcsb.fms.util.dao.LocationDAO;
import com.lcsb.fms.util.dao.SupplierDAO;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VendorPaymentDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("021009");
        mod.setModuleDesc("Payment Voucher");
        mod.setMainTable("cb_payvoucher");
        mod.setReferID_Master("refer");
        return mod;

    }

    public static List<ListTable> tableList() {

        String column_names[] = {"refer", "tarikh", "paidname", "amount", "checkid", "appid", "post"};
        String title_name[] = {"Reference No.", "Date", "Payee", "Amount (RM)", "Check ID", " Approve ID", "Post"};
        boolean boolview[] = {true, true, true, true, false, false, false};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            i.setBoolView(boolview[j]);
            lt.add(i);
        }

        return lt;
    }

    public static void deleteExist(LoginProfile log, String no) throws SQLException, Exception {
        Connection con = log.getCon();

        String deleteQuery_1 = "delete from cb_payvoucher where refer = ?";
        String deleteQuery_2 = "delete from cb_payvaucher_debit where refer = ?";
        String deleteQuery_3 = "delete from cb_payvoucer_refer where voucer = ?";
        PreparedStatement ps_1 = con.prepareStatement(deleteQuery_1);
        PreparedStatement ps_2 = con.prepareStatement(deleteQuery_2);
        PreparedStatement ps_3 = con.prepareStatement(deleteQuery_3);
        ps_1.setString(1, no);
        ps_2.setString(1, no);
        ps_3.setString(1, no);
        ps_1.executeUpdate();
        ps_2.executeUpdate();
        ps_3.executeUpdate();
        ps_1.close();
        ps_2.close();
        ps_3.close();

        updateInvoiceIfPVDeleted(log, no);
        updateChequeBook(log, no);
        LiveGLDAO.deleteFromGL(log, no);

    }

    private static void updateChequeBook(LoginProfile log, String no) throws Exception {

        String q = ("UPDATE cb_cekbook_cek set payno = '', paycode = '', payname = '',paydate = '0000-00-00',payamount = '0.00', remarks = '', flag = 'NO' where payno = ?");
        PreparedStatement ps = log.getCon().prepareStatement(q);
        ps.setString(1, no);
        ps.executeUpdate();
        ps.close();

    }

    private static void updateInvoiceIfPVDeleted(LoginProfile log, String refno) throws Exception {

        String oldcekno = "";
        ResultSet rs = null;
        ResultSet rs1 = null;
        Connection conet = log.getCon();
        PreparedStatement stmt = conet.prepareStatement("select * from cb_payvoucher_invoice where pvno = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();

        while (rs.next()) {

            String q = ("UPDATE ap_inv set paid = (paid - " + rs.getString("amount") + ")  where invrefno = '" + rs.getString("invno") + "'");
            PreparedStatement ps = conet.prepareStatement(q);
            //ps.setString(1, pvno);
            ps.executeUpdate();
            ps.close();
        }

        String deleteQuery = "delete from cb_payvoucher_invoice where pvno = ?";
        PreparedStatement ps = conet.prepareStatement(deleteQuery);
        ps.setString(1, refno);
        ps.executeUpdate();
        ps.close();

        rs.close();

    }

    public static void deleteItem(LoginProfile log, String novoucher, String refer) throws SQLException, Exception {

        String deleteQuery_2 = "delete from cb_payvaucher_debit where voucer= ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, novoucher);
        ps_2.executeUpdate();
        ps_2.close();

        updateInvoiceIfPVDeletedEach(log, novoucher);
        LiveGLDAO.reloadGL(log, refer, "PV");

    }

    private static void updateInvoiceIfPVDeletedEach(LoginProfile log, String refno) throws Exception {

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_payvoucher_invoice where pvrefno = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();

        if (rs.next()) {

            String q = ("UPDATE ap_inv set paid = (paid - " + rs.getString("amount") + ")  where invrefno = '" + rs.getString("invno") + "'");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            //ps.setString(1, pvno);
            ps.executeUpdate();
            ps.close();
        }

        String deleteQuery = "delete from cb_payvoucher_invoice where pvrefno = ?";
        PreparedStatement ps = log.getCon().prepareStatement(deleteQuery);
        ps.setString(1, refno);
        ps.executeUpdate();
        ps.close();

        rs.close();

    }

    public static String saveMain(LoginProfile log, PaymentVoucher item) throws Exception {

        String newrefer = AutoGenerate.getReferenceNox(log, "refer", "cb_payvoucher", "PVN", item.getEstatecode(), item.getYear(), item.getPeriod());

        try {
            //ps.setString(1, AutoGenerate.get2digitNo("info_bank", "code"));
            String q = ("insert into cb_payvoucher(refer,tarikh,paidtype,paidcode,paidname,paidaddress,paidcity,paidstate,paidpostcode,gstid,remarks,racoacode,racoadesc,amount,rm,paymentmode,bankcode,bankname,cekno,year,period,estatecode,estatename,voucherno,preid,prename,preposition,preparedate) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, newrefer);
            //ps.setString(1, item.getRefer());
            ps.setString(2, item.getTarikh());
            ps.setString(3, item.getPaidtype());
            ps.setString(4, item.getPaidcode());
            ps.setString(5, item.getPaidname());
            ps.setString(6, item.getPaidaddress());
            ps.setString(7, item.getPaidcity());
            ps.setString(8, item.getPaidstate());
            ps.setString(9, item.getPaidpostcode());
            ps.setString(10, item.getGstid());
            ps.setString(11, item.getRemarks());
            ps.setString(12, item.getRacoacode());
            ps.setString(13, item.getRacoadesc());
            ps.setDouble(14, item.getAmount());
            ps.setString(15, item.getRm());
            ps.setString(16, item.getPaymentmode());
            ps.setString(17, item.getBankcode());
            ps.setString(18, item.getBankname());
            ps.setString(19, item.getCekno());
            ps.setString(20, item.getYear());
            ps.setString(21, item.getPeriod());
            ps.setString(22, item.getEstatecode());
            ps.setString(23, item.getEstatename());
            ps.setString(24, AutoGenerate.getVoucherNo(log, "cb_payvoucher", "voucherno"));
            ps.setString(25, item.getPreid());
            ps.setString(26, item.getPrename());
            ps.setString(27, item.getPreposition());
            ps.setString(28, item.getPreparedate());

            ps.executeUpdate();

            ps.close();

            if (item.getPaymentmode().equals("Cheque")) {
                updateChequeBook(log, item, newrefer);
            }
            
            LiveGLDAO.reloadGL(log, newrefer, "PV");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newrefer;

    }

    private static void updateChequeBook(LoginProfile log, PaymentVoucher cb, String refer) throws Exception {

        String query = "UPDATE cb_cekbook_cek SET payno = ?, paycode = ?, payname = ?,paydate = ?, payamount = ?, remarks = ?, flag = ? where nocek = '" + cb.getCekno() + "'";

        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.setString(1, refer);
        ps.setString(2, cb.getPaidcode());
        ps.setString(3, cb.getPaidname());
        ps.setString(4, cb.getTarikh());
        ps.setDouble(5, cb.getAmount());
        ps.setString(6, cb.getRemarks());
        ps.setString(7, "Yes");
        ps.executeUpdate();
        ps.close();

    }

    public static void saveItem(LoginProfile log, PaymentVoucherItem item) throws Exception {

        try {
            String q = ("insert into cb_payvaucher_debit(coacode,coadescp,loclevel,loccode,locname,remarks,amount,refer,voucer,satype,sacode,sadesc,taxcode,taxdescp,taxrate,taxamt,taxcoacode,taxcoadescp) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCoacode());
            ps.setString(2, item.getCoadescp());
            ps.setString(3, item.getLoclevel());
            ps.setString(4, item.getLoccode());
            ps.setString(5, item.getLocname());
            ps.setString(6, item.getRemarks());
            ps.setDouble(7, item.getAmount());
            ps.setString(8, item.getRefer());

            ps.setString(9, AutoGenerate.getReferAddBack(log, "cb_payvaucher_debit", "voucer", item.getRefer(), "refer"));
            Logger.getLogger(VendorPaymentDAO.class.getName()).log(Level.INFO, "--------Mark #1 voucer---" + item.getVoucer());
            Logger.getLogger(VendorPaymentDAO.class.getName()).log(Level.INFO, "--------Mark #2 refer---" + item.getRefer());
            ps.setString(10, item.getSatype());
            ps.setString(11, item.getSacode());
            ps.setString(12, item.getSadesc());
            ps.setString(13, item.getTaxcode());
            ps.setString(14, item.getTaxdescp());
            ps.setDouble(15, item.getTaxrate());
            ps.setDouble(16, item.getTaxamt());
            ps.setString(17, item.getTaxcoacode());
            ps.setString(18, item.getTaxcoadescp());

            ps.executeUpdate();
            Logger.getLogger(VendorPaymentDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();
            
            LiveGLDAO.reloadGL(log, item.getRefer(), "PV");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void saveRefer(LoginProfile log, PaymentVoucherRefer item) throws Exception {

        try {
            String q = ("insert into cb_payvoucer_refer(amount,bf,cf,no,paid,payment,refer,remarks,tarikh,type,voucer) values (?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setDouble(1, item.getAmount());
            ps.setString(2, item.getBf());
            ps.setString(3, item.getCf());
            ps.setString(4, item.getNo());
            ps.setString(5, item.getPaid());
            ps.setString(6, item.getPayment());
            ps.setString(7, AutoGenerate.getReferAddBack(log, "cb_payvoucer_refer", "refer", item.getVoucer(), "voucer"));
            ps.setString(8, item.getRemarks());
            ps.setString(9, item.getTarikh());
            ps.setString(10, item.getType());
            ps.setString(11, item.getVoucer());

            ps.executeUpdate();
            Logger.getLogger(VendorPaymentDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateMain(LoginProfile log, PaymentVoucher m, String id) throws Exception {

        if (checkChequeforUpdate(log, m.getCekno(), id)) {
            updateChequeBookCekno(log, getOldChequeNo(log, id));
        }

        try {
            //String q = ("insert into gl_jv_item(Jvid,JVrefno,remark,actcode,actdesc,loccode,locdesc,loclevel,satype,sacode,sadesc,debit,credit,taxcode,taxdescp,taxrate,gstid,amtbeforetax) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            String q = ("UPDATE cb_payvoucher set refer=? ,tarikh=?,paidtype=?,paidcode=?,paidname=?,paidaddress=?,paidcity=?,paidstate=?,paidpostcode=?,gstid=?,remarks=?,racoacode=?,racoadesc=?,amount=?,rm=?,paymentmode=?,bankcode=?,bankname=?,cekno=?,year=?,period=?,estatecode=?,estatename=?,voucherno=? WHERE refer = '" + id + "'");
            //amtbeforetax=?
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, m.getRefer());
            ps.setString(2, m.getTarikh());
            ps.setString(3, m.getPaidtype());
            ps.setString(4, m.getPaidcode());
            ps.setString(5, m.getPaidname());
            ps.setString(6, m.getPaidaddress());
            ps.setString(7, m.getPaidcity());
            ps.setString(8, m.getPaidstate());
            ps.setString(9, m.getPaidpostcode());
            ps.setString(10, m.getGstid());
            ps.setString(11, m.getRemarks());
            ps.setString(12, m.getRacoacode());
            ps.setString(13, m.getRacoadesc());
            ps.setDouble(14, m.getAmount());
            ps.setString(15, m.getRm());
            Logger.getLogger(VendorPaymentDAO.class.getName()).log(Level.INFO, "--------Mark #0 before generator--- refer---" + m.getRm());
            ps.setString(16, m.getPaymentmode());
            ps.setString(17, m.getBankcode());
            ps.setString(18, m.getBankname());
            ps.setString(19, m.getCekno());
            ps.setString(20, m.getYear());
            ps.setString(21, m.getPeriod());
            ps.setString(22, m.getEstatecode());
            ps.setString(23, m.getEstatename());
            ps.setString(24, m.getVoucherno());
            Logger.getLogger(VendorPaymentDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();

            //Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

            VendorPaymentDAO.updateChequeBook(log, m, id);
            LiveGLDAO.reloadGL(log, m.getRefer(), "PV");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static String getOldChequeNo(LoginProfile log, String refno) throws SQLException, Exception {

        String oldcekno = "";
        ResultSet rs = null;
        ResultSet rs1 = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select cekno from cb_payvoucher where refer = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();

        if (rs.next()) {
            oldcekno = rs.getString("cekno");
        }

        return oldcekno;
    }

    public static boolean checkChequeforUpdate(LoginProfile log, String cekno, String refno) throws SQLException, Exception {
        boolean cek = false;
        String oldcekno = "";
        ResultSet rs = null;
        ResultSet rs1 = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select cekno from cb_payvoucher where refer = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();

        if (rs.next()) {
            oldcekno = rs.getString("cekno");
        }

        if (!cekno.equals(oldcekno)) {
            cek = true;
        }

        Logger.getLogger(VendorPaymentDAO.class.getName()).log(Level.INFO, "-----asdasdsadasdasda------" + cek);
        return cek;
    }

    private static void updateChequeBookCekno(LoginProfile log, String cekno) throws Exception {

        String query = "UPDATE cb_cekbook_cek SET `payno` = '', `paycode` = '', `payname` = '', `paydate` = '0000-00-00', `payamount` = '0.00', `remarks` = '', `flag` = 'NO', `status` = 'UNUSED' where nocek = '" + cekno + "'";
        try {
            Logger.getLogger(VendorPaymentDAO.class.getName()).log(Level.INFO, query);
            PreparedStatement ps = log.getCon().prepareStatement(query);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateItem(LoginProfile log, PaymentVoucherItem item, String id) throws Exception {

        try {
            String q = ("UPDATE cb_payvaucher_debit set coacode=?,coadescp=?,loclevel=?,loccode=?,locname=?,remarks=?,amount=?,satype=?,sacode=?,sadesc=?,taxcode=?,taxdescp=?,taxrate=?,taxamt=? WHERE voucer = '" + id + "'");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCoacode());
            ps.setString(2, item.getCoadescp());
            ps.setString(3, item.getLoclevel());
            ps.setString(4, item.getLoccode());
            ps.setString(5, item.getLocname());
            ps.setString(6, item.getRemarks());
            ps.setDouble(7, item.getAmount());
            ps.setString(8, item.getRefer());
            ps.setString(9, item.getVoucer());
            Logger.getLogger(VendorPaymentDAO.class.getName()).log(Level.INFO, "--------Mark #1 voucer---" + item.getVoucer());

            Logger.getLogger(VendorPaymentDAO.class.getName()).log(Level.INFO, "--------Mark #2 refer---" + item.getRefer());
            ps.setString(8, item.getSatype());
            ps.setString(9, item.getSacode());
            ps.setString(10, item.getSadesc());
            ps.setString(11, item.getTaxcode());
            ps.setString(12, item.getTaxdescp());
            ps.setDouble(13, item.getTaxrate());
            ps.setDouble(14, item.getTaxamt());

            Logger.getLogger(VendorPaymentDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();

            ps.close();
            
            LiveGLDAO.reloadGL(log, item.getRefer(), "PV");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static String getCVno(LoginProfile log, String abb) throws Exception {

        ResultSet rs = null;
        String CVno = "";
        String year = AccountingPeriod.getCurYear(log);
        String curperiod = AccountingPeriod.getCurPeriod(log);
        //String estatecode = LoginProfile.getEstateCode();
        String estatecode = "9911";

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat(?,'" + estatecode + "',substring('" + year + "',3,2),lpad('" + curperiod + "',2,'0'),lpad((max(substring(refer,14,4))+1),4,'0')),concat('JPY','" + estatecode + "',substring('" + year + "',3,2),lpad('" + curperiod + "',2,'0'),'0001')) as refer from cb_cashvoucher where refer like concat(?,'%') and year='" + year + "' and period='" + curperiod + "' and estatecode like concat('" + estatecode + "','%')");
        stmt.setString(1, abb);
        stmt.setString(2, abb);

        rs = stmt.executeQuery();
        if (rs.next()) {
            CVno = rs.getString(1);

        }

        rs.close();
        stmt.close();
        return CVno;

    }

    public static String getCVoucherNo(LoginProfile log) throws Exception {

        ResultSet rs = null;
        String CVno = "";

        PreparedStatement stmt = log.getCon().prepareStatement("SELECT ifnull(concat(lpad((max(voucherid)+1),5,'0')),concat('0001')) as new FROM `cb_cashvoucher`");

        rs = stmt.executeQuery();
        if (rs.next()) {
            CVno = rs.getString(1);
        }

        rs.close();
        stmt.close();
        return CVno;

    }

    public static String getCVid(LoginProfile log, String refno) throws Exception, SQLException {

        ResultSet rs = null;
        String JVno = "";

        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat('" + refno + "',lpad((max(right(novoucher,4))+1),4,'0')),concat('" + refno + "','0001')) as njvno from cb_cashvoucher_account where refer='" + refno + "'");

        rs = stmt.executeQuery();
        if (rs.next()) {
            JVno = rs.getString(1);
        }

        rs.close();
        stmt.close();
        return JVno;

    }

    public static Double getSum(LoginProfile log, String column, String id) throws Exception {
        Double x = 0.00;
        Statement stment = log.getCon().createStatement();
        ResultSet setent = stment.executeQuery(" select sum(" + column + ") as sumamount from cb_payvaucher_debit where refer = '" + id + "'");

        while (setent.next()) {
            x += setent.getDouble("sumamount");
        }

        stment.close();
        setent.close();
        return x;
    }

    public static Double getBalance(LoginProfile log, String column, String id) throws Exception {
        Double x = 0.00;
        Double y = 0.00;
        Statement stment = log.getCon().createStatement();
        ResultSet setent = stment.executeQuery(" select sum(" + column + ") as sumamount from cb_cashvoucher_account where refer = '" + id + "'");

        while (setent.next()) {
            x += setent.getDouble("sumamount");
        }

        setent = stment.executeQuery(" select total from cb_cashvoucher where refer = '" + id + "'");

        if (setent.next()) {
            y = setent.getDouble("total");
        }

        stment.close();
        setent.close();
        return y - x;
    }

    public static PaymentVoucherItem getItem(String id) throws Exception {

        PaymentVoucherItem v = new PaymentVoucherItem();

        v.setVoucer(id);
        //v.setAmtbeforetax(getBalance("debit",id));

        return v;
    }

    public static PaymentVoucherRefer getRefer(String id) throws Exception {

        PaymentVoucherRefer v = new PaymentVoucherRefer();

        v.setRefer(id);
        //v.setAmtbeforetax(getBalance("debit",id));

        return v;
    }

    private static void updateAmount(LoginProfile log, String JVrefno) throws Exception {

        String query = "UPDATE gl_jv SET todebit = '" + getSum(log, "debit", JVrefno) + "',tocredit = '" + getSum(log, "credit", JVrefno) + "' where JVrefno = '" + JVrefno + "'";

        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }

    public static List<PaymentVoucherItem> getAllPYVItem(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<PaymentVoucherItem> CVi;
        CVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_payvaucher_debit where refer = '" + refno + "' order by voucer");

            while (rs.next()) {
                CVi.add(getPYVitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static PaymentVoucherItem getPYVitem(LoginProfile log, String id) throws SQLException, Exception {
        PaymentVoucherItem c = null;
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_payvaucher_debit where voucer=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getPYVitem(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    private static PaymentVoucherItem getPYVitem(ResultSet rs) throws SQLException {
        PaymentVoucherItem cv = new PaymentVoucherItem();

        cv.setCoacode(rs.getString("coacode"));
        cv.setCoadescp(rs.getString("coadescp"));
        cv.setLoclevel(rs.getString("loclevel"));
        cv.setLoccode(rs.getString("loccode"));
        cv.setLocname(rs.getString("locname"));
        cv.setChargetype(rs.getString("chargetype"));
        cv.setChargecode(rs.getString("chargedescp"));
        cv.setRemarks(rs.getString("remarks"));
        cv.setAmount(Double.parseDouble(rs.getString("amount")));
        cv.setVoucer(rs.getString("voucer"));
        cv.setRefer(rs.getString("refer"));
        cv.setSatype(rs.getString("satype"));
        cv.setSacode(rs.getString("sacode"));
        cv.setSadesc(rs.getString("sadesc"));
        cv.setTaxcode(rs.getString("taxcode"));
        cv.setTaxdescp(rs.getString("taxdescp"));
        cv.setTaxcoacode(rs.getString("taxcoacode"));
        cv.setTaxcoadescp(rs.getString("taxcoadescp"));
        cv.setTaxrate(rs.getDouble("taxrate"));
        cv.setTaxamt(rs.getDouble("taxamt"));
        cv.setAmtbeforetax(rs.getDouble("amtbeforetax"));

        return cv;
    }

    public static List<PaymentVoucher> getAllPYV(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<PaymentVoucher> CV;
        CV = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_payvoucher where refer = '" + refno + "' order by refer");

            while (rs.next()) {
                CV.add(getPYV(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }

    public static PaymentVoucher getPYV(LoginProfile log, String refno) throws SQLException, Exception {
        PaymentVoucher c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_payvoucher where refer=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getPYV(rs);
        }

        return c;
    }

    public static PaymentVoucher getPYV(ResultSet rs) throws SQLException {

        PaymentVoucher cv = new PaymentVoucher();

        cv.setEstatecode(rs.getString("estatecode"));
        cv.setEstatename(rs.getString("estatename"));
        cv.setEstateaddress(rs.getString("estateaddress"));
        cv.setEstatecity(rs.getString("estatecity"));
        cv.setEstatestate(rs.getString("estatestate"));
        cv.setEstatepostcode(rs.getString("estatepostcode"));
        cv.setEstatephone(rs.getString("estatephone"));
        cv.setEstatefax(rs.getString("estatefax"));
        cv.setYear(rs.getString("year"));
        cv.setPeriod(rs.getString("period"));
        cv.setBankcode(rs.getString("bankcode"));
        cv.setBankname(rs.getString("bankname"));
        cv.setVoucherno(rs.getString("voucherno"));
        cv.setTarikh(rs.getString("tarikh"));
        cv.setPaidtype(rs.getString("paidtype"));
        cv.setPaidcode(rs.getString("paidcode"));
        cv.setPaidname(rs.getString("paidname"));
        cv.setPaidaddress(rs.getString("paidaddress"));
        cv.setPaidcity(rs.getString("paidcity"));
        cv.setPaidstate(rs.getString("paidstate"));
        cv.setPaidpostcode(rs.getString("paidpostcode"));
        cv.setPost(rs.getString("post"));
        cv.setTarikhpost(rs.getString("tarikhpost"));
        cv.setRemarks(rs.getString("remarks"));
        cv.setAmount(rs.getDouble("amount"));
        cv.setPaymentmode(rs.getString("paymentmode"));
        cv.setCekno(rs.getString("cekno"));
        cv.setAppid(rs.getString("appid"));
        cv.setAppname(rs.getString("appname"));
        cv.setAppposition(rs.getString("appposition"));
        cv.setPreid(rs.getString("preid"));
        cv.setPrename(rs.getString("prename"));
        cv.setPreposition(rs.getString("preposition"));
        cv.setFlag(rs.getString("flag"));
        cv.setRefer(rs.getString("refer"));
        cv.setFlagbank(rs.getString("flagbank"));
        cv.setRm(rs.getString("rm"));
        cv.setCheckid(rs.getString("checkid"));
        cv.setCheckname(rs.getString("checkname"));
        cv.setCheckposition(rs.getString("checkposition"));
        cv.setStmtyear(rs.getString("stmtyear"));
        cv.setStmtperiod(rs.getString("stmtperiod"));
        cv.setNoagree(rs.getString("noagree"));
        cv.setPaymentflag(rs.getString("paymentflag"));
        cv.setAdvancetype(rs.getString("advancetype"));
        cv.setPreparedate(rs.getString("preparedate"));
        cv.setCheckdate(rs.getString("checkdate"));
        cv.setApprovedate(rs.getString("approvedate"));
        cv.setAdvance(rs.getString("advance"));
        cv.setPostgst(rs.getString("postgst"));
        cv.setGstid(rs.getString("gstid"));
        cv.setGstdate(rs.getString("gstdate"));
        cv.setRacoacode(rs.getString("racoacode"));
        cv.setRacoadesc(rs.getString("racoadesc"));

        return cv;
    }

    public static double getDebit(LoginProfile log, String refno) throws SQLException, Exception {
        double amount = 0;
        String btype = "";
        ResultSet rs = null;
        ResultSet rs1 = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select btype,amountno from sl_inv where invref=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amount = rs.getDouble("amountno");
            btype = rs.getString("btype");
        }

        if (btype.equals("Stock And Store")) {

            PreparedStatement stmt1 = log.getCon().prepareStatement("select sum(amount) as amt,sum(taxamt) as tax from sl_inv_item where ivref=?");
            stmt1.setString(1, refno);
            rs1 = stmt.executeQuery();
            if (rs.next()) {
                amount = rs.getDouble("tax");
            }

        }

        return amount;
    }

    public static double getCredit(LoginProfile log, String refno) throws SQLException, Exception {
        double amt = 0;
        double amttax = 0;
        double totalCredit = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select sum(amount) as amt,sum(taxamt) as amttax from sl_inv_item where ivref=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amt = rs.getDouble("amt");
            amttax = rs.getDouble("amttax");
        }

        totalCredit = amt + amttax;

        return totalCredit;
    }

    public static int getCheckCounter(LoginProfile log) throws Exception {

        int cnt = 0;
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from sl_inv where year = ? and period = ? and cid = ''");

        stmt.setString(1, AccountingPeriod.getCurYear(log));
        stmt.setString(2, AccountingPeriod.getCurPeriod(log));
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }

        return cnt;
    }

    public static int getApproveCounter(LoginProfile log) throws Exception {

        int cnt = 0;
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from sl_inv where year = ? and period = ? and cid <> ? and pid = ?");

        stmt.setString(1, AccountingPeriod.getCurYear(log));
        stmt.setString(2, AccountingPeriod.getCurPeriod(log));
        stmt.setString(3, "");
        stmt.setString(4, "");
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }

        return cnt;
    }

    public static void checkVoucher(LoginProfile log, String refer, String staff_id, String staff_name) throws Exception {

        String query = "UPDATE cb_payvoucher SET checkid = '" + staff_id + "',checkname = '" + staff_name + "',checkdate='" + AccountingPeriod.getCurrentTimeStamp() + "' where refer = '" + refer + "'";

        try {
            PreparedStatement ps = log.getCon().prepareStatement(query);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void approveVoucher(LoginProfile log, String refer, String staff_id, String staff_name, String position) throws Exception {

        String query = "UPDATE cb_payvoucher SET appid = '" + staff_id + "',appname = '" + staff_name + "',approvedate='" + AccountingPeriod.getCurrentTimeStamp() + "', appposition = '" + position + "' where refer = '" + refer + "'";

        Logger.getLogger(VendorPaymentDAO.class.getName()).log(Level.INFO, "-------mmmmmmmmmmmmmmmmmmmmmmmmm----" + query);

        try {
            PreparedStatement ps = log.getCon().prepareStatement(query);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static List<PaymentVoucherRefer> getAllRefer(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<PaymentVoucherRefer> pvr;
        pvr = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cb_payvoucer_refer where voucer = '" + refno + "' order by voucer");

            while (rs.next()) {
                pvr.add(getRefer(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return pvr;
    }

    private static PaymentVoucherRefer getRefer(ResultSet rs) throws SQLException {
        PaymentVoucherRefer pv = new PaymentVoucherRefer();

        pv.setAmount(rs.getDouble("amount"));
        pv.setBf(rs.getString("bf"));
        pv.setCf(rs.getString("cf"));
        pv.setNo(rs.getString("no"));
        pv.setPaid(rs.getString("paid"));
        pv.setPayment(rs.getString("payment"));
        pv.setRefer(rs.getString("refer"));
        pv.setRemarks(rs.getString("remarks"));
        pv.setTarikh(rs.getString("tarikh"));
        pv.setType(rs.getString("type"));
        pv.setVoucer(rs.getString("voucer"));

        return pv;
    }

    public static PaymentVoucherRefer getReferEach(LoginProfile log, String id) throws SQLException, Exception {
        PaymentVoucherRefer c = new PaymentVoucherRefer();
        ResultSet rs = null;

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_payvoucer_refer where refer=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getRefer(rs);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

//        if(c == null){
//                
//        }
        return c;
    }

    public static void updateRefer(LoginProfile log, PaymentVoucherRefer item, String id) throws Exception {
        try {
            String q = ("UPDATE cb_payvoucer_refer set amount =?,bf =?,cf =?,no =?,paid =?,payment =?,refer =?,remarks =?,tarikh =?,type =?,voucer =? WHERE refer = '" + id + "'");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setDouble(1, item.getAmount());
            ps.setString(2, item.getBf());
            ps.setString(3, item.getCf());
            ps.setString(4, item.getNo());
            ps.setString(5, item.getPaid());
            ps.setString(6, item.getPayment());
            ps.setString(7, item.getRefer());
            ps.setString(8, item.getRemarks());
            ps.setString(9, item.getTarikh());
            ps.setString(10, item.getType());
            ps.setString(11, item.getVoucer());

            ps.executeUpdate();
            Logger.getLogger(VendorPaymentDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteRefer(LoginProfile log, String novoucher, String refer) throws SQLException, Exception {

        try {
            String deleteQuery_2 = "delete from cb_payvoucer_refer where refer = ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, novoucher);
            Logger.getLogger(VendorPaymentDAO.class.getName()).log(Level.INFO, String.valueOf(ps_2));
            ps_2.executeUpdate();
            ps_2.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        //updateAmount(refer);

    }

    public static List<VendorPayee> getSupplierHasInvoice(LoginProfile log, String keyword) throws Exception {

        Statement stmt = null;
        List<VendorPayee> Com;
        Com = new ArrayList();

        String q = "";

        if (keyword != null) {
            q = " and suppcode like '%" + keyword + "%' or suppname like '%" + keyword + "%'";
        }

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select distinct(suppcode) as code,suppname as name from ap_inv where totalamount > paid and postflag='posted' " + q + " order by suppcode");
            Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {

                VendorPayee vp = new VendorPayee();

                vp.setSuppcode(rs.getString("code"));
                vp.setSuppname(rs.getString("name"));
                vp.setTotalInvoice(getTotalInvoices(log, rs.getString("code")));

                Com.add(vp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }

    private static int getTotalInvoices(LoginProfile log, String code) throws Exception {

        int cnt = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from ap_inv where suppcode=? and totalamount > paid and postflag='posted'");
        stmt.setString(1, code);
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }

        return cnt;
    }

    public static int saveIntoTempInvoice(LoginProfile log, List<ApTempInvoiceAmount> listT) throws Exception {

        int i = 0;
        try {

            String deleteQuery_2 = "delete from cb_payvoucher_temp where sessionid = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, listT.get(0).getSessionid());
                ps_2.executeUpdate();
            }

            for (ApTempInvoiceAmount m : listT) {

                String q = ("INSERT INTO cb_payvoucher_temp(sessionid,invrefno,amounttopay,suppcode) values (?,?,?,?)");

                try (PreparedStatement ps = log.getCon().prepareStatement(q)) {

                    ps.setString(1, m.getSessionid());
                    ps.setString(2, m.getInvrefno() == null ? "-" : m.getInvrefno());
                    ps.setDouble(3, m.getAmounttopay());
                    ps.setString(4, m.getSuppcode());

                    Logger.getLogger(VendorPaymentDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
                    ps.executeUpdate();

                    i = 1;
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
            i = 0;
        }

        return i;

    }

    public static ApTempInvoiceAmount getInvoiceEach(LoginProfile log, String id) throws SQLException, Exception {
        ApTempInvoiceAmount c = new ApTempInvoiceAmount();
        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_payvoucher_temp where sessionid=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                //c = getRefer(rs);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    public static List<ApTempInvoiceAmount> getInvoice(LoginProfile log, String id) throws Exception {

        List<ApTempInvoiceAmount> Com;
        Com = new ArrayList();

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_payvoucher_temp where sessionid=? order by invrefno asc");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            while (rs.next()) {

                ApTempInvoiceAmount vp = new ApTempInvoiceAmount();

                vp.setInvrefno(rs.getString("invrefno"));
                vp.setAmounttopay(rs.getDouble("amounttopay"));
                vp.setPortion(rs.getString("portion"));
                vp.setSuppcode(rs.getString("suppcode"));

                Com.add(vp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }

    public static String getRemark(LoginProfile log, String id) throws Exception {

        String remark = "Payment for Invoice No : ";
        String inv = "";

        List<ApTempInvoiceAmount> listAll = (List<ApTempInvoiceAmount>) getInvoice(log, id);
        int x = listAll.size();
        int i = 0;
        for (ApTempInvoiceAmount all : listAll) {
            i++;

            if (i == listAll.size()) {
                inv = inv + all.getInvrefno();
            } else {
                inv = inv + all.getInvrefno() + ", ";
            }

        }

        return remark + inv;
    }

    public static Double getSumInvoice(LoginProfile log, String id) throws Exception {
        Double x = 0.00;
        Statement stment = log.getCon().createStatement();
        ResultSet setent = stment.executeQuery(" select sum(amounttopay) as sumamount from cb_payvoucher_temp where sessionid = '" + id + "'");

        if (setent.next()) {
            x = setent.getDouble("sumamount");
        }

        stment.close();
        setent.close();
        return x;
    }

    public static int saveIntoTempMaster(LoginProfile log, PaymentVoucher item) throws Exception {
        int i = 0;
        //String newrefer = AutoGenerate.getReferenceNo("refer", "cb_payvoucher", "PVN", item.getEstatecode());

        String deleteQuery_2 = "delete from cb_payvoucher_temp_master where sessionid = ?";
        try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
            ps_2.setString(1, item.getSessionID());
            ps_2.executeUpdate();
        }

        try {
            //ps.setString(1, AutoGenerate.get2digitNo("info_bank", "code"));
            String q = ("insert into cb_payvoucher_temp_master(refer,tarikh,paidtype,paidcode,paidname,paidaddress,paidcity,paidstate,paidpostcode,gstid,remarks,racoacode,racoadesc,amount,rm,paymentmode,bankcode,bankname,cekno,year,period,estatecode,estatename,voucherno,sessionid,flagbank) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, "PVN");
            //ps.setString(1, item.getRefer());
            ps.setString(2, item.getTarikh());
            ps.setString(3, item.getPaidtype());
            ps.setString(4, item.getPaidcode());
            ps.setString(5, item.getPaidname());
            ps.setString(6, item.getPaidaddress());
            ps.setString(7, item.getPaidcity());
            ps.setString(8, item.getPaidstate());
            ps.setString(9, item.getPaidpostcode());
            ps.setString(10, item.getGstid());
            ps.setString(11, item.getRemarks());
            ps.setString(12, item.getRacoacode());
            ps.setString(13, item.getRacoadesc());
            ps.setDouble(14, item.getAmount());
            ps.setString(15, item.getRm());
            ps.setString(16, item.getPaymentmode());
            ps.setString(17, item.getBankcode());
            ps.setString(18, item.getBankname());
            ps.setString(19, item.getCekno());
            ps.setString(20, item.getYear());
            ps.setString(21, item.getPeriod());
            ps.setString(22, item.getEstatecode());
            ps.setString(23, item.getEstatename());
            ps.setString(24, "");
            ps.setString(25, item.getSessionID());
            ps.setString(26, item.getFlagbank());

            ps.executeUpdate();

            ps.close();

            i = 1;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return i;

    }

    public static ApPaymentMaster getAllInfo(LoginProfile log, String sessionID, String companycode, String companyname) throws Exception {

        ApPaymentMaster mt = new ApPaymentMaster();
        PaymentVoucher pv = new PaymentVoucher();
        List<PaymentVoucherItem> l = new ArrayList();

        ResultSet rs = null;
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cb_payvoucher_temp_master where sessionid=?");
            stmt.setString(1, sessionID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                pv = getPYV(rs);
            }

            mt.setMaster(pv);

            List<ApTempInvoiceAmount> listx = (List<ApTempInvoiceAmount>) getInvoice(log, sessionID);
            for (ApTempInvoiceAmount j : listx) {

                PaymentVoucherItem pi = new PaymentVoucherItem();

                pi.setAmount(j.getAmounttopay());
                pi.setCoacode(SupplierDAO.getAssignSupplier(log, j.getSuppcode()).getAccode());
                pi.setCoadescp(SupplierDAO.getAssignSupplier(log, j.getSuppcode()).getAcdesc());
                pi.setLoccode(companycode);
                pi.setLoclevel("Company");
                pi.setLocname(companyname);
                pi.setRemarks("INVOICE NO : " + j.getInvrefno());
                pi.setSacode(j.getSuppcode());
                pi.setSadesc(SupplierDAO.getAssignSupplier(log, j.getSuppcode()).getSuppname());
                pi.setSatype("Supplier");
                pi.setTaxamt(0.00);
                pi.setTaxcoacode("00");
                pi.setTaxcoadescp("None");
                pi.setTaxcode("00");
                pi.setTaxdescp("None");
                pi.setTaxrate(0.00);
                pi.setInvrefno(j.getInvrefno());

                l.add(pi);

            }

            mt.setListInvoice(l);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return mt;
    }

    public static String generateVoucher(LoginProfile log, String sessionID, String companycode, String companyname) throws Exception {

        ApPaymentMaster am = (ApPaymentMaster) VendorPaymentDAO.getAllInfo(log, sessionID, companycode, companyname);

        PaymentVoucher pv = new PaymentVoucher();
        PaymentVoucher p = am.getMaster();

        String newrefer = AutoGenerate.getReferenceNox(log, "refer", "cb_payvoucher", "PVN", p.getEstatecode(), p.getYear(), p.getPeriod());
        p.setRefer(newrefer);
        p.setVoucherno(AutoGenerate.getVoucherNo(log, "cb_payvoucher", "voucherno"));

        VendorPaymentDAO.saveMain(log, p);
        updatePaymentVoucherforInvoice(log, newrefer);

        List<PaymentVoucherItem> listx = (List<PaymentVoucherItem>) am.getListInvoice();
        for (PaymentVoucherItem j : listx) {

            String addBack = AutoGenerate.getReferAddBack(log, "cb_payvaucher_debit", "voucer", newrefer, "refer");
            j.setVoucer(addBack);
            j.setRefer(newrefer);
            VendorPaymentDAO.saveItem(log, j);
            updateInvoice(log, j.getInvrefno(), j.getAmount());
            savePaymentInvoiceReference(log, newrefer, j.getInvrefno(), addBack, j.getAmount());

        }

        return newrefer;
    }

    private static void updateInvoice(LoginProfile log, String invrefno, double amount) throws SQLException, Exception {

        ResultSet rs = null;

        double amountinv = 0.0;
        double paid = 0.00;

        PreparedStatement stmt = log.getCon().prepareStatement("select * from ap_inv where invrefno=?");
        stmt.setString(1, invrefno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amountinv = rs.getDouble("totalamount");
            paid = rs.getDouble("paid");
        }

        if (amountinv >= amount) {

            String query = "UPDATE ap_inv SET paid = ? where invrefno = '" + invrefno + "'";

            try (PreparedStatement ps = log.getCon().prepareStatement(query)) {
                ps.setDouble(1, amount + paid);
                ps.executeUpdate();
            }

        }

    }

    private static void updatePaymentVoucherforInvoice(LoginProfile log, String refer) throws SQLException, Exception {

        String query = "UPDATE cb_payvoucher SET invoice = ? where refer = '" + refer + "'";

        try (PreparedStatement ps = log.getCon().prepareStatement(query)) {
            ps.setBoolean(1, true);
            ps.executeUpdate();
        }

    }

    private static void savePaymentInvoiceReference(LoginProfile log, String pvno, String invno, String pvrefno, double amount) throws SQLException, Exception {

        ResultSet rs = null;

        String q = ("insert into cb_payvoucher_invoice(pvno,invno,pvrefno,amount) values (?,?,?,?)");
        PreparedStatement ps = log.getCon().prepareStatement(q);
        ps.setString(1, pvno);
        ps.setString(2, invno);
        ps.setString(3, pvrefno);
        ps.setDouble(4, amount);

        ps.executeUpdate();

    }

}
