/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.dao.financial.gl;

import com.lcsb.fms.financial.gl.post.Distribute;
import com.lcsb.fms.financial.gl.post.ErrorPost;
import com.lcsb.fms.financial.gl.post.Item;
import com.lcsb.fms.financial.gl.post.Master;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.ext.ParseSafely;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class DistributeDAO {
    
    public static Distribute distributeNow(String voucherno, String vtype, LoginProfile log) throws Exception{
        
        Master post = (Master) PostDAO.getInfo(log, voucherno, vtype);//get all voucher information and detail 
        Distribute dist = (Distribute) DistributeDAO.checkTransaction(log, voucherno, post);//get all checking detail and return back an error status
        
        if(dist.isSuccess()){
            
        }else{
            executeDistribution(log, post.getListItem());
            updateVoucherStatus(log, voucherno, vtype);
            
            if(!DebitCreditNoteDAO.hasDebitCreditNote(log, voucherno)){
                dist.setNoteNo(DebitCreditNoteDAO.generateDebitCredit(post, log));//get debit/credit no and keep it in Dist
            }else{
                dist.setNoteNo("none");
            }
            
        }
        dist.setMaster(post);
        return dist;
    }
    
    private static Distribute checkTransaction(LoginProfile log, String voucherno, Master all) throws Exception{
        
        Distribute checkStack = new Distribute();
        boolean mainCheck = false;
        
        List<ErrorPost> er = new ArrayList();
        
        //check voucher already posted or not
        if(checkExisting(log, voucherno)){
            ErrorPost error = new ErrorPost();
            error.setError(true);
            error.setErrorID("Post-b-1");
            error.setErrorDesc("Transaction already posted");
            error.setErrorItem("");
            er.add(error);
            mainCheck = true;
            Logger.getLogger(DistributeDAO.class.getName()).log(Level.INFO, "+++++++++++++checkexisting+++++++++++++"+String.valueOf(mainCheck));
        }else{
            ErrorPost error = new ErrorPost();
            error.setError(false);
            error.setErrorID("Post-b-1");
            error.setErrorDesc("Transaction has never been posted before.");
            error.setErrorItem("");
            er.add(error);
        }
        
        //check account code for each item or detail voucher
        List<Item> listAll = (List<Item>) all.getListItem();
        for (Item it : listAll) {
            int i = 0;
            ResultSet rs = null;
            Connection con = log.getCon();
            PreparedStatement stmt = con.prepareStatement("select count(*) as cnt from chartofacccount where code=? and finallvl='yes' and active='yes'");
            stmt.setString(1, it.getCoacode());
            rs = stmt.executeQuery();
            if (rs.next()) {
                i = rs.getInt("cnt");
            }
            if(i<1){
                ErrorPost error = new ErrorPost();
                error.setError(true);
                error.setErrorID("Post-b-2");
                error.setErrorDesc("Account Code does not exist , Not Final Level or Not Active");
                error.setErrorItem(it.getCoacode()+" - "+it.getCoadesc());
                er.add(error);
                mainCheck = true;
                Logger.getLogger(DistributeDAO.class.getName()).log(Level.INFO, "+++++++++++++checkeaccount+++++++++++++"+it.getCoacode()+"-"+it.getCoadesc());
            }else{
                ErrorPost error = new ErrorPost();
                error.setError(false);
                error.setErrorID("Post-b-2");
                error.setErrorDesc("Account code existed.");
                error.setErrorItem(it.getCoacode()+" - "+it.getCoadesc());
                er.add(error);
            }
        }
        
        ErrorPost cPeriod = (ErrorPost) PostDAO.checkPeriod(log, all.getPeriod());
        
        //if(cPeriod.getError()){
        //    er.add(cPeriod);
        //    mainCheck = true;
        //}
        
        ErrorPost cBal = (ErrorPost) PostDAO.checkBalance(ParseSafely.parseDoubleSafely(all.getDebit()), ParseSafely.parseDoubleSafely(all.getCredit()));
        
        if(cBal.getError()){
            er.add(cBal);
            mainCheck = true;
        }
        
        
        checkStack.setSuccess(mainCheck);
        checkStack.setListError(er);
        
        return checkStack;
    }
    
    private static boolean checkExisting(LoginProfile log, String voucherno) throws Exception{
        
        boolean check = false;
        int i = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from gl_posting_distribute where novoucher=?");
        stmt.setString(1, voucherno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            i = rs.getInt("cnt");
        }
        
        if(i>0){
            check = true;
        }
        
        return check;
    }
    
    private static void executeDistribution(LoginProfile log, List<Item> listItem) throws Exception{
        
        
        try{
            
            for (Item it : listItem) {
                String q = ("INSERT INTO gl_posting_distribute(tarikh,year,period,coacode,coadesc,loclevel,loccode,locdesc,satype,sacode,sadesc,remark,novoucher,source,debit,credit,postdate) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                PreparedStatement ps = log.getCon().prepareStatement(q);
                ps.setString(1, it.getTarikh());
                ps.setString(2, it.getYear());
                ps.setString(3, it.getPeriod());
                ps.setString(4, it.getCoacode());
                ps.setString(5, it.getCoadesc());
                ps.setString(6, it.getLoclevel());
                ps.setString(7, it.getLoccode());
                ps.setString(8, it.getLocdesc());
                ps.setString(9, it.getSatype());
                ps.setString(10, it.getSacode());
                ps.setString(11, it.getSadesc());
                ps.setString(12, it.getRemark());
                ps.setString(13, it.getNovoucher());
                ps.setString(14, it.getSource());
                ps.setDouble(15, it.getDebit());
                ps.setDouble(16, it.getCredit());
                ps.setString(17, AccountingPeriod.getCurrentTimeStamp());
Logger.getLogger(DistributeDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
                ps.executeUpdate();
                
                ps.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
    
    private static void updateVoucherStatus(LoginProfile log, String voucherno, String vtype) throws Exception, SQLException{
        
        String query = "";
        if(vtype.equals("JV")){
            query = "UPDATE gl_jv SET postflag = 'posted', postdate = ? where JVrefno = '"+voucherno+"'";
        }else if(vtype.equals("SNV")){
            query = "UPDATE sl_inv SET postflag = 'posted', postdate = ? where invref = '"+voucherno+"'";
        }else if(vtype.equals("OR")){
            query = "UPDATE cb_official SET post = 'posted', postdate = ? where refer = '"+voucherno+"'";
        }else if(vtype.equals("PV")){
            query = "UPDATE cb_payvoucher SET post = 'posted', tarikhpost = ? where refer = '"+voucherno+"'";
        }else if(vtype.equals("PNV")){
            query = "UPDATE ap_inv SET postflag = 'posted', postdate = ? where invrefno = '"+voucherno+"'";
        }else if(vtype.equals("DN")){
            query = "UPDATE gl_debitnote SET postflag = 'posted', postdate = ? where noteno = '"+voucherno+"'";
        }else if(vtype.equals("CN")){
            query = "UPDATE gl_creditnote SET postflag = 'posted', postdate = ? where noteno = '"+voucherno+"'";
        }else if(vtype.equals("DNR")){
            query = "UPDATE ar_debitnote SET postflag = 'posted', postdate = ? where noteno = '"+voucherno+"'";
        }else if(vtype.equals("CNR")){
            query = "UPDATE ar_creditnote SET postflag = 'posted', postdate = ? where noteno = '"+voucherno+"'";
        }
        
        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.setString(1, AccountingPeriod.getCurrentTimeStamp());
        Logger.getLogger(DistributeDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
        ps.executeUpdate();
        ps.close();
        
    }
    
    
    
}
