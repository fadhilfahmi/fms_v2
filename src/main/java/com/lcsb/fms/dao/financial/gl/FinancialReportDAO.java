/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.dao.financial.gl;

import com.lcsb.fms.util.model.Module;

/**
 *
 * @author Dell
 */
public class FinancialReportDAO {
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("020112");
        mod.setModuleDesc("Financial Report");
        mod.setMainTable("");
        mod.setReferID_Master("");
        return mod;
         
    }
    
}
