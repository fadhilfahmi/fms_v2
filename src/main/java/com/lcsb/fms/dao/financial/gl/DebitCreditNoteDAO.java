/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.gl;

import com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO;
import com.lcsb.fms.financial.gl.post.Item;
import com.lcsb.fms.financial.gl.post.Master;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.ar.SalesInvoice;
import com.lcsb.fms.model.financial.gl.GLAccCredit;
import com.lcsb.fms.model.financial.gl.GLAccDebit;
import com.lcsb.fms.model.financial.gl.GLCreditNote;
import com.lcsb.fms.model.financial.gl.GLDebitNote;
import com.lcsb.fms.model.financial.gl.GLItemCreditNote;
import com.lcsb.fms.model.financial.gl.GLItemDebitNote;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.dao.EstateDAO;
import com.lcsb.fms.util.model.Estate;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class DebitCreditNoteDAO {

    public static String generateDebitCredit(Master mt, LoginProfile log) throws Exception, SQLException {

        boolean dn = false;
        boolean cn = false;

        String noteno = "none";

        if (mt.getVtype().equals("DN") || mt.getVtype().equals("CN")) {//to avoid debit/credit note created when debit/credit note itself posted
        } else {

            List<Item> listAll = (List<Item>) mt.getListItem();
            ArrayList<String> totalSuspenseCode = new ArrayList<String>();
            for (Item a : listAll) {//listing to get all detail/item

                //Logger.getLogger(DebitCreditNoteDAO.class.getName()).log(Level.INFO, "coa=={0}", a.getCoacode());
                if (checkSuspence(log, a.getCoacode())) {
                    if (totalSuspenseCode.size() == 0) {
                        //Logger.getLogger(DebitCreditNoteDAO.class.getName()).log(Level.INFO, "firstinsert=={0}", a.getCoacode());
                        totalSuspenseCode.add(a.getCoacode());
                    }

                    int x = 0;
                    for (int i = 0; i < totalSuspenseCode.size(); i++) {
                        if (totalSuspenseCode.get(i).equals(a.getCoacode())) {
                            x++;

                        }
                    }

                    //Logger.getLogger(DebitCreditNoteDAO.class.getName()).log(Level.INFO, "2ndrow=={0}", x);
                    if (x == 0) {
                        totalSuspenseCode.add(a.getCoacode());
                    }
                    //Logger.getLogger(DebitCreditNoteDAO.class.getName()).log(Level.INFO, "3rdrow=={0}", x);

                    if ((a.getDebit() > 0 || a.getDebit() < 0) && a.getCredit() == 0.0) {
                        dn = true;
                    } else if ((a.getCredit() > 0 || a.getCredit() < 0) && a.getDebit() == 0.0) {//for credit note
                        cn = true;
                    }

                    //Logger.getLogger(DebitCreditNoteDAO.class.getName()).log(Level.INFO, "4throw=={0}", x);
                }

                //Logger.getLogger(DebitCreditNoteDAO.class.getName()).log(Level.INFO, "coaend=={0}", a.getCoacode());
            }

            //Logger.getLogger(DebitCreditNoteDAO.class.getName()).log(Level.INFO, "@@{0}-----", totalSuspenseCode);
            if (dn) {

                for (int i = 0; i < totalSuspenseCode.size(); i++) {
                    noteno = generateDebit(mt, log, totalSuspenseCode.get(i));
                    LiveGLDAO.reloadGL(log, noteno, "DN");
                }

            }
            if (cn) {
                for (int i = 0; i < totalSuspenseCode.size(); i++) {
                    noteno = generateCredit(mt, log, totalSuspenseCode.get(i));
                    LiveGLDAO.reloadGL(log, noteno, "CN");
                }
            }
        }

        return noteno;
    }

    private static String generateDebit(Master mt, LoginProfile log, String code) throws Exception, SQLException {

        List<Item> listAll = (List<Item>) mt.getListItem();

        GLDebitNote dn = new GLDebitNote();
        GLAccDebit ad = new GLAccDebit();

        double sumAmount = 0.00;

        for (Item a : listAll) {//listing to get all detail/item

            if (code.equals(a.getCoacode())) {

                if ((a.getDebit() > 0 || a.getDebit() < 0) && a.getCredit() == 0.0) {

                    sumAmount += a.getDebit();
                    dn.setReceivercode(EstateDAO.getEstateInfo(log, a.getCoacode(), "hqsuspence").getEstatecode());
                    dn.setReceivername(EstateDAO.getEstateInfo(log, a.getCoacode(), "hqsuspence").getEstatedescp());
                    dn.setAcccode(EstateDAO.getEstateInfo(log, a.getCoacode(), "hqsuspence").getHqcurrent());
                    dn.setAccdesc(EstateDAO.getEstateInfo(log, a.getCoacode(), "hqsuspence").getHqcurrentdescp());
                    dn.setRemark("Charge to " + EstateDAO.getEstateInfo(log, a.getCoacode(), "hqsuspence").getEstatecode() + " - " + EstateDAO.getEstateInfo(log, a.getCoacode(), "hqsuspence").getEstatedescp() + " for " + mt.getNovoucher());

                    ad.setAcccode(a.getCoacode());
                    ad.setAccdesc(a.getCoadesc());
                    ad.setLoccode(a.getLoccode());
                    ad.setLocdesc(a.getLocdesc());
                    ad.setLoclevel("Company");

                }

            }

        }

        dn.setEstcode(mt.getEstatecode());
        dn.setEstname(mt.getEstatename());
        dn.setNotedate(AccountingPeriod.convertStringtoDate(mt.getTarikh()));
        dn.setPeriod(mt.getPeriod());
        dn.setPreparedate(AccountingPeriod.getCurrentTimeStamp());
        dn.setPrepareid(log.getUserID());
        dn.setPreparename(log.getFullname());
        dn.setSacode("00");
        dn.setSadesc("Not Applicable");
        dn.setSatype("None");
        dn.setTotal(sumAmount);
        dn.setYear(mt.getYear());
        dn.setRefernoteno(mt.getNovoucher());

        String noteno = saveMainDN(log, dn);

        ad.setAmount(sumAmount);
        ad.setNoteno(noteno);
        ad.setRemark("Clear suspend for " + mt.getNovoucher());
        ad.setSacode("00");
        ad.setSadesc("Not Applicable");
        ad.setSatype("None");

        saveAccDebit(log, ad);

        for (Item a : listAll) {//listing to get all detail/item

            if (code.equals(a.getCoacode())) {

                if ((a.getDebit() > 0 || a.getDebit() < 0) && a.getCredit() == 0.0) {

                    GLItemDebitNote idn = new GLItemDebitNote();

                    idn.setAmount(a.getDebit());
                    idn.setDate(AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()));
                    idn.setDescp(a.getRemark());
                    idn.setNoteno(noteno);
                    idn.setQuantity(0);
                    idn.setReference(mt.getNovoucher());
                    idn.setUnitprice(0);

                    saveItemDebit(log, idn);

                }

            }

        }

        return noteno;

    }

    private static String generateCredit(Master mt, LoginProfile log, String code) throws Exception, SQLException {

        List<Item> listAll = (List<Item>) mt.getListItem();

        GLCreditNote dn = new GLCreditNote();
        GLAccCredit ad = new GLAccCredit();

        double sumAmount = 0.00;

        for (Item a : listAll) {//listing to get all detail/item

            if (code.equals(a.getCoacode())) {

                if ((a.getCredit() > 0 || a.getCredit() < 0) && a.getDebit() == 0.0) {//for credit note

                    sumAmount += a.getCredit();
                    //sumAmount += GeneralTerm.PrecisionDouble(a.getCredit());
                    Logger.getLogger(DebitCreditNoteDAO.class.getName()).log(Level.INFO, "credit=={0}", a.getCredit());
                    dn.setReceivercode(EstateDAO.getEstateInfo(log, a.getCoacode(), "hqsuspence").getEstatecode());
                    dn.setReceivername(EstateDAO.getEstateInfo(log, a.getCoacode(), "hqsuspence").getEstatedescp());
                    dn.setAcccode(EstateDAO.getEstateInfo(log, a.getCoacode(), "hqsuspence").getHqcurrent());
                    dn.setAccdesc(EstateDAO.getEstateInfo(log, a.getCoacode(), "hqsuspence").getHqcurrentdescp());
                    dn.setRemark("Charge to " + EstateDAO.getEstateInfo(log, a.getCoacode(), "hqsuspence").getEstatecode() + " - " + EstateDAO.getEstateInfo(log, a.getCoacode(), "hqsuspence").getEstatedescp() + " for " + mt.getNovoucher());

                    ad.setAcccode(a.getCoacode());
                    ad.setAccdesc(a.getCoadesc());
                    ad.setLoccode(a.getLoccode());
                    ad.setLocdesc(a.getLocdesc());
                    ad.setLoclevel("Company");

                }

            }

        }

        dn.setEstcode(mt.getEstatecode());
        dn.setEstname(mt.getEstatename());
        dn.setNotedate(AccountingPeriod.convertStringtoDate(mt.getTarikh()));
        dn.setPeriod(mt.getPeriod());
        dn.setPreparedate(AccountingPeriod.getCurrentTimeStamp());
        dn.setPrepareid(log.getUserID());
        dn.setPreparename(log.getFullname());
        dn.setSacode("00");
        dn.setSadesc("Not Applicable");
        dn.setSatype("None");
        dn.setTotal(sumAmount);
        dn.setYear(mt.getYear());
        dn.setRefernoteno(mt.getNovoucher());

        String noteno = saveMainCN(log, dn);

        ad.setAmount(sumAmount);
        ad.setNoteno(noteno);
        ad.setRemark("Clear suspend for " + mt.getNovoucher());
        ad.setSacode("00");
        ad.setSadesc("Not Applicable");
        ad.setSatype("None");

        saveAccCredit(log, ad);

        for (Item a : listAll) {//listing to get all detail/item

            if (code.equals(a.getCoacode())) {

                if ((a.getCredit() > 0 || a.getCredit() < 0) && a.getDebit() == 0.0) {//for credit note

                    GLItemCreditNote idn = new GLItemCreditNote();

                    //idn.setAmount(GeneralTerm.PrecisionDouble(a.getCredit()));
                    idn.setAmount(a.getCredit());
                    idn.setDate(AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()));
                    idn.setDescp(changeRemark(log, a.getRemark(), mt.getNovoucher()));
                    idn.setNoteno(noteno);
                    idn.setQuantity(0);
                    idn.setReference(mt.getNovoucher());
                    idn.setUnitprice(0);

                    saveItemCredit(log, idn);

                }

            }

        }

        return noteno;

    }

    public static boolean checkSuspence(LoginProfile log, String code) throws Exception, SQLException {

        boolean x = false;
        String suspenceCode = "";

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select suspence_code from sys_acc_config");
        rs = stmt.executeQuery();
        if (rs.next()) {
            suspenceCode = rs.getString(1);
        }

        if (code.length() > 5) {
            if (code.substring(0, 6).equals(suspenceCode)) {
                x = true;
            }
        }

        if (code.equals("13710290")) {
            x = false;
        }

        return x;

    }

    public static String saveMainDN(LoginProfile log, GLDebitNote item) throws Exception, SQLException {

        String newrefer = AutoGenerate.getReferenceNox(log, "noteno", "gl_debitnote", "DNE", item.getEstcode(), item.getYear(), item.getPeriod());

        try {

            String q = ("insert into gl_debitnote(noteno,notedate,year,period,remark,estcode,estname,acccode,accdesc,total,receivercode,receivername,prepareid,preparename,preparedate,refernoteno) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, newrefer);
            ps.setDate(2, (item.getNotedate() != null) ? new java.sql.Date(item.getNotedate().getTime()) : null);
            ps.setString(3, item.getYear());
            ps.setString(4, item.getPeriod());
            ps.setString(5, item.getRemark());
            ps.setString(6, item.getEstcode());
            ps.setString(7, item.getEstname());
            ps.setString(8, item.getAcccode());
            ps.setString(9, item.getAccdesc());
            ps.setDouble(10, item.getTotal());
            ps.setString(11, item.getReceivercode());
            ps.setString(12, item.getReceivername());
            ps.setString(13, item.getPrepareid());
            ps.setString(14, item.getPreparename());
            ps.setString(15, item.getPreparedate());
            ps.setString(16, item.getRefernoteno());

            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newrefer;

    }

    public static void saveAccDebit(LoginProfile log, GLAccDebit item) throws Exception, SQLException {

        try {

            String q = ("insert into gl_accdebit(noteno,acccode,accdesc,loclevel,loccode,locdesc,amount,remark) values (?,?,?,?,?,?,?,?)");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getNoteno());
            ps.setString(2, item.getAcccode());
            ps.setString(3, item.getAccdesc());
            ps.setString(4, item.getLoclevel());
            ps.setString(5, item.getLoccode());
            ps.setString(6, item.getLocdesc());
            ps.setDouble(7, item.getAmount());
            ps.setString(8, item.getRemark());

            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void saveItemDebit(LoginProfile log, GLItemDebitNote item) throws Exception, SQLException {

        try {

            String q = ("insert into gl_itemdebitnote(noteno,descp,amount,reference,date) values (?,?,?,?,?)");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getNoteno());
            ps.setString(2, item.getDescp());
            ps.setDouble(3, item.getAmount());
            ps.setString(4, item.getReference());
            ps.setDate(5, (item.getDate() != null) ? new java.sql.Date(item.getDate().getTime()) : null);

            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static String saveMainCN(LoginProfile log, GLCreditNote item) throws Exception, SQLException {

        String newrefer = AutoGenerate.getReferenceNox(log, "noteno", "gl_creditnote", "CNE", item.getEstcode(), item.getYear(), item.getPeriod());

        try {

            String q = ("insert into gl_creditnote(noteno,notedate,year,period,remark,estcode,estname,acccode,accdesc,total,receivercode,receivername,prepareid,preparename,preparedate,refernoteno) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, newrefer);
            ps.setDate(2, (item.getNotedate() != null) ? new java.sql.Date(item.getNotedate().getTime()) : null);
            ps.setString(3, item.getYear());
            ps.setString(4, item.getPeriod());
            ps.setString(5, item.getRemark());
            ps.setString(6, item.getEstcode());
            ps.setString(7, item.getEstname());
            ps.setString(8, item.getAcccode());
            ps.setString(9, item.getAccdesc());
            ps.setDouble(10, item.getTotal());
            ps.setString(11, item.getReceivercode());
            ps.setString(12, item.getReceivername());
            ps.setString(13, item.getPrepareid());
            ps.setString(14, item.getPreparename());
            ps.setString(15, item.getPreparedate());
            ps.setString(16, item.getRefernoteno());

            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newrefer;

    }

    public static void saveAccCredit(LoginProfile log, GLAccCredit item) throws Exception, SQLException {

        try {

            String q = ("insert into gl_acccredit(noteno,acccode,accdesc,loclevel,loccode,locdesc,amount,remark) values (?,?,?,?,?,?,?,?)");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getNoteno());
            ps.setString(2, item.getAcccode());
            ps.setString(3, item.getAccdesc());
            ps.setString(4, item.getLoclevel());
            ps.setString(5, item.getLoccode());
            ps.setString(6, item.getLocdesc());
            ps.setDouble(7, item.getAmount());
            ps.setString(8, item.getRemark());

            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void saveItemCredit(LoginProfile log, GLItemCreditNote item) throws Exception, SQLException {

        try {

            String q = ("insert into gl_itemcreditnote(noteno,descp,amount,reference,date) values (?,?,?,?,?)");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getNoteno());
            ps.setString(2, item.getDescp());
            ps.setDouble(3, item.getAmount());
            ps.setString(4, item.getReference());
            ps.setDate(5, (item.getDate() != null) ? new java.sql.Date(item.getDate().getTime()) : null);

            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static String getTypeNote(String noteno) {

        String type = "";

        if (noteno.substring(0, 3).equals("DNE")) {
            type = "Debit Note";
        } else if (noteno.substring(0, 3).equals("CNE")) {
            type = "Credit Note";
        }

        return type;
    }

    public static String checkDCNoteExist(LoginProfile log, String id) throws Exception {

        String dc = "";

        try {

            ResultSet rs = null;
            ResultSet rs1 = null;

            PreparedStatement stmt = log.getCon().prepareStatement("select * from gl_debitnote where refernoteno=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                dc = "<span class=\"label label-danger\">DN</span>";
            }

            PreparedStatement stmt1 = log.getCon().prepareStatement("select * from gl_creditnote where refernoteno=?");
            stmt1.setString(1, id);
            rs1 = stmt1.executeQuery();
            if (rs1.next()) {
                dc = "<span class=\"label label-info\">CN</span>";
            }

            rs.close();
            rs1.close();
            stmt.close();
            stmt1.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return dc;

    }

    public static boolean hasDebitCreditNote(LoginProfile log, String id) throws Exception {

        boolean dc = false;

        try {

            ResultSet rs = null;
            ResultSet rs1 = null;

            PreparedStatement stmt = log.getCon().prepareStatement("select * from gl_debitnote where refernoteno=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                dc = true;
            }

            PreparedStatement stmt1 = log.getCon().prepareStatement("select * from gl_creditnote where refernoteno=?");
            stmt1.setString(1, id);
            rs1 = stmt1.executeQuery();
            if (rs1.next()) {
                dc = true;
            }

            rs.close();
            rs1.close();
            stmt.close();
            stmt1.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return dc;

    }

    public static ArrayList<String> getDebitCreditNote(LoginProfile log, String id) throws Exception {

        ArrayList<String> r = new ArrayList<String>();

        try {
            ResultSet rs = null;
            ResultSet rs1 = null;

            PreparedStatement stmt = log.getCon().prepareStatement("select * from gl_debitnote where refernoteno=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            while (rs.next()) {

                r.add(rs.getString("noteno"));
            }

            PreparedStatement stmt1 = log.getCon().prepareStatement("select * from gl_creditnote where refernoteno=?");
            stmt1.setString(1, id);
            rs1 = stmt1.executeQuery();
            while (rs1.next()) {
                r.add(rs1.getString("noteno"));
            }

            rs.close();
            rs1.close();
            stmt.close();
            stmt1.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return r;

    }

    public static String getDebitCreditNoteSingle(LoginProfile log, String id) throws Exception {

        String dc = "";

        try {
            ResultSet rs = null;
            ResultSet rs1 = null;

            PreparedStatement stmt = log.getCon().prepareStatement("select * from gl_debitnote where refernoteno=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                dc = rs.getString("noteno");
            }

            PreparedStatement stmt1 = log.getCon().prepareStatement("select * from gl_creditnote where refernoteno=?");
            stmt1.setString(1, id);
            rs1 = stmt1.executeQuery();
            if (rs1.next()) {
                dc = rs1.getString("noteno");
            }

            rs.close();
            rs1.close();
            stmt.close();
            stmt1.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return dc;

    }

    public static String getDebitCreditNoteLink(LoginProfile log, String id, String moduleid) throws Exception {

        String link = "";

        ArrayList<String> list = DebitCreditNoteDAO.getDebitCreditNote(log, id);

        for (int i = 0; i < list.size(); i++) {

            if (i == list.size() - 1) {
                link += "<a title=\"" + list.get(i) + "\" id=\"" + id + "\" class=\"thelink gotonote\" name=\"" + moduleid + "\">" + list.get(i) + " </a>";
            } else {
                link += "<a title=\"" + list.get(i) + "\" id=\"" + id + "\" class=\"thelink gotonote\" name=\"" + moduleid + "\">" + list.get(i) + " </a>, ";
            }

        }

        return link;
    }

    private static String changeRemark(LoginProfile log, String remark, String referno) throws Exception {

        String newRemark = remark;
        String toChange = "to you";

        if (referno.substring(0, 3).equals("INV")) {
            SalesInvoice si = (SalesInvoice) SalesInvoiceDAO.getINV(log, referno);

            if (si.getProdcode().equals("03") && remark.toLowerCase().contains(toChange.toLowerCase())) {//prodcode 03 = FFB

                newRemark = remark.replaceAll("YOU", si.getBname());

            }
        }

        return newRemark;

    }

}
