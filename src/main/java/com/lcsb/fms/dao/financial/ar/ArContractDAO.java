/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.ar;

import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.ar.ArContractAgree;
import com.lcsb.fms.model.financial.ar.ArContractInfo;
import com.lcsb.fms.model.financial.ar.ArContractMaster;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class ArContractDAO {
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("020902");
        mod.setModuleDesc("Contract");
        mod.setMainTable("sl_contractor_info");
        mod.setReferID_Master("code");
        return mod;
         
    }
    
    public static List<ListTable> tableList(){
        
        String column_names[] = {"code","descp"};
        String title_name[] = {"Code","Description"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
    
    public static int getDiffCount(LoginProfile log, String code)throws Exception {
        Connection con_local = log.getCon();
        Connection con_remote = ConnectionUtil.createMillConnection(code);
        
        int x = 0;
        int y = 0;
        int z = 0;
        Statement stment = con_local.createStatement();   
        ResultSet setent =stment.executeQuery("select count(*) as cnt from sl_contractor_info");
       
        if(setent.next()) {
            x=setent.getInt("cnt");
        }
        
        Statement stmentx = con_remote.createStatement();   
        ResultSet setentx =stmentx.executeQuery("select count(*) as cnt from sl_contractor_info");
       
        if(setentx.next()) {
            y=setentx.getInt("cnt");
        }
        
        stment.close();
        setent.close();
        stmentx.close();
        setentx.close();
        
        con_remote.close();
        
        z = y - x;
        
        if(z < 0){
            z = 0;
        }
        
        return z;
    }
    
    public static int getDiffCountAgreement(LoginProfile log, String code, String buyercode)throws Exception {
        Connection con_local = log.getCon();
        Connection con_remote = ConnectionUtil.createMillConnection(code);
        
        int x = 0;
        int y = 0;
        int z = 0;
        
        try{
            ResultSet rs = null;
            PreparedStatement stmt = con_local.prepareStatement("select count(*) as cnt from sl_contract_aggree where buyercode = ?");
            stmt.setString(1, buyercode);
            rs = stmt.executeQuery();
            if (rs.next()) {
                x=rs.getInt("cnt");
            }
            Logger.getLogger(ArContractDAO.class.getName()).log(Level.INFO, "-------mmmmmmmmmmmmmmmmmmmmmmmmm----"+x);
            ResultSet rs1 = null;
            PreparedStatement stmt1 = con_remote.prepareStatement("select count(*) as cnt from sl_contract_aggree where buyercode = ?");
            stmt1.setString(1, buyercode);
            rs1 = stmt1.executeQuery();
            if (rs1.next()) {
                y=rs1.getInt("cnt");
            }



            stmt.close();
            stmt1.close();
            rs.close();
            rs1.close();
        
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        con_remote.close();
        
        z = y - x;
        
        if(z < 0){
            z = 0;
        }
        return z;
    }
    
    private static ArContractInfo getContractInfo(ResultSet rs) throws SQLException {
        ArContractInfo ar = new ArContractInfo();
       
        ar.setActcode(rs.getString("actcode"));
        ar.setActdescp(rs.getString("actdescp"));
        ar.setActive(rs.getString("active"));
        ar.setAddress(rs.getString("address"));
        ar.setCode(rs.getString("code"));
        ar.setDescp(rs.getString("descp"));
        ar.setLoccode(rs.getString("loccode"));
        ar.setLocname(rs.getString("locname"));
        
        return ar;
    }
    
    private static ArContractAgree getAgreeList(ResultSet rs) throws SQLException {
        ArContractAgree ar = new ArContractAgree();
       
        ar.setActive(rs.getString("active"));
        ar.setBrokercd(rs.getString("brokercd"));
        ar.setBrokerde(rs.getString("brokerde"));
        ar.setBrokernum(rs.getString("brokernum"));
        ar.setBuyercode(rs.getString("buyercode"));
        ar.setComacc(rs.getString("comacc"));
        ar.setComcd(rs.getString("comcd"));
        ar.setComde(rs.getString("comde"));
        ar.setDate(rs.getDate("date"));
        ar.setDestination(rs.getString("destination"));
        ar.setDmonth(rs.getString("dmonth"));
        ar.setDyear(rs.getString("dyear"));
        ar.setEffectivedate(rs.getDate("effectivedate"));
        ar.setLoccode(rs.getString("loccode"));
        ar.setLocname(rs.getString("locname"));
        ar.setMpob(rs.getString("mpob"));
        ar.setNo(rs.getString("no"));
        ar.setPrice(rs.getDouble("price"));
        ar.setQty(rs.getDouble("qty"));
        ar.setSubgrp(rs.getString("subgrp"));
        ar.setId(rs.getInt("id"));
      
        
        return ar;
    }
    
    private static ArContractAgree getAgreeListMill(ResultSet rs) throws SQLException {
        ArContractAgree ar = new ArContractAgree();
       
        ar.setActive(rs.getString("active"));
        ar.setBrokercd(rs.getString("brokercd"));
        ar.setBrokerde(rs.getString("brokerde"));
        ar.setBrokernum(rs.getString("brokernum"));
        ar.setBuyercode(rs.getString("buyercode"));
        ar.setComacc(rs.getString("comacc"));
        ar.setComcd(rs.getString("comcd"));
        ar.setComde(rs.getString("comde"));
        ar.setDate(rs.getDate("date"));
        ar.setDestination(rs.getString("destination"));
        ar.setDmonth(rs.getString("dmonth"));
        ar.setDyear(rs.getString("dyear"));
        ar.setEffectivedate(rs.getDate("effectivedate"));
        ar.setLoccode(rs.getString("loccode"));
        ar.setLocname(rs.getString("locname"));
        ar.setMpob(rs.getString("mpob"));
        ar.setNo(rs.getString("no"));
        ar.setPrice(rs.getDouble("price"));
        ar.setQty(rs.getDouble("qty"));
        ar.setSubgrp(rs.getString("subgrp"));
        
        return ar;
    }
    
    public static void saveNewAgreement(LoginProfile log, String code) throws Exception{
        saveNewMaster(log, getMaster(log, code));
    }
    
    public static void saveNewAgreementListOnly(LoginProfile log, String code, String buyercode) throws Exception{
        Connection con_remote = ConnectionUtil.createMillConnection(code);
        saveNewAgreeList(log, getNewListAgree(log, code, buyercode, con_remote));
        con_remote.close();
    }
    
    public static void saveNewAgreementListAll(LoginProfile log, Connection con_remote) throws Exception{
        saveNewAgreeList(log, getNewListAgreeAll(log, con_remote));
    }
    
    private static ArContractMaster getMaster(LoginProfile log, String code) throws Exception{
        
        ArContractMaster m = new ArContractMaster();
        
        Connection con_local = log.getCon();
        Connection con_remote = ConnectionUtil.createMillConnection(code);
        
        List<ArContractInfo> ar = new ArrayList();
        
        try{
            String millCode = "";
            Statement stmentx = con_remote.createStatement();   
            ResultSet rs =stmentx.executeQuery("select * from sl_contractor_info");

            while(rs.next()) {
                millCode=rs.getString("code");

                if(!checkExist(log, millCode)){

                    ArContractInfo ai = new ArContractInfo();
                    List<ArContractAgree> ag = new ArrayList();

                    ai = getContractInfo(rs);

                    Statement stmt2 = con_remote.createStatement();   
                    ResultSet rs2 =stmt2.executeQuery("select * from sl_contract_aggree where buyercode = '"+rs.getString("code")+"'");

                    while(rs2.next()) {
                        ag.add(getAgreeListMill(rs2));

                        ai.setListAgree(ag);


                    }
                    ar.add(ai);
                    stmt2.close();
                    rs2.close();

                    m.setListMaster(ar);
                }

            }
            
            stmentx.close();
            rs.close();
        
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        con_remote.close();
        return m;
    }
    
    private static boolean checkExist(LoginProfile log, String code) throws Exception{
        
        boolean exist = false;
        String checkcode = "";
        int x = 0;
        
        ResultSet rs = null;
        Connection conet = log.getCon();
        PreparedStatement stmt = conet.prepareStatement("select * from sl_contractor_info where code=?");
        stmt.setString(1, code);
        rs = stmt.executeQuery();
        if (rs.next()) {
            checkcode = rs.getString("code");
            x++;
        }
        
        if(x > 0){
            exist = true;
        }

        return exist;
    }
    
    private static boolean checkExistAgree(LoginProfile log, String code) throws Exception{
        
        boolean exist = false;
        String checkcode = "";
        int x = 0;
        
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from sl_contract_aggree where no=?");
        stmt.setString(1, code);
        rs = stmt.executeQuery();
        if (rs.next()) {
            checkcode = rs.getString("no");
            x++;
        }
        
        if(x > 0){
            exist = true;
        }

        return exist;
    }
    
    public static void saveNewMaster(LoginProfile log, ArContractMaster master) throws Exception{
        
        
        try{
            List<ArContractInfo> listMaster = (List<ArContractInfo>) master.getListMaster();
            for (ArContractInfo j : listMaster) {
                String q = ("insert into sl_contractor_info(actcode,actdescp,active,address,code,descp,loccode,locname) values (?,?,?,?,?,?,?,?)");
                PreparedStatement ps = log.getCon().prepareStatement(q);
                ps.setString(1, j.getActcode());
                ps.setString(2, j.getActdescp());
                ps.setString(3, j.getActive());
                ps.setString(4, j.getAddress());
                ps.setString(5, j.getCode());
                ps.setString(6, j.getDescp());
                ps.setString(7, j.getLoccode());
                ps.setString(8, j.getLocname());


                ps.executeUpdate();
                Logger.getLogger(ArContractDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
                ps.close();
                
                try{
                List<ArContractAgree> listAgree = (List<ArContractAgree>) j.getListAgree();
                for (ArContractAgree i : listAgree) {
                    
                    String q1 = ("insert into sl_contract_aggree(active,brokercd,brokerde,brokernum,buyercode,comacc,comcd,comde,destination,dmonth,dyear,loccode,locname,mpob,no,subgrp,price,qty,date,effectivedate) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                    PreparedStatement ps1 = log.getCon().prepareStatement(q1);
                    ps1.setString(1, i.getActive());
                    ps1.setString(2, i.getBrokercd());
                    ps1.setString(3, i.getBrokerde());
                    ps1.setString(4, i.getBrokernum());
                    ps1.setString(5, i.getBuyercode());
                    ps1.setString(6, i.getComacc());
                    ps1.setString(7, i.getComcd());
                    ps1.setString(8, i.getComde());
                    ps1.setString(9, i.getDestination());
                    ps1.setString(10, i.getDmonth());
                    ps1.setString(11, i.getDyear());
                    ps1.setString(12, i.getLoccode());
                    ps1.setString(13, i.getLocname());
                    ps1.setString(14, i.getMpob());
                    ps1.setString(15, i.getNo());
                    ps1.setString(16, i.getSubgrp());
                    ps1.setDouble(17, i.getPrice());
                    ps1.setDouble(18, i.getQty());
                    ps1.setDate(19, (i.getDate()!=null)?new java.sql.Date(i.getDate().getTime()):null);
                    ps1.setDate(20, (i.getEffectivedate()!=null)?new java.sql.Date(i.getEffectivedate().getTime()):null);

                    Logger.getLogger(ArContractDAO.class.getName()).log(Level.INFO, String.valueOf(ps1));
                    ps1.executeUpdate();
                    ps1.close();
                        

                }
                
                } catch (Exception e) {
                        e.printStackTrace();
                        Logger.getLogger(ArContractDAO.class.getName()).log(Level.INFO, String.valueOf(e));
                }
            }
            

       } catch (Exception e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void saveNew(LoginProfile log, ArContractInfo item) throws Exception{
        
        try{
            String q = ("insert into sl_contractor_info(actcode,actdescp,active,address,code,descp,loccode,locname) values (?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getActcode());
            ps.setString(2, item.getActdescp());
            ps.setString(3, item.getActive());
            ps.setString(4, item.getAddress());
            ps.setString(5, item.getCode());
            ps.setString(6, item.getDescp());
            ps.setString(7, item.getLoccode());
            ps.setString(8, item.getLocname());
           
            
            ps.executeUpdate();
            Logger.getLogger(ArContractDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();
            

       } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
        return new java.sql.Date(date.getTime());
    }
    
    public static java.util.Date convertSqlDateToJavaDate(java.sql.Date date) {
        
        //Date date = ...; // wherever you get this from
 
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String text = df.format(date);

        System.out.println("The date is: " + text);
        //javaDate = new Date(sqlDate.getTime());
        return new java.util.Date(date.getTime());
    }
    
    public static void saveNewAgreeList(LoginProfile log, List<ArContractAgree> listAgree) throws Exception{
        
        try{
            for (ArContractAgree i : listAgree) {
                
                 
                    String q1 = ("insert into sl_contract_aggree(active,brokercd,brokerde,brokernum,buyercode,comacc,comcd,comde,destination,dmonth,dyear,loccode,locname,mpob,no,subgrp,price,qty,date,effectivedate) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                    PreparedStatement ps1 = log.getCon().prepareStatement(q1);
                    ps1.setString(1, i.getActive());
                    ps1.setString(2, i.getBrokercd());
                    ps1.setString(3, i.getBrokerde());
                    ps1.setString(4, i.getBrokernum());
                    ps1.setString(5, i.getBuyercode());
                    ps1.setString(6, i.getComacc());
                    ps1.setString(7, i.getComcd());
                    ps1.setString(8, i.getComde());
                    ps1.setString(9, i.getDestination());
                    ps1.setString(10, i.getDmonth());
                    ps1.setString(11, i.getDyear());
                    ps1.setString(12, i.getLoccode());
                    ps1.setString(13, i.getLocname());
                    ps1.setString(14, i.getMpob());
                    ps1.setString(15, i.getNo());
                    ps1.setString(16, i.getSubgrp());
                    ps1.setDouble(17, i.getPrice());
                    ps1.setDouble(18, i.getQty());
                    ps1.setDate(19, (i.getDate()!=null)?new java.sql.Date(i.getDate().getTime()):null);
                    ps1.setDate(20, (i.getEffectivedate()!=null)?new java.sql.Date(i.getEffectivedate().getTime()):null);

                ps1.executeUpdate();
                ps1.close();
                
            }

       } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static ArContractInfo getInfo(LoginProfile log, String refno) throws SQLException, Exception {
        ArContractInfo c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from sl_contractor_info where code=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getContractInfo(rs);
        }

        
        return c;
    }
    
    public static ArContractAgree getEachAgree(LoginProfile log, String refno) throws SQLException, Exception {
        ArContractAgree c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from sl_contract_aggree where id=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getAgreeList(rs);
        }
        
        return c;
    }
    
    public static ArContractAgree getEachAgreeUsingContractNo(LoginProfile log, String refno) throws SQLException, Exception {
        ArContractAgree c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from sl_contract_aggree where no=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getAgreeList(rs);
        }
        
        return c;
    }
    
    public static List<ArContractAgree> getAllAgreement(LoginProfile log, String refno) throws Exception {
        
        List<ArContractAgree> CVi;
        CVi = new ArrayList();
        
        ResultSet rs = null;
        
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from sl_contract_aggree where buyercode = ? order by date desc");
            stmt.setString(1, refno);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getAgreeList(rs));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static List<ArContractAgree> getAllAgreementByProduct(LoginProfile log, String refno, String comcode) throws Exception {
        
        List<ArContractAgree> CVi;
        CVi = new ArrayList();
        
        ResultSet rs = null;
        
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from sl_contract_aggree where buyercode = ? and comcd = ? order by date desc");
            stmt.setString(1, refno);
            stmt.setString(2, comcode);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getAgreeList(rs));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    private static List<ArContractAgree> getNewListAgree(LoginProfile log, String code, String buyercode, Connection con_remote) throws Exception{
        
        List<ArContractAgree> ar = new ArrayList();
        
        try{
            String agreeNo = "";
            Statement stmentx = con_remote.createStatement();   
            ResultSet rs =stmentx.executeQuery("select * from sl_contract_aggree where buyercode = '"+buyercode+"'");

            while(rs.next()) {
                agreeNo=rs.getString("no");

                if(!checkExistAgree(log, agreeNo)){
                    ar.add(getAgreeListMill(rs));

                }

            }
            
            stmentx.close();
            rs.close();
        
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
     
        
        return ar;
    }
    
    private static List<ArContractAgree> getNewListAgreeAll(LoginProfile log, Connection con_remote) throws Exception{
        
        List<ArContractAgree> ar = new ArrayList();
        
        try{
            String agreeNo = "";
            Statement stmentx = con_remote.createStatement();   
            ResultSet rs =stmentx.executeQuery("select * from sl_contract_aggree");

            while(rs.next()) {
                agreeNo=rs.getString("no");

                if(!checkExistAgree(log, agreeNo)){
                    ar.add(getAgreeListMill(rs));

                }

            }
            
            stmentx.close();
            rs.close();
        
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
     
        
        return ar;
    }
    
    public static double getTotalAmount(double qty, double price){
        double total = (GeneralTerm.PrecisionDouble((qty * price))) + (GeneralTerm.PrecisionDouble((6 * GeneralTerm.PrecisionDouble((qty * price)) / 100)));
        //double total = (qty * price) + (6 * (qty * price) / 100);
        Logger.getLogger(ArContractDAO.class.getName()).log(Level.INFO, "------qty-----"+GeneralTerm.PrecisionDouble(qty * price)+"----"+GeneralTerm.PrecisionDouble(6 * (qty * price) / 100));
        
        return total;
    }
    
    public static void saveNewAgree(LoginProfile log, ArContractAgree i, String estatecode, String estatename) throws Exception{
        
        String refer = "";
        
         Logger.getLogger(ArContractDAO.class.getName()).log(Level.INFO, "-----------"+i.getNo());
        
        if(i.getNo().equals("") || i.getNo().equals("null") || i.getNo()==null || i.getNo().equals(null) || i.getNo()=="null"){
            
            refer = AutoGenerate.getReferenceNo(log, "no", "sl_contract_aggree", "SAG", estatecode);
            
        }else{
            refer = i.getNo();
        }
        
        try{
                 
                    String q1 = ("insert into sl_contract_aggree(active,brokercd,brokerde,brokernum,buyercode,comacc,comcd,comde,destination,dmonth,dyear,loccode,locname,mpob,no,subgrp,price,qty,date,effectivedate,year, period, estatecode, estatename) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                    PreparedStatement ps1 = log.getCon().prepareStatement(q1);
                    ps1.setString(1, i.getActive());
                    ps1.setString(2, i.getBrokercd());
                    ps1.setString(3, i.getBrokerde());
                    ps1.setString(4, i.getBrokernum());
                    ps1.setString(5, i.getBuyercode());
                    ps1.setString(6, i.getComacc());
                    ps1.setString(7, i.getComcd());
                    ps1.setString(8, i.getComde());
                    ps1.setString(9, i.getDestination());
                    ps1.setString(10, i.getDmonth());
                    ps1.setString(11, i.getDyear());
                    ps1.setString(12, i.getLoccode());
                    ps1.setString(13, i.getLocname());
                    ps1.setString(14, i.getMpob());
                    ps1.setString(15, refer);
                    ps1.setString(16, i.getSubgrp());
                    ps1.setDouble(17, i.getPrice());
                    ps1.setDouble(18, i.getQty());
                    ps1.setDate(19, (i.getDate()!=null)?new java.sql.Date(i.getDate().getTime()):null);
                    ps1.setDate(20, (i.getEffectivedate()!=null)?new java.sql.Date(i.getEffectivedate().getTime()):null);
                    ps1.setString(21, AccountingPeriod.getCurYear(log));
                    ps1.setString(22, AccountingPeriod.getCurPeriod(log));
                    ps1.setString(23, estatecode);
                    ps1.setString(24, estatename);

                ps1.executeUpdate();
                ps1.close();
             

       } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void updateAgree(LoginProfile log, ArContractAgree i) throws Exception{
        
        
        try{
                 
                    String q1 = ("UPDATE sl_contract_aggree SET active = ?,brokercd = ?,brokerde = ?,brokernum = ?,buyercode = ?,comacc = ?,comcd = ?,comde = ?,destination = ?,dmonth = ?,dyear = ?,loccode = ?,locname = ?,mpob = ?,no = ?,subgrp = ?,price = ?,qty = ?,date = ?,effectivedate = ? WHERE id = '"+ i.getId() +"'");
                    PreparedStatement ps1 = log.getCon().prepareStatement(q1);
                    ps1.setString(1, i.getActive());
                    ps1.setString(2, i.getBrokercd());
                    ps1.setString(3, i.getBrokerde());
                    ps1.setString(4, i.getBrokernum());
                    ps1.setString(5, i.getBuyercode());
                    ps1.setString(6, i.getComacc());
                    ps1.setString(7, i.getComcd());
                    ps1.setString(8, i.getComde());
                    ps1.setString(9, i.getDestination());
                    ps1.setString(10, i.getDmonth());
                    ps1.setString(11, i.getDyear());
                    ps1.setString(12, i.getLoccode());
                    ps1.setString(13, i.getLocname());
                    ps1.setString(14, i.getMpob());
                    ps1.setString(15, i.getNo());
                    ps1.setString(16, i.getSubgrp());
                    ps1.setDouble(17, i.getPrice());
                    ps1.setDouble(18, i.getQty());
                    ps1.setDate(19, (i.getDate()!=null)?new java.sql.Date(i.getDate().getTime()):null);
                    ps1.setDate(20, (i.getEffectivedate()!=null)?new java.sql.Date(i.getEffectivedate().getTime()):null);
                ps1.executeUpdate();
                Logger.getLogger(ArContractDAO.class.getName()).log(Level.INFO, "---"+String.valueOf(ps1));
                ps1.close();
             

       } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    
    
    
    
//    public static void saveItem(SalesInvoiceItem item) throws Exception{
//        
//        Connection con_local = ConnectionUtil.getConnection();
//        Connection con_remote = ConnectionUtil.getMillConnection(code);
//        
//        try{
//            
//            Statement stment = con_local.createStatement();   
//            ResultSet setent =stment.executeQuery("select * as cnt from sl_contractor_info");
//       
//            while(setent.next()) {
//                x=setent.getInt("cnt");
//            }
//            
//            String q = ("insert into sl_inv_item(ivref,prodcode,prodname,coacode,coaname,qty,unitp,amount,unitm,remarks,loclevel,loccode,locdesc,taxcode,taxrate,taxamt,taxcoacode,taxcoadescp,ivno,satype,sacode,sadesc,refer) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
//            PreparedStatement ps = con.prepareStatement(q);
//            ps.setString(1, item.getIvref());
//            ps.setString(2, item.getProdcode());
//            ps.setString(3, item.getProdname());
//            ps.setString(4, item.getCoacode());
//            ps.setString(5, item.getCoaname());
//            ps.setDouble(6, item.getQty());
//            ps.setString(7, item.getUnitp());
//            ps.setDouble(8, item.getAmount());
//            ps.setString(9, item.getUnitm());
//            ps.setString(10, item.getRemarks());
//            ps.setString(11, item.getLoclevel());
//            ps.setString(12, item.getLoccode());
//            ps.setString(13, item.getLocdesc());
//            ps.setString(14, item.getTaxcode());
//            ps.setDouble(15, item.getTaxrate());
//            ps.setDouble(16, item.getTaxamt());
//            ps.setString(17, item.getTaxcoacode());
//            ps.setString(18, item.getTaxcoadescp());
//            ps.setString(19, AutoGenerate.getVoucherNo("sl_inv_item", "ivno"));
//            ps.setString(20, item.getSatype());
//            ps.setString(21, item.getSacode());
//            ps.setString(22, item.getSadesc());
//            ps.setString(23, AutoGenerate.getReferAddBack("sl_inv_item", "refer", item.getIvref(), "ivref"));
//           
//            ps.executeUpdate();
//            Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
//            ps.close();
//
//       } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        
//        
//    }
    
}
