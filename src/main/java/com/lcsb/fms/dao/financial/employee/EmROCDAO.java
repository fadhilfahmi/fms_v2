/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.employee;

import com.lcsb.fms.dao.setup.company.StaffDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.financial.employee.EmPayroll;
import com.lcsb.fms.model.financial.employee.EmRoc;
import com.lcsb.fms.util.dao.DataDAO;
import com.lcsb.fms.util.ext.ParseSafely;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class EmROCDAO {
    
    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("020301");
        mod.setModuleDesc("Report of Change");
        mod.setMainTable("em_roc");
        mod.setReferID_Master("refer");
        return mod;

    }

    public static List<ListTable> tableList() {

        String column_names[] = {"refer", "staffid", "staffname", "staffdesignation", "dateeffective","fileno"};
        String title_name[] = {"Refer", "Staff ID", "Name", "Designation", "Date Effective", "File No"};
        boolean boolview[] = {true, true, true, true, true,true};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            i.setBoolView(boolview[j]);
            lt.add(i);
        }

        return lt;
    }
    
    public static String saveMain(LoginProfile log, EmRoc item) throws Exception {

        String newrefer = AutoGenerate.get4digitNoWithThreeSpecialChar(log, "em_roc", "refer", "ROC");

        try {
            String q = ("insert into em_roc(refer,dateeffective,fileno,rchange,remark,staffdesignation,staffid,staffname) values (?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, newrefer);
            ps.setString(2, item.getDateeffective());
            ps.setString(3, item.getFileno());
            ps.setString(4, item.getRchange());
            ps.setString(5, item.getRemark());
            ps.setString(6, StaffDAO.getCurrentDesignation(log, item.getStaffid()).getDesignation());
            ps.setString(7, item.getStaffid());
            ps.setString(8, item.getStaffname());

            Logger.getLogger(EmROCDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(ps));
            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newrefer;

    }
    
    public static void updateMain(LoginProfile log, EmRoc item) throws Exception {

        try {
            String q = ("UPDATE em_roc SET dateeffective = ?,fileno = ?,rchange = ?,remark = ?,staffdesignation = ?,staffid = ?,staffname = ? WHERE refer = '"+item.getRefer()+"'");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getDateeffective());
            ps.setString(2, item.getFileno());
            ps.setString(3, item.getRchange());
            ps.setString(4, item.getRemark());
            ps.setString(5, StaffDAO.getCurrentDesignation(log, item.getStaffid()).getDesignation());
            ps.setString(6, item.getStaffid());
            ps.setString(7, item.getStaffname());

            Logger.getLogger(EmROCDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(ps));
            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
    public static EmRoc getROC(LoginProfile log, String no) throws SQLException, Exception {
        EmRoc c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM em_roc WHERE refer=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getRSRoc(rs);
        }

        return c;
    }

    private static EmRoc getRSRoc(ResultSet rs) throws SQLException {

        EmRoc g = new EmRoc();

        g.setAmount(rs.getDouble("amount"));
        g.setDateeffective(rs.getString("dateeffective"));
        g.setFileno(rs.getString("fileno"));
        g.setRchange(rs.getString("rchange"));
        g.setRefer(rs.getString("refer"));
        g.setRemark(rs.getString("remark"));
        g.setStaffdesignation(rs.getString("staffdesignation"));
        g.setStaffid(rs.getString("staffid"));
        g.setStaffname(rs.getString("staffname"));

        return g;
    }
    
    public static void deleteROC(LoginProfile log, String no) throws SQLException, Exception {

        try {
           DataDAO.deleteData(log, no, "em_roc", "refer");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
}
