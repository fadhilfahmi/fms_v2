/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.tx;

import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.development.SecModule;
import com.lcsb.fms.model.financial.tx.TxInput;
import com.lcsb.fms.model.financial.tx.TxInputDetail;
import com.lcsb.fms.model.financial.tx.TxInputDetailPK;
import com.lcsb.fms.model.financial.tx.TxOutput;
import com.lcsb.fms.model.financial.tx.TxOutputDetail;
import com.lcsb.fms.model.financial.tx.TxOutputDetailPK;
import com.lcsb.fms.model.financial.tx.TxReconcileMaster;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class TaxReconcileDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("021104");
        mod.setModuleDesc("Tax Reconcile");
        mod.setMainTable("tx_reconcile_master");
        mod.setReferID_Master("refno");
        return mod;

    }

    public static List<ListTable> tableList() {

        String column_names[] = {"refno", "name", "taxperiodfrom", "taxperiodto", "taxreturndate", "lockedby", "approveid"};
        String title_name[] = {"Refer", "Name", "From", "To", "Return", "App id", "Post"};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }

        return lt;
    }

    public static String saveMain(TxReconcileMaster item, LoginProfile log) throws Exception {

        Connection con = log.getCon();

         String newrefer = AutoGenerate.getReferenceNox(log, "refno", "tx_reconcile_master", "TXR", log.getEstateCode(), String.valueOf(item.getYear()), String.valueOf(item.getPeriod()));

        try {
            String q = ("insert into tx_reconcile_master(refno,period,year,loccode,locdesc,name,preparedate,prepareid,taxperiodfrom,taxperiodto,taxreturndate) values (?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = con.prepareStatement(q);
            ps.setString(1, newrefer);
            ps.setInt(2, item.getPeriod());
            ps.setInt(3, item.getYear());
            ps.setString(4, log.getEstateCode());
            ps.setString(5, log.getEstateDescp());
            ps.setString(6, item.getName());
            ps.setDate(7, (item.getPreparedate() != null) ? new java.sql.Date(item.getPreparedate().getTime()) : null);
            ps.setString(8, log.getUserID());
            ps.setDate(9, (item.getTaxperiodfrom() != null) ? new java.sql.Date(item.getTaxperiodfrom().getTime()) : null);
            ps.setDate(10, (item.getTaxperiodto() != null) ? new java.sql.Date(item.getTaxperiodto().getTime()) : null);
            ps.setDate(11, (item.getTaxreturndate() != null) ? new java.sql.Date(item.getTaxreturndate().getTime()) : null);

            Logger.getLogger(TaxOutputDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(ps));
            ps.executeUpdate();

            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newrefer;

    }

    public static TxReconcileMaster getRecon(LoginProfile log, String refno) throws SQLException, Exception {
        TxReconcileMaster c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from tx_reconcile_master where refno=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getRecon(rs);
        }

        return c;
    }

    public static TxReconcileMaster getRecon(ResultSet rs) throws SQLException {

        TxReconcileMaster t = new TxReconcileMaster();

        t.setApproveby(rs.getString("approveby"));
        t.setApprovedate(rs.getDate("approvedate"));
        t.setApprovedesignation(rs.getString("approvedesignation"));
        t.setApproveid(rs.getString("approveid"));
        t.setCheckedby(rs.getString("checkedby"));
        t.setCheckeddate(rs.getDate("checkeddate"));
        t.setCheckedid(rs.getString("checkedid"));
        t.setLoccode(rs.getString("loccode"));
        t.setLocdesc(rs.getString("locdesc"));
        t.setLockedby(rs.getString("lockedby"));
        t.setLockeddate(rs.getDate("lockeddate"));
        t.setName(rs.getString("name"));
        t.setPeriod(rs.getInt("period"));
        t.setPrepareby(rs.getString("prepareby"));
        t.setPreparedate(rs.getDate("preparedate"));
        t.setPrepareid(rs.getString("prepareid"));
        t.setRefno(rs.getString("refno"));
        t.setTaxperiodfrom(rs.getDate("taxperiodfrom"));
        t.setTaxperiodto(rs.getDate("taxperiodto"));
        t.setTaxreturndate(rs.getDate("taxreturndate"));
        t.setTotalAmt(rs.getDouble("total_amt"));
        t.setYear(rs.getInt("year"));
        t.setTaxdebit(rs.getDouble("taxdebit"));
        t.setTaxcredit(rs.getDouble("taxcredit"));

        return t;
    }

    public static String getStatus(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String check = "";
        String app = "";
        String post = "";
        String status = "<span class=\"label label-primary\">Preparing</span>";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select checkedid,approveid from tx_reconcile_master where refno = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            check = rs.getString(1);
            app = rs.getString(2);
        }

        stmt.close();
        rs.close();

        if (!check.equals("")) {
            status = "Checked";
        }

        if (!app.equals("")) {
            status = "Approved";
        }

        return status;
    }

    public static double getSum(LoginProfile log, String column, String table, String type) throws Exception {

        double t = 0.00;
        double u = 0.00;
        String qsum = "";

        ResultSet rs = null;

        
        if(column.equals("taxamt")){
            qsum = "sum(taxamt_ct)-sum(taxamt_dt)";
        }
        
        if(column.equals("amount")){
            qsum = "sum(amount_ct)-sum(amount_dt)";
        }
        
        
        PreparedStatement stmt = log.getCon().prepareStatement("select " + qsum + " as tot from " + table + " where refer = ?");

        stmt.setString(1, type);
        rs = stmt.executeQuery();
        if (rs.next()) {
            t = rs.getDouble(1);
        }
        
        
        stmt.close();
        rs.close();

        return t;

    }

    public static List<TxOutput> getReconOutput(LoginProfile log, String reconview) throws Exception {

        List<TxOutput> txi;
        txi = new ArrayList();

        String q = "";
        if (reconview.equals("New")) {
            q = "(recon_refno ='' OR recon_refno is null)";
        } else {
            q = "recon_refno ='" + reconview + "'";
        }

        TxOutput tx = new TxOutput();
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM tx_output WHERE " + q + "");
        //stmt.setString(1, t.getType());
        rs = stmt.executeQuery();
        while (rs.next()) {

            txi.add(TaxOutputDAO.getOutput(rs));
        }

        return txi;

    }

    public static List<TxOutputDetail> getReconOutputAll(LoginProfile log, String refno) throws Exception {

        List<TxOutputDetail> txi;
        txi = new ArrayList();

        TxOutputDetail tx = new TxOutputDetail();
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM tx_output_detail WHERE refer = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        while (rs.next()) {

            txi.add(getOutput(rs));
        }

        return txi;

    }

    private static TxOutputDetail getOutput(ResultSet rs) throws SQLException {

        TxOutputDetail t = new TxOutputDetail();
        TxOutputDetailPK tk = new TxOutputDetailPK();

        tk.setRefno(rs.getString("refno"));
        tk.setVoucherno(rs.getString("voucherno"));

        t.setTxOutputDetailPK(tk);
        t.setDate(rs.getDate("date"));
        t.setTaxdescp(rs.getString("taxdescp"));
        t.setVoucherrefer(rs.getString("voucherrefer"));
        t.setAmountDt(rs.getDouble("amount_dt"));
        t.setAmountCt(rs.getDouble("amount_ct"));
        t.setTaxamtCt(rs.getDouble("taxamt_ct"));
        t.setTaxamtDt(rs.getDouble("taxamt_dt"));
        t.setAmount(t.getAmountDt() - t.getAmountCt());
        t.setTaxamt(t.getTaxamtDt() - t.getTaxamtCt());

        return t;
    }

    public static List<TxInput> getReconInput(LoginProfile log, String reconview) throws Exception {

        List<TxInput> txi;
        txi = new ArrayList();

        String q = "";
        if (reconview.equals("New")) {
            q = "(recon_refno ='' OR recon_refno is null)";
        } else {
            q = "recon_refno ='" + reconview + "'";
        }

        TxOutput tx = new TxOutput();
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM tx_input WHERE " + q + "");
        //stmt.setString(1, t.getType());
        rs = stmt.executeQuery();
        while (rs.next()) {

            txi.add(TaxInputDAO.getInput(rs));
        }

        return txi;

    }

    public static List<TxInputDetail> getReconInputAll(LoginProfile log, String refno) throws Exception {

        List<TxInputDetail> txi;
        txi = new ArrayList();

        TxInputDetail tx = new TxInputDetail();
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM tx_input_detail WHERE refer = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        while (rs.next()) {

            txi.add(getInput(rs));
        }

        return txi;

    }

    private static TxInputDetail getInput(ResultSet rs) throws SQLException {

        TxInputDetail t = new TxInputDetail();
        TxInputDetailPK tk = new TxInputDetailPK();

        tk.setRefno(rs.getString("refno"));
        tk.setVoucherno(rs.getString("voucherno"));

        t.setTxInputDetailPK(tk);
        t.setDate(rs.getDate("date"));
        t.setTaxdescp(rs.getString("taxdescp"));
        t.setVoucherrefer(rs.getString("voucherrefer"));
        t.setAmountDt(rs.getDouble("amount_dt"));
        t.setAmountCt(rs.getDouble("amount_ct"));
        t.setTaxamtCt(rs.getDouble("taxamt_ct"));
        t.setTaxamtDt(rs.getDouble("taxamt_dt"));
        t.setAmount(t.getAmountDt() - t.getAmountCt());
        t.setTaxamt(t.getTaxamtDt() - t.getTaxamtCt());

        return t;
    }

    public static boolean isCheck(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select checkedid from tx_reconcile_master where refno = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString(1);
        }

        if (!ck.equals("")) {
            cek = true;
        }

        stmt.close();
        rs.close();

        return cek;
    }

    public static boolean isApprove(LoginProfile log, String refer) throws Exception {

        boolean cek = false;
        String ck = "";
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select approveid from tx_reconcile_master where refno = ?");

        stmt.setString(1, refer);
        rs = stmt.executeQuery();
        if (rs.next()) {
            ck = rs.getString(1);
        }

        if (!ck.equals("")) {
            cek = true;
        }

        stmt.close();
        rs.close();

        return cek;
    }

    public static boolean checkExistDetail(LoginProfile log, String refno) throws Exception {

        boolean b = false;
        ResultSet rs = null;

        PreparedStatement stmt = log.getCon().prepareStatement("select * from tx_reconcile where refer = ?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            b = true;
        }

        return b;
    }

    public static void updateOutputInput(LoginProfile log, String refno, String recon_refno, String table) throws Exception {
        
        String query = "UPDATE " + table + " SET recon_refno = '" + recon_refno + "' where refno = '" + refno + "'";

        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }

    public static void saveReconcileOutput(String refno, LoginProfile log) throws Exception {

        ResultSet rs = null;

        Connection con = log.getCon();
        PreparedStatement stmt = con.prepareStatement("select a.refno as txrefno,taxcode, taxdescp,taxcoacode,taxcoadescp,sum(taxamt_ct-taxamt_dt) as taxamt,sum(amount_ct-amount_dt) as amount ,taxrate, baddebt from tx_output a inner join tx_output_detail b on a.refno=b.refer where a.recon_refno=? group by taxcode, taxcoacode, baddebt");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        while (rs.next()) {

            String query = "insert into tx_reconcile (refer,year,period,taxperiodfrom,taxperiodto,date,taxcode,taxdescp,taxrate,inamt,outamt,taxreturn,preid,prename,predate,predesign,taxcoacode,taxcoadesc,amount,baddebt,refno) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, refno);
            ps.setInt(2, TaxReconcileDAO.getRecon(log, refno).getYear());
            ps.setInt(3, TaxReconcileDAO.getRecon(log, refno).getPeriod());
            ps.setDate(4, (TaxReconcileDAO.getRecon(log, refno).getTaxperiodfrom() != null) ? new java.sql.Date(TaxReconcileDAO.getRecon(log, refno).getTaxperiodfrom().getTime()) : null);
            ps.setDate(5, (TaxReconcileDAO.getRecon(log, refno).getTaxperiodto() != null) ? new java.sql.Date(TaxReconcileDAO.getRecon(log, refno).getTaxperiodto().getTime()) : null);
            ps.setDate(6, (AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()) != null) ? new java.sql.Date(AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()).getTime()) : null);
            ps.setString(7, rs.getString("taxcode"));
            ps.setString(8, rs.getString("taxdescp"));
            ps.setString(9, rs.getString("taxrate"));
            ps.setDouble(10, 0.00);
            ps.setDouble(11, rs.getDouble("taxamt"));
            ps.setDouble(12, 0.00);
            ps.setString(13, log.getUserID());
            ps.setString(14, log.getFullname());
            ps.setDate(15, (AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()) != null) ? new java.sql.Date(AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()).getTime()) : null);
            ps.setString(16, "");
            ps.setString(17, rs.getString("taxcoacode"));
            ps.setString(18, rs.getString("taxcoadescp"));
            ps.setDouble(19, rs.getDouble("amount"));
            ps.setString(20, rs.getString("baddebt"));
            ps.setString(21, AutoGenerate.getReferAddBack(log, "tx_reconcile", "refno", refno, "refer"));
            ps.executeUpdate();
            ps.close();

            //update each voucher
            TaxReconcileDAO.updateEachVoucher(log, rs.getString("txrefno"));

        }

        rs.close();

    }

    public static void saveReconcileInput(String refno, LoginProfile log) throws Exception {

        ResultSet rs = null;

        Connection con = log.getCon();
        PreparedStatement stmt = con.prepareStatement("select a.refno as txrefno,taxcode, taxdescp,taxcoacode,taxcoadescp,sum(taxamt_dt-taxamt_ct) as taxamt,sum(amount_dt-amount_ct) as amount,taxrate, baddebt from tx_input a inner join tx_input_detail b on a.refno=b.refer where a.recon_refno=? group by taxcode, taxcoacode, baddebt");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        while (rs.next()) {

            String query = "insert into tx_reconcile (refer,year,period,taxperiodfrom,taxperiodto,date,taxcode,taxdescp,taxrate,inamt,outamt,taxreturn,preid,prename,predate,predesign,taxcoacode,taxcoadesc,amount,baddebt,refno) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, refno);
            ps.setInt(2, TaxReconcileDAO.getRecon(log, refno).getYear());
            ps.setInt(3, TaxReconcileDAO.getRecon(log, refno).getPeriod());
            ps.setDate(4, (TaxReconcileDAO.getRecon(log, refno).getTaxperiodfrom() != null) ? new java.sql.Date(TaxReconcileDAO.getRecon(log, refno).getTaxperiodfrom().getTime()) : null);
            ps.setDate(5, (TaxReconcileDAO.getRecon(log, refno).getTaxperiodto() != null) ? new java.sql.Date(TaxReconcileDAO.getRecon(log, refno).getTaxperiodto().getTime()) : null);
            ps.setDate(6, (AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()) != null) ? new java.sql.Date(AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()).getTime()) : null);
            ps.setString(7, rs.getString("taxcode"));
            ps.setString(8, rs.getString("taxdescp"));
            ps.setString(9, rs.getString("taxrate"));
            ps.setDouble(10, rs.getDouble("taxamt"));
            ps.setDouble(11, 0.00);
            ps.setDouble(12, 0.00);
            ps.setString(13, log.getUserID());
            ps.setString(14, log.getFullname());
            ps.setDate(15, (AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()) != null) ? new java.sql.Date(AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()).getTime()) : null);
            ps.setString(16, "");
            ps.setString(17, rs.getString("taxcoacode"));
            ps.setString(18, rs.getString("taxcoadescp"));
            ps.setDouble(19, rs.getDouble("amount"));
            ps.setString(20, rs.getString("baddebt"));
            ps.setString(21, AutoGenerate.getReferAddBack(log, "tx_reconcile", "refno", refno, "refer"));
            ps.executeUpdate();
            ps.close();

            TaxReconcileDAO.updateEachVoucher(log, rs.getString("txrefno"));

        }

        rs.close();

    }

    private static void updateEachVoucher(LoginProfile log, String refno) throws Exception {


        String txtable = "";

        if (refno.substring(0, 3).equals("TXI")) {
            txtable = "tx_input_detail";
        }

        if (refno.substring(0, 3).equals("TXO")) {
            txtable = "tx_output_detail";
        }

        try {

            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("select voucherno from " + txtable + "  where refer = ?");
            stmt.setString(1, refno);
            rs = stmt.executeQuery();
            while (rs.next()) {

                SecModule sc = GeneralTerm.getType(log, rs.getString("voucherno").substring(0, 3));

                String query = "UPDATE " + sc.getMaintable() + " SET postgst = ? WHERE " + sc.getReferidMaster() + " = '" + rs.getString("voucherno") + "'";
                
                Logger.getLogger(TaxReconcileDAO.class.getName()).log(Level.INFO, "-------" + query);

                PreparedStatement ps = log.getCon().prepareStatement(query);
                ps.setString(1, "reconcile");
                ps.executeUpdate();
                ps.close();
                sc = null;

            }
            
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateReconcileMaster(String refno, LoginProfile log) throws Exception {

        Connection con = log.getCon();

        ResultSet rs = null;
        ResultSet rs1 = null;
        double taxdebit = 0.00;
        double taxcredit = 0.00;

        PreparedStatement stmt = con.prepareStatement("select sum(taxamt_ct-taxamt_dt) as taxamt from tx_output a inner join tx_output_detail b on a.refno=b.refer where a.recon_refno=? group by taxcode, taxcoacode, baddebt");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        while (rs.next()) {
            taxdebit += rs.getDouble("taxamt");
        }

        PreparedStatement stmt1 = con.prepareStatement("select sum(taxamt_dt-taxamt_ct) as taxamt from tx_input a inner join tx_input_detail b on a.refno=b.refer where a.recon_refno=? group by taxcode, taxcoacode, baddebt");
        stmt1.setString(1, refno);
        rs1 = stmt1.executeQuery();
        while (rs1.next()) {
            taxcredit += rs1.getDouble("taxamt");
        }

        String query = "UPDATE tx_reconcile_master SET taxdebit = ?, taxcredit = ?, total_amt = ?, prepareid = ?, prepareby = ?, preparedate = ? WHERE refno = '" + refno + "'";

        PreparedStatement ps = con.prepareStatement(query);
        ps.setDouble(1, taxdebit);
        ps.setDouble(2, taxcredit);
        ps.setDouble(3, taxdebit - taxcredit);
        ps.setString(4, log.getUserID());
        ps.setString(5, log.getFullname());
        ps.setDate(6, (AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()) != null) ? new java.sql.Date(AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()).getTime()) : null);
        ps.executeUpdate();
        ps.close();

    }
    
    public static String getMinusOrAdd(String refer){
        
        String t = "+";
        
        if(refer.substring(0, 3).equals("CNR")) {
            t = "-";
        }
        
        return t;
        
    }
    
    public static void deleteReconcileDetail(LoginProfile log, String refer) throws SQLException, Exception {
        
        String deleteQuery_2 = "delete from tx_reconcile where refer= ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, refer);
        ps_2.executeUpdate();
        ps_2.close();
        
        updateTaxStatus(log, refer, "");
        updateInputOutput(log, refer);
        
        
    }
    
    public static void updateTaxStatus(LoginProfile log, String refer, String status) throws Exception {
        
        String query = "UPDATE tx_reconcile_master SET lockedby = ? where refno = '" + refer + "'";
        
        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.setString(1, status);
        ps.executeUpdate();
        ps.close();
        
    }
    
    public static void updateInputOutput(LoginProfile log, String refer) throws Exception {
        
        String query = "UPDATE tx_input SET recon_refno = ? where recon_refno = '" + refer + "'";
        String query1 = "UPDATE tx_output SET recon_refno = ? where recon_refno = '" + refer + "'";
        
        PreparedStatement ps = log.getCon().prepareStatement(query);
        PreparedStatement ps1 = log.getCon().prepareStatement(query1);
        ps.setString(1, "");
        ps1.setString(1, "");
        ps.executeUpdate();
        ps1.executeUpdate();
        ps.close();
        ps1.close();
        
    }

}
