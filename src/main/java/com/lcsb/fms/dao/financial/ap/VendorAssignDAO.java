/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.financial.ap;

import com.lcsb.fms.model.financial.ap.VendorAssign;
import com.lcsb.fms.model.financial.ap.VendorInvoice;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class VendorAssignDAO {
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("021013");
        mod.setModuleDesc("Vendor Assign");
        mod.setMainTable("ap_vendor");
        mod.setReferID_Master("suppcode");
        return mod;
         
    }
    public static List<ListTable> tableList(){
        
        String column_names[] = {"suppcode","suppname","accode","acdesc"};
        String title_name[] = {"Code.","Description","Account Code","Acc. Description"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
    
    public static void saveMain(LoginProfile log, VendorAssign item) throws Exception{
        
        try{
            String q = ("insert into ap_vendor(suppcode,suppname,suppaddress,accode,acdesc,companycode,companyname,gstid) values (?,?,?,?,?,?,?,?)");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getSuppcode());
            ps.setString(2, item.getSuppname());
            ps.setString(3, item.getSuppaddress());
            ps.setString(4, item.getAccode());
            ps.setString(5, item.getAcdesc());
            ps.setString(6, item.getCompanycode());
            ps.setString(7, item.getCompanyname());
            ps.setString(8, item.getGstid());
            
            ps.executeUpdate();

            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
    
    public static void updateMain(LoginProfile log, VendorAssign item, String id) throws Exception{
        
        try{
            String q = ("UPDATE ap_vendor set suppcode = ?,suppname = ?,suppaddress = ?,accode = ?,acdesc = ?,companycode = ?,companyname = ?, gstid = ? WHERE suppcode = '"+id+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getSuppcode());
            ps.setString(2, item.getSuppname());
            ps.setString(3, item.getSuppaddress());
            ps.setString(4, item.getAccode());
            ps.setString(5, item.getAcdesc());
            ps.setString(6, item.getCompanycode());
            ps.setString(7, item.getCompanyname());
            ps.setString(8, item.getGstid());
            
            ps.executeUpdate();

            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
    
    public static void delete(LoginProfile log, String refer) throws SQLException, Exception{
         
        
        try{
            String deleteQuery_2 = "delete from ap_vendor where suppcode= ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, refer);
            ps_2.executeUpdate();
            ps_2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
    
    public static VendorAssign getInfo(LoginProfile log, String refno) throws SQLException, Exception {
        VendorAssign c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from ap_vendor where suppcode=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static VendorAssign getInfo(ResultSet rs) throws SQLException {
        
        VendorAssign cv = new VendorAssign();
     
        cv.setAccode(rs.getString("accode"));
        cv.setAcdesc(rs.getString("acdesc"));
        cv.setCompanycode(rs.getString("companycode"));
        cv.setCompanyname(rs.getString("companyname"));
        cv.setSuppaddress(rs.getString("suppaddress"));
        cv.setSuppcode(rs.getString("suppcode"));
        cv.setSuppname(rs.getString("suppname"));
        cv.setGstid(rs.getString("gstid"));
        
        
        return cv;
    }
    
    
}
