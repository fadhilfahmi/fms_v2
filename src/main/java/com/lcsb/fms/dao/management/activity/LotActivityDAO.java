/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.management.activity;

import com.lcsb.fms.dao.setup.configuration.LotMemberDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.management.activity.LotActivity;
import com.lcsb.fms.model.management.activity.LotProfitCalc;
import com.lcsb.fms.model.management.activity.LotTempMember;
import com.lcsb.fms.model.setup.configuration.LotMember;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class LotActivityDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("030103");
        mod.setModuleDesc("Lot Activity");
        mod.setMainTable("ac_lot_activity");
        mod.setReferID_Master("refer");
        return mod;

    }

    public static List<ListTable> tableList() {

        String column_names[] = {"refer", "date", "quarter", "lotcode", "lotdesc", "checkid", "appid"};
        String title_name[] = {"Refer", "Date", "Quarter", "Lot Code", "Lot Desc.", "App id", "Post"};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }

        return lt;
    }

    public static String saveData(LotActivity item, LoginProfile log) throws Exception {

        String refer = "";
        //String no, String bankcode, String bankname, Date tarikh, String startcek, int nocek, String endcek, String active)
        try {

            refer = AutoGenerate.getReferenceNo(log, "refer", "ac_lot_activity", "LTA", log.getEstateCode());

            String q = ("insert into ac_lot_activity(refer, lotcode, lotdesc,period,year,quarter,estatecode,estatename,preid,prename,date,predate,remark) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, refer);
            ps.setString(2, item.getLotcode());
            ps.setString(3, item.getLotdesc());
            ps.setString(4, item.getPeriod());
            ps.setString(5, item.getYear());
            ps.setString(6, item.getQuarter());
            ps.setString(7, log.getEstateCode());
            ps.setString(8, log.getEstateDescp());
            ps.setString(9, log.getUserID());
            ps.setString(10, log.getFullname());
            ps.setDate(11, (AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()) != null) ? new java.sql.Date(AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()).getTime()) : null);
            ps.setDate(12, (AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()) != null) ? new java.sql.Date(AccountingPeriod.convertStringtoDate(AccountingPeriod.getCurrentTimeStamp()).getTime()) : null);
            ps.setString(13, item.getRemark());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return refer;

    }

    public static LotActivity getInfo(LoginProfile log, String no) throws SQLException, Exception {
        LotActivity c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM ac_lot_activity where refer=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        return c;
    }

    private static LotActivity getInfo(ResultSet rs) throws SQLException {
        LotActivity g = new LotActivity();
        g.setAmount(rs.getDouble("amount"));
        g.setAppdate(rs.getDate("appdate"));
        g.setAppdesign(rs.getString("appdesign"));
        g.setAppid(rs.getString("appid"));
        g.setAppname(rs.getString("appname"));
        g.setCheckdate(rs.getDate("checkdate"));
        g.setCheckid(rs.getString("checkid"));
        g.setCheckname(rs.getString("checkname"));
        g.setDate(rs.getDate("date"));
        g.setEstatecode(rs.getString("estatecode"));
        g.setEstatename(rs.getString("estatename"));
        g.setLotcode(rs.getString("lotcode"));
        g.setLotdesc(rs.getString("lotdesc"));
        g.setPeriod(rs.getString("period"));
        g.setPredate(rs.getDate("predate"));
        g.setPreid(rs.getString("preid"));
        g.setPrename(rs.getString("prename"));
        g.setQuarter(rs.getString("quarter"));
        g.setRefer(rs.getString("refer"));
        g.setRemark(rs.getString("remark"));
        g.setYear(rs.getString("year"));

        return g;
    }

    public static void deleteMain(LoginProfile log, String refer) throws SQLException, Exception {


        String deleteQuery_2 = "delete from ac_lot_activity where refer = ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, refer);
        ps_2.executeUpdate();
        ps_2.close();

    }

    public static double getProfit(String loccode, String year, String period) throws Exception {

        double profit = 0.0;

        //revenue
        double revenue = getRevenue(loccode, year, period);

        double code24 = getCode24(loccode, year, period) * -1;
        double code22 = getCode22(loccode, year, period) * -1;

        //expense - general charges - 220290
        double expense = getSumsales(loccode, "220290", year, period, "") * -1;
        double selling = getSumsales(loccode, "29", year, period, "");

        profit = revenue - expense - code24 - code22 - selling;
        Logger.getLogger(LotActivityDAO.class.getName()).log(Level.INFO, profit + "----------$" + revenue + "$" + expense + "$" + code24 + "$" + code22 + "$" + selling);

        return profit;

    }

    private static double getSumsales(String loccode, String code, String year, String period, String extra) throws Exception {

        double sum = 0.0;
        ResultSet rs = null;
        Connection conet = ConnectionUtil.getHQConnection();
        PreparedStatement stmt = conet.prepareStatement("select ifnull(credit-debit,0) as sumsales from " + ConnectionUtil.getServerInfo(loccode.substring(0, 2)).getFolderldg() + ".gl_closingbalance as t1 left join product_info as t2 on t2.entercode=right(t1.loccode,2) left join enterprise_info as t3 on t3.code=right(t1.loccode,2) where t1.coacode like '" + code + "%' and t1.loccode like '" + loccode + "%' and concat(year,period)=concat('" + year + "','" + period + "') and stagecode='0006' " + extra + " group by t1.itemid");

        //stmt.setString(1, no);
        rs = stmt.executeQuery();
        while (rs.next()) {
            sum += rs.getDouble("sumsales");
        }

        rs.close();
        return sum;

    }

    private static double getRevenue(String loccode, String year, String period) throws Exception {

        double rev = 0.0;

        ResultSet rs = null;
        Connection conet = ConnectionUtil.getHQConnection();
        PreparedStatement stmt = conet.prepareStatement("select t1.code,t1.descp from chartofacccount as t1 left join product_info  as t2 on t2.coa=t1.code left join enterprise_info as t3 on t3.code=t2.entercode left join ce_estate_enterprise as t4 on t4.code=t3.code where t4.estatecode like concat(left('" + loccode + "',4),'%') and t1.code like '2101%' and length(t1.code)=6  group by t1.code");

        //stmt.setString(1, no);
        rs = stmt.executeQuery();
        while (rs.next()) {
            rev += getSumsales(loccode, rs.getString("t1.code"), year, period, "");
        }
        rs.close();
        return rev;
    }

    private static double getCode24(String loccode, String year, String period) throws Exception {

        double rev = 0.0;

        ResultSet rs = null;
        Connection conet = ConnectionUtil.getHQConnection();
        PreparedStatement stmt = conet.prepareStatement("select code,descp from chartofacccount where code like '24%' and length(code)=4  group by code");

        //stmt.setString(1, no);
        rs = stmt.executeQuery();
        while (rs.next()) {
            rev += getSumsales(loccode, rs.getString("code"), year, period, "");
        }
        rs.close();
        return rev;
    }

    private static double getCode22(String loccode, String year, String period) throws Exception {

        double rev = 0.0;

        ResultSet rs = null;
        Connection conet = ConnectionUtil.getHQConnection();
        PreparedStatement stmt = conet.prepareStatement("select code,descp from chartofacccount  where code like '220%' and length(code)=4  and code not like '2201' and code <> '220290' group by code");

        //stmt.setString(1, no);
        rs = stmt.executeQuery();
        while (rs.next()) {
            rev += getSumsales(loccode, rs.getString("code"), year, period, "and t1.coacode<>'220290'");

        }
        rs.close();
        return rev;
    }

    public static int saveIntoTempMember(LoginProfile log, List<LotTempMember> listT) throws Exception {

        Connection con = log.getCon();
        int i = 0;
        try {

            String deleteQuery_2 = "delete from ac_lot_temp_member where prepareid = ?";
            try (PreparedStatement ps_2 = con.prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, listT.get(0).getPrepareid());
                ps_2.executeUpdate();
            }

            for (LotTempMember m : listT) {

                String q = ("INSERT INTO ac_lot_temp_member(code,lotcode,name,period,quarter,sessionid,year,prepareid) values (?,?,?,?,?,?,?,?)");

                try (PreparedStatement ps = con.prepareStatement(q)) {

                    ps.setString(1, m.getCode());
                    ps.setString(2, m.getLotcode());
                    ps.setString(3, m.getName());
                    ps.setString(4, m.getPeriod());
                    ps.setString(5, m.getQuarter());
                    ps.setString(6, m.getSessionid());
                    ps.setString(7, m.getYear());
                    ps.setString(8, m.getPrepareid());

                    Logger.getLogger(LotActivityDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
                    ps.executeUpdate();

                    i = 1;
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
            i = 0;
        }

        return i;

    }

    public static int saveProfitCalculated(LoginProfile log, LotProfitCalc m) throws Exception {

        Connection con = log.getCon();
        int i = 0;
        try {

            String deleteQuery_2 = "delete from ac_lot_profit_calc where sessionid = ?";
            try (PreparedStatement ps_2 = con.prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, m.getSessionid());
                ps_2.executeUpdate();
            }

            String q = ("INSERT INTO ac_lot_profit_calc(lotcode,lotdesc,period,quarter,sessionid,year,balafterdist,currevenue,curyearreplanting,distributableprofit,distributed,distsuggest,lkpprevenue,prerevenue,preyearreplanting,profitperhect,rollingcapital,totaldistributed,totalrevenue) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            try (PreparedStatement ps = con.prepareStatement(q)) {

                ps.setString(1, m.getLotcode());
                ps.setString(2, m.getLotdesc());
                ps.setString(3, m.getPeriod());
                ps.setString(4, m.getQuarter());
                ps.setString(5, m.getSessionid());
                ps.setString(6, m.getYear());
                ps.setDouble(7, m.getBalafterdist());
                ps.setDouble(8, m.getCurrevenue());
                ps.setDouble(9, m.getCuryearreplanting());
                ps.setDouble(10, m.getDistributableprofit());
                ps.setDouble(11, m.getDistributed());
                ps.setDouble(12, m.getDistsuggest());
                ps.setDouble(13, m.getLkpprevenue());
                ps.setDouble(14, m.getPrerevenue());
                ps.setDouble(15, m.getPreyearreplanting());
                ps.setDouble(16, m.getProfitperhect());
                ps.setDouble(17, m.getRollingcapital());
                ps.setDouble(18, m.getTotaldistributed());
                ps.setDouble(19, m.getTotalrevenue());

                Logger.getLogger(LotActivityDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
                ps.executeUpdate();

                i = 1;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            i = 0;
        }

        return i;

    }
    
    public static List<LotTempMember> getAllMemberByLot(LoginProfile log, String sessionid) throws Exception {

        Statement stmt = null;
        List<LotTempMember> CVi;
        CVi = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from ac_lot_temp_member where sessionid = '"+sessionid+"'  order by name asc");

            while (rs.next()) {
                LotTempMember lt = new LotTempMember();
                
                lt = getResult(rs);
                CVi.add(lt);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    private static LotTempMember getResult(ResultSet rs) throws SQLException {
        LotTempMember cv = new LotTempMember();

        cv.setCode(rs.getString("code"));
        cv.setLotcode(rs.getString("lotcode"));
        cv.setName(rs.getString("name"));
        cv.setPeriod(rs.getString("period"));
        cv.setPrepareid(rs.getString("prepareid"));
        cv.setQuarter(rs.getString("quarter"));
        cv.setSessionid(rs.getString("sessionid"));
        cv.setYear(rs.getString("year"));

        return cv;
    }
    
    public static LotProfitCalc getFinalize(LoginProfile log, String sessionid) throws SQLException, Exception {
        LotProfitCalc c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM ac_lot_profit_calc where sessionid=?");
        stmt.setString(1, sessionid);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getFinalize(rs);
        }

        return c;
    }
    
    private static LotProfitCalc getFinalize(ResultSet rs) throws SQLException {
        LotProfitCalc g = new LotProfitCalc();
        g.setBalafterdist(rs.getDouble("balafterdist"));
        g.setCurrevenue(rs.getDouble("currevenue"));
        g.setCuryearreplanting(rs.getDouble("curyearreplanting"));
        g.setDistributableprofit(rs.getDouble("distributableprofit"));
        g.setDistributed(rs.getDouble("distributed"));
        g.setDistsuggest(rs.getDouble("distsuggest"));
        g.setLkpprevenue(rs.getDouble("lkpprevenue"));
        g.setLotcode(rs.getString("lotcode"));
        g.setLotdesc(rs.getString("lotdesc"));
        g.setPeriod(rs.getString("period"));
        g.setPrerevenue(rs.getDouble("prerevenue"));
        g.setPreyearreplanting(rs.getDouble("preyearreplanting"));
        g.setProfitperhect(rs.getDouble("profitperhect"));
        g.setQuarter(rs.getString("quarter"));
        g.setRollingcapital(rs.getDouble("rollingcapital"));
        g.setSessionid(rs.getString("sessionid"));
        g.setTotaldistributed(rs.getDouble("totaldistributed"));
        g.setTotalrevenue(rs.getDouble("totalrevenue"));
        g.setYear(rs.getString("year"));

        return g;
    }
    
    public static int getTotalMember(LoginProfile log, String sessionid) throws SQLException, Exception {
        int c = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT count(*) as cnt FROM ac_lot_temp_member where sessionid=?");
        stmt.setString(1, sessionid);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = rs.getInt("cnt");
        }

        return c;
    }
}
