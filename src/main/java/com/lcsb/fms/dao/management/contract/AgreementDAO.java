/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.management.contract;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.dao.financial.ap.VendorPaymentDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucher;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherRefer;
import com.lcsb.fms.model.management.contract.Agreement;
import com.lcsb.fms.model.management.contract.AgreementJob;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class AgreementDAO {
    
    public static PathModel PathTo(LoginProfile log, String moduleid, String process, Agreement data, AgreementJob job, String referno) throws Exception{
        
        String urlsend = "";
        PathModel pm = new PathModel();
        
        try{
            if(process.equals("viewlist")){
                urlsend = "/co_agree_list.jsp?id="+moduleid;

            }else if(process.equals("addnew")){
                urlsend = "/co_agree_add.jsp";

            }else if(process.equals("addmain")){

                AgreementDAO.saveMain(log, data);
                urlsend = "/co_agree_list.jsp";

            }else if(process.equals("addnewitem")){
                urlsend = "/co_agree_add_job.jsp?referno="+referno; 

            }else if(process.equals("addjob")){
                try{

                    AgreementDAO.saveItem(log, job);
                    urlsend = "/co_agree_list.jsp?id="+moduleid;//+"&referenceno="+request.getParameter("referenceno");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }else if(process.equals("editinv")){
                urlsend = "/ar_inv_edit_list.jsp?referno="+referno;

            }else if(process.equals("viewinv")){
                urlsend = "/ar_inv_view_voucher.jsp?referno="+referno;

            }else if(process.equals("editmain")){
                urlsend = "/ar_inv_edit_main.jsp?referno="+referno;

            }else if(process.equals("editmainprocess")){

                //AgreementDAO.updateMain(data, request.getParameter("invref"));
                urlsend = "/ar_inv_edit_list.jsp?referno="+referno;

            }else if(process.equals("edititemprocess")){

                //AgreementDAO.updateItem(data,request.getParameter("refer"));
                urlsend = "/ar_inv_edit_list.jsp?referno="+referno;

            }else if(process.equals("edititem")){
                urlsend = "/ar_inv_edit_item.jsp?referno="+referno;

            }else if(process.equals("post")){
                urlsend = "/gl_post.jsp?referno="+referno+"&vtype=SNV";

            }else if(process.equals("dist")){
                //Distribute dist = (Distribute) DistributeDAO.distributeNow(request.getParameter("referno"),"SNV");
                //session.setAttribute("post_detail", dist);
                //if(dist.isSuccess()){
                //    urlsend = "/gl_post_status.jsp";
                //}else{
                //    urlsend = "/gl_post_status.jsp";
                //}
            }else if(process.equals("delete")){
                AgreementDAO.deleteExist(log, referno);
                urlsend = "/ar_inv_list.jsp?id="+moduleid;//+"&referenceno="+request.getParameter("referenceno");

            }else if(process.equals("check")){
                //AgreementDAO.checkVoucher(referno,log.getUserID(), log.getUserName());
                urlsend = "/ar_inv_list.jsp?id="+moduleid;//+"&referenceno="+request.getParameter("referenceno");

            }else if(process.equals("approve")){
                //AgreementDAO.approveVoucher(request.getParameter("referno"),log.getUserID(), log.getUserName(), log.getPosition());
                urlsend = "/ar_inv_list.jsp?id="+moduleid;//+"&referenceno="+request.getParameter("referenceno");

            }else if(process.equals("view")){
                //AgreementDAO.approveVoucher(request.getParameter("referno"),log.getUserID(), log.getUserName(), log.getPosition());
                urlsend = "/co_agree_view.jsp?id="+moduleid;//+"&referenceno="+request.getParameter("referenceno");

            }else{

                ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                ErrorIO.setCode("100");//nullpointer
                ErrorIO.setType("data");
                urlsend = "/error.jsp";
            }   
        } catch (Exception e) {
            e.printStackTrace();
            ErrorIO.setError(String.valueOf(e));
            ErrorIO.setCode("100");//nullpointer
            ErrorIO.setType("data");
            urlsend = "/error.jsp";
        }
        
        
        pm.setUrlSend(urlsend);
            
        return pm;
    }
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("030206");
        mod.setModuleDesc("Agreement");
        mod.setMainTable("co_big_agree");
        mod.setReferID_Master("refer");
        return mod;
         
    }
    public static List<ListTable> tableList(){
        
        String column_names[] = {"refer","name","saksiid","pengurusid"};
        String title_name[] = {"Reference No.","Company Name","saksi","pengurus"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
    
    public static void deleteExist(LoginProfile log, String no) throws SQLException, Exception{
        Connection con = log.getCon();
        
        String deleteQuery_1 = "delete from cb_payvoucher where refer = ?";
        String deleteQuery_2 = "delete from cb_payvaucher_debit where voucer = ?";
        PreparedStatement ps_1 = con.prepareStatement(deleteQuery_1);
        PreparedStatement ps_2 = con.prepareStatement(deleteQuery_2);
        ps_1.setString(1, no);
        ps_2.setString(1, no);
        ps_1.executeUpdate();
        ps_2.executeUpdate();
        ps_1.close();
        ps_2.close();
        
        
    }
     
     public static void deleteItem(LoginProfile log, String novoucher, String refer) throws SQLException, Exception{
         
        String deleteQuery_2 = "delete from cb_payvaucher_debit where voucer= ?";
        PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
        ps_2.setString(1, novoucher);
        ps_2.executeUpdate();
        ps_2.close();
        
        updateAmount(log, refer);
        
        
    }
    
     public static void saveMain(LoginProfile log, Agreement item) throws Exception{
        try{
             //ps.setString(1, AutoGenerate.get2digitNo("info_bank", "code"));
            String q = ("insert into co_big_agree(dateend,year,period,estatecode,estatename,refer,code,name,date,ic,address,owner,typeagree,tkipercent,preid,prename,predesign,predate,hnoagree,flag,gstid) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getDateend());
            ps.setString(2, item.getYear());
            ps.setString(3, item.getPeriod());
            ps.setString(4, item.getEstatecode());
            ps.setString(5, item.getEstatename());
            ps.setString(6, AutoGenerate.getReferenceNo(log, "refer", "co_big_agree", "CAG", item.getEstatecode()));
            ps.setString(7, item.getCode());
            ps.setString(8, item.getName());
            ps.setString(9, item.getDate());
            ps.setString(10, item.getIc());
            ps.setString(11, item.getAddress());
            ps.setString(12, item.getOwner());
            ps.setString(13, item.getTypeagree());
            ps.setString(14, item.getTkipercent());
            ps.setString(15, item.getPreid());
            ps.setString(16, item.getPrename());
            ps.setString(17, item.getPredesign());
            ps.setString(18, item.getPredate());
            ps.setString(19, item.getHnoagree());
            ps.setString(20, item.getFlag());
            ps.setString(21, item.getGstid());
            
            ps.executeUpdate();
            ps.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
     
   
    
    public static void saveItem(LoginProfile log, AgreementJob item) throws Exception{
        
        try{
            String q = ("insert into co_big_agree_work(no,refer,unit,priceunit,pricework,jobcode,jobdescp,loccode,locdesc,unitmeasure,loclevel,type,outputcode,outputdesc,entercode,enterdesc,remark,taxcode,taxdescp,taxcoacode,taxcoadescp,taxrate,taxamt) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getNo());
            ps.setString(2, AutoGenerate.getReferAddBack(log, "co_big_agree_work", "refer", item.getNo(), "no"));
            ps.setString(3, item.getUnit());
            ps.setString(4, item.getPriceunit());
            ps.setDouble(5, item.getPricework());
            ps.setString(6, item.getJobcode());
            ps.setString(7, item.getJobdescp());
            ps.setString(8, item.getLoccode());
            ps.setString(9, item.getLocdesc());
            ps.setString(10, item.getUnitmeasure());
            ps.setString(11, item.getLoclevel());
            ps.setString(12, item.getType());
            ps.setString(13, item.getOutputcode());
            ps.setString(14, item.getOutputdesc());
            ps.setString(15, item.getEntercode());
            ps.setString(16, item.getEnterdesc());
            ps.setString(17, item.getRemark());
            ps.setString(18, item.getTaxcode());
            ps.setString(19, item.getTaxdescp());
            ps.setString(20, item.getTaxcoacode());
            ps.setString(21, item.getTaxcoadescp());
            ps.setDouble(22, item.getTaxrate());
            ps.setDouble(23, item.getTaxamt());
           
            ps.executeUpdate();
            Logger.getLogger(AgreementDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

       } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void updateMain(LoginProfile log, Agreement item, String id) throws Exception{
        
        
        try{
            String q = ("UPDATE co_big_agree set dateend = ?,year = ?,period = ?,estatecode = ?,estatename = ?,refer = ?,code = ?,name = ?,date = ?,ic = ?,address = ?,owner = ?,typeagree = ?,tkipercent = ?,preid = ?,prename = ?,predesign = ?,predate = ?,hnoagree = ?,flag = ?,gstid = ? WHERE refer = '"+id+"'");
            //amtbeforetax=?
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getDateend());
            ps.setString(2, item.getYear());
            ps.setString(3, item.getPeriod());
            ps.setString(4, item.getEstatecode());
            ps.setString(5, item.getEstatename());
            ps.setString(6, item.getRefer());
            ps.setString(7, item.getCode());
            ps.setString(8, item.getName());
            ps.setString(9, item.getDate());
            ps.setString(10, item.getIc());
            ps.setString(11, item.getAddress());
            ps.setString(12, item.getOwner());
            ps.setString(13, item.getTypeagree());
            ps.setString(14, item.getTkipercent());
            ps.setString(15, item.getPreid());
            ps.setString(16, item.getPrename());
            ps.setString(17, item.getPredesign());
            ps.setString(18, item.getHnoagree());
            ps.setString(19, item.getFlag());
            ps.setString(20, item.getGstid());
            Logger.getLogger(VendorPaymentDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            
            //Logger.getLogger(SalesInvoiceDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, AgreementJob item, String id) throws Exception{
        
        
        try{
            String q = ("UPDATE co_big_agree_work set no = ?,refer = ?,unit = ?,priceunit = ?,pricework = ?,jobcode = ?,jobdescp = ?,loccode = ?,locdesc = ?,unitmeasure = ?,loclevel = ?,type = ?,outputcode = ?,outputdesc = ?,entercode = ?,enterdesc = ?,remark = ?,taxcode = ?,taxdescp = ?,taxcoacode = ?,taxcoadescp = ?,taxrate = ?,taxamt = ? WHERE refer = '"+id+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getNo());
            ps.setString(2, item.getRefer());
            ps.setString(3, item.getUnit());
            ps.setString(4, item.getPriceunit());
            ps.setDouble(5, item.getPricework());
            ps.setString(6, item.getJobcode());
            ps.setString(7, item.getJobdescp());
            ps.setString(8, item.getLoccode());
            ps.setString(9, item.getLocdesc());
            ps.setString(10, item.getUnitmeasure());
            ps.setString(11, item.getLoclevel());
            ps.setString(12, item.getType());
            ps.setString(13, item.getOutputcode());
            ps.setString(14, item.getOutputdesc());
            ps.setString(15, item.getEntercode());
            ps.setString(16, item.getEnterdesc());
            ps.setString(17, item.getRemark());
            ps.setString(18, item.getTaxcode());
            ps.setString(19, item.getTaxdescp());
            ps.setString(20, item.getTaxcoacode());
            ps.setString(21, item.getTaxcoadescp());
            ps.setDouble(22, item.getTaxrate());
            ps.setDouble(23, item.getTaxamt());
            
            Logger.getLogger(VendorPaymentDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static String getCVno(LoginProfile log, String abb) throws Exception{
        
        ResultSet rs = null;
        String CVno = "";
        String year = AccountingPeriod.getCurYear(log);
        String curperiod = AccountingPeriod.getCurPeriod(log);
        //String estatecode = LoginProfile.getEstateCode();
        String estatecode = "9911";
        
        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat(?,'"+ estatecode +"',substring('"+ year +"',3,2),lpad('"+ curperiod +"',2,'0'),lpad((max(substring(refer,14,4))+1),4,'0')),concat('JPY','"+ estatecode +"',substring('"+ year +"',3,2),lpad('"+ curperiod +"',2,'0'),'0001')) as refer from cb_cashvoucher where refer like concat(?,'%') and year='"+ year +"' and period='"+ curperiod +"' and estatecode like concat('"+ estatecode +"','%')");
        stmt.setString(1, abb);
        stmt.setString(2, abb);
        
        rs = stmt.executeQuery();
        if (rs.next()) {
            CVno = rs.getString(1);
            
            
        }
        
        rs.close();
        stmt.close();
        return CVno;
        
    }
    
    public static String getCVoucherNo(LoginProfile log) throws Exception{
        
        ResultSet rs = null;
        String CVno = "";
        
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT ifnull(concat(lpad((max(voucherid)+1),5,'0')),concat('0001')) as new FROM `cb_cashvoucher`");
        
        rs = stmt.executeQuery();
        if (rs.next()) {
            CVno = rs.getString(1); 
        }
        
        rs.close();
        stmt.close();
        return CVno;
        
    }
    
    public static String getCVid(LoginProfile log, String refno) throws Exception, SQLException{
        
        ResultSet rs = null;
        String JVno = "";
        
        PreparedStatement stmt = log.getCon().prepareStatement("select ifnull(concat('"+refno+"',lpad((max(right(novoucher,4))+1),4,'0')),concat('"+refno+"','0001')) as njvno from cb_cashvoucher_account where refer='"+refno+"'");
        
        rs = stmt.executeQuery();
        if (rs.next()) {
            JVno = rs.getString(1); 
        }
        
        rs.close();
        stmt.close();
        return JVno;
        
    }
   
    public static Double getSum(LoginProfile log, String column, String id)throws Exception {
        
        Double x = 0.00;
        Statement stment = log.getCon().createStatement();   
        ResultSet setent =stment.executeQuery(" select sum("+column+") as sumamount from co_big_agree_work where no = '"+id+"'");
       
        while(setent.next()) {
            x+=setent.getDouble("sumamount");
        }
        
        stment.close();
        setent.close();
        return x;
    }
    
    public static Double getBalance(LoginProfile log, String column, String id)throws Exception {
        
        Double x = 0.00;
        Double y = 0.00;
        Statement stment = log.getCon().createStatement();   
        ResultSet setent =stment.executeQuery(" select sum("+column+") as sumamount from cb_cashvoucher_account where refer = '"+id+"'");
       
        while(setent.next()) {
            x+=setent.getDouble("sumamount");
        }
        
          
        setent =stment.executeQuery(" select total from cb_cashvoucher where refer = '"+id+"'");
       
        if(setent.next()) {
            y=setent.getDouble("total");
        }
        
        stment.close();
        setent.close();
        return y-x;
    }
    
    public static AgreementJob getJob(String id)throws Exception {
        
        AgreementJob v = new AgreementJob();
        
        v.setNo(id);
        //v.setAmtbeforetax(getBalance("debit",id));
       
        return v;
    }
    
    public static PaymentVoucherRefer getRefer(String id)throws Exception {
        
        PaymentVoucherRefer v = new PaymentVoucherRefer();
        
        v.setRefer(id);
        //v.setAmtbeforetax(getBalance("debit",id));
       
        return v;
    }
   
    
    private static void updateAmount(LoginProfile log, String JVrefno) throws Exception{
        
        String query = "UPDATE gl_jv SET todebit = '"+getSum(log, "debit",JVrefno)+"',tocredit = '"+getSum(log, "credit",JVrefno)+"' where JVrefno = '"+JVrefno+"'";
        
        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }
    
    public static List<AgreementJob> getAllCAGjob(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<AgreementJob> CVi;
        CVi = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from co_big_agree_work where no = '"+refno+"' order by no");

            while (rs.next()) {
                CVi.add(getCAGjob(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static double getTotalJob(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        
        double totaljob = 0;
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select sum(pricework) as total from co_big_agree_work where no = '"+refno+"'");

            while (rs.next()) {
                totaljob = rs.getDouble("total");
            }
            
            rs.close();;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        stmt.close();
       
        return totaljob;
    }
    
    public static double getTotalGST(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        
        double total = 0;
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select sum(taxamt) as total from co_big_agree_work where no = '"+refno+"'");

            while (rs.next()) {
                total = rs.getDouble("total");
            }
            
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        stmt.close();
       
        return total;
    }
    
    public static double getGrandTotal(LoginProfile log, String refno) throws Exception{
        
        double grandTotal = getTotalJob(log, refno) + getTotalGST(log, refno);
        
        return grandTotal;
    }
    
    public static AgreementJob getCAGjob(LoginProfile log, String id) throws SQLException, Exception {
        AgreementJob c = null;
        ResultSet rs = null;
        try{
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_big_agree_work where refer=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getCAGjob(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        
        return c;
    }
    
    private static AgreementJob getCAGjob(ResultSet rs) throws SQLException {
        AgreementJob aj = new AgreementJob();
        
        aj.setAccode(rs.getString("accode"));
        aj.setAcdesc(rs.getString("acdesc"));
        aj.setEntercode(rs.getString("entercode"));
        aj.setEnterdesc(rs.getString("enterdesc"));
        aj.setLoccode(rs.getString("loccode"));
        aj.setLocdesc(rs.getString("locdesc"));
        aj.setFlag(rs.getString("flag"));
        aj.setJobcode(rs.getString("jobcode"));
        aj.setJobdescp(rs.getString("jobdescp"));
        aj.setLoclevel(rs.getString("loclevel"));
        aj.setNo(rs.getString("no"));
        aj.setOutputcode(rs.getString("outputcode"));
        aj.setOutputdesc(rs.getString("outputdesc"));
        aj.setPriceunit(rs.getString("priceunit"));
        aj.setPricework(rs.getDouble("pricework"));
        aj.setPvenddate(rs.getString("pvenddate"));
        aj.setPvpenalty(rs.getString("pvpenalty"));
        aj.setPvpercent(rs.getString("pvpercent"));
        aj.setPvqty(rs.getString("pvqty"));
        aj.setPvstartdate(rs.getString("pvstartdate"));
        aj.setRefer(rs.getString("refer"));
        aj.setRemark(rs.getString("remark"));
        aj.setStartdate(rs.getString("startdate"));
        aj.setTaxamt(rs.getDouble("taxamt"));
        aj.setTaxcoacode(rs.getString("taxcoacode"));
        aj.setTaxcoadescp(rs.getString("taxcoadescp"));
        aj.setTaxcode(rs.getString("taxcode"));
        aj.setTaxdescp(rs.getString("taxdescp"));
        aj.setTaxrate(rs.getDouble("taxrate"));
        aj.setType(rs.getString("type"));
        aj.setUnit(rs.getString("unit"));
        aj.setUnitmeasure(rs.getString("unitmeasure"));
        aj.setVotype(rs.getString("votype"));
        
        return aj;
    }
    
    public static List<Agreement> getAllCAG(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<Agreement> CV;
        CV = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from co_big_agree where refer = '"+refno+"' order by refer");

            while (rs.next()) {
                CV.add(getCAG(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }
    
    public static Agreement getCAG(LoginProfile log, String refno) throws SQLException, Exception {
        Agreement c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from co_big_agree where refer=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getCAG(rs);
        }

        
        return c;
    }
    
    private static Agreement getCAG(ResultSet rs) throws SQLException {
        
        Agreement ag = new Agreement();
        
        ag.setAddress(rs.getString("address"));
        ag.setAppdate(rs.getString("appdate"));
        ag.setCode(rs.getString("code"));
        ag.setConfirmagree(rs.getString("confirmagree"));
        ag.setDate(rs.getString("date"));
        ag.setDatecheck(rs.getString("datecheck"));
        ag.setDateend(rs.getString("dateend"));
        ag.setEstatecode(rs.getString("estatecode"));
        ag.setEstatename(rs.getString("estatename"));
        ag.setFlag(rs.getString("flag"));
        ag.setGstid(rs.getString("gstid"));
        ag.setHnoagree(rs.getString("hnoagree"));
        ag.setIc(rs.getString("ic"));
        ag.setIcpengurus(rs.getString("icpengurus"));
        ag.setName(rs.getString("name"));
        ag.setOwner(rs.getString("owner"));
        ag.setPamount(rs.getDouble("pamount"));
        ag.setPdesignation(rs.getString("pdesignation"));
        ag.setPengurus(rs.getString("pengurus"));
        ag.setPengurusid(rs.getString("pengurusid"));
        ag.setPeriod(rs.getString("period"));
        ag.setPredate(rs.getString("predate"));
        ag.setPredesign(rs.getString("predesign"));
        ag.setPreid(rs.getString("preid"));
        ag.setPrename(rs.getString("prename"));
        ag.setPtype(rs.getString("ptype"));
        ag.setRefer(rs.getString("refer"));
        ag.setSaksi(rs.getString("saksi"));
        ag.setSaksiic(rs.getString("saksiic"));
        ag.setSaksiid(rs.getString("saksiid"));
        ag.setSdesignation(rs.getString("sdesignation"));
        ag.setTkipercent(rs.getString("tkipercent"));
        ag.setTypeagree(rs.getString("typeagree"));
        ag.setYear(rs.getString("year"));
        
        return ag;
    }
    
    public static double getDebit(LoginProfile log, String refno) throws SQLException, Exception {
        double amount = 0;
        String btype = "";
        ResultSet rs = null;
        ResultSet rs1 = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select btype,amountno from sl_inv where invref=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amount = rs.getDouble("amountno");
            btype = rs.getString("btype");
        }
        
        if(btype.equals("Stock And Store")){
            
            PreparedStatement stmt1 = log.getCon().prepareStatement("select sum(amount) as amt,sum(taxamt) as tax from sl_inv_item where ivref=?");
            stmt1.setString(1, refno);
            rs1 = stmt.executeQuery();
            if (rs.next()) {
                amount = rs.getDouble("tax");
            }
            
        }

        
        return amount;
    }
    
    public static double getCredit(LoginProfile log, String refno) throws SQLException, Exception {
        double amt = 0;
        double amttax = 0;
        double totalCredit = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select sum(amount) as amt,sum(taxamt) as amttax from sl_inv_item where ivref=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            amt = rs.getDouble("amt");
            amttax = rs.getDouble("amttax");
        }
        
        totalCredit = amt + amttax;
        
        return totalCredit;
    }
    
    public static int getCheckCounter(LoginProfile log) throws Exception{
        
        int cnt = 0;
        ResultSet rs = null;
        
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from sl_inv where year = ? and period = ? and cid = ''");
        
        stmt.setString(1, AccountingPeriod.getCurYear(log));
        stmt.setString(2, AccountingPeriod.getCurPeriod(log));
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }
        
        return cnt;
    }
    
    public static int getApproveCounter(LoginProfile log) throws Exception{
        
        int cnt = 0;
        ResultSet rs = null;
        
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from sl_inv where year = ? and period = ? and cid <> ? and pid = ?");
        
        stmt.setString(1, AccountingPeriod.getCurYear(log));
        stmt.setString(2, AccountingPeriod.getCurPeriod(log));
        stmt.setString(3, "");
        stmt.setString(4, "");
        rs = stmt.executeQuery();
        if (rs.next()) {
            cnt = rs.getInt("cnt");
        }
        
        return cnt;
    }
    
    public static void checkVoucher(LoginProfile log, String refer, String staff_id, String staff_name) throws Exception{
        
        String query = "UPDATE sl_inv SET cid = '"+staff_id+"',cname = '"+staff_name+"',cdate='"+AccountingPeriod.getCurrentTimeStamp()+"' where invref = '"+refer+"'";
        
        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }
    
    public static void approveVoucher(LoginProfile log, String refer, String staff_id, String staff_name, String position) throws Exception{
        
        String query = "UPDATE sl_inv SET aid = '"+staff_id+"',aname = '"+staff_name+"',adate='"+AccountingPeriod.getCurrentTimeStamp()+"', adesignation = '"+position+"' where invref = '"+refer+"'";
        
        PreparedStatement ps = log.getCon().prepareStatement(query);
        ps.executeUpdate();
        ps.close();
    }
    
}
