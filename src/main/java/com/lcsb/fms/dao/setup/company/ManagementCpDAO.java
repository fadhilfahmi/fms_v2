/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.company;

import com.lcsb.fms.dao.financial.ap.VendorPaymentDAO;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem;
import com.lcsb.fms.model.setup.company.CeManage;
import com.lcsb.fms.model.setup.company.CeManageBank;
import com.lcsb.fms.model.setup.company.CeManageBankPK;
import com.lcsb.fms.model.setup.company.CeManagePetty;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class ManagementCpDAO {
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010204");
        mod.setModuleDesc("Management");
        mod.setMainTable("ce_manage");
        mod.setReferID_Master("code");
        return mod;
         
    }
    
    public static List<ListTable> tableList(){
        
        String column_names[] = {"code","name"};
        String title_name[] = {"Code","Description"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
    
    public static CeManage getManagement(LoginProfile log, String refno) throws SQLException, Exception {
        CeManage c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from ce_manage where code=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getManagement(rs);
        }

        
        return c;
    }
    
    public static CeManageBank getBank(LoginProfile log, String refno) throws SQLException, Exception {
        CeManageBank c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from ce_manage_bank where code=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getBank(rs);
        }

        
        return c;
    }
    
    public static CeManagePetty getPetty(LoginProfile log, String refno) throws SQLException, Exception {
        CeManagePetty c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from ce_manage_petty where code=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getPetty(rs);
        }

        
        return c;
    }
    
    private static CeManage getManagement(ResultSet rs) throws SQLException {
        
        CeManage c = new CeManage();
     
        c.setBacd(rs.getString("bacd"));
        c.setBcac(rs.getString("bcac"));
        c.setCashbill(rs.getString("cashbill"));
        c.setCoa(rs.getString("coa"));
        c.setCoadescp(rs.getString("coadescp"));
        c.setCode(rs.getString("code"));
        c.setHac(rs.getString("hac"));
        c.setHad(rs.getString("had"));
        c.setMaksimum(rs.getString("maksimum"));
        c.setName(rs.getString("name"));
        c.setRemarks(rs.getString("remarks"));
        
        return c;
    }
    
    public static List<CeManageBank> getAllBank(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<CeManageBank> pvr;
        pvr = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from ce_manage_bank where managecode = '"+refno+"' order by code");

            while (rs.next()) {
                pvr.add(getBank(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return pvr;
    }
    
    public static CeManageBank getBank(ResultSet rs) throws SQLException {
        CeManageBank cb = new CeManageBank();
       
        cb.setActive(rs.getString("active"));
        cb.setBankaccno(rs.getString("bankaccno"));
        cb.setBranchname(rs.getString("branchname"));
        cb.setCeManageBankPK(getBankPK(rs));
        cb.setCoacode(rs.getString("coacode"));
        cb.setCoadesc(rs.getString("coadesc"));
        cb.setFtype(rs.getString("ftype"));
        cb.setMajor(rs.getString("major"));
        cb.setName(rs.getString("name"));
        cb.setRemarks(rs.getString("remarks"));
        cb.setManagename(rs.getString("managename"));
        
        return cb;
    }
    
    private static CeManageBankPK getBankPK(ResultSet rs) throws SQLException {
        CeManageBankPK cb = new CeManageBankPK();
       
        cb.setBranchcode(rs.getString("branchcode"));
        cb.setCode(rs.getString("code"));
        cb.setManagecode(rs.getString("managecode"));
        
        return cb;
    }
    
    public static void saveBank(LoginProfile log, CeManageBank item) throws Exception{
        
        try{
            String q = ("INSERT INTO ce_manage_bank(active,bankaccno,branchname,coacode,coadesc,ftype,major,name,remarks,branchcode,code,managecode,managename) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getActive());
            ps.setString(2, item.getBankaccno());
            ps.setString(3, item.getBranchname());
            ps.setString(4, item.getCoacode());
            ps.setString(5, item.getCoadesc());
            ps.setString(6, item.getFtype());
            ps.setString(7, item.getMajor());
            ps.setString(8, item.getName());
            ps.setString(9, item.getRemarks());
            ps.setString(10, item.getCeManageBankPK().getBranchcode());
            ps.setString(11, AutoGenerate.get4digitNo(log, "ce_manage_bank", "code"));
            ps.setString(12, item.getCeManageBankPK().getManagecode());
            ps.setString(13, item.getManagename());
           
            ps.executeUpdate();
            Logger.getLogger(ManagementCpDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

       } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    
    public static void updateBank(LoginProfile log, CeManageBank item) throws Exception{
        
        try{
            String q = ("UPDATE ce_manage_bank set active = ?,bankaccno = ?,branchname = ?,coacode = ?,coadesc = ?,ftype = ?,major = ?,name = ?,remarks = ?,branchcode = ?,code = ?,managecode = ?,managename = ? WHERE code = '"+item.getCeManageBankPK().getCode()+"'");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getActive());
            ps.setString(2, item.getBankaccno());
            ps.setString(3, item.getBranchname());
            ps.setString(4, item.getCoacode());
            ps.setString(5, item.getCoadesc());
            ps.setString(6, item.getFtype());
            ps.setString(7, item.getMajor());
            ps.setString(8, item.getName());
            ps.setString(9, item.getRemarks());
            ps.setString(10, item.getCeManageBankPK().getBranchcode());
            ps.setString(11, item.getCeManageBankPK().getCode());
            ps.setString(12, item.getCeManageBankPK().getManagecode());
            ps.setString(13, item.getManagename());
           
            ps.executeUpdate();
            Logger.getLogger(ManagementCpDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

       } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static int deleteBank(LoginProfile log, String refer) throws SQLException, Exception{
         
        int x = 0;
        
        try{
            String deleteQuery_2 = "delete from ce_manage_bank where code= ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, refer);
            ps_2.executeUpdate();
            ps_2.close();
            x = 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return x;
    }
    
    public static List<CeManagePetty> getAllPetty(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<CeManagePetty> pvr;
        pvr = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from ce_manage_petty where managecode = '"+refno+"' order by code");

            while (rs.next()) {
                pvr.add(getPetty(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return pvr;
    }
    
    public static CeManagePetty getPetty(ResultSet rs) throws SQLException {
        CeManagePetty cb = new CeManagePetty();
       
        cb.setCoacode(rs.getString("coacode"));
        cb.setCoadesc(rs.getString("coadesc"));
        cb.setCode(rs.getString("code"));
        cb.setDescp(rs.getString("descp"));
        cb.setManagecode(rs.getString("managecode"));
        cb.setMaxcash(rs.getDouble("maxcash"));
        cb.setMaxfloat(rs.getDouble("maxfloat"));
        cb.setRemarks(rs.getString("remarks"));
        
        return cb;
    }
    
    public static void savePetty(LoginProfile log, CeManagePetty item) throws Exception{
        
        try{
            String q = ("INSERT INTO ce_manage_petty(coacode,coadesc,code,descp,managecode,remarks,maxcash,maxfloat) VALUES (?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCoacode());
            ps.setString(2, item.getCoadesc());
            ps.setString(3, AutoGenerate.get4digitNo(log, "ce_manage_petty", "code"));
            ps.setString(4, item.getDescp());
            ps.setString(5, item.getManagecode());
            ps.setString(6, item.getRemarks());
            ps.setDouble(7, item.getMaxcash());
            ps.setDouble(8, item.getMaxfloat());
           
            ps.executeUpdate();
            Logger.getLogger(ManagementCpDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

       } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void updatePetty(LoginProfile log, CeManagePetty item) throws Exception{
        
        try{
            String q = ("UPDATE ce_manage_petty set coacode = ?,coadesc = ?,code = ?,descp = ?,managecode = ?,remarks = ?,maxcash = ?,maxfloat = ? WHERE code = '"+item.getCode()+"'");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCoacode());
            ps.setString(2, item.getCoadesc());
            ps.setString(3, item.getCode());
            ps.setString(4, item.getDescp());
            ps.setString(5, item.getManagecode());
            ps.setString(6, item.getRemarks());
            ps.setDouble(7, item.getMaxcash());
            ps.setDouble(8, item.getMaxfloat());
           
            ps.executeUpdate();
            Logger.getLogger(ManagementCpDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

       } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static int deletePetty(LoginProfile log, String refer) throws SQLException, Exception{
         
        int x = 0;
        
        try{
            String deleteQuery_2 = "delete from ce_manage_petty where code= ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, refer);
            ps_2.executeUpdate();
            ps_2.close();
            x = 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return x;
    }
    
}
