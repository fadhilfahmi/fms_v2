/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.company;

import com.lcsb.fms.dao.setup.configuration.ExecDAO;
import com.lcsb.fms.model.setup.company.staff.Staff;
import com.lcsb.fms.model.setup.company.staff.StaffBank;
import com.lcsb.fms.model.setup.company.staff.StaffChild;
import com.lcsb.fms.model.setup.company.staff.StaffDesignation;
import com.lcsb.fms.model.setup.company.staff.StaffSpouse;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class StaffDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("010225");
        mod.setModuleDesc("Staff Information");
        mod.setMainTable("co_staff");
        mod.setReferID_Master("staffid");
        return mod;

    }

    public static List<ListTable> tableList() {

        String column_names[] = {"staffid", "name", "status", "ic"};
        String title_name[] = {"Id", "Name", "Service Status", "I/C No."};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }

        return lt;
    }

    public static void saveData(LoginProfile log, Staff item) throws Exception {

        try {

            String q = ("INSERT INTO `co_staff` (`id`, `staffid`, `name`, `status`, `ic`, `birth`, `sex`, `marital`, `religion`, `race`, `citizen`, `address`, `city`, `state`, `postcode`, `phone`, `hp`, `fax`, `email`, `remarks`, `pobirth`,`datejoin`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, null);
                ps.setString(2, item.getStaffid());
                ps.setString(3, item.getName());
                ps.setString(4, item.getStatus());
                ps.setString(5, item.getIc());
                ps.setString(6, item.getBirth());
                ps.setString(7, item.getSex());
                ps.setString(8, item.getMarital());
                ps.setString(9, item.getReligion());
                ps.setString(10, item.getRace());
                ps.setString(11, item.getCitizen());
                ps.setString(12, item.getAddress());
                ps.setString(13, item.getCity());
                ps.setString(14, item.getState());
                ps.setString(15, item.getPostcode());
                ps.setString(16, item.getPhone());
                ps.setString(17, item.getHp());
                ps.setString(18, item.getFax());
                ps.setString(19, item.getEmail());
                ps.setString(20, item.getRemarks());
                ps.setString(21, item.getPobirth());
                ps.setString(22, item.getDatejoin());

                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void saveDesignation(LoginProfile log, StaffDesignation item) throws Exception {

        try {

            String q = ("INSERT INTO `co_staff_designation` (`id`, `staffid`, `dept_code`, `dept_descp`, `datejoin`, `dateend`, `designation`,`status`, `worklocation`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, null);
                ps.setString(2, item.getStaffid());
                ps.setString(3, item.getDeptCode());
                ps.setString(4, item.getDeptDescp());
                ps.setString(5, item.getDatejoin());
                ps.setString(6, item.getDateend());
                ps.setString(7, item.getDesignation());
                ps.setString(8, item.getStatus());
                ps.setString(9, item.getWorklocation());

                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
    public static void saveBank(LoginProfile log, StaffBank item) throws Exception {

        try {

            String q = ("INSERT INTO `co_staff_bank` (`id`, `staffid`, `bankcode`, `bankdesc`, `accno`, `status`) VALUES (?, ?, ?, ?, ?, ?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, null);
                ps.setString(2, item.getStaffid());
                ps.setString(3, item.getBankcode());
                ps.setString(4, item.getBankdesc());
                ps.setString(5, item.getAccno());
                ps.setString(6, item.getStatus());

                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateDesignation(LoginProfile log, StaffDesignation item) throws Exception {

        try {

            String q = ("UPDATE `co_staff_designation` SET `staffid` = ?, `dept_code` = ?, `dept_descp` = ?, `datejoin` = ?, `dateend` = ?, `designation` = ?,`status` = ?, `worklocation` = ? WHERE id = '" + item.getId() + "'");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, item.getStaffid());
                ps.setString(2, item.getDeptCode());
                ps.setString(3, item.getDeptDescp());
                ps.setString(4, item.getDatejoin());
                ps.setString(5, item.getDateend());
                ps.setString(6, item.getDesignation());
                ps.setString(7, item.getStatus());
                ps.setString(8, item.getWorklocation());

                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
    public static void updateBank(LoginProfile log, StaffBank item) throws Exception {

        try {

            String q = ("UPDATE `co_staff_bank` SET `staffid` = ?, `bankcode` = ?, `bankdesc` = ?, `accno` = ?,`status` = ? WHERE id = '" + item.getId() + "'");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, item.getStaffid());
                ps.setString(2, item.getBankcode());
                ps.setString(3, item.getBankdesc());
                ps.setString(4, item.getAccno());
                ps.setString(5, item.getStatus());

                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteDesignation(LoginProfile log, String no) throws SQLException, Exception {

        try {
            String deleteQuery_2 = "delete from co_staff_designation where id = ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, no);
            ps_2.executeUpdate();
            ps_2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
    public static void deleteBank(LoginProfile log, String no) throws SQLException, Exception {

        try {
            String deleteQuery_2 = "delete from co_staff_bank where id = ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, no);
            ps_2.executeUpdate();
            ps_2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void saveSpouse(LoginProfile log, StaffSpouse item) throws Exception {

        try {

            String q = ("INSERT INTO `co_staff_spouse` (`id`, `staffid`, `citizen`, `dob`, `ic`, `name`, `passport`,`race`, `religion`, `working`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, null);
                ps.setString(2, item.getStaffid());
                ps.setString(3, item.getCitizen());
                ps.setString(4, item.getDob());
                ps.setString(5, item.getIc());
                ps.setString(6, item.getName());
                ps.setString(7, item.getPassport());
                ps.setString(8, item.getRace());
                ps.setString(9, item.getReligion());
                ps.setString(10, item.getWorking());

                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateSpouse(LoginProfile log, StaffSpouse item) throws Exception {

        try {

            String q = ("UPDATE `co_staff_spouse` SET `staffid` = ?, `citizen` = ?, `dob` = ?, `ic` = ?, `name` = ?, `passport` = ?,`race` = ?, `religion` = ?, `working` = ? WHERE id = '" + item.getId() + "'");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, item.getStaffid());
                ps.setString(2, item.getCitizen());
                ps.setString(3, item.getDob());
                ps.setString(4, item.getIc());
                ps.setString(5, item.getName());
                ps.setString(6, item.getPassport());
                ps.setString(7, item.getRace());
                ps.setString(8, item.getReligion());
                ps.setString(9, item.getWorking());

                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteSpouse(LoginProfile log, String no) throws SQLException, Exception {

        try {
            String deleteQuery_2 = "delete from co_staff_spouse where id = ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, no);
            ps_2.executeUpdate();
            ps_2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateBasis(LoginProfile log, Staff item) throws Exception {

        try {
            String q = ("UPDATE co_staff SET `staffid` = ?, `name` = ?, `status` = ?, `ic` = ?, `birth` = ?, `sex` = ?, `marital` = ?, `religion` = ?, `race` = ?, `citizen` = ?, `address` = ?, `city` = ?, `state` = ?, `postcode` = ?, `phone` = ?, `hp` = ?, `fax` = ?, `email` = ?, `remarks` = ?, `pobirth` = ?, `title` = ?, `datejoin` = ?, `oldic` = ? WHERE staffid = '" + item.getStaffid() + "'");

            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, item.getStaffid());
                ps.setString(2, item.getName());
                ps.setString(3, item.getStatus());
                ps.setString(4, item.getIc());
                ps.setString(5, item.getBirth());
                ps.setString(6, item.getSex());
                ps.setString(7, item.getMarital());
                ps.setString(8, item.getReligion());
                ps.setString(9, item.getRace());
                ps.setString(10, item.getCitizen());
                ps.setString(11, item.getAddress());
                ps.setString(12, item.getCity());
                ps.setString(13, item.getState());
                ps.setString(14, item.getPostcode());
                ps.setString(15, item.getPhone());
                ps.setString(16, item.getHp());
                ps.setString(17, item.getFax());
                ps.setString(18, item.getEmail());
                ps.setString(19, item.getRemarks());
                ps.setString(20, item.getPobirth());
                ps.setString(21, item.getTitle());
                ps.setString(22, item.getDatejoin());
                ps.setString(23, item.getOldic());
                Logger.getLogger(ExecDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
                ps.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static Staff getInfo(LoginProfile log, String no) throws SQLException, Exception {
        Staff c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff WHERE staffid=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        return c;
    }

    public static Staff getInfo(ResultSet rs) throws SQLException {

        Staff g = new Staff();

        g.setAddress(rs.getString("address"));
        g.setBirth(rs.getString("birth"));
        g.setCitizen(rs.getString("citizen"));
        g.setCity(rs.getString("city"));
        g.setEmail(rs.getString("email"));
        g.setFax(rs.getString("fax"));
        g.setHp(rs.getString("hp"));
        g.setMarital(rs.getString("marital"));
        g.setName(rs.getString("name"));
        g.setIc(rs.getString("ic"));
        g.setPhone(rs.getString("phone"));
        g.setPostcode(rs.getString("postcode"));
        g.setRace(rs.getString("race"));
        g.setReligion(rs.getString("religion"));
        g.setSex(rs.getString("sex"));
        g.setState(rs.getString("state"));
        g.setStatus(rs.getString("status"));
        g.setStaffid(rs.getString("staffid"));
        g.setPobirth(rs.getString("pobirth"));
        g.setRemarks(rs.getString("remarks"));
        g.setOldic(rs.getString("oldic"));
        g.setTitle(rs.getString("title"));
        g.setStaffid(rs.getString("staffid"));

        return g;
    }

    public static List<Staff> getAllStaff(LoginProfile log) {

//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.lcsb_fms_mvn_war_1.0-SNAPSHOTPU");
//
//        EntityManager em = emf.createEntityManager();
//
//        Query query = em.createNamedQuery("Staff.findAll");
//
//        List<Staff> resultList = query.getResultList();
//        for (Staff c : resultList) {
//
//            resultList.add(c);
//
//        }
        ResultSet rs = null;
        List<Staff> CVi;
        CVi = new ArrayList();
        Staff c = new Staff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static List<StaffDesignation> getAllDesignation(LoginProfile log, String staffid) {

        ResultSet rs = null;
        List<StaffDesignation> CVi;
        CVi = new ArrayList();
        StaffDesignation c = new StaffDesignation();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff_designation where staffid=?");
            stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfoDesignation(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static StaffDesignation getDesignation(LoginProfile log, String no) throws SQLException, Exception {
        StaffDesignation c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff_designation WHERE id=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfoDesignation(rs);
        }

        return c;
    }
    
    public static StaffDesignation getCurrentDesignation(LoginProfile log, String no) throws SQLException, Exception {
        StaffDesignation c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff_designation WHERE staffid=? and status = ?");
        stmt.setString(1, no);
        stmt.setString(2, "Yes");
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfoDesignation(rs);
        }

        return c;
    }

    private static StaffDesignation getInfoDesignation(ResultSet rs) throws SQLException {

        StaffDesignation g = new StaffDesignation();

        g.setDateend(rs.getString("dateend"));
        g.setDatejoin(rs.getString("datejoin"));
        g.setDeptCode(rs.getString("dept_code"));
        g.setDeptDescp(rs.getString("dept_descp"));
        g.setStaffid(rs.getString("staffid"));
        g.setStatus(rs.getString("status"));
        g.setDesignation(rs.getString("designation"));
        g.setWorklocation(rs.getString("worklocation"));
        g.setId(rs.getInt("id"));

        return g;
    }
    
    public static List<StaffBank> getAllBank(LoginProfile log, String staffid) {

        ResultSet rs = null;
        List<StaffBank> CVi;
        CVi = new ArrayList();
        StaffBank c = new StaffBank();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff_bank where staffid=?");
            stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfoBank(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static StaffBank getBank(LoginProfile log, String no) throws SQLException, Exception {
        StaffBank c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff_bank WHERE id=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfoBank(rs);
        }

        return c;
    }
    
    private static StaffBank getInfoBank(ResultSet rs) throws SQLException {

        StaffBank g = new StaffBank();

        g.setAccno(rs.getString("accno"));
        g.setBankcode(rs.getString("bankcode"));
        g.setBankdesc(rs.getString("bankdesc"));
        g.setStaffid(rs.getString("staffid"));
        g.setStatus(rs.getString("status"));
        g.setId(rs.getInt("id"));

        return g;
    }

    public static List<StaffSpouse> getAllSpouse(LoginProfile log, String staffid) {

        ResultSet rs = null;
        List<StaffSpouse> CVi;
        CVi = new ArrayList();
        StaffDesignation c = new StaffDesignation();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff_spouse where staffid=?");
            stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfoSpouse(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static StaffSpouse getSpouse(LoginProfile log, String no) throws SQLException, Exception {
        StaffSpouse c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff_spouse WHERE id=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfoSpouse(rs);
        }

        return c;
    }

    private static StaffSpouse getInfoSpouse(ResultSet rs) throws SQLException {

        StaffSpouse g = new StaffSpouse();

        g.setCitizen(rs.getString("citizen"));
        g.setDivorce(rs.getString("divorce"));
        g.setDob(rs.getString("dob"));
        g.setDom(rs.getString("dom"));
        g.setIc(rs.getString("ic"));
        g.setId(rs.getInt("id"));
        g.setName(rs.getString("name"));
        g.setPassport(rs.getString("passport"));
        g.setRace(rs.getString("race"));
        g.setReligion(rs.getString("religion"));
        g.setSpouseno(rs.getString("spouseno"));
        g.setStaffid(rs.getString("staffid"));
        g.setTaxno(rs.getString("taxno"));
        g.setWork(rs.getString("work"));
        g.setWorking(rs.getString("working"));

        return g;
    }

    //Children
    public static List<StaffChild> getAllChild(LoginProfile log, String staffid, boolean tax) {

        ResultSet rs = null;
        List<StaffChild> CVi;
        CVi = new ArrayList();
        StaffDesignation c = new StaffDesignation();
        
        String q = "";
        if(tax){
            q = " and taxdependent = true";
        }

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff_child where staffid=? "+q+" order by no asc");
            stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfoChild(rs));
            }
            Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(stmt));
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static StaffChild getChild(LoginProfile log, String no) throws SQLException, Exception {
        StaffChild c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff_child WHERE id=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfoChild(rs);
        }

        return c;
    }

    private static StaffChild getInfoChild(ResultSet rs) throws SQLException {

        StaffChild g = new StaffChild();

        g.setAlive(rs.getString("alive"));
        g.setBirthno(rs.getString("birthno"));
        g.setCitizen(rs.getString("citizen"));
        g.setDob(rs.getString("dob"));
        g.setGender(rs.getString("gender"));
        g.setIc(rs.getString("ic"));
        g.setId(rs.getInt("id"));
        g.setMotherid(rs.getString("motherid"));
        g.setName(rs.getString("name"));
        g.setNo(rs.getString("no"));
        g.setRace(rs.getString("race"));
        g.setReligion(rs.getString("religion"));
        g.setStaffid(rs.getString("staffid"));
        g.setTaxdependent(rs.getBoolean("taxdependent"));

        return g;
    }

    public static void saveChild(LoginProfile log, StaffChild item) throws Exception {

        try {

            String q = ("INSERT INTO `co_staff_child` (`id`, `alive`, `birthno`, `citizen`, `dob`, `gender`, `ic`,`motherid`, `name`, `no` ,`race`, `religion`, `staffid`,`taxdependent`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, null);
                ps.setString(2, item.getAlive());
                ps.setString(3, item.getBirthno());
                ps.setString(4, item.getCitizen());
                ps.setString(5, item.getDob());
                ps.setString(6, item.getGender());
                ps.setString(7, item.getIc());
                ps.setString(8, item.getMotherid());
                ps.setString(9, item.getName());
                ps.setString(10, item.getNo());
                ps.setString(11, item.getRace());
                ps.setString(12, item.getReligion());
                ps.setString(13, item.getStaffid());
                ps.setBoolean(14, item.isTaxdependent());

                ps.executeUpdate();
                ps.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateChild(LoginProfile log, StaffChild item) throws Exception {

        try {

            String q = ("UPDATE `co_staff_child` SET `alive` = ?, `birthno` = ?, `citizen` = ?, `dob` = ?, `gender` = ?, `ic` = ?,`motherid` = ?, `name` = ?, `no` = ? ,`race` = ?, `religion` = ?, `staffid` = ?, `taxdependent` = ? WHERE id = '" + item.getId() + "'");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, item.getAlive());
                ps.setString(2, item.getBirthno());
                ps.setString(3, item.getCitizen());
                ps.setString(4, item.getDob());
                ps.setString(5, item.getGender());
                ps.setString(6, item.getIc());
                ps.setString(7, item.getMotherid());
                ps.setString(8, item.getName());
                ps.setString(9, item.getNo());
                ps.setString(10, item.getRace());
                ps.setString(11, item.getReligion());
                ps.setString(12, item.getStaffid());
                ps.setBoolean(13, item.isTaxdependent());

                ps.executeUpdate();
                Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(ps));
                ps.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteChild(LoginProfile log, String no) throws SQLException, Exception {

        try {
            String deleteQuery_2 = "delete from co_staff_child where id = ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, no);
            ps_2.executeUpdate();
            ps_2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static String getMotherList(LoginProfile log, String staffid, String selected) throws Exception {

        ResultSet rs = null;
        String drop = "";
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff_spouse where staffid = ?");
            stmt.setString(1, staffid);
            rs = stmt.executeQuery();

            while (rs.next()) {
                if (rs.getString("id").equals(selected) && !selected.equals("")) {
                    drop += "<option value = \"" + rs.getString("id") + "\" selected>" + rs.getString("name") + "</option>";
                } else {
                    drop += "<option value = \"" + rs.getString("id") + "\">" + rs.getString("name") + "</option>";
                }

            }

            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return drop;
    }

    public static String childListNumber(LoginProfile log, String type, String selected, String staffid) throws Exception {

        ResultSet rs = null;
        String drop = "";
        try {

            PreparedStatement stmt = log.getCon().prepareStatement("select * from parameter where parameter = ? order by  dorder");
            stmt.setString(1, type);
            rs = stmt.executeQuery();

            while (rs.next()) {

                if (selected.equals("")) {
                    if (!getChildNumber(log, rs.getString("value"), staffid)) {

                        if (rs.getString("value").equals(selected) && !selected.equals("")) {
                            drop += "<option value = \"" + rs.getString("value") + "\" selected>" + rs.getString("value") + "</option>";
                        } else {
                            drop += "<option value = \"" + rs.getString("value") + "\">" + rs.getString("value") + "</option>";
                        }

                    }
                } else {
                    if (rs.getString("value").equals(selected) && !selected.equals("")) {
                        drop += "<option value = \"" + rs.getString("value") + "\" selected>" + rs.getString("value") + "</option>";
                    }
                    if (!getChildNumber(log, rs.getString("value"), staffid)) {

                        drop += "<option value = \"" + rs.getString("value") + "\">" + rs.getString("value") + "</option>";

                    }
                }
            }

            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return drop;
    }

    private static boolean getChildNumber(LoginProfile log, String no, String staffid) throws SQLException, Exception {
        boolean e = false;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff_child WHERE no=? AND staffid = ?");
        stmt.setString(1, no);
        stmt.setString(2, staffid);
        rs = stmt.executeQuery();
        if (rs.next()) {

            e = true;

        }

        return e;
    }

    public static boolean getChildBeneathMother(LoginProfile log, int motherid, String staffid) throws SQLException, Exception {
        boolean e = false;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM co_staff_child WHERE motherid=? AND staffid = ?");
        stmt.setInt(1, motherid);
        stmt.setString(2, staffid);
        rs = stmt.executeQuery();
        if (rs.next()) {

            e = true;

        }

        return e;
    }
    
    public static List<Staff> getAllHQStaff(LoginProfile log) {

        ResultSet rs = null;
        List<Staff> CVi;
        CVi = new ArrayList();
        Staff c = new Staff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from co_staff a, co_staff_designation b where a.staffid = b.staffid and b.worklocation = 'Headquarters' order by a.name");
            //stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                
                Staff st = (Staff) getInfo(rs);
                st.setPposition(rs.getString("b.designation"));
                CVi.add(st);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static Staff getHQStaff(LoginProfile log, String staffid) {

        ResultSet rs = null;
        Staff c = new Staff();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * FROM `co_staff` `m` LEFT OUTER JOIN `co_staff_designation` `r` ON `m`.`staffid` = `r`.`staffid`  and r.status = 'Yes' and r.worklocation = 'Headquarters' LEFT OUTER JOIN `co_staff_bank` `p` ON `m`.`staffid` = `p`.`staffid` and p.status='Yes' where m.staffid = ?");
            stmt.setString(1, staffid);
            rs = stmt.executeQuery();
            Logger.getLogger(StaffDAO.class.getName()).log(Level.INFO, "-------" + String.valueOf(stmt));
            while (rs.next()) {
                
                c = getInfo(rs);
                c.setPposition(rs.getString("r.designation"));
                c.setDepartment(rs.getString("r.dept_descp"));
                c.setBankcode(rs.getString("p.bankcode"));
                c.setBankdesc(rs.getString("p.bankdesc"));
                c.setAccno(rs.getString("p.accno"));
                
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

}
