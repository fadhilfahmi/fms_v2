/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.company;

import com.lcsb.fms.model.setup.company.BankBranch;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.company.Bank;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BankDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("010229");
        mod.setModuleDesc("Bank Information");
        mod.setMainTable("info_bank");
        mod.setReferID_Master("code");
        return mod;

    }

    public static List<ListTable> tableList() {

        String column_names[] = {"code", "name", "abbreviation"};
        String title_name[] = {"Code", "Bank Name", "Abbreviation"};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }

        return lt;
    }

    public static void saveData(LoginProfile log, Bank item) throws Exception {
        
        try {

            String q = ("insert into info_bank(code, name, abbreviation, coa, coadescp,overdue, remarks) values (?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, AutoGenerate.get2digitNo(log, "info_bank", "code"));
            ps.setString(2, item.getName());
            ps.setString(3, item.getAbbreviation());
            ps.setString(4, item.getCoa());
            ps.setString(5, item.getCoadescp());
            ps.setInt(6, item.getOverdue());
            ps.setString(7, item.getRemarks());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void saveDataBranch(LoginProfile log, BankBranch item) throws Exception {

        //String no, String bankcode, String bankname, Date tarikh, String startcek, int nocek, String endcek, String active)
        try {

            String q = ("insert into info_branch(bankcode,bankname,branchname,branchcode,person,title,position,hp,address,city,state,postcode,phone,fax,email,url,remarks) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getBankcode());
            ps.setString(2, item.getBankname());
            ps.setString(3, item.getBranchname());
            ps.setString(4, AutoGenerate.getBranchCode(log, "info_branch", "branchcode", "bankcode", item.getBankcode()));
            ps.setString(5, item.getPerson());
            ps.setString(6, item.getTitle());
            ps.setString(7, item.getPosition());
            ps.setString(8, item.getHp());
            ps.setString(9, item.getAddress());
            ps.setString(10, item.getCity());
            ps.setString(11, item.getState());
            ps.setString(12, item.getPostcode());
            ps.setString(13, item.getPhone());
            ps.setString(14, item.getFax());
            ps.setString(15, item.getEmail());
            ps.setString(16, item.getRemarks());
            ps.setString(17, item.getRemarks());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void editDataBranch(LoginProfile log, BankBranch item) throws Exception {

        try {

            String q = ("update info_branch set bankcode = ?,bankname = ?,branchname = ?,branchcode = ?,person = ?,title = ?,position = ?,hp = ?,address = ?,city = ?,state = ?,postcode = ?,phone = ?,fax = ?,email = ?,url = ?,remarks = ? WHERE branchcode = '" + item.getBranchcode() + "'");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getBankcode());
            ps.setString(2, item.getBankname());
            ps.setString(3, item.getBranchname());
            ps.setString(4, item.getBranchcode());
            ps.setString(5, item.getPerson());
            ps.setString(6, item.getTitle());
            ps.setString(7, item.getPosition());
            ps.setString(8, item.getHp());
            ps.setString(9, item.getAddress());
            ps.setString(10, item.getCity());
            ps.setString(11, item.getState());
            ps.setString(12, item.getPostcode());
            ps.setString(13, item.getPhone());
            ps.setString(14, item.getFax());
            ps.setString(15, item.getEmail());
            ps.setString(16, item.getRemarks());
            ps.setString(17, item.getRemarks());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteBank(LoginProfile log, String no) throws SQLException, Exception {


        try {
            String deleteQuery_2 = "delete from info_bank where code = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, no);
                ps_2.executeUpdate();
            }
        } catch (SQLException e) {
        }

    }

    public static void deleteBranch(LoginProfile log, String no) throws SQLException, Exception {


        try {
            String deleteQuery_2 = "delete from info_branch where branchcode = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, no);
                ps_2.executeUpdate();
            }
        } catch (SQLException e) {
        }

    }

    public static void updateItem(LoginProfile log, Bank item) throws Exception {


        try {
            String q = ("UPDATE info_bank SET code=? ,name=? , abbreviation=? , coa=? , coadescp=? , overdue=? , remarks=?  WHERE code = '" + item.getCode() + "'");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCode());
            ps.setString(2, item.getName());
            ps.setString(3, item.getAbbreviation());
            ps.setString(4, item.getCoa());
            ps.setString(5, item.getCoadescp());
            ps.setInt(6, item.getOverdue());
            ps.setString(7, item.getRemarks());

            Logger.getLogger(BankDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static Bank getInfo(LoginProfile log, String no) throws SQLException, Exception {
        
        Bank c = null;
        
        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM info_bank WHERE code=?");
            stmt.setString(1, no);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getInfo(rs);
            }
            Logger.getLogger(BankDAO.class.getName()).log(Level.INFO, String.valueOf(c.getCode()));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    private static Bank getInfo(ResultSet rs) throws SQLException {
        Bank g = new Bank();
        g.setCode(rs.getString("code"));
        g.setName(rs.getString("name"));
        g.setAbbreviation(rs.getString("abbreviation"));
        g.setCoa(rs.getString("coa"));
        g.setCoadescp(rs.getString("coadescp"));
        g.setOverdue(Integer.parseInt(rs.getString("overdue")));
        g.setRemarks(rs.getString("remarks"));

        return g;
    }

    public static BankBranch getBranch(LoginProfile log, String no) throws SQLException, Exception {
        BankBranch c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM info_branch WHERE branchcode=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getBranch(rs);
        }

        return c;
    }

    public static List<BankBranch> getAllBranch(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<BankBranch> pvr;
        pvr = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from info_branch where bankcode = '" + refno + "' order by branchcode");

            while (rs.next()) {
                pvr.add(getBranch(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return pvr;
    }

    private static BankBranch getBranch(ResultSet rs) throws SQLException {
        BankBranch cb = new BankBranch();

        cb.setAddress(rs.getString("address"));
        cb.setBankcode(rs.getString("bankcode"));
        cb.setBankname(rs.getString("bankname"));
        cb.setBranchcode(rs.getString("branchcode"));
        cb.setBranchname(rs.getString("branchname"));
        cb.setCity(rs.getString("city"));
        cb.setEmail(rs.getString("email"));
        cb.setFax(rs.getString("fax"));
        cb.setHp(rs.getString("hp"));
        cb.setPerson(rs.getString("person"));
        cb.setPhone(rs.getString("phone"));
        cb.setPosition(rs.getString("position"));
        cb.setPostcode(rs.getString("postcode"));
        cb.setRemarks(rs.getString("remarks"));
        cb.setTitle(rs.getString("title"));
        cb.setState(rs.getString("state"));
        cb.setUrl(rs.getString("url"));

        return cb;
    }

}
