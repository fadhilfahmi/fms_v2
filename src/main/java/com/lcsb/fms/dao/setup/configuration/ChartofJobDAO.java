/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.configuration;
import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.dao.financial.cashbook.PaymentVoucherDAO;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucher;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem;
import com.lcsb.fms.model.setup.configuration.ChartofJob;
import com.lcsb.fms.model.setup.configuration.ChartofJobItem;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class ChartofJobDAO {
    
    public static PathModel PathTo(LoginProfile log, String moduleid, String process, ChartofJob data, ChartofJobItem item, String referno) throws Exception{
        
        String urlsend = "";
        PathModel pm = new PathModel();
        
        try{
            if(process.equals("viewlist")){
                urlsend = "/cf_coj_list.jsp?id="+moduleid;

            }else if(process.equals("addnew")){
                urlsend = "/cf_coj_add.jsp";

            }else if(process.equals("addprocess")){

               ChartofJobDAO.saveData(log, data);
                urlsend = "/cf_coj_list.jsp";

            }else if(process.equals("addnewitem")){
                urlsend = "/cf_coj_add_item.jsp?referno="+referno; 

            }else if(process.equals("additem")){
                try{

                    ChartofJobDAO.saveItem(log, item);
                    urlsend = "/cf_coj_list.jsp?id="+moduleid;//+"&referenceno="+request.getParameter("referenceno");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }else if(process.equals("edititemlist")){
                urlsend = "/cf_coj_edit_list.jsp?referno="+referno;

            }else if(process.equals("viewpv")){
                urlsend = "/cf_coj_view_voucher.jsp?referno="+referno;

            }else if(process.equals("edit")){
                urlsend = "cf_coj_edit.jsp?referno="+referno;

            }else if(process.equals("editprocess")){

                ChartofJobDAO.updateData(log, data);
                urlsend = "/cf_coj_list.jsp?";

            }else if(process.equals("edititemprocess")){
                
                ChartofJobDAO.updateItem(log, item,String.valueOf(item.getId()));
                urlsend = "/cf_coj_edit_list.jsp?referno="+item.getJobcode();
                

            }else if(process.equals("edititem")){
                urlsend = "/cf_coj_edit_item.jsp?referno="+referno;

            }else if(process.equals("post")){
                urlsend = "/gl_post.jsp?referno="+referno+"&vtype=SNV";

            }else if(process.equals("dist")){
                //Distribute dist = (Distribute) DistributeDAO.distributeNow(request.getParameter("referno"),"SNV");
                //session.setAttribute("post_detail", dist);
                //if(dist.isSuccess()){
                //    urlsend = "/gl_post_status.jsp";
                //}else{
                //    urlsend = "/gl_post_status.jsp";
                //}
            }else if(process.equals("delete")){
                ChartofJobDAO.deleteExist(log, referno);
                urlsend = "/cf_coj_list.jsp?id="+moduleid;//+"&referenceno="+request.getParameter("referenceno");

            }else if(process.equals("check")){
                //AgreementDAO.checkVoucher(referno,log.getUserID(), log.getUserName());
                urlsend = "/cf_coj_list.jsp?id="+moduleid;//+"&referenceno="+request.getParameter("referenceno");

            }else if(process.equals("approve")){
                //AgreementDAO.approveVoucher(request.getParameter("referno"),log.getUserID(), log.getUserName(), log.getPosition());
                urlsend = "/cf_coj_list.jsp?id="+moduleid;//+"&referenceno="+request.getParameter("referenceno");

            }else{

                ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                ErrorIO.setCode("100");//nullpointer
                ErrorIO.setType("data");
                urlsend = "/error.jsp";
            }   
        } catch (Exception e) {
            e.printStackTrace();
            ErrorIO.setError(String.valueOf(e));
            ErrorIO.setCode("100");//nullpointer
            ErrorIO.setType("data");
            urlsend = "/error.jsp";
        }
        
        
        pm.setUrlSend(urlsend);
            
        return pm;
    }
     public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010111");
        mod.setModuleDesc("Chart of Job Information");
        mod.setMainTable("chartofjob");
        mod.setReferID_Master("code");
        return mod;
         
    }
    
     public static List<ListTable> tableList(){
        
        String column_names[] = {"code","descp","finallvl"};
        String title_name[] = {"Code","Description","Final Level"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
     
    public static void saveData(LoginProfile log, ChartofJob item) throws Exception{
        
        try {
            
            String q = ("insert into chartofjob(code, descp, type1,  type2,  finallvl,  active) values (?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCode());
            ps.setString(2, item.getDescp());
            ps.setString(3, item.getType1());
            ps.setString(4, item.getType2());
            ps.setString(5, item.getFinallvl());
            ps.setString(6, item.getActive());
            
            
           ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
     public static void updateItem(LoginProfile log, ChartofJobItem item, String id) throws Exception{
        
        
        try{
            String q = ("UPDATE addcoa set accountcode=?,jobcode=?,accountdescp=?,remarks=? WHERE id = '"+id+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getAccountcode());
            ps.setString(2, item.getJobcode());
            ps.setString(3, item.getAccountdescp());
            ps.setString(4, item.getRemarks());
            
            
            
            Logger.getLogger(ChartofJobDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
        
        try{
            String deleteQuery_2 = "delete from broker_info where code = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, no);
                ps_2.executeUpdate();
            }
        } catch (SQLException e) {
        }
        
        
    }
    
    public static void updateData(LoginProfile log, ChartofJob item) throws Exception{
        
        
        try{
            String q = ("UPDATE chartofjob SET code=? , descp=? , type1=? ,  type2=? ,  finallvl=? ,  active=?  WHERE code = '"+item.getCode()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            
            ps.setString(1, item.getCode());
            ps.setString(2, item.getDescp());
            ps.setString(3, item.getType1());
            ps.setString(4, item.getType2());
            ps.setString(5, item.getFinallvl());
            ps.setString(6, item.getActive());
           
           Logger.getLogger(BrokerDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static ChartofJob getInfo(LoginProfile log, String no) throws SQLException, Exception {
       ChartofJob c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM chartofjob WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static ChartofJob getInfo(ResultSet rs) throws SQLException {
        ChartofJob g = new ChartofJob();
        
        g.setCode(rs.getString("code"));
        g.setDescp(rs.getString("descp"));
        g.setType1(rs.getString("type1"));
        g.setType2(rs.getString("type2"));
        g.setFinallvl(rs.getString("finallvl"));
        g.setActive(rs.getString("active"));
       
       
        
        
       
        return g;
    }
    
    public static ChartofJobItem getInfoItem(LoginProfile log, String no) throws SQLException, Exception {
       ChartofJobItem c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM addcoa WHERE id=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfoItem(rs);
        }

        
        return c;
    }
    
    private static ChartofJobItem getInfoItem(ResultSet rs) throws SQLException {
        ChartofJobItem g = new ChartofJobItem();
        
        g.setAccountcode(rs.getString("accountcode"));
        g.setJobcode(rs.getString("jobcode"));
        g.setAccountdescp(rs.getString("accountdescp"));
        g.setRemarks(rs.getString("remarks"));
        g.setId(Integer.parseInt(rs.getString("id")));
        
       
       
        
        
       
        return g;
    }/*
    public static List<ChartofJob> getAllCJ(String refno) throws Exception {

        Connection con = ConnectionUtil.getConnection();
        Statement stmt = null;
        List<ChartofJob> CV;
        CV = new ArrayList();
        
        
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from chartofjob where code = '"+refno+"' order by code");

            while (rs.next()) {
                CV.add(getCJ(de));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CV;
    }
    */
    public static ChartofJob getCJ(LoginProfile log, String refno) throws SQLException, Exception {
        ChartofJob c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from chartofjob where code=?");
        stmt.setString(1, refno);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getPUO(rs);
        }

        
        return c;
    }
    
    private static ChartofJob getPUO(ResultSet rs) throws SQLException {
        
        ChartofJob cv = new ChartofJob();
     
        cv.setCode(rs.getString("code"));
        cv.setDescp(rs.getString("descp"));
        cv.setType1(rs.getString("type1"));
        cv.setType2(rs.getString("type2"));
        cv.setFinallvl(rs.getString("finallvl"));
        cv.setActive(rs.getString("active"));
        
        
        
        return cv;
    }
    public static void saveItem(LoginProfile log, ChartofJobItem item) throws Exception{
        
        try{
            String q = ("insert into addcoa(accountcode,jobcode,accountdescp,remarks) values (?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            //ps.setLong(1, item.getBil());
            ps.setString(1, item.getAccountcode());
            ps.setString(2, item.getJobcode());
            //ps.setString(3, AutoGenerate.getReferAddBack("ph_po_detail", "porefno", item.getNo(), "no"));//<--porefno
            ps.setString(3, item.getAccountdescp());
            ps.setString(4, item.getRemarks());
            
            
            
            ps.executeUpdate();
            Logger.getLogger(ChartofJobDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

       } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    public static List<ChartofJobItem> getAllCJIItem(LoginProfile log, String refno) throws Exception {

        Statement stmt = null;
        List<ChartofJobItem> CVi;
        CVi = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from addcoa where jobcode = '"+refno+"' order by id");

            while (rs.next()) {
                CVi.add(getCJIitem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    public static ChartofJobItem getCJIitem(LoginProfile log, String id) throws SQLException, Exception {
        ChartofJobItem c = null;
        ResultSet rs = null;
        try{
            PreparedStatement stmt = log.getCon().prepareStatement("select * from addcoa where id=?");
            stmt.setString(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getCJIitem(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        
        return c;
    }
    
    public static void deleteExist(LoginProfile log, String no) throws SQLException, Exception{
        Connection con = log.getCon();
        
        String deleteQuery_1 = "delete from chartofjob where code = ?";
        String deleteQuery_2 = "delete from addcoa where id = ?";
        PreparedStatement ps_1 = con.prepareStatement(deleteQuery_1);
        PreparedStatement ps_2 = con.prepareStatement(deleteQuery_2);
        ps_1.setString(1, no);
        ps_2.setString(1, no);
        ps_1.executeUpdate();
        ps_2.executeUpdate();
        ps_1.close();
        ps_2.close();
        
        
    }
    
    private static ChartofJobItem getCJIitem(ResultSet rs) throws SQLException {
        ChartofJobItem cv = new ChartofJobItem();
        
        cv.setAccountcode(rs.getString("accountcode"));
        cv.setJobcode(rs.getString("jobcode"));
        cv.setAccountdescp(rs.getString("accountdescp"));
        cv.setRemarks(rs.getString("remarks"));
        cv.setId(Integer.parseInt(rs.getString("id")));
       
        return cv;
    }
    
}
