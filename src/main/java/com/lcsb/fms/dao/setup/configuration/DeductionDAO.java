/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.configuration;

import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.configuration.Deduction;
import com.lcsb.fms.model.setup.configuration.Earning;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class DeductionDAO {
  
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010135");
        mod.setModuleDesc("Deduction Information");
        mod.setMainTable("cf_deductioninfo");
        mod.setReferID_Master("code");
        return mod;
         
    }
    public static List<ListTable> tableList(){
        
        String column_names[] = {"code","descp","status"};
        String title_name[] = {"Code","Descp","Status"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
    
    public static void saveData(LoginProfile log, Deduction item) throws Exception{
        
        try {
            
            String q = ("insert into cf_deductioninfo(code,descp,status) values (?,?,?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, AutoGenerate.get4digitNoWithSpecialChar(log, "cf_deductioninfo","code","D"));
                ps.setString(2, item.getDescp());
                ps.setString(3, item.getStatus());
                
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
        
        try{
            String deleteQuery_2 = "delete from cf_deductioninfo where code = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, no);
                ps_2.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, Deduction item) throws Exception{
        
        try{
            String q = ("UPDATE cf_deductioninfo SET code = ?,  descp = ?,  status = ? WHERE code = '"+item.getCode()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCode());
            ps.setString(2, item.getDescp());
            ps.setString(3, item.getStatus());
       
           Logger.getLogger(DeductionDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static Deduction getInfo(LoginProfile log, String no) throws SQLException, Exception {
        Deduction c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM cf_deductioninfo WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getDeduction(rs);
        }

        
        return c;
    }
    
    private static Deduction getDeduction(ResultSet rs) throws SQLException {
        Deduction g = new Deduction();
        
        g.setCode(rs.getString("code"));
        g.setDescp(rs.getString("descp"));
        g.setStatus(rs.getString("status"));
        g.setPortion(rs.getString("portion"));
        g.setType(rs.getString("type"));
     
       
        return g;
    }
    
    public static List<Deduction> getAllLot(LoginProfile log, String keyword) throws Exception {
        
        
        
        String q = "";
        
        if(keyword!=null){
            q = "where code like '%"+keyword+"%' or descp like '%"+keyword+"%'";
        }
        
        Statement stmt = null;
        List<Deduction> Com;
        Com = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from cf_deductioninfo "+q+" order by code limit 10");
Logger.getLogger(DeductionDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getDeduction(rs));
            }
        } catch (SQLException e) {
        }

        return Com;
    }




}
