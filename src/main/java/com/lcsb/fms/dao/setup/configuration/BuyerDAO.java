/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.configuration;

import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.configuration.Buyer;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.dao.LocationDAO;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class BuyerDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("010117");
        mod.setModuleDesc("Buyer");
        mod.setMainTable("buyer_info");
        mod.setReferID_Master("code");
        return mod;

    }

    public static List<ListTable> tableList() {

        String column_names[] = {"code", "companyname", "coregister"};
        String title_name[] = {"Code", "Company Name", "Register No."};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }

        return lt;
    }

    public static void saveData(LoginProfile log, Buyer item, String code) throws Exception {

        try {

            String q = ("insert into buyer_info(code, vendor,  companyname,  coregister,  bumiputra,  porla, mill, coa, coadescp, person, title, position, hp, address, city, state, postcode, country, phone, fax, email, url, remarks, gstid) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, code);
                ps.setString(2, item.getVendor());
                ps.setString(3, item.getCompanyname());
                ps.setString(4, item.getCoregister());
                ps.setString(5, item.getBumiputra());
                ps.setString(6, item.getPorla());
                ps.setString(7, item.getMill());
                ps.setString(8, item.getCoa());
                ps.setString(9, item.getCoadescp());
                ps.setString(10, item.getPerson());
                ps.setString(11, item.getTitle());
                ps.setString(12, item.getPosition());
                ps.setString(13, item.getHp());
                ps.setString(14, item.getAddress());
                ps.setString(15, item.getCity());
                ps.setString(16, item.getState());
                ps.setString(17, item.getPostcode());
                ps.setString(18, item.getCountry());
                ps.setString(19, item.getPhone());
                ps.setString(20, item.getFax());
                ps.setString(21, item.getEmail());
                ps.setString(22, item.getUrl());
                ps.setString(23, item.getRemarks());
                ps.setString(24, item.getGstid());

                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void saveDatatoHQ(LoginProfile log, Buyer item) throws Exception {

        Logger.getLogger(BuyerDAO.class.getName()).log(Level.INFO, "-------testtt----");
        Connection con = ConnectionUtil.getHQConnection();

        String newcode = AutoGenerate.getNewCode("buyer_info", "code");
        //String no, String bankcode, String bankname, Date tarikh, String startcek, int nocek, String endcek, String active)
        try {

            String q = ("insert into buyer_info(code,  companyname,  coregister,  bumiputra,  porla, mill, coa, coadescp, person, title, position, hp, address, city, state, postcode, country, phone, fax, email, url, remarks, gstid) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            try (PreparedStatement ps = con.prepareStatement(q)) {
                ps.setString(1, newcode);
                ps.setString(2, item.getCompanyname());
                ps.setString(3, item.getCoregister());
                ps.setString(4, item.getBumiputra());
                ps.setString(5, item.getPorla());
                ps.setString(6, item.getMill());
                ps.setString(7, item.getCoa());
                ps.setString(8, item.getCoadescp());
                ps.setString(9, item.getPerson());
                ps.setString(10, item.getTitle());
                ps.setString(11, item.getPosition());
                ps.setString(12, item.getHp());
                ps.setString(13, item.getAddress());
                ps.setString(14, item.getCity());
                ps.setString(15, item.getState());
                ps.setString(16, item.getPostcode());
                ps.setString(17, item.getCountry());
                ps.setString(18, item.getPhone());
                ps.setString(19, item.getFax());
                ps.setString(20, item.getEmail());
                ps.setString(21, item.getUrl());
                ps.setString(22, item.getRemarks());
                ps.setString(23, item.getGstid());

                ps.executeUpdate();

                BuyerDAO.saveData(log, item, newcode);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception {

        try {
            String deleteQuery_2 = "delete from buyer_info where code = ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, no);
            ps_2.executeUpdate();
            ps_2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateItem(LoginProfile log, Buyer item) throws Exception {


        try {
            String q = ("UPDATE buyer_info SET code = ?,  vendor = ?,  companyname = ?,  coregister = ?,  bumiputra = ?,  porla = ?,  mill = ?,  coa = ?,  coadescp = ?,  person = ?,  title = ?, position = ?,  hp = ?,  address = ?,  city = ?,  state = ?,  postcode = ?,  country = ?,  phone = ?,  fax = ?,  email = ?,  url = ?,  remarks = ?,  gstid = ?, taxinvoicetype = ? WHERE code = '" + item.getCode() + "'");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCode());
            ps.setString(2, item.getVendor());
            ps.setString(3, item.getCompanyname());
            ps.setString(4, item.getCoregister());
            ps.setString(5, item.getBumiputra());
            ps.setString(6, item.getPorla());
            ps.setString(7, item.getMill());
            ps.setString(8, item.getCoa());
            ps.setString(9, item.getCoadescp());
            ps.setString(10, item.getPerson());
            ps.setString(11, item.getTitle());
            ps.setString(12, item.getPosition());
            ps.setString(13, item.getHp());
            ps.setString(14, item.getAddress());
            ps.setString(15, item.getCity());
            ps.setString(16, item.getState());
            ps.setString(17, item.getPostcode());
            ps.setString(18, item.getCountry());
            ps.setString(19, item.getPhone());
            ps.setString(20, item.getFax());
            ps.setString(21, item.getEmail());
            ps.setString(22, item.getUrl());
            ps.setString(23, item.getRemarks());
            ps.setString(24, item.getGstid());
            ps.setString(25, item.getTaxinvoicetype());
            Logger.getLogger(BuyerDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static Buyer getInfo(LoginProfile log, String no) throws SQLException, Exception {

        Buyer c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM buyer_info WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
            Logger.getLogger(BuyerDAO.class.getName()).log(Level.INFO, "*********" + rs.getString("companyname"));
        }

        return c;
    }

    public static Buyer getLocalCode(LoginProfile log, String no) throws SQLException, Exception {
        Buyer c = null;
        try {
            
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM buyer_info WHERE codemill=?");
            stmt.setString(1, no);
            rs = stmt.executeQuery();
            if (rs.next()) {
                c = getInfo(rs);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    private static Buyer getInfo(ResultSet rs) throws SQLException {
        Buyer g = new Buyer();
        g.setAddress(rs.getString("address"));
        g.setBumiputra(rs.getString("bumiputra"));
        g.setCity(rs.getString("city"));
        g.setCoa(rs.getString("coa"));
        g.setCoadescp(rs.getString("coadescp"));
        g.setCode(rs.getString("code"));
        g.setCompanyname(rs.getString("companyname"));
        g.setCoregister(rs.getString("coregister"));
        g.setCountry(rs.getString("country"));
        g.setEmail(rs.getString("email"));
        g.setFax(rs.getString("fax"));
        g.setGstid(rs.getString("gstid"));
        g.setHp(rs.getString("hp"));
        g.setMill(rs.getString("mill"));
        g.setPerson(rs.getString("person"));
        g.setPhone(rs.getString("phone"));
        g.setPorla(rs.getString("porla"));
        g.setPosition(rs.getString("position"));
        g.setPostcode(rs.getString("postcode"));
        g.setRemarks(rs.getString("remarks"));
        g.setState(rs.getString("state"));
        g.setTitle(rs.getString("title"));
        g.setUrl(rs.getString("url"));
        g.setVendor(rs.getString("vendor"));
        g.setCodemill(rs.getString("codemill"));
        g.setPaymentterm(rs.getString("paymentterm"));
        g.setTaxinvoicetype(rs.getString("taxinvoicetype"));

        return g;
    }

    public static List<com.lcsb.fms.util.model.Buyer> getBuyerHQ(String keyword) throws Exception {

        Connection con = ConnectionUtil.getHQConnection();

        Statement stmt = null;
        List<com.lcsb.fms.util.model.Buyer> Com;
        Com = new ArrayList();

        String q = "";

        if (keyword != null) {
            q = "where code like '%" + keyword + "%' or companyname like '%" + keyword + "%'";
        }

        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select code,companyname,address,city,state,postcode from buyer_info " + q + " order by code limit 10");
            while (rs.next()) {

                Com.add(getResult(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }

    private static com.lcsb.fms.util.model.Buyer getResult(ResultSet rs) throws SQLException {
        com.lcsb.fms.util.model.Buyer comp = new com.lcsb.fms.util.model.Buyer();
        comp.setBuyerAddress(rs.getString("address"));
        comp.setBuyerCity(rs.getString("city"));
        comp.setBuyerCode(rs.getString("code"));
        comp.setBuyerName(rs.getString("companyname"));
        comp.setBuyerPostcode(rs.getString("postcode"));
        comp.setBuyerState(rs.getString("state"));
        return comp;
    }

    public static int checkExist(LoginProfile log, String no) throws Exception {

        int i = 0;

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT count(*) as cnt FROM buyer_info WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            i = rs.getInt("cnt");
        }

        return i;
    }

    public static Buyer getHQBuyer(String no) throws SQLException, Exception {
        Buyer c = null;
        ResultSet rs = null;
        Connection conet = ConnectionUtil.getHQConnection();
        PreparedStatement stmt = conet.prepareStatement("SELECT * FROM buyer_info WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfoHQ(rs);
        }

        return c;
    }

    public static int retrieveData(LoginProfile log, String no) throws Exception {

        int i = 0;

        try {
            saveDataFromHQ(log, getHQBuyer(no), "");
            i = 1;
        } catch (SQLException e) {
            e.printStackTrace();
            i = 0;
        }

        return i;
    }

    private static Buyer getInfoHQ(ResultSet rs) throws SQLException {

        Buyer g = new Buyer();
        g.setAddress(rs.getString("address"));
        g.setBumiputra(rs.getString("bumiputra"));
        g.setCity(rs.getString("city"));
        g.setCoa(rs.getString("coa"));
        g.setCoadescp(rs.getString("coadescp"));
        g.setCode(rs.getString("code"));
        g.setCompanyname(rs.getString("companyname"));
        g.setCoregister(rs.getString("coregister"));
        g.setCountry(rs.getString("country"));
        g.setEmail(rs.getString("email"));
        g.setFax(rs.getString("fax"));
        g.setGstid(rs.getString("gstid"));
        g.setHp(rs.getString("hp"));
        g.setMill(rs.getString("mill"));
        g.setPerson(rs.getString("person"));
        g.setPhone(rs.getString("phone"));
        g.setPorla(rs.getString("porla"));
        g.setPosition(rs.getString("position"));
        g.setPostcode(rs.getString("postcode"));
        g.setRemarks(rs.getString("remarks"));
        g.setState(rs.getString("state"));
        g.setTitle(rs.getString("title"));
        g.setUrl(rs.getString("url"));

        return g;
    }

    public static void saveDataFromHQ(LoginProfile log, Buyer item, String code) throws Exception {

        //String no, String bankcode, String bankname, Date tarikh, String startcek, int nocek, String endcek, String active)
        try {

            String q = ("insert into buyer_info(code, vendor,  companyname,  coregister,  bumiputra,  porla, mill, coa, coadescp, person, title, position, hp, address, city, state, postcode, country, phone, fax, email, url, remarks, gstid) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, item.getCode());
                ps.setString(2, "");
                ps.setString(3, item.getCompanyname());
                ps.setString(4, item.getCoregister());
                ps.setString(5, item.getBumiputra());
                ps.setString(6, item.getPorla());
                ps.setString(7, item.getMill());
                ps.setString(8, item.getCoa());
                ps.setString(9, item.getCoadescp());
                ps.setString(10, item.getPerson());
                ps.setString(11, item.getTitle());
                ps.setString(12, item.getPosition());
                ps.setString(13, item.getHp());
                ps.setString(14, item.getAddress());
                ps.setString(15, item.getCity());
                ps.setString(16, item.getState());
                ps.setString(17, item.getPostcode());
                ps.setString(18, item.getCountry());
                ps.setString(19, item.getPhone());
                ps.setString(20, item.getFax());
                ps.setString(21, item.getEmail());
                ps.setString(22, item.getUrl());
                ps.setString(23, item.getRemarks());
                ps.setString(24, item.getGstid());
                Logger.getLogger(BuyerDAO.class.getName()).log(Level.INFO, String.valueOf(ps));

                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
