/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.configuration;

import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.configuration.ChartofMaterial;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class ChartofMaterialDAO {
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010110");
        mod.setModuleDesc("Chart of Material");
        mod.setMainTable("chartofmaterial");
        mod.setReferID_Master("code");
        return mod;
         
    }
    public static List<ListTable> tableList(){
        
        String column_names[] = {"code","descp","type","finallvl","active"};
        String title_name[] = {"Code","Description","Material Type","Final Level","Active"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
    
    public static void saveData(LoginProfile log, ChartofMaterial item) throws Exception{
        
        try {
            
            String q = ("insert into chartofmaterial(code, descp, type, accountcode, accountdescp, finallvl, active, matgroup, unitmeasure) values (?,?,?,?,?,?,?,?,?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, item.getCode());
                ps.setString(2, item.getDescp());
                ps.setString(3, item.getType());
                ps.setString(4, item.getAccountcode());
                ps.setString(5, item.getAccountdescp());
                ps.setString(6, item.getFinallvl());
                ps.setString(7, item.getActive());
                ps.setString(8, item.getMatgroup());
                ps.setString(9, item.getUnitmeasure());
                
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
         
        
        try{
            String deleteQuery_2 = "delete from chartofmaterial where code = ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, no);
            ps_2.executeUpdate();
            ps_2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, ChartofMaterial item) throws Exception{
        
        
        try{
            String q = ("UPDATE chartofmaterial SET code = ?,  descp = ?,  type = ?,  accountcode = ?,  accountdescp = ?,  finallvl = ?,  active = ?,  matgroup = ?,  unitmeasure = ? WHERE code = '"+item.getCode()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCode());
            ps.setString(2, item.getDescp());
            ps.setString(3, item.getType());
            ps.setString(4, item.getAccountcode());
            ps.setString(5, item.getAccountdescp());
            ps.setString(6, item.getFinallvl());
            ps.setString(7, item.getActive());
            ps.setString(8, item.getMatgroup());
            ps.setString(9, item.getUnitmeasure());
           Logger.getLogger(ChartofMaterialDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static ChartofMaterial getInfo(LoginProfile log, String no) throws SQLException, Exception {
        ChartofMaterial c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM chartofmaterial WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static ChartofMaterial getInfo(ResultSet rs) throws SQLException {
        ChartofMaterial g = new ChartofMaterial();
        g.setAccountcode(rs.getString("accountcode"));
        g.setAccountdescp(rs.getString("accountdescp"));
        g.setActive(rs.getString("active"));
        g.setCode(rs.getString("code"));
        g.setDescp(rs.getString("descp"));
        g.setFinallvl(rs.getString("finallvl"));
        g.setMatgroup(rs.getString("matgroup"));
        g.setType(rs.getString("type"));
        g.setUnitmeasure(rs.getString("unitmeasure"));
       
       
        return g;
    }
    
}
