/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.configuration;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.configuration.Broker;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class BrokerDAO {
     public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010131");
        mod.setModuleDesc("Broker Information");
        mod.setMainTable("broker_info");
        mod.setReferID_Master("code");
        return mod;
         
    }
    
     public static List<ListTable> tableList(){
        
        String column_names[] = {"code","companyname","coregister"};
        String title_name[] = {"Code","Company Name","Register No."};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
     
    public static void saveData(LoginProfile log, Broker item) throws Exception{
        
        try {
            
            String q = ("insert into broker_info(code, companyname, coregister,  bumiputra,  hqactdesc,  hqactcode,  coa,  coadescp,  person,  title, position,  hp,  address,  city,  state,  postcode,  country,  phone,  fax,  email,  url,  bank,  bankaccount, payment, remarks, bankdesc, active) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, AutoGenerate.get4digitNo(log, "broker_info", "code"));
            ps.setString(2, item.getCompanyname());
            ps.setString(3, item.getCoregister());
            ps.setString(4, item.getBumiputra());
            ps.setString(5, item.getHqactdesc());
            ps.setString(6, item.getHqactcode());
            ps.setString(7, item.getCoa());
            ps.setString(8, item.getCoadescp());
            ps.setString(9, item.getPerson());
            ps.setString(10, item.getTitle());
            ps.setString(11, item.getPosition());
            ps.setString(12, item.getHp());
            ps.setString(13, item.getAddress());
            ps.setString(14, item.getCity());
            ps.setString(15, item.getState());
            ps.setString(16, item.getPostcode());
            ps.setString(17, item.getCountry());
            ps.setString(18, item.getPhone());
            ps.setString(19, item.getFax());
            ps.setString(20, item.getEmail());
            ps.setString(21, item.getUrl());
            ps.setString(22, item.getBank());
            ps.setString(23, item.getBankaccount());
            ps.setString(24, item.getPayment());
            ps.setString(25, item.getRemarks());
            ps.setString(26, item.getBankdesc());
            ps.setString(27, item.getActive());
            
           ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
         
        
        try{
            String deleteQuery_2 = "delete from broker_info where code = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, no);
                ps_2.executeUpdate();
            }
        } catch (SQLException e) {
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, Broker item) throws Exception{
        
        
        try{
            String q = ("UPDATE broker_info SET code=? , companyname=? , coregister=? ,  bumiputra=? ,  hqactdesc=? ,  hqactcode=? ,  coa=? ,  coadescp=? ,  person=? ,  title=? , position=? ,  hp=? ,  address=? ,  city=? ,  state=? ,  postcode=? ,  country=? ,  phone=? ,  fax=? ,  email=? ,  url=? ,  bank=? ,  bankaccount=? , payment=? , remarks=? , bankdesc=? , active=?  WHERE code = '"+item.getCode()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
           
            ps.setString(1, item.getCode());
            ps.setString(2, item.getCompanyname());
            ps.setString(3, item.getCoregister());
            ps.setString(4, item.getBumiputra());
            ps.setString(5, item.getHqactdesc());
            ps.setString(6, item.getHqactcode());
            ps.setString(7, item.getCoa());
            ps.setString(8, item.getCoadescp());
            ps.setString(9, item.getPerson());
            ps.setString(10, item.getTitle());
            ps.setString(11, item.getPosition());
            ps.setString(12, item.getHp());
            ps.setString(13, item.getAddress());
            ps.setString(14, item.getCity());
            ps.setString(15, item.getState());
            ps.setString(16, item.getPostcode());
            ps.setString(17, item.getCountry());
            ps.setString(18, item.getPhone());
            ps.setString(19, item.getFax());
            ps.setString(20, item.getEmail());
            ps.setString(21, item.getUrl());
            ps.setString(22, item.getBank());
            ps.setString(23, item.getBankaccount());
            ps.setString(24, item.getPayment());
            ps.setString(25, item.getRemarks());
            ps.setString(26, item.getBankdesc());
            ps.setString(27, item.getActive());
           
           Logger.getLogger(BrokerDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static Broker getInfo(LoginProfile log, String no) throws SQLException, Exception {
        Broker c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM broker_info WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static Broker getInfo(ResultSet rs) throws SQLException {
        Broker g = new Broker();
        
        g.setCode(rs.getString("code"));
        g.setCompanyname(rs.getString("companyname"));
        g.setCoregister(rs.getString("coregister"));
        g.setBumiputra(rs.getString("bumiputra"));
        g.setHqactdesc(rs.getString("hqactdesc"));
        g.setHqactcode(rs.getString("hqactcode"));
        g.setCoa(rs.getString("coa"));
        g.setCoadescp(rs.getString("coadescp"));
        g.setPerson(rs.getString("person"));
        g.setTitle(rs.getString("title"));//10
        g.setPosition(rs.getString("position"));
        g.setHp(rs.getString("hp"));
        g.setAddress(rs.getString("address"));
        g.setCity(rs.getString("city"));
        g.setState(rs.getString("state"));
        g.setPostcode(rs.getString("postcode"));
        g.setCountry(rs.getString("country"));
        g.setPhone(rs.getString("phone"));
        g.setFax(rs.getString("fax"));
        g.setEmail(rs.getString("email"));//20
        g.setUrl(rs.getString("url"));
        g.setBank(rs.getString("bank"));
        g.setBankaccount(rs.getString("bankaccount"));
        g.setPayment(rs.getString("payment"));
        g.setRemarks(rs.getString("remarks"));
        g.setBankdesc(rs.getString("bankdesc"));
        g.setActive(rs.getString("active"));
       
        
        
       
        return g;
    }
    
    public static List<Broker> getAllBroker(LoginProfile log, String keyword) throws Exception {
        
        
        Statement stmt = null;
        List<Broker> Com;
        Com = new ArrayList();
        
        String q = "";
        
        if(keyword!=null){
            q = "where code like '%"+keyword+"%' or companyname like '%"+keyword+"%'";
        }
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from broker_info "+q+" order by code limit 10");
            while (rs.next()) {
                
                Com.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
}

