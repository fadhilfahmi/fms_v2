/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.configuration;

import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.configuration.LotMember;
import com.lcsb.fms.model.setup.configuration.Portfolio;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class PortfolioDAO {
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010129");
        mod.setModuleDesc("Share Portfolio");
        mod.setMainTable("cf_portfolio");
        mod.setReferID_Master("code");
        return mod;
         
    }
    
    public static List<ListTable> tableList(){
        
        String column_names[] = {"code","descp","coa", "coadescp"};
        String title_name[] = {"Code","Description","Account Code", "Account Desc."};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
    
    public static void saveData(LoginProfile log, Portfolio item) throws Exception {
        try {

            String q = ("insert into cf_portfolio(address,city,coa,coadescp,code,coregister,country,descp,fax,gstid,phone,postcode,remarks,state) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getAddress());
            ps.setString(2, item.getCity());
            ps.setString(3, item.getCoa());
            ps.setString(4, item.getCoadescp());
            ps.setString(5, AutoGenerate.get4digitNo(log, "cf_portfolio", "code"));
            ps.setString(6, item.getCoregister());
            ps.setString(7, item.getCountry());
            ps.setString(8, item.getDescp());
            ps.setString(9, item.getFax());
            ps.setString(10, item.getGstid());
            ps.setString(11, item.getPhone());
            ps.setString(12, item.getPostcode());
            ps.setString(13, item.getRemarks());
            ps.setString(14, item.getState());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
    public static Portfolio getInfo(LoginProfile log, String no) throws SQLException, Exception {
        Portfolio c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM cf_portfolio WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    
    private static Portfolio getInfo(ResultSet rs) throws SQLException {
        Portfolio g = new Portfolio();
        g.setAddress(rs.getString("address"));
        g.setCity(rs.getString("city"));
        g.setCoa(rs.getString("coa"));
        g.setCoadescp(rs.getString("coadescp"));
        g.setCode(rs.getString("code"));
        g.setDescp(rs.getString("descp"));
        g.setCoregister(rs.getString("coregister"));
        g.setCountry(rs.getString("country"));
        g.setPhone(rs.getString("phone"));
        g.setFax(rs.getString("fax"));
        g.setGstid(rs.getString("gstid"));
        g.setRemarks(rs.getString("remarks"));
        g.setPostcode(rs.getString("postcode"));
        
       
        return g;
    }
    
    public static void updateData(LoginProfile log, Portfolio item) throws Exception {

        try {

            String q = ("UPDATE cf_portfolio SET address = ?,city = ?,coa = ?,coadescp = ?,code = ?,coregister = ?,country = ?,descp = ?,fax = ?,gstid = ?,phone = ?,postcode = ?,remarks = ?,state = ? WHERE code = '"+item.getCode()+"'");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getAddress());
            ps.setString(2, item.getCity());
            ps.setString(3, item.getCoa());
            ps.setString(4, item.getCoadescp());
            ps.setString(5, item.getCode());
            ps.setString(6, item.getCoregister());
            ps.setString(7, item.getCountry());
            ps.setString(8, item.getDescp());
            ps.setString(9, item.getFax());
            ps.setString(10, item.getGstid());
            ps.setString(11, item.getPhone());
            ps.setString(12, item.getPostcode());
            ps.setString(13, item.getRemarks());
            ps.setString(14, item.getState());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public static void deleteData(LoginProfile log, String no) throws SQLException, Exception {


        try {
            String deleteQuery_2 = "delete from cf_portfolio where code = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, no);
                ps_2.executeUpdate();
            }
        } catch (SQLException e) {
        }

    }
    
}
