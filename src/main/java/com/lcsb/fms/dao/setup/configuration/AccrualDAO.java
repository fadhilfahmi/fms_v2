/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.configuration;

import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.configuration.Accrual;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class AccrualDAO {
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010119");
        mod.setModuleDesc("Accrual");
        mod.setMainTable("conf_accrual_info");
        mod.setReferID_Master("code");
        return mod;
         
    }
    public static List<ListTable> tableList(){
        
        String column_names[] = {"code","companyname","coregister"};
        String title_name[] = {"Code","Company Name","Register No."};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
    
    public static void saveData(LoginProfile log, Accrual item) throws Exception{
        
        try {
            
            String q = ("insert into conf_accrual_info(code, companyname, coregister, bumiputra, coa, coadescp, person, title, position, hp, address, city, state, postcode, country, phone, fax, email, url, bank, bankaccount, payment, remarks, bankdesc, depositcode, depositdesc, suspensecode, suspensedesc) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, AutoGenerate.get4digitNo(log, "conf_accrual_info", "code"));
                ps.setString(2, item.getCompanyname());
                ps.setString(3, item.getCoregister());
                ps.setString(4, item.getBumiputra());
                ps.setString(5, item.getCoa());
                ps.setString(6, item.getCoadescp());
                ps.setString(7, item.getPerson());
                ps.setString(8, item.getTitle());
                ps.setString(9, item.getPosition());
                ps.setString(10, item.getHp());
                ps.setString(11, item.getAddress());
                ps.setString(12, item.getCity());
                ps.setString(13, item.getState());
                ps.setString(14, item.getPostcode());
                ps.setString(15, item.getCountry());
                ps.setString(16, item.getPhone());
                ps.setString(17, item.getFax());
                ps.setString(18, item.getEmail());
                ps.setString(19, item.getUrl());
                ps.setString(20, item.getBank());
                ps.setString(21, item.getBankaccount());
                ps.setString(22, item.getPayment());
                ps.setString(23, item.getRemarks());
                ps.setString(24, item.getBankdesc());
                ps.setString(25, item.getDepositcode());
                ps.setString(26, item.getDepositdesc());
                ps.setString(27, item.getSuspensecode());
                ps.setString(28, item.getSuspensedesc());
                
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
         
        
        try{
            String deleteQuery_2 = "delete from conf_accrual_info where code = ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, no);
            ps_2.executeUpdate();
            ps_2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, Accrual item) throws Exception{
        
        
        try{
            String q = ("UPDATE conf_accrual_info SET code = ?,  companyname = ?,  coregister = ?,  bumiputra = ?,  coa = ?,  coadescp = ?,  person = ?,  title = ?,  position = ?,  hp = ?,  address = ?, city = ?,  state = ?,  postcode = ?,  country = ?,  phone = ?,  fax = ?,  email = ?,  url = ?,  bank = ?,  bankaccount = ?,  payment = ?,  remarks = ?,  bankdesc = ?, depositcode = ?, depositdesc = ?, suspensecode = ?, suspensedesc = ? WHERE code = '"+item.getCode()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCode());
            ps.setString(2, item.getCompanyname());
            ps.setString(3, item.getCoregister());
            ps.setString(4, item.getBumiputra());
            ps.setString(5, item.getCoa());
            ps.setString(6, item.getCoadescp());
            ps.setString(7, item.getPerson());
            ps.setString(8, item.getTitle());
            ps.setString(9, item.getPosition());
            ps.setString(10, item.getHp());
            ps.setString(11, item.getAddress());
            ps.setString(12, item.getCity());
            ps.setString(13, item.getState());
            ps.setString(14, item.getPostcode());
            ps.setString(15, item.getCountry());
            ps.setString(16, item.getPhone());
            ps.setString(17, item.getFax());
            ps.setString(18, item.getEmail());
            ps.setString(19, item.getUrl());
            ps.setString(20, item.getBank());
            ps.setString(21, item.getBankaccount());
            ps.setString(22, item.getPayment());
            ps.setString(23, item.getRemarks());
            ps.setString(24, item.getBankdesc());
            ps.setString(25, item.getDepositcode());
            ps.setString(26, item.getDepositdesc());
            ps.setString(27, item.getSuspensecode());
            ps.setString(28, item.getSuspensedesc());
           Logger.getLogger(AccrualDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static Accrual getInfo(LoginProfile log, String no) throws SQLException, Exception {
        Accrual c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM conf_accrual_info WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static Accrual getInfo(ResultSet rs) throws SQLException {
        Accrual g = new Accrual();
        g.setAddress(rs.getString("address"));
        g.setBank(rs.getString("bank"));
        g.setBankaccount(rs.getString("bankaccount"));
        g.setBankdesc(rs.getString("bankdesc"));
        g.setBumiputra(rs.getString("bumiputra"));
        g.setCity(rs.getString("city"));
        g.setCoa(rs.getString("coa"));
        g.setCoadescp(rs.getString("coadescp"));
        g.setCode(rs.getString("code"));
        g.setCompanyname(rs.getString("companyname"));
        g.setCoregister(rs.getString("coregister"));
        g.setCountry(rs.getString("country"));
        g.setDepositcode(rs.getString("depositcode"));
        g.setDepositdesc(rs.getString("depositdesc"));
        g.setEmail(rs.getString("email"));
        g.setFax(rs.getString("fax"));
        g.setHp(rs.getString("hp"));
        g.setPayment(rs.getString("payment"));
        g.setPerson(rs.getString("person"));
        g.setPhone(rs.getString("phone"));
        g.setPosition(rs.getString("position"));
        g.setPostcode(rs.getString("postcode"));
        g.setRemarks(rs.getString("remarks"));
        g.setState(rs.getString("state"));
        g.setSuspensecode(rs.getString("suspensecode"));
        g.setSuspensedesc(rs.getString("suspensedesc"));
        g.setTitle(rs.getString("title"));
        g.setUrl(rs.getString("url"));
        
       
        return g;
    }
    
}
