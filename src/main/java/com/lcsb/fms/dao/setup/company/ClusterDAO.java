
package com.lcsb.fms.dao.setup.company;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.company.Cluster;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class ClusterDAO {
     public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010226");
        mod.setModuleDesc("Cluster Information");
        mod.setMainTable("cluster");
        mod.setReferID_Master("id");
        return mod;
         
    }
    
     public static List<ListTable> tableList(){
        
        String column_names[] = {"id","code","sub","level"};
        String title_name[] = {"Id","Code","Name","Level"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
     
    public static void saveData(LoginProfile log, Cluster item) throws Exception{
        try {
            
            String q = ("insert into cluster( code, subcode, sub, level, ccode, cdescp) values (?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCode());
            ps.setString(2, item.getSubcode());
            ps.setString(3, item.getSub());
            ps.setString(4, item.getLevel());
            ps.setString(5, item.getCcode());
            ps.setString(6, item.getCdescp());
            
            
           ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
         
        
        try{
            String deleteQuery_2 = "delete from cluster where id = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, no);
                ps_2.executeUpdate();
            }
        } catch (SQLException e) {
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, Cluster item) throws Exception{
        
        
        try{
            String q = ("UPDATE cluster SET code=? , subcode=? , sub=? ,  level=? ,  ccode=? ,  cdescp=?  WHERE id = '"+item.getId()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
             ps.setString(1, item.getCode());
            ps.setString(2, item.getSubcode());
            ps.setString(3, item.getSub());
            ps.setString(4, item.getLevel());
            ps.setString(5, item.getCcode());
            ps.setString(6, item.getCdescp());
            
           Logger.getLogger(ClusterDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static Cluster getInfo(LoginProfile log, String no) throws SQLException, Exception {
        Cluster c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM cluster WHERE id=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static Cluster getInfo(ResultSet rs) throws SQLException {
        Cluster g = new Cluster();
        
        g.setCode(rs.getString("code"));
        g.setSubcode(rs.getString("subcode"));
        g.setSub(rs.getString("sub"));
        g.setLevel(rs.getString("level"));
        g.setCcode(rs.getString("ccode"));
        g.setCdescp(rs.getString("cdescp"));
        
        
        
       
        return g;
    }
    
}

