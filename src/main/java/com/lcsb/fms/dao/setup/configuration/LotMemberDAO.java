/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.configuration;

import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.configuration.LotMember;
import com.lcsb.fms.model.setup.configuration.LotMemberLot;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LotMemberDAO {

    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("010127");
        mod.setModuleDesc("Lot Member Information");
        mod.setMainTable("lot_memberinfo");
        mod.setReferID_Master("mem_Code");
        return mod;

    }

    public static List<ListTable> tableList() {

        String column_names[] = {"mem_Code", "mem_Name", "ic", "status"};
        String title_name[] = {"Member Code", "Member Name", "IC Number", "Status"};

        List<ListTable> lt = new ArrayList();

        for (int j = 0; j < column_names.length; j++) {
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }

        return lt;
    }

    public static void saveData(LoginProfile log, LotMember item) throws Exception {

        try {

            String q = ("insert into lot_memberinfo(mem_Code, title, usetitle, mem_Name, ic, address, postcode, city, state, bank, acc, paymethod, status, notel, date_inactive, pre_owner, relation, date_active, new_owner, remark) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getMemCode());
            ps.setString(2, item.getTitle());
            ps.setString(3, item.getUsetitle());
            ps.setString(4, item.getMemName());
            ps.setString(5, item.getIc());
            ps.setString(6, item.getAddress());
            ps.setString(7, item.getPostcode());
            ps.setString(8, item.getCity());
            ps.setString(9, item.getState());
            ps.setString(10, item.getBank());
            ps.setString(11, item.getAcc());
            ps.setString(12, item.getPaymethod());
            ps.setString(13, item.getStatus());
            ps.setString(14, item.getNotel());
            ps.setString(15, item.getDateInactive());
            ps.setString(16, item.getPreOwner());
            ps.setString(17, item.getRelation());
            ps.setString(18, item.getDateActive());
            ps.setString(19, item.getNewOwner());
            ps.setString(20, item.getRemark());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception {


        try {
            String deleteQuery_2 = "delete from lot_memberinfo where mem_Code = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, no);
                ps_2.executeUpdate();
            }
        } catch (SQLException e) {
        }

    }

    public static void updateItem(LoginProfile log, LotMember item) throws Exception {


        try {
            String q = ("UPDATE lot_memberinfo SET mem_Code=?, title=?, usetitle=?, mem_Name=?, ic=?, address=?, postcode=?, city=?, state=?, bank=?, acc=?, paymethod=?, status=?, notel=?, date_inactive=?, pre_owner=?, relation=?, date_active=?, new_owner=?, remark=?  WHERE mem_Code = '" + item.getMemCode() + "'");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getMemCode());
            ps.setString(2, item.getTitle());
            ps.setString(3, item.getUsetitle());
            ps.setString(4, item.getMemName());
            ps.setString(5, item.getIc());
            ps.setString(6, item.getAddress());
            ps.setString(7, item.getPostcode());
            ps.setString(8, item.getCity());
            ps.setString(9, item.getState());
            ps.setString(10, item.getBank());
            ps.setString(11, item.getAcc());
            ps.setString(12, item.getPaymethod());
            ps.setString(13, item.getStatus());
            ps.setString(14, item.getNotel());
            ps.setString(15, item.getDateInactive());
            ps.setString(16, item.getPreOwner());
            ps.setString(17, item.getRelation());
            ps.setString(18, item.getDateActive());
            ps.setString(19, item.getNewOwner());
            ps.setString(20, item.getRemark());

            Logger.getLogger(LotMemberDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static LotMember getInfo(LoginProfile log, String no) throws SQLException, Exception {
        LotMember c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM lot_memberinfo where mem_Code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        return c;
    }

    private static LotMember getInfo(ResultSet rs) throws SQLException {
        LotMember g = new LotMember();
        g.setMemCode(checkisNull(rs.getString("mem_Code")));
        g.setTitle(checkisNull(rs.getString("title")));
        g.setUsetitle(checkisNull(rs.getString("usetitle")));
        g.setMemName(checkisNull(rs.getString("mem_Name")));
        g.setIc(checkisNull(rs.getString("ic")));
        g.setAddress(checkisNull(rs.getString("address")));
        g.setPostcode(checkisNull(rs.getString("postcode")));
        g.setCity(checkisNull(rs.getString("city")));
        g.setState(checkisNull(rs.getString("state")));
        g.setBank(checkisNull(rs.getString("bank")));
        g.setAcc(checkisNull(rs.getString("acc")));
        g.setPaymethod(checkisNull(rs.getString("paymethod")));
        g.setStatus(checkisNull(rs.getString("Status")));
        g.setNotel(checkisNull(rs.getString("notel")));
        g.setDateInactive(checkisNull(rs.getString("date_inactive")));
        g.setPreOwner(checkisNull(rs.getString("pre_owner")));
        g.setRelation(checkisNull(rs.getString("relation")));
        g.setDateActive(checkisNull(rs.getString("date_active")));
        g.setNewOwner(checkisNull(rs.getString("new_owner")));
        g.setRemark(checkisNull(rs.getString("remark")));
        g.setAddress1(checkisNull(rs.getString("address1")));
        g.setAddress2(checkisNull(rs.getString("address2")));
        g.setAddress3(checkisNull(rs.getString("address3")));
        g.setAddress4(checkisNull(rs.getString("address4")));
        g.setAddress5(checkisNull(rs.getString("address5")));

        return g;
    }

    public static List<LotMemberLot> getAllLotForMember(LoginProfile log, String id) throws Exception {

        Statement stmt = null;
        List<LotMemberLot> CVi;
        CVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from lot_memberlot where mem_Code = '" + id + "' order by id");

            while (rs.next()) {
                CVi.add(getResult(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }

    public static LotMemberLot getInfoLot(LoginProfile log, String no) throws SQLException, Exception {
        LotMemberLot c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM lot_memberlot where id=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getResult(rs);
        }

        return c;
    }
    
    public static LotMemberLot getInfoLotByCode(LoginProfile log, String memcode, String lotcode) throws SQLException, Exception {
        LotMemberLot c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM lot_memberlot where mem_Code=? and lot_Code=?");
        stmt.setString(1, memcode);
        stmt.setString(2, lotcode);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getResult(rs);
        }

        return c;
    }

    private static LotMemberLot getResult(ResultSet rs) throws SQLException {
        LotMemberLot cv = new LotMemberLot();

        cv.setAcre(rs.getString("acre"));
        cv.setHectarage(rs.getString("hectarage"));
        cv.setHsdno(rs.getString("hsdno"));
        cv.setLotCode(rs.getString("lot_Code"));
        cv.setLotDescp(rs.getString("lot_Descp"));
        cv.setMemCode(rs.getString("mem_Code"));
        cv.setNopetak(rs.getString("nopetak"));
        cv.setId(rs.getInt("id"));

        return cv;
    }

    public static void saveLot(LoginProfile log, LotMemberLot item) throws Exception {

        try {

            String q = ("insert into lot_memberlot(acre, hectarage, lot_Code, lot_Descp, mem_Code, nopetak) values (?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getAcre());
            ps.setString(2, item.getHectarage());
            ps.setString(3, item.getLotCode());
            ps.setString(4, item.getLotDescp());
            ps.setString(5, item.getMemCode());
            ps.setString(6, item.getNopetak());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateLot(LoginProfile log, LotMemberLot item) throws Exception {

        try {
            String q = ("UPDATE lot_memberlot SET acre = ?, hectarage = ?, lot_Code = ?, lot_Descp = ?, mem_Code = ?, nopetak = ?  WHERE id = '" + item.getId() + "'");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getAcre());
            ps.setString(2, item.getHectarage());
            ps.setString(3, item.getLotCode());
            ps.setString(4, item.getLotDescp());
            ps.setString(5, item.getMemCode());
            ps.setString(6, item.getNopetak());

            Logger.getLogger(LotMemberDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static int deleteLot(LoginProfile log, String refer) throws SQLException, Exception {

        int x = 0;

        try {
            String deleteQuery_2 = "delete from lot_memberlot where id= ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, refer);
            ps_2.executeUpdate();
            ps_2.close();
            x = 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return x;
    }

    public static void transferData(LoginProfile log) throws Exception {//data migration from excel for rtk maran

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM lot_member_extract where exist = 'No'");
        //stmt.setString(1, no);
        rs = stmt.executeQuery();
        while (rs.next()) {
            //c = getResult(rs);

            try {
                
                String newcode = AutoGenerate.get4digitNo(log, "lot_memberinfo", "mem_Code");

                String q = ("insert into lot_memberinfo(mem_Code,title,usetitle,mem_Name,ic,address1,address2,address3,address4,address5,bank,acc,paymethod,status,notel,remark) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                PreparedStatement ps = log.getCon().prepareStatement(q);
                ps.setString(1, newcode);
                ps.setString(2, "None");
                ps.setString(3, "No");
                ps.setString(4, rs.getString("name"));
                ps.setString(5, rs.getString("ic"));
                ps.setString(6, rs.getString("address1"));
                ps.setString(7, rs.getString("address2"));
                ps.setString(8, rs.getString("address3"));
                ps.setString(9, rs.getString("address4"));
                ps.setString(10, rs.getString("address5"));
                ps.setString(11, rs.getString("bank"));
                ps.setString(12, rs.getString("noakaun"));
                ps.setString(13, rs.getString("payment"));
                ps.setString(14, "Active");
                ps.setString(15, rs.getString("notel"));
                ps.setString(16, "*Data Migration*");

                ps.executeUpdate();
                ps.close();
                
                String q1 = ("UPDATE lot_member_extract SET memcode = ?  WHERE id = '" + rs.getInt("id") + "'");

                PreparedStatement ps1 = log.getCon().prepareStatement(q1);
                ps1.setString(1, newcode);

                ps1.executeUpdate();
                ps1.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            
        }
    }
    
    public static void transferDataLot(LoginProfile log) throws Exception {//data migration from excel for rtk maran
        

        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM lot_member_extract");
        //stmt.setString(1, no);
        rs = stmt.executeQuery();
        while (rs.next()) {
            //c = getResult(rs);

            try {

                String q = ("insert into lot_memberlot(mem_Code,lot_Code,lot_Descp,hectarage,acre,nopetak) values (?,?,?,?,?,?)");
                PreparedStatement ps = log.getCon().prepareStatement(q);
                ps.setString(1, rs.getString("memcode"));
                ps.setString(2, "0005");
                ps.setString(3, "LADANG RTK BATU 55 MARAN");
                ps.setString(4, rs.getString("hektar"));
                ps.setString(5, rs.getString("ekar"));
                ps.setString(6, rs.getString("nopetak"));

                ps.executeUpdate();
                ps.close();
                
            } catch (SQLException e) {
                e.printStackTrace();
            }
            
        }
        
    }
    
    public static List<LotMember> getAllMember(LoginProfile log, String filter, String keyword) throws Exception {

        Statement stmt = null;
        List<LotMember> CVi;
        CVi = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from lot_memberinfo  order by mem_Code");

            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static LotMember getMember(LoginProfile log, String memcode) throws Exception {

        ResultSet rs = null;
        LotMember lt = new LotMember();
        
        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from lot_memberinfo where mem_Code = ?");
            stmt.setString(1, memcode);
            rs = stmt.executeQuery();
            if (rs.next()) {
                lt = getInfo(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lt;
    }
    
    public static List<LotMemberLot> getAllMemberByLot(LoginProfile log, String lotcode) throws Exception {

        Statement stmt = null;
        List<LotMemberLot> CVi;
        CVi = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from lot_memberlot where lot_Code = '"+lotcode+"'  order by mem_Code");

            while (rs.next()) {
                LotMemberLot lt = new LotMemberLot();
                
                lt = getResult(rs);
                lt.setMemName(getMember(log, lt.getMemCode()).getMemName());
                CVi.add(lt);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    private static String checkisNull(String str){
        
        String rtn = "";
        
        if(str==null/* || str.equals("null") || str.equals("NULL") || str == null || str == "null" || str == "NULL"*/){
            rtn = "-";
        }else{
            rtn = str;
        }
        
        return rtn;
        
    }
    
    public static int updateMemberInfo(LoginProfile log, LotMember item) throws Exception {
        
        int t = 0;


        try {
            String q = ("UPDATE lot_memberinfo SET mem_Name=?, ic=?, address1=?, address2=?, address3=?, address4=?, address5=?, bank=?, acc=?, paymethod=?, status=?  WHERE mem_Code = '" + item.getMemCode() + "'");

            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getMemName());
            ps.setString(2, item.getIc());
            ps.setString(3, item.getAddress1());
            ps.setString(4, item.getAddress2());
            ps.setString(5, item.getAddress3());
            ps.setString(6, item.getAddress4());
            ps.setString(7, item.getAddress5());
            ps.setString(8, item.getBank());
            ps.setString(9, item.getAcc());
            ps.setString(10, item.getPaymethod());
            ps.setString(11, item.getStatus());

            Logger.getLogger(LotMemberDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();
            
            t = 1;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return t;

    }
    
   

}
