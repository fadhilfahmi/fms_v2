/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.configuration;

import com.lcsb.fms.model.setup.configuration.Executive;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class ExecDAO {
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010116");
        mod.setModuleDesc("Executive");
        mod.setMainTable("executive");
        mod.setReferID_Master("id");
        return mod;
         
    }
    public static List<ListTable> tableList(){
        
        String column_names[] = {"id","name","status","newic"};
        String title_name[] = {"Id","Name","Service Status","New I/C No."};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
    
    public static void saveData(LoginProfile log, Executive item) throws Exception{
        
        try {
            
            String q = ("insert into executive(id, name, status, birth, sex, marital, religion, race, citizen, blood, oldic, newic, passport, address, city, state, postcode, phone, hp, fax, email) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, item.getId());
                ps.setString(2, item.getName());
                ps.setString(3, item.getStatus());
                ps.setString(4, item.getBirth());
                ps.setString(5, item.getSex());
                ps.setString(6, item.getMarital());
                ps.setString(7, item.getReligion());
                ps.setString(8, item.getRace());
                ps.setString(9, item.getCitizen());
                ps.setString(10, item.getBlood());
                ps.setString(11, item.getOldic());
                ps.setString(12, item.getNewic());
                ps.setString(13, item.getPassport());
                ps.setString(14, item.getAddress());
                ps.setString(15, item.getCity());
                ps.setString(16, item.getState());
                ps.setString(17, item.getPostcode());
                ps.setString(18, item.getPhone());
                ps.setString(19, item.getHp());
                ps.setString(20, item.getFax());
                ps.setString(21, item.getEmail());
             
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
         
        
        try{
            String deleteQuery_2 = "delete from executive where id = ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, no);
            ps_2.executeUpdate();
            ps_2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, Executive item) throws Exception{
        
        
        try{
            String q = ("UPDATE executive SET id = ?,  name = ?,  status = ?,  birth = ?,  sex = ?,  marital = ?,  religion = ?,  race = ?,  citizen = ?,  blood = ?,  oldic = ?, newic = ?,  passport = ?,  address = ?,  city = ?,  state = ?,  postcode = ?,  phone = ?,  hp = ?,  fax = ?,  email = ? WHERE id = '"+item.getId()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getId());
            ps.setString(2, item.getName());
            ps.setString(3, item.getStatus());
            ps.setString(4, item.getBirth());
            ps.setString(5, item.getSex());
            ps.setString(6, item.getMarital());
            ps.setString(7, item.getReligion());
            ps.setString(8, item.getRace());
            ps.setString(9, item.getCitizen());
            ps.setString(10, item.getBlood());
            ps.setString(11, item.getOldic());
            ps.setString(12, item.getNewic());
            ps.setString(13, item.getPassport());
            ps.setString(14, item.getAddress());
            ps.setString(15, item.getCity());
            ps.setString(16, item.getState());
            ps.setString(17, item.getPostcode());
            ps.setString(18, item.getPhone());
            ps.setString(19, item.getHp());
            ps.setString(20, item.getFax());
            ps.setString(21, item.getEmail());
           Logger.getLogger(ExecDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static Executive getInfo(LoginProfile log, String no) throws SQLException, Exception {
        Executive c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM executive WHERE id=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static Executive getInfo(ResultSet rs) throws SQLException {
        Executive g = new Executive();
        g.setAddress(rs.getString("address"));
        g.setBirth(rs.getString("birth"));
        g.setBlood(rs.getString("blood"));
        g.setCitizen(rs.getString("citizen"));
        g.setCity(rs.getString("city"));
        g.setEmail(rs.getString("email"));
        g.setFax(rs.getString("fax"));
        g.setHp(rs.getString("hp"));
        g.setId(rs.getString("id"));
        g.setMarital(rs.getString("marital"));
        g.setName(rs.getString("name"));
        g.setNewic(rs.getString("newic"));
        g.setOldic(rs.getString("oldic"));
        g.setPassport(rs.getString("passport"));
        g.setPhone(rs.getString("phone"));
        g.setPostcode(rs.getString("postcode"));
        g.setRace(rs.getString("race"));
        g.setReligion(rs.getString("religion"));
        g.setSex(rs.getString("sex"));
        g.setState(rs.getString("state"));
        g.setStatus(rs.getString("status"));
        
        return g;
    }
    
}
