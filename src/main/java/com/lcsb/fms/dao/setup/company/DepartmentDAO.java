/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.company;

import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.company.Department;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class DepartmentDAO {
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010227");
        mod.setModuleDesc("Department");
        mod.setMainTable("department");
        mod.setReferID_Master("code");
        return mod;
         
    }
    public static List<ListTable> tableList(){
        
        String column_names[] = {"code","name","type","active"};
        String title_name[] = {"Code","Name","Type","Active"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
    
    public static void saveData(LoginProfile log, Department item) throws Exception{
        try {
            
            String q = ("insert into department(code, type, name, active) values (?,?,?,?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, AutoGenerate.get4digitNo(log, "department", "code"));
                ps.setString(2, item.getType());
                ps.setString(3, item.getName());
                ps.setString(4, item.getActive());
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
         
        
        try{
            String deleteQuery_2 = "delete from department where code = ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, no);
            ps_2.executeUpdate();
            ps_2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, Department item) throws Exception{
        
        
        try{
            String q = ("UPDATE department SET code = ?,  type = ?,  name = ?,  active = ?  WHERE code = '"+item.getCode()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCode());
            ps.setString(2, item.getType());
            ps.setString(3, item.getName());
            ps.setString(4, item.getActive()); 
           Logger.getLogger(DepartmentDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static Department getInfo(LoginProfile log, String no) throws SQLException, Exception {
        Department c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM department WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static Department getInfo(ResultSet rs) throws SQLException {
        Department g = new Department();
        g.setActive(rs.getString("active"));
        g.setCode(rs.getString("code"));
        g.setName(rs.getString("name"));
        g.setType(rs.getString("type"));
       
        return g;
    }
    
}
