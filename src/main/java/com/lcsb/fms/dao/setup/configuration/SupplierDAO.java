
package com.lcsb.fms.dao.setup.configuration;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.configuration.Supplier;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.dao.LocationDAO;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class SupplierDAO {
     public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010118");
        mod.setModuleDesc("Supplier Information");
        mod.setMainTable("supplier_info");
        mod.setReferID_Master("code");
        return mod;
         
    }
    
     public static List<ListTable> tableList(){
        
        String column_names[] = {"code","companyname","coregister"};
        String title_name[] = {"Code","Company Name","Register No."};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
     
    public static void saveData(LoginProfile log, Supplier item, String code) throws Exception{
        
        try {
            
            String q = ("insert into supplier_info( code, companyname, coregister,  bumiputra,  hqactdesc,  hqactcode,  coa,  coadescp,  person,  title, position,  hp,  address,  city,  state,  postcode,  country,  phone,  fax,  email,  url,  bank,  bankaccount, payment, remarks, bankdesc, depositcode, depositdesc, suspensecode, suspensedesc,gstid) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            //ps.setString(1, item.getVendor());
            ps.setString(1, code);
            ps.setString(2, item.getCompanyname());
            ps.setString(3, item.getCoregister());
            ps.setString(4, item.getBumiputra());
            ps.setString(5, item.getHqactdesc());
            ps.setString(6, item.getHqactcode());
            ps.setString(7, item.getCoa());
            ps.setString(8, item.getCoadescp());
            ps.setString(9, item.getPerson());
            ps.setString(10, item.getTitle());
            ps.setString(11, item.getPosition());
            ps.setString(12, item.getHp());
            ps.setString(13, item.getAddress());
            ps.setString(14, item.getCity());
            ps.setString(15, item.getState());
            ps.setString(16, item.getPostcode());
            ps.setString(17, item.getCountry());
            ps.setString(18, item.getPhone());
            ps.setString(19, item.getFax());
            ps.setString(20, item.getEmail());
            ps.setString(21, item.getUrl());
            ps.setString(22, item.getBank());
            ps.setString(23, item.getBankaccount());
            ps.setString(24, item.getPayment());
            ps.setString(25, item.getRemarks());
            ps.setString(26, item.getBankdesc());
            ps.setString(27, item.getDepositcode());
            ps.setString(28, item.getDepositdesc());
            ps.setString(29, item.getSuspensecode());
            ps.setString(30, item.getSuspensedesc());
            ps.setString(31, item.getGstid());
            
           ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static void saveDataToHQ(LoginProfile log, Supplier item) throws Exception{
        
        Logger.getLogger(SupplierDAO.class.getName()).log(Level.INFO, "-------testtt----");
        Connection con = ConnectionUtil.getHQConnection();
        
        String newcode = AutoGenerate.getNewCode("supplier_info", "code");
        //String no, String bankcode, String bankname, Date tarikh, String startcek, int nocek, String endcek, String active)
        try {
            
            String q = ("insert into supplier_info( code, companyname, coregister,  bumiputra,  hqactdesc,  hqactcode,  coa,  coadescp,  person,  title, position,  hp,  address,  city,  state,  postcode,  country,  phone,  fax,  email,  url,  bank,  bankaccount, payment, remarks, bankdesc, depositcode, depositdesc, suspensecode, suspensedesc,gstid) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = con.prepareStatement(q);
            //ps.setString(1, item.getVendor());
            ps.setString(1, newcode);
            ps.setString(2, item.getCompanyname());
            ps.setString(3, item.getCoregister());
            ps.setString(4, item.getBumiputra());
            ps.setString(5, item.getHqactdesc());
            ps.setString(6, item.getHqactcode());
            ps.setString(7, item.getCoa());
            ps.setString(8, item.getCoadescp());
            ps.setString(9, item.getPerson());
            ps.setString(10, item.getTitle());
            ps.setString(11, item.getPosition());
            ps.setString(12, item.getHp());
            ps.setString(13, item.getAddress());
            ps.setString(14, item.getCity());
            ps.setString(15, item.getState());
            ps.setString(16, item.getPostcode());
            ps.setString(17, item.getCountry());
            ps.setString(18, item.getPhone());
            ps.setString(19, item.getFax());
            ps.setString(20, item.getEmail());
            ps.setString(21, item.getUrl());
            ps.setString(22, item.getBank());
            ps.setString(23, item.getBankaccount());
            ps.setString(24, item.getPayment());
            ps.setString(25, item.getRemarks());
            ps.setString(26, item.getBankdesc());
            ps.setString(27, item.getDepositcode());
            ps.setString(28, item.getDepositdesc());
            ps.setString(29, item.getSuspensecode());
            ps.setString(30, item.getSuspensedesc());
            ps.setString(31, item.getGstid());
            
           ps.executeUpdate();
            ps.close();
            
            SupplierDAO.saveData(log, item, newcode);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
         
        
        try{
            String deleteQuery_2 = "delete from supplier_info where code = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, no);
                ps_2.executeUpdate();
            }
        } catch (SQLException e) {
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, Supplier item) throws Exception{
        
        try{
            String q = ("UPDATE supplier_info SET vendor=? ,code=? , companyname=? , coregister=? ,  bumiputra=? ,  hqactdesc=? ,  hqactcode=? ,  coa=? ,  coadescp=? ,  person=? ,  title=? , position=? ,  hp=? ,  address=? ,  city=? ,  state=? ,  postcode=? ,  country=? ,  phone=? ,  fax=? ,  email=? ,  url=? ,  bank=? ,  bankaccount=? , payment=? , remarks=? , bankdesc=? , depositcode=? , depositdesc=? , suspensecode=? , suspensedesc=?,gstid=?  WHERE code = '"+item.getCode()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getVendor());
            ps.setString(2, item.getCode());
            ps.setString(3, item.getCompanyname());
            ps.setString(4, item.getCoregister());
            ps.setString(5, item.getBumiputra());
            ps.setString(6, item.getHqactdesc());
            ps.setString(7, item.getHqactcode());
            ps.setString(8, item.getCoa());
            ps.setString(9, item.getCoadescp());
            ps.setString(10, item.getPerson());
            ps.setString(11, item.getTitle());
            ps.setString(12, item.getPosition());
            ps.setString(13, item.getHp());
            ps.setString(14, item.getAddress());
            ps.setString(15, item.getCity());
            ps.setString(16, item.getState());
            ps.setString(17, item.getPostcode());
            ps.setString(18, item.getCountry());
            ps.setString(19, item.getPhone());
            ps.setString(20, item.getFax());
            ps.setString(21, item.getEmail());
            ps.setString(22, item.getUrl());
            ps.setString(23, item.getBank());
            ps.setString(24, item.getBankaccount());
            ps.setString(25, item.getPayment());
            ps.setString(26, item.getRemarks());
            ps.setString(27, item.getBankdesc());
            ps.setString(28, item.getDepositcode());
            ps.setString(29, item.getDepositdesc());
            ps.setString(30, item.getSuspensecode());
            ps.setString(31, item.getSuspensedesc());
            ps.setString(32, item.getGstid());
           Logger.getLogger(SupplierDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static Supplier getInfo(LoginProfile log, String no) throws SQLException, Exception {
        Supplier c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM supplier_info WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    
    
    private static Supplier getInfo(ResultSet rs) throws SQLException {
        Supplier g = new Supplier();
        g.setVendor(rs.getString("vendor"));
        g.setCode(rs.getString("code"));
        g.setCompanyname(rs.getString("companyname"));
        g.setCoregister(rs.getString("coregister"));
        g.setBumiputra(rs.getString("bumiputra"));
        g.setHqactdesc(rs.getString("hqactdesc"));
        g.setHqactcode(rs.getString("hqactcode"));
        g.setCoa(rs.getString("coa"));
        g.setCoadescp(rs.getString("coadescp"));
        g.setPerson(rs.getString("person"));//10
        g.setTitle(rs.getString("title"));
        g.setPosition(rs.getString("position"));
        g.setHp(rs.getString("hp"));
        g.setAddress(rs.getString("address"));
        g.setCity(rs.getString("city"));
        g.setState(rs.getString("state"));
        g.setPostcode(rs.getString("postcode"));
        g.setCountry(rs.getString("country"));
        g.setPhone(rs.getString("phone"));
        g.setFax(rs.getString("fax"));//20
        g.setEmail(rs.getString("email"));
        g.setUrl(rs.getString("url"));
        g.setBank(rs.getString("bank"));
        g.setBankaccount(rs.getString("bankaccount"));
        g.setPayment(rs.getString("payment"));
        g.setRemarks(rs.getString("remarks"));
        g.setBankdesc(rs.getString("bankdesc"));
        g.setDepositcode(rs.getString("depositcode"));
        g.setDepositdesc(rs.getString("depositdesc"));
        g.setSuspensecode(rs.getString("suspensecode"));
        g.setSuspensedesc(rs.getString("suspensedesc"));
        g.setGstid(rs.getString("gstid"));
        
        
       
        return g;
    }
    
    private static Supplier getInfoHQ(ResultSet rs) throws SQLException {
        Supplier g = new Supplier();
        g.setCode(rs.getString("code"));
        g.setCompanyname(rs.getString("companyname"));
        g.setCoregister(rs.getString("coregister"));
        g.setBumiputra(rs.getString("bumiputra"));
        g.setHqactdesc(rs.getString("hqactdesc"));
        g.setHqactcode(rs.getString("hqactcode"));
        g.setCoa(rs.getString("coa"));
        g.setCoadescp(rs.getString("coadescp"));
        g.setPerson(rs.getString("person"));//10
        g.setTitle(rs.getString("title"));
        g.setPosition(rs.getString("position"));
        g.setHp(rs.getString("hp"));
        g.setAddress(rs.getString("address"));
        g.setCity(rs.getString("city"));
        g.setState(rs.getString("state"));
        g.setPostcode(rs.getString("postcode"));
        g.setCountry(rs.getString("country"));
        g.setPhone(rs.getString("phone"));
        g.setFax(rs.getString("fax"));//20
        g.setEmail(rs.getString("email"));
        g.setUrl(rs.getString("url"));
        g.setBank(rs.getString("bank"));
        g.setBankaccount(rs.getString("bankaccount"));
        g.setPayment(rs.getString("payment"));
        g.setRemarks(rs.getString("remarks"));
        g.setBankdesc(rs.getString("bankdesc"));
        g.setDepositcode(rs.getString("depositcode"));
        g.setDepositdesc(rs.getString("depositdesc"));
        g.setSuspensecode(rs.getString("suspensecode"));
        g.setSuspensedesc(rs.getString("suspensedesc"));
        g.setGstid(rs.getString("gstid"));
        
        
       
        return g;
    }
    
    public static List<com.lcsb.fms.util.model.Supplier> getSupplierHQ(String keyword) throws Exception {
        
        Connection con = ConnectionUtil.getHQConnection();
        
        Statement stmt = null;
        List<com.lcsb.fms.util.model.Supplier> Com;
        Com = new ArrayList();
        
        String q = "";
        
        if(keyword!=null){
            q = "where code like '%"+keyword+"%' or companyname like '%"+keyword+"%'";
        }
        
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select code,companyname,address,city,state,postcode from supplier_info "+q+" order by code limit 10");
Logger.getLogger(LocationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getResult(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    private static com.lcsb.fms.util.model.Supplier getResult(ResultSet rs) throws SQLException {
        com.lcsb.fms.util.model.Supplier comp= new com.lcsb.fms.util.model.Supplier();
        comp.setSupplierAddress(rs.getString("address"));
        comp.setSupplierCity(rs.getString("city"));
        comp.setSupplierCode(rs.getString("code"));
        comp.setSupplierName(rs.getString("companyname"));
        comp.setSupplierPostcode(rs.getString("postcode"));
        comp.setSupplierState(rs.getString("state"));
        return comp;
    }
    
    
    public static int checkExist(LoginProfile log, String no) throws Exception{
        
        int i = 0;
        
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT count(*) as cnt FROM supplier_info WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            i = rs.getInt("cnt");
        }
        
        return i;
    }
    
    public static Supplier getHQSupplier(String no) throws SQLException, Exception {
        Supplier c = null;
        ResultSet rs = null;
        Connection conet = ConnectionUtil.getHQConnection();
        PreparedStatement stmt = conet.prepareStatement("SELECT * FROM supplier_info WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfoHQ(rs);
        }

        
        return c;
    }
    
    public static void saveDataFromHQ(LoginProfile log, Supplier item, String code) throws Exception{
        
        try {
            
            String q = ("insert into supplier_info( code, companyname, coregister,  bumiputra,  hqactdesc,  hqactcode,  coa,  coadescp,  person,  title, position,  hp,  address,  city,  state,  postcode,  country,  phone,  fax,  email,  url,  bank,  bankaccount, payment, remarks, bankdesc, depositcode, depositdesc, suspensecode, suspensedesc,gstid) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            //ps.setString(1, item.getVendor());
            ps.setString(1, item.getCode());
            ps.setString(2, item.getCompanyname());
            ps.setString(3, item.getCoregister());
            ps.setString(4, item.getBumiputra());
            ps.setString(5, item.getHqactdesc());
            ps.setString(6, item.getHqactcode());
            ps.setString(7, item.getCoa());
            ps.setString(8, item.getCoadescp());
            ps.setString(9, item.getPerson());
            ps.setString(10, item.getTitle());
            ps.setString(11, item.getPosition());
            ps.setString(12, item.getHp());
            ps.setString(13, item.getAddress());
            ps.setString(14, item.getCity());
            ps.setString(15, item.getState());
            ps.setString(16, item.getPostcode());
            ps.setString(17, item.getCountry());
            ps.setString(18, item.getPhone());
            ps.setString(19, item.getFax());
            ps.setString(20, item.getEmail());
            ps.setString(21, item.getUrl());
            ps.setString(22, item.getBank());
            ps.setString(23, item.getBankaccount());
            ps.setString(24, item.getPayment());
            ps.setString(25, item.getRemarks());
            ps.setString(26, item.getBankdesc());
            ps.setString(27, item.getDepositcode());
            ps.setString(28, item.getDepositdesc());
            ps.setString(29, item.getSuspensecode());
            ps.setString(30, item.getSuspensedesc());
            ps.setString(31, item.getGstid());
            
           ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static int retrieveData(LoginProfile log, String no) throws Exception{
        
        int i = 0;
        
        try{
            saveDataFromHQ(log, getHQSupplier(no),"");
            i = 1;
        } catch (SQLException e) {
            e.printStackTrace();
            i = 0;
        }
        
        return i;
    }
    
}

