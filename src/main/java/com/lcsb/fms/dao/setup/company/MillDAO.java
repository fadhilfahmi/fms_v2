/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.company;

import com.lcsb.fms.dao.financial.ap.VendorPaymentDAO;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.company.CeManage;
import com.lcsb.fms.model.setup.company.CeManageBank;
import com.lcsb.fms.model.setup.company.CeManageBankPK;
import com.lcsb.fms.model.setup.company.CeManagePetty;
import com.lcsb.fms.model.setup.company.Mill;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class MillDAO {
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010206");
        mod.setModuleDesc("Mill");
        mod.setMainTable("mill_info");
        mod.setReferID_Master("code");
        return mod;
         
    }
    
    public static List<ListTable> tableList(){
        
        String column_names[] = {"code","descp","active","defaultmill"};
        String title_name[] = {"Code","Description", "Active", "Default"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
    
    public static void saveData(LoginProfile log, Mill item) throws Exception {
        
        try {

            String q = ("insert into mill_info(code,descp,coacode,coadescp,active,defaultmill) values (?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCode());
            ps.setString(2, item.getDescp());
            ps.setString(3, item.getCoacode());
            ps.setString(4, item.getCoadescp());
            ps.setString(5, item.getActive());
            ps.setString(6, item.getDefaultmill());

            ps.executeUpdate();
            ps.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
    public static Mill getInfo(LoginProfile log, String no) throws SQLException, Exception {
        
        Mill c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM mill_info WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        return c;
    }
    
    
    private static Mill getInfo(ResultSet rs) throws SQLException {
        Mill g = new Mill();
        g.setActive(rs.getString("active"));
        g.setDefaultmill(rs.getString("defaultmill"));
        g.setCoacode(rs.getString("coacode"));
        g.setCoadescp(rs.getString("coadescp"));
        g.setCode(rs.getString("code"));
        g.setDescp(rs.getString("descp"));
        
        return g;
    }
    
    public static void updateData(LoginProfile log, Mill item) throws Exception {

        try {

            String q = ("UPDATE mill_info SET coacode = ?,coadescp = ?,code = ?,descp = ?,active = ?,defaultmill = ? WHERE code = '"+item.getCode()+"'");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCoacode());
            ps.setString(2, item.getCoadescp());
            ps.setString(3, item.getCode());
            ps.setString(4, item.getDescp());
            ps.setString(5, item.getActive());
            ps.setString(6, item.getDefaultmill());

            ps.executeUpdate();
            ps.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public static void deleteData(LoginProfile log, String no) throws SQLException, Exception {


        try {
            String deleteQuery_2 = "delete from mill_info where code = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, no);
                ps_2.executeUpdate();
            }
        } catch (SQLException e) {
        }

    }
    
    public static Mill getDefaultMill(LoginProfile log) throws Exception {

        Mill sc = new Mill();

        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM mill_info WHERE defaultmill = 'Yes'");
            //stmt.setString(1, "%"+refer.substring(0, 3)+"%");
            rs = stmt.executeQuery();
            if (rs.next()) {
                sc = getInfo(rs);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return sc;
    }
    
}
