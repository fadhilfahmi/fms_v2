/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.company;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.company.Board;
import com.lcsb.fms.model.setup.configuration.LotMember;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BoardDAO {
     public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010228");
        mod.setModuleDesc("Board Information");
        mod.setMainTable("board_info");
        mod.setReferID_Master("code");
        return mod;
         
    }
    
     public static List<ListTable> tableList(){
        
        String column_names[] = {"code","name","ic","status"};
        String title_name[] = {"Board Code","Board Name","IC Number","Status"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
     
    public static void saveData(LoginProfile log, Board item) throws Exception{
        try {
            
            String q = ("insert into board_info(code, title, usetitle, name, ic, address, postcode, city, state, bank, acc, paymethod, status, notel,  remark) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, AutoGenerate.get4digitNo(log, "board_info", "code"));
            ps.setString(2, item.getTitle());
            ps.setString(3, item.getUsetitle());
            ps.setString(4, item.getName());
            ps.setString(5, item.getIc());
            ps.setString(6, item.getAddress());
            ps.setString(7, item.getPostcode());
            ps.setString(8, item.getCity());
            ps.setString(9, item.getState());
            ps.setString(10, item.getBank());
            ps.setString(11, item.getAcc());
            ps.setString(12, item.getPaymethod());
            ps.setString(13, item.getStatus());
            ps.setString(14, item.getNotel());
            ps.setString(15, item.getRemark());
            
            
            
            
            
            
            
           ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
         
        
        try{
            String deleteQuery_2 = "delete from board_info where code = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, no);
                ps_2.executeUpdate();
            }
        } catch (SQLException e) {
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, Board item) throws Exception{
        
        
        try{
            String q = ("UPDATE board_info SET code=?, title=?, usetitle=?, name=?, ic=?, address=?, postcode=?, city=?, state=?, bank=?, acc=?, paymethod=?, status=?, notel=?, remark=?  WHERE code = '"+item.getCode()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCode());
            ps.setString(2, item.getTitle());
            ps.setString(3, item.getUsetitle());
            ps.setString(4, item.getName());
            ps.setString(5, item.getIc());
            ps.setString(6, item.getAddress());
            ps.setString(7, item.getPostcode());
            ps.setString(8, item.getCity());
            ps.setString(9, item.getState());
            ps.setString(10, item.getBank());
            ps.setString(11, item.getAcc());
            ps.setString(12, item.getPaymethod());
            ps.setString(13, item.getStatus());
            ps.setString(14, item.getNotel());
            ps.setString(15, item.getRemark());
            
            
            
           Logger.getLogger(BoardDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static Board getInfo(LoginProfile log, String no) throws SQLException, Exception {
         Board c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT code, title, usetitle, name, ic, address, postcode, city, state, bank, acc, paymethod, status, notel, remark FROM board_info where code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static Board getInfo(ResultSet rs) throws SQLException {
        Board g = new Board();
        g.setCode(rs.getString("code"));
        g.setTitle(rs.getString("title"));
        g.setUsetitle(rs.getString("usetitle"));
        g.setName(rs.getString("name"));
        g.setIc(rs.getString("ic"));
        g.setAddress(rs.getString("address"));
        g.setPostcode(rs.getString("postcode"));
        g.setCity(rs.getString("city"));
        g.setState(rs.getString("state"));
        g.setBank(rs.getString("bank"));
        g.setAcc(rs.getString("acc"));
        g.setPaymethod(rs.getString("paymethod"));
        g.setStatus(rs.getString("Status"));
        g.setNotel(rs.getString("notel"));
        g.setRemark(rs.getString("remark"));
       
        return g;
    }

    
    
}

