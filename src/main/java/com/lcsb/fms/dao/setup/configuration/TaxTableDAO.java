/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.configuration;

import com.lcsb.fms.model.setup.company.staff.StaffBank;
import com.lcsb.fms.model.setup.configuration.TaxTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class TaxTableDAO {
    
    public static Module getModule() {

        Module mod = new Module();
        mod.setModuleID("010136");
        mod.setModuleDesc("Income Tax Table");
        mod.setMainTable("cf_taxtable");
        mod.setReferID_Master("id");
        return mod;

    }
    
    public static List<TaxTable> getListTax(LoginProfile log) {

        ResultSet rs = null;
        List<TaxTable> CVi;
        CVi = new ArrayList();

        try {
            PreparedStatement stmt = log.getCon().prepareStatement("select * from cf_taxtable");
            rs = stmt.executeQuery();
            while (rs.next()) {
                CVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return CVi;
    }
    
    public static TaxTable getTaxTable(LoginProfile log, String no) throws SQLException, Exception {
        TaxTable c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM cf_taxtable");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        return c;
    }
    
    private static TaxTable getInfo(ResultSet rs) throws SQLException {

        TaxTable g = new TaxTable();

        g.setB13(rs.getDouble("B13"));
        g.setB2(rs.getDouble("B2"));
        g.setId(rs.getInt("id"));
        g.setM(rs.getDouble("M"));
        g.setPend(rs.getDouble("Pend"));
        g.setPstart(rs.getDouble("Pstart"));
        g.setR(rs.getDouble("R"));

        return g;
    }
    
}
