/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.configuration;

import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.configuration.LotInformation;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class LotInformationDAO {
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010126");
        mod.setModuleDesc("Lot Information");
        mod.setMainTable("lot_info");
        mod.setReferID_Master("lot_Code");
        return mod;
         
    }
    public static List<ListTable> tableList(){
        
        String column_names[] = {"lot_Code","lot_Descp","lot_Acre","lot_Hect","active"};
        String title_name[] = {"Lot Code","Lot Name","Acre","Hectare","Active"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
    
    public static void saveData(LoginProfile log, LotInformation item) throws Exception{
        
        try {
            
            String q = ("insert into lot_info(lot_Code, loccode, lot_Descp, lot_Hect, lot_Acre, active, loc_Descp) values (?,?,?,?,?,?,?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, AutoGenerate.get4digitNo(log, "lot_info", "lot_Code"));
                ps.setString(2, item.getLoccode());
                ps.setString(3, item.getLotDescp());
                ps.setString(4, item.getLotHect());
                ps.setString(5, item.getLotAcre());
                ps.setString(6, item.getActive());
                ps.setString(7, item.getLocDescp());
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
        
        try{
            String deleteQuery_2 = "delete from lot_info where lot_Code = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, no);
                ps_2.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, LotInformation item) throws Exception{
        
        try{
            String q = ("UPDATE lot_info SET lot_Code = ?,  loccode = ?,  lot_Descp = ?,  lot_Hect = ?,  lot_Acre = ?,  active = ?  loc_Descp = ? WHERE lot_Code = '"+item.getLotCode()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getLotCode());
            ps.setString(2, item.getLoccode());
            ps.setString(3, item.getLotDescp());
            ps.setString(4, item.getLotHect());
            ps.setString(5, item.getLotAcre());
            ps.setString(6, item.getActive()); 
            ps.setString(7, item.getLocDescp());
           Logger.getLogger(LotInformationDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static LotInformation getInfo(LoginProfile log, String no) throws SQLException, Exception {
        LotInformation c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM lot_info WHERE lot_Code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static LotInformation getInfo(ResultSet rs) throws SQLException {
        LotInformation g = new LotInformation();
        g.setActive(rs.getString("active"));
        g.setLocDescp(rs.getString("loc_Descp"));
        g.setLoccode(rs.getString("loccode"));
        g.setLotAcre(rs.getString("lot_Acre"));
        g.setLotCode(rs.getString("lot_Code"));
        g.setLotDescp(rs.getString("lot_Descp"));
        g.setLotHect(rs.getString("lot_Hect"));
       
        return g;
    }
    
    public static List<LotInformation> getAllLot(LoginProfile log, String keyword) throws Exception {
        
        
        
        String q = "";
        
        if(keyword!=null){
            q = "where lot_Code like '%"+keyword+"%' or lot_Descp like '%"+keyword+"%'";
        }
        
        Statement stmt = null;
        List<LotInformation> Com;
        Com = new ArrayList();
        
        
        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from lot_info "+q+" order by lot_Code limit 10");
Logger.getLogger(LotInformationDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
}
