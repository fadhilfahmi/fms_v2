
package com.lcsb.fms.dao.setup.configuration;
import com.lcsb.fms.model.setup.configuration.Contractor;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class ContractorDAO {
     public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010120");
        mod.setModuleDesc("Contractor Information");
        mod.setMainTable("contractor_info");
        mod.setReferID_Master("code");
        return mod;
         
    }
    
     public static List<ListTable> tableList(){
        
        String column_names[] = {"code","companyname","register"};
        String title_name[] = {"Code","Company Name","Register No."};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
     
    public static void saveData(LoginProfile log, Contractor item) throws Exception{
        
        try {
            
            String q = ("insert into contractor_info(vendor,code, companyname, register,  bumiputra, ownername, owneric, nationality, kelas, coa, coadescp, contactperson, contacttitle, contactposition, address, city, state, postcode, country, phone, fax, email, url, bank, bankaccount, payment, remarks, depositcoa, depositcoadescp, retention, retentiondescp, hp, bankdesc, permitcode, permitdesc) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getVendor());
            ps.setString(2, item.getCode());
            ps.setString(3, item.getCompanyname());
            ps.setString(4, item.getRegister());
            ps.setString(5, item.getBumiputra());
            ps.setString(6, item.getOwnername());
            ps.setString(7, item.getOwneric());
            ps.setString(8, item.getNationality());
            ps.setString(9, item.getKelas());
            ps.setString(10, item.getCoa());
            ps.setString(11, item.getCoadescp());
            ps.setString(12, item.getContactperson());
            ps.setString(13, item.getContacttitle());
            ps.setString(14, item.getContactposition());
            ps.setString(15, item.getAddress());
            ps.setString(16, item.getCity());
            ps.setString(17, item.getState());
            ps.setString(18, item.getPostcode());
            ps.setString(19, item.getCountry());
            ps.setString(20, item.getPhone());
            ps.setString(21, item.getFax());
            ps.setString(22, item.getEmail());
            ps.setString(23, item.getUrl());
            ps.setString(24, item.getBank());
            ps.setString(25, item.getBankaccount());
            ps.setString(26, item.getPayment());
            ps.setString(27, item.getRemarks());
            ps.setString(28, item.getDepositcoa());
            ps.setString(29, item.getDepositcoadescp());
            ps.setString(30, item.getRetention());
            ps.setString(31, item.getRetentiondescp());
            ps.setString(32, item.getHp());
            ps.setString(33, item.getBankdesc());
            ps.setString(34, item.getPermitcode());
            ps.setString(35, item.getPermitdesc());
             
           ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
        
        try{
            String deleteQuery_2 = "delete from contractor_info where code = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, no);
                ps_2.executeUpdate();
            }
        } catch (SQLException e) {
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, Contractor item) throws Exception{
        
        
        try{
            String q = ("UPDATE contractor_info SET vendor=?, code=?, companyname=?, register=?,  bumiputra=?, ownername=?, owneric=?, nationality=?, kelas=?, coa=?, coadescp=?, contactperson=?, contacttitle=?, contactposition=?, address=?, city=?, state=?, postcode=?, country=?, phone=?, fax=?, email=?, url=?, bank=?, bankaccount=?, payment=?, remarks=?, depositcoa=?, depositcoadescp=?, retention=?, retentiondescp=?, hp=?, bankdesc=?, permitcode=?, permitdesc=?  WHERE code = '"+item.getCode()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getVendor());
            ps.setString(2, item.getCode());
            ps.setString(3, item.getCompanyname());
            ps.setString(4, item.getRegister());
            ps.setString(5, item.getBumiputra());
            ps.setString(6, item.getOwnername());
            ps.setString(7, item.getOwneric());
            ps.setString(8, item.getNationality());
            ps.setString(9, item.getKelas());
            ps.setString(10, item.getCoa());
            ps.setString(11, item.getCoadescp());
            ps.setString(12, item.getContactperson());
            ps.setString(13, item.getContacttitle());
            ps.setString(14, item.getContactposition());
            ps.setString(15, item.getAddress());
            ps.setString(16, item.getCity());
            ps.setString(17, item.getState());
            ps.setString(18, item.getPostcode());
            ps.setString(19, item.getCountry());
            ps.setString(20, item.getPhone());
            ps.setString(21, item.getFax());
            ps.setString(22, item.getEmail());
            ps.setString(23, item.getUrl());
            ps.setString(24, item.getBank());
            ps.setString(25, item.getBankaccount());
            ps.setString(26, item.getPayment());
            ps.setString(27, item.getRemarks());
            ps.setString(28, item.getDepositcoa());
            ps.setString(29, item.getDepositcoadescp());
            ps.setString(30, item.getRetention());
            ps.setString(31, item.getRetentiondescp());
            ps.setString(32, item.getHp());
            ps.setString(33, item.getBankdesc());
            ps.setString(34, item.getPermitcode());
            ps.setString(35, item.getPermitdesc());
           Logger.getLogger(ContractorDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static Contractor getInfo(LoginProfile log, String no) throws SQLException, Exception {
        Contractor c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM contractor_info WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static Contractor getInfo(ResultSet rs) throws SQLException {
        Contractor g = new Contractor();
        g.setVendor(rs.getString("vendor"));
        g.setCode(rs.getString("code"));
        g.setCompanyname(rs.getString("companyname"));
        g.setRegister(rs.getString("register"));
        g.setBumiputra(rs.getString("bumiputra"));
        g.setOwnername(rs.getString("ownername"));
        g.setOwneric(rs.getString("owneric"));
        g.setNationality(rs.getString("nationality"));
        g.setKelas(rs.getString("kelas"));
        g.setCoa(rs.getString("coa"));//10
        g.setCoadescp(rs.getString("coadescp"));
        g.setContactperson(rs.getString("contactperson"));
        g.setContacttitle(rs.getString("contacttitle"));
        g.setContactposition(rs.getString("contactposition"));
        g.setAddress(rs.getString("address"));
        g.setCity(rs.getString("city"));
        g.setState(rs.getString("state"));
        g.setPostcode(rs.getString("postcode"));
        g.setCountry(rs.getString("country"));
        g.setPhone(rs.getString("phone"));//20
        g.setFax(rs.getString("fax"));
        g.setEmail(rs.getString("email"));
        g.setUrl(rs.getString("url"));
        g.setBank(rs.getString("bank"));
        g.setBankaccount(rs.getString("bankaccount"));
        g.setPayment(rs.getString("payment"));
        g.setRemarks(rs.getString("remarks"));
        g.setDepositcoa(rs.getString("depositcoa"));
        g.setDepositcoadescp(rs.getString("depositcoadescp"));
        g.setRetention(rs.getString("retention"));
        g.setRetentiondescp(rs.getString("retentiondescp"));
        g.setHp(rs.getString("hp"));
        g.setBankdesc(rs.getString("bankdesc"));
        g.setPermitcode(rs.getString("permitcode"));
        g.setPermitdesc(rs.getString("permitdesc"));
        
       
        return g;
    }
    
}


