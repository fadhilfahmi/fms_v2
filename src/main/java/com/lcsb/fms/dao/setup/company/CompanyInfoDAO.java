package com.lcsb.fms.dao.setup.company;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.company.CompanyInfo;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class CompanyInfoDAO {
     public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010205");
        mod.setModuleDesc("Company Information");
        mod.setMainTable("ce_estate");
        mod.setReferID_Master("code");
        return mod;
         
    }
    
     public static List<ListTable> tableList(){
        
        String column_names[] = {"code","name"};
        String title_name[] = {"Code","Name"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
     
    public static void saveData(LoginProfile log, CompanyInfo item) throws Exception{
        
        try {
            
            String q = ("insert into ce_estate(code, name) values (?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, AutoGenerate.get4digitNo(log, "ce_estate", "code"));
            ps.setString(2, item.getName());
            
            
           ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
     public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
         
        
        try{
            String deleteQuery_2 = "delete from ce_estate where code = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, no);
                ps_2.executeUpdate();
            }
        } catch (SQLException e) {
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, CompanyInfo item) throws Exception{
        
        
        try{
            String q = ("UPDATE ce_estate SET code=? , name=?   WHERE code = '"+item.getCode()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
           
            ps.setString(1, item.getCode());
            ps.setString(2, item.getName());
            
           
           Logger.getLogger(CompanyInfoDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static CompanyInfo getInfo(LoginProfile log, String no) throws SQLException, Exception {
        CompanyInfo c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM ce_estate WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static CompanyInfo getInfo(ResultSet rs) throws SQLException {
        CompanyInfo g = new CompanyInfo();
        
        g.setCode(rs.getString("code"));
        g.setName(rs.getString("name"));
       
        return g;
    }
    
}

