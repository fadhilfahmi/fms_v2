/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.configuration;

import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.configuration.Management;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class ManagementDAO {
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010121");
        mod.setModuleDesc("Management");
        mod.setMainTable("managementinfo");
        mod.setReferID_Master("code");
        return mod;
         
    }
    public static List<ListTable> tableList(){
        
        String column_names[] = {"code","descp","phone","fax"};
        String title_name[] = {"Code","Name","Phone No.","Fax"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
    
    public static void saveData(LoginProfile log, Management item) throws Exception{
        
        try {
            
            String q = ("insert into managementinfo(code, descp, coregister, employeetax, address, postcode, city, state, phone, fax, email, website, mgtcode1, flag1, mgtcode2, flag2, mgtdesc1, mgtdesc2) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            try (PreparedStatement ps = log.getCon().prepareStatement(q)) {
                ps.setString(1, AutoGenerate.get4digitNo(log, "managementinfo", "code"));
                ps.setString(2, item.getDescp());
                ps.setString(3, item.getCoregister());
                ps.setString(4, item.getEmployeetax());
                ps.setString(5, item.getAddress());
                ps.setString(6, item.getPostcode());
                ps.setString(7, item.getCity());
                ps.setString(8, item.getState());
                ps.setString(9, item.getPhone());
                ps.setString(10, item.getFax());
                ps.setString(11, item.getEmail());
                ps.setString(12, item.getWebsite());
                ps.setString(13, item.getMgtcode1());
                ps.setString(14, item.getFlag1());
                ps.setString(15, item.getMgtcode2());
                ps.setString(16, item.getFlag2());
                ps.setString(17, item.getMgtdesc1());
                ps.setString(18, item.getMgtdesc2());              
                
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
         
        
        try{
            String deleteQuery_2 = "delete from managementinfo where code = ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, no);
            ps_2.executeUpdate();
            ps_2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, Management item) throws Exception{
        
        
        try{
            String q = ("UPDATE managementinfo SET code = ?,  descp = ?,  coregister = ?,  employeetax = ?,  address = ?,  postcode = ?,  city = ?,  state = ?,  phone = ?,  fax = ?,  email = ?, website = ?,  mgtcode1 = ?,  flag1 = ?,  mgtcode2 = ?,  flag2 = ?,  mgtdesc1 = ?,  mgtdesc2 = ? WHERE code = '"+item.getCode()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCode());
            ps.setString(2, item.getDescp());
            ps.setString(3, item.getCoregister());
            ps.setString(4, item.getEmployeetax());
            ps.setString(5, item.getAddress());
            ps.setString(6, item.getPostcode());
            ps.setString(7, item.getCity());
            ps.setString(8, item.getState());
            ps.setString(9, item.getPhone());
            ps.setString(10, item.getFax());
            ps.setString(11, item.getEmail());
            ps.setString(12, item.getWebsite());
            ps.setString(13, item.getMgtcode1());
            ps.setString(14, item.getFlag1());
            ps.setString(15, item.getMgtcode2());
            ps.setString(16, item.getFlag2());
            ps.setString(17, item.getMgtdesc1());
            ps.setString(18, item.getMgtdesc2());       
           Logger.getLogger(ManagementDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static Management getInfo(LoginProfile log, String no) throws SQLException, Exception {
        Management c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM managementinfo WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static Management getInfo(ResultSet rs) throws SQLException {
        Management g = new Management();
        g.setAddress(rs.getString("address"));
        g.setCity(rs.getString("city"));
        g.setCode(rs.getString("code"));
        g.setCoregister(rs.getString("coregister"));
        g.setDescp(rs.getString("descp"));
        g.setEmail(rs.getString("email"));
        g.setEmployeetax(rs.getString("employeetax"));
        g.setFax(rs.getString("fax"));
        g.setFlag1(rs.getString("flag1"));
        g.setFlag2(rs.getString("flag2"));
        g.setMgtcode1(rs.getString("mgtcode1"));
        g.setMgtcode2(rs.getString("mgtcode2"));
        g.setMgtdesc1(rs.getString("mgtdesc1"));
        g.setMgtdesc2(rs.getString("mgtdesc2"));
        g.setPhone(rs.getString("phone"));
        g.setPostcode(rs.getString("postcode"));
        g.setState(rs.getString("state"));
        g.setWebsite(rs.getString("website"));       
       
        return g;
    }
    
}
