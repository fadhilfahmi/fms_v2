/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.configuration;

import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.company.Bank;
import com.lcsb.fms.model.setup.configuration.BankGeneral;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class BankGeneralDAO {
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010115");
        mod.setModuleDesc("Bank Information");
        mod.setMainTable("info_bank_general");
        mod.setReferID_Master("code");
        return mod;
         
    }
    
     public static List<ListTable> tableList(){
        
        String column_names[] = {"code","name"};
        String title_name[] = {"Code","Bank Name"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
     
    public static void saveData(LoginProfile log, BankGeneral item) throws Exception{
        
        try {
            
            String q = ("insert into info_bank_general(code, name, address, postcode, city,state, gstid) values (?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, AutoGenerate.get4digitNo(log, "info_bank_general", "code"));
            ps.setString(2, item.getName());
            ps.setString(3, item.getAddress());
            ps.setString(4, item.getPostcode());
            ps.setString(5, item.getCity());
            ps.setString(6, item.getState());
            ps.setString(7, item.getGstid());
            
           ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
         
        
        try{
            String deleteQuery_2 = "delete from info_bank_general where code = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, no);
                ps_2.executeUpdate();
            }
        } catch (SQLException e) {
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, BankGeneral item) throws Exception{
        
        
        try{
            String q = ("UPDATE info_bank_general SET code=? ,name=? , address=? , postcode=? , city=? , state=? , gstid=?  WHERE code = '"+item.getCode()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCode());
            ps.setString(2, item.getName());
            ps.setString(3, item.getAddress());
            ps.setString(4, item.getPostcode());
            ps.setString(5, item.getCity());
            ps.setString(6, item.getState());
            ps.setString(7, item.getGstid());
            
            
           Logger.getLogger(com.lcsb.fms.dao.setup.company.BankDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static BankGeneral getInfo(LoginProfile log, String no) throws SQLException, Exception {
        BankGeneral c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM info_bank_general WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static BankGeneral getInfo(ResultSet rs) throws SQLException {
        BankGeneral g = new BankGeneral();
        g.setCode(rs.getString("code"));
        g.setName(rs.getString("name"));
        g.setAddress(rs.getString("address"));
        g.setPostcode(rs.getString("postcode"));
        g.setCity(rs.getString("city"));
        g.setState(rs.getString("state"));
        g.setGstid(rs.getString("gstid"));
        
        
       
        return g;
    }
    
}
