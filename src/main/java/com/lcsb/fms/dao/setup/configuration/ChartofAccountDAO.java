/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.dao.setup.configuration;

import com.lcsb.fms.model.financial.gl.JournalVoucherItem;
import com.lcsb.fms.model.setup.configuration.ChartofAccount;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class ChartofAccountDAO {
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010109");
        mod.setModuleDesc("Chart of Account");
        mod.setMainTable("chartofacccount");
        mod.setReferID_Master("code");
        return mod;
         
    }
    
    public static void saveData(LoginProfile log, ChartofAccount item) throws Exception{
        
        try {
            String q = ("insert into chartofacccount(code,descp,type,apptype,applevel,attype,finallvl,active) values (?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCode());
            ps.setString(2, item.getDescp());
            ps.setString(3, item.getType());
            ps.setString(4, item.getApptype());
            ps.setString(5, item.getApplevel());
            ps.setString(6, item.getAttype());
            ps.setString(7, item.getFinallvl());
            ps.setString(8, item.getActive());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
         
        
        try{
            String deleteQuery_2 = "delete from chartofacccount where code = ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, no);
            ps_2.executeUpdate();
            ps_2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, ChartofAccount item) throws Exception{
        
        
        try{
            String q = ("UPDATE chartofacccount set code = ?,descp = ?,type = ?,apptype = ?,applevel = ?,attype = ?, finallvl = ?, active = ? WHERE code = '"+item.getCode()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCode());
            ps.setString(2, item.getDescp());
            ps.setString(3, item.getType());
            ps.setString(4, item.getApptype());
            ps.setString(5, item.getApplevel());
            ps.setString(6, item.getAttype());
            ps.setString(7, item.getFinallvl());
            ps.setString(8, item.getActive());
           
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static ChartofAccount getInfo(LoginProfile log, String no) throws SQLException, Exception {
        ChartofAccount c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from chartofacccount where code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static ChartofAccount getInfo(ResultSet rs) throws SQLException {
        ChartofAccount g = new ChartofAccount();
        g.setActive(rs.getString("active"));
        g.setApplevel(rs.getString("applevel"));
        g.setApptype(rs.getString("apptype"));
        g.setAttype(rs.getString("attype"));
        g.setCode(rs.getString("code"));
        g.setDescp(rs.getString("descp"));
        g.setFinallvl(rs.getString("finallvl"));
        g.setReport(rs.getString("report"));
        g.setType(rs.getString("type"));
        return g;
    }
    
    public static List<ChartofAccount> getAllCode(LoginProfile log, String code) throws Exception {

        Statement stmt = null;
        List<ChartofAccount> JVi;
        JVi = new ArrayList();

        try {
            stmt = log.getCon().createStatement();
            ResultSet rs = stmt.executeQuery("select * from chartofacccount where code like '" + code + "%' order by code");

            while (rs.next()) {
                JVi.add(getInfo(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return JVi;
    }
    
    
    
}
