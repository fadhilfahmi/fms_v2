/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.configuration;

import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.configuration.Vendor;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class VendorDAO {
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010130");
        mod.setModuleDesc("Vendor");
        mod.setMainTable("vendor_info");
        mod.setReferID_Master("code");
        return mod;
         
    }
    
    public static List<ListTable> tableList(){
        
        String column_names[] = {"code","vendor_name","register_no","active","hp"};
        String title_name[] = {"Code","Vendor Name","Register No.","Active","Hp"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
    
    public static void saveData(LoginProfile log, Vendor item) throws Exception{
        
        try {
            
            String q = ("insert into vendor_info(code,  vendor_name,  register_no,  vendor_address,  postcode,  city,  state,  country,  active,  bumiputra,  person, title,  hp,  phone,  fax,  email,  url,  bank,  bankaccount,  payment,  remarks,  position,  coa,  coadescp,gstid) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, AutoGenerate.get4digitNo(log, "vendor_info", "code"));
            ps.setString(2, item.getVendorName());
            ps.setString(3, item.getRegisterNo());
            ps.setString(4, item.getVendorAddress());
            ps.setString(5, item.getPostcode());
            ps.setString(6, item.getCity());
            ps.setString(7, item.getState());
            ps.setString(8, item.getCountry());
            ps.setString(9, item.getActive());
            ps.setString(10, item.getBumiputra());
            ps.setString(11, item.getPerson());
            ps.setString(12, item.getTitle());
            ps.setString(13, item.getHp());
            ps.setString(14, item.getPhone());
            ps.setString(15, item.getFax());
            ps.setString(16, item.getEmail());
            ps.setString(17, item.getUrl());
            ps.setString(18, item.getBank());
            ps.setString(19, item.getBankaccount());
            ps.setString(20, item.getPayment());
            ps.setString(21, item.getRemarks());
            ps.setString(22, item.getPosition());
            ps.setString(23, item.getCoa());
            ps.setString(24, item.getCoadescp());
            ps.setString(25, item.getGstid());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
         
        
        try{
            String deleteQuery_2 = "delete from vendor_info where code = ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, no);
            ps_2.executeUpdate();
            ps_2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, Vendor item) throws Exception{
        
        
        try{
            String q = ("UPDATE vendor_info SET code = ?,  vendor_name = ?,  register_no = ?,  vendor_address = ?,  postcode = ?,  city = ?,  state = ?,  country = ?,  active = ?,  bumiputra = ?,  person = ?, title = ?,  hp = ?,  phone = ?,  fax = ?,  email = ?,  url = ?,  bank = ?,  bankaccount = ?,  payment = ?,  remarks = ?,  position = ?,  coa = ?,  coadescp = ?,  gstid = ? WHERE code = '"+item.getCode()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCode());
            ps.setString(2, item.getVendorName());
            ps.setString(3, item.getRegisterNo());
            ps.setString(4, item.getVendorAddress());
            ps.setString(5, item.getPostcode());
            ps.setString(6, item.getCity());
            ps.setString(7, item.getState());
            ps.setString(8, item.getCountry());
            ps.setString(9, item.getActive());
            ps.setString(10, item.getBumiputra());
            ps.setString(11, item.getPerson());
            ps.setString(12, item.getTitle());
            ps.setString(13, item.getHp());
            ps.setString(14, item.getPhone());
            ps.setString(15, item.getFax());
            ps.setString(16, item.getEmail());
            ps.setString(17, item.getUrl());
            ps.setString(18, item.getBank());
            ps.setString(19, item.getBankaccount());
            ps.setString(20, item.getPayment());
            ps.setString(21, item.getRemarks());
            ps.setString(22, item.getPosition());
            ps.setString(23, item.getCoa());
            ps.setString(24, item.getCoadescp());
            ps.setString(25, item.getGstid());
           Logger.getLogger(VendorDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static Vendor getInfo(LoginProfile log, String no) throws SQLException, Exception {
        Vendor c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM vendor_info WHERE code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static Vendor getInfo(ResultSet rs) throws SQLException {
        Vendor g = new Vendor();
        g.setActive(rs.getString("active"));
        g.setBank(rs.getString("bank"));
        g.setBankaccount(rs.getString("bankaccount"));
        g.setBankcode(rs.getString("bankcode"));
        g.setBankdesc(rs.getString("bankdesc"));
        g.setBumiputra(rs.getString("bumiputra"));
        g.setCity(rs.getString("city"));
        g.setCoa(rs.getString("coa"));
        g.setCoadescp(rs.getString("coadescp"));
        g.setCode(rs.getString("code"));
        g.setCountry(rs.getString("country"));
        g.setEmail(rs.getString("email"));
        g.setFax(rs.getString("fax"));
        g.setHp(rs.getString("hp"));
        g.setPayment(rs.getString("payment"));
        g.setPerson(rs.getString("person"));
        g.setPhone(rs.getString("phone"));
        g.setPosition(rs.getString("position"));
        g.setPostcode(rs.getString("postcode"));
        g.setRegisterNo(rs.getString("register_no"));
        g.setRemarks(rs.getString("remarks"));
        g.setState(rs.getString("state"));
        g.setTitle(rs.getString("title"));
        g.setUrl(rs.getString("url"));
        g.setVendorAddress(rs.getString("vendor_address"));
        g.setVendorName(rs.getString("vendor_name"));
        
       
        return g;
    }
    
    public static List<Vendor> searchEntity(String keyword) throws Exception {
        
        Connection con = ConnectionUtil.getHQConnection();
        
        Statement stmt = null;
        List<Vendor> Com;
        Com = new ArrayList();
        
        String q = "";
        String code = "";
        String table = "";
        String companyname = "";
        String register = "";
        String type = "";
        
        
        Logger.getLogger(VendorDAO.class.getName()).log(Level.INFO, "select "+code+" as code,"+companyname+" as vendor_name, address as vendor_address, postcode, city, state, gstid, "+register+" as register_no  from "+table+" "+q+" order by "+code+" limit 10");
        try {
            
            code = "code";
            table = "supplier_info";
            companyname = "companyname";
            register = "coregister";
            type = "Supplier";
            
            if(keyword!=null){
                q = "where "+code+" like '%"+keyword+"%' or companyname like '%"+keyword+"%'";
            }
            
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select "+code+" as code,"+companyname+" as vendor_name, address as vendor_address, postcode, city, state, gstid, "+register+" as register_no  from "+table+" "+q+" order by "+code+" limit 10");
Logger.getLogger(VendorDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getInfox(rs,type));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        try {
            
            code = "suppliercode";
            table = "contractor_info";
            companyname = "companyname";
            register = "register";
            type = "Contractor";
            
            if(keyword!=null){
                q = "where "+code+" like '%"+keyword+"%' or companyname like '%"+keyword+"%'";
            }
            
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select "+code+" as code,"+companyname+" as vendor_name, address as vendor_address, postcode, city, state, gstid, "+register+" as register_no  from "+table+" "+q+" order by "+code+" limit 10");
Logger.getLogger(VendorDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getInfox(rs,type));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        try {
            
            code = "code";
            table = "buyer_info";
            companyname = "companyname";
            register = "coregister";
            type = "Buyer";
            
            if(keyword!=null){
                q = "where "+code+" like '%"+keyword+"%' or companyname like '%"+keyword+"%'";
            }
            
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select "+code+" as code,"+companyname+" as vendor_name, address as vendor_address, postcode, city, state, gstid, "+register+" as register_no  from "+table+" "+q+" order by "+code+" limit 10");
Logger.getLogger(VendorDAO.class.getName()).log(Level.INFO, String.valueOf(rs));
            while (rs.next()) {
                
                Com.add(getInfox(rs,type));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Com;
    }
    
    private static Vendor getInfox(ResultSet rs, String type) throws SQLException {
        Vendor g = new Vendor();
        g.setCity(rs.getString("city"));
        g.setCode(rs.getString("code"));
        g.setPostcode(rs.getString("postcode"));
        g.setRegisterNo(rs.getString("register_no"));
        g.setState(rs.getString("state"));
        g.setVendorAddress(rs.getString("vendor_address"));
        g.setVendorName(rs.getString("vendor_name"));
        g.setType(type);
        
       
        return g;
    }
    
}
