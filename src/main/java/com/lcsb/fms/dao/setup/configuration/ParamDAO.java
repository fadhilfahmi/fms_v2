/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.configuration;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.configuration.Param;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ParamDAO {
     public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010123");
        mod.setModuleDesc("Parameter Information");
        mod.setMainTable("parameter");
        mod.setReferID_Master("id");
        return mod;
         
    }
    
     public static List<ListTable> tableList(){
        
        String column_names[] = {"id","parameter","value","dorder"};
        String title_name[] = {"id","Parameter","Value","Order"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
     
    public static void saveData(LoginProfile log, Param item) throws Exception{
        
        try {
            
            String q = ("insert into parameter(parameter, value, dorder) values (?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getParameter());
            ps.setString(2, item.getValue());
            ps.setInt(3, item.getDorder());
           
            
           ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
        
        try{
            String deleteQuery_2 = "delete from parameter where id = ?";
            try (PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2)) {
                ps_2.setString(1, no);
                ps_2.executeUpdate();
            }
        } catch (SQLException e) {
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, Param item) throws Exception{
        
        
        try{
            String q = ("UPDATE parameter SET parameter=? ,value=? , dorder=? WHERE id = '"+item.getId()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getParameter());
            ps.setString(2, item.getValue());
            ps.setInt(3, item.getDorder());
           // ps.setInt(4, item.getId());
           
            
            
            
           Logger.getLogger(ParamDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static Param getInfo(LoginProfile log, String no) throws SQLException, Exception {
        Param c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("SELECT parameter,value,dorder,id FROM parameter WHERE id=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static Param getInfo(ResultSet rs) throws SQLException {
        Param g = new Param();
        g.setParameter(rs.getString("parameter"));
        g.setValue(rs.getString("value"));
        g.setDorder(Integer.parseInt(rs.getString("dorder")));
        g.setId(Integer.parseInt(rs.getString("id")));
      
        
       
        return g;
    }

    public static void saveData(com.lcsb.fms.util.model.Parameter pm) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static void updateItem(com.lcsb.fms.util.model.Parameter pm) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}




