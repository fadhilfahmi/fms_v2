/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dao.setup.configuration;

import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.setup.configuration.ChartofAccount;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.ListTable;
import com.lcsb.fms.util.model.LoginProfile;
import com.lcsb.fms.util.model.Module;
import com.lcsb.fms.util.model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class ProductInfoDAO {
    
    public static Module getModule(){
        
        Module mod = new Module();
        mod.setModuleID("010133");
        mod.setModuleDesc("Product");
        mod.setMainTable("product_info");
        mod.setReferID_Master("code");
        return mod;
         
    }
    
    public static List<ListTable> tableList(){
        
        String column_names[] = {"code","descp","coa","coadescp","major","measure"};
        String title_name[] = {"Code","Description","Acc. Code","Acc. Description","Major","U. Measure"};
        
        List<ListTable> lt = new ArrayList();
        
        for(int j = 0;j<column_names.length;j++){
            ListTable i = new ListTable();
            i.setColumnName(column_names[j]);
            i.setTitleName(title_name[j]);
            lt.add(i);
        }
        
        return lt;
    }
    
    public static void saveData(LoginProfile log, Product item) throws Exception{
        
        try {
            String q = ("insert into product_info(coa,coadescp,code,descp,entercode,major,measure,spesifik) values (?,?,?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCoa());
            ps.setString(2, item.getCoadescp());
            ps.setString(3, AutoGenerate.get2digitNo(log, "product_info", "code"));
            ps.setString(4, item.getDescp());
            ps.setString(5, item.getEntercode());
            ps.setString(6, item.getMajor());
            ps.setString(7, item.getMeasure());
            ps.setString(8, item.getSpesifik());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
        
    }
    
    public static void deleteItem(LoginProfile log, String no) throws SQLException, Exception{
        
        try{
            String deleteQuery_2 = "delete from product_info where code = ?";
            PreparedStatement ps_2 = log.getCon().prepareStatement(deleteQuery_2);
            ps_2.setString(1, no);
            ps_2.executeUpdate();
            ps_2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static void updateItem(LoginProfile log, Product item) throws Exception{
        
        try{
            String q = ("UPDATE product_info set coa = ?,coadescp = ?,code = ?,descp = ?,entercode = ?,major = ?,measure = ?,spesifik = ? WHERE code = '"+item.getCode()+"'");
            
            PreparedStatement ps = log.getCon().prepareStatement(q);
            ps.setString(1, item.getCoa());
            ps.setString(2, item.getCoadescp());
            ps.setString(3, item.getCode());
            ps.setString(4, item.getDescp());
            ps.setString(5, item.getEntercode());
            ps.setString(6, item.getMajor());
            ps.setString(7, item.getMeasure());
            ps.setString(8, item.getSpesifik());
           
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        
    }
    
    public static Product getInfo(LoginProfile log, String no) throws SQLException, Exception {
        Product c = null;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select * from product_info where code=?");
        stmt.setString(1, no);
        rs = stmt.executeQuery();
        if (rs.next()) {
            c = getInfo(rs);
        }

        
        return c;
    }
    
    private static Product getInfo(ResultSet rs) throws SQLException {
        Product g = new Product();
        g.setCoa(rs.getString("coa"));
        g.setCoadescp(rs.getString("coadescp"));
        g.setCode(rs.getString("code"));
        g.setDescp(rs.getString("descp"));
        g.setEntercode(rs.getString("entercode"));
        g.setMajor(rs.getString("major"));
        g.setMeasure(rs.getString("measure"));
        g.setSpesifik(rs.getString("spesifik"));
        return g;
    }
    
   
    
}
