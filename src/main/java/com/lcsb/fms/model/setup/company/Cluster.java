/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.company;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "cluster")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cluster.findAll", query = "SELECT c FROM Cluster c"),
    @NamedQuery(name = "Cluster.findByCode", query = "SELECT c FROM Cluster c WHERE c.code = :code"),
    @NamedQuery(name = "Cluster.findById", query = "SELECT c FROM Cluster c WHERE c.id = :id"),
    @NamedQuery(name = "Cluster.findBySubcode", query = "SELECT c FROM Cluster c WHERE c.subcode = :subcode"),
    @NamedQuery(name = "Cluster.findBySub", query = "SELECT c FROM Cluster c WHERE c.sub = :sub"),
    @NamedQuery(name = "Cluster.findByLevel", query = "SELECT c FROM Cluster c WHERE c.level = :level"),
    @NamedQuery(name = "Cluster.findByCcode", query = "SELECT c FROM Cluster c WHERE c.ccode = :ccode"),
    @NamedQuery(name = "Cluster.findByCdescp", query = "SELECT c FROM Cluster c WHERE c.cdescp = :cdescp")})
public class Cluster implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "code")
    private String code;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "subcode")
    private String subcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "sub")
    private String sub;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "level")
    private String level;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ccode")
    private String ccode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cdescp")
    private String cdescp;

    public Cluster() {
    }

    public Cluster(Integer id) {
        this.id = id;
    }

    public Cluster(Integer id, String code, String subcode, String sub, String level, String ccode, String cdescp) {
        this.id = id;
        this.code = code;
        this.subcode = subcode;
        this.sub = sub;
        this.level = level;
        this.ccode = ccode;
        this.cdescp = cdescp;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubcode() {
        return subcode;
    }

    public void setSubcode(String subcode) {
        this.subcode = subcode;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getCcode() {
        return ccode;
    }

    public void setCcode(String ccode) {
        this.ccode = ccode;
    }

    public String getCdescp() {
        return cdescp;
    }

    public void setCdescp(String cdescp) {
        this.cdescp = cdescp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cluster)) {
            return false;
        }
        Cluster other = (Cluster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.company.Cluster[ id=" + id + " ]";
    }
    
}
