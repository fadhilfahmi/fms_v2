/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.company;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "mill_info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mill.findAll", query = "SELECT m FROM Mill m"),
    @NamedQuery(name = "Mill.findByCode", query = "SELECT m FROM Mill m WHERE m.code = :code"),
    @NamedQuery(name = "Mill.findByDescp", query = "SELECT m FROM Mill m WHERE m.descp = :descp"),
    @NamedQuery(name = "Mill.findByCoacode", query = "SELECT m FROM Mill m WHERE m.coacode = :coacode"),
    @NamedQuery(name = "Mill.findByCoadescp", query = "SELECT m FROM Mill m WHERE m.coadescp = :coadescp"),
    @NamedQuery(name = "Mill.findByActive", query = "SELECT m FROM Mill m WHERE m.active = :active"),
    @NamedQuery(name = "Mill.findByDefaultmill", query = "SELECT m FROM Mill m WHERE m.defaultmill = :defaultmill")})
public class Mill implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "code")
    private String code;
    @Size(max = 100)
    @Column(name = "descp")
    private String descp;
    @Size(max = 20)
    @Column(name = "coacode")
    private String coacode;
    @Size(max = 100)
    @Column(name = "coadescp")
    private String coadescp;
    @Size(max = 5)
    @Column(name = "active")
    private String active;
    @Size(max = 5)
    @Column(name = "defaultmill")
    private String defaultmill;

    public Mill() {
    }

    public Mill(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public String getCoacode() {
        return coacode;
    }

    public void setCoacode(String coacode) {
        this.coacode = coacode;
    }

    public String getCoadescp() {
        return coadescp;
    }

    public void setCoadescp(String coadescp) {
        this.coadescp = coadescp;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getDefaultmill() {
        return defaultmill;
    }

    public void setDefaultmill(String defaultmill) {
        this.defaultmill = defaultmill;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mill)) {
            return false;
        }
        Mill other = (Mill) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.company.Mill[ code=" + code + " ]";
    }
    
}
