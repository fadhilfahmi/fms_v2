/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.configuration;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "chartofjob")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ChartofJob.findAll", query = "SELECT c FROM ChartofJob c"),
    @NamedQuery(name = "ChartofJob.findByAccountcode", query = "SELECT c FROM ChartofJob c WHERE c.accountcode = :accountcode"),
    @NamedQuery(name = "ChartofJob.findByAccountdescp", query = "SELECT c FROM ChartofJob c WHERE c.accountdescp = :accountdescp"),
    @NamedQuery(name = "ChartofJob.findByRemarks", query = "SELECT c FROM ChartofJob c WHERE c.remarks = :remarks"),
    @NamedQuery(name = "ChartofJob.findByType4", query = "SELECT c FROM ChartofJob c WHERE c.type4 = :type4"),
    @NamedQuery(name = "ChartofJob.findByType3", query = "SELECT c FROM ChartofJob c WHERE c.type3 = :type3"),
    @NamedQuery(name = "ChartofJob.findByCode", query = "SELECT c FROM ChartofJob c WHERE c.code = :code"),
    @NamedQuery(name = "ChartofJob.findByDescp", query = "SELECT c FROM ChartofJob c WHERE c.descp = :descp"),
    @NamedQuery(name = "ChartofJob.findByType1", query = "SELECT c FROM ChartofJob c WHERE c.type1 = :type1"),
    @NamedQuery(name = "ChartofJob.findByType2", query = "SELECT c FROM ChartofJob c WHERE c.type2 = :type2"),
    @NamedQuery(name = "ChartofJob.findByFinallvl", query = "SELECT c FROM ChartofJob c WHERE c.finallvl = :finallvl"),
    @NamedQuery(name = "ChartofJob.findByActive", query = "SELECT c FROM ChartofJob c WHERE c.active = :active")})
public class ChartofJob implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "accountcode")
    private String accountcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "accountdescp")
    private String accountdescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "type4")
    private String type4;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "type3")
    private String type3;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "descp")
    private String descp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "type1")
    private String type1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "type2")
    private String type2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "finallvl")
    private String finallvl;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "active")
    private String active;

    public ChartofJob() {
    }

    public ChartofJob(String code) {
        this.code = code;
    }

    public ChartofJob(String code, String accountcode, String accountdescp, String remarks, String type4, String type3, String descp, String type1, String type2, String finallvl, String active) {
        this.code = code;
        this.accountcode = accountcode;
        this.accountdescp = accountdescp;
        this.remarks = remarks;
        this.type4 = type4;
        this.type3 = type3;
        this.descp = descp;
        this.type1 = type1;
        this.type2 = type2;
        this.finallvl = finallvl;
        this.active = active;
    }

    public String getAccountcode() {
        return accountcode;
    }

    public void setAccountcode(String accountcode) {
        this.accountcode = accountcode;
    }

    public String getAccountdescp() {
        return accountdescp;
    }

    public void setAccountdescp(String accountdescp) {
        this.accountdescp = accountdescp;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getType4() {
        return type4;
    }

    public void setType4(String type4) {
        this.type4 = type4;
    }

    public String getType3() {
        return type3;
    }

    public void setType3(String type3) {
        this.type3 = type3;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public String getType1() {
        return type1;
    }

    public void setType1(String type1) {
        this.type1 = type1;
    }

    public String getType2() {
        return type2;
    }

    public void setType2(String type2) {
        this.type2 = type2;
    }

    public String getFinallvl() {
        return finallvl;
    }

    public void setFinallvl(String finallvl) {
        this.finallvl = finallvl;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ChartofJob)) {
            return false;
        }
        ChartofJob other = (ChartofJob) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.configuration.ChartofJob[ code=" + code + " ]";
    }
    
}
