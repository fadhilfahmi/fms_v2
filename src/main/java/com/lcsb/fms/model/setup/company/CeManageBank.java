/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.company;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ce_manage_bank")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CeManageBank.findAll", query = "SELECT c FROM CeManageBank c"),
    @NamedQuery(name = "CeManageBank.findByCode", query = "SELECT c FROM CeManageBank c WHERE c.ceManageBankPK.code = :code"),
    @NamedQuery(name = "CeManageBank.findByName", query = "SELECT c FROM CeManageBank c WHERE c.name = :name"),
    @NamedQuery(name = "CeManageBank.findByBranchcode", query = "SELECT c FROM CeManageBank c WHERE c.ceManageBankPK.branchcode = :branchcode"),
    @NamedQuery(name = "CeManageBank.findByBranchname", query = "SELECT c FROM CeManageBank c WHERE c.branchname = :branchname"),
    @NamedQuery(name = "CeManageBank.findByMajor", query = "SELECT c FROM CeManageBank c WHERE c.major = :major"),
    @NamedQuery(name = "CeManageBank.findByRemarks", query = "SELECT c FROM CeManageBank c WHERE c.remarks = :remarks"),
    @NamedQuery(name = "CeManageBank.findByManagecode", query = "SELECT c FROM CeManageBank c WHERE c.ceManageBankPK.managecode = :managecode"),
    @NamedQuery(name = "CeManageBank.findByBankaccno", query = "SELECT c FROM CeManageBank c WHERE c.bankaccno = :bankaccno"),
    @NamedQuery(name = "CeManageBank.findByCoacode", query = "SELECT c FROM CeManageBank c WHERE c.coacode = :coacode"),
    @NamedQuery(name = "CeManageBank.findByCoadesc", query = "SELECT c FROM CeManageBank c WHERE c.coadesc = :coadesc"),
    @NamedQuery(name = "CeManageBank.findByFtype", query = "SELECT c FROM CeManageBank c WHERE c.ftype = :ftype"),
    @NamedQuery(name = "CeManageBank.findByActive", query = "SELECT c FROM CeManageBank c WHERE c.active = :active"),
    @NamedQuery(name = "CeManageBank.findByManagename", query = "SELECT c FROM CeManageBank c WHERE c.managename = :managename")})
public class CeManageBank implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CeManageBankPK ceManageBankPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "branchname")
    private String branchname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "major")
    private String major;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bankaccno")
    private String bankaccno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "coacode")
    private String coacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coadesc")
    private String coadesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ftype")
    private String ftype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "active")
    private String active;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "managename")
    private String managename;

    public CeManageBank() {
    }

    public CeManageBank(CeManageBankPK ceManageBankPK) {
        this.ceManageBankPK = ceManageBankPK;
    }

    public CeManageBank(CeManageBankPK ceManageBankPK, String name, String branchname, String major, String remarks, String bankaccno, String coacode, String coadesc, String ftype, String active, String managename) {
        this.ceManageBankPK = ceManageBankPK;
        this.name = name;
        this.branchname = branchname;
        this.major = major;
        this.remarks = remarks;
        this.bankaccno = bankaccno;
        this.coacode = coacode;
        this.coadesc = coadesc;
        this.ftype = ftype;
        this.active = active;
        this.managename = managename;
    }

    public CeManageBank(String code, String branchcode, String managecode) {
        this.ceManageBankPK = new CeManageBankPK(code, branchcode, managecode);
    }

    public CeManageBankPK getCeManageBankPK() {
        return ceManageBankPK;
    }

    public void setCeManageBankPK(CeManageBankPK ceManageBankPK) {
        this.ceManageBankPK = ceManageBankPK;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBranchname() {
        return branchname;
    }

    public void setBranchname(String branchname) {
        this.branchname = branchname;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getBankaccno() {
        return bankaccno;
    }

    public void setBankaccno(String bankaccno) {
        this.bankaccno = bankaccno;
    }

    public String getCoacode() {
        return coacode;
    }

    public void setCoacode(String coacode) {
        this.coacode = coacode;
    }

    public String getCoadesc() {
        return coadesc;
    }

    public void setCoadesc(String coadesc) {
        this.coadesc = coadesc;
    }

    public String getFtype() {
        return ftype;
    }

    public void setFtype(String ftype) {
        this.ftype = ftype;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getManagename() {
        return managename;
    }

    public void setManagename(String managename) {
        this.managename = managename;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ceManageBankPK != null ? ceManageBankPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CeManageBank)) {
            return false;
        }
        CeManageBank other = (CeManageBank) object;
        if ((this.ceManageBankPK == null && other.ceManageBankPK != null) || (this.ceManageBankPK != null && !this.ceManageBankPK.equals(other.ceManageBankPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.company.CeManageBank[ ceManageBankPK=" + ceManageBankPK + " ]";
    }
    
}
