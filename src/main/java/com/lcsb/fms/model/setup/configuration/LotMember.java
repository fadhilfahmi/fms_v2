/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.configuration;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "lot_memberinfo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LotMember.findAll", query = "SELECT l FROM LotMember l"),
    @NamedQuery(name = "LotMember.findByMemCode", query = "SELECT l FROM LotMember l WHERE l.memCode = :memCode"),
    @NamedQuery(name = "LotMember.findByTitle", query = "SELECT l FROM LotMember l WHERE l.title = :title"),
    @NamedQuery(name = "LotMember.findByUsetitle", query = "SELECT l FROM LotMember l WHERE l.usetitle = :usetitle"),
    @NamedQuery(name = "LotMember.findByMemName", query = "SELECT l FROM LotMember l WHERE l.memName = :memName"),
    @NamedQuery(name = "LotMember.findByIc", query = "SELECT l FROM LotMember l WHERE l.ic = :ic"),
    @NamedQuery(name = "LotMember.findByAddress", query = "SELECT l FROM LotMember l WHERE l.address = :address"),
    @NamedQuery(name = "LotMember.findByPostcode", query = "SELECT l FROM LotMember l WHERE l.postcode = :postcode"),
    @NamedQuery(name = "LotMember.findByCity", query = "SELECT l FROM LotMember l WHERE l.city = :city"),
    @NamedQuery(name = "LotMember.findByState", query = "SELECT l FROM LotMember l WHERE l.state = :state"),
    @NamedQuery(name = "LotMember.findByBank", query = "SELECT l FROM LotMember l WHERE l.bank = :bank"),
    @NamedQuery(name = "LotMember.findByAcc", query = "SELECT l FROM LotMember l WHERE l.acc = :acc"),
    @NamedQuery(name = "LotMember.findByPaymethod", query = "SELECT l FROM LotMember l WHERE l.paymethod = :paymethod"),
    @NamedQuery(name = "LotMember.findByStatus", query = "SELECT l FROM LotMember l WHERE l.status = :status"),
    @NamedQuery(name = "LotMember.findByNotel", query = "SELECT l FROM LotMember l WHERE l.notel = :notel"),
    @NamedQuery(name = "LotMember.findByDateInactive", query = "SELECT l FROM LotMember l WHERE l.dateInactive = :dateInactive"),
    @NamedQuery(name = "LotMember.findByPreOwner", query = "SELECT l FROM LotMember l WHERE l.preOwner = :preOwner"),
    @NamedQuery(name = "LotMember.findByRelation", query = "SELECT l FROM LotMember l WHERE l.relation = :relation"),
    @NamedQuery(name = "LotMember.findByDateActive", query = "SELECT l FROM LotMember l WHERE l.dateActive = :dateActive"),
    @NamedQuery(name = "LotMember.findByNewOwner", query = "SELECT l FROM LotMember l WHERE l.newOwner = :newOwner"),
    @NamedQuery(name = "LotMember.findByRemark", query = "SELECT l FROM LotMember l WHERE l.remark = :remark"),
    @NamedQuery(name = "LotMember.findByAddress1", query = "SELECT l FROM LotMember l WHERE l.address1 = :address1"),
    @NamedQuery(name = "LotMember.findByAddress2", query = "SELECT l FROM LotMember l WHERE l.address2 = :address2"),
    @NamedQuery(name = "LotMember.findByAddress3", query = "SELECT l FROM LotMember l WHERE l.address3 = :address3"),
    @NamedQuery(name = "LotMember.findByAddress4", query = "SELECT l FROM LotMember l WHERE l.address4 = :address4"),
    @NamedQuery(name = "LotMember.findByAddress5", query = "SELECT l FROM LotMember l WHERE l.address5 = :address5")})
public class LotMember implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "mem_Code")
    private String memCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "title")
    private String title;
    @Size(max = 10)
    @Column(name = "usetitle")
    private String usetitle;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "mem_Name")
    private String memName;
    @Size(max = 100)
    @Column(name = "ic")
    private String ic;
    @Size(max = 300)
    @Column(name = "address")
    private String address;
    @Size(max = 100)
    @Column(name = "postcode")
    private String postcode;
    @Size(max = 100)
    @Column(name = "city")
    private String city;
    @Size(max = 100)
    @Column(name = "state")
    private String state;
    @Size(max = 100)
    @Column(name = "bank")
    private String bank;
    @Size(max = 100)
    @Column(name = "acc")
    private String acc;
    @Size(max = 100)
    @Column(name = "paymethod")
    private String paymethod;
    @Size(max = 100)
    @Column(name = "status")
    private String status;
    @Size(max = 300)
    @Column(name = "notel")
    private String notel;
    @Size(max = 10)
    @Column(name = "date_inactive")
    private String dateInactive;
    @Size(max = 250)
    @Column(name = "pre_owner")
    private String preOwner;
    @Size(max = 100)
    @Column(name = "relation")
    private String relation;
    @Size(max = 10)
    @Column(name = "date_active")
    private String dateActive;
    @Size(max = 100)
    @Column(name = "new_owner")
    private String newOwner;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "remark")
    private String remark;
    @Size(max = 200)
    @Column(name = "address1")
    private String address1;
    @Size(max = 200)
    @Column(name = "address2")
    private String address2;
    @Size(max = 200)
    @Column(name = "address3")
    private String address3;
    @Size(max = 200)
    @Column(name = "address4")
    private String address4;
    @Size(max = 200)
    @Column(name = "address5")
    private String address5;

    public LotMember() {
    }

    public LotMember(String memCode) {
        this.memCode = memCode;
    }

    public LotMember(String memCode, String title, String memName, String remark) {
        this.memCode = memCode;
        this.title = title;
        this.memName = memName;
        this.remark = remark;
    }

    public String getMemCode() {
        return memCode;
    }

    public void setMemCode(String memCode) {
        this.memCode = memCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUsetitle() {
        return usetitle;
    }

    public void setUsetitle(String usetitle) {
        this.usetitle = usetitle;
    }

    public String getMemName() {
        return memName;
    }

    public void setMemName(String memName) {
        this.memName = memName;
    }

    public String getIc() {
        return ic;
    }

    public void setIc(String ic) {
        this.ic = ic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getAcc() {
        return acc;
    }

    public void setAcc(String acc) {
        this.acc = acc;
    }

    public String getPaymethod() {
        return paymethod;
    }

    public void setPaymethod(String paymethod) {
        this.paymethod = paymethod;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNotel() {
        return notel;
    }

    public void setNotel(String notel) {
        this.notel = notel;
    }

    public String getDateInactive() {
        return dateInactive;
    }

    public void setDateInactive(String dateInactive) {
        this.dateInactive = dateInactive;
    }

    public String getPreOwner() {
        return preOwner;
    }

    public void setPreOwner(String preOwner) {
        this.preOwner = preOwner;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getDateActive() {
        return dateActive;
    }

    public void setDateActive(String dateActive) {
        this.dateActive = dateActive;
    }

    public String getNewOwner() {
        return newOwner;
    }

    public void setNewOwner(String newOwner) {
        this.newOwner = newOwner;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getAddress4() {
        return address4;
    }

    public void setAddress4(String address4) {
        this.address4 = address4;
    }

    public String getAddress5() {
        return address5;
    }

    public void setAddress5(String address5) {
        this.address5 = address5;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (memCode != null ? memCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LotMember)) {
            return false;
        }
        LotMember other = (LotMember) object;
        if ((this.memCode == null && other.memCode != null) || (this.memCode != null && !this.memCode.equals(other.memCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.configuration.LotMember[ memCode=" + memCode + " ]";
    }
    
}
