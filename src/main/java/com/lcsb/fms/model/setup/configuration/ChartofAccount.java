/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.model.setup.configuration;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dell
 */
@Entity
@Table(name = "chartofacccount")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ChartofAccount.findAll", query = "SELECT c FROM ChartofAccount c"),
    @NamedQuery(name = "ChartofAccount.findByCode", query = "SELECT c FROM ChartofAccount c WHERE c.code = :code"),
    @NamedQuery(name = "ChartofAccount.findByDescp", query = "SELECT c FROM ChartofAccount c WHERE c.descp = :descp"),
    @NamedQuery(name = "ChartofAccount.findByType", query = "SELECT c FROM ChartofAccount c WHERE c.type = :type"),
    @NamedQuery(name = "ChartofAccount.findByApptype", query = "SELECT c FROM ChartofAccount c WHERE c.apptype = :apptype"),
    @NamedQuery(name = "ChartofAccount.findByApplevel", query = "SELECT c FROM ChartofAccount c WHERE c.applevel = :applevel"),
    @NamedQuery(name = "ChartofAccount.findByReport", query = "SELECT c FROM ChartofAccount c WHERE c.report = :report"),
    @NamedQuery(name = "ChartofAccount.findByAttype", query = "SELECT c FROM ChartofAccount c WHERE c.attype = :attype"),
    @NamedQuery(name = "ChartofAccount.findByFinallvl", query = "SELECT c FROM ChartofAccount c WHERE c.finallvl = :finallvl"),
    @NamedQuery(name = "ChartofAccount.findByActive", query = "SELECT c FROM ChartofAccount c WHERE c.active = :active"),
    @NamedQuery(name = "ChartofAccount.findByBalancesheet", query = "SELECT c FROM ChartofAccount c WHERE c.balancesheet = :balancesheet")})
public class ChartofAccount implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @Column(name = "descp")
    private String descp;
    @Basic(optional = false)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @Column(name = "apptype")
    private String apptype;
    @Basic(optional = false)
    @Column(name = "applevel")
    private String applevel;
    @Basic(optional = false)
    @Column(name = "report")
    private String report;
    @Basic(optional = false)
    @Column(name = "attype")
    private String attype;
    @Basic(optional = false)
    @Column(name = "finallvl")
    private String finallvl;
    @Basic(optional = false)
    @Column(name = "active")
    private String active;
    @Basic(optional = false)
    @Column(name = "balancesheet")
    private String balancesheet;

    public ChartofAccount() {
    }

    public ChartofAccount(String code) {
        this.code = code;
    }

    public ChartofAccount(String code, String descp, String type, String apptype, String applevel, String report, String attype, String finallvl, String active, String balancesheet) {
        this.code = code;
        this.descp = descp;
        this.type = type;
        this.apptype = apptype;
        this.applevel = applevel;
        this.report = report;
        this.attype = attype;
        this.finallvl = finallvl;
        this.active = active;
        this.balancesheet = balancesheet;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getApptype() {
        return apptype;
    }

    public void setApptype(String apptype) {
        this.apptype = apptype;
    }

    public String getApplevel() {
        return applevel;
    }

    public void setApplevel(String applevel) {
        this.applevel = applevel;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String getAttype() {
        return attype;
    }

    public void setAttype(String attype) {
        this.attype = attype;
    }

    public String getFinallvl() {
        return finallvl;
    }

    public void setFinallvl(String finallvl) {
        this.finallvl = finallvl;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getBalancesheet() {
        return balancesheet;
    }

    public void setBalancesheet(String balancesheet) {
        this.balancesheet = balancesheet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ChartofAccount)) {
            return false;
        }
        ChartofAccount other = (ChartofAccount) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fms.model.setup.configuration.ChartofAccount[ code=" + code + " ]";
    }
    
}
