/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.company.staff;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "co_staff_child")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StaffChild.findAll", query = "SELECT s FROM StaffChild s"),
    @NamedQuery(name = "StaffChild.findByName", query = "SELECT s FROM StaffChild s WHERE s.name = :name"),
    @NamedQuery(name = "StaffChild.findByNo", query = "SELECT s FROM StaffChild s WHERE s.no = :no"),
    @NamedQuery(name = "StaffChild.findByMotherid", query = "SELECT s FROM StaffChild s WHERE s.motherid = :motherid"),
    @NamedQuery(name = "StaffChild.findByIc", query = "SELECT s FROM StaffChild s WHERE s.ic = :ic"),
    @NamedQuery(name = "StaffChild.findByBirthno", query = "SELECT s FROM StaffChild s WHERE s.birthno = :birthno"),
    @NamedQuery(name = "StaffChild.findByDob", query = "SELECT s FROM StaffChild s WHERE s.dob = :dob"),
    @NamedQuery(name = "StaffChild.findByGender", query = "SELECT s FROM StaffChild s WHERE s.gender = :gender"),
    @NamedQuery(name = "StaffChild.findByRace", query = "SELECT s FROM StaffChild s WHERE s.race = :race"),
    @NamedQuery(name = "StaffChild.findByReligion", query = "SELECT s FROM StaffChild s WHERE s.religion = :religion"),
    @NamedQuery(name = "StaffChild.findByCitizen", query = "SELECT s FROM StaffChild s WHERE s.citizen = :citizen"),
    @NamedQuery(name = "StaffChild.findByAlive", query = "SELECT s FROM StaffChild s WHERE s.alive = :alive"),
    @NamedQuery(name = "StaffChild.findByStaffid", query = "SELECT s FROM StaffChild s WHERE s.staffid = :staffid"),
    @NamedQuery(name = "StaffChild.findById", query = "SELECT s FROM StaffChild s WHERE s.id = :id")})
public class StaffChild implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "no")
    private String no;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "motherid")
    private String motherid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ic")
    private String ic;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "birthno")
    private String birthno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "dob")
    private String dob;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "gender")
    private String gender;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "race")
    private String race;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "religion")
    private String religion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "citizen")
    private String citizen;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "alive")
    private String alive;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "staffid")
    private String staffid;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    private boolean taxdependent;

    public StaffChild() {
    }

    public StaffChild(Integer id) {
        this.id = id;
    }

    public StaffChild(Integer id, String name, String no, String motherid, String ic, String birthno, String dob, String gender, String race, String religion, String citizen, String alive, String staffid, boolean taxdependent) {
        this.id = id;
        this.name = name;
        this.no = no;
        this.motherid = motherid;
        this.ic = ic;
        this.birthno = birthno;
        this.dob = dob;
        this.gender = gender;
        this.race = race;
        this.religion = religion;
        this.citizen = citizen;
        this.alive = alive;
        this.staffid = staffid;
        this.taxdependent = taxdependent;
    }

    public boolean isTaxdependent() {
        return taxdependent;
    }

    public void setTaxdependent(boolean taxdependent) {
        this.taxdependent = taxdependent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getMotherid() {
        return motherid;
    }

    public void setMotherid(String motherid) {
        this.motherid = motherid;
    }

    public String getIc() {
        return ic;
    }

    public void setIc(String ic) {
        this.ic = ic;
    }

    public String getBirthno() {
        return birthno;
    }

    public void setBirthno(String birthno) {
        this.birthno = birthno;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getCitizen() {
        return citizen;
    }

    public void setCitizen(String citizen) {
        this.citizen = citizen;
    }

    public String getAlive() {
        return alive;
    }

    public void setAlive(String alive) {
        this.alive = alive;
    }

    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StaffChild)) {
            return false;
        }
        StaffChild other = (StaffChild) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.company.staff.StaffChild[ id=" + id + " ]";
    }
    
}
