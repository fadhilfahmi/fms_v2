/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.configuration;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "conf_accrual_info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Accrual.findAll", query = "SELECT a FROM Accrual a"),
    @NamedQuery(name = "Accrual.findByCode", query = "SELECT a FROM Accrual a WHERE a.code = :code"),
    @NamedQuery(name = "Accrual.findByCompanyname", query = "SELECT a FROM Accrual a WHERE a.companyname = :companyname"),
    @NamedQuery(name = "Accrual.findByCoregister", query = "SELECT a FROM Accrual a WHERE a.coregister = :coregister"),
    @NamedQuery(name = "Accrual.findByBumiputra", query = "SELECT a FROM Accrual a WHERE a.bumiputra = :bumiputra"),
    @NamedQuery(name = "Accrual.findByCoa", query = "SELECT a FROM Accrual a WHERE a.coa = :coa"),
    @NamedQuery(name = "Accrual.findByCoadescp", query = "SELECT a FROM Accrual a WHERE a.coadescp = :coadescp"),
    @NamedQuery(name = "Accrual.findByPerson", query = "SELECT a FROM Accrual a WHERE a.person = :person"),
    @NamedQuery(name = "Accrual.findByTitle", query = "SELECT a FROM Accrual a WHERE a.title = :title"),
    @NamedQuery(name = "Accrual.findByPosition", query = "SELECT a FROM Accrual a WHERE a.position = :position"),
    @NamedQuery(name = "Accrual.findByHp", query = "SELECT a FROM Accrual a WHERE a.hp = :hp"),
    @NamedQuery(name = "Accrual.findByAddress", query = "SELECT a FROM Accrual a WHERE a.address = :address"),
    @NamedQuery(name = "Accrual.findByCity", query = "SELECT a FROM Accrual a WHERE a.city = :city"),
    @NamedQuery(name = "Accrual.findByState", query = "SELECT a FROM Accrual a WHERE a.state = :state"),
    @NamedQuery(name = "Accrual.findByPostcode", query = "SELECT a FROM Accrual a WHERE a.postcode = :postcode"),
    @NamedQuery(name = "Accrual.findByCountry", query = "SELECT a FROM Accrual a WHERE a.country = :country"),
    @NamedQuery(name = "Accrual.findByPhone", query = "SELECT a FROM Accrual a WHERE a.phone = :phone"),
    @NamedQuery(name = "Accrual.findByFax", query = "SELECT a FROM Accrual a WHERE a.fax = :fax"),
    @NamedQuery(name = "Accrual.findByEmail", query = "SELECT a FROM Accrual a WHERE a.email = :email"),
    @NamedQuery(name = "Accrual.findByUrl", query = "SELECT a FROM Accrual a WHERE a.url = :url"),
    @NamedQuery(name = "Accrual.findByBank", query = "SELECT a FROM Accrual a WHERE a.bank = :bank"),
    @NamedQuery(name = "Accrual.findByBankaccount", query = "SELECT a FROM Accrual a WHERE a.bankaccount = :bankaccount"),
    @NamedQuery(name = "Accrual.findByPayment", query = "SELECT a FROM Accrual a WHERE a.payment = :payment"),
    @NamedQuery(name = "Accrual.findByRemarks", query = "SELECT a FROM Accrual a WHERE a.remarks = :remarks"),
    @NamedQuery(name = "Accrual.findByBankdesc", query = "SELECT a FROM Accrual a WHERE a.bankdesc = :bankdesc"),
    @NamedQuery(name = "Accrual.findByDepositcode", query = "SELECT a FROM Accrual a WHERE a.depositcode = :depositcode"),
    @NamedQuery(name = "Accrual.findByDepositdesc", query = "SELECT a FROM Accrual a WHERE a.depositdesc = :depositdesc"),
    @NamedQuery(name = "Accrual.findBySuspensecode", query = "SELECT a FROM Accrual a WHERE a.suspensecode = :suspensecode"),
    @NamedQuery(name = "Accrual.findBySuspensedesc", query = "SELECT a FROM Accrual a WHERE a.suspensedesc = :suspensedesc")})
public class Accrual implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "companyname")
    private String companyname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coregister")
    private String coregister;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bumiputra")
    private String bumiputra;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coa")
    private String coa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coadescp")
    private String coadescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "person")
    private String person;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "position")
    private String position;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "hp")
    private String hp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "state")
    private String state;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postcode")
    private String postcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "country")
    private String country;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "phone")
    private String phone;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "fax")
    private String fax;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "url")
    private String url;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bank")
    private String bank;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bankaccount")
    private String bankaccount;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "payment")
    private String payment;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bankdesc")
    private String bankdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "depositcode")
    private String depositcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "depositdesc")
    private String depositdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "suspensecode")
    private String suspensecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "suspensedesc")
    private String suspensedesc;

    public Accrual() {
    }

    public Accrual(String code) {
        this.code = code;
    }

    public Accrual(String code, String companyname, String coregister, String bumiputra, String coa, String coadescp, String person, String title, String position, String hp, String address, String city, String state, String postcode, String country, String phone, String fax, String email, String url, String bank, String bankaccount, String payment, String remarks, String bankdesc, String depositcode, String depositdesc, String suspensecode, String suspensedesc) {
        this.code = code;
        this.companyname = companyname;
        this.coregister = coregister;
        this.bumiputra = bumiputra;
        this.coa = coa;
        this.coadescp = coadescp;
        this.person = person;
        this.title = title;
        this.position = position;
        this.hp = hp;
        this.address = address;
        this.city = city;
        this.state = state;
        this.postcode = postcode;
        this.country = country;
        this.phone = phone;
        this.fax = fax;
        this.email = email;
        this.url = url;
        this.bank = bank;
        this.bankaccount = bankaccount;
        this.payment = payment;
        this.remarks = remarks;
        this.bankdesc = bankdesc;
        this.depositcode = depositcode;
        this.depositdesc = depositdesc;
        this.suspensecode = suspensecode;
        this.suspensedesc = suspensedesc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getCoregister() {
        return coregister;
    }

    public void setCoregister(String coregister) {
        this.coregister = coregister;
    }

    public String getBumiputra() {
        return bumiputra;
    }

    public void setBumiputra(String bumiputra) {
        this.bumiputra = bumiputra;
    }

    public String getCoa() {
        return coa;
    }

    public void setCoa(String coa) {
        this.coa = coa;
    }

    public String getCoadescp() {
        return coadescp;
    }

    public void setCoadescp(String coadescp) {
        this.coadescp = coadescp;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankaccount() {
        return bankaccount;
    }

    public void setBankaccount(String bankaccount) {
        this.bankaccount = bankaccount;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getBankdesc() {
        return bankdesc;
    }

    public void setBankdesc(String bankdesc) {
        this.bankdesc = bankdesc;
    }

    public String getDepositcode() {
        return depositcode;
    }

    public void setDepositcode(String depositcode) {
        this.depositcode = depositcode;
    }

    public String getDepositdesc() {
        return depositdesc;
    }

    public void setDepositdesc(String depositdesc) {
        this.depositdesc = depositdesc;
    }

    public String getSuspensecode() {
        return suspensecode;
    }

    public void setSuspensecode(String suspensecode) {
        this.suspensecode = suspensecode;
    }

    public String getSuspensedesc() {
        return suspensedesc;
    }

    public void setSuspensedesc(String suspensedesc) {
        this.suspensedesc = suspensedesc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Accrual)) {
            return false;
        }
        Accrual other = (Accrual) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.configuration.Accrual[ code=" + code + " ]";
    }
    
}
