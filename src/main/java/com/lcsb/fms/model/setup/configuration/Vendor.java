/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.configuration;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "vendor_info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vendor.findAll", query = "SELECT v FROM Vendor v"),
    @NamedQuery(name = "Vendor.findByCode", query = "SELECT v FROM Vendor v WHERE v.code = :code"),
    @NamedQuery(name = "Vendor.findByVendorName", query = "SELECT v FROM Vendor v WHERE v.vendorName = :vendorName"),
    @NamedQuery(name = "Vendor.findByRegisterNo", query = "SELECT v FROM Vendor v WHERE v.registerNo = :registerNo"),
    @NamedQuery(name = "Vendor.findByVendorAddress", query = "SELECT v FROM Vendor v WHERE v.vendorAddress = :vendorAddress"),
    @NamedQuery(name = "Vendor.findByPostcode", query = "SELECT v FROM Vendor v WHERE v.postcode = :postcode"),
    @NamedQuery(name = "Vendor.findByCity", query = "SELECT v FROM Vendor v WHERE v.city = :city"),
    @NamedQuery(name = "Vendor.findByState", query = "SELECT v FROM Vendor v WHERE v.state = :state"),
    @NamedQuery(name = "Vendor.findByCountry", query = "SELECT v FROM Vendor v WHERE v.country = :country"),
    @NamedQuery(name = "Vendor.findByActive", query = "SELECT v FROM Vendor v WHERE v.active = :active"),
    @NamedQuery(name = "Vendor.findByBumiputra", query = "SELECT v FROM Vendor v WHERE v.bumiputra = :bumiputra"),
    @NamedQuery(name = "Vendor.findByPerson", query = "SELECT v FROM Vendor v WHERE v.person = :person"),
    @NamedQuery(name = "Vendor.findByTitle", query = "SELECT v FROM Vendor v WHERE v.title = :title"),
    @NamedQuery(name = "Vendor.findByHp", query = "SELECT v FROM Vendor v WHERE v.hp = :hp"),
    @NamedQuery(name = "Vendor.findByPhone", query = "SELECT v FROM Vendor v WHERE v.phone = :phone"),
    @NamedQuery(name = "Vendor.findByFax", query = "SELECT v FROM Vendor v WHERE v.fax = :fax"),
    @NamedQuery(name = "Vendor.findByEmail", query = "SELECT v FROM Vendor v WHERE v.email = :email"),
    @NamedQuery(name = "Vendor.findByUrl", query = "SELECT v FROM Vendor v WHERE v.url = :url"),
    @NamedQuery(name = "Vendor.findByBank", query = "SELECT v FROM Vendor v WHERE v.bank = :bank"),
    @NamedQuery(name = "Vendor.findByBankaccount", query = "SELECT v FROM Vendor v WHERE v.bankaccount = :bankaccount"),
    @NamedQuery(name = "Vendor.findByPayment", query = "SELECT v FROM Vendor v WHERE v.payment = :payment"),
    @NamedQuery(name = "Vendor.findByRemarks", query = "SELECT v FROM Vendor v WHERE v.remarks = :remarks"),
    @NamedQuery(name = "Vendor.findByBankdesc", query = "SELECT v FROM Vendor v WHERE v.bankdesc = :bankdesc"),
    @NamedQuery(name = "Vendor.findByBankcode", query = "SELECT v FROM Vendor v WHERE v.bankcode = :bankcode"),
    @NamedQuery(name = "Vendor.findByPosition", query = "SELECT v FROM Vendor v WHERE v.position = :position"),
    @NamedQuery(name = "Vendor.findByCoa", query = "SELECT v FROM Vendor v WHERE v.coa = :coa"),
    @NamedQuery(name = "Vendor.findByCoadescp", query = "SELECT v FROM Vendor v WHERE v.coadescp = :coadescp"),
    @NamedQuery(name = "Vendor.findByGstid", query = "SELECT v FROM Vendor v WHERE v.gstid = :gstid")})
public class Vendor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "vendor_name")
    private String vendorName;
    @Size(max = 50)
    @Column(name = "register_no")
    private String registerNo;
    @Size(max = 300)
    @Column(name = "vendor_address")
    private String vendorAddress;
    @Size(max = 10)
    @Column(name = "postcode")
    private String postcode;
    @Size(max = 50)
    @Column(name = "city")
    private String city;
    @Size(max = 50)
    @Column(name = "state")
    private String state;
    @Size(max = 50)
    @Column(name = "country")
    private String country;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "active")
    private String active;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bumiputra")
    private String bumiputra;
    @Size(max = 100)
    @Column(name = "person")
    private String person;
    @Size(max = 100)
    @Column(name = "title")
    private String title;
    @Size(max = 100)
    @Column(name = "hp")
    private String hp;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "phone")
    private String phone;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "fax")
    private String fax;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "email")
    private String email;
    @Size(max = 100)
    @Column(name = "url")
    private String url;
    @Size(max = 100)
    @Column(name = "bank")
    private String bank;
    @Size(max = 100)
    @Column(name = "bankaccount")
    private String bankaccount;
    @Size(max = 100)
    @Column(name = "payment")
    private String payment;
    @Size(max = 100)
    @Column(name = "remarks")
    private String remarks;
    @Size(max = 100)
    @Column(name = "bankdesc")
    private String bankdesc;
    @Size(max = 100)
    @Column(name = "bankcode")
    private String bankcode;
    @Size(max = 100)
    @Column(name = "position")
    private String position;
    @Size(max = 100)
    @Column(name = "coa")
    private String coa;
    @Size(max = 100)
    @Column(name = "coadescp")
    private String coadescp;
    @Size(max = 100)
    @Column(name = "gstid")
    private String gstid;
    private String type;

    public Vendor() {
    }

    public Vendor(String code) {
        this.code = code;
    }

    public Vendor(String code, String vendorName, String active, String bumiputra) {
        this.code = code;
        this.vendorName = vendorName;
        this.active = active;
        this.bumiputra = bumiputra;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getRegisterNo() {
        return registerNo;
    }

    public void setRegisterNo(String registerNo) {
        this.registerNo = registerNo;
    }

    public String getVendorAddress() {
        return vendorAddress;
    }

    public void setVendorAddress(String vendorAddress) {
        this.vendorAddress = vendorAddress;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getBumiputra() {
        return bumiputra;
    }

    public void setBumiputra(String bumiputra) {
        this.bumiputra = bumiputra;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankaccount() {
        return bankaccount;
    }

    public void setBankaccount(String bankaccount) {
        this.bankaccount = bankaccount;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getBankdesc() {
        return bankdesc;
    }

    public void setBankdesc(String bankdesc) {
        this.bankdesc = bankdesc;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCoa() {
        return coa;
    }

    public void setCoa(String coa) {
        this.coa = coa;
    }

    public String getCoadescp() {
        return coadescp;
    }

    public void setCoadescp(String coadescp) {
        this.coadescp = coadescp;
    }

    public String getGstid() {
        return gstid;
    }

    public void setGstid(String gstid) {
        this.gstid = gstid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vendor)) {
            return false;
        }
        Vendor other = (Vendor) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.configuration.Vendor[ code=" + code + " ]";
    }
    
}
