/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.configuration;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "lot_memberlot")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LotMemberLot.findAll", query = "SELECT l FROM LotMemberLot l"),
    @NamedQuery(name = "LotMemberLot.findById", query = "SELECT l FROM LotMemberLot l WHERE l.id = :id"),
    @NamedQuery(name = "LotMemberLot.findByMemCode", query = "SELECT l FROM LotMemberLot l WHERE l.memCode = :memCode"),
    @NamedQuery(name = "LotMemberLot.findByLotCode", query = "SELECT l FROM LotMemberLot l WHERE l.lotCode = :lotCode"),
    @NamedQuery(name = "LotMemberLot.findByLotDescp", query = "SELECT l FROM LotMemberLot l WHERE l.lotDescp = :lotDescp"),
    @NamedQuery(name = "LotMemberLot.findByHectarage", query = "SELECT l FROM LotMemberLot l WHERE l.hectarage = :hectarage"),
    @NamedQuery(name = "LotMemberLot.findByAcre", query = "SELECT l FROM LotMemberLot l WHERE l.acre = :acre"),
    @NamedQuery(name = "LotMemberLot.findByHsdno", query = "SELECT l FROM LotMemberLot l WHERE l.hsdno = :hsdno"),
    @NamedQuery(name = "LotMemberLot.findByNopetak", query = "SELECT l FROM LotMemberLot l WHERE l.nopetak = :nopetak")})
public class LotMemberLot implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "mem_Code")
    private String memCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "lot_Code")
    private String lotCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "lot_Descp")
    private String lotDescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "hectarage")
    private String hectarage;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "acre")
    private String acre;
    @Size(max = 100)
    @Column(name = "hsdno")
    private String hsdno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "nopetak")
    private String nopetak;
    private String memName;

    public LotMemberLot() {
    }

    public LotMemberLot(Integer id) {
        this.id = id;
    }

    public LotMemberLot(Integer id, String memCode, String lotCode, String lotDescp, String hectarage, String acre, String nopetak, String memName) {
        this.id = id;
        this.memCode = memCode;
        this.lotCode = lotCode;
        this.lotDescp = lotDescp;
        this.hectarage = hectarage;
        this.acre = acre;
        this.nopetak = nopetak;
        this.memName = memName;
    }

    public String getMemName() {
        return memName;
    }

    public void setMemName(String memName) {
        this.memName = memName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMemCode() {
        return memCode;
    }

    public void setMemCode(String memCode) {
        this.memCode = memCode;
    }

    public String getLotCode() {
        return lotCode;
    }

    public void setLotCode(String lotCode) {
        this.lotCode = lotCode;
    }

    public String getLotDescp() {
        return lotDescp;
    }

    public void setLotDescp(String lotDescp) {
        this.lotDescp = lotDescp;
    }

    public String getHectarage() {
        return hectarage;
    }

    public void setHectarage(String hectarage) {
        this.hectarage = hectarage;
    }

    public String getAcre() {
        return acre;
    }

    public void setAcre(String acre) {
        this.acre = acre;
    }

    public String getHsdno() {
        return hsdno;
    }

    public void setHsdno(String hsdno) {
        this.hsdno = hsdno;
    }

    public String getNopetak() {
        return nopetak;
    }

    public void setNopetak(String nopetak) {
        this.nopetak = nopetak;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LotMemberLot)) {
            return false;
        }
        LotMemberLot other = (LotMemberLot) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.configuration.LotMemberLot[ id=" + id + " ]";
    }
    
}
