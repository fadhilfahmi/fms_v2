/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.configuration;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "addcoa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ChartofJobItem.findAll", query = "SELECT c FROM ChartofJobItem c"),
    @NamedQuery(name = "ChartofJobItem.findByAccountcode", query = "SELECT c FROM ChartofJobItem c WHERE c.accountcode = :accountcode"),
    @NamedQuery(name = "ChartofJobItem.findByJobcode", query = "SELECT c FROM ChartofJobItem c WHERE c.jobcode = :jobcode"),
    @NamedQuery(name = "ChartofJobItem.findByAccountdescp", query = "SELECT c FROM ChartofJobItem c WHERE c.accountdescp = :accountdescp"),
    @NamedQuery(name = "ChartofJobItem.findByRemarks", query = "SELECT c FROM ChartofJobItem c WHERE c.remarks = :remarks"),
    @NamedQuery(name = "ChartofJobItem.findById", query = "SELECT c FROM ChartofJobItem c WHERE c.id = :id")})
public class ChartofJobItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "accountcode")
    private String accountcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "jobcode")
    private String jobcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "accountdescp")
    private String accountdescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "remarks")
    private String remarks;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    public ChartofJobItem() {
    }

    public ChartofJobItem(Integer id) {
        this.id = id;
    }

    public ChartofJobItem(Integer id, String accountcode, String jobcode, String accountdescp, String remarks) {
        this.id = id;
        this.accountcode = accountcode;
        this.jobcode = jobcode;
        this.accountdescp = accountdescp;
        this.remarks = remarks;
    }

    public String getAccountcode() {
        return accountcode;
    }

    public void setAccountcode(String accountcode) {
        this.accountcode = accountcode;
    }

    public String getJobcode() {
        return jobcode;
    }

    public void setJobcode(String jobcode) {
        this.jobcode = jobcode;
    }

    public String getAccountdescp() {
        return accountdescp;
    }

    public void setAccountdescp(String accountdescp) {
        this.accountdescp = accountdescp;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ChartofJobItem)) {
            return false;
        }
        ChartofJobItem other = (ChartofJobItem) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.configuration.ChartofJobItem[ id=" + id + " ]";
    }
    
}
