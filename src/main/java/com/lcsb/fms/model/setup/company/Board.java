/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.company;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "board_info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Board.findAll", query = "SELECT b FROM Board b"),
    @NamedQuery(name = "Board.findByCode", query = "SELECT b FROM Board b WHERE b.code = :code"),
    @NamedQuery(name = "Board.findByTitle", query = "SELECT b FROM Board b WHERE b.title = :title"),
    @NamedQuery(name = "Board.findByUsetitle", query = "SELECT b FROM Board b WHERE b.usetitle = :usetitle"),
    @NamedQuery(name = "Board.findByName", query = "SELECT b FROM Board b WHERE b.name = :name"),
    @NamedQuery(name = "Board.findByIc", query = "SELECT b FROM Board b WHERE b.ic = :ic"),
    @NamedQuery(name = "Board.findByAddress", query = "SELECT b FROM Board b WHERE b.address = :address"),
    @NamedQuery(name = "Board.findByPostcode", query = "SELECT b FROM Board b WHERE b.postcode = :postcode"),
    @NamedQuery(name = "Board.findByCity", query = "SELECT b FROM Board b WHERE b.city = :city"),
    @NamedQuery(name = "Board.findByState", query = "SELECT b FROM Board b WHERE b.state = :state"),
    @NamedQuery(name = "Board.findByBank", query = "SELECT b FROM Board b WHERE b.bank = :bank"),
    @NamedQuery(name = "Board.findByAcc", query = "SELECT b FROM Board b WHERE b.acc = :acc"),
    @NamedQuery(name = "Board.findByPaymethod", query = "SELECT b FROM Board b WHERE b.paymethod = :paymethod"),
    @NamedQuery(name = "Board.findByStatus", query = "SELECT b FROM Board b WHERE b.status = :status"),
    @NamedQuery(name = "Board.findByNotel", query = "SELECT b FROM Board b WHERE b.notel = :notel"),
    @NamedQuery(name = "Board.findByRemark", query = "SELECT b FROM Board b WHERE b.remark = :remark")})
public class Board implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "title")
    private String title;
    @Size(max = 10)
    @Column(name = "usetitle")
    private String usetitle;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ic")
    private String ic;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postcode")
    private String postcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "state")
    private String state;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bank")
    private String bank;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "acc")
    private String acc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paymethod")
    private String paymethod;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "notel")
    private String notel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "remark")
    private String remark;

    public Board() {
    }

    public Board(String code) {
        this.code = code;
    }

    public Board(String code, String title, String name, String ic, String address, String postcode, String city, String state, String bank, String acc, String paymethod, String status, String notel, String remark) {
        this.code = code;
        this.title = title;
        this.name = name;
        this.ic = ic;
        this.address = address;
        this.postcode = postcode;
        this.city = city;
        this.state = state;
        this.bank = bank;
        this.acc = acc;
        this.paymethod = paymethod;
        this.status = status;
        this.notel = notel;
        this.remark = remark;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUsetitle() {
        return usetitle;
    }

    public void setUsetitle(String usetitle) {
        this.usetitle = usetitle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIc() {
        return ic;
    }

    public void setIc(String ic) {
        this.ic = ic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getAcc() {
        return acc;
    }

    public void setAcc(String acc) {
        this.acc = acc;
    }

    public String getPaymethod() {
        return paymethod;
    }

    public void setPaymethod(String paymethod) {
        this.paymethod = paymethod;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNotel() {
        return notel;
    }

    public void setNotel(String notel) {
        this.notel = notel;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Board)) {
            return false;
        }
        Board other = (Board) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.company.Board[ code=" + code + " ]";
    }
    
}
