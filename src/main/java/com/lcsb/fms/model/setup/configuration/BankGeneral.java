/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.configuration;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "info_bank_general")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BankGeneral.findAll", query = "SELECT b FROM BankGeneral b"),
    @NamedQuery(name = "BankGeneral.findByCode", query = "SELECT b FROM BankGeneral b WHERE b.code = :code"),
    @NamedQuery(name = "BankGeneral.findByName", query = "SELECT b FROM BankGeneral b WHERE b.name = :name"),
    @NamedQuery(name = "BankGeneral.findByAddress", query = "SELECT b FROM BankGeneral b WHERE b.address = :address"),
    @NamedQuery(name = "BankGeneral.findByPostcode", query = "SELECT b FROM BankGeneral b WHERE b.postcode = :postcode"),
    @NamedQuery(name = "BankGeneral.findByCity", query = "SELECT b FROM BankGeneral b WHERE b.city = :city"),
    @NamedQuery(name = "BankGeneral.findByState", query = "SELECT b FROM BankGeneral b WHERE b.state = :state"),
    @NamedQuery(name = "BankGeneral.findByGstid", query = "SELECT b FROM BankGeneral b WHERE b.gstid = :gstid")})
public class BankGeneral implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postcode")
    private String postcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "city")
    private String city;
    @Size(max = 100)
    @Column(name = "state")
    private String state;
    @Size(max = 100)
    @Column(name = "gstid")
    private String gstid;
    private String abb;

    public BankGeneral() {
    }

    public BankGeneral(String code) {
        this.code = code;
    }

    public BankGeneral(String code, String name, String address, String postcode, String city, String abb) {
        this.code = code;
        this.name = name;
        this.address = address;
        this.postcode = postcode;
        this.city = city;
        this.abb = abb;
    }

    public String getAbb() {
        return abb;
    }

    public void setAbb(String abb) {
        this.abb = abb;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getGstid() {
        return gstid;
    }

    public void setGstid(String gstid) {
        this.gstid = gstid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BankGeneral)) {
            return false;
        }
        BankGeneral other = (BankGeneral) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.configuration.BankGeneral[ code=" + code + " ]";
    }
    
}
