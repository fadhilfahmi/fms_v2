/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.company;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "info_branch")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BankBranch.findAll", query = "SELECT b FROM BankBranch b"),
    @NamedQuery(name = "BankBranch.findByBranchcode", query = "SELECT b FROM BankBranch b WHERE b.branchcode = :branchcode"),
    @NamedQuery(name = "BankBranch.findByBranchname", query = "SELECT b FROM BankBranch b WHERE b.branchname = :branchname"),
    @NamedQuery(name = "BankBranch.findByPerson", query = "SELECT b FROM BankBranch b WHERE b.person = :person"),
    @NamedQuery(name = "BankBranch.findByTitle", query = "SELECT b FROM BankBranch b WHERE b.title = :title"),
    @NamedQuery(name = "BankBranch.findByPosition", query = "SELECT b FROM BankBranch b WHERE b.position = :position"),
    @NamedQuery(name = "BankBranch.findByHp", query = "SELECT b FROM BankBranch b WHERE b.hp = :hp"),
    @NamedQuery(name = "BankBranch.findByAddress", query = "SELECT b FROM BankBranch b WHERE b.address = :address"),
    @NamedQuery(name = "BankBranch.findByCity", query = "SELECT b FROM BankBranch b WHERE b.city = :city"),
    @NamedQuery(name = "BankBranch.findByState", query = "SELECT b FROM BankBranch b WHERE b.state = :state"),
    @NamedQuery(name = "BankBranch.findByPostcode", query = "SELECT b FROM BankBranch b WHERE b.postcode = :postcode"),
    @NamedQuery(name = "BankBranch.findByPhone", query = "SELECT b FROM BankBranch b WHERE b.phone = :phone"),
    @NamedQuery(name = "BankBranch.findByFax", query = "SELECT b FROM BankBranch b WHERE b.fax = :fax"),
    @NamedQuery(name = "BankBranch.findByEmail", query = "SELECT b FROM BankBranch b WHERE b.email = :email"),
    @NamedQuery(name = "BankBranch.findByUrl", query = "SELECT b FROM BankBranch b WHERE b.url = :url"),
    @NamedQuery(name = "BankBranch.findByRemarks", query = "SELECT b FROM BankBranch b WHERE b.remarks = :remarks"),
    @NamedQuery(name = "BankBranch.findByBankcode", query = "SELECT b FROM BankBranch b WHERE b.bankcode = :bankcode"),
    @NamedQuery(name = "BankBranch.findByBankname", query = "SELECT b FROM BankBranch b WHERE b.bankname = :bankname")})
public class BankBranch implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "branchcode")
    private String branchcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "branchname")
    private String branchname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "person")
    private String person;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "position")
    private String position;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "hp")
    private String hp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "state")
    private String state;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postcode")
    private String postcode;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "phone")
    private String phone;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "fax")
    private String fax;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "url")
    private String url;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bankcode")
    private String bankcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bankname")
    private String bankname;

    public BankBranch() {
    }

    public BankBranch(String branchcode) {
        this.branchcode = branchcode;
    }

    public BankBranch(String branchcode, String branchname, String person, String title, String position, String hp, String address, String city, String state, String postcode, String phone, String fax, String email, String url, String remarks, String bankcode, String bankname) {
        this.branchcode = branchcode;
        this.branchname = branchname;
        this.person = person;
        this.title = title;
        this.position = position;
        this.hp = hp;
        this.address = address;
        this.city = city;
        this.state = state;
        this.postcode = postcode;
        this.phone = phone;
        this.fax = fax;
        this.email = email;
        this.url = url;
        this.remarks = remarks;
        this.bankcode = bankcode;
        this.bankname = bankname;
    }

    public String getBranchcode() {
        return branchcode;
    }

    public void setBranchcode(String branchcode) {
        this.branchcode = branchcode;
    }

    public String getBranchname() {
        return branchname;
    }

    public void setBranchname(String branchname) {
        this.branchname = branchname;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (branchcode != null ? branchcode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BankBranch)) {
            return false;
        }
        BankBranch other = (BankBranch) object;
        if ((this.branchcode == null && other.branchcode != null) || (this.branchcode != null && !this.branchcode.equals(other.branchcode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.dao.setup.configuration.BankBranch[ branchcode=" + branchcode + " ]";
    }
    
}
