/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.company.staff;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "co_staff_bank")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StaffBank.findAll", query = "SELECT s FROM StaffBank s"),
    @NamedQuery(name = "StaffBank.findById", query = "SELECT s FROM StaffBank s WHERE s.id = :id"),
    @NamedQuery(name = "StaffBank.findByStaffid", query = "SELECT s FROM StaffBank s WHERE s.staffid = :staffid"),
    @NamedQuery(name = "StaffBank.findByBankcode", query = "SELECT s FROM StaffBank s WHERE s.bankcode = :bankcode"),
    @NamedQuery(name = "StaffBank.findByBankdesc", query = "SELECT s FROM StaffBank s WHERE s.bankdesc = :bankdesc"),
    @NamedQuery(name = "StaffBank.findByStatus", query = "SELECT s FROM StaffBank s WHERE s.status = :status"),
    @NamedQuery(name = "StaffBank.findByAccno", query = "SELECT s FROM StaffBank s WHERE s.accno = :accno")})
public class StaffBank implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "staffid")
    private String staffid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "bankcode")
    private String bankcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bankdesc")
    private String bankdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "accno")
    private String accno;

    public StaffBank() {
    }

    public StaffBank(Integer id) {
        this.id = id;
    }

    public StaffBank(Integer id, String staffid, String bankcode, String bankdesc, String status, String accno) {
        this.id = id;
        this.staffid = staffid;
        this.bankcode = bankcode;
        this.bankdesc = bankdesc;
        this.status = status;
        this.accno = accno;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getBankdesc() {
        return bankdesc;
    }

    public void setBankdesc(String bankdesc) {
        this.bankdesc = bankdesc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAccno() {
        return accno;
    }

    public void setAccno(String accno) {
        this.accno = accno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StaffBank)) {
            return false;
        }
        StaffBank other = (StaffBank) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.company.staff.StaffBank[ id=" + id + " ]";
    }
    
}
