/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.configuration;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "lot_info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LotInfomation.findAll", query = "SELECT l FROM LotInfomation l"),
    @NamedQuery(name = "LotInfomation.findByLotCode", query = "SELECT l FROM LotInfomation l WHERE l.lotCode = :lotCode"),
    @NamedQuery(name = "LotInfomation.findByLoccode", query = "SELECT l FROM LotInfomation l WHERE l.loccode = :loccode"),
    @NamedQuery(name = "LotInfomation.findByLotDescp", query = "SELECT l FROM LotInfomation l WHERE l.lotDescp = :lotDescp"),
    @NamedQuery(name = "LotInfomation.findByLocDescp", query = "SELECT l FROM LotInfomation l WHERE l.locDescp = :locDescp"),
    @NamedQuery(name = "LotInfomation.findByLotHect", query = "SELECT l FROM LotInfomation l WHERE l.lotHect = :lotHect"),
    @NamedQuery(name = "LotInfomation.findByLotAcre", query = "SELECT l FROM LotInfomation l WHERE l.lotAcre = :lotAcre"),
    @NamedQuery(name = "LotInfomation.findByActive", query = "SELECT l FROM LotInfomation l WHERE l.active = :active"),
    @NamedQuery(name = "LotInfomation.findByCapProv", query = "SELECT l FROM LotInfomation l WHERE l.capProv = :capProv")})
public class LotInformation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "lot_Code")
    private String lotCode;
    @Size(max = 10)
    @Column(name = "loccode")
    private String loccode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "lot_Descp")
    private String lotDescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loc_Descp")
    private String locDescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "lot_Hect")
    private String lotHect;
    @Size(max = 50)
    @Column(name = "lot_Acre")
    private String lotAcre;
    @Size(max = 10)
    @Column(name = "active")
    private String active;
    @Size(max = 10)
    @Column(name = "cap_prov")
    private String capProv;

    public LotInformation() {
    }

    public LotInformation(String lotCode) {
        this.lotCode = lotCode;
    }

    public LotInformation(String lotCode, String lotDescp, String locDescp, String lotHect) {
        this.lotCode = lotCode;
        this.lotDescp = lotDescp;
        this.locDescp = locDescp;
        this.lotHect = lotHect;
    }

    public String getLotCode() {
        return lotCode;
    }

    public void setLotCode(String lotCode) {
        this.lotCode = lotCode;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLotDescp() {
        return lotDescp;
    }

    public void setLotDescp(String lotDescp) {
        this.lotDescp = lotDescp;
    }

    public String getLocDescp() {
        return locDescp;
    }

    public void setLocDescp(String locDescp) {
        this.locDescp = locDescp;
    }

    public String getLotHect() {
        return lotHect;
    }

    public void setLotHect(String lotHect) {
        this.lotHect = lotHect;
    }

    public String getLotAcre() {
        return lotAcre;
    }

    public void setLotAcre(String lotAcre) {
        this.lotAcre = lotAcre;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCapProv() {
        return capProv;
    }

    public void setCapProv(String capProv) {
        this.capProv = capProv;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lotCode != null ? lotCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LotInformation)) {
            return false;
        }
        LotInformation other = (LotInformation) object;
        if ((this.lotCode == null && other.lotCode != null) || (this.lotCode != null && !this.lotCode.equals(other.lotCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.configuration.LotInfomation[ lotCode=" + lotCode + " ]";
    }
    
}
