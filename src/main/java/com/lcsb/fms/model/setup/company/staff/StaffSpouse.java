/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.company.staff;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "co_staff_spouse")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StaffSpouse.findAll", query = "SELECT s FROM StaffSpouse s"),
    @NamedQuery(name = "StaffSpouse.findByName", query = "SELECT s FROM StaffSpouse s WHERE s.name = :name"),
    @NamedQuery(name = "StaffSpouse.findBySpouseno", query = "SELECT s FROM StaffSpouse s WHERE s.spouseno = :spouseno"),
    @NamedQuery(name = "StaffSpouse.findByIc", query = "SELECT s FROM StaffSpouse s WHERE s.ic = :ic"),
    @NamedQuery(name = "StaffSpouse.findByPassport", query = "SELECT s FROM StaffSpouse s WHERE s.passport = :passport"),
    @NamedQuery(name = "StaffSpouse.findByTaxno", query = "SELECT s FROM StaffSpouse s WHERE s.taxno = :taxno"),
    @NamedQuery(name = "StaffSpouse.findByDob", query = "SELECT s FROM StaffSpouse s WHERE s.dob = :dob"),
    @NamedQuery(name = "StaffSpouse.findByRace", query = "SELECT s FROM StaffSpouse s WHERE s.race = :race"),
    @NamedQuery(name = "StaffSpouse.findByReligion", query = "SELECT s FROM StaffSpouse s WHERE s.religion = :religion"),
    @NamedQuery(name = "StaffSpouse.findByCitizen", query = "SELECT s FROM StaffSpouse s WHERE s.citizen = :citizen"),
    @NamedQuery(name = "StaffSpouse.findByDom", query = "SELECT s FROM StaffSpouse s WHERE s.dom = :dom"),
    @NamedQuery(name = "StaffSpouse.findByDivorce", query = "SELECT s FROM StaffSpouse s WHERE s.divorce = :divorce"),
    @NamedQuery(name = "StaffSpouse.findByWork", query = "SELECT s FROM StaffSpouse s WHERE s.work = :work"),
    @NamedQuery(name = "StaffSpouse.findByStaffid", query = "SELECT s FROM StaffSpouse s WHERE s.staffid = :staffid"),
    @NamedQuery(name = "StaffSpouse.findById", query = "SELECT s FROM StaffSpouse s WHERE s.id = :id"),
    @NamedQuery(name = "StaffSpouse.findByWorking", query = "SELECT s FROM StaffSpouse s WHERE s.working = :working")})
public class StaffSpouse implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "spouseno")
    private String spouseno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ic")
    private String ic;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "passport")
    private String passport;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxno")
    private String taxno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "dob")
    private String dob;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "race")
    private String race;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "religion")
    private String religion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "citizen")
    private String citizen;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "dom")
    private String dom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "divorce")
    private String divorce;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "work")
    private String work;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "staffid")
    private String staffid;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "working")
    private String working;

    public StaffSpouse() {
    }

    public StaffSpouse(Integer id) {
        this.id = id;
    }

    public StaffSpouse(Integer id, String name, String spouseno, String ic, String passport, String taxno, String dob, String race, String religion, String citizen, String dom, String divorce, String work, String staffid, String working) {
        this.id = id;
        this.name = name;
        this.spouseno = spouseno;
        this.ic = ic;
        this.passport = passport;
        this.taxno = taxno;
        this.dob = dob;
        this.race = race;
        this.religion = religion;
        this.citizen = citizen;
        this.dom = dom;
        this.divorce = divorce;
        this.work = work;
        this.staffid = staffid;
        this.working = working;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpouseno() {
        return spouseno;
    }

    public void setSpouseno(String spouseno) {
        this.spouseno = spouseno;
    }

    public String getIc() {
        return ic;
    }

    public void setIc(String ic) {
        this.ic = ic;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getTaxno() {
        return taxno;
    }

    public void setTaxno(String taxno) {
        this.taxno = taxno;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getCitizen() {
        return citizen;
    }

    public void setCitizen(String citizen) {
        this.citizen = citizen;
    }

    public String getDom() {
        return dom;
    }

    public void setDom(String dom) {
        this.dom = dom;
    }

    public String getDivorce() {
        return divorce;
    }

    public void setDivorce(String divorce) {
        this.divorce = divorce;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWorking() {
        return working;
    }

    public void setWorking(String working) {
        this.working = working;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StaffSpouse)) {
            return false;
        }
        StaffSpouse other = (StaffSpouse) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.company.staff.StaffSpouse[ id=" + id + " ]";
    }
    
}
