/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.configuration;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "contractor_info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contractor.findAll", query = "SELECT c FROM Contractor c"),
    @NamedQuery(name = "Contractor.findByVendor", query = "SELECT c FROM Contractor c WHERE c.vendor = :vendor"),
    @NamedQuery(name = "Contractor.findByCode", query = "SELECT c FROM Contractor c WHERE c.code = :code"),
    @NamedQuery(name = "Contractor.findByCompanyname", query = "SELECT c FROM Contractor c WHERE c.companyname = :companyname"),
    @NamedQuery(name = "Contractor.findByRegister", query = "SELECT c FROM Contractor c WHERE c.register = :register"),
    @NamedQuery(name = "Contractor.findByBumiputra", query = "SELECT c FROM Contractor c WHERE c.bumiputra = :bumiputra"),
    @NamedQuery(name = "Contractor.findByOwnername", query = "SELECT c FROM Contractor c WHERE c.ownername = :ownername"),
    @NamedQuery(name = "Contractor.findByOwneric", query = "SELECT c FROM Contractor c WHERE c.owneric = :owneric"),
    @NamedQuery(name = "Contractor.findByNationality", query = "SELECT c FROM Contractor c WHERE c.nationality = :nationality"),
    @NamedQuery(name = "Contractor.findByKelas", query = "SELECT c FROM Contractor c WHERE c.kelas = :kelas"),
    @NamedQuery(name = "Contractor.findByCoa", query = "SELECT c FROM Contractor c WHERE c.coa = :coa"),
    @NamedQuery(name = "Contractor.findByCoadescp", query = "SELECT c FROM Contractor c WHERE c.coadescp = :coadescp"),
    @NamedQuery(name = "Contractor.findByContactperson", query = "SELECT c FROM Contractor c WHERE c.contactperson = :contactperson"),
    @NamedQuery(name = "Contractor.findByContacttitle", query = "SELECT c FROM Contractor c WHERE c.contacttitle = :contacttitle"),
    @NamedQuery(name = "Contractor.findByContactposition", query = "SELECT c FROM Contractor c WHERE c.contactposition = :contactposition"),
    @NamedQuery(name = "Contractor.findByAddress", query = "SELECT c FROM Contractor c WHERE c.address = :address"),
    @NamedQuery(name = "Contractor.findByCity", query = "SELECT c FROM Contractor c WHERE c.city = :city"),
    @NamedQuery(name = "Contractor.findByState", query = "SELECT c FROM Contractor c WHERE c.state = :state"),
    @NamedQuery(name = "Contractor.findByPostcode", query = "SELECT c FROM Contractor c WHERE c.postcode = :postcode"),
    @NamedQuery(name = "Contractor.findByCountry", query = "SELECT c FROM Contractor c WHERE c.country = :country"),
    @NamedQuery(name = "Contractor.findByPhone", query = "SELECT c FROM Contractor c WHERE c.phone = :phone"),
    @NamedQuery(name = "Contractor.findByFax", query = "SELECT c FROM Contractor c WHERE c.fax = :fax"),
    @NamedQuery(name = "Contractor.findByEmail", query = "SELECT c FROM Contractor c WHERE c.email = :email"),
    @NamedQuery(name = "Contractor.findByUrl", query = "SELECT c FROM Contractor c WHERE c.url = :url"),
    @NamedQuery(name = "Contractor.findByBank", query = "SELECT c FROM Contractor c WHERE c.bank = :bank"),
    @NamedQuery(name = "Contractor.findByBankaccount", query = "SELECT c FROM Contractor c WHERE c.bankaccount = :bankaccount"),
    @NamedQuery(name = "Contractor.findByPayment", query = "SELECT c FROM Contractor c WHERE c.payment = :payment"),
    @NamedQuery(name = "Contractor.findByRemarks", query = "SELECT c FROM Contractor c WHERE c.remarks = :remarks"),
    @NamedQuery(name = "Contractor.findByDepositcoa", query = "SELECT c FROM Contractor c WHERE c.depositcoa = :depositcoa"),
    @NamedQuery(name = "Contractor.findByDepositcoadescp", query = "SELECT c FROM Contractor c WHERE c.depositcoadescp = :depositcoadescp"),
    @NamedQuery(name = "Contractor.findByRetention", query = "SELECT c FROM Contractor c WHERE c.retention = :retention"),
    @NamedQuery(name = "Contractor.findByRetentiondescp", query = "SELECT c FROM Contractor c WHERE c.retentiondescp = :retentiondescp"),
    @NamedQuery(name = "Contractor.findByHp", query = "SELECT c FROM Contractor c WHERE c.hp = :hp"),
    @NamedQuery(name = "Contractor.findByBankdesc", query = "SELECT c FROM Contractor c WHERE c.bankdesc = :bankdesc"),
    @NamedQuery(name = "Contractor.findByPermitcode", query = "SELECT c FROM Contractor c WHERE c.permitcode = :permitcode"),
    @NamedQuery(name = "Contractor.findByPermitdesc", query = "SELECT c FROM Contractor c WHERE c.permitdesc = :permitdesc")})
public class Contractor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "vendor")
    private String vendor;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "companyname")
    private String companyname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "register")
    private String register;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bumiputra")
    private String bumiputra;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ownername")
    private String ownername;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "owneric")
    private String owneric;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nationality")
    private String nationality;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "kelas")
    private String kelas;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coa")
    private String coa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coadescp")
    private String coadescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "contactperson")
    private String contactperson;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "contacttitle")
    private String contacttitle;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "contactposition")
    private String contactposition;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "state")
    private String state;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postcode")
    private String postcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "country")
    private String country;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "phone")
    private String phone;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "fax")
    private String fax;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "url")
    private String url;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bank")
    private String bank;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bankaccount")
    private String bankaccount;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "payment")
    private String payment;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "depositcoa")
    private String depositcoa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "depositcoadescp")
    private String depositcoadescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "retention")
    private String retention;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "retentiondescp")
    private String retentiondescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "hp")
    private String hp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bankdesc")
    private String bankdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "permitcode")
    private String permitcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "permitdesc")
    private String permitdesc;

    public Contractor() {
    }

    public Contractor(String code) {
        this.code = code;
    }

    public Contractor(String code, String vendor, String companyname, String register, String bumiputra, String ownername, String owneric, String nationality, String kelas, String coa, String coadescp, String contactperson, String contacttitle, String contactposition, String address, String city, String state, String postcode, String country, String phone, String fax, String email, String url, String bank, String bankaccount, String payment, String remarks, String depositcoa, String depositcoadescp, String retention, String retentiondescp, String hp, String bankdesc, String permitcode, String permitdesc) {
        this.code = code;
        this.vendor = vendor;
        this.companyname = companyname;
        this.register = register;
        this.bumiputra = bumiputra;
        this.ownername = ownername;
        this.owneric = owneric;
        this.nationality = nationality;
        this.kelas = kelas;
        this.coa = coa;
        this.coadescp = coadescp;
        this.contactperson = contactperson;
        this.contacttitle = contacttitle;
        this.contactposition = contactposition;
        this.address = address;
        this.city = city;
        this.state = state;
        this.postcode = postcode;
        this.country = country;
        this.phone = phone;
        this.fax = fax;
        this.email = email;
        this.url = url;
        this.bank = bank;
        this.bankaccount = bankaccount;
        this.payment = payment;
        this.remarks = remarks;
        this.depositcoa = depositcoa;
        this.depositcoadescp = depositcoadescp;
        this.retention = retention;
        this.retentiondescp = retentiondescp;
        this.hp = hp;
        this.bankdesc = bankdesc;
        this.permitcode = permitcode;
        this.permitdesc = permitdesc;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }

    public String getBumiputra() {
        return bumiputra;
    }

    public void setBumiputra(String bumiputra) {
        this.bumiputra = bumiputra;
    }

    public String getOwnername() {
        return ownername;
    }

    public void setOwnername(String ownername) {
        this.ownername = ownername;
    }

    public String getOwneric() {
        return owneric;
    }

    public void setOwneric(String owneric) {
        this.owneric = owneric;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getCoa() {
        return coa;
    }

    public void setCoa(String coa) {
        this.coa = coa;
    }

    public String getCoadescp() {
        return coadescp;
    }

    public void setCoadescp(String coadescp) {
        this.coadescp = coadescp;
    }

    public String getContactperson() {
        return contactperson;
    }

    public void setContactperson(String contactperson) {
        this.contactperson = contactperson;
    }

    public String getContacttitle() {
        return contacttitle;
    }

    public void setContacttitle(String contacttitle) {
        this.contacttitle = contacttitle;
    }

    public String getContactposition() {
        return contactposition;
    }

    public void setContactposition(String contactposition) {
        this.contactposition = contactposition;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankaccount() {
        return bankaccount;
    }

    public void setBankaccount(String bankaccount) {
        this.bankaccount = bankaccount;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDepositcoa() {
        return depositcoa;
    }

    public void setDepositcoa(String depositcoa) {
        this.depositcoa = depositcoa;
    }

    public String getDepositcoadescp() {
        return depositcoadescp;
    }

    public void setDepositcoadescp(String depositcoadescp) {
        this.depositcoadescp = depositcoadescp;
    }

    public String getRetention() {
        return retention;
    }

    public void setRetention(String retention) {
        this.retention = retention;
    }

    public String getRetentiondescp() {
        return retentiondescp;
    }

    public void setRetentiondescp(String retentiondescp) {
        this.retentiondescp = retentiondescp;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getBankdesc() {
        return bankdesc;
    }

    public void setBankdesc(String bankdesc) {
        this.bankdesc = bankdesc;
    }

    public String getPermitcode() {
        return permitcode;
    }

    public void setPermitcode(String permitcode) {
        this.permitcode = permitcode;
    }

    public String getPermitdesc() {
        return permitdesc;
    }

    public void setPermitdesc(String permitdesc) {
        this.permitdesc = permitdesc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contractor)) {
            return false;
        }
        Contractor other = (Contractor) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.configuration.Contractor[ code=" + code + " ]";
    }
    
}
