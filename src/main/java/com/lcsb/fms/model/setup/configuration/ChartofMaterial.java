/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.configuration;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "chartofmaterial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ChartofMaterial.findAll", query = "SELECT c FROM ChartofMaterial c"),
    @NamedQuery(name = "ChartofMaterial.findByCode", query = "SELECT c FROM ChartofMaterial c WHERE c.code = :code"),
    @NamedQuery(name = "ChartofMaterial.findByDescp", query = "SELECT c FROM ChartofMaterial c WHERE c.descp = :descp"),
    @NamedQuery(name = "ChartofMaterial.findByType", query = "SELECT c FROM ChartofMaterial c WHERE c.type = :type"),
    @NamedQuery(name = "ChartofMaterial.findByAccountcode", query = "SELECT c FROM ChartofMaterial c WHERE c.accountcode = :accountcode"),
    @NamedQuery(name = "ChartofMaterial.findByAccountdescp", query = "SELECT c FROM ChartofMaterial c WHERE c.accountdescp = :accountdescp"),
    @NamedQuery(name = "ChartofMaterial.findByFinallvl", query = "SELECT c FROM ChartofMaterial c WHERE c.finallvl = :finallvl"),
    @NamedQuery(name = "ChartofMaterial.findByActive", query = "SELECT c FROM ChartofMaterial c WHERE c.active = :active"),
    @NamedQuery(name = "ChartofMaterial.findByMatgroup", query = "SELECT c FROM ChartofMaterial c WHERE c.matgroup = :matgroup"),
    @NamedQuery(name = "ChartofMaterial.findByFuellubricant", query = "SELECT c FROM ChartofMaterial c WHERE c.fuellubricant = :fuellubricant"),
    @NamedQuery(name = "ChartofMaterial.findByUnitmeasure", query = "SELECT c FROM ChartofMaterial c WHERE c.unitmeasure = :unitmeasure")})
public class ChartofMaterial implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "descp")
    private String descp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "accountcode")
    private String accountcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "accountdescp")
    private String accountdescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "finallvl")
    private String finallvl;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "active")
    private String active;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "matgroup")
    private String matgroup;
    @Size(max = 100)
    @Column(name = "fuellubricant")
    private String fuellubricant;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "unitmeasure")
    private String unitmeasure;

    public ChartofMaterial() {
    }

    public ChartofMaterial(String code) {
        this.code = code;
    }

    public ChartofMaterial(String code, String descp, String type, String accountcode, String accountdescp, String finallvl, String active, String matgroup, String unitmeasure) {
        this.code = code;
        this.descp = descp;
        this.type = type;
        this.accountcode = accountcode;
        this.accountdescp = accountdescp;
        this.finallvl = finallvl;
        this.active = active;
        this.matgroup = matgroup;
        this.unitmeasure = unitmeasure;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAccountcode() {
        return accountcode;
    }

    public void setAccountcode(String accountcode) {
        this.accountcode = accountcode;
    }

    public String getAccountdescp() {
        return accountdescp;
    }

    public void setAccountdescp(String accountdescp) {
        this.accountdescp = accountdescp;
    }

    public String getFinallvl() {
        return finallvl;
    }

    public void setFinallvl(String finallvl) {
        this.finallvl = finallvl;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getMatgroup() {
        return matgroup;
    }

    public void setMatgroup(String matgroup) {
        this.matgroup = matgroup;
    }

    public String getFuellubricant() {
        return fuellubricant;
    }

    public void setFuellubricant(String fuellubricant) {
        this.fuellubricant = fuellubricant;
    }

    public String getUnitmeasure() {
        return unitmeasure;
    }

    public void setUnitmeasure(String unitmeasure) {
        this.unitmeasure = unitmeasure;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ChartofMaterial)) {
            return false;
        }
        ChartofMaterial other = (ChartofMaterial) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.configuration.ChartofMaterial[ code=" + code + " ]";
    }
    
}
