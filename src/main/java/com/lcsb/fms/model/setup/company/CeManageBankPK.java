/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.company;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author fadhilfahmi
 */
@Embeddable
public class CeManageBankPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "branchcode")
    private String branchcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "managecode")
    private String managecode;

    public CeManageBankPK() {
    }

    public CeManageBankPK(String code, String branchcode, String managecode) {
        this.code = code;
        this.branchcode = branchcode;
        this.managecode = managecode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBranchcode() {
        return branchcode;
    }

    public void setBranchcode(String branchcode) {
        this.branchcode = branchcode;
    }

    public String getManagecode() {
        return managecode;
    }

    public void setManagecode(String managecode) {
        this.managecode = managecode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        hash += (branchcode != null ? branchcode.hashCode() : 0);
        hash += (managecode != null ? managecode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CeManageBankPK)) {
            return false;
        }
        CeManageBankPK other = (CeManageBankPK) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        if ((this.branchcode == null && other.branchcode != null) || (this.branchcode != null && !this.branchcode.equals(other.branchcode))) {
            return false;
        }
        if ((this.managecode == null && other.managecode != null) || (this.managecode != null && !this.managecode.equals(other.managecode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.company.CeManageBankPK[ code=" + code + ", branchcode=" + branchcode + ", managecode=" + managecode + " ]";
    }
    
}
