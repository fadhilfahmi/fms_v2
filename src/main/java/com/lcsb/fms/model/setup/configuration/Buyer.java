/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.configuration;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "buyer_info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Buyer.findAll", query = "SELECT b FROM Buyer b"),
    @NamedQuery(name = "Buyer.findByVendor", query = "SELECT b FROM Buyer b WHERE b.vendor = :vendor"),
    @NamedQuery(name = "Buyer.findByCode", query = "SELECT b FROM Buyer b WHERE b.code = :code"),
    @NamedQuery(name = "Buyer.findByCodemill", query = "SELECT b FROM Buyer b WHERE b.codemill = :codemill"),
    @NamedQuery(name = "Buyer.findByCompanyname", query = "SELECT b FROM Buyer b WHERE b.companyname = :companyname"),
    @NamedQuery(name = "Buyer.findByCoregister", query = "SELECT b FROM Buyer b WHERE b.coregister = :coregister"),
    @NamedQuery(name = "Buyer.findByBumiputra", query = "SELECT b FROM Buyer b WHERE b.bumiputra = :bumiputra"),
    @NamedQuery(name = "Buyer.findByPorla", query = "SELECT b FROM Buyer b WHERE b.porla = :porla"),
    @NamedQuery(name = "Buyer.findByMill", query = "SELECT b FROM Buyer b WHERE b.mill = :mill"),
    @NamedQuery(name = "Buyer.findByCoa", query = "SELECT b FROM Buyer b WHERE b.coa = :coa"),
    @NamedQuery(name = "Buyer.findByCoadescp", query = "SELECT b FROM Buyer b WHERE b.coadescp = :coadescp"),
    @NamedQuery(name = "Buyer.findByPerson", query = "SELECT b FROM Buyer b WHERE b.person = :person"),
    @NamedQuery(name = "Buyer.findByTitle", query = "SELECT b FROM Buyer b WHERE b.title = :title"),
    @NamedQuery(name = "Buyer.findByPosition", query = "SELECT b FROM Buyer b WHERE b.position = :position"),
    @NamedQuery(name = "Buyer.findByHp", query = "SELECT b FROM Buyer b WHERE b.hp = :hp"),
    @NamedQuery(name = "Buyer.findByAddress", query = "SELECT b FROM Buyer b WHERE b.address = :address"),
    @NamedQuery(name = "Buyer.findByCity", query = "SELECT b FROM Buyer b WHERE b.city = :city"),
    @NamedQuery(name = "Buyer.findByState", query = "SELECT b FROM Buyer b WHERE b.state = :state"),
    @NamedQuery(name = "Buyer.findByPostcode", query = "SELECT b FROM Buyer b WHERE b.postcode = :postcode"),
    @NamedQuery(name = "Buyer.findByCountry", query = "SELECT b FROM Buyer b WHERE b.country = :country"),
    @NamedQuery(name = "Buyer.findByPhone", query = "SELECT b FROM Buyer b WHERE b.phone = :phone"),
    @NamedQuery(name = "Buyer.findByFax", query = "SELECT b FROM Buyer b WHERE b.fax = :fax"),
    @NamedQuery(name = "Buyer.findByEmail", query = "SELECT b FROM Buyer b WHERE b.email = :email"),
    @NamedQuery(name = "Buyer.findByUrl", query = "SELECT b FROM Buyer b WHERE b.url = :url"),
    @NamedQuery(name = "Buyer.findByRemarks", query = "SELECT b FROM Buyer b WHERE b.remarks = :remarks"),
    @NamedQuery(name = "Buyer.findByGstid", query = "SELECT b FROM Buyer b WHERE b.gstid = :gstid")})
public class Buyer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "vendor")
    private String vendor;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "codemill")
    private String codemill;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "companyname")
    private String companyname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coregister")
    private String coregister;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bumiputra")
    private String bumiputra;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "porla")
    private String porla;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "mill")
    private String mill;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coa")
    private String coa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coadescp")
    private String coadescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "person")
    private String person;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "position")
    private String position;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "hp")
    private String hp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "state")
    private String state;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postcode")
    private String postcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "country")
    private String country;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "phone")
    private String phone;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "fax")
    private String fax;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "url")
    private String url;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "gstid")
    private String gstid;
    private String paymentterm;
    private String taxinvoicetype;

    public Buyer() {
    }

    public Buyer(String code) {
        this.code = code;
    }

    public Buyer(String code, String vendor, String codemill, String companyname, String coregister, String bumiputra, String porla, String mill, String coa, String coadescp, String person, String title, String position, String hp, String address, String city, String state, String postcode, String country, String phone, String fax, String email, String url, String remarks, String gstid, String paymentterm, String taxinvoicetype) {
        this.code = code;
        this.vendor = vendor;
        this.codemill = codemill;
        this.companyname = companyname;
        this.coregister = coregister;
        this.bumiputra = bumiputra;
        this.porla = porla;
        this.mill = mill;
        this.coa = coa;
        this.coadescp = coadescp;
        this.person = person;
        this.title = title;
        this.position = position;
        this.hp = hp;
        this.address = address;
        this.city = city;
        this.state = state;
        this.postcode = postcode;
        this.country = country;
        this.phone = phone;
        this.fax = fax;
        this.email = email;
        this.url = url;
        this.remarks = remarks;
        this.gstid = gstid;
        this.paymentterm = paymentterm;
        this.taxinvoicetype = taxinvoicetype;
    }

    public String getTaxinvoicetype() {
        return taxinvoicetype;
    }

    public void setTaxinvoicetype(String taxinvoicetype) {
        this.taxinvoicetype = taxinvoicetype;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getPaymentterm() {
        return paymentterm;
    }

    public void setPaymentterm(String paymentterm) {
        this.paymentterm = paymentterm;
    }

    

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodemill() {
        return codemill;
    }

    public void setCodemill(String codemill) {
        this.codemill = codemill;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getCoregister() {
        return coregister;
    }

    public void setCoregister(String coregister) {
        this.coregister = coregister;
    }

    public String getBumiputra() {
        return bumiputra;
    }

    public void setBumiputra(String bumiputra) {
        this.bumiputra = bumiputra;
    }

    public String getPorla() {
        return porla;
    }

    public void setPorla(String porla) {
        this.porla = porla;
    }

    public String getMill() {
        return mill;
    }

    public void setMill(String mill) {
        this.mill = mill;
    }

    public String getCoa() {
        return coa;
    }

    public void setCoa(String coa) {
        this.coa = coa;
    }

    public String getCoadescp() {
        return coadescp;
    }

    public void setCoadescp(String coadescp) {
        this.coadescp = coadescp;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getGstid() {
        return gstid;
    }

    public void setGstid(String gstid) {
        this.gstid = gstid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Buyer)) {
            return false;
        }
        Buyer other = (Buyer) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.configuration.Buyer[ code=" + code + " ]";
    }
    
}
