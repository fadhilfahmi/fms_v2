/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.configuration;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "broker_info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Broker.findAll", query = "SELECT b FROM Broker b"),
    @NamedQuery(name = "Broker.findByCode", query = "SELECT b FROM Broker b WHERE b.code = :code"),
    @NamedQuery(name = "Broker.findByCompanyname", query = "SELECT b FROM Broker b WHERE b.companyname = :companyname"),
    @NamedQuery(name = "Broker.findByCoregister", query = "SELECT b FROM Broker b WHERE b.coregister = :coregister"),
    @NamedQuery(name = "Broker.findByBumiputra", query = "SELECT b FROM Broker b WHERE b.bumiputra = :bumiputra"),
    @NamedQuery(name = "Broker.findByHqactdesc", query = "SELECT b FROM Broker b WHERE b.hqactdesc = :hqactdesc"),
    @NamedQuery(name = "Broker.findByHqactcode", query = "SELECT b FROM Broker b WHERE b.hqactcode = :hqactcode"),
    @NamedQuery(name = "Broker.findByCoa", query = "SELECT b FROM Broker b WHERE b.coa = :coa"),
    @NamedQuery(name = "Broker.findByCoadescp", query = "SELECT b FROM Broker b WHERE b.coadescp = :coadescp"),
    @NamedQuery(name = "Broker.findByPerson", query = "SELECT b FROM Broker b WHERE b.person = :person"),
    @NamedQuery(name = "Broker.findByTitle", query = "SELECT b FROM Broker b WHERE b.title = :title"),
    @NamedQuery(name = "Broker.findByPosition", query = "SELECT b FROM Broker b WHERE b.position = :position"),
    @NamedQuery(name = "Broker.findByHp", query = "SELECT b FROM Broker b WHERE b.hp = :hp"),
    @NamedQuery(name = "Broker.findByAddress", query = "SELECT b FROM Broker b WHERE b.address = :address"),
    @NamedQuery(name = "Broker.findByCity", query = "SELECT b FROM Broker b WHERE b.city = :city"),
    @NamedQuery(name = "Broker.findByState", query = "SELECT b FROM Broker b WHERE b.state = :state"),
    @NamedQuery(name = "Broker.findByPostcode", query = "SELECT b FROM Broker b WHERE b.postcode = :postcode"),
    @NamedQuery(name = "Broker.findByCountry", query = "SELECT b FROM Broker b WHERE b.country = :country"),
    @NamedQuery(name = "Broker.findByPhone", query = "SELECT b FROM Broker b WHERE b.phone = :phone"),
    @NamedQuery(name = "Broker.findByFax", query = "SELECT b FROM Broker b WHERE b.fax = :fax"),
    @NamedQuery(name = "Broker.findByEmail", query = "SELECT b FROM Broker b WHERE b.email = :email"),
    @NamedQuery(name = "Broker.findByUrl", query = "SELECT b FROM Broker b WHERE b.url = :url"),
    @NamedQuery(name = "Broker.findByBank", query = "SELECT b FROM Broker b WHERE b.bank = :bank"),
    @NamedQuery(name = "Broker.findByBankaccount", query = "SELECT b FROM Broker b WHERE b.bankaccount = :bankaccount"),
    @NamedQuery(name = "Broker.findByPayment", query = "SELECT b FROM Broker b WHERE b.payment = :payment"),
    @NamedQuery(name = "Broker.findByRemarks", query = "SELECT b FROM Broker b WHERE b.remarks = :remarks"),
    @NamedQuery(name = "Broker.findByBankdesc", query = "SELECT b FROM Broker b WHERE b.bankdesc = :bankdesc"),
    @NamedQuery(name = "Broker.findByActive", query = "SELECT b FROM Broker b WHERE b.active = :active")})
public class Broker implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "companyname")
    private String companyname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coregister")
    private String coregister;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bumiputra")
    private String bumiputra;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "hqactdesc")
    private String hqactdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "hqactcode")
    private String hqactcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coa")
    private String coa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coadescp")
    private String coadescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "person")
    private String person;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "position")
    private String position;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "hp")
    private String hp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "state")
    private String state;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postcode")
    private String postcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "country")
    private String country;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "phone")
    private String phone;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "fax")
    private String fax;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "url")
    private String url;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bank")
    private String bank;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bankaccount")
    private String bankaccount;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "payment")
    private String payment;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bankdesc")
    private String bankdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "active")
    private String active;

    public Broker() {
    }

    public Broker(String code) {
        this.code = code;
    }

    public Broker(String code, String companyname, String coregister, String bumiputra, String hqactdesc, String hqactcode, String coa, String coadescp, String person, String title, String position, String hp, String address, String city, String state, String postcode, String country, String phone, String fax, String email, String url, String bank, String bankaccount, String payment, String remarks, String bankdesc, String active) {
        this.code = code;
        this.companyname = companyname;
        this.coregister = coregister;
        this.bumiputra = bumiputra;
        this.hqactdesc = hqactdesc;
        this.hqactcode = hqactcode;
        this.coa = coa;
        this.coadescp = coadescp;
        this.person = person;
        this.title = title;
        this.position = position;
        this.hp = hp;
        this.address = address;
        this.city = city;
        this.state = state;
        this.postcode = postcode;
        this.country = country;
        this.phone = phone;
        this.fax = fax;
        this.email = email;
        this.url = url;
        this.bank = bank;
        this.bankaccount = bankaccount;
        this.payment = payment;
        this.remarks = remarks;
        this.bankdesc = bankdesc;
        this.active = active;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getCoregister() {
        return coregister;
    }

    public void setCoregister(String coregister) {
        this.coregister = coregister;
    }

    public String getBumiputra() {
        return bumiputra;
    }

    public void setBumiputra(String bumiputra) {
        this.bumiputra = bumiputra;
    }

    public String getHqactdesc() {
        return hqactdesc;
    }

    public void setHqactdesc(String hqactdesc) {
        this.hqactdesc = hqactdesc;
    }

    public String getHqactcode() {
        return hqactcode;
    }

    public void setHqactcode(String hqactcode) {
        this.hqactcode = hqactcode;
    }

    public String getCoa() {
        return coa;
    }

    public void setCoa(String coa) {
        this.coa = coa;
    }

    public String getCoadescp() {
        return coadescp;
    }

    public void setCoadescp(String coadescp) {
        this.coadescp = coadescp;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankaccount() {
        return bankaccount;
    }

    public void setBankaccount(String bankaccount) {
        this.bankaccount = bankaccount;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getBankdesc() {
        return bankdesc;
    }

    public void setBankdesc(String bankdesc) {
        this.bankdesc = bankdesc;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Broker)) {
            return false;
        }
        Broker other = (Broker) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.configuration.Broker[ code=" + code + " ]";
    }
    
}
