/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.configuration;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "cf_taxtable")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TaxTable.findAll", query = "SELECT t FROM TaxTable t"),
    @NamedQuery(name = "TaxTable.findById", query = "SELECT t FROM TaxTable t WHERE t.id = :id"),
    @NamedQuery(name = "TaxTable.findByPstart", query = "SELECT t FROM TaxTable t WHERE t.pstart = :pstart"),
    @NamedQuery(name = "TaxTable.findByPend", query = "SELECT t FROM TaxTable t WHERE t.pend = :pend"),
    @NamedQuery(name = "TaxTable.findByM", query = "SELECT t FROM TaxTable t WHERE t.m = :m"),
    @NamedQuery(name = "TaxTable.findByR", query = "SELECT t FROM TaxTable t WHERE t.r = :r"),
    @NamedQuery(name = "TaxTable.findByB13", query = "SELECT t FROM TaxTable t WHERE t.b13 = :b13"),
    @NamedQuery(name = "TaxTable.findByB2", query = "SELECT t FROM TaxTable t WHERE t.b2 = :b2")})
public class TaxTable implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Pstart")
    private double pstart;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Pend")
    private double pend;
    @Basic(optional = false)
    @NotNull
    @Column(name = "M")
    private double m;
    @Basic(optional = false)
    @NotNull
    @Column(name = "R")
    private double r;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B13")
    private double b13;
    @Basic(optional = false)
    @NotNull
    @Column(name = "B2")
    private double b2;

    public TaxTable() {
    }

    public TaxTable(Integer id) {
        this.id = id;
    }

    public TaxTable(Integer id, double pstart, double pend, double m, double r, double b13, double b2) {
        this.id = id;
        this.pstart = pstart;
        this.pend = pend;
        this.m = m;
        this.r = r;
        this.b13 = b13;
        this.b2 = b2;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getPstart() {
        return pstart;
    }

    public void setPstart(double pstart) {
        this.pstart = pstart;
    }

    public double getPend() {
        return pend;
    }

    public void setPend(double pend) {
        this.pend = pend;
    }

    public double getM() {
        return m;
    }

    public void setM(double m) {
        this.m = m;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public double getB13() {
        return b13;
    }

    public void setB13(double b13) {
        this.b13 = b13;
    }

    public double getB2() {
        return b2;
    }

    public void setB2(double b2) {
        this.b2 = b2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TaxTable)) {
            return false;
        }
        TaxTable other = (TaxTable) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.configuration.TaxTable[ id=" + id + " ]";
    }
    
}
