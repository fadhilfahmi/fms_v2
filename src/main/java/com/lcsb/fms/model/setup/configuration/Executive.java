/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.configuration;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "executive")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Executive.findAll", query = "SELECT e FROM Executive e"),
    @NamedQuery(name = "Executive.findByMgtcode", query = "SELECT e FROM Executive e WHERE e.mgtcode = :mgtcode"),
    @NamedQuery(name = "Executive.findByContractcode", query = "SELECT e FROM Executive e WHERE e.contractcode = :contractcode"),
    @NamedQuery(name = "Executive.findByFlagcontract", query = "SELECT e FROM Executive e WHERE e.flagcontract = :flagcontract"),
    @NamedQuery(name = "Executive.findById", query = "SELECT e FROM Executive e WHERE e.id = :id"),
    @NamedQuery(name = "Executive.findByName", query = "SELECT e FROM Executive e WHERE e.name = :name"),
    @NamedQuery(name = "Executive.findByStatus", query = "SELECT e FROM Executive e WHERE e.status = :status"),
    @NamedQuery(name = "Executive.findByAlias", query = "SELECT e FROM Executive e WHERE e.alias = :alias"),
    @NamedQuery(name = "Executive.findByOldic", query = "SELECT e FROM Executive e WHERE e.oldic = :oldic"),
    @NamedQuery(name = "Executive.findByNewic", query = "SELECT e FROM Executive e WHERE e.newic = :newic"),
    @NamedQuery(name = "Executive.findByPassport", query = "SELECT e FROM Executive e WHERE e.passport = :passport"),
    @NamedQuery(name = "Executive.findByBirth", query = "SELECT e FROM Executive e WHERE e.birth = :birth"),
    @NamedQuery(name = "Executive.findBySex", query = "SELECT e FROM Executive e WHERE e.sex = :sex"),
    @NamedQuery(name = "Executive.findByMarital", query = "SELECT e FROM Executive e WHERE e.marital = :marital"),
    @NamedQuery(name = "Executive.findByReligion", query = "SELECT e FROM Executive e WHERE e.religion = :religion"),
    @NamedQuery(name = "Executive.findByCitizen", query = "SELECT e FROM Executive e WHERE e.citizen = :citizen"),
    @NamedQuery(name = "Executive.findByResident", query = "SELECT e FROM Executive e WHERE e.resident = :resident"),
    @NamedQuery(name = "Executive.findByBlood", query = "SELECT e FROM Executive e WHERE e.blood = :blood"),
    @NamedQuery(name = "Executive.findByAddress", query = "SELECT e FROM Executive e WHERE e.address = :address"),
    @NamedQuery(name = "Executive.findByCity", query = "SELECT e FROM Executive e WHERE e.city = :city"),
    @NamedQuery(name = "Executive.findByState", query = "SELECT e FROM Executive e WHERE e.state = :state"),
    @NamedQuery(name = "Executive.findByPostcode", query = "SELECT e FROM Executive e WHERE e.postcode = :postcode"),
    @NamedQuery(name = "Executive.findByPhone", query = "SELECT e FROM Executive e WHERE e.phone = :phone"),
    @NamedQuery(name = "Executive.findByHp", query = "SELECT e FROM Executive e WHERE e.hp = :hp"),
    @NamedQuery(name = "Executive.findByFax", query = "SELECT e FROM Executive e WHERE e.fax = :fax"),
    @NamedQuery(name = "Executive.findByEmail", query = "SELECT e FROM Executive e WHERE e.email = :email"),
    @NamedQuery(name = "Executive.findByEcname", query = "SELECT e FROM Executive e WHERE e.ecname = :ecname"),
    @NamedQuery(name = "Executive.findByEcrelationship", query = "SELECT e FROM Executive e WHERE e.ecrelationship = :ecrelationship"),
    @NamedQuery(name = "Executive.findByEcaddress", query = "SELECT e FROM Executive e WHERE e.ecaddress = :ecaddress"),
    @NamedQuery(name = "Executive.findByEccity", query = "SELECT e FROM Executive e WHERE e.eccity = :eccity"),
    @NamedQuery(name = "Executive.findByEcstate", query = "SELECT e FROM Executive e WHERE e.ecstate = :ecstate"),
    @NamedQuery(name = "Executive.findByEcpostcode", query = "SELECT e FROM Executive e WHERE e.ecpostcode = :ecpostcode"),
    @NamedQuery(name = "Executive.findByEchomephone", query = "SELECT e FROM Executive e WHERE e.echomephone = :echomephone"),
    @NamedQuery(name = "Executive.findByEcofficephone", query = "SELECT e FROM Executive e WHERE e.ecofficephone = :ecofficephone"),
    @NamedQuery(name = "Executive.findByEchp", query = "SELECT e FROM Executive e WHERE e.echp = :echp"),
    @NamedQuery(name = "Executive.findByEcfax", query = "SELECT e FROM Executive e WHERE e.ecfax = :ecfax"),
    @NamedQuery(name = "Executive.findByEcemail", query = "SELECT e FROM Executive e WHERE e.ecemail = :ecemail"),
    @NamedQuery(name = "Executive.findByDatejoin", query = "SELECT e FROM Executive e WHERE e.datejoin = :datejoin"),
    @NamedQuery(name = "Executive.findByPposition", query = "SELECT e FROM Executive e WHERE e.pposition = :pposition"),
    @NamedQuery(name = "Executive.findByAposition", query = "SELECT e FROM Executive e WHERE e.aposition = :aposition"),
    @NamedQuery(name = "Executive.findByEpf", query = "SELECT e FROM Executive e WHERE e.epf = :epf"),
    @NamedQuery(name = "Executive.findBySosco", query = "SELECT e FROM Executive e WHERE e.sosco = :sosco"),
    @NamedQuery(name = "Executive.findByTax", query = "SELECT e FROM Executive e WHERE e.tax = :tax"),
    @NamedQuery(name = "Executive.findByBank", query = "SELECT e FROM Executive e WHERE e.bank = :bank"),
    @NamedQuery(name = "Executive.findByAccountbank", query = "SELECT e FROM Executive e WHERE e.accountbank = :accountbank"),
    @NamedQuery(name = "Executive.findByPension", query = "SELECT e FROM Executive e WHERE e.pension = :pension"),
    @NamedQuery(name = "Executive.findByVehicle", query = "SELECT e FROM Executive e WHERE e.vehicle = :vehicle"),
    @NamedQuery(name = "Executive.findByAccommodation", query = "SELECT e FROM Executive e WHERE e.accommodation = :accommodation"),
    @NamedQuery(name = "Executive.findByRemarks", query = "SELECT e FROM Executive e WHERE e.remarks = :remarks"),
    @NamedQuery(name = "Executive.findByRace", query = "SELECT e FROM Executive e WHERE e.race = :race"),
    @NamedQuery(name = "Executive.findByFlag", query = "SELECT e FROM Executive e WHERE e.flag = :flag"),
    @NamedQuery(name = "Executive.findByCategory", query = "SELECT e FROM Executive e WHERE e.category = :category"),
    @NamedQuery(name = "Executive.findByPassportexpired", query = "SELECT e FROM Executive e WHERE e.passportexpired = :passportexpired"),
    @NamedQuery(name = "Executive.findByPermit", query = "SELECT e FROM Executive e WHERE e.permit = :permit"),
    @NamedQuery(name = "Executive.findByPermitexpired", query = "SELECT e FROM Executive e WHERE e.permitexpired = :permitexpired"),
    @NamedQuery(name = "Executive.findBySalary", query = "SELECT e FROM Executive e WHERE e.salary = :salary"),
    @NamedQuery(name = "Executive.findByBasicsalarytype", query = "SELECT e FROM Executive e WHERE e.basicsalarytype = :basicsalarytype"),
    @NamedQuery(name = "Executive.findByBasic", query = "SELECT e FROM Executive e WHERE e.basic = :basic"),
    @NamedQuery(name = "Executive.findByPayment", query = "SELECT e FROM Executive e WHERE e.payment = :payment"),
    @NamedQuery(name = "Executive.findByDatejoincorp", query = "SELECT e FROM Executive e WHERE e.datejoincorp = :datejoincorp"),
    @NamedQuery(name = "Executive.findByDatejoinlkpp", query = "SELECT e FROM Executive e WHERE e.datejoinlkpp = :datejoinlkpp"),
    @NamedQuery(name = "Executive.findByWorkgred", query = "SELECT e FROM Executive e WHERE e.workgred = :workgred"),
    @NamedQuery(name = "Executive.findByVehicleent", query = "SELECT e FROM Executive e WHERE e.vehicleent = :vehicleent"),
    @NamedQuery(name = "Executive.findByAccoent", query = "SELECT e FROM Executive e WHERE e.accoent = :accoent"),
    @NamedQuery(name = "Executive.findByAcallow", query = "SELECT e FROM Executive e WHERE e.acallow = :acallow"),
    @NamedQuery(name = "Executive.findByBankbranch", query = "SELECT e FROM Executive e WHERE e.bankbranch = :bankbranch"),
    @NamedQuery(name = "Executive.findByCvoucher", query = "SELECT e FROM Executive e WHERE e.cvoucher = :cvoucher"),
    @NamedQuery(name = "Executive.findByPvoucher", query = "SELECT e FROM Executive e WHERE e.pvoucher = :pvoucher"),
    @NamedQuery(name = "Executive.findByAcheque", query = "SELECT e FROM Executive e WHERE e.acheque = :acheque"),
    @NamedQuery(name = "Executive.findByWorkertype", query = "SELECT e FROM Executive e WHERE e.workertype = :workertype"),
    @NamedQuery(name = "Executive.findByDepartment", query = "SELECT e FROM Executive e WHERE e.department = :department"),
    @NamedQuery(name = "Executive.findByEpftype", query = "SELECT e FROM Executive e WHERE e.epftype = :epftype"),
    @NamedQuery(name = "Executive.findBySocsocategory", query = "SELECT e FROM Executive e WHERE e.socsocategory = :socsocategory"),
    @NamedQuery(name = "Executive.findByNormalrate", query = "SELECT e FROM Executive e WHERE e.normalrate = :normalrate"),
    @NamedQuery(name = "Executive.findByRestrate", query = "SELECT e FROM Executive e WHERE e.restrate = :restrate"),
    @NamedQuery(name = "Executive.findByJobspec", query = "SELECT e FROM Executive e WHERE e.jobspec = :jobspec"),
    @NamedQuery(name = "Executive.findByPublicrate", query = "SELECT e FROM Executive e WHERE e.publicrate = :publicrate"),
    @NamedQuery(name = "Executive.findByTitle", query = "SELECT e FROM Executive e WHERE e.title = :title")})
public class Executive implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "mgtcode")
    private String mgtcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "contractcode")
    private String contractcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "flagcontract")
    private String flagcontract;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "id")
    private String id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "alias")
    private String alias;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "oldic")
    private String oldic;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "newic")
    private String newic;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "passport")
    private String passport;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "birth")
    private String birth;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "sex")
    private String sex;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "marital")
    private String marital;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "religion")
    private String religion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "citizen")
    private String citizen;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "resident")
    private String resident;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "blood")
    private String blood;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "state")
    private String state;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postcode")
    private String postcode;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "phone")
    private String phone;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "hp")
    private String hp;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "fax")
    private String fax;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ecname")
    private String ecname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ecrelationship")
    private String ecrelationship;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ecaddress")
    private String ecaddress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "eccity")
    private String eccity;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ecstate")
    private String ecstate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ecpostcode")
    private String ecpostcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "echomephone")
    private String echomephone;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ecofficephone")
    private String ecofficephone;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "echp")
    private String echp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ecfax")
    private String ecfax;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ecemail")
    private String ecemail;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "datejoin")
    private String datejoin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pposition")
    private String pposition;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "aposition")
    private String aposition;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "epf")
    private String epf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "sosco")
    private String sosco;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "tax")
    private String tax;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bank")
    private String bank;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "accountbank")
    private String accountbank;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pension")
    private String pension;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "vehicle")
    private String vehicle;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "accommodation")
    private String accommodation;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "race")
    private String race;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "flag")
    private String flag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "category")
    private String category;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "passportexpired")
    private String passportexpired;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "permit")
    private String permit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "permitexpired")
    private String permitexpired;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "salary")
    private String salary;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "basicsalarytype")
    private String basicsalarytype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "basic")
    private String basic;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "payment")
    private String payment;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "datejoincorp")
    private String datejoincorp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "datejoinlkpp")
    private String datejoinlkpp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "workgred")
    private String workgred;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "vehicleent")
    private String vehicleent;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "accoent")
    private String accoent;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "acallow")
    private String acallow;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bankbranch")
    private String bankbranch;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cvoucher")
    private String cvoucher;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pvoucher")
    private String pvoucher;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "acheque")
    private String acheque;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "workertype")
    private String workertype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "department")
    private String department;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "epftype")
    private String epftype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "socsocategory")
    private String socsocategory;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "normalrate")
    private String normalrate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "restrate")
    private String restrate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "jobspec")
    private String jobspec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "publicrate")
    private String publicrate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "title")
    private String title;

    public Executive() {
    }

    public Executive(String id) {
        this.id = id;
    }

    public Executive(String id, String mgtcode, String contractcode, String flagcontract, String name, String status, String alias, String oldic, String newic, String passport, String birth, String sex, String marital, String religion, String citizen, String resident, String blood, String address, String city, String state, String postcode, String phone, String hp, String fax, String email, String ecname, String ecrelationship, String ecaddress, String eccity, String ecstate, String ecpostcode, String echomephone, String ecofficephone, String echp, String ecfax, String ecemail, String datejoin, String pposition, String aposition, String epf, String sosco, String tax, String bank, String accountbank, String pension, String vehicle, String accommodation, String remarks, String race, String flag, String category, String passportexpired, String permit, String permitexpired, String salary, String basicsalarytype, String basic, String payment, String datejoincorp, String datejoinlkpp, String workgred, String vehicleent, String accoent, String acallow, String bankbranch, String cvoucher, String pvoucher, String acheque, String workertype, String department, String epftype, String socsocategory, String normalrate, String restrate, String jobspec, String publicrate, String title) {
        this.id = id;
        this.mgtcode = mgtcode;
        this.contractcode = contractcode;
        this.flagcontract = flagcontract;
        this.name = name;
        this.status = status;
        this.alias = alias;
        this.oldic = oldic;
        this.newic = newic;
        this.passport = passport;
        this.birth = birth;
        this.sex = sex;
        this.marital = marital;
        this.religion = religion;
        this.citizen = citizen;
        this.resident = resident;
        this.blood = blood;
        this.address = address;
        this.city = city;
        this.state = state;
        this.postcode = postcode;
        this.phone = phone;
        this.hp = hp;
        this.fax = fax;
        this.email = email;
        this.ecname = ecname;
        this.ecrelationship = ecrelationship;
        this.ecaddress = ecaddress;
        this.eccity = eccity;
        this.ecstate = ecstate;
        this.ecpostcode = ecpostcode;
        this.echomephone = echomephone;
        this.ecofficephone = ecofficephone;
        this.echp = echp;
        this.ecfax = ecfax;
        this.ecemail = ecemail;
        this.datejoin = datejoin;
        this.pposition = pposition;
        this.aposition = aposition;
        this.epf = epf;
        this.sosco = sosco;
        this.tax = tax;
        this.bank = bank;
        this.accountbank = accountbank;
        this.pension = pension;
        this.vehicle = vehicle;
        this.accommodation = accommodation;
        this.remarks = remarks;
        this.race = race;
        this.flag = flag;
        this.category = category;
        this.passportexpired = passportexpired;
        this.permit = permit;
        this.permitexpired = permitexpired;
        this.salary = salary;
        this.basicsalarytype = basicsalarytype;
        this.basic = basic;
        this.payment = payment;
        this.datejoincorp = datejoincorp;
        this.datejoinlkpp = datejoinlkpp;
        this.workgred = workgred;
        this.vehicleent = vehicleent;
        this.accoent = accoent;
        this.acallow = acallow;
        this.bankbranch = bankbranch;
        this.cvoucher = cvoucher;
        this.pvoucher = pvoucher;
        this.acheque = acheque;
        this.workertype = workertype;
        this.department = department;
        this.epftype = epftype;
        this.socsocategory = socsocategory;
        this.normalrate = normalrate;
        this.restrate = restrate;
        this.jobspec = jobspec;
        this.publicrate = publicrate;
        this.title = title;
    }

    public String getMgtcode() {
        return mgtcode;
    }

    public void setMgtcode(String mgtcode) {
        this.mgtcode = mgtcode;
    }

    public String getContractcode() {
        return contractcode;
    }

    public void setContractcode(String contractcode) {
        this.contractcode = contractcode;
    }

    public String getFlagcontract() {
        return flagcontract;
    }

    public void setFlagcontract(String flagcontract) {
        this.flagcontract = flagcontract;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getOldic() {
        return oldic;
    }

    public void setOldic(String oldic) {
        this.oldic = oldic;
    }

    public String getNewic() {
        return newic;
    }

    public void setNewic(String newic) {
        this.newic = newic;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getMarital() {
        return marital;
    }

    public void setMarital(String marital) {
        this.marital = marital;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getCitizen() {
        return citizen;
    }

    public void setCitizen(String citizen) {
        this.citizen = citizen;
    }

    public String getResident() {
        return resident;
    }

    public void setResident(String resident) {
        this.resident = resident;
    }

    public String getBlood() {
        return blood;
    }

    public void setBlood(String blood) {
        this.blood = blood;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEcname() {
        return ecname;
    }

    public void setEcname(String ecname) {
        this.ecname = ecname;
    }

    public String getEcrelationship() {
        return ecrelationship;
    }

    public void setEcrelationship(String ecrelationship) {
        this.ecrelationship = ecrelationship;
    }

    public String getEcaddress() {
        return ecaddress;
    }

    public void setEcaddress(String ecaddress) {
        this.ecaddress = ecaddress;
    }

    public String getEccity() {
        return eccity;
    }

    public void setEccity(String eccity) {
        this.eccity = eccity;
    }

    public String getEcstate() {
        return ecstate;
    }

    public void setEcstate(String ecstate) {
        this.ecstate = ecstate;
    }

    public String getEcpostcode() {
        return ecpostcode;
    }

    public void setEcpostcode(String ecpostcode) {
        this.ecpostcode = ecpostcode;
    }

    public String getEchomephone() {
        return echomephone;
    }

    public void setEchomephone(String echomephone) {
        this.echomephone = echomephone;
    }

    public String getEcofficephone() {
        return ecofficephone;
    }

    public void setEcofficephone(String ecofficephone) {
        this.ecofficephone = ecofficephone;
    }

    public String getEchp() {
        return echp;
    }

    public void setEchp(String echp) {
        this.echp = echp;
    }

    public String getEcfax() {
        return ecfax;
    }

    public void setEcfax(String ecfax) {
        this.ecfax = ecfax;
    }

    public String getEcemail() {
        return ecemail;
    }

    public void setEcemail(String ecemail) {
        this.ecemail = ecemail;
    }

    public String getDatejoin() {
        return datejoin;
    }

    public void setDatejoin(String datejoin) {
        this.datejoin = datejoin;
    }

    public String getPposition() {
        return pposition;
    }

    public void setPposition(String pposition) {
        this.pposition = pposition;
    }

    public String getAposition() {
        return aposition;
    }

    public void setAposition(String aposition) {
        this.aposition = aposition;
    }

    public String getEpf() {
        return epf;
    }

    public void setEpf(String epf) {
        this.epf = epf;
    }

    public String getSosco() {
        return sosco;
    }

    public void setSosco(String sosco) {
        this.sosco = sosco;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getAccountbank() {
        return accountbank;
    }

    public void setAccountbank(String accountbank) {
        this.accountbank = accountbank;
    }

    public String getPension() {
        return pension;
    }

    public void setPension(String pension) {
        this.pension = pension;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getAccommodation() {
        return accommodation;
    }

    public void setAccommodation(String accommodation) {
        this.accommodation = accommodation;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPassportexpired() {
        return passportexpired;
    }

    public void setPassportexpired(String passportexpired) {
        this.passportexpired = passportexpired;
    }

    public String getPermit() {
        return permit;
    }

    public void setPermit(String permit) {
        this.permit = permit;
    }

    public String getPermitexpired() {
        return permitexpired;
    }

    public void setPermitexpired(String permitexpired) {
        this.permitexpired = permitexpired;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getBasicsalarytype() {
        return basicsalarytype;
    }

    public void setBasicsalarytype(String basicsalarytype) {
        this.basicsalarytype = basicsalarytype;
    }

    public String getBasic() {
        return basic;
    }

    public void setBasic(String basic) {
        this.basic = basic;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getDatejoincorp() {
        return datejoincorp;
    }

    public void setDatejoincorp(String datejoincorp) {
        this.datejoincorp = datejoincorp;
    }

    public String getDatejoinlkpp() {
        return datejoinlkpp;
    }

    public void setDatejoinlkpp(String datejoinlkpp) {
        this.datejoinlkpp = datejoinlkpp;
    }

    public String getWorkgred() {
        return workgred;
    }

    public void setWorkgred(String workgred) {
        this.workgred = workgred;
    }

    public String getVehicleent() {
        return vehicleent;
    }

    public void setVehicleent(String vehicleent) {
        this.vehicleent = vehicleent;
    }

    public String getAccoent() {
        return accoent;
    }

    public void setAccoent(String accoent) {
        this.accoent = accoent;
    }

    public String getAcallow() {
        return acallow;
    }

    public void setAcallow(String acallow) {
        this.acallow = acallow;
    }

    public String getBankbranch() {
        return bankbranch;
    }

    public void setBankbranch(String bankbranch) {
        this.bankbranch = bankbranch;
    }

    public String getCvoucher() {
        return cvoucher;
    }

    public void setCvoucher(String cvoucher) {
        this.cvoucher = cvoucher;
    }

    public String getPvoucher() {
        return pvoucher;
    }

    public void setPvoucher(String pvoucher) {
        this.pvoucher = pvoucher;
    }

    public String getAcheque() {
        return acheque;
    }

    public void setAcheque(String acheque) {
        this.acheque = acheque;
    }

    public String getWorkertype() {
        return workertype;
    }

    public void setWorkertype(String workertype) {
        this.workertype = workertype;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getEpftype() {
        return epftype;
    }

    public void setEpftype(String epftype) {
        this.epftype = epftype;
    }

    public String getSocsocategory() {
        return socsocategory;
    }

    public void setSocsocategory(String socsocategory) {
        this.socsocategory = socsocategory;
    }

    public String getNormalrate() {
        return normalrate;
    }

    public void setNormalrate(String normalrate) {
        this.normalrate = normalrate;
    }

    public String getRestrate() {
        return restrate;
    }

    public void setRestrate(String restrate) {
        this.restrate = restrate;
    }

    public String getJobspec() {
        return jobspec;
    }

    public void setJobspec(String jobspec) {
        this.jobspec = jobspec;
    }

    public String getPublicrate() {
        return publicrate;
    }

    public void setPublicrate(String publicrate) {
        this.publicrate = publicrate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Executive)) {
            return false;
        }
        Executive other = (Executive) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.configuration.Executive[ id=" + id + " ]";
    }
    
}
