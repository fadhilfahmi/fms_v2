/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.configuration;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "managementinfo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Management.findAll", query = "SELECT m FROM Management m"),
    @NamedQuery(name = "Management.findByCode", query = "SELECT m FROM Management m WHERE m.code = :code"),
    @NamedQuery(name = "Management.findByDescp", query = "SELECT m FROM Management m WHERE m.descp = :descp"),
    @NamedQuery(name = "Management.findByCoregister", query = "SELECT m FROM Management m WHERE m.coregister = :coregister"),
    @NamedQuery(name = "Management.findByEmployeetax", query = "SELECT m FROM Management m WHERE m.employeetax = :employeetax"),
    @NamedQuery(name = "Management.findByPostcode", query = "SELECT m FROM Management m WHERE m.postcode = :postcode"),
    @NamedQuery(name = "Management.findByCity", query = "SELECT m FROM Management m WHERE m.city = :city"),
    @NamedQuery(name = "Management.findByState", query = "SELECT m FROM Management m WHERE m.state = :state"),
    @NamedQuery(name = "Management.findByPhone", query = "SELECT m FROM Management m WHERE m.phone = :phone"),
    @NamedQuery(name = "Management.findByFax", query = "SELECT m FROM Management m WHERE m.fax = :fax"),
    @NamedQuery(name = "Management.findByEmail", query = "SELECT m FROM Management m WHERE m.email = :email"),
    @NamedQuery(name = "Management.findByWebsite", query = "SELECT m FROM Management m WHERE m.website = :website"),
    @NamedQuery(name = "Management.findByMgtcode1", query = "SELECT m FROM Management m WHERE m.mgtcode1 = :mgtcode1"),
    @NamedQuery(name = "Management.findByFlag1", query = "SELECT m FROM Management m WHERE m.flag1 = :flag1"),
    @NamedQuery(name = "Management.findByMgtcode2", query = "SELECT m FROM Management m WHERE m.mgtcode2 = :mgtcode2"),
    @NamedQuery(name = "Management.findByFlag2", query = "SELECT m FROM Management m WHERE m.flag2 = :flag2"),
    @NamedQuery(name = "Management.findByMgtdesc1", query = "SELECT m FROM Management m WHERE m.mgtdesc1 = :mgtdesc1"),
    @NamedQuery(name = "Management.findByMgtdesc2", query = "SELECT m FROM Management m WHERE m.mgtdesc2 = :mgtdesc2")})
public class Management implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "descp")
    private String descp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coregister")
    private String coregister;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "employeetax")
    private String employeetax;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postcode")
    private String postcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "state")
    private String state;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "phone")
    private String phone;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "fax")
    private String fax;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "website")
    private String website;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "mgtcode1")
    private String mgtcode1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "flag1")
    private String flag1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "mgtcode2")
    private String mgtcode2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "flag2")
    private String flag2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "mgtdesc1")
    private String mgtdesc1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "mgtdesc2")
    private String mgtdesc2;

    public Management() {
    }

    public Management(String code) {
        this.code = code;
    }

    public Management(String code, String descp, String coregister, String employeetax, String address, String postcode, String city, String state, String phone, String fax, String email, String website, String mgtcode1, String flag1, String mgtcode2, String flag2, String mgtdesc1, String mgtdesc2) {
        this.code = code;
        this.descp = descp;
        this.coregister = coregister;
        this.employeetax = employeetax;
        this.address = address;
        this.postcode = postcode;
        this.city = city;
        this.state = state;
        this.phone = phone;
        this.fax = fax;
        this.email = email;
        this.website = website;
        this.mgtcode1 = mgtcode1;
        this.flag1 = flag1;
        this.mgtcode2 = mgtcode2;
        this.flag2 = flag2;
        this.mgtdesc1 = mgtdesc1;
        this.mgtdesc2 = mgtdesc2;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public String getCoregister() {
        return coregister;
    }

    public void setCoregister(String coregister) {
        this.coregister = coregister;
    }

    public String getEmployeetax() {
        return employeetax;
    }

    public void setEmployeetax(String employeetax) {
        this.employeetax = employeetax;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getMgtcode1() {
        return mgtcode1;
    }

    public void setMgtcode1(String mgtcode1) {
        this.mgtcode1 = mgtcode1;
    }

    public String getFlag1() {
        return flag1;
    }

    public void setFlag1(String flag1) {
        this.flag1 = flag1;
    }

    public String getMgtcode2() {
        return mgtcode2;
    }

    public void setMgtcode2(String mgtcode2) {
        this.mgtcode2 = mgtcode2;
    }

    public String getFlag2() {
        return flag2;
    }

    public void setFlag2(String flag2) {
        this.flag2 = flag2;
    }

    public String getMgtdesc1() {
        return mgtdesc1;
    }

    public void setMgtdesc1(String mgtdesc1) {
        this.mgtdesc1 = mgtdesc1;
    }

    public String getMgtdesc2() {
        return mgtdesc2;
    }

    public void setMgtdesc2(String mgtdesc2) {
        this.mgtdesc2 = mgtdesc2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Management)) {
            return false;
        }
        Management other = (Management) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.configuration.Management[ code=" + code + " ]";
    }
    
}
