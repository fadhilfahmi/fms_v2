/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.company.staff;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "co_staff_designation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StaffDesignation.findAll", query = "SELECT s FROM StaffDesignation s"),
    @NamedQuery(name = "StaffDesignation.findById", query = "SELECT s FROM StaffDesignation s WHERE s.id = :id"),
    @NamedQuery(name = "StaffDesignation.findByStaffid", query = "SELECT s FROM StaffDesignation s WHERE s.staffid = :staffid"),
    @NamedQuery(name = "StaffDesignation.findByDeptCode", query = "SELECT s FROM StaffDesignation s WHERE s.deptCode = :deptCode"),
    @NamedQuery(name = "StaffDesignation.findByDeptDescp", query = "SELECT s FROM StaffDesignation s WHERE s.deptDescp = :deptDescp"),
    @NamedQuery(name = "StaffDesignation.findByDatejoin", query = "SELECT s FROM StaffDesignation s WHERE s.datejoin = :datejoin"),
    @NamedQuery(name = "StaffDesignation.findByDateend", query = "SELECT s FROM StaffDesignation s WHERE s.dateend = :dateend"),
    @NamedQuery(name = "StaffDesignation.findByStatus", query = "SELECT s FROM StaffDesignation s WHERE s.status = :status"),
    @NamedQuery(name = "StaffDesignation.findByDesignation", query = "SELECT s FROM StaffDesignation s WHERE s.designation = :designation")})
public class StaffDesignation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "staffid")
    private String staffid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "dept_code")
    private String deptCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "dept_descp")
    private String deptDescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "datejoin")
    private String datejoin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "dateend")
    private String dateend;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "designation")
    private String designation;
    private String worklocation;

    public StaffDesignation() {
    }

    public StaffDesignation(Integer id) {
        this.id = id;
    }

    public StaffDesignation(Integer id, String staffid, String deptCode, String deptDescp, String datejoin, String dateend, String status, String designation, String worklocation) {
        this.id = id;
        this.staffid = staffid;
        this.deptCode = deptCode;
        this.deptDescp = deptDescp;
        this.datejoin = datejoin;
        this.dateend = dateend;
        this.status = status;
        this.designation = designation;
        this.worklocation = worklocation;
    }

    public String getWorklocation() {
        return worklocation;
    }

    public void setWorklocation(String worklocation) {
        this.worklocation = worklocation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptDescp() {
        return deptDescp;
    }

    public void setDeptDescp(String deptDescp) {
        this.deptDescp = deptDescp;
    }

    public String getDatejoin() {
        return datejoin;
    }

    public void setDatejoin(String datejoin) {
        this.datejoin = datejoin;
    }

    public String getDateend() {
        return dateend;
    }

    public void setDateend(String dateend) {
        this.dateend = dateend;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StaffDesignation)) {
            return false;
        }
        StaffDesignation other = (StaffDesignation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.company.staff.StaffDesignation[ id=" + id + " ]";
    }
    
}
