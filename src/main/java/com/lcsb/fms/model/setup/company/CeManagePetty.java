/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.company;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ce_manage_petty")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CeManagePetty.findAll", query = "SELECT c FROM CeManagePetty c"),
    @NamedQuery(name = "CeManagePetty.findByMaxfloat", query = "SELECT c FROM CeManagePetty c WHERE c.maxfloat = :maxfloat"),
    @NamedQuery(name = "CeManagePetty.findByMaxcash", query = "SELECT c FROM CeManagePetty c WHERE c.maxcash = :maxcash"),
    @NamedQuery(name = "CeManagePetty.findByCoacode", query = "SELECT c FROM CeManagePetty c WHERE c.coacode = :coacode"),
    @NamedQuery(name = "CeManagePetty.findByCoadesc", query = "SELECT c FROM CeManagePetty c WHERE c.coadesc = :coadesc"),
    @NamedQuery(name = "CeManagePetty.findByManagecode", query = "SELECT c FROM CeManagePetty c WHERE c.managecode = :managecode"),
    @NamedQuery(name = "CeManagePetty.findByCode", query = "SELECT c FROM CeManagePetty c WHERE c.code = :code"),
    @NamedQuery(name = "CeManagePetty.findByDescp", query = "SELECT c FROM CeManagePetty c WHERE c.descp = :descp")})
public class CeManagePetty implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "maxfloat")
    private double maxfloat;
    @Basic(optional = false)
    @NotNull
    @Column(name = "maxcash")
    private double maxcash;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coacode")
    private String coacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coadesc")
    private String coadesc;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "managecode")
    private String managecode;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "descp")
    private String descp;

    public CeManagePetty() {
    }

    public CeManagePetty(String code) {
        this.code = code;
    }

    public CeManagePetty(String code, double maxfloat, double maxcash, String coacode, String coadesc, String remarks, String managecode, String descp) {
        this.code = code;
        this.maxfloat = maxfloat;
        this.maxcash = maxcash;
        this.coacode = coacode;
        this.coadesc = coadesc;
        this.remarks = remarks;
        this.managecode = managecode;
        this.descp = descp;
    }

    public double getMaxfloat() {
        return maxfloat;
    }

    public void setMaxfloat(double maxfloat) {
        this.maxfloat = maxfloat;
    }

    public double getMaxcash() {
        return maxcash;
    }

    public void setMaxcash(double maxcash) {
        this.maxcash = maxcash;
    }

    public String getCoacode() {
        return coacode;
    }

    public void setCoacode(String coacode) {
        this.coacode = coacode;
    }

    public String getCoadesc() {
        return coadesc;
    }

    public void setCoadesc(String coadesc) {
        this.coadesc = coadesc;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getManagecode() {
        return managecode;
    }

    public void setManagecode(String managecode) {
        this.managecode = managecode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CeManagePetty)) {
            return false;
        }
        CeManagePetty other = (CeManagePetty) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.company.CeManagePetty[ code=" + code + " ]";
    }
    
}
