/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.configuration;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "cf_earninginfo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Earning.findAll", query = "SELECT e FROM Earning e"),
    @NamedQuery(name = "Earning.findByCode", query = "SELECT e FROM Earning e WHERE e.code = :code"),
    @NamedQuery(name = "Earning.findByDescp", query = "SELECT e FROM Earning e WHERE e.descp = :descp"),
    @NamedQuery(name = "Earning.findByStatus", query = "SELECT e FROM Earning e WHERE e.status = :status"),
    @NamedQuery(name = "Earning.findByPortion", query = "SELECT e FROM Earning e WHERE e.portion = :portion"),
    @NamedQuery(name = "Earning.findByType", query = "SELECT e FROM Earning e WHERE e.type = :type")})
public class Earning implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "code")
    private String code;
    @Size(max = 100)
    @Column(name = "descp")
    private String descp;
    @Size(max = 10)
    @Column(name = "status")
    private String status;
    @Size(max = 10)
    @Column(name = "portion")
    private String portion;
    @Size(max = 20)
    @Column(name = "type")
    private String type;

    public Earning() {
    }

    public Earning(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPortion() {
        return portion;
    }

    public void setPortion(String portion) {
        this.portion = portion;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Earning)) {
            return false;
        }
        Earning other = (Earning) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.configuration.Earning[ code=" + code + " ]";
    }
    
}
