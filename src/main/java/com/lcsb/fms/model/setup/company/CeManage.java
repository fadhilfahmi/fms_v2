/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.setup.company;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ce_manage")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Management.findAll", query = "SELECT m FROM Management m"),
    @NamedQuery(name = "Management.findByCode", query = "SELECT m FROM Management m WHERE m.code = :code"),
    @NamedQuery(name = "Management.findByName", query = "SELECT m FROM Management m WHERE m.name = :name"),
    @NamedQuery(name = "Management.findByMaksimum", query = "SELECT m FROM Management m WHERE m.maksimum = :maksimum"),
    @NamedQuery(name = "Management.findByCashbill", query = "SELECT m FROM Management m WHERE m.cashbill = :cashbill"),
    @NamedQuery(name = "Management.findByCoa", query = "SELECT m FROM Management m WHERE m.coa = :coa"),
    @NamedQuery(name = "Management.findByCoadescp", query = "SELECT m FROM Management m WHERE m.coadescp = :coadescp"),
    @NamedQuery(name = "Management.findByRemarks", query = "SELECT m FROM Management m WHERE m.remarks = :remarks"),
    @NamedQuery(name = "Management.findByBcac", query = "SELECT m FROM Management m WHERE m.bcac = :bcac"),
    @NamedQuery(name = "Management.findByBacd", query = "SELECT m FROM Management m WHERE m.bacd = :bacd"),
    @NamedQuery(name = "Management.findByHac", query = "SELECT m FROM Management m WHERE m.hac = :hac"),
    @NamedQuery(name = "Management.findByHad", query = "SELECT m FROM Management m WHERE m.had = :had")})
public class CeManage implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "maksimum")
    private String maksimum;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cashbill")
    private String cashbill;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coa")
    private String coa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coadescp")
    private String coadescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bcac")
    private String bcac;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bacd")
    private String bacd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "hac")
    private String hac;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "had")
    private String had;

    public CeManage() {
    }

    public CeManage(String code) {
        this.code = code;
    }

    public CeManage(String code, String name, String maksimum, String cashbill, String coa, String coadescp, String remarks, String bcac, String bacd, String hac, String had) {
        this.code = code;
        this.name = name;
        this.maksimum = maksimum;
        this.cashbill = cashbill;
        this.coa = coa;
        this.coadescp = coadescp;
        this.remarks = remarks;
        this.bcac = bcac;
        this.bacd = bacd;
        this.hac = hac;
        this.had = had;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMaksimum() {
        return maksimum;
    }

    public void setMaksimum(String maksimum) {
        this.maksimum = maksimum;
    }

    public String getCashbill() {
        return cashbill;
    }

    public void setCashbill(String cashbill) {
        this.cashbill = cashbill;
    }

    public String getCoa() {
        return coa;
    }

    public void setCoa(String coa) {
        this.coa = coa;
    }

    public String getCoadescp() {
        return coadescp;
    }

    public void setCoadescp(String coadescp) {
        this.coadescp = coadescp;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getBcac() {
        return bcac;
    }

    public void setBcac(String bcac) {
        this.bcac = bcac;
    }

    public String getBacd() {
        return bacd;
    }

    public void setBacd(String bacd) {
        this.bacd = bacd;
    }

    public String getHac() {
        return hac;
    }

    public void setHac(String hac) {
        this.hac = hac;
    }

    public String getHad() {
        return had;
    }

    public void setHad(String had) {
        this.had = had;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CeManage)) {
            return false;
        }
        CeManage other = (CeManage) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.setup.company.Management[ code=" + code + " ]";
    }
    
}
