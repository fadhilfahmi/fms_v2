/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.development;

import com.lcsb.fms.model.financial.gl.GeneralLedger;
import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class Unapprove {
    
    private List<GeneralLedger> listGL;
    private String checkStatus;
    private String approveStatus;
    private String postStatus;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<GeneralLedger> getListGL() {
        return listGL;
    }

    public void setListGL(List<GeneralLedger> listGL) {
        this.listGL = listGL;
    }

    public String getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(String checkStatus) {
        this.checkStatus = checkStatus;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getPostStatus() {
        return postStatus;
    }

    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }
    
    
    
}
