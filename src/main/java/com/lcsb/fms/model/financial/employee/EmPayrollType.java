/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.employee;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "em_payroll_type")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EmPayrollType.findAll", query = "SELECT e FROM EmPayrollType e"),
    @NamedQuery(name = "EmPayrollType.findById", query = "SELECT e FROM EmPayrollType e WHERE e.id = :id"),
    @NamedQuery(name = "EmPayrollType.findByRefer", query = "SELECT e FROM EmPayrollType e WHERE e.refer = :refer"),
    @NamedQuery(name = "EmPayrollType.findByEarncode", query = "SELECT e FROM EmPayrollType e WHERE e.earncode = :earncode"),
    @NamedQuery(name = "EmPayrollType.findByEarndesc", query = "SELECT e FROM EmPayrollType e WHERE e.earndesc = :earndesc"),
    @NamedQuery(name = "EmPayrollType.findByType", query = "SELECT e FROM EmPayrollType e WHERE e.type = :type"),
    @NamedQuery(name = "EmPayrollType.findByStatus", query = "SELECT e FROM EmPayrollType e WHERE e.status = :status")})
public class EmPayrollType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "earncode")
    private String earncode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "earndesc")
    private String earndesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "status")
    private String status;

    public EmPayrollType() {
    }

    public EmPayrollType(Integer id) {
        this.id = id;
    }

    public EmPayrollType(Integer id, String refer, String earncode, String earndesc, String type, String status) {
        this.id = id;
        this.refer = refer;
        this.earncode = earncode;
        this.earndesc = earndesc;
        this.type = type;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getEarncode() {
        return earncode;
    }

    public void setEarncode(String earncode) {
        this.earncode = earncode;
    }

    public String getEarndesc() {
        return earndesc;
    }

    public void setEarndesc(String earndesc) {
        this.earndesc = earndesc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmPayrollType)) {
            return false;
        }
        EmPayrollType other = (EmPayrollType) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.employee.EmPayrollType[ id=" + id + " ]";
    }
    
}
