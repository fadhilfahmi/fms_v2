/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.fms.model.financial.cashbook;

/**
 *
 * @author Dell
 */
public class PettyCash {
    
    private static String period;
    private static String year;
    private static String currentYear;
    private static String currentPeriod;
    private static String startPeriod;
    private static String endPeriod;
    private static String pettyCode;
    private String pcYear;

    public String getPcYear() {
        return pcYear;
    }

    public void setPcYear(String pcYear) {
        this.pcYear = pcYear;
    }

    public String getPcPeriod() {
        return pcPeriod;
    }

    public void setPcPeriod(String pcPeriod) {
        this.pcPeriod = pcPeriod;
    }

    public String getPcPvDate() {
        return pcPvDate;
    }

    public void setPcPvDate(String pcPvDate) {
        this.pcPvDate = pcPvDate;
    }

    public String getPcPvid() {
        return pcPvid;
    }

    public void setPcPvid(String pcPvid) {
        this.pcPvid = pcPvid;
    }

    public String getPcPvremarks() {
        return pcPvremarks;
    }

    public void setPcPvremarks(String pcPvremarks) {
        this.pcPvremarks = pcPvremarks;
    }

    public String getPvListcoa() {
        return pvListcoa;
    }

    public void setPvListcoa(String pvListcoa) {
        this.pvListcoa = pvListcoa;
    }

    public String getPcPvamount() {
        return pcPvamount;
    }

    public void setPcPvamount(String pcPvamount) {
        this.pcPvamount = pcPvamount;
    }

    public String getPvStatus() {
        return pvStatus;
    }

    public void setPvStatus(String pvStatus) {
        this.pvStatus = pvStatus;
    }
    
    private String pcPeriod;
    private String pcPvDate;
    private String pcPvid;
    private String pcPvremarks;
    private String pvListcoa;
    private String pcPvamount;
    private String pvStatus;

    public static String getPeriod() {
        return period;
    }

    public static void setPeriod(String period) {
        PettyCash.period = period;
    }

    public static String getYear() {
        return year;
    }

    public static void setYear(String year) {
        PettyCash.year = year;
    }

    public static String getCurrentYear() {
        return currentYear;
    }

    public static void setCurrentYear(String currentYear) {
        PettyCash.currentYear = currentYear;
    }

    public static String getCurrentPeriod() {
        return currentPeriod;
    }

    public static void setCurrentPeriod(String currentPeriod) {
        PettyCash.currentPeriod = currentPeriod;
    }

    public static String getStartPeriod() {
        return startPeriod;
    }

    public static void setStartPeriod(String startPeriod) {
        PettyCash.startPeriod = startPeriod;
    }

    public static String getEndPeriod() {
        return endPeriod;
    }

    public static void setEndPeriod(String endPeriod) {
        PettyCash.endPeriod = endPeriod;
    }

    public static String getPettyCode() {
        return pettyCode;
    }

    public static void setPettyCode(String pettyCode) {
        PettyCash.pettyCode = pettyCode;
    }
    
    

    
    
    
    
}
