/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.model.financial.cashbook;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dell
 */
@Entity
@Table(name = "cb_cekbook")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbCekbook.findAll", query = "SELECT c FROM CbCekbook c"),
    @NamedQuery(name = "CbCekbook.findByNo", query = "SELECT c FROM CbCekbook c WHERE c.no = :no"),
    @NamedQuery(name = "CbCekbook.findByBankcode", query = "SELECT c FROM CbCekbook c WHERE c.bankcode = :bankcode"),
    @NamedQuery(name = "CbCekbook.findByBankname", query = "SELECT c FROM CbCekbook c WHERE c.bankname = :bankname"),
    @NamedQuery(name = "CbCekbook.findByTarikh", query = "SELECT c FROM CbCekbook c WHERE c.tarikh = :tarikh"),
    @NamedQuery(name = "CbCekbook.findByStartcek", query = "SELECT c FROM CbCekbook c WHERE c.startcek = :startcek"),
    @NamedQuery(name = "CbCekbook.findByNocek", query = "SELECT c FROM CbCekbook c WHERE c.nocek = :nocek"),
    @NamedQuery(name = "CbCekbook.findByEndcek", query = "SELECT c FROM CbCekbook c WHERE c.endcek = :endcek"),
    @NamedQuery(name = "CbCekbook.findByActive", query = "SELECT c FROM CbCekbook c WHERE c.active = :active")})
public class CbCekbook implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "no")
    private String no;
    @Basic(optional = false)
    @Column(name = "bankcode")
    private String bankcode;
    @Basic(optional = false)
    @Column(name = "bankname")
    private String bankname;
    @Column(name = "tarikh")
    private String tarikh;
    @Basic(optional = false)
    @Column(name = "startcek")
    private String startcek;
    @Basic(optional = false)
    @Column(name = "nocek")
    private int nocek;
    @Basic(optional = false)
    @Column(name = "endcek")
    private String endcek;
    @Basic(optional = false)
    @Column(name = "active")
    private String active;

    public CbCekbook() {
    }

    public CbCekbook(String no) {
        this.no = no;
    }

    public CbCekbook(String no, String bankcode, String bankname, String startcek, int nocek, String endcek, String active) {
        this.no = no;
        this.bankcode = bankcode;
        this.bankname = bankname;
        this.startcek = startcek;
        this.nocek = nocek;
        this.endcek = endcek;
        this.active = active;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getTarikh() {
        return tarikh;
    }

    public void setTarikh(String tarikh) {
        this.tarikh = tarikh;
    }

    public String getStartcek() {
        return startcek;
    }

    public void setStartcek(String startcek) {
        this.startcek = startcek;
    }

    public int getNocek() {
        return nocek;
    }

    public void setNocek(int nocek) {
        this.nocek = nocek;
    }

    public String getEndcek() {
        return endcek;
    }

    public void setEndcek(String endcek) {
        this.endcek = endcek;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (no != null ? no.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbCekbook)) {
            return false;
        }
        CbCekbook other = (CbCekbook) object;
        if ((this.no == null && other.no != null) || (this.no != null && !this.no.equals(other.no))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fms.model.financial.cashbook.CbCekbook[ no=" + no + " ]";
    }
    
}
