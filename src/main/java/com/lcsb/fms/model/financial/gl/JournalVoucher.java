/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.fms.model.financial.gl;

/**
 *
 * @author Dell
 */
public class JournalVoucher {
    
    private String JVrefno;
    private String JVdate;
    private String JVno;
    private String reflexcb;
    private String reason;
    private String preparedbyid;
    private String preparedbyname;
    private String preparedate;
    private String year;
    private String curperiod;
    private String estatecode;
    private String estatename;
    private String todebit;
    private String tocredit;
    private String checkbyid;
    private String checkbyname;
    private String approvebyid;
    private String approvebyname;
    private String approvebydesig;
    private String checkdate;
    private String approvedate;
    private String postflag;
    private String postdate;
    private String JVtype;

    public String getJVtype() {
        return JVtype;
    }

    public void setJVtype(String JVtype) {
        this.JVtype = JVtype;
    }

    public String getCheckbyid() {
        return checkbyid;
    }

    public void setCheckbyid(String checkbyid) {
        this.checkbyid = checkbyid;
    }

    public String getCheckbyname() {
        return checkbyname;
    }

    public void setCheckbyname(String checkbyname) {
        this.checkbyname = checkbyname;
    }

    public String getApprovebyid() {
        return approvebyid;
    }

    public void setApprovebyid(String approvebyid) {
        this.approvebyid = approvebyid;
    }

    public String getApprovebyname() {
        return approvebyname;
    }

    public void setApprovebyname(String approvebyname) {
        this.approvebyname = approvebyname;
    }

    public String getApprovebydesig() {
        return approvebydesig;
    }

    public void setApprovebydesig(String approvebydesig) {
        this.approvebydesig = approvebydesig;
    }

    public String getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(String checkdate) {
        this.checkdate = checkdate;
    }

    public String getApprovedate() {
        return approvedate;
    }

    public void setApprovedate(String approvedate) {
        this.approvedate = approvedate;
    }

    public String getPostflag() {
        return postflag;
    }

    public void setPostflag(String postflag) {
        this.postflag = postflag;
    }

    public String getPostdate() {
        return postdate;
    }

    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }
    

    public String getReflexcb() {
        return reflexcb;
    }

    public void setReflexcb(String reflexcb) {
        this.reflexcb = reflexcb;
    }

    public String getJVrefno() {
        return JVrefno;
    }

    public void setJVrefno(String JVrefno) {
        this.JVrefno = JVrefno;
    }

    public String getJVdate() {
        return JVdate;
    }

    public void setJVdate(String JVdate) {
        this.JVdate = JVdate;
    }

    public String getJVno() {
        return JVno;
    }

    public void setJVno(String JVno) {
        this.JVno = JVno;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getPreparedbyid() {
        return preparedbyid;
    }

    public void setPreparedbyid(String preparedbyid) {
        this.preparedbyid = preparedbyid;
    }

    public String getPreparedbyname() {
        return preparedbyname;
    }

    public void setPreparedbyname(String preparedbyname) {
        this.preparedbyname = preparedbyname;
    }

    public String getPreparedate() {
        return preparedate;
    }

    public void setPreparedate(String preparedate) {
        this.preparedate = preparedate;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCurperiod() {
        return curperiod;
    }

    public void setCurperiod(String curperiod) {
        this.curperiod = curperiod;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getEstatename() {
        return estatename;
    }

    public void setEstatename(String estatename) {
        this.estatename = estatename;
    }

    public String getTodebit() {
        return todebit;
    }

    public void setTodebit(String todebit) {
        this.todebit = todebit;
    }

    public String getTocredit() {
        return tocredit;
    }

    public void setTocredit(String tocredit) {
        this.tocredit = tocredit;
    }
    
}
