/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ar;

/**
 *
 * @author fadhilfahmi
 */
public class ArFinalizeItem {
    
    private String date, refer, contractno, trailer, ivref, dispatchno, prodcode, prodname, coacode, coaname,  unitm, remarks,  loclevel,  loccode, locdesc, taxcode, taxcoacode,taxcoadescp, ivno, satype, sacode, sadesc;
    
    private double qty, unitp, amount, taxrate, mtMill, mtRefinery;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getContractno() {
        return contractno;
    }

    public void setContractno(String contractno) {
        this.contractno = contractno;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getIvref() {
        return ivref;
    }

    public void setIvref(String ivref) {
        this.ivref = ivref;
    }

    public String getDispatchno() {
        return dispatchno;
    }

    public void setDispatchno(String dispatchno) {
        this.dispatchno = dispatchno;
    }

    public String getProdcode() {
        return prodcode;
    }

    public void setProdcode(String prodcode) {
        this.prodcode = prodcode;
    }

    public String getProdname() {
        return prodname;
    }

    public void setProdname(String prodname) {
        this.prodname = prodname;
    }

    public String getCoacode() {
        return coacode;
    }

    public void setCoacode(String coacode) {
        this.coacode = coacode;
    }

    public String getCoaname() {
        return coaname;
    }

    public void setCoaname(String coaname) {
        this.coaname = coaname;
    }

    public String getUnitm() {
        return unitm;
    }

    public void setUnitm(String unitm) {
        this.unitm = unitm;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getLoclevel() {
        return loclevel;
    }

    public void setLoclevel(String loclevel) {
        this.loclevel = loclevel;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLocdesc() {
        return locdesc;
    }

    public void setLocdesc(String locdesc) {
        this.locdesc = locdesc;
    }

    public String getTaxcode() {
        return taxcode;
    }

    public void setTaxcode(String taxcode) {
        this.taxcode = taxcode;
    }

    public String getTaxcoacode() {
        return taxcoacode;
    }

    public void setTaxcoacode(String taxcoacode) {
        this.taxcoacode = taxcoacode;
    }

    public String getTaxcoadescp() {
        return taxcoadescp;
    }

    public void setTaxcoadescp(String taxcoadescp) {
        this.taxcoadescp = taxcoadescp;
    }

    public String getIvno() {
        return ivno;
    }

    public void setIvno(String ivno) {
        this.ivno = ivno;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }

    public double getUnitp() {
        return unitp;
    }

    public void setUnitp(double unitp) {
        this.unitp = unitp;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getTaxrate() {
        return taxrate;
    }

    public void setTaxrate(double taxrate) {
        this.taxrate = taxrate;
    }

    public double getMtMill() {
        return mtMill;
    }

    public void setMtMill(double mtMill) {
        this.mtMill = mtMill;
    }

    public double getMtRefinery() {
        return mtRefinery;
    }

    public void setMtRefinery(double mtRefinery) {
        this.mtRefinery = mtRefinery;
    }
    
    
    
}
