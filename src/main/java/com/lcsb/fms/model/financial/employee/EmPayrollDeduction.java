/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.employee;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "em_payroll_deduction")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EmPayrollDeduction.findAll", query = "SELECT e FROM EmPayrollDeduction e"),
    @NamedQuery(name = "EmPayrollDeduction.findByRefer", query = "SELECT e FROM EmPayrollDeduction e WHERE e.refer = :refer"),
    @NamedQuery(name = "EmPayrollDeduction.findByYear", query = "SELECT e FROM EmPayrollDeduction e WHERE e.year = :year"),
    @NamedQuery(name = "EmPayrollDeduction.findByPeriod", query = "SELECT e FROM EmPayrollDeduction e WHERE e.period = :period"),
    @NamedQuery(name = "EmPayrollDeduction.findByStaffid", query = "SELECT e FROM EmPayrollDeduction e WHERE e.staffid = :staffid"),
    @NamedQuery(name = "EmPayrollDeduction.findByDeductcode", query = "SELECT e FROM EmPayrollDeduction e WHERE e.deductcode = :deductcode"),
    @NamedQuery(name = "EmPayrollDeduction.findByDeductdesc", query = "SELECT e FROM EmPayrollDeduction e WHERE e.deductdesc = :deductdesc"),
    @NamedQuery(name = "EmPayrollDeduction.findByAmount", query = "SELECT e FROM EmPayrollDeduction e WHERE e.amount = :amount"),
    @NamedQuery(name = "EmPayrollDeduction.findByStaffname", query = "SELECT e FROM EmPayrollDeduction e WHERE e.staffname = :staffname"),
    @NamedQuery(name = "EmPayrollDeduction.findByEpf", query = "SELECT e FROM EmPayrollDeduction e WHERE e.epf = :epf"),
    @NamedQuery(name = "EmPayrollDeduction.findBySocso", query = "SELECT e FROM EmPayrollDeduction e WHERE e.socso = :socso"),
    @NamedQuery(name = "EmPayrollDeduction.findByBasicflag", query = "SELECT e FROM EmPayrollDeduction e WHERE e.basicflag = :basicflag"),
    @NamedQuery(name = "EmPayrollDeduction.findByLoclevel", query = "SELECT e FROM EmPayrollDeduction e WHERE e.loclevel = :loclevel"),
    @NamedQuery(name = "EmPayrollDeduction.findByDaymth", query = "SELECT e FROM EmPayrollDeduction e WHERE e.daymth = :daymth"),
    @NamedQuery(name = "EmPayrollDeduction.findByFlag", query = "SELECT e FROM EmPayrollDeduction e WHERE e.flag = :flag"),
    @NamedQuery(name = "EmPayrollDeduction.findByPosition", query = "SELECT e FROM EmPayrollDeduction e WHERE e.position = :position"),
    @NamedQuery(name = "EmPayrollDeduction.findByDeducttype", query = "SELECT e FROM EmPayrollDeduction e WHERE e.deducttype = :deducttype"),
    @NamedQuery(name = "EmPayrollDeduction.findByMinwday", query = "SELECT e FROM EmPayrollDeduction e WHERE e.minwday = :minwday"),
    @NamedQuery(name = "EmPayrollDeduction.findByEstatecode", query = "SELECT e FROM EmPayrollDeduction e WHERE e.estatecode = :estatecode"),
    @NamedQuery(name = "EmPayrollDeduction.findByEstatename", query = "SELECT e FROM EmPayrollDeduction e WHERE e.estatename = :estatename"),
    @NamedQuery(name = "EmPayrollDeduction.findByStafftype", query = "SELECT e FROM EmPayrollDeduction e WHERE e.stafftype = :stafftype"),
    @NamedQuery(name = "EmPayrollDeduction.findBySalary", query = "SELECT e FROM EmPayrollDeduction e WHERE e.salary = :salary"),
    @NamedQuery(name = "EmPayrollDeduction.findByGuna", query = "SELECT e FROM EmPayrollDeduction e WHERE e.guna = :guna"),
    @NamedQuery(name = "EmPayrollDeduction.findByIncludeinp", query = "SELECT e FROM EmPayrollDeduction e WHERE e.includeinp = :includeinp"),
    @NamedQuery(name = "EmPayrollDeduction.findById", query = "SELECT e FROM EmPayrollDeduction e WHERE e.id = :id")})
public class EmPayrollDeduction implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "period")
    private String period;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "staffid")
    private String staffid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "deductcode")
    private String deductcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "deductdesc")
    private String deductdesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "staffname")
    private String staffname;
    @Size(max = 100)
    @Column(name = "epf")
    private String epf;
    @Size(max = 100)
    @Column(name = "socso")
    private String socso;
    @Size(max = 100)
    @Column(name = "basicflag")
    private String basicflag;
    @Size(max = 100)
    @Column(name = "loclevel")
    private String loclevel;
    @Size(max = 100)
    @Column(name = "daymth")
    private String daymth;
    @Size(max = 100)
    @Column(name = "flag")
    private String flag;
    @Size(max = 100)
    @Column(name = "position")
    private String position;
    @Size(max = 100)
    @Column(name = "deducttype")
    private String deducttype;
    @Size(max = 100)
    @Column(name = "minwday")
    private String minwday;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatecode")
    private String estatecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatename")
    private String estatename;
    @Size(max = 100)
    @Column(name = "stafftype")
    private String stafftype;
    @Size(max = 100)
    @Column(name = "salary")
    private String salary;
    @Size(max = 100)
    @Column(name = "guna")
    private String guna;
    @Size(max = 100)
    @Column(name = "includeinp")
    private String includeinp;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    public EmPayrollDeduction() {
    }

    public EmPayrollDeduction(Integer id) {
        this.id = id;
    }

    public EmPayrollDeduction(Integer id, String refer, String year, String period, String staffid, String deductcode, String deductdesc, double amount, String staffname, String estatecode, String estatename) {
        this.id = id;
        this.refer = refer;
        this.year = year;
        this.period = period;
        this.staffid = staffid;
        this.deductcode = deductcode;
        this.deductdesc = deductdesc;
        this.amount = amount;
        this.staffname = staffname;
        this.estatecode = estatecode;
        this.estatename = estatename;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    public String getDeductcode() {
        return deductcode;
    }

    public void setDeductcode(String deductcode) {
        this.deductcode = deductcode;
    }

    public String getDeductdesc() {
        return deductdesc;
    }

    public void setDeductdesc(String deductdesc) {
        this.deductdesc = deductdesc;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getStaffname() {
        return staffname;
    }

    public void setStaffname(String staffname) {
        this.staffname = staffname;
    }

    public String getEpf() {
        return epf;
    }

    public void setEpf(String epf) {
        this.epf = epf;
    }

    public String getSocso() {
        return socso;
    }

    public void setSocso(String socso) {
        this.socso = socso;
    }

    public String getBasicflag() {
        return basicflag;
    }

    public void setBasicflag(String basicflag) {
        this.basicflag = basicflag;
    }

    public String getLoclevel() {
        return loclevel;
    }

    public void setLoclevel(String loclevel) {
        this.loclevel = loclevel;
    }

    public String getDaymth() {
        return daymth;
    }

    public void setDaymth(String daymth) {
        this.daymth = daymth;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDeducttype() {
        return deducttype;
    }

    public void setDeducttype(String deducttype) {
        this.deducttype = deducttype;
    }

    public String getMinwday() {
        return minwday;
    }

    public void setMinwday(String minwday) {
        this.minwday = minwday;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getEstatename() {
        return estatename;
    }

    public void setEstatename(String estatename) {
        this.estatename = estatename;
    }

    public String getStafftype() {
        return stafftype;
    }

    public void setStafftype(String stafftype) {
        this.stafftype = stafftype;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getGuna() {
        return guna;
    }

    public void setGuna(String guna) {
        this.guna = guna;
    }

    public String getIncludeinp() {
        return includeinp;
    }

    public void setIncludeinp(String includeinp) {
        this.includeinp = includeinp;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmPayrollDeduction)) {
            return false;
        }
        EmPayrollDeduction other = (EmPayrollDeduction) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.employee.EmPayrollDeduction[ id=" + id + " ]";
    }
    
}
