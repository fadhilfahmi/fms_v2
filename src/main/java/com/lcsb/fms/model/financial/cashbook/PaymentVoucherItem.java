/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "cb_payvaucher_debit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PaymentVoucherItem.findAll", query = "SELECT p FROM PaymentVoucherItem p"),
    @NamedQuery(name = "PaymentVoucherItem.findByCoacode", query = "SELECT p FROM PaymentVoucherItem p WHERE p.coacode = :coacode"),
    @NamedQuery(name = "PaymentVoucherItem.findByCoadescp", query = "SELECT p FROM PaymentVoucherItem p WHERE p.coadescp = :coadescp"),
    @NamedQuery(name = "PaymentVoucherItem.findByLoclevel", query = "SELECT p FROM PaymentVoucherItem p WHERE p.loclevel = :loclevel"),
    @NamedQuery(name = "PaymentVoucherItem.findByLoccode", query = "SELECT p FROM PaymentVoucherItem p WHERE p.loccode = :loccode"),
    @NamedQuery(name = "PaymentVoucherItem.findByLocname", query = "SELECT p FROM PaymentVoucherItem p WHERE p.locname = :locname"),
    @NamedQuery(name = "PaymentVoucherItem.findByChargetype", query = "SELECT p FROM PaymentVoucherItem p WHERE p.chargetype = :chargetype"),
    @NamedQuery(name = "PaymentVoucherItem.findByChargecode", query = "SELECT p FROM PaymentVoucherItem p WHERE p.chargecode = :chargecode"),
    @NamedQuery(name = "PaymentVoucherItem.findByChargedescp", query = "SELECT p FROM PaymentVoucherItem p WHERE p.chargedescp = :chargedescp"),
    @NamedQuery(name = "PaymentVoucherItem.findByAmount", query = "SELECT p FROM PaymentVoucherItem p WHERE p.amount = :amount"),
    @NamedQuery(name = "PaymentVoucherItem.findByVoucer", query = "SELECT p FROM PaymentVoucherItem p WHERE p.voucer = :voucer"),
    @NamedQuery(name = "PaymentVoucherItem.findByRefer", query = "SELECT p FROM PaymentVoucherItem p WHERE p.refer = :refer"),
    @NamedQuery(name = "PaymentVoucherItem.findBySatype", query = "SELECT p FROM PaymentVoucherItem p WHERE p.satype = :satype"),
    @NamedQuery(name = "PaymentVoucherItem.findBySacode", query = "SELECT p FROM PaymentVoucherItem p WHERE p.sacode = :sacode"),
    @NamedQuery(name = "PaymentVoucherItem.findBySadesc", query = "SELECT p FROM PaymentVoucherItem p WHERE p.sadesc = :sadesc"),
    @NamedQuery(name = "PaymentVoucherItem.findByTaxcode", query = "SELECT p FROM PaymentVoucherItem p WHERE p.taxcode = :taxcode"),
    @NamedQuery(name = "PaymentVoucherItem.findByTaxcoacode", query = "SELECT p FROM PaymentVoucherItem p WHERE p.taxcoacode = :taxcoacode"),
    @NamedQuery(name = "PaymentVoucherItem.findByTaxcoadescp", query = "SELECT p FROM PaymentVoucherItem p WHERE p.taxcoadescp = :taxcoadescp"),
    @NamedQuery(name = "PaymentVoucherItem.findByTaxrate", query = "SELECT p FROM PaymentVoucherItem p WHERE p.taxrate = :taxrate"),
    @NamedQuery(name = "PaymentVoucherItem.findByTaxamt", query = "SELECT p FROM PaymentVoucherItem p WHERE p.taxamt = :taxamt"),
    @NamedQuery(name = "PaymentVoucherItem.findByAmtbeforetax", query = "SELECT p FROM PaymentVoucherItem p WHERE p.amtbeforetax = :amtbeforetax")})
public class PaymentVoucherItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coacode")
    private String coacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coadescp")
    private String coadescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loclevel")
    private String loclevel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loccode")
    private String loccode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "locname")
    private String locname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "chargetype")
    private String chargetype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "chargecode")
    private String chargecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "chargedescp")
    private String chargedescp;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "voucer")
    private String voucer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;
    @Size(max = 100)
    @Column(name = "satype")
    private String satype;
    @Size(max = 100)
    @Column(name = "sacode")
    private String sacode;
    @Size(max = 100)
    @Column(name = "sadesc")
    private String sadesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcode")
    private String taxcode;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "taxdescp")
    private String taxdescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcoacode")
    private String taxcoacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcoadescp")
    private String taxcoadescp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxrate")
    private double taxrate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxamt")
    private double taxamt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amtbeforetax")
    private double amtbeforetax;
    
    private String invrefno;

    public PaymentVoucherItem() {
    }

    public PaymentVoucherItem(String voucer) {
        this.voucer = voucer;
    }

    public PaymentVoucherItem(String voucer, String coacode, String coadescp, String loclevel, String loccode, String locname, String chargetype, String chargecode, String chargedescp, String remarks, double amount, String refer, String taxcode, String taxdescp, String taxcoacode, String taxcoadescp,String invrefno, double taxrate, double taxamt, double amtbeforetax) {
        this.voucer = voucer;
        this.coacode = coacode;
        this.coadescp = coadescp;
        this.loclevel = loclevel;
        this.loccode = loccode;
        this.locname = locname;
        this.chargetype = chargetype;
        this.chargecode = chargecode;
        this.chargedescp = chargedescp;
        this.remarks = remarks;
        this.amount = amount;
        this.refer = refer;
        this.taxcode = taxcode;
        this.taxdescp = taxdescp;
        this.taxcoacode = taxcoacode;
        this.taxcoadescp = taxcoadescp;
        this.taxrate = taxrate;
        this.taxamt = taxamt;
        this.amtbeforetax = amtbeforetax;
        this.invrefno = invrefno;
    }

    public String getInvrefno() {
        return invrefno;
    }

    public void setInvrefno(String invrefno) {
        this.invrefno = invrefno;
    }
    
    public String getCoacode() {
        return coacode;
    }

    public void setCoacode(String coacode) {
        this.coacode = coacode;
    }

    public String getCoadescp() {
        return coadescp;
    }

    public void setCoadescp(String coadescp) {
        this.coadescp = coadescp;
    }

    public String getLoclevel() {
        return loclevel;
    }

    public void setLoclevel(String loclevel) {
        this.loclevel = loclevel;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLocname() {
        return locname;
    }

    public void setLocname(String locname) {
        this.locname = locname;
    }

    public String getChargetype() {
        return chargetype;
    }

    public void setChargetype(String chargetype) {
        this.chargetype = chargetype;
    }

    public String getChargecode() {
        return chargecode;
    }

    public void setChargecode(String chargecode) {
        this.chargecode = chargecode;
    }

    public String getChargedescp() {
        return chargedescp;
    }

    public void setChargedescp(String chargedescp) {
        this.chargedescp = chargedescp;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getVoucer() {
        return voucer;
    }

    public void setVoucer(String voucer) {
        this.voucer = voucer;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    public String getTaxcode() {
        return taxcode;
    }

    public void setTaxcode(String taxcode) {
        this.taxcode = taxcode;
    }

    public String getTaxdescp() {
        return taxdescp;
    }

    public void setTaxdescp(String taxdescp) {
        this.taxdescp = taxdescp;
    }

    public String getTaxcoacode() {
        return taxcoacode;
    }

    public void setTaxcoacode(String taxcoacode) {
        this.taxcoacode = taxcoacode;
    }

    public String getTaxcoadescp() {
        return taxcoadescp;
    }

    public void setTaxcoadescp(String taxcoadescp) {
        this.taxcoadescp = taxcoadescp;
    }

    public double getTaxrate() {
        return taxrate;
    }

    public void setTaxrate(double taxrate) {
        this.taxrate = taxrate;
    }

    public double getTaxamt() {
        return taxamt;
    }

    public void setTaxamt(double taxamt) {
        this.taxamt = taxamt;
    }

    public double getAmtbeforetax() {
        return amtbeforetax;
    }

    public void setAmtbeforetax(double amtbeforetax) {
        this.amtbeforetax = amtbeforetax;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (voucer != null ? voucer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentVoucherItem)) {
            return false;
        }
        PaymentVoucherItem other = (PaymentVoucherItem) object;
        if ((this.voucer == null && other.voucer != null) || (this.voucer != null && !this.voucer.equals(other.voucer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem[ voucer=" + voucer + " ]";
    }
    
}
