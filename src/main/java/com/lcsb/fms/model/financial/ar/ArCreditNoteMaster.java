/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ar;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class ArCreditNoteMaster {
    
    private ArCreditNote aMain;
    private List<ArCreditNoteItem> aList;

    public ArCreditNote getaMain() {
        return aMain;
    }

    public void setaMain(ArCreditNote aMain) {
        this.aMain = aMain;
    }

    public List<ArCreditNoteItem> getaList() {
        return aList;
    }

    public void setaList(List<ArCreditNoteItem> aList) {
        this.aList = aList;
    }
    
    
}
