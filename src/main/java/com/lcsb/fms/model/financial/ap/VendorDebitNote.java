/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ap;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ap_debitnote")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VendorDebitNote.findAll", query = "SELECT v FROM VendorDebitNote v"),
    @NamedQuery(name = "VendorDebitNote.findByNoteno", query = "SELECT v FROM VendorDebitNote v WHERE v.noteno = :noteno"),
    @NamedQuery(name = "VendorDebitNote.findByNotedate", query = "SELECT v FROM VendorDebitNote v WHERE v.notedate = :notedate"),
    @NamedQuery(name = "VendorDebitNote.findByYear", query = "SELECT v FROM VendorDebitNote v WHERE v.year = :year"),
    @NamedQuery(name = "VendorDebitNote.findByPeriod", query = "SELECT v FROM VendorDebitNote v WHERE v.period = :period"),
    @NamedQuery(name = "VendorDebitNote.findByAcccode", query = "SELECT v FROM VendorDebitNote v WHERE v.acccode = :acccode"),
    @NamedQuery(name = "VendorDebitNote.findByAccdesc", query = "SELECT v FROM VendorDebitNote v WHERE v.accdesc = :accdesc"),
    @NamedQuery(name = "VendorDebitNote.findByEstcode", query = "SELECT v FROM VendorDebitNote v WHERE v.estcode = :estcode"),
    @NamedQuery(name = "VendorDebitNote.findByEstname", query = "SELECT v FROM VendorDebitNote v WHERE v.estname = :estname"),
    @NamedQuery(name = "VendorDebitNote.findByCttype", query = "SELECT v FROM VendorDebitNote v WHERE v.cttype = :cttype"),
    @NamedQuery(name = "VendorDebitNote.findByCtcode", query = "SELECT v FROM VendorDebitNote v WHERE v.ctcode = :ctcode"),
    @NamedQuery(name = "VendorDebitNote.findByCtdesc", query = "SELECT v FROM VendorDebitNote v WHERE v.ctdesc = :ctdesc"),
    @NamedQuery(name = "VendorDebitNote.findByTotal", query = "SELECT v FROM VendorDebitNote v WHERE v.total = :total"),
    @NamedQuery(name = "VendorDebitNote.findByPostflag", query = "SELECT v FROM VendorDebitNote v WHERE v.postflag = :postflag"),
    @NamedQuery(name = "VendorDebitNote.findByPostdate", query = "SELECT v FROM VendorDebitNote v WHERE v.postdate = :postdate"),
    @NamedQuery(name = "VendorDebitNote.findByTopost", query = "SELECT v FROM VendorDebitNote v WHERE v.topost = :topost"),
    @NamedQuery(name = "VendorDebitNote.findByPrepareid", query = "SELECT v FROM VendorDebitNote v WHERE v.prepareid = :prepareid"),
    @NamedQuery(name = "VendorDebitNote.findByPreparename", query = "SELECT v FROM VendorDebitNote v WHERE v.preparename = :preparename"),
    @NamedQuery(name = "VendorDebitNote.findByPreparedate", query = "SELECT v FROM VendorDebitNote v WHERE v.preparedate = :preparedate"),
    @NamedQuery(name = "VendorDebitNote.findByApproveid", query = "SELECT v FROM VendorDebitNote v WHERE v.approveid = :approveid"),
    @NamedQuery(name = "VendorDebitNote.findByApprovename", query = "SELECT v FROM VendorDebitNote v WHERE v.approvename = :approvename"),
    @NamedQuery(name = "VendorDebitNote.findByApprovedesig", query = "SELECT v FROM VendorDebitNote v WHERE v.approvedesig = :approvedesig"),
    @NamedQuery(name = "VendorDebitNote.findByApprovedate", query = "SELECT v FROM VendorDebitNote v WHERE v.approvedate = :approvedate"),
    @NamedQuery(name = "VendorDebitNote.findByReceivercode", query = "SELECT v FROM VendorDebitNote v WHERE v.receivercode = :receivercode"),
    @NamedQuery(name = "VendorDebitNote.findByReceivername", query = "SELECT v FROM VendorDebitNote v WHERE v.receivername = :receivername"),
    @NamedQuery(name = "VendorDebitNote.findBySatype", query = "SELECT v FROM VendorDebitNote v WHERE v.satype = :satype"),
    @NamedQuery(name = "VendorDebitNote.findBySacode", query = "SELECT v FROM VendorDebitNote v WHERE v.sacode = :sacode"),
    @NamedQuery(name = "VendorDebitNote.findBySadesc", query = "SELECT v FROM VendorDebitNote v WHERE v.sadesc = :sadesc")})
public class VendorDebitNote implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "noteno")
    private String noteno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "notedate")
    private String notedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "period")
    private String period;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "acccode")
    private String acccode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "accdesc")
    private String accdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estcode")
    private String estcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estname")
    private String estname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cttype")
    private String cttype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ctcode")
    private String ctcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ctdesc")
    private String ctdesc;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "remark")
    private String remark;
    @Basic(optional = false)
    @NotNull
    @Column(name = "total")
    private double total;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postflag")
    private String postflag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postdate")
    private String postdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "topost")
    private String topost;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prepareid")
    private String prepareid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "preparename")
    private String preparename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "preparedate")
    private String preparedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "approveid")
    private String approveid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "approvename")
    private String approvename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "approvedesig")
    private String approvedesig;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "approvedate")
    private String approvedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "receivercode")
    private String receivercode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "receivername")
    private String receivername;
    @Size(max = 100)
    @Column(name = "satype")
    private String satype;
    @Size(max = 100)
    @Column(name = "sacode")
    private String sacode;
    @Size(max = 100)
    @Column(name = "sadesc")
    private String sadesc;

    public VendorDebitNote() {
    }

    public VendorDebitNote(String noteno) {
        this.noteno = noteno;
    }

    public VendorDebitNote(String noteno, String notedate, String year, String period, String acccode, String accdesc, String estcode, String estname, String cttype, String ctcode, String ctdesc, String remark, double total, String postflag, String postdate, String topost, String prepareid, String preparename, String preparedate, String approveid, String approvename, String approvedesig, String approvedate, String receivercode, String receivername) {
        this.noteno = noteno;
        this.notedate = notedate;
        this.year = year;
        this.period = period;
        this.acccode = acccode;
        this.accdesc = accdesc;
        this.estcode = estcode;
        this.estname = estname;
        this.cttype = cttype;
        this.ctcode = ctcode;
        this.ctdesc = ctdesc;
        this.remark = remark;
        this.total = total;
        this.postflag = postflag;
        this.postdate = postdate;
        this.topost = topost;
        this.prepareid = prepareid;
        this.preparename = preparename;
        this.preparedate = preparedate;
        this.approveid = approveid;
        this.approvename = approvename;
        this.approvedesig = approvedesig;
        this.approvedate = approvedate;
        this.receivercode = receivercode;
        this.receivername = receivername;
    }

    public String getNoteno() {
        return noteno;
    }

    public void setNoteno(String noteno) {
        this.noteno = noteno;
    }

    public String getNotedate() {
        return notedate;
    }

    public void setNotedate(String notedate) {
        this.notedate = notedate;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getAcccode() {
        return acccode;
    }

    public void setAcccode(String acccode) {
        this.acccode = acccode;
    }

    public String getAccdesc() {
        return accdesc;
    }

    public void setAccdesc(String accdesc) {
        this.accdesc = accdesc;
    }

    public String getEstcode() {
        return estcode;
    }

    public void setEstcode(String estcode) {
        this.estcode = estcode;
    }

    public String getEstname() {
        return estname;
    }

    public void setEstname(String estname) {
        this.estname = estname;
    }

    public String getCttype() {
        return cttype;
    }

    public void setCttype(String cttype) {
        this.cttype = cttype;
    }

    public String getCtcode() {
        return ctcode;
    }

    public void setCtcode(String ctcode) {
        this.ctcode = ctcode;
    }

    public String getCtdesc() {
        return ctdesc;
    }

    public void setCtdesc(String ctdesc) {
        this.ctdesc = ctdesc;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getPostflag() {
        return postflag;
    }

    public void setPostflag(String postflag) {
        this.postflag = postflag;
    }

    public String getPostdate() {
        return postdate;
    }

    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }

    public String getTopost() {
        return topost;
    }

    public void setTopost(String topost) {
        this.topost = topost;
    }

    public String getPrepareid() {
        return prepareid;
    }

    public void setPrepareid(String prepareid) {
        this.prepareid = prepareid;
    }

    public String getPreparename() {
        return preparename;
    }

    public void setPreparename(String preparename) {
        this.preparename = preparename;
    }

    public String getPreparedate() {
        return preparedate;
    }

    public void setPreparedate(String preparedate) {
        this.preparedate = preparedate;
    }

    public String getApproveid() {
        return approveid;
    }

    public void setApproveid(String approveid) {
        this.approveid = approveid;
    }

    public String getApprovename() {
        return approvename;
    }

    public void setApprovename(String approvename) {
        this.approvename = approvename;
    }

    public String getApprovedesig() {
        return approvedesig;
    }

    public void setApprovedesig(String approvedesig) {
        this.approvedesig = approvedesig;
    }

    public String getApprovedate() {
        return approvedate;
    }

    public void setApprovedate(String approvedate) {
        this.approvedate = approvedate;
    }

    public String getReceivercode() {
        return receivercode;
    }

    public void setReceivercode(String receivercode) {
        this.receivercode = receivercode;
    }

    public String getReceivername() {
        return receivername;
    }

    public void setReceivername(String receivername) {
        this.receivername = receivername;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noteno != null ? noteno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VendorDebitNote)) {
            return false;
        }
        VendorDebitNote other = (VendorDebitNote) object;
        if ((this.noteno == null && other.noteno != null) || (this.noteno != null && !this.noteno.equals(other.noteno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.ap.VendorDebitNote[ noteno=" + noteno + " ]";
    }
    
}
