/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ar;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "sl_inv_temp_refinery")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ArTempRefinery.findAll", query = "SELECT a FROM ArTempRefinery a"),
    @NamedQuery(name = "ArTempRefinery.findById", query = "SELECT a FROM ArTempRefinery a WHERE a.id = :id"),
    @NamedQuery(name = "ArTempRefinery.findByDate", query = "SELECT a FROM ArTempRefinery a WHERE a.date = :date"),
    @NamedQuery(name = "ArTempRefinery.findByContractno", query = "SELECT a FROM ArTempRefinery a WHERE a.contractno = :contractno"),
    @NamedQuery(name = "ArTempRefinery.findByTrailer", query = "SELECT a FROM ArTempRefinery a WHERE a.trailer = :trailer"),
    @NamedQuery(name = "ArTempRefinery.findByDispatchno", query = "SELECT a FROM ArTempRefinery a WHERE a.dispatchno = :dispatchno"),
    @NamedQuery(name = "ArTempRefinery.findByMtMill", query = "SELECT a FROM ArTempRefinery a WHERE a.mtMill = :mtMill"),
    @NamedQuery(name = "ArTempRefinery.findByMtRefinery", query = "SELECT a FROM ArTempRefinery a WHERE a.mtRefinery = :mtRefinery")})
public class ArTempRefinery implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "date")
    private String date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "contractno")
    private String contractno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "trailer")
    private String trailer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "dispatchno")
    private String dispatchno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "mt_mill")
    private double mtMill;
    @Basic(optional = false)
    @NotNull
    @Column(name = "mt_refinery")
    private double mtRefinery;
    private String sessionid;

    public ArTempRefinery() {
    }

    public ArTempRefinery(Integer id) {
        this.id = id;
    }

    public ArTempRefinery(Integer id, String date, String contractno, String trailer, String dispatchno, double mtMill, double mtRefinery, String sessionid) {
        this.id = id;
        this.date = date;
        this.contractno = contractno;
        this.trailer = trailer;
        this.dispatchno = dispatchno;
        this.mtMill = mtMill;
        this.mtRefinery = mtRefinery;
        this.sessionid = sessionid;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContractno() {
        return contractno;
    }

    public void setContractno(String contractno) {
        this.contractno = contractno;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getDispatchno() {
        return dispatchno;
    }

    public void setDispatchno(String dispatchno) {
        this.dispatchno = dispatchno;
    }

    public double getMtMill() {
        return mtMill;
    }

    public void setMtMill(double mtMill) {
        this.mtMill = mtMill;
    }

    public double getMtRefinery() {
        return mtRefinery;
    }

    public void setMtRefinery(double mtRefinery) {
        this.mtRefinery = mtRefinery;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArTempRefinery)) {
            return false;
        }
        ArTempRefinery other = (ArTempRefinery) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.ar.ArTempRefinery[ id=" + id + " ]";
    }
    
}
