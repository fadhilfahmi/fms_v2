/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ar;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dell
 */
@Entity
@Table(name = "sl_inv_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SolesInvoiceItem.findAll", query = "SELECT s FROM SolesInvoiceItem s"),
    @NamedQuery(name = "SolesInvoiceItem.findByIvref", query = "SELECT s FROM SolesInvoiceItem s WHERE s.ivref = :ivref"),
    @NamedQuery(name = "SolesInvoiceItem.findByProdcode", query = "SELECT s FROM SolesInvoiceItem s WHERE s.prodcode = :prodcode"),
    @NamedQuery(name = "SolesInvoiceItem.findByProdname", query = "SELECT s FROM SolesInvoiceItem s WHERE s.prodname = :prodname"),
    @NamedQuery(name = "SolesInvoiceItem.findByCoacode", query = "SELECT s FROM SolesInvoiceItem s WHERE s.coacode = :coacode"),
    @NamedQuery(name = "SolesInvoiceItem.findByCoaname", query = "SELECT s FROM SolesInvoiceItem s WHERE s.coaname = :coaname"),
    @NamedQuery(name = "SolesInvoiceItem.findByQty", query = "SELECT s FROM SolesInvoiceItem s WHERE s.qty = :qty"),
    @NamedQuery(name = "SolesInvoiceItem.findByUnitp", query = "SELECT s FROM SolesInvoiceItem s WHERE s.unitp = :unitp"),
    @NamedQuery(name = "SolesInvoiceItem.findByAmount", query = "SELECT s FROM SolesInvoiceItem s WHERE s.amount = :amount"),
    @NamedQuery(name = "SolesInvoiceItem.findByUnitm", query = "SELECT s FROM SolesInvoiceItem s WHERE s.unitm = :unitm"),
    @NamedQuery(name = "SolesInvoiceItem.findByLoclevel", query = "SELECT s FROM SolesInvoiceItem s WHERE s.loclevel = :loclevel"),
    @NamedQuery(name = "SolesInvoiceItem.findByLoccode", query = "SELECT s FROM SolesInvoiceItem s WHERE s.loccode = :loccode"),
    @NamedQuery(name = "SolesInvoiceItem.findByLocdesc", query = "SELECT s FROM SolesInvoiceItem s WHERE s.locdesc = :locdesc"),
    @NamedQuery(name = "SolesInvoiceItem.findByCttype", query = "SELECT s FROM SolesInvoiceItem s WHERE s.cttype = :cttype"),
    @NamedQuery(name = "SolesInvoiceItem.findByCtcode", query = "SELECT s FROM SolesInvoiceItem s WHERE s.ctcode = :ctcode"),
    @NamedQuery(name = "SolesInvoiceItem.findByCtdesc", query = "SELECT s FROM SolesInvoiceItem s WHERE s.ctdesc = :ctdesc"),
    @NamedQuery(name = "SolesInvoiceItem.findByTaxcode", query = "SELECT s FROM SolesInvoiceItem s WHERE s.taxcode = :taxcode"),
    @NamedQuery(name = "SolesInvoiceItem.findByTaxrate", query = "SELECT s FROM SolesInvoiceItem s WHERE s.taxrate = :taxrate"),
    @NamedQuery(name = "SolesInvoiceItem.findByTaxamt", query = "SELECT s FROM SolesInvoiceItem s WHERE s.taxamt = :taxamt"),
    @NamedQuery(name = "SolesInvoiceItem.findByTaxcoacode", query = "SELECT s FROM SolesInvoiceItem s WHERE s.taxcoacode = :taxcoacode"),
    @NamedQuery(name = "SolesInvoiceItem.findByTaxcoadescp", query = "SELECT s FROM SolesInvoiceItem s WHERE s.taxcoadescp = :taxcoadescp"),
    @NamedQuery(name = "SolesInvoiceItem.findByIvno", query = "SELECT s FROM SolesInvoiceItem s WHERE s.ivno = :ivno"),
    @NamedQuery(name = "SolesInvoiceItem.findBySatype", query = "SELECT s FROM SolesInvoiceItem s WHERE s.satype = :satype"),
    @NamedQuery(name = "SolesInvoiceItem.findBySacode", query = "SELECT s FROM SolesInvoiceItem s WHERE s.sacode = :sacode"),
    @NamedQuery(name = "SolesInvoiceItem.findBySadesc", query = "SELECT s FROM SolesInvoiceItem s WHERE s.sadesc = :sadesc"),
    @NamedQuery(name = "SolesInvoiceItem.findByRefer", query = "SELECT s FROM SolesInvoiceItem s WHERE s.refer = :refer")})
public class SalesInvoiceItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ivref")
    private String ivref;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prodcode")
    private String prodcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prodname")
    private String prodname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coacode")
    private String coacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coaname")
    private String coaname;
    @Basic(optional = false)
    @NotNull
    @Column(name = "qty")
    private double qty;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "unitp")
    private String unitp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "unitm")
    private String unitm;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loclevel")
    private String loclevel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loccode")
    private String loccode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "locdesc")
    private String locdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cttype")
    private String cttype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ctcode")
    private String ctcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ctdesc")
    private String ctdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcode")
    private String taxcode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxrate")
    private double taxrate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxamt")
    private double taxamt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcoacode")
    private String taxcoacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcoadescp")
    private String taxcoadescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ivno")
    private String ivno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "satype")
    private String satype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "sacode")
    private String sacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "sadesc")
    private String sadesc;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;
    private String dispatchno;

    public SalesInvoiceItem() {
    }

    public SalesInvoiceItem(String refer) {
        this.refer = refer;
    }

    public SalesInvoiceItem(String refer, String ivref, String prodcode, String prodname, String coacode, String coaname, double qty, String unitp, double amount, String unitm, String remarks, String loclevel, String loccode, String locdesc, String cttype, String ctcode, String ctdesc, String taxcode, double taxrate, double taxamt, String taxcoacode, String taxcoadescp, String ivno, String satype, String sacode, String sadesc,String dispatchno) {
        this.refer = refer;
        this.ivref = ivref;
        this.prodcode = prodcode;
        this.prodname = prodname;
        this.coacode = coacode;
        this.coaname = coaname;
        this.qty = qty;
        this.unitp = unitp;
        this.amount = amount;
        this.unitm = unitm;
        this.remarks = remarks;
        this.loclevel = loclevel;
        this.loccode = loccode;
        this.locdesc = locdesc;
        this.cttype = cttype;
        this.ctcode = ctcode;
        this.ctdesc = ctdesc;
        this.taxcode = taxcode;
        this.taxrate = taxrate;
        this.taxamt = taxamt;
        this.taxcoacode = taxcoacode;
        this.taxcoadescp = taxcoadescp;
        this.ivno = ivno;
        this.satype = satype;
        this.sacode = sacode;
        this.sadesc = sadesc;
        this.dispatchno = dispatchno;
    }

    public String getDispatchno() {
        return dispatchno;
    }

    public void setDispatchno(String dispatchno) {
        this.dispatchno = dispatchno;
    }

    public String getIvref() {
        return ivref;
    }

    public void setIvref(String ivref) {
        this.ivref = ivref;
    }

    public String getProdcode() {
        return prodcode;
    }

    public void setProdcode(String prodcode) {
        this.prodcode = prodcode;
    }

    public String getProdname() {
        return prodname;
    }

    public void setProdname(String prodname) {
        this.prodname = prodname;
    }

    public String getCoacode() {
        return coacode;
    }

    public void setCoacode(String coacode) {
        this.coacode = coacode;
    }

    public String getCoaname() {
        return coaname;
    }

    public void setCoaname(String coaname) {
        this.coaname = coaname;
    }

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }

    public String getUnitp() {
        return unitp;
    }

    public void setUnitp(String unitp) {
        this.unitp = unitp;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getUnitm() {
        return unitm;
    }

    public void setUnitm(String unitm) {
        this.unitm = unitm;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getLoclevel() {
        return loclevel;
    }

    public void setLoclevel(String loclevel) {
        this.loclevel = loclevel;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLocdesc() {
        return locdesc;
    }

    public void setLocdesc(String locdesc) {
        this.locdesc = locdesc;
    }

    public String getCttype() {
        return cttype;
    }

    public void setCttype(String cttype) {
        this.cttype = cttype;
    }

    public String getCtcode() {
        return ctcode;
    }

    public void setCtcode(String ctcode) {
        this.ctcode = ctcode;
    }

    public String getCtdesc() {
        return ctdesc;
    }

    public void setCtdesc(String ctdesc) {
        this.ctdesc = ctdesc;
    }

    public String getTaxcode() {
        return taxcode;
    }

    public void setTaxcode(String taxcode) {
        this.taxcode = taxcode;
    }

    public double getTaxrate() {
        return taxrate;
    }

    public void setTaxrate(double taxrate) {
        this.taxrate = taxrate;
    }

    public double getTaxamt() {
        return taxamt;
    }

    public void setTaxamt(double taxamt) {
        this.taxamt = taxamt;
    }

    public String getTaxcoacode() {
        return taxcoacode;
    }

    public void setTaxcoacode(String taxcoacode) {
        this.taxcoacode = taxcoacode;
    }

    public String getTaxcoadescp() {
        return taxcoadescp;
    }

    public void setTaxcoadescp(String taxcoadescp) {
        this.taxcoadescp = taxcoadescp;
    }

    public String getIvno() {
        return ivno;
    }

    public void setIvno(String ivno) {
        this.ivno = ivno;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SalesInvoiceItem)) {
            return false;
        }
        SalesInvoiceItem other = (SalesInvoiceItem) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.ar.SolesInvoiceItem[ refer=" + refer + " ]";
    }
    
}
