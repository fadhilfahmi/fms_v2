/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.fms.model.financial.gl;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dell
 */
@Entity
@Table(name = "cb_official_credit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbOfficialCredit.findAll", query = "SELECT c FROM CbOfficialCredit c"),
    @NamedQuery(name = "CbOfficialCredit.findByVoucherno", query = "SELECT c FROM CbOfficialCredit c WHERE c.voucherno = :voucherno"),
    @NamedQuery(name = "CbOfficialCredit.findByCoacode", query = "SELECT c FROM CbOfficialCredit c WHERE c.coacode = :coacode"),
    @NamedQuery(name = "CbOfficialCredit.findByCoadescp", query = "SELECT c FROM CbOfficialCredit c WHERE c.coadescp = :coadescp"),
    @NamedQuery(name = "CbOfficialCredit.findByLoclevel", query = "SELECT c FROM CbOfficialCredit c WHERE c.loclevel = :loclevel"),
    @NamedQuery(name = "CbOfficialCredit.findByLoccode", query = "SELECT c FROM CbOfficialCredit c WHERE c.loccode = :loccode"),
    @NamedQuery(name = "CbOfficialCredit.findByLocname", query = "SELECT c FROM CbOfficialCredit c WHERE c.locname = :locname"),
    @NamedQuery(name = "CbOfficialCredit.findByChargetype", query = "SELECT c FROM CbOfficialCredit c WHERE c.chargetype = :chargetype"),
    @NamedQuery(name = "CbOfficialCredit.findByChargecode", query = "SELECT c FROM CbOfficialCredit c WHERE c.chargecode = :chargecode"),
    @NamedQuery(name = "CbOfficialCredit.findByChargedescp", query = "SELECT c FROM CbOfficialCredit c WHERE c.chargedescp = :chargedescp"),
    @NamedQuery(name = "CbOfficialCredit.findByAmount", query = "SELECT c FROM CbOfficialCredit c WHERE c.amount = :amount"),
    @NamedQuery(name = "CbOfficialCredit.findByRefer", query = "SELECT c FROM CbOfficialCredit c WHERE c.refer = :refer"),
    @NamedQuery(name = "CbOfficialCredit.findBySatype", query = "SELECT c FROM CbOfficialCredit c WHERE c.satype = :satype"),
    @NamedQuery(name = "CbOfficialCredit.findBySacode", query = "SELECT c FROM CbOfficialCredit c WHERE c.sacode = :sacode"),
    @NamedQuery(name = "CbOfficialCredit.findBySadesc", query = "SELECT c FROM CbOfficialCredit c WHERE c.sadesc = :sadesc"),
    @NamedQuery(name = "CbOfficialCredit.findByTaxcode", query = "SELECT c FROM CbOfficialCredit c WHERE c.taxcode = :taxcode"),
    @NamedQuery(name = "CbOfficialCredit.findByTaxdescp", query = "SELECT c FROM CbOfficialCredit c WHERE c.taxdescp = :taxdescp"),
    @NamedQuery(name = "CbOfficialCredit.findByTaxrate", query = "SELECT c FROM CbOfficialCredit c WHERE c.taxrate = :taxrate"),
    @NamedQuery(name = "CbOfficialCredit.findByTaxamt", query = "SELECT c FROM CbOfficialCredit c WHERE c.taxamt = :taxamt"),
    @NamedQuery(name = "CbOfficialCredit.findByAmtbeforetax", query = "SELECT c FROM CbOfficialCredit c WHERE c.amtbeforetax = :amtbeforetax"),
    @NamedQuery(name = "CbOfficialCredit.findByTaxcoacode", query = "SELECT c FROM CbOfficialCredit c WHERE c.taxcoacode = :taxcoacode"),
    @NamedQuery(name = "CbOfficialCredit.findByTaxcoadescp", query = "SELECT c FROM CbOfficialCredit c WHERE c.taxcoadescp = :taxcoadescp")})
public class CbOfficialCredit implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "voucherno")
    private String voucherno;
    @Basic(optional = false)
    @Column(name = "coacode")
    private String coacode;
    @Basic(optional = false)
    @Column(name = "coadescp")
    private String coadescp;
    @Basic(optional = false)
    @Column(name = "loclevel")
    private String loclevel;
    @Basic(optional = false)
    @Column(name = "loccode")
    private String loccode;
    @Basic(optional = false)
    @Column(name = "locname")
    private String locname;
    @Basic(optional = false)
    @Column(name = "chargetype")
    private String chargetype;
    @Basic(optional = false)
    @Column(name = "chargecode")
    private String chargecode;
    @Basic(optional = false)
    @Column(name = "chargedescp")
    private String chargedescp;
    @Basic(optional = false)
    @Lob
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @Column(name = "amount")
    private String amount;
    @Id
    @Basic(optional = false)
    @Column(name = "refer")
    private String refer;
    @Column(name = "satype")
    private String satype;
    @Column(name = "sacode")
    private String sacode;
    @Column(name = "sadesc")
    private String sadesc;
    @Basic(optional = false)
    @Column(name = "taxcode")
    private String taxcode;
    @Basic(optional = false)
    @Column(name = "taxdescp")
    private String taxdescp;
    @Basic(optional = false)
    @Column(name = "taxrate")
    private double taxrate;
    @Basic(optional = false)
    @Column(name = "taxamt")
    private double taxamt;
    @Basic(optional = false)
    @Column(name = "amtbeforetax")
    private double amtbeforetax;
    @Basic(optional = false)
    @Column(name = "taxcoacode")
    private String taxcoacode;
    @Basic(optional = false)
    @Column(name = "taxcoadescp")
    private String taxcoadescp;

    public CbOfficialCredit() {
    }

    public CbOfficialCredit(String refer) {
        this.refer = refer;
    }

    public CbOfficialCredit(String refer, String voucherno, String coacode, String coadescp, String loclevel, String loccode, String locname, String chargetype, String chargecode, String chargedescp, String remarks, String amount, String taxcode, String taxdescp, double taxrate, double taxamt, double amtbeforetax, String taxcoacode, String taxcoadescp) {
        this.refer = refer;
        this.voucherno = voucherno;
        this.coacode = coacode;
        this.coadescp = coadescp;
        this.loclevel = loclevel;
        this.loccode = loccode;
        this.locname = locname;
        this.chargetype = chargetype;
        this.chargecode = chargecode;
        this.chargedescp = chargedescp;
        this.remarks = remarks;
        this.amount = amount;
        this.taxcode = taxcode;
        this.taxdescp = taxdescp;
        this.taxrate = taxrate;
        this.taxamt = taxamt;
        this.amtbeforetax = amtbeforetax;
        this.taxcoacode = taxcoacode;
        this.taxcoadescp = taxcoadescp;
    }

    public String getVoucherno() {
        return voucherno;
    }

    public void setVoucherno(String voucherno) {
        this.voucherno = voucherno;
    }

    public String getCoacode() {
        return coacode;
    }

    public void setCoacode(String coacode) {
        this.coacode = coacode;
    }

    public String getCoadescp() {
        return coadescp;
    }

    public void setCoadescp(String coadescp) {
        this.coadescp = coadescp;
    }

    public String getLoclevel() {
        return loclevel;
    }

    public void setLoclevel(String loclevel) {
        this.loclevel = loclevel;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLocname() {
        return locname;
    }

    public void setLocname(String locname) {
        this.locname = locname;
    }

    public String getChargetype() {
        return chargetype;
    }

    public void setChargetype(String chargetype) {
        this.chargetype = chargetype;
    }

    public String getChargecode() {
        return chargecode;
    }

    public void setChargecode(String chargecode) {
        this.chargecode = chargecode;
    }

    public String getChargedescp() {
        return chargedescp;
    }

    public void setChargedescp(String chargedescp) {
        this.chargedescp = chargedescp;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    public String getTaxcode() {
        return taxcode;
    }

    public void setTaxcode(String taxcode) {
        this.taxcode = taxcode;
    }

    public String getTaxdescp() {
        return taxdescp;
    }

    public void setTaxdescp(String taxdescp) {
        this.taxdescp = taxdescp;
    }

    public double getTaxrate() {
        return taxrate;
    }

    public void setTaxrate(double taxrate) {
        this.taxrate = taxrate;
    }

    public double getTaxamt() {
        return taxamt;
    }

    public void setTaxamt(double taxamt) {
        this.taxamt = taxamt;
    }

    public double getAmtbeforetax() {
        return amtbeforetax;
    }

    public void setAmtbeforetax(double amtbeforetax) {
        this.amtbeforetax = amtbeforetax;
    }

    public String getTaxcoacode() {
        return taxcoacode;
    }

    public void setTaxcoacode(String taxcoacode) {
        this.taxcoacode = taxcoacode;
    }

    public String getTaxcoadescp() {
        return taxcoadescp;
    }

    public void setTaxcoadescp(String taxcoadescp) {
        this.taxcoadescp = taxcoadescp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbOfficialCredit)) {
            return false;
        }
        CbOfficialCredit other = (CbOfficialCredit) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fms.model.financial.gl.CbOfficialCredit[ refer=" + refer + " ]";
    }
    
}
