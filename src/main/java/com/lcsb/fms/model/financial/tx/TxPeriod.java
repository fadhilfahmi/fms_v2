/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.tx;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "tx_return_period")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TxPeriod.findAll", query = "SELECT t FROM TxPeriod t"),
    @NamedQuery(name = "TxPeriod.findByYear", query = "SELECT t FROM TxPeriod t WHERE t.year = :year"),
    @NamedQuery(name = "TxPeriod.findByPeriod", query = "SELECT t FROM TxPeriod t WHERE t.period = :period"),
    @NamedQuery(name = "TxPeriod.findByTaxstart", query = "SELECT t FROM TxPeriod t WHERE t.taxstart = :taxstart"),
    @NamedQuery(name = "TxPeriod.findByTaxend", query = "SELECT t FROM TxPeriod t WHERE t.taxend = :taxend"),
    @NamedQuery(name = "TxPeriod.findByTaxdue", query = "SELECT t FROM TxPeriod t WHERE t.taxdue = :taxdue"),
    @NamedQuery(name = "TxPeriod.findByCurrent", query = "SELECT t FROM TxPeriod t WHERE t.current = :current"),
    @NamedQuery(name = "TxPeriod.findByClosedate", query = "SELECT t FROM TxPeriod t WHERE t.closedate = :closedate"),
    @NamedQuery(name = "TxPeriod.findByStartyear", query = "SELECT t FROM TxPeriod t WHERE t.startyear = :startyear"),
    @NamedQuery(name = "TxPeriod.findByEndyear", query = "SELECT t FROM TxPeriod t WHERE t.endyear = :endyear"),
    @NamedQuery(name = "TxPeriod.findByStatus", query = "SELECT t FROM TxPeriod t WHERE t.status = :status"),
    @NamedQuery(name = "TxPeriod.findByTaxreturnperiod", query = "SELECT t FROM TxPeriod t WHERE t.taxreturnperiod = :taxreturnperiod"),
    @NamedQuery(name = "TxPeriod.findById", query = "SELECT t FROM TxPeriod t WHERE t.id = :id")})
public class TxPeriod implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "year")
    private int year;
    @Basic(optional = false)
    @NotNull
    @Column(name = "period")
    private int period;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxstart")
    @Temporal(TemporalType.DATE)
    private Date taxstart;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxend")
    @Temporal(TemporalType.DATE)
    private Date taxend;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxdue")
    @Temporal(TemporalType.DATE)
    private Date taxdue;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "current")
    private String current;
    @Basic(optional = false)
    @NotNull
    @Column(name = "closedate")
    @Temporal(TemporalType.DATE)
    private Date closedate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "startyear")
    @Temporal(TemporalType.DATE)
    private Date startyear;
    @Basic(optional = false)
    @NotNull
    @Column(name = "endyear")
    @Temporal(TemporalType.DATE)
    private Date endyear;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 14)
    @Column(name = "taxreturnperiod")
    private String taxreturnperiod;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    private String name;

    public TxPeriod() {
    }

    public TxPeriod(Integer id) {
        this.id = id;
    }

    public TxPeriod(Integer id, int year, int period, Date taxstart, Date taxend, Date taxdue, String current, Date closedate, Date startyear, Date endyear, String status, String taxreturnperiod,String name) {
        this.id = id;
        this.year = year;
        this.period = period;
        this.taxstart = taxstart;
        this.taxend = taxend;
        this.taxdue = taxdue;
        this.current = current;
        this.closedate = closedate;
        this.startyear = startyear;
        this.endyear = endyear;
        this.status = status;
        this.taxreturnperiod = taxreturnperiod;
        this.name = name;
    } 

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public Date getTaxstart() {
        return taxstart;
    }

    public void setTaxstart(Date taxstart) {
        this.taxstart = taxstart;
    }

    public Date getTaxend() {
        return taxend;
    }

    public void setTaxend(Date taxend) {
        this.taxend = taxend;
    }

    public Date getTaxdue() {
        return taxdue;
    }

    public void setTaxdue(Date taxdue) {
        this.taxdue = taxdue;
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public Date getClosedate() {
        return closedate;
    }

    public void setClosedate(Date closedate) {
        this.closedate = closedate;
    }

    public Date getStartyear() {
        return startyear;
    }

    public void setStartyear(Date startyear) {
        this.startyear = startyear;
    }

    public Date getEndyear() {
        return endyear;
    }

    public void setEndyear(Date endyear) {
        this.endyear = endyear;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTaxreturnperiod() {
        return taxreturnperiod;
    }

    public void setTaxreturnperiod(String taxreturnperiod) {
        this.taxreturnperiod = taxreturnperiod;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TxPeriod)) {
            return false;
        }
        TxPeriod other = (TxPeriod) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.tx.TxPeriod[ id=" + id + " ]";
    }
    
}
