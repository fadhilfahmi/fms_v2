/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class ItemMain {
    
    private List<ItemDetail> listItemDetail;
    private String title;
    private String type;
    private double totalItem;
    private double totalCurrent;

    public double getTotalCurrent() {
        return totalCurrent;
    }

    public void setTotalCurrent(double totalCurrent) {
        this.totalCurrent = totalCurrent;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getTotalItem() {
        return totalItem;
    }

    public void setTotalItem(double totalItem) {
        this.totalItem = totalItem;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ItemDetail> getListItemDetail() {
        return listItemDetail;
    }

    public void setListItemDetail(List<ItemDetail> listItemDetail) {
        this.listItemDetail = listItemDetail;
    }
    
    
}
