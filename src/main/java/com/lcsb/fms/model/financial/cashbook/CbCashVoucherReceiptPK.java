/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author fadhilfahmi
 */
@Embeddable
public class CbCashVoucherReceiptPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "noreceipt")
    private String noreceipt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;

    public CbCashVoucherReceiptPK() {
    }

    public CbCashVoucherReceiptPK(String noreceipt, String refer) {
        this.noreceipt = noreceipt;
        this.refer = refer;
    }

    public String getNoreceipt() {
        return noreceipt;
    }

    public void setNoreceipt(String noreceipt) {
        this.noreceipt = noreceipt;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noreceipt != null ? noreceipt.hashCode() : 0);
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbCashVoucherReceiptPK)) {
            return false;
        }
        CbCashVoucherReceiptPK other = (CbCashVoucherReceiptPK) object;
        if ((this.noreceipt == null && other.noreceipt != null) || (this.noreceipt != null && !this.noreceipt.equals(other.noreceipt))) {
            return false;
        }
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.cashbook.CbCashVoucherReceiptPK[ noreceipt=" + noreceipt + ", refer=" + refer + " ]";
    }
    
}
