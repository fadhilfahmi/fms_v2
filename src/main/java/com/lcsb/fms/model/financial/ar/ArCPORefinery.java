/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ar;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "qc_cpo_grade")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ArCPORefinery.findAll", query = "SELECT a FROM ArCPORefinery a"),
    @NamedQuery(name = "ArCPORefinery.findByTransporter", query = "SELECT a FROM ArCPORefinery a WHERE a.transporter = :transporter"),
    @NamedQuery(name = "ArCPORefinery.findByMt", query = "SELECT a FROM ArCPORefinery a WHERE a.mt = :mt"),
    @NamedQuery(name = "ArCPORefinery.findByCode", query = "SELECT a FROM ArCPORefinery a WHERE a.code = :code"),
    @NamedQuery(name = "ArCPORefinery.findByDate", query = "SELECT a FROM ArCPORefinery a WHERE a.date = :date"),
    @NamedQuery(name = "ArCPORefinery.findByLot", query = "SELECT a FROM ArCPORefinery a WHERE a.lot = :lot"),
    @NamedQuery(name = "ArCPORefinery.findByContract", query = "SELECT a FROM ArCPORefinery a WHERE a.contract = :contract"),
    @NamedQuery(name = "ArCPORefinery.findByTrailer", query = "SELECT a FROM ArCPORefinery a WHERE a.trailer = :trailer"),
    @NamedQuery(name = "ArCPORefinery.findByCid", query = "SELECT a FROM ArCPORefinery a WHERE a.cid = :cid"),
    @NamedQuery(name = "ArCPORefinery.findByCname", query = "SELECT a FROM ArCPORefinery a WHERE a.cname = :cname"),
    @NamedQuery(name = "ArCPORefinery.findByCdate", query = "SELECT a FROM ArCPORefinery a WHERE a.cdate = :cdate"),
    @NamedQuery(name = "ArCPORefinery.findByAid", query = "SELECT a FROM ArCPORefinery a WHERE a.aid = :aid"),
    @NamedQuery(name = "ArCPORefinery.findByAname", query = "SELECT a FROM ArCPORefinery a WHERE a.aname = :aname"),
    @NamedQuery(name = "ArCPORefinery.findByAdesign", query = "SELECT a FROM ArCPORefinery a WHERE a.adesign = :adesign"),
    @NamedQuery(name = "ArCPORefinery.findByAdate", query = "SELECT a FROM ArCPORefinery a WHERE a.adate = :adate"),
    @NamedQuery(name = "ArCPORefinery.findByDay", query = "SELECT a FROM ArCPORefinery a WHERE a.day = :day"),
    @NamedQuery(name = "ArCPORefinery.findByMtRefinery", query = "SELECT a FROM ArCPORefinery a WHERE a.mtRefinery = :mtRefinery"),
    @NamedQuery(name = "ArCPORefinery.findByPl3", query = "SELECT a FROM ArCPORefinery a WHERE a.pl3 = :pl3"),
    @NamedQuery(name = "ArCPORefinery.findByDispatchNo", query = "SELECT a FROM ArCPORefinery a WHERE a.dispatchNo = :dispatchNo"),
    @NamedQuery(name = "ArCPORefinery.findByDriver", query = "SELECT a FROM ArCPORefinery a WHERE a.driver = :driver"),
    @NamedQuery(name = "ArCPORefinery.findByDispatchNoRefinery", query = "SELECT a FROM ArCPORefinery a WHERE a.dispatchNoRefinery = :dispatchNoRefinery"),
    @NamedQuery(name = "ArCPORefinery.findByDateRefinery", query = "SELECT a FROM ArCPORefinery a WHERE a.dateRefinery = :dateRefinery"),
    @NamedQuery(name = "ArCPORefinery.findById", query = "SELECT a FROM ArCPORefinery a WHERE a.id = :id")})
public class ArCPORefinery implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "transporter")
    private String transporter;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "mt")
    private String mt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "lot")
    private String lot;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "contract")
    private String contract;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "trailer")
    private String trailer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cid")
    private String cid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cname")
    private String cname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cdate")
    private String cdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "aid")
    private String aid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "aname")
    private String aname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "adesign")
    private String adesign;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "adate")
    private String adate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "day")
    private String day;
    @Basic(optional = false)
    @NotNull
    @Column(name = "mt_refinery")
    private double mtRefinery;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pl3")
    private String pl3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "dispatch_no")
    private String dispatchNo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "driver")
    private String driver;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "dispatch_no_refinery")
    private String dispatchNoRefinery;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_refinery")
    @Temporal(TemporalType.DATE)
    private Date dateRefinery;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    public ArCPORefinery() {
    }

    public ArCPORefinery(Integer id) {
        this.id = id;
    }

    public ArCPORefinery(Integer id, String transporter, String mt, String code, Date date, String lot, String contract, String trailer, String cid, String cname, String cdate, String aid, String aname, String adesign, String adate, String day, double mtRefinery, String pl3, String dispatchNo, String driver, String dispatchNoRefinery, Date dateRefinery) {
        this.id = id;
        this.transporter = transporter;
        this.mt = mt;
        this.code = code;
        this.date = date;
        this.lot = lot;
        this.contract = contract;
        this.trailer = trailer;
        this.cid = cid;
        this.cname = cname;
        this.cdate = cdate;
        this.aid = aid;
        this.aname = aname;
        this.adesign = adesign;
        this.adate = adate;
        this.day = day;
        this.mtRefinery = mtRefinery;
        this.pl3 = pl3;
        this.dispatchNo = dispatchNo;
        this.driver = driver;
        this.dispatchNoRefinery = dispatchNoRefinery;
        this.dateRefinery = dateRefinery;
    }

    public String getTransporter() {
        return transporter;
    }

    public void setTransporter(String transporter) {
        this.transporter = transporter;
    }

    public String getMt() {
        return mt;
    }

    public void setMt(String mt) {
        this.mt = mt;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCdate() {
        return cdate;
    }

    public void setCdate(String cdate) {
        this.cdate = cdate;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getAname() {
        return aname;
    }

    public void setAname(String aname) {
        this.aname = aname;
    }

    public String getAdesign() {
        return adesign;
    }

    public void setAdesign(String adesign) {
        this.adesign = adesign;
    }

    public String getAdate() {
        return adate;
    }

    public void setAdate(String adate) {
        this.adate = adate;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public double getMtRefinery() {
        return mtRefinery;
    }

    public void setMtRefinery(double mtRefinery) {
        this.mtRefinery = mtRefinery;
    }

    public String getPl3() {
        return pl3;
    }

    public void setPl3(String pl3) {
        this.pl3 = pl3;
    }

    public String getDispatchNo() {
        return dispatchNo;
    }

    public void setDispatchNo(String dispatchNo) {
        this.dispatchNo = dispatchNo;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getDispatchNoRefinery() {
        return dispatchNoRefinery;
    }

    public void setDispatchNoRefinery(String dispatchNoRefinery) {
        this.dispatchNoRefinery = dispatchNoRefinery;
    }

    public Date getDateRefinery() {
        return dateRefinery;
    }

    public void setDateRefinery(Date dateRefinery) {
        this.dateRefinery = dateRefinery;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArCPORefinery)) {
            return false;
        }
        ArCPORefinery other = (ArCPORefinery) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.ar.ArCPORefinery[ id=" + id + " ]";
    }
    
}
