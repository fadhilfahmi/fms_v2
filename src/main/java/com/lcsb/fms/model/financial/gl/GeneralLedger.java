/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.gl;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "gl_posting_distribute")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GeneralLedger.findAll", query = "SELECT g FROM GeneralLedger g"),
    @NamedQuery(name = "GeneralLedger.findByTarikh", query = "SELECT g FROM GeneralLedger g WHERE g.tarikh = :tarikh"),
    @NamedQuery(name = "GeneralLedger.findByCOACode", query = "SELECT g FROM GeneralLedger g WHERE g.cOACode = :cOACode"),
    @NamedQuery(name = "GeneralLedger.findByCOADesc", query = "SELECT g FROM GeneralLedger g WHERE g.cOADesc = :cOADesc"),
    @NamedQuery(name = "GeneralLedger.findByCTType", query = "SELECT g FROM GeneralLedger g WHERE g.cTType = :cTType"),
    @NamedQuery(name = "GeneralLedger.findByCTCode", query = "SELECT g FROM GeneralLedger g WHERE g.cTCode = :cTCode"),
    @NamedQuery(name = "GeneralLedger.findByCTDesc", query = "SELECT g FROM GeneralLedger g WHERE g.cTDesc = :cTDesc"),
    @NamedQuery(name = "GeneralLedger.findByLocLevel", query = "SELECT g FROM GeneralLedger g WHERE g.locLevel = :locLevel"),
    @NamedQuery(name = "GeneralLedger.findByLocCode", query = "SELECT g FROM GeneralLedger g WHERE g.locCode = :locCode"),
    @NamedQuery(name = "GeneralLedger.findByLocDesc", query = "SELECT g FROM GeneralLedger g WHERE g.locDesc = :locDesc"),
    @NamedQuery(name = "GeneralLedger.findByDebit", query = "SELECT g FROM GeneralLedger g WHERE g.debit = :debit"),
    @NamedQuery(name = "GeneralLedger.findByCredit", query = "SELECT g FROM GeneralLedger g WHERE g.credit = :credit"),
    @NamedQuery(name = "GeneralLedger.findByNoVoucher", query = "SELECT g FROM GeneralLedger g WHERE g.noVoucher = :noVoucher"),
    @NamedQuery(name = "GeneralLedger.findBySource", query = "SELECT g FROM GeneralLedger g WHERE g.source = :source"),
    @NamedQuery(name = "GeneralLedger.findByPostlevel", query = "SELECT g FROM GeneralLedger g WHERE g.postlevel = :postlevel"),
    @NamedQuery(name = "GeneralLedger.findByYear", query = "SELECT g FROM GeneralLedger g WHERE g.year = :year"),
    @NamedQuery(name = "GeneralLedger.findByPeriod", query = "SELECT g FROM GeneralLedger g WHERE g.period = :period"),
    @NamedQuery(name = "GeneralLedger.findByPostdate", query = "SELECT g FROM GeneralLedger g WHERE g.postdate = :postdate"),
    @NamedQuery(name = "GeneralLedger.findByStgcode", query = "SELECT g FROM GeneralLedger g WHERE g.stgcode = :stgcode"),
    @NamedQuery(name = "GeneralLedger.findByStgdesc", query = "SELECT g FROM GeneralLedger g WHERE g.stgdesc = :stgdesc"),
    @NamedQuery(name = "GeneralLedger.findByEntcode", query = "SELECT g FROM GeneralLedger g WHERE g.entcode = :entcode"),
    @NamedQuery(name = "GeneralLedger.findByEntdesc", query = "SELECT g FROM GeneralLedger g WHERE g.entdesc = :entdesc"),
    @NamedQuery(name = "GeneralLedger.findByAccstgentcode", query = "SELECT g FROM GeneralLedger g WHERE g.accstgentcode = :accstgentcode"),
    @NamedQuery(name = "GeneralLedger.findByEntflag", query = "SELECT g FROM GeneralLedger g WHERE g.entflag = :entflag"),
    @NamedQuery(name = "GeneralLedger.findByGcflag", query = "SELECT g FROM GeneralLedger g WHERE g.gcflag = :gcflag"),
    @NamedQuery(name = "GeneralLedger.findBySatype", query = "SELECT g FROM GeneralLedger g WHERE g.satype = :satype"),
    @NamedQuery(name = "GeneralLedger.findBySacode", query = "SELECT g FROM GeneralLedger g WHERE g.sacode = :sacode"),
    @NamedQuery(name = "GeneralLedger.findBySadesc", query = "SELECT g FROM GeneralLedger g WHERE g.sadesc = :sadesc"),
    @NamedQuery(name = "GeneralLedger.findById", query = "SELECT g FROM GeneralLedger g WHERE g.id = :id")})
public class GeneralLedger implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Tarikh")
    @Temporal(TemporalType.DATE)
    private Date tarikh;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "COACode")
    private String cOACode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "COADesc")
    private String cOADesc;
    @Size(max = 100)
    @Column(name = "CTType")
    private String cTType;
    @Size(max = 100)
    @Column(name = "CTCode")
    private String cTCode;
    @Size(max = 100)
    @Column(name = "CTDesc")
    private String cTDesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "LocLevel")
    private String locLevel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "LocCode")
    private String locCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "LocDesc")
    private String locDesc;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "Remark")
    private String remark;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Debit")
    private double debit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Credit")
    private double credit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NoVoucher")
    private String noVoucher;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Source")
    private String source;
    @Column(name = "postlevel")
    private Integer postlevel;
    @Basic(optional = false)
    @NotNull
    @Column(name = "year")
    private int year;
    @Basic(optional = false)
    @NotNull
    @Column(name = "period")
    private int period;
    @Basic(optional = false)
    @NotNull
    @Column(name = "postdate")
    @Temporal(TemporalType.DATE)
    private Date postdate;
    @Size(max = 100)
    @Column(name = "stgcode")
    private String stgcode;
    @Size(max = 100)
    @Column(name = "stgdesc")
    private String stgdesc;
    @Size(max = 100)
    @Column(name = "entcode")
    private String entcode;
    @Size(max = 100)
    @Column(name = "entdesc")
    private String entdesc;
    @Size(max = 100)
    @Column(name = "accstgentcode")
    private String accstgentcode;
    @Size(max = 100)
    @Column(name = "entflag")
    private String entflag;
    @Size(max = 100)
    @Column(name = "gcflag")
    private String gcflag;
    @Size(max = 50)
    @Column(name = "satype")
    private String satype;
    @Size(max = 50)
    @Column(name = "sacode")
    private String sacode;
    @Size(max = 100)
    @Column(name = "sadesc")
    private String sadesc;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    public GeneralLedger() {
    }

    public GeneralLedger(Long id) {
        this.id = id;
    }

    public GeneralLedger(Long id, Date tarikh, String cOACode, String cOADesc, String locLevel, String locCode, String locDesc, String remark, double debit, double credit, String noVoucher, String source, int year, int period, Date postdate) {
        this.id = id;
        this.tarikh = tarikh;
        this.cOACode = cOACode;
        this.cOADesc = cOADesc;
        this.locLevel = locLevel;
        this.locCode = locCode;
        this.locDesc = locDesc;
        this.remark = remark;
        this.debit = debit;
        this.credit = credit;
        this.noVoucher = noVoucher;
        this.source = source;
        this.year = year;
        this.period = period;
        this.postdate = postdate;
    }

    public Date getTarikh() {
        return tarikh;
    }

    public void setTarikh(Date tarikh) {
        this.tarikh = tarikh;
    }

    public String getCOACode() {
        return cOACode;
    }

    public void setCOACode(String cOACode) {
        this.cOACode = cOACode;
    }

    public String getCOADesc() {
        return cOADesc;
    }

    public void setCOADesc(String cOADesc) {
        this.cOADesc = cOADesc;
    }

    public String getCTType() {
        return cTType;
    }

    public void setCTType(String cTType) {
        this.cTType = cTType;
    }

    public String getCTCode() {
        return cTCode;
    }

    public void setCTCode(String cTCode) {
        this.cTCode = cTCode;
    }

    public String getCTDesc() {
        return cTDesc;
    }

    public void setCTDesc(String cTDesc) {
        this.cTDesc = cTDesc;
    }

    public String getLocLevel() {
        return locLevel;
    }

    public void setLocLevel(String locLevel) {
        this.locLevel = locLevel;
    }

    public String getLocCode() {
        return locCode;
    }

    public void setLocCode(String locCode) {
        this.locCode = locCode;
    }

    public String getLocDesc() {
        return locDesc;
    }

    public void setLocDesc(String locDesc) {
        this.locDesc = locDesc;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public double getDebit() {
        return debit;
    }

    public void setDebit(double debit) {
        this.debit = debit;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public String getNoVoucher() {
        return noVoucher;
    }

    public void setNoVoucher(String noVoucher) {
        this.noVoucher = noVoucher;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Integer getPostlevel() {
        return postlevel;
    }

    public void setPostlevel(Integer postlevel) {
        this.postlevel = postlevel;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public Date getPostdate() {
        return postdate;
    }

    public void setPostdate(Date postdate) {
        this.postdate = postdate;
    }

    public String getStgcode() {
        return stgcode;
    }

    public void setStgcode(String stgcode) {
        this.stgcode = stgcode;
    }

    public String getStgdesc() {
        return stgdesc;
    }

    public void setStgdesc(String stgdesc) {
        this.stgdesc = stgdesc;
    }

    public String getEntcode() {
        return entcode;
    }

    public void setEntcode(String entcode) {
        this.entcode = entcode;
    }

    public String getEntdesc() {
        return entdesc;
    }

    public void setEntdesc(String entdesc) {
        this.entdesc = entdesc;
    }

    public String getAccstgentcode() {
        return accstgentcode;
    }

    public void setAccstgentcode(String accstgentcode) {
        this.accstgentcode = accstgentcode;
    }

    public String getEntflag() {
        return entflag;
    }

    public void setEntflag(String entflag) {
        this.entflag = entflag;
    }

    public String getGcflag() {
        return gcflag;
    }

    public void setGcflag(String gcflag) {
        this.gcflag = gcflag;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GeneralLedger)) {
            return false;
        }
        GeneralLedger other = (GeneralLedger) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.gl.GeneralLedger[ id=" + id + " ]";
    }
    
}
