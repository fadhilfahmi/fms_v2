/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.fms.model.financial.gl;

/**
 *
 * @author Dell
 */
public class PaymentVoucher {
    
    private String voucherno;
    private String refer;
    private String tarikh;
    private String paidtype;
    private String paidcode;
    private String paidname;
    private String paidaddress;
    private String paidstate;
    private String paidpostcode;
    private String gstid;
    private String remarks;
    private String racoacode;
    private String racoadesc;
    private Double amount;
    private String rm;
    private String paymentmode;
    private String bankname;
    private String bankcode;
    private String year;
    private String cekno;
    private String estatename;
    private String estatecode;
    private String period;
    
    //not in form
    private String appid;
    private String appname;
    private String apposition;
    private String approvedate;
    private String preid;
    private String prename;
    private String preposition;
    private String preparedate;
    private String flag;
    private String checkid;
    private String checkname;
    private String checkposition;
    private String checkdate;
    

    public String getVoucherno() {
        return voucherno;
    }

    public void setVoucherno(String voucherno) {
        this.voucherno = voucherno;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getTarikh() {
        return tarikh;
    }

    public void setTarikh(String tarikh) {
        this.tarikh = tarikh;
    }

    public String getPaidtype() {
        return paidtype;
    }

    public void setPaidtype(String paidtype) {
        this.paidtype = paidtype;
    }

    public String getPaidcode() {
        return paidcode;
    }

    public void setPaidcode(String paidcode) {
        this.paidcode = paidcode;
    }

    public String getPaidname() {
        return paidname;
    }

    public void setPaidname(String paidname) {
        this.paidname = paidname;
    }

    public String getPaidaddress() {
        return paidaddress;
    }

    public void setPaidaddress(String paidaddress) {
        this.paidaddress = paidaddress;
    }

    public String getPaidstate() {
        return paidstate;
    }

    public void setPaidstate(String paidstate) {
        this.paidstate = paidstate;
    }

    public String getPaidpostcode() {
        return paidpostcode;
    }

    public void setPaidpostcode(String paidpostcode) {
        this.paidpostcode = paidpostcode;
    }

    public String getGstid() {
        return gstid;
    }

    public void setGstid(String gstid) {
        this.gstid = gstid;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRacoacode() {
        return racoacode;
    }

    public void setRacoacode(String racoacode) {
        this.racoacode = racoacode;
    }

    public String getRacoadesc() {
        return racoadesc;
    }

    public void setRacoadesc(String racoadesc) {
        this.racoadesc = racoadesc;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getRm() {
        return rm;
    }

    public void setRm(String rm) {
        this.rm = rm;
    }

    public String getPaymentmode() {
        return paymentmode;
    }

    public void setPaymentmode(String paymentmode) {
        this.paymentmode = paymentmode;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCekno() {
        return cekno;
    }

    public void setCekno(String cekno) {
        this.cekno = cekno;
    }

    public String getEstatename() {
        return estatename;
    }

    public void setEstatename(String estatename) {
        this.estatename = estatename;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getApposition() {
        return apposition;
    }

    public void setApposition(String apposition) {
        this.apposition = apposition;
    }

    public String getApprovedate() {
        return approvedate;
    }

    public void setApprovedate(String approvedate) {
        this.approvedate = approvedate;
    }

    public String getPreid() {
        return preid;
    }

    public void setPreid(String preid) {
        this.preid = preid;
    }

    public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        this.prename = prename;
    }

    public String getPreposition() {
        return preposition;
    }

    public void setPreposition(String preposition) {
        this.preposition = preposition;
    }

    public String getPreparedate() {
        return preparedate;
    }

    public void setPreparedate(String preparedate) {
        this.preparedate = preparedate;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCheckid() {
        return checkid;
    }

    public void setCheckid(String checkid) {
        this.checkid = checkid;
    }

    public String getCheckname() {
        return checkname;
    }

    public void setCheckname(String checkname) {
        this.checkname = checkname;
    }

    public String getCheckposition() {
        return checkposition;
    }

    public void setCheckposition(String checkposition) {
        this.checkposition = checkposition;
    }

    public String getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(String checkdate) {
        this.checkdate = checkdate;
    }
    
    

    
}
