/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.tx;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "tx_output_detail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TxOutputDetail.findAll", query = "SELECT t FROM TxOutputDetail t"),
    @NamedQuery(name = "TxOutputDetail.findByRefer", query = "SELECT t FROM TxOutputDetail t WHERE t.txOutputDetailPK.refer = :refer"),
    @NamedQuery(name = "TxOutputDetail.findByDate", query = "SELECT t FROM TxOutputDetail t WHERE t.date = :date"),
    @NamedQuery(name = "TxOutputDetail.findByYear", query = "SELECT t FROM TxOutputDetail t WHERE t.year = :year"),
    @NamedQuery(name = "TxOutputDetail.findByPeriod", query = "SELECT t FROM TxOutputDetail t WHERE t.period = :period"),
    @NamedQuery(name = "TxOutputDetail.findByTaxperiodfrom", query = "SELECT t FROM TxOutputDetail t WHERE t.taxperiodfrom = :taxperiodfrom"),
    @NamedQuery(name = "TxOutputDetail.findByTaxperiodto", query = "SELECT t FROM TxOutputDetail t WHERE t.taxperiodto = :taxperiodto"),
    @NamedQuery(name = "TxOutputDetail.findByTaxamt", query = "SELECT t FROM TxOutputDetail t WHERE t.taxamt = :taxamt"),
    @NamedQuery(name = "TxOutputDetail.findByTaxcode", query = "SELECT t FROM TxOutputDetail t WHERE t.txOutputDetailPK.taxcode = :taxcode"),
    @NamedQuery(name = "TxOutputDetail.findByTaxrate", query = "SELECT t FROM TxOutputDetail t WHERE t.taxrate = :taxrate"),
    @NamedQuery(name = "TxOutputDetail.findByTaxcoacode", query = "SELECT t FROM TxOutputDetail t WHERE t.taxcoacode = :taxcoacode"),
    @NamedQuery(name = "TxOutputDetail.findByTaxcoadescp", query = "SELECT t FROM TxOutputDetail t WHERE t.taxcoadescp = :taxcoadescp"),
    @NamedQuery(name = "TxOutputDetail.findByGstid", query = "SELECT t FROM TxOutputDetail t WHERE t.gstid = :gstid"),
    @NamedQuery(name = "TxOutputDetail.findByVoucherno", query = "SELECT t FROM TxOutputDetail t WHERE t.txOutputDetailPK.voucherno = :voucherno"),
    @NamedQuery(name = "TxOutputDetail.findByTrxtype", query = "SELECT t FROM TxOutputDetail t WHERE t.trxtype = :trxtype"),
    @NamedQuery(name = "TxOutputDetail.findByCompcode", query = "SELECT t FROM TxOutputDetail t WHERE t.compcode = :compcode"),
    @NamedQuery(name = "TxOutputDetail.findByCompname", query = "SELECT t FROM TxOutputDetail t WHERE t.compname = :compname"),
    @NamedQuery(name = "TxOutputDetail.findByEstcode", query = "SELECT t FROM TxOutputDetail t WHERE t.estcode = :estcode"),
    @NamedQuery(name = "TxOutputDetail.findByEstname", query = "SELECT t FROM TxOutputDetail t WHERE t.estname = :estname"),
    @NamedQuery(name = "TxOutputDetail.findByRemarks", query = "SELECT t FROM TxOutputDetail t WHERE t.remarks = :remarks"),
    @NamedQuery(name = "TxOutputDetail.findByAmount", query = "SELECT t FROM TxOutputDetail t WHERE t.amount = :amount"),
    @NamedQuery(name = "TxOutputDetail.findByTaxamtDt", query = "SELECT t FROM TxOutputDetail t WHERE t.taxamtDt = :taxamtDt"),
    @NamedQuery(name = "TxOutputDetail.findByTaxamtCt", query = "SELECT t FROM TxOutputDetail t WHERE t.taxamtCt = :taxamtCt"),
    @NamedQuery(name = "TxOutputDetail.findByAmountDt", query = "SELECT t FROM TxOutputDetail t WHERE t.amountDt = :amountDt"),
    @NamedQuery(name = "TxOutputDetail.findByAmountCt", query = "SELECT t FROM TxOutputDetail t WHERE t.amountCt = :amountCt"),
    @NamedQuery(name = "TxOutputDetail.findByVoucherrefer", query = "SELECT t FROM TxOutputDetail t WHERE t.voucherrefer = :voucherrefer"),
    @NamedQuery(name = "TxOutputDetail.findByRefno", query = "SELECT t FROM TxOutputDetail t WHERE t.txOutputDetailPK.refno = :refno"),
    @NamedQuery(name = "TxOutputDetail.findByBaddebt", query = "SELECT t FROM TxOutputDetail t WHERE t.baddebt = :baddebt")})
public class TxOutputDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TxOutputDetailPK txOutputDetailPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Basic(optional = false)
    @NotNull
    @Column(name = "year")
    private int year;
    @Basic(optional = false)
    @NotNull
    @Column(name = "period")
    private int period;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxperiodfrom")
    @Temporal(TemporalType.DATE)
    private Date taxperiodfrom;
    @Column(name = "taxperiodto")
    @Temporal(TemporalType.DATE)
    private Date taxperiodto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxamt")
    private double taxamt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxrate")
    private double taxrate;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "taxdescp")
    private String taxdescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "taxcoacode")
    private String taxcoacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcoadescp")
    private String taxcoadescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "gstid")
    private String gstid;
    @Size(max = 50)
    @Column(name = "trxtype")
    private String trxtype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "compcode")
    private String compcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "compname")
    private String compname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estcode")
    private String estcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estname")
    private String estname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxamt_dt")
    private double taxamtDt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxamt_ct")
    private double taxamtCt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount_dt")
    private double amountDt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount_ct")
    private double amountCt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "voucherrefer")
    private String voucherrefer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "baddebt")
    private String baddebt;

    public TxOutputDetail() {
    }

    public TxOutputDetail(TxOutputDetailPK txOutputDetailPK) {
        this.txOutputDetailPK = txOutputDetailPK;
    }

    public TxOutputDetail(TxOutputDetailPK txOutputDetailPK, Date date, int year, int period, Date taxperiodfrom, double taxamt, double taxrate, String taxdescp, String taxcoacode, String taxcoadescp, String gstid, String compcode, String compname, String estcode, String estname, String remarks, double amount, double taxamtDt, double taxamtCt, double amountDt, double amountCt, String voucherrefer, String baddebt) {
        this.txOutputDetailPK = txOutputDetailPK;
        this.date = date;
        this.year = year;
        this.period = period;
        this.taxperiodfrom = taxperiodfrom;
        this.taxamt = taxamt;
        this.taxrate = taxrate;
        this.taxdescp = taxdescp;
        this.taxcoacode = taxcoacode;
        this.taxcoadescp = taxcoadescp;
        this.gstid = gstid;
        this.compcode = compcode;
        this.compname = compname;
        this.estcode = estcode;
        this.estname = estname;
        this.remarks = remarks;
        this.amount = amount;
        this.taxamtDt = taxamtDt;
        this.taxamtCt = taxamtCt;
        this.amountDt = amountDt;
        this.amountCt = amountCt;
        this.voucherrefer = voucherrefer;
        this.baddebt = baddebt;
    }

    public TxOutputDetail(String refer, String taxcode, String voucherno, String refno) {
        this.txOutputDetailPK = new TxOutputDetailPK(refer, taxcode, voucherno, refno);
    }

    public TxOutputDetailPK getTxOutputDetailPK() {
        return txOutputDetailPK;
    }

    public void setTxOutputDetailPK(TxOutputDetailPK txOutputDetailPK) {
        this.txOutputDetailPK = txOutputDetailPK;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public Date getTaxperiodfrom() {
        return taxperiodfrom;
    }

    public void setTaxperiodfrom(Date taxperiodfrom) {
        this.taxperiodfrom = taxperiodfrom;
    }

    public Date getTaxperiodto() {
        return taxperiodto;
    }

    public void setTaxperiodto(Date taxperiodto) {
        this.taxperiodto = taxperiodto;
    }

    public double getTaxamt() {
        return taxamt;
    }

    public void setTaxamt(double taxamt) {
        this.taxamt = taxamt;
    }

    public double getTaxrate() {
        return taxrate;
    }

    public void setTaxrate(double taxrate) {
        this.taxrate = taxrate;
    }

    public String getTaxdescp() {
        return taxdescp;
    }

    public void setTaxdescp(String taxdescp) {
        this.taxdescp = taxdescp;
    }

    public String getTaxcoacode() {
        return taxcoacode;
    }

    public void setTaxcoacode(String taxcoacode) {
        this.taxcoacode = taxcoacode;
    }

    public String getTaxcoadescp() {
        return taxcoadescp;
    }

    public void setTaxcoadescp(String taxcoadescp) {
        this.taxcoadescp = taxcoadescp;
    }

    public String getGstid() {
        return gstid;
    }

    public void setGstid(String gstid) {
        this.gstid = gstid;
    }

    public String getTrxtype() {
        return trxtype;
    }

    public void setTrxtype(String trxtype) {
        this.trxtype = trxtype;
    }

    public String getCompcode() {
        return compcode;
    }

    public void setCompcode(String compcode) {
        this.compcode = compcode;
    }

    public String getCompname() {
        return compname;
    }

    public void setCompname(String compname) {
        this.compname = compname;
    }

    public String getEstcode() {
        return estcode;
    }

    public void setEstcode(String estcode) {
        this.estcode = estcode;
    }

    public String getEstname() {
        return estname;
    }

    public void setEstname(String estname) {
        this.estname = estname;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getTaxamtDt() {
        return taxamtDt;
    }

    public void setTaxamtDt(double taxamtDt) {
        this.taxamtDt = taxamtDt;
    }

    public double getTaxamtCt() {
        return taxamtCt;
    }

    public void setTaxamtCt(double taxamtCt) {
        this.taxamtCt = taxamtCt;
    }

    public double getAmountDt() {
        return amountDt;
    }

    public void setAmountDt(double amountDt) {
        this.amountDt = amountDt;
    }

    public double getAmountCt() {
        return amountCt;
    }

    public void setAmountCt(double amountCt) {
        this.amountCt = amountCt;
    }

    public String getVoucherrefer() {
        return voucherrefer;
    }

    public void setVoucherrefer(String voucherrefer) {
        this.voucherrefer = voucherrefer;
    }

    public String getBaddebt() {
        return baddebt;
    }

    public void setBaddebt(String baddebt) {
        this.baddebt = baddebt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (txOutputDetailPK != null ? txOutputDetailPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TxOutputDetail)) {
            return false;
        }
        TxOutputDetail other = (TxOutputDetail) object;
        if ((this.txOutputDetailPK == null && other.txOutputDetailPK != null) || (this.txOutputDetailPK != null && !this.txOutputDetailPK.equals(other.txOutputDetailPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.tx.TxOutputDetail[ txOutputDetailPK=" + txOutputDetailPK + " ]";
    }
    
}
