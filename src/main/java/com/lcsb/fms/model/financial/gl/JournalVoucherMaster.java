/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.gl;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class JournalVoucherMaster {
    
    private JournalVoucher master;
    private List<JournalVoucherItem> listItem;

    public JournalVoucher getMaster() {
        return master;
    }

    public void setMaster(JournalVoucher master) {
        this.master = master;
    }

    public List<JournalVoucherItem> getListItem() {
        return listItem;
    }

    public void setListItem(List<JournalVoucherItem> listItem) {
        this.listItem = listItem;
    }

    
    
}
