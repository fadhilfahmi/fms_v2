/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class BankBookMaster {
    
    private double bfAmount;
    private double cfAmount;
    private List<BankBook> listBankBook;

    public double getBfAmount() {
        return bfAmount;
    }

    public void setBfAmount(double bfAmount) {
        this.bfAmount = bfAmount;
    }

    public double getCfAmount() {
        return cfAmount;
    }

    public void setCfAmount(double cfAmount) {
        this.cfAmount = cfAmount;
    }

    public List<BankBook> getListBankBook() {
        return listBankBook;
    }

    public void setListBankBook(List<BankBook> listBankBook) {
        this.listBankBook = listBankBook;
    }
    
    
    
}
