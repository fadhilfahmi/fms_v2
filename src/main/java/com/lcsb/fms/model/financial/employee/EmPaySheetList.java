/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.employee;

import com.lcsb.fms.model.setup.company.staff.Staff;
import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class EmPaySheetList {
    
    private List<String> staffID;
    private List<String> staffName;
    private List<String> staffDesignation;
    private List<Staff> staffAll;
    private int pageCount;
    private List<EmPayrollType> listEarn;
    private List<EmPayrollType> listDeduct;
    private EmPayroll master;

    public EmPayroll getMaster() {
        return master;
    }

    public void setMaster(EmPayroll master) {
        this.master = master;
    }

    public List<EmPayrollType> getListEarn() {
        return listEarn;
    }

    public void setListEarn(List<EmPayrollType> listEarn) {
        this.listEarn = listEarn;
    }

    public List<EmPayrollType> getListDeduct() {
        return listDeduct;
    }

    public void setListDeduct(List<EmPayrollType> listDeduct) {
        this.listDeduct = listDeduct;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public List<Staff> getStaffAll() {
        return staffAll;
    }

    public void setStaffAll(List<Staff> staffAll) {
        this.staffAll = staffAll;
    }

    public List<String> getStaffID() {
        return staffID;
    }

    public void setStaffID(List<String> staffID) {
        this.staffID = staffID;
    }

    public List<String> getStaffName() {
        return staffName;
    }

    public void setStaffName(List<String> staffName) {
        this.staffName = staffName;
    }

    public List<String> getStaffDesignation() {
        return staffDesignation;
    }

    public void setStaffDesignation(List<String> staffDesignation) {
        this.staffDesignation = staffDesignation;
    }
    
    
    
    
}
