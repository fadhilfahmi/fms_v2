/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.employee;

import com.lcsb.fms.model.setup.company.staff.Staff;
import java.util.List;

/**
 *
 * @author Dell
 */
public class EmPayrollMaster {
    
    private EmPayroll emPayroll;
    private Staff getStaff;
    private List<EmPayrollEarning> listEarn;
    private List<EmPayrollDeduction> listDeduct;
    private boolean confirmSlip;

    public boolean isConfirmSlip() {
        return confirmSlip;
    }

    public void setConfirmSlip(boolean confirmSlip) {
        this.confirmSlip = confirmSlip;
    }

    public Staff getGetStaff() {
        return getStaff;
    }

    public void setGetStaff(Staff getStaff) {
        this.getStaff = getStaff;
    }

    public EmPayroll getEmPayroll() {
        return emPayroll;
    }

    public void setEmPayroll(EmPayroll emPayroll) {
        this.emPayroll = emPayroll;
    }

    public List<EmPayrollEarning> getListEarn() {
        return listEarn;
    }

    public void setListEarn(List<EmPayrollEarning> listEarn) {
        this.listEarn = listEarn;
    }

    public List<EmPayrollDeduction> getListDeduct() {
        return listDeduct;
    }

    public void setListDeduct(List<EmPayrollDeduction> listDeduct) {
        this.listDeduct = listDeduct;
    }
    
    
    
}
