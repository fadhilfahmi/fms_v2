/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ar;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ar_debitnote")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ArDebitNote.findAll", query = "SELECT a FROM ArDebitNote a"),
    @NamedQuery(name = "ArDebitNote.findByNoteno", query = "SELECT a FROM ArDebitNote a WHERE a.noteno = :noteno"),
    @NamedQuery(name = "ArDebitNote.findByJvrefno", query = "SELECT a FROM ArDebitNote a WHERE a.jvrefno = :jvrefno"),
    @NamedQuery(name = "ArDebitNote.findByYear", query = "SELECT a FROM ArDebitNote a WHERE a.year = :year"),
    @NamedQuery(name = "ArDebitNote.findByPeriod", query = "SELECT a FROM ArDebitNote a WHERE a.period = :period"),
    @NamedQuery(name = "ArDebitNote.findByNotedate", query = "SELECT a FROM ArDebitNote a WHERE a.notedate = :notedate"),
    @NamedQuery(name = "ArDebitNote.findByLoclevel", query = "SELECT a FROM ArDebitNote a WHERE a.loclevel = :loclevel"),
    @NamedQuery(name = "ArDebitNote.findByLoccode", query = "SELECT a FROM ArDebitNote a WHERE a.loccode = :loccode"),
    @NamedQuery(name = "ArDebitNote.findByLocname", query = "SELECT a FROM ArDebitNote a WHERE a.locname = :locname"),
    @NamedQuery(name = "ArDebitNote.findByAcccode", query = "SELECT a FROM ArDebitNote a WHERE a.acccode = :acccode"),
    @NamedQuery(name = "ArDebitNote.findByAccdescp", query = "SELECT a FROM ArDebitNote a WHERE a.accdescp = :accdescp"),
    @NamedQuery(name = "ArDebitNote.findByBuyercode", query = "SELECT a FROM ArDebitNote a WHERE a.buyercode = :buyercode"),
    @NamedQuery(name = "ArDebitNote.findByBuyername", query = "SELECT a FROM ArDebitNote a WHERE a.buyername = :buyername"),
    @NamedQuery(name = "ArDebitNote.findByProdcode", query = "SELECT a FROM ArDebitNote a WHERE a.prodcode = :prodcode"),
    @NamedQuery(name = "ArDebitNote.findByProdname", query = "SELECT a FROM ArDebitNote a WHERE a.prodname = :prodname"),
    @NamedQuery(name = "ArDebitNote.findByProdacc", query = "SELECT a FROM ArDebitNote a WHERE a.prodacc = :prodacc"),
    @NamedQuery(name = "ArDebitNote.findByKontrak", query = "SELECT a FROM ArDebitNote a WHERE a.kontrak = :kontrak"),
    @NamedQuery(name = "ArDebitNote.findByPostflag", query = "SELECT a FROM ArDebitNote a WHERE a.postflag = :postflag"),
    @NamedQuery(name = "ArDebitNote.findByPid", query = "SELECT a FROM ArDebitNote a WHERE a.pid = :pid"),
    @NamedQuery(name = "ArDebitNote.findByPname", query = "SELECT a FROM ArDebitNote a WHERE a.pname = :pname"),
    @NamedQuery(name = "ArDebitNote.findByPdate", query = "SELECT a FROM ArDebitNote a WHERE a.pdate = :pdate"),
    @NamedQuery(name = "ArDebitNote.findByCheckid", query = "SELECT a FROM ArDebitNote a WHERE a.checkid = :checkid"),
    @NamedQuery(name = "ArDebitNote.findByCheckname", query = "SELECT a FROM ArDebitNote a WHERE a.checkname = :checkname"),
    @NamedQuery(name = "ArDebitNote.findByCheckdate", query = "SELECT a FROM ArDebitNote a WHERE a.checkdate = :checkdate"),
    @NamedQuery(name = "ArDebitNote.findByAppid", query = "SELECT a FROM ArDebitNote a WHERE a.appid = :appid"),
    @NamedQuery(name = "ArDebitNote.findByAppname", query = "SELECT a FROM ArDebitNote a WHERE a.appname = :appname"),
    @NamedQuery(name = "ArDebitNote.findByAppdate", query = "SELECT a FROM ArDebitNote a WHERE a.appdate = :appdate"),
    @NamedQuery(name = "ArDebitNote.findByAppdesign", query = "SELECT a FROM ArDebitNote a WHERE a.appdesign = :appdesign"),
    @NamedQuery(name = "ArDebitNote.findByEstcode", query = "SELECT a FROM ArDebitNote a WHERE a.estcode = :estcode"),
    @NamedQuery(name = "ArDebitNote.findByEstname", query = "SELECT a FROM ArDebitNote a WHERE a.estname = :estname"),
    @NamedQuery(name = "ArDebitNote.findByTotal", query = "SELECT a FROM ArDebitNote a WHERE a.total = :total"),
    @NamedQuery(name = "ArDebitNote.findBySatype", query = "SELECT a FROM ArDebitNote a WHERE a.satype = :satype"),
    @NamedQuery(name = "ArDebitNote.findBySacode", query = "SELECT a FROM ArDebitNote a WHERE a.sacode = :sacode"),
    @NamedQuery(name = "ArDebitNote.findBySadecs", query = "SELECT a FROM ArDebitNote a WHERE a.sadecs = :sadecs")})
public class ArDebitNote implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "noteno")
    private String noteno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "jvrefno")
    private String jvrefno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "period")
    private String period;
    @Basic(optional = false)
    @NotNull
    @Column(name = "notedate")
    @Temporal(TemporalType.DATE)
    private Date notedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loclevel")
    private String loclevel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loccode")
    private String loccode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "locname")
    private String locname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "acccode")
    private String acccode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "accdescp")
    private String accdescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "buyercode")
    private String buyercode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "buyername")
    private String buyername;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prodcode")
    private String prodcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prodname")
    private String prodname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prodacc")
    private String prodacc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "kontrak")
    private String kontrak;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "postflag")
    private String postflag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pid")
    private String pid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pname")
    private String pname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pdate")
    private String pdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkid")
    private String checkid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkname")
    private String checkname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkdate")
    private String checkdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appid")
    private String appid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appname")
    private String appname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appdate")
    private String appdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appdesign")
    private String appdesign;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "estcode")
    private String estcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estname")
    private String estname;
    @Lob
    @Size(max = 65535)
    @Column(name = "remark")
    private String remark;
    @Basic(optional = false)
    @NotNull
    @Column(name = "total")
    private double total;
    @Size(max = 20)
    @Column(name = "satype")
    private String satype;
    @Size(max = 4)
    @Column(name = "sacode")
    private String sacode;
    @Size(max = 100)
    @Column(name = "sadecs")
    private String sadecs;

    public ArDebitNote() {
    }

    public ArDebitNote(String noteno) {
        this.noteno = noteno;
    }

    public ArDebitNote(String noteno, String jvrefno, String year, String period, Date notedate, String loclevel, String loccode, String locname, String acccode, String accdescp, String buyercode, String buyername, String prodcode, String prodname, String prodacc, String kontrak, String postflag, String pid, String pname, String pdate, String checkid, String checkname, String checkdate, String appid, String appname, String appdate, String appdesign, String estcode, String estname, double total) {
        this.noteno = noteno;
        this.jvrefno = jvrefno;
        this.year = year;
        this.period = period;
        this.notedate = notedate;
        this.loclevel = loclevel;
        this.loccode = loccode;
        this.locname = locname;
        this.acccode = acccode;
        this.accdescp = accdescp;
        this.buyercode = buyercode;
        this.buyername = buyername;
        this.prodcode = prodcode;
        this.prodname = prodname;
        this.prodacc = prodacc;
        this.kontrak = kontrak;
        this.postflag = postflag;
        this.pid = pid;
        this.pname = pname;
        this.pdate = pdate;
        this.checkid = checkid;
        this.checkname = checkname;
        this.checkdate = checkdate;
        this.appid = appid;
        this.appname = appname;
        this.appdate = appdate;
        this.appdesign = appdesign;
        this.estcode = estcode;
        this.estname = estname;
        this.total = total;
    }

    public String getNoteno() {
        return noteno;
    }

    public void setNoteno(String noteno) {
        this.noteno = noteno;
    }

    public String getJvrefno() {
        return jvrefno;
    }

    public void setJvrefno(String jvrefno) {
        this.jvrefno = jvrefno;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public Date getNotedate() {
        return notedate;
    }

    public void setNotedate(Date notedate) {
        this.notedate = notedate;
    }

    public String getLoclevel() {
        return loclevel;
    }

    public void setLoclevel(String loclevel) {
        this.loclevel = loclevel;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLocname() {
        return locname;
    }

    public void setLocname(String locname) {
        this.locname = locname;
    }

    public String getAcccode() {
        return acccode;
    }

    public void setAcccode(String acccode) {
        this.acccode = acccode;
    }

    public String getAccdescp() {
        return accdescp;
    }

    public void setAccdescp(String accdescp) {
        this.accdescp = accdescp;
    }

    public String getBuyercode() {
        return buyercode;
    }

    public void setBuyercode(String buyercode) {
        this.buyercode = buyercode;
    }

    public String getBuyername() {
        return buyername;
    }

    public void setBuyername(String buyername) {
        this.buyername = buyername;
    }

    public String getProdcode() {
        return prodcode;
    }

    public void setProdcode(String prodcode) {
        this.prodcode = prodcode;
    }

    public String getProdname() {
        return prodname;
    }

    public void setProdname(String prodname) {
        this.prodname = prodname;
    }

    public String getProdacc() {
        return prodacc;
    }

    public void setProdacc(String prodacc) {
        this.prodacc = prodacc;
    }

    public String getKontrak() {
        return kontrak;
    }

    public void setKontrak(String kontrak) {
        this.kontrak = kontrak;
    }

    public String getPostflag() {
        return postflag;
    }

    public void setPostflag(String postflag) {
        this.postflag = postflag;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPdate() {
        return pdate;
    }

    public void setPdate(String pdate) {
        this.pdate = pdate;
    }

    public String getCheckid() {
        return checkid;
    }

    public void setCheckid(String checkid) {
        this.checkid = checkid;
    }

    public String getCheckname() {
        return checkname;
    }

    public void setCheckname(String checkname) {
        this.checkname = checkname;
    }

    public String getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(String checkdate) {
        this.checkdate = checkdate;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getAppdate() {
        return appdate;
    }

    public void setAppdate(String appdate) {
        this.appdate = appdate;
    }

    public String getAppdesign() {
        return appdesign;
    }

    public void setAppdesign(String appdesign) {
        this.appdesign = appdesign;
    }

    public String getEstcode() {
        return estcode;
    }

    public void setEstcode(String estcode) {
        this.estcode = estcode;
    }

    public String getEstname() {
        return estname;
    }

    public void setEstname(String estname) {
        this.estname = estname;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadecs() {
        return sadecs;
    }

    public void setSadecs(String sadecs) {
        this.sadecs = sadecs;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noteno != null ? noteno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArDebitNote)) {
            return false;
        }
        ArDebitNote other = (ArDebitNote) object;
        if ((this.noteno == null && other.noteno != null) || (this.noteno != null && !this.noteno.equals(other.noteno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.ar.ArDebitNote[ noteno=" + noteno + " ]";
    }
    
}
