/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "bank_reconcilation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BankReconcilation.findAll", query = "SELECT b FROM BankReconcilation b"),
    @NamedQuery(name = "BankReconcilation.findByVoucherdate", query = "SELECT b FROM BankReconcilation b WHERE b.voucherdate = :voucherdate"),
    @NamedQuery(name = "BankReconcilation.findByVoucherno", query = "SELECT b FROM BankReconcilation b WHERE b.bankReconcilationPK.voucherno = :voucherno"),
    @NamedQuery(name = "BankReconcilation.findByCek", query = "SELECT b FROM BankReconcilation b WHERE b.cek = :cek"),
    @NamedQuery(name = "BankReconcilation.findByAmountpv", query = "SELECT b FROM BankReconcilation b WHERE b.amountpv = :amountpv"),
    @NamedQuery(name = "BankReconcilation.findByStatus", query = "SELECT b FROM BankReconcilation b WHERE b.status = :status"),
    @NamedQuery(name = "BankReconcilation.findByRemarks", query = "SELECT b FROM BankReconcilation b WHERE b.remarks = :remarks"),
    @NamedQuery(name = "BankReconcilation.findByDateclear", query = "SELECT b FROM BankReconcilation b WHERE b.dateclear = :dateclear"),
    @NamedQuery(name = "BankReconcilation.findByAmountclear", query = "SELECT b FROM BankReconcilation b WHERE b.amountclear = :amountclear"),
    @NamedQuery(name = "BankReconcilation.findByVarians", query = "SELECT b FROM BankReconcilation b WHERE b.varians = :varians"),
    @NamedQuery(name = "BankReconcilation.findByBankcode", query = "SELECT b FROM BankReconcilation b WHERE b.bankcode = :bankcode"),
    @NamedQuery(name = "BankReconcilation.findByBankname", query = "SELECT b FROM BankReconcilation b WHERE b.bankname = :bankname"),
    @NamedQuery(name = "BankReconcilation.findByYear", query = "SELECT b FROM BankReconcilation b WHERE b.bankReconcilationPK.year = :year"),
    @NamedQuery(name = "BankReconcilation.findByPeriod", query = "SELECT b FROM BankReconcilation b WHERE b.bankReconcilationPK.period = :period"),
    @NamedQuery(name = "BankReconcilation.findByPaid", query = "SELECT b FROM BankReconcilation b WHERE b.paid = :paid"),
    @NamedQuery(name = "BankReconcilation.findByPaymode", query = "SELECT b FROM BankReconcilation b WHERE b.paymode = :paymode"),
    @NamedQuery(name = "BankReconcilation.findByCreatedvoucher", query = "SELECT b FROM BankReconcilation b WHERE b.createdvoucher = :createdvoucher"),
    @NamedQuery(name = "BankReconcilation.findByEstatecode", query = "SELECT b FROM BankReconcilation b WHERE b.estatecode = :estatecode"),
    @NamedQuery(name = "BankReconcilation.findByDepan", query = "SELECT b FROM BankReconcilation b WHERE b.bankReconcilationPK.depan = :depan"),
    @NamedQuery(name = "BankReconcilation.findByRefer", query = "SELECT b FROM BankReconcilation b WHERE b.bankReconcilationPK.refer = :refer"),
    @NamedQuery(name = "BankReconcilation.findByFlag", query = "SELECT b FROM BankReconcilation b WHERE b.flag = :flag"),
    @NamedQuery(name = "BankReconcilation.findByBankcharges", query = "SELECT b FROM BankReconcilation b WHERE b.bankcharges = :bankcharges"),
    @NamedQuery(name = "BankReconcilation.findByCoacode", query = "SELECT b FROM BankReconcilation b WHERE b.coacode = :coacode"),
    @NamedQuery(name = "BankReconcilation.findByCoadesc", query = "SELECT b FROM BankReconcilation b WHERE b.coadesc = :coadesc"),
    @NamedQuery(name = "BankReconcilation.findByPaidcode", query = "SELECT b FROM BankReconcilation b WHERE b.paidcode = :paidcode")})
public class BankReconcilation implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BankReconcilationPK bankReconcilationPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "voucherdate")
    private String voucherdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cek")
    private String cek;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amountpv")
    private double amountpv;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "status")
    private String status;
    @Size(max = 100)
    @Column(name = "remarks")
    private String remarks;
    @Size(max = 10)
    @Column(name = "dateclear")
    private String dateclear;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amountclear")
    private double amountclear;
    @Basic(optional = false)
    @NotNull
    @Column(name = "varians")
    private double varians;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bankcode")
    private String bankcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bankname")
    private String bankname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paid")
    private String paid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paymode")
    private String paymode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "createdvoucher")
    private String createdvoucher;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatecode")
    private String estatecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "flag")
    private String flag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bankcharges")
    private String bankcharges;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coacode")
    private String coacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coadesc")
    private String coadesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paidcode")
    private String paidcode;
    
    private String labelStatus;
    private String labelPaymode;
    private String labelTrans;

    public BankReconcilation() {
    }

    public BankReconcilation(BankReconcilationPK bankReconcilationPK) {
        this.bankReconcilationPK = bankReconcilationPK;
    }

    public BankReconcilation(BankReconcilationPK bankReconcilationPK, String voucherdate, String cek, double amountpv, String status, double amountclear, double varians, String bankcode, String bankname, String paid, String paymode, String createdvoucher, String estatecode, String flag, String bankcharges, String coacode, String coadesc, String paidcode, String labelStatus, String labelPaymode, String labelTrans) {
        this.bankReconcilationPK = bankReconcilationPK;
        this.voucherdate = voucherdate;
        this.cek = cek;
        this.amountpv = amountpv;
        this.status = status;
        this.amountclear = amountclear;
        this.varians = varians;
        this.bankcode = bankcode;
        this.bankname = bankname;
        this.paid = paid;
        this.paymode = paymode;
        this.createdvoucher = createdvoucher;
        this.estatecode = estatecode;
        this.flag = flag;
        this.bankcharges = bankcharges;
        this.coacode = coacode;
        this.coadesc = coadesc;
        this.paidcode = paidcode;
        this.labelPaymode = labelPaymode;
        this.labelStatus = labelStatus;
        this.labelTrans = labelTrans;
        
    }

    public String getLabelStatus() {
        return labelStatus;
    }

    public void setLabelStatus(String labelStatus) {
        this.labelStatus = labelStatus;
    }

    public String getLabelPaymode() {
        return labelPaymode;
    }

    public void setLabelPaymode(String labelPaymode) {
        this.labelPaymode = labelPaymode;
    }

    public String getLabelTrans() {
        return labelTrans;
    }

    public void setLabelTrans(String labelTrans) {
        this.labelTrans = labelTrans;
    }

    public BankReconcilation(String voucherno, String year, String period, String depan, String refer) {
        this.bankReconcilationPK = new BankReconcilationPK(voucherno, year, period, depan, refer);
    }

    public BankReconcilationPK getBankReconcilationPK() {
        return bankReconcilationPK;
    }

    public void setBankReconcilationPK(BankReconcilationPK bankReconcilationPK) {
        this.bankReconcilationPK = bankReconcilationPK;
    }

    public String getVoucherdate() {
        return voucherdate;
    }

    public void setVoucherdate(String voucherdate) {
        this.voucherdate = voucherdate;
    }

    public String getCek() {
        return cek;
    }

    public void setCek(String cek) {
        this.cek = cek;
    }

    public double getAmountpv() {
        return amountpv;
    }

    public void setAmountpv(double amountpv) {
        this.amountpv = amountpv;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDateclear() {
        return dateclear;
    }

    public void setDateclear(String dateclear) {
        this.dateclear = dateclear;
    }

    public double getAmountclear() {
        return amountclear;
    }

    public void setAmountclear(double amountclear) {
        this.amountclear = amountclear;
    }

    public double getVarians() {
        return varians;
    }

    public void setVarians(double varians) {
        this.varians = varians;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getPaymode() {
        return paymode;
    }

    public void setPaymode(String paymode) {
        this.paymode = paymode;
    }

    public String getCreatedvoucher() {
        return createdvoucher;
    }

    public void setCreatedvoucher(String createdvoucher) {
        this.createdvoucher = createdvoucher;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getBankcharges() {
        return bankcharges;
    }

    public void setBankcharges(String bankcharges) {
        this.bankcharges = bankcharges;
    }

    public String getCoacode() {
        return coacode;
    }

    public void setCoacode(String coacode) {
        this.coacode = coacode;
    }

    public String getCoadesc() {
        return coadesc;
    }

    public void setCoadesc(String coadesc) {
        this.coadesc = coadesc;
    }

    public String getPaidcode() {
        return paidcode;
    }

    public void setPaidcode(String paidcode) {
        this.paidcode = paidcode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bankReconcilationPK != null ? bankReconcilationPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BankReconcilation)) {
            return false;
        }
        BankReconcilation other = (BankReconcilation) object;
        if ((this.bankReconcilationPK == null && other.bankReconcilationPK != null) || (this.bankReconcilationPK != null && !this.bankReconcilationPK.equals(other.bankReconcilationPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.cashbook.BankReconcilation[ bankReconcilationPK=" + bankReconcilationPK + " ]";
    }
    
}
