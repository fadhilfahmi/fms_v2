/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ap;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ap_debitnote_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VendorDebitNoteItem.findAll", query = "SELECT v FROM VendorDebitNoteItem v"),
    @NamedQuery(name = "VendorDebitNoteItem.findByNoteno", query = "SELECT v FROM VendorDebitNoteItem v WHERE v.noteno = :noteno"),
    @NamedQuery(name = "VendorDebitNoteItem.findByQuantity", query = "SELECT v FROM VendorDebitNoteItem v WHERE v.quantity = :quantity"),
    @NamedQuery(name = "VendorDebitNoteItem.findByUnitprice", query = "SELECT v FROM VendorDebitNoteItem v WHERE v.unitprice = :unitprice"),
    @NamedQuery(name = "VendorDebitNoteItem.findByAmount", query = "SELECT v FROM VendorDebitNoteItem v WHERE v.amount = :amount"),
    @NamedQuery(name = "VendorDebitNoteItem.findByDate", query = "SELECT v FROM VendorDebitNoteItem v WHERE v.date = :date"),
    @NamedQuery(name = "VendorDebitNoteItem.findByRefer", query = "SELECT v FROM VendorDebitNoteItem v WHERE v.refer = :refer"),
    @NamedQuery(name = "VendorDebitNoteItem.findByReference", query = "SELECT v FROM VendorDebitNoteItem v WHERE v.reference = :reference")})
public class VendorDebitNoteItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "noteno")
    private String noteno;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "descp")
    private String descp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantity")
    private double quantity;
    @Basic(optional = false)
    @NotNull
    @Column(name = "unitprice")
    private double unitprice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "date")
    private String date;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "refer")
    private Integer refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "reference")
    private String reference;

    public VendorDebitNoteItem() {
    }

    public VendorDebitNoteItem(Integer refer) {
        this.refer = refer;
    }

    public VendorDebitNoteItem(Integer refer, String noteno, String descp, double quantity, double unitprice, double amount, String date, String reference) {
        this.refer = refer;
        this.noteno = noteno;
        this.descp = descp;
        this.quantity = quantity;
        this.unitprice = unitprice;
        this.amount = amount;
        this.date = date;
        this.reference = reference;
    }

    public String getNoteno() {
        return noteno;
    }

    public void setNoteno(String noteno) {
        this.noteno = noteno;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(double unitprice) {
        this.unitprice = unitprice;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getRefer() {
        return refer;
    }

    public void setRefer(Integer refer) {
        this.refer = refer;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VendorDebitNoteItem)) {
            return false;
        }
        VendorDebitNoteItem other = (VendorDebitNoteItem) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.ap.VendorDebitNoteItem[ refer=" + refer + " ]";
    }
    
}
