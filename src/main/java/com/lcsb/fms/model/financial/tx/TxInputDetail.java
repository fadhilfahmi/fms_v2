/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.tx;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "tx_input_detail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TxInputDetail.findAll", query = "SELECT t FROM TxInputDetail t"),
    @NamedQuery(name = "TxInputDetail.findByRefer", query = "SELECT t FROM TxInputDetail t WHERE t.txInputDetailPK.refer = :refer"),
    @NamedQuery(name = "TxInputDetail.findByDate", query = "SELECT t FROM TxInputDetail t WHERE t.date = :date"),
    @NamedQuery(name = "TxInputDetail.findByYear", query = "SELECT t FROM TxInputDetail t WHERE t.year = :year"),
    @NamedQuery(name = "TxInputDetail.findByPeriod", query = "SELECT t FROM TxInputDetail t WHERE t.period = :period"),
    @NamedQuery(name = "TxInputDetail.findByTaxperiodfrom", query = "SELECT t FROM TxInputDetail t WHERE t.taxperiodfrom = :taxperiodfrom"),
    @NamedQuery(name = "TxInputDetail.findByTaxperiodto", query = "SELECT t FROM TxInputDetail t WHERE t.taxperiodto = :taxperiodto"),
    @NamedQuery(name = "TxInputDetail.findByTaxamt", query = "SELECT t FROM TxInputDetail t WHERE t.taxamt = :taxamt"),
    @NamedQuery(name = "TxInputDetail.findByTaxcode", query = "SELECT t FROM TxInputDetail t WHERE t.txInputDetailPK.taxcode = :taxcode"),
    @NamedQuery(name = "TxInputDetail.findByTaxrate", query = "SELECT t FROM TxInputDetail t WHERE t.taxrate = :taxrate"),
    @NamedQuery(name = "TxInputDetail.findByTaxcoacode", query = "SELECT t FROM TxInputDetail t WHERE t.taxcoacode = :taxcoacode"),
    @NamedQuery(name = "TxInputDetail.findByTaxcoadescp", query = "SELECT t FROM TxInputDetail t WHERE t.taxcoadescp = :taxcoadescp"),
    @NamedQuery(name = "TxInputDetail.findByGstid", query = "SELECT t FROM TxInputDetail t WHERE t.gstid = :gstid"),
    @NamedQuery(name = "TxInputDetail.findByVoucherno", query = "SELECT t FROM TxInputDetail t WHERE t.txInputDetailPK.voucherno = :voucherno"),
    @NamedQuery(name = "TxInputDetail.findByTrxtype", query = "SELECT t FROM TxInputDetail t WHERE t.trxtype = :trxtype"),
    @NamedQuery(name = "TxInputDetail.findByCompcode", query = "SELECT t FROM TxInputDetail t WHERE t.compcode = :compcode"),
    @NamedQuery(name = "TxInputDetail.findByCompname", query = "SELECT t FROM TxInputDetail t WHERE t.compname = :compname"),
    @NamedQuery(name = "TxInputDetail.findByEstcode", query = "SELECT t FROM TxInputDetail t WHERE t.estcode = :estcode"),
    @NamedQuery(name = "TxInputDetail.findByEstname", query = "SELECT t FROM TxInputDetail t WHERE t.estname = :estname"),
    @NamedQuery(name = "TxInputDetail.findByAmount", query = "SELECT t FROM TxInputDetail t WHERE t.amount = :amount"),
    @NamedQuery(name = "TxInputDetail.findByTaxamtDt", query = "SELECT t FROM TxInputDetail t WHERE t.taxamtDt = :taxamtDt"),
    @NamedQuery(name = "TxInputDetail.findByTaxamtCt", query = "SELECT t FROM TxInputDetail t WHERE t.taxamtCt = :taxamtCt"),
    @NamedQuery(name = "TxInputDetail.findByAmountDt", query = "SELECT t FROM TxInputDetail t WHERE t.amountDt = :amountDt"),
    @NamedQuery(name = "TxInputDetail.findByAmountCt", query = "SELECT t FROM TxInputDetail t WHERE t.amountCt = :amountCt"),
    @NamedQuery(name = "TxInputDetail.findByVoucherrefer", query = "SELECT t FROM TxInputDetail t WHERE t.voucherrefer = :voucherrefer"),
    @NamedQuery(name = "TxInputDetail.findByRefno", query = "SELECT t FROM TxInputDetail t WHERE t.txInputDetailPK.refno = :refno"),
    @NamedQuery(name = "TxInputDetail.findByBaddebt", query = "SELECT t FROM TxInputDetail t WHERE t.baddebt = :baddebt")})
public class TxInputDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TxInputDetailPK txInputDetailPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Basic(optional = false)
    @NotNull
    @Column(name = "year")
    private int year;
    @Basic(optional = false)
    @NotNull
    @Column(name = "period")
    private int period;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxperiodfrom")
    @Temporal(TemporalType.DATE)
    private Date taxperiodfrom;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxperiodto")
    @Temporal(TemporalType.DATE)
    private Date taxperiodto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxamt")
    private double taxamt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxrate")
    private double taxrate;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "taxdescp")
    private String taxdescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcoacode")
    private String taxcoacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcoadescp")
    private String taxcoadescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "gstid")
    private String gstid;
    @Size(max = 100)
    @Column(name = "trxtype")
    private String trxtype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "compcode")
    private String compcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "compname")
    private String compname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estcode")
    private String estcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estname")
    private String estname;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxamt_dt")
    private double taxamtDt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxamt_ct")
    private double taxamtCt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount_dt")
    private double amountDt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount_ct")
    private double amountCt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "voucherrefer")
    private String voucherrefer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "baddebt")
    private String baddebt;

    public TxInputDetail() {
    }

    public TxInputDetail(TxInputDetailPK txInputDetailPK) {
        this.txInputDetailPK = txInputDetailPK;
    }

    public TxInputDetail(TxInputDetailPK txInputDetailPK, Date date, int year, int period, Date taxperiodfrom, Date taxperiodto, double taxamt, double taxrate, String taxdescp, String taxcoacode, String taxcoadescp, String gstid, String compcode, String compname, String estcode, String estname, String remarks, double amount, double taxamtDt, double taxamtCt, double amountDt, double amountCt, String voucherrefer, String baddebt) {
        this.txInputDetailPK = txInputDetailPK;
        this.date = date;
        this.year = year;
        this.period = period;
        this.taxperiodfrom = taxperiodfrom;
        this.taxperiodto = taxperiodto;
        this.taxamt = taxamt;
        this.taxrate = taxrate;
        this.taxdescp = taxdescp;
        this.taxcoacode = taxcoacode;
        this.taxcoadescp = taxcoadescp;
        this.gstid = gstid;
        this.compcode = compcode;
        this.compname = compname;
        this.estcode = estcode;
        this.estname = estname;
        this.remarks = remarks;
        this.amount = amount;
        this.taxamtDt = taxamtDt;
        this.taxamtCt = taxamtCt;
        this.amountDt = amountDt;
        this.amountCt = amountCt;
        this.voucherrefer = voucherrefer;
        this.baddebt = baddebt;
    }

    public TxInputDetail(String refer, String taxcode, String voucherno, String refno) {
        this.txInputDetailPK = new TxInputDetailPK(refer, taxcode, voucherno, refno);
    }

    public TxInputDetailPK getTxInputDetailPK() {
        return txInputDetailPK;
    }

    public void setTxInputDetailPK(TxInputDetailPK txInputDetailPK) {
        this.txInputDetailPK = txInputDetailPK;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public Date getTaxperiodfrom() {
        return taxperiodfrom;
    }

    public void setTaxperiodfrom(Date taxperiodfrom) {
        this.taxperiodfrom = taxperiodfrom;
    }

    public Date getTaxperiodto() {
        return taxperiodto;
    }

    public void setTaxperiodto(Date taxperiodto) {
        this.taxperiodto = taxperiodto;
    }

    public double getTaxamt() {
        return taxamt;
    }

    public void setTaxamt(double taxamt) {
        this.taxamt = taxamt;
    }

    public double getTaxrate() {
        return taxrate;
    }

    public void setTaxrate(double taxrate) {
        this.taxrate = taxrate;
    }

    public String getTaxdescp() {
        return taxdescp;
    }

    public void setTaxdescp(String taxdescp) {
        this.taxdescp = taxdescp;
    }

    public String getTaxcoacode() {
        return taxcoacode;
    }

    public void setTaxcoacode(String taxcoacode) {
        this.taxcoacode = taxcoacode;
    }

    public String getTaxcoadescp() {
        return taxcoadescp;
    }

    public void setTaxcoadescp(String taxcoadescp) {
        this.taxcoadescp = taxcoadescp;
    }

    public String getGstid() {
        return gstid;
    }

    public void setGstid(String gstid) {
        this.gstid = gstid;
    }

    public String getTrxtype() {
        return trxtype;
    }

    public void setTrxtype(String trxtype) {
        this.trxtype = trxtype;
    }

    public String getCompcode() {
        return compcode;
    }

    public void setCompcode(String compcode) {
        this.compcode = compcode;
    }

    public String getCompname() {
        return compname;
    }

    public void setCompname(String compname) {
        this.compname = compname;
    }

    public String getEstcode() {
        return estcode;
    }

    public void setEstcode(String estcode) {
        this.estcode = estcode;
    }

    public String getEstname() {
        return estname;
    }

    public void setEstname(String estname) {
        this.estname = estname;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getTaxamtDt() {
        return taxamtDt;
    }

    public void setTaxamtDt(double taxamtDt) {
        this.taxamtDt = taxamtDt;
    }

    public double getTaxamtCt() {
        return taxamtCt;
    }

    public void setTaxamtCt(double taxamtCt) {
        this.taxamtCt = taxamtCt;
    }

    public double getAmountDt() {
        return amountDt;
    }

    public void setAmountDt(double amountDt) {
        this.amountDt = amountDt;
    }

    public double getAmountCt() {
        return amountCt;
    }

    public void setAmountCt(double amountCt) {
        this.amountCt = amountCt;
    }

    public String getVoucherrefer() {
        return voucherrefer;
    }

    public void setVoucherrefer(String voucherrefer) {
        this.voucherrefer = voucherrefer;
    }

    public String getBaddebt() {
        return baddebt;
    }

    public void setBaddebt(String baddebt) {
        this.baddebt = baddebt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (txInputDetailPK != null ? txInputDetailPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TxInputDetail)) {
            return false;
        }
        TxInputDetail other = (TxInputDetail) object;
        if ((this.txInputDetailPK == null && other.txInputDetailPK != null) || (this.txInputDetailPK != null && !this.txInputDetailPK.equals(other.txInputDetailPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.tx.TxInputDetail[ txInputDetailPK=" + txInputDetailPK + " ]";
    }
    
}
