/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ar;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "sl_inv")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SalesInvoice.findAll", query = "SELECT s FROM SalesInvoice s"),
    @NamedQuery(name = "SalesInvoice.findByInvref", query = "SELECT s FROM SalesInvoice s WHERE s.invref = :invref"),
    @NamedQuery(name = "SalesInvoice.findByInvno", query = "SELECT s FROM SalesInvoice s WHERE s.invno = :invno"),
    @NamedQuery(name = "SalesInvoice.findByInvdate", query = "SELECT s FROM SalesInvoice s WHERE s.invdate = :invdate"),
    @NamedQuery(name = "SalesInvoice.findByEstcode", query = "SELECT s FROM SalesInvoice s WHERE s.estcode = :estcode"),
    @NamedQuery(name = "SalesInvoice.findByEstname", query = "SELECT s FROM SalesInvoice s WHERE s.estname = :estname"),
    @NamedQuery(name = "SalesInvoice.findByPostflag", query = "SELECT s FROM SalesInvoice s WHERE s.postflag = :postflag"),
    @NamedQuery(name = "SalesInvoice.findByPostdate", query = "SELECT s FROM SalesInvoice s WHERE s.postdate = :postdate"),
    @NamedQuery(name = "SalesInvoice.findByYear", query = "SELECT s FROM SalesInvoice s WHERE s.year = :year"),
    @NamedQuery(name = "SalesInvoice.findByPeriod", query = "SELECT s FROM SalesInvoice s WHERE s.period = :period"),
    @NamedQuery(name = "SalesInvoice.findByAmountno", query = "SELECT s FROM SalesInvoice s WHERE s.amountno = :amountno"),
    @NamedQuery(name = "SalesInvoice.findByPaid", query = "SELECT s FROM SalesInvoice s WHERE s.paid = :paid"),
    @NamedQuery(name = "SalesInvoice.findByAmountstr", query = "SELECT s FROM SalesInvoice s WHERE s.amountstr = :amountstr"),
    @NamedQuery(name = "SalesInvoice.findByBtype", query = "SELECT s FROM SalesInvoice s WHERE s.btype = :btype"),
    @NamedQuery(name = "SalesInvoice.findByBcode", query = "SELECT s FROM SalesInvoice s WHERE s.bcode = :bcode"),
    @NamedQuery(name = "SalesInvoice.findByBname", query = "SELECT s FROM SalesInvoice s WHERE s.bname = :bname"),
    @NamedQuery(name = "SalesInvoice.findByBaddress", query = "SELECT s FROM SalesInvoice s WHERE s.baddress = :baddress"),
    @NamedQuery(name = "SalesInvoice.findByBpostcode", query = "SELECT s FROM SalesInvoice s WHERE s.bpostcode = :bpostcode"),
    @NamedQuery(name = "SalesInvoice.findByBstate", query = "SELECT s FROM SalesInvoice s WHERE s.bstate = :bstate"),
    @NamedQuery(name = "SalesInvoice.findByLoclevel", query = "SELECT s FROM SalesInvoice s WHERE s.loclevel = :loclevel"),
    @NamedQuery(name = "SalesInvoice.findByLoccode", query = "SELECT s FROM SalesInvoice s WHERE s.loccode = :loccode"),
    @NamedQuery(name = "SalesInvoice.findByLocdesc", query = "SELECT s FROM SalesInvoice s WHERE s.locdesc = :locdesc"),
    @NamedQuery(name = "SalesInvoice.findByCttype", query = "SELECT s FROM SalesInvoice s WHERE s.cttype = :cttype"),
    @NamedQuery(name = "SalesInvoice.findByCtcode", query = "SELECT s FROM SalesInvoice s WHERE s.ctcode = :ctcode"),
    @NamedQuery(name = "SalesInvoice.findByCtdesc", query = "SELECT s FROM SalesInvoice s WHERE s.ctdesc = :ctdesc"),
    @NamedQuery(name = "SalesInvoice.findByPid", query = "SELECT s FROM SalesInvoice s WHERE s.pid = :pid"),
    @NamedQuery(name = "SalesInvoice.findByPname", query = "SELECT s FROM SalesInvoice s WHERE s.pname = :pname"),
    @NamedQuery(name = "SalesInvoice.findByPdate", query = "SELECT s FROM SalesInvoice s WHERE s.pdate = :pdate"),
    @NamedQuery(name = "SalesInvoice.findByCid", query = "SELECT s FROM SalesInvoice s WHERE s.cid = :cid"),
    @NamedQuery(name = "SalesInvoice.findByCname", query = "SELECT s FROM SalesInvoice s WHERE s.cname = :cname"),
    @NamedQuery(name = "SalesInvoice.findByCdate", query = "SELECT s FROM SalesInvoice s WHERE s.cdate = :cdate"),
    @NamedQuery(name = "SalesInvoice.findByAid", query = "SELECT s FROM SalesInvoice s WHERE s.aid = :aid"),
    @NamedQuery(name = "SalesInvoice.findByAname", query = "SELECT s FROM SalesInvoice s WHERE s.aname = :aname"),
    @NamedQuery(name = "SalesInvoice.findByAdesignation", query = "SELECT s FROM SalesInvoice s WHERE s.adesignation = :adesignation"),
    @NamedQuery(name = "SalesInvoice.findByAdate", query = "SELECT s FROM SalesInvoice s WHERE s.adate = :adate"),
    @NamedQuery(name = "SalesInvoice.findByCoacode", query = "SELECT s FROM SalesInvoice s WHERE s.coacode = :coacode"),
    @NamedQuery(name = "SalesInvoice.findByCoadesc", query = "SELECT s FROM SalesInvoice s WHERE s.coadesc = :coadesc"),
    @NamedQuery(name = "SalesInvoice.findByGstid", query = "SELECT s FROM SalesInvoice s WHERE s.gstid = :gstid"),
    @NamedQuery(name = "SalesInvoice.findByPostgst", query = "SELECT s FROM SalesInvoice s WHERE s.postgst = :postgst"),
    @NamedQuery(name = "SalesInvoice.findByDono", query = "SELECT s FROM SalesInvoice s WHERE s.dono = :dono"),
    @NamedQuery(name = "SalesInvoice.findByGstdate", query = "SELECT s FROM SalesInvoice s WHERE s.gstdate = :gstdate"),
    @NamedQuery(name = "SalesInvoice.findBySatype", query = "SELECT s FROM SalesInvoice s WHERE s.satype = :satype"),
    @NamedQuery(name = "SalesInvoice.findBySacode", query = "SELECT s FROM SalesInvoice s WHERE s.sacode = :sacode"),
    @NamedQuery(name = "SalesInvoice.findBySadesc", query = "SELECT s FROM SalesInvoice s WHERE s.sadesc = :sadesc"),
    @NamedQuery(name = "SalesInvoice.findByReceiveamt", query = "SELECT s FROM SalesInvoice s WHERE s.receiveamt = :receiveamt"),
    @NamedQuery(name = "SalesInvoice.findByRacoacode", query = "SELECT s FROM SalesInvoice s WHERE s.racoacode = :racoacode"),
    @NamedQuery(name = "SalesInvoice.findByRacoadesc", query = "SELECT s FROM SalesInvoice s WHERE s.racoadesc = :racoadesc"),
    @NamedQuery(name = "SalesInvoice.findByBaddebt", query = "SELECT s FROM SalesInvoice s WHERE s.baddebt = :baddebt"),
    @NamedQuery(name = "SalesInvoice.findByContractno", query = "SELECT s FROM SalesInvoice s WHERE s.contractno = :contractno"),
    @NamedQuery(name = "SalesInvoice.findByProdcode", query = "SELECT s FROM SalesInvoice s WHERE s.prodcode = :prodcode"),
    @NamedQuery(name = "SalesInvoice.findByProdname", query = "SELECT s FROM SalesInvoice s WHERE s.prodname = :prodname"),
    @NamedQuery(name = "SalesInvoice.findByOldinvno", query = "SELECT s FROM SalesInvoice s WHERE s.oldinvno = :oldinvno"),
    @NamedQuery(name = "SalesInvoice.findByBcity", query = "SELECT s FROM SalesInvoice s WHERE s.bcity = :bcity"),
    @NamedQuery(name = "SalesInvoice.findByOrderno", query = "SELECT s FROM SalesInvoice s WHERE s.orderno = :orderno")})
public class SalesInvoice implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "invref")
    private String invref;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "invno")
    private String invno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "invdate")
    private String invdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estcode")
    private String estcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estname")
    private String estname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "postflag")
    private String postflag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "postdate")
    private String postdate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "year")
    private int year;
    @Basic(optional = false)
    @NotNull
    @Column(name = "period")
    private int period;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amountno")
    private double amountno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "paid")
    private double paid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "amountstr")
    private String amountstr;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "btype")
    private String btype;
    @Size(max = 100)
    @Column(name = "bcode")
    private String bcode;
    @Size(max = 100)
    @Column(name = "bname")
    private String bname;
    @Size(max = 100)
    @Column(name = "baddress")
    private String baddress;
    @Size(max = 100)
    @Column(name = "bpostcode")
    private String bpostcode;
    @Size(max = 100)
    @Column(name = "bstate")
    private String bstate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loclevel")
    private String loclevel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loccode")
    private String loccode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "locdesc")
    private String locdesc;
    @Size(max = 100)
    @Column(name = "cttype")
    private String cttype;
    @Size(max = 100)
    @Column(name = "ctcode")
    private String ctcode;
    @Size(max = 100)
    @Column(name = "ctdesc")
    private String ctdesc;
    @Size(max = 100)
    @Column(name = "pid")
    private String pid;
    @Size(max = 100)
    @Column(name = "pname")
    private String pname;
    @Size(max = 100)
    @Column(name = "pdate")
    private String pdate;
    @Size(max = 100)
    @Column(name = "cid")
    private String cid;
    @Size(max = 100)
    @Column(name = "cname")
    private String cname;
    @Size(max = 100)
    @Column(name = "cdate")
    private String cdate;
    @Size(max = 100)
    @Column(name = "aid")
    private String aid;
    @Size(max = 100)
    @Column(name = "aname")
    private String aname;
    @Size(max = 100)
    @Column(name = "adesignation")
    private String adesignation;
    @Size(max = 100)
    @Column(name = "adate")
    private String adate;
    @Size(max = 100)
    @Column(name = "coacode")
    private String coacode;
    @Size(max = 100)
    @Column(name = "coadesc")
    private String coadesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "gstid")
    private String gstid;
    @Size(max = 100)
    @Column(name = "postgst")
    private String postgst;
    @Size(max = 100)
    @Column(name = "dono")
    private String dono;
    @Size(max = 100)
    @Column(name = "gstdate")
    private String gstdate;
    @Size(max = 100)
    @Column(name = "satype")
    private String satype;
    @Size(max = 100)
    @Column(name = "sacode")
    private String sacode;
    @Size(max = 100)
    @Column(name = "sadesc")
    private String sadesc;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "receiveamt")
    private Double receiveamt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "racoacode")
    private String racoacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "racoadesc")
    private String racoadesc;
    @Size(max = 100)
    @Column(name = "baddebt")
    private String baddebt;
    @Size(max = 100)
    @Column(name = "contractno")
    private String contractno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "prodcode")
    private String prodcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "prodname")
    private String prodname;
    @Size(max = 30)
    @Column(name = "oldinvno")
    private String oldinvno;
    @Size(max = 100)
    @Column(name = "bcity")
    private String bcity;
    @Size(max = 50)
    @Column(name = "orderno")
    private String orderno;

    public SalesInvoice() {
    }

    public SalesInvoice(String invref) {
        this.invref = invref;
    }

    public SalesInvoice(String invref, String invno, String invdate, String estcode, String estname, String postflag, String postdate, int year, int period, double amountno, double paid, String amountstr, String remarks, String btype, String loclevel, String loccode, String locdesc, String gstid, String racoacode, String racoadesc, String prodcode, String prodname) {
        this.invref = invref;
        this.invno = invno;
        this.invdate = invdate;
        this.estcode = estcode;
        this.estname = estname;
        this.postflag = postflag;
        this.postdate = postdate;
        this.year = year;
        this.period = period;
        this.amountno = amountno;
        this.paid = paid;
        this.amountstr = amountstr;
        this.remarks = remarks;
        this.btype = btype;
        this.loclevel = loclevel;
        this.loccode = loccode;
        this.locdesc = locdesc;
        this.gstid = gstid;
        this.racoacode = racoacode;
        this.racoadesc = racoadesc;
        this.prodcode = prodcode;
        this.prodname = prodname;
    }

    public String getInvref() {
        return invref;
    }

    public void setInvref(String invref) {
        this.invref = invref;
    }

    public String getInvno() {
        return invno;
    }

    public void setInvno(String invno) {
        this.invno = invno;
    }

    public String getInvdate() {
        return invdate;
    }

    public void setInvdate(String invdate) {
        this.invdate = invdate;
    }

    public String getEstcode() {
        return estcode;
    }

    public void setEstcode(String estcode) {
        this.estcode = estcode;
    }

    public String getEstname() {
        return estname;
    }

    public void setEstname(String estname) {
        this.estname = estname;
    }

    public String getPostflag() {
        return postflag;
    }

    public void setPostflag(String postflag) {
        this.postflag = postflag;
    }

    public String getPostdate() {
        return postdate;
    }

    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public double getAmountno() {
        return amountno;
    }

    public void setAmountno(double amountno) {
        this.amountno = amountno;
    }

    public double getPaid() {
        return paid;
    }

    public void setPaid(double paid) {
        this.paid = paid;
    }

    public String getAmountstr() {
        return amountstr;
    }

    public void setAmountstr(String amountstr) {
        this.amountstr = amountstr;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getBtype() {
        return btype;
    }

    public void setBtype(String btype) {
        this.btype = btype;
    }

    public String getBcode() {
        return bcode;
    }

    public void setBcode(String bcode) {
        this.bcode = bcode;
    }

    public String getBname() {
        return bname;
    }

    public void setBname(String bname) {
        this.bname = bname;
    }

    public String getBaddress() {
        return baddress;
    }

    public void setBaddress(String baddress) {
        this.baddress = baddress;
    }

    public String getBpostcode() {
        return bpostcode;
    }

    public void setBpostcode(String bpostcode) {
        this.bpostcode = bpostcode;
    }

    public String getBstate() {
        return bstate;
    }

    public void setBstate(String bstate) {
        this.bstate = bstate;
    }

    public String getLoclevel() {
        return loclevel;
    }

    public void setLoclevel(String loclevel) {
        this.loclevel = loclevel;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLocdesc() {
        return locdesc;
    }

    public void setLocdesc(String locdesc) {
        this.locdesc = locdesc;
    }

    public String getCttype() {
        return cttype;
    }

    public void setCttype(String cttype) {
        this.cttype = cttype;
    }

    public String getCtcode() {
        return ctcode;
    }

    public void setCtcode(String ctcode) {
        this.ctcode = ctcode;
    }

    public String getCtdesc() {
        return ctdesc;
    }

    public void setCtdesc(String ctdesc) {
        this.ctdesc = ctdesc;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPdate() {
        return pdate;
    }

    public void setPdate(String pdate) {
        this.pdate = pdate;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCdate() {
        return cdate;
    }

    public void setCdate(String cdate) {
        this.cdate = cdate;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getAname() {
        return aname;
    }

    public void setAname(String aname) {
        this.aname = aname;
    }

    public String getAdesignation() {
        return adesignation;
    }

    public void setAdesignation(String adesignation) {
        this.adesignation = adesignation;
    }

    public String getAdate() {
        return adate;
    }

    public void setAdate(String adate) {
        this.adate = adate;
    }

    public String getCoacode() {
        return coacode;
    }

    public void setCoacode(String coacode) {
        this.coacode = coacode;
    }

    public String getCoadesc() {
        return coadesc;
    }

    public void setCoadesc(String coadesc) {
        this.coadesc = coadesc;
    }

    public String getGstid() {
        return gstid;
    }

    public void setGstid(String gstid) {
        this.gstid = gstid;
    }

    public String getPostgst() {
        return postgst;
    }

    public void setPostgst(String postgst) {
        this.postgst = postgst;
    }

    public String getDono() {
        return dono;
    }

    public void setDono(String dono) {
        this.dono = dono;
    }

    public String getGstdate() {
        return gstdate;
    }

    public void setGstdate(String gstdate) {
        this.gstdate = gstdate;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    public Double getReceiveamt() {
        return receiveamt;
    }

    public void setReceiveamt(Double receiveamt) {
        this.receiveamt = receiveamt;
    }

    public String getRacoacode() {
        return racoacode;
    }

    public void setRacoacode(String racoacode) {
        this.racoacode = racoacode;
    }

    public String getRacoadesc() {
        return racoadesc;
    }

    public void setRacoadesc(String racoadesc) {
        this.racoadesc = racoadesc;
    }

    public String getBaddebt() {
        return baddebt;
    }

    public void setBaddebt(String baddebt) {
        this.baddebt = baddebt;
    }

    public String getContractno() {
        return contractno;
    }

    public void setContractno(String contractno) {
        this.contractno = contractno;
    }

    public String getProdcode() {
        return prodcode;
    }

    public void setProdcode(String prodcode) {
        this.prodcode = prodcode;
    }

    public String getProdname() {
        return prodname;
    }

    public void setProdname(String prodname) {
        this.prodname = prodname;
    }

    public String getOldinvno() {
        return oldinvno;
    }

    public void setOldinvno(String oldinvno) {
        this.oldinvno = oldinvno;
    }

    public String getBcity() {
        return bcity;
    }

    public void setBcity(String bcity) {
        this.bcity = bcity;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (invref != null ? invref.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SalesInvoice)) {
            return false;
        }
        SalesInvoice other = (SalesInvoice) object;
        if ((this.invref == null && other.invref != null) || (this.invref != null && !this.invref.equals(other.invref))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.ar.SalesInvoice[ invref=" + invref + " ]";
    }
    
}
