/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "cb_cashvoucher")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CashVoucher.findAll", query = "SELECT c FROM CashVoucher c"),
    @NamedQuery(name = "CashVoucher.findByVoucherid", query = "SELECT c FROM CashVoucher c WHERE c.voucherid = :voucherid"),
    @NamedQuery(name = "CashVoucher.findByVoucherdate", query = "SELECT c FROM CashVoucher c WHERE c.voucherdate = :voucherdate"),
    @NamedQuery(name = "CashVoucher.findByEstatecode", query = "SELECT c FROM CashVoucher c WHERE c.estatecode = :estatecode"),
    @NamedQuery(name = "CashVoucher.findByPayto", query = "SELECT c FROM CashVoucher c WHERE c.payto = :payto"),
    @NamedQuery(name = "CashVoucher.findByAmount", query = "SELECT c FROM CashVoucher c WHERE c.amount = :amount"),
    @NamedQuery(name = "CashVoucher.findByTotal", query = "SELECT c FROM CashVoucher c WHERE c.total = :total"),
    @NamedQuery(name = "CashVoucher.findByReceiveid", query = "SELECT c FROM CashVoucher c WHERE c.receiveid = :receiveid"),
    @NamedQuery(name = "CashVoucher.findByReceivebyname", query = "SELECT c FROM CashVoucher c WHERE c.receivebyname = :receivebyname"),
    @NamedQuery(name = "CashVoucher.findByAppid", query = "SELECT c FROM CashVoucher c WHERE c.appid = :appid"),
    @NamedQuery(name = "CashVoucher.findByApprobyname", query = "SELECT c FROM CashVoucher c WHERE c.approbyname = :approbyname"),
    @NamedQuery(name = "CashVoucher.findByAppdesign", query = "SELECT c FROM CashVoucher c WHERE c.appdesign = :appdesign"),
    @NamedQuery(name = "CashVoucher.findByReceiptno", query = "SELECT c FROM CashVoucher c WHERE c.receiptno = :receiptno"),
    @NamedQuery(name = "CashVoucher.findByPaymentflag", query = "SELECT c FROM CashVoucher c WHERE c.paymentflag = :paymentflag"),
    @NamedQuery(name = "CashVoucher.findByAmounttotaldebit", query = "SELECT c FROM CashVoucher c WHERE c.amounttotaldebit = :amounttotaldebit"),
    @NamedQuery(name = "CashVoucher.findByPost", query = "SELECT c FROM CashVoucher c WHERE c.post = :post"),
    @NamedQuery(name = "CashVoucher.findByRefer", query = "SELECT c FROM CashVoucher c WHERE c.refer = :refer"),
    @NamedQuery(name = "CashVoucher.findByYear", query = "SELECT c FROM CashVoucher c WHERE c.year = :year"),
    @NamedQuery(name = "CashVoucher.findByPeriod", query = "SELECT c FROM CashVoucher c WHERE c.period = :period"),
    @NamedQuery(name = "CashVoucher.findByReceivetype", query = "SELECT c FROM CashVoucher c WHERE c.receivetype = :receivetype"),
    @NamedQuery(name = "CashVoucher.findByPreparedate", query = "SELECT c FROM CashVoucher c WHERE c.preparedate = :preparedate"),
    @NamedQuery(name = "CashVoucher.findByCheckdate", query = "SELECT c FROM CashVoucher c WHERE c.checkdate = :checkdate"),
    @NamedQuery(name = "CashVoucher.findByApprovedate", query = "SELECT c FROM CashVoucher c WHERE c.approvedate = :approvedate"),
    @NamedQuery(name = "CashVoucher.findByPrepareid", query = "SELECT c FROM CashVoucher c WHERE c.prepareid = :prepareid"),
    @NamedQuery(name = "CashVoucher.findByPreparename", query = "SELECT c FROM CashVoucher c WHERE c.preparename = :preparename"),
    @NamedQuery(name = "CashVoucher.findByCheckid", query = "SELECT c FROM CashVoucher c WHERE c.checkid = :checkid"),
    @NamedQuery(name = "CashVoucher.findByCheckname", query = "SELECT c FROM CashVoucher c WHERE c.checkname = :checkname"),
    @NamedQuery(name = "CashVoucher.findByPostdate", query = "SELECT c FROM CashVoucher c WHERE c.postdate = :postdate"),
    @NamedQuery(name = "CashVoucher.findByPettycode", query = "SELECT c FROM CashVoucher c WHERE c.pettycode = :pettycode"),
    @NamedQuery(name = "CashVoucher.findByPostgst", query = "SELECT c FROM CashVoucher c WHERE c.postgst = :postgst"),
    @NamedQuery(name = "CashVoucher.findByGstdate", query = "SELECT c FROM CashVoucher c WHERE c.gstdate = :gstdate"),
    @NamedQuery(name = "CashVoucher.findByPostflag", query = "SELECT c FROM CashVoucher c WHERE c.postflag = :postflag"),
    @NamedQuery(name = "CashVoucher.findByRacoacode", query = "SELECT c FROM CashVoucher c WHERE c.racoacode = :racoacode"),
    @NamedQuery(name = "CashVoucher.findByRacoadesc", query = "SELECT c FROM CashVoucher c WHERE c.racoadesc = :racoadesc")})
public class CashVoucher implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "voucherid")
    private String voucherid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "voucherdate")
    private String voucherdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "estatecode")
    private String estatecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "payto")
    private String payto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "amount")
    private String amount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "total")
    private double total;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "reason")
    private String reason;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "receiveid")
    private String receiveid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "receivebyname")
    private String receivebyname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "appid")
    private String appid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "approbyname")
    private String approbyname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appdesign")
    private String appdesign;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "receiptno")
    private String receiptno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paymentflag")
    private String paymentflag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "amounttotaldebit")
    private String amounttotaldebit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "post")
    private String post;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "period")
    private String period;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "receivetype")
    private String receivetype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "preparedate")
    private String preparedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "checkdate")
    private String checkdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "approvedate")
    private String approvedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prepareid")
    private String prepareid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "preparename")
    private String preparename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkid")
    private String checkid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkname")
    private String checkname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "postdate")
    private String postdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pettycode")
    private String pettycode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postgst")
    private String postgst;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "gstdate")
    private String gstdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postflag")
    private String postflag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "racoacode")
    private String racoacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "racoadesc")
    private String racoadesc;

    public CashVoucher() {
    }

    public CashVoucher(String refer) {
        this.refer = refer;
    }

    public CashVoucher(String refer, String voucherid, String voucherdate, String estatecode, String payto, String amount, double total, String reason, String receiveid, String receivebyname, String appid, String approbyname, String appdesign, String receiptno, String paymentflag, String amounttotaldebit, String post, String year, String period, String receivetype, String preparedate, String checkdate, String approvedate, String prepareid, String preparename, String checkid, String checkname, String postdate, String pettycode, String postgst, String gstdate, String postflag, String racoacode, String racoadesc) {
        this.refer = refer;
        this.voucherid = voucherid;
        this.voucherdate = voucherdate;
        this.estatecode = estatecode;
        this.payto = payto;
        this.amount = amount;
        this.total = total;
        this.reason = reason;
        this.receiveid = receiveid;
        this.receivebyname = receivebyname;
        this.appid = appid;
        this.approbyname = approbyname;
        this.appdesign = appdesign;
        this.receiptno = receiptno;
        this.paymentflag = paymentflag;
        this.amounttotaldebit = amounttotaldebit;
        this.post = post;
        this.year = year;
        this.period = period;
        this.receivetype = receivetype;
        this.preparedate = preparedate;
        this.checkdate = checkdate;
        this.approvedate = approvedate;
        this.prepareid = prepareid;
        this.preparename = preparename;
        this.checkid = checkid;
        this.checkname = checkname;
        this.postdate = postdate;
        this.pettycode = pettycode;
        this.postgst = postgst;
        this.gstdate = gstdate;
        this.postflag = postflag;
        this.racoacode = racoacode;
        this.racoadesc = racoadesc;
    }

    public String getVoucherid() {
        return voucherid;
    }

    public void setVoucherid(String voucherid) {
        this.voucherid = voucherid;
    }

    public String getVoucherdate() {
        return voucherdate;
    }

    public void setVoucherdate(String voucherdate) {
        this.voucherdate = voucherdate;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getPayto() {
        return payto;
    }

    public void setPayto(String payto) {
        this.payto = payto;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReceiveid() {
        return receiveid;
    }

    public void setReceiveid(String receiveid) {
        this.receiveid = receiveid;
    }

    public String getReceivebyname() {
        return receivebyname;
    }

    public void setReceivebyname(String receivebyname) {
        this.receivebyname = receivebyname;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getApprobyname() {
        return approbyname;
    }

    public void setApprobyname(String approbyname) {
        this.approbyname = approbyname;
    }

    public String getAppdesign() {
        return appdesign;
    }

    public void setAppdesign(String appdesign) {
        this.appdesign = appdesign;
    }

    public String getReceiptno() {
        return receiptno;
    }

    public void setReceiptno(String receiptno) {
        this.receiptno = receiptno;
    }

    public String getPaymentflag() {
        return paymentflag;
    }

    public void setPaymentflag(String paymentflag) {
        this.paymentflag = paymentflag;
    }

    public String getAmounttotaldebit() {
        return amounttotaldebit;
    }

    public void setAmounttotaldebit(String amounttotaldebit) {
        this.amounttotaldebit = amounttotaldebit;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getReceivetype() {
        return receivetype;
    }

    public void setReceivetype(String receivetype) {
        this.receivetype = receivetype;
    }

    public String getPreparedate() {
        return preparedate;
    }

    public void setPreparedate(String preparedate) {
        this.preparedate = preparedate;
    }

    public String getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(String checkdate) {
        this.checkdate = checkdate;
    }

    public String getApprovedate() {
        return approvedate;
    }

    public void setApprovedate(String approvedate) {
        this.approvedate = approvedate;
    }

    public String getPrepareid() {
        return prepareid;
    }

    public void setPrepareid(String prepareid) {
        this.prepareid = prepareid;
    }

    public String getPreparename() {
        return preparename;
    }

    public void setPreparename(String preparename) {
        this.preparename = preparename;
    }

    public String getCheckid() {
        return checkid;
    }

    public void setCheckid(String checkid) {
        this.checkid = checkid;
    }

    public String getCheckname() {
        return checkname;
    }

    public void setCheckname(String checkname) {
        this.checkname = checkname;
    }

    public String getPostdate() {
        return postdate;
    }

    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }

    public String getPettycode() {
        return pettycode;
    }

    public void setPettycode(String pettycode) {
        this.pettycode = pettycode;
    }

    public String getPostgst() {
        return postgst;
    }

    public void setPostgst(String postgst) {
        this.postgst = postgst;
    }

    public String getGstdate() {
        return gstdate;
    }

    public void setGstdate(String gstdate) {
        this.gstdate = gstdate;
    }

    public String getPostflag() {
        return postflag;
    }

    public void setPostflag(String postflag) {
        this.postflag = postflag;
    }

    public String getRacoacode() {
        return racoacode;
    }

    public void setRacoacode(String racoacode) {
        this.racoacode = racoacode;
    }

    public String getRacoadesc() {
        return racoadesc;
    }

    public void setRacoadesc(String racoadesc) {
        this.racoadesc = racoadesc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CashVoucher)) {
            return false;
        }
        CashVoucher other = (CashVoucher) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.cashbook.CashVoucher[ refer=" + refer + " ]";
    }
    
}
