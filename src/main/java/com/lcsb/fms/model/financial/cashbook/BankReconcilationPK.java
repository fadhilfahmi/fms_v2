/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author fadhilfahmi
 */
@Embeddable
public class BankReconcilationPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "voucherno")
    private String voucherno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "period")
    private String period;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "depan")
    private String depan;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;

    public BankReconcilationPK() {
    }

    public BankReconcilationPK(String voucherno, String year, String period, String depan, String refer) {
        this.voucherno = voucherno;
        this.year = year;
        this.period = period;
        this.depan = depan;
        this.refer = refer;
    }

    public String getVoucherno() {
        return voucherno;
    }

    public void setVoucherno(String voucherno) {
        this.voucherno = voucherno;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getDepan() {
        return depan;
    }

    public void setDepan(String depan) {
        this.depan = depan;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (voucherno != null ? voucherno.hashCode() : 0);
        hash += (year != null ? year.hashCode() : 0);
        hash += (period != null ? period.hashCode() : 0);
        hash += (depan != null ? depan.hashCode() : 0);
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BankReconcilationPK)) {
            return false;
        }
        BankReconcilationPK other = (BankReconcilationPK) object;
        if ((this.voucherno == null && other.voucherno != null) || (this.voucherno != null && !this.voucherno.equals(other.voucherno))) {
            return false;
        }
        if ((this.year == null && other.year != null) || (this.year != null && !this.year.equals(other.year))) {
            return false;
        }
        if ((this.period == null && other.period != null) || (this.period != null && !this.period.equals(other.period))) {
            return false;
        }
        if ((this.depan == null && other.depan != null) || (this.depan != null && !this.depan.equals(other.depan))) {
            return false;
        }
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.cashbook.BankReconcilationPK[ voucherno=" + voucherno + ", year=" + year + ", period=" + period + ", depan=" + depan + ", refer=" + refer + " ]";
    }
    
}
