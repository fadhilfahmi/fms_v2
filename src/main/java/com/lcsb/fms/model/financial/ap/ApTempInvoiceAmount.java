/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ap;

/**
 *
 * @author fadhilfahmi
 */
public class ApTempInvoiceAmount {
    
    private String invrefno;
    private double amounttopay;
    private String portion;
    private String sessionid;
    private String suppcode;

    public String getSuppcode() {
        return suppcode;
    }

    public void setSuppcode(String suppcode) {
        this.suppcode = suppcode;
    }
    
    

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    

    public String getInvrefno() {
        return invrefno;
    }

    public void setInvrefno(String invrefno) {
        this.invrefno = invrefno;
    }

    public double getAmounttopay() {
        return amounttopay;
    }

    public void setAmounttopay(double amounttopay) {
        this.amounttopay = amounttopay;
    }

    public String getPortion() {
        return portion;
    }

    public void setPortion(String portion) {
        this.portion = portion;
    }
    
    
    
}
