/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ap;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ap_inv_refer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VendorInvoiceRefer.findAll", query = "SELECT v FROM VendorInvoiceRefer v"),
    @NamedQuery(name = "VendorInvoiceRefer.findByRefer", query = "SELECT v FROM VendorInvoiceRefer v WHERE v.refer = :refer"),
    @NamedQuery(name = "VendorInvoiceRefer.findByType", query = "SELECT v FROM VendorInvoiceRefer v WHERE v.type = :type"),
    @NamedQuery(name = "VendorInvoiceRefer.findByNo", query = "SELECT v FROM VendorInvoiceRefer v WHERE v.no = :no"),
    @NamedQuery(name = "VendorInvoiceRefer.findByTarikh", query = "SELECT v FROM VendorInvoiceRefer v WHERE v.tarikh = :tarikh"),
    @NamedQuery(name = "VendorInvoiceRefer.findByRemark", query = "SELECT v FROM VendorInvoiceRefer v WHERE v.remark = :remark"),
    @NamedQuery(name = "VendorInvoiceRefer.findByAmount", query = "SELECT v FROM VendorInvoiceRefer v WHERE v.amount = :amount"),
    @NamedQuery(name = "VendorInvoiceRefer.findByReferid", query = "SELECT v FROM VendorInvoiceRefer v WHERE v.referid = :referid")})
public class VendorInvoiceRefer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "no")
    private String no;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "tarikh")
    private String tarikh;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "remark")
    private String remark;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "referid")
    private String referid;

    public VendorInvoiceRefer() {
    }

    public VendorInvoiceRefer(String referid) {
        this.referid = referid;
    }

    public VendorInvoiceRefer(String referid, String refer, String type, String no, String tarikh, String remark, double amount) {
        this.referid = referid;
        this.refer = refer;
        this.type = type;
        this.no = no;
        this.tarikh = tarikh;
        this.remark = remark;
        this.amount = amount;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getTarikh() {
        return tarikh;
    }

    public void setTarikh(String tarikh) {
        this.tarikh = tarikh;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getReferid() {
        return referid;
    }

    public void setReferid(String referid) {
        this.referid = referid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (referid != null ? referid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VendorInvoiceRefer)) {
            return false;
        }
        VendorInvoiceRefer other = (VendorInvoiceRefer) object;
        if ((this.referid == null && other.referid != null) || (this.referid != null && !this.referid.equals(other.referid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.ap.VendorInvoiceRefer[ referid=" + referid + " ]";
    }
    
}
