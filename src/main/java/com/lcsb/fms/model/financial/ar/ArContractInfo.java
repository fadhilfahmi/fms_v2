/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ar;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class ArContractInfo {
    
    private String code;
    private String descp;
    private String loccode;
    private String locname;
    private String actcode;
    private String actdescp;
    private String address;
    private String active;
    private List<ArContractAgree> listAgree;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLocname() {
        return locname;
    }

    public void setLocname(String locname) {
        this.locname = locname;
    }

    public String getActcode() {
        return actcode;
    }

    public void setActcode(String actcode) {
        this.actcode = actcode;
    }

    public String getActdescp() {
        return actdescp;
    }

    public void setActdescp(String actdescp) {
        this.actdescp = actdescp;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public List<ArContractAgree> getListAgree() {
        return listAgree;
    }

    public void setListAgree(List<ArContractAgree> listAgree) {
        this.listAgree = listAgree;
    }

    
    
    
    
}
