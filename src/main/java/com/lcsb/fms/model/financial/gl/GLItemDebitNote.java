/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.gl;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "gl_itemdebitnote")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GLItemDebitNote.findAll", query = "SELECT g FROM GLItemDebitNote g"),
    @NamedQuery(name = "GLItemDebitNote.findByNoteno", query = "SELECT g FROM GLItemDebitNote g WHERE g.noteno = :noteno"),
    @NamedQuery(name = "GLItemDebitNote.findByQuantity", query = "SELECT g FROM GLItemDebitNote g WHERE g.quantity = :quantity"),
    @NamedQuery(name = "GLItemDebitNote.findByUnitprice", query = "SELECT g FROM GLItemDebitNote g WHERE g.unitprice = :unitprice"),
    @NamedQuery(name = "GLItemDebitNote.findByAmount", query = "SELECT g FROM GLItemDebitNote g WHERE g.amount = :amount"),
    @NamedQuery(name = "GLItemDebitNote.findByDate", query = "SELECT g FROM GLItemDebitNote g WHERE g.date = :date"),
    @NamedQuery(name = "GLItemDebitNote.findByRefer", query = "SELECT g FROM GLItemDebitNote g WHERE g.refer = :refer"),
    @NamedQuery(name = "GLItemDebitNote.findByReference", query = "SELECT g FROM GLItemDebitNote g WHERE g.reference = :reference")})
public class GLItemDebitNote implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "noteno")
    private String noteno;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "descp")
    private String descp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantity")
    private double quantity;
    @Basic(optional = false)
    @NotNull
    @Column(name = "unitprice")
    private double unitprice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "refer")
    private Integer refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "reference")
    private String reference;

    public GLItemDebitNote() {
    }

    public GLItemDebitNote(Integer refer) {
        this.refer = refer;
    }

    public GLItemDebitNote(Integer refer, String noteno, String descp, double quantity, double unitprice, double amount, Date date, String reference) {
        this.refer = refer;
        this.noteno = noteno;
        this.descp = descp;
        this.quantity = quantity;
        this.unitprice = unitprice;
        this.amount = amount;
        this.date = date;
        this.reference = reference;
    }

    public String getNoteno() {
        return noteno;
    }

    public void setNoteno(String noteno) {
        this.noteno = noteno;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(double unitprice) {
        this.unitprice = unitprice;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getRefer() {
        return refer;
    }

    public void setRefer(Integer refer) {
        this.refer = refer;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GLItemDebitNote)) {
            return false;
        }
        GLItemDebitNote other = (GLItemDebitNote) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.gl.GLItemDebitNote[ refer=" + refer + " ]";
    }
    
}
