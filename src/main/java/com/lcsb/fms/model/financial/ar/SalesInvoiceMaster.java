/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ar;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class SalesInvoiceMaster {
    
    private SalesInvoice siMain;
    private List<SalesInvoiceItem> siList;

    public SalesInvoice getSiMain() {
        return siMain;
    }

    public void setSiMain(SalesInvoice siMain) {
        this.siMain = siMain;
    }

    public List<SalesInvoiceItem> getSiList() {
        return siList;
    }

    public void setSiList(List<SalesInvoiceItem> siList) {
        this.siList = siList;
    }

    
}
