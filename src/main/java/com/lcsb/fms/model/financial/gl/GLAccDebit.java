/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.gl;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "gl_accdebit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GLAccDebit.findAll", query = "SELECT g FROM GLAccDebit g"),
    @NamedQuery(name = "GLAccDebit.findByNoteno", query = "SELECT g FROM GLAccDebit g WHERE g.noteno = :noteno"),
    @NamedQuery(name = "GLAccDebit.findByAcccode", query = "SELECT g FROM GLAccDebit g WHERE g.acccode = :acccode"),
    @NamedQuery(name = "GLAccDebit.findByAccdesc", query = "SELECT g FROM GLAccDebit g WHERE g.accdesc = :accdesc"),
    @NamedQuery(name = "GLAccDebit.findByLoclevel", query = "SELECT g FROM GLAccDebit g WHERE g.loclevel = :loclevel"),
    @NamedQuery(name = "GLAccDebit.findByLoccode", query = "SELECT g FROM GLAccDebit g WHERE g.loccode = :loccode"),
    @NamedQuery(name = "GLAccDebit.findByLocdesc", query = "SELECT g FROM GLAccDebit g WHERE g.locdesc = :locdesc"),
    @NamedQuery(name = "GLAccDebit.findByCttype", query = "SELECT g FROM GLAccDebit g WHERE g.cttype = :cttype"),
    @NamedQuery(name = "GLAccDebit.findByCtcode", query = "SELECT g FROM GLAccDebit g WHERE g.ctcode = :ctcode"),
    @NamedQuery(name = "GLAccDebit.findByCtdesc", query = "SELECT g FROM GLAccDebit g WHERE g.ctdesc = :ctdesc"),
    @NamedQuery(name = "GLAccDebit.findByAmount", query = "SELECT g FROM GLAccDebit g WHERE g.amount = :amount"),
    @NamedQuery(name = "GLAccDebit.findByRefer", query = "SELECT g FROM GLAccDebit g WHERE g.refer = :refer"),
    @NamedQuery(name = "GLAccDebit.findBySatype", query = "SELECT g FROM GLAccDebit g WHERE g.satype = :satype"),
    @NamedQuery(name = "GLAccDebit.findBySacode", query = "SELECT g FROM GLAccDebit g WHERE g.sacode = :sacode"),
    @NamedQuery(name = "GLAccDebit.findBySadesc", query = "SELECT g FROM GLAccDebit g WHERE g.sadesc = :sadesc")})
public class GLAccDebit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "noteno")
    private String noteno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "acccode")
    private String acccode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "accdesc")
    private String accdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loclevel")
    private String loclevel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loccode")
    private String loccode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "locdesc")
    private String locdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cttype")
    private String cttype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ctcode")
    private String ctcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ctdesc")
    private String ctdesc;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "remark")
    private String remark;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "refer")
    private Integer refer;
    @Size(max = 100)
    @Column(name = "satype")
    private String satype;
    @Size(max = 100)
    @Column(name = "sacode")
    private String sacode;
    @Size(max = 100)
    @Column(name = "sadesc")
    private String sadesc;

    public GLAccDebit() {
    }

    public GLAccDebit(Integer refer) {
        this.refer = refer;
    }

    public GLAccDebit(Integer refer, String noteno, String acccode, String accdesc, String loclevel, String loccode, String locdesc, String cttype, String ctcode, String ctdesc, String remark, double amount) {
        this.refer = refer;
        this.noteno = noteno;
        this.acccode = acccode;
        this.accdesc = accdesc;
        this.loclevel = loclevel;
        this.loccode = loccode;
        this.locdesc = locdesc;
        this.cttype = cttype;
        this.ctcode = ctcode;
        this.ctdesc = ctdesc;
        this.remark = remark;
        this.amount = amount;
    }

    public String getNoteno() {
        return noteno;
    }

    public void setNoteno(String noteno) {
        this.noteno = noteno;
    }

    public String getAcccode() {
        return acccode;
    }

    public void setAcccode(String acccode) {
        this.acccode = acccode;
    }

    public String getAccdesc() {
        return accdesc;
    }

    public void setAccdesc(String accdesc) {
        this.accdesc = accdesc;
    }

    public String getLoclevel() {
        return loclevel;
    }

    public void setLoclevel(String loclevel) {
        this.loclevel = loclevel;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLocdesc() {
        return locdesc;
    }

    public void setLocdesc(String locdesc) {
        this.locdesc = locdesc;
    }

    public String getCttype() {
        return cttype;
    }

    public void setCttype(String cttype) {
        this.cttype = cttype;
    }

    public String getCtcode() {
        return ctcode;
    }

    public void setCtcode(String ctcode) {
        this.ctcode = ctcode;
    }

    public String getCtdesc() {
        return ctdesc;
    }

    public void setCtdesc(String ctdesc) {
        this.ctdesc = ctdesc;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Integer getRefer() {
        return refer;
    }

    public void setRefer(Integer refer) {
        this.refer = refer;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GLAccDebit)) {
            return false;
        }
        GLAccDebit other = (GLAccDebit) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.gl.GLAccDebit[ refer=" + refer + " ]";
    }
    
}
