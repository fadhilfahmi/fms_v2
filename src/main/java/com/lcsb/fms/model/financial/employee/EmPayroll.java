/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.employee;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "em_payroll_master")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EmPayroll.findAll", query = "SELECT e FROM EmPayroll e"),
    @NamedQuery(name = "EmPayroll.findByRefer", query = "SELECT e FROM EmPayroll e WHERE e.refer = :refer"),
    @NamedQuery(name = "EmPayroll.findByYear", query = "SELECT e FROM EmPayroll e WHERE e.year = :year"),
    @NamedQuery(name = "EmPayroll.findByPeriod", query = "SELECT e FROM EmPayroll e WHERE e.period = :period"),
    @NamedQuery(name = "EmPayroll.findByPreid", query = "SELECT e FROM EmPayroll e WHERE e.preid = :preid"),
    @NamedQuery(name = "EmPayroll.findByPrename", query = "SELECT e FROM EmPayroll e WHERE e.prename = :prename"),
    @NamedQuery(name = "EmPayroll.findByPredate", query = "SELECT e FROM EmPayroll e WHERE e.predate = :predate"),
    @NamedQuery(name = "EmPayroll.findByPreposition", query = "SELECT e FROM EmPayroll e WHERE e.preposition = :preposition"),
    @NamedQuery(name = "EmPayroll.findByDate", query = "SELECT e FROM EmPayroll e WHERE e.date = :date"),
    @NamedQuery(name = "EmPayroll.findByStatus", query = "SELECT e FROM EmPayroll e WHERE e.status = :status"),
    @NamedQuery(name = "EmPayroll.findByCompanycode", query = "SELECT e FROM EmPayroll e WHERE e.companycode = :companycode"),
    @NamedQuery(name = "EmPayroll.findByCompanydescp", query = "SELECT e FROM EmPayroll e WHERE e.companydescp = :companydescp")})
public class EmPayroll implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "period")
    private String period;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "preid")
    private String preid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "prename")
    private String prename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "predate")
    private String predate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "preposition")
    private String preposition;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "date")
    private String date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "companycode")
    private String companycode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "companydescp")
    private String companydescp;

    public EmPayroll() {
    }

    public EmPayroll(String refer) {
        this.refer = refer;
    }

    public EmPayroll(String refer, String year, String period, String preid, String prename, String predate, String preposition, String date, String status, String companycode, String companydescp) {
        this.refer = refer;
        this.year = year;
        this.period = period;
        this.preid = preid;
        this.prename = prename;
        this.predate = predate;
        this.preposition = preposition;
        this.date = date;
        this.status = status;
        this.companycode = companycode;
        this.companydescp = companydescp;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getPreid() {
        return preid;
    }

    public void setPreid(String preid) {
        this.preid = preid;
    }

    public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        this.prename = prename;
    }

    public String getPredate() {
        return predate;
    }

    public void setPredate(String predate) {
        this.predate = predate;
    }

    public String getPreposition() {
        return preposition;
    }

    public void setPreposition(String preposition) {
        this.preposition = preposition;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompanycode() {
        return companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public String getCompanydescp() {
        return companydescp;
    }

    public void setCompanydescp(String companydescp) {
        this.companydescp = companydescp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmPayroll)) {
            return false;
        }
        EmPayroll other = (EmPayroll) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.employee.EmPayroll[ refer=" + refer + " ]";
    }
    
}
