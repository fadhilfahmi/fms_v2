/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.model.financial.gl;

/**
 *
 * @author Dell
 */
public class Period {
    
    private String startperiod;
    private String endperiod;
    private int month;
    private int period;

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }
    
   

    public String getStartperiod() {
        return startperiod;
    }

    public void setStartperiod(String startperiod) {
        this.startperiod = startperiod;
    }

    public String getEndperiod() {
        return endperiod;
    }

    public void setEndperiod(String endperiod) {
        this.endperiod = endperiod;
    }
    
    
    
}
