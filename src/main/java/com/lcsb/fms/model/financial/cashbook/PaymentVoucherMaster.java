/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class PaymentVoucherMaster {
    
    private PaymentVoucher pvMain;
    private List<PaymentVoucherItem> pvList;

    public PaymentVoucher getPvMain() {
        return pvMain;
    }

    public void setPvMain(PaymentVoucher pvMain) {
        this.pvMain = pvMain;
    }

    public List<PaymentVoucherItem> getPvList() {
        return pvList;
    }

    public void setPvList(List<PaymentVoucherItem> pvList) {
        this.pvList = pvList;
    }
    
    
    
}
