/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ar;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class ArDebitNoteMaster {
    
    private ArDebitNote aMain;
    private List<ArDebitNoteItem> aList;

    public ArDebitNote getaMain() {
        return aMain;
    }

    public void setaMain(ArDebitNote aMain) {
        this.aMain = aMain;
    }

    public List<ArDebitNoteItem> getaList() {
        return aList;
    }

    public void setaList(List<ArDebitNoteItem> aList) {
        this.aList = aList;
    }
    
}
