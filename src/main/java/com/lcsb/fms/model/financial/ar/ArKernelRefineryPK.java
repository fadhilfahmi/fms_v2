/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ar;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author fadhilfahmi
 */
@Embeddable
public class ArKernelRefineryPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "trailer")
    private String trailer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "lori")
    private String lori;

    public ArKernelRefineryPK() {
    }

    public ArKernelRefineryPK(String code, Date date, String trailer, String lori) {
        this.code = code;
        this.date = date;
        this.trailer = trailer;
        this.lori = lori;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getLori() {
        return lori;
    }

    public void setLori(String lori) {
        this.lori = lori;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        hash += (date != null ? date.hashCode() : 0);
        hash += (trailer != null ? trailer.hashCode() : 0);
        hash += (lori != null ? lori.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArKernelRefineryPK)) {
            return false;
        }
        ArKernelRefineryPK other = (ArKernelRefineryPK) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        if ((this.date == null && other.date != null) || (this.date != null && !this.date.equals(other.date))) {
            return false;
        }
        if ((this.trailer == null && other.trailer != null) || (this.trailer != null && !this.trailer.equals(other.trailer))) {
            return false;
        }
        if ((this.lori == null && other.lori != null) || (this.lori != null && !this.lori.equals(other.lori))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.ar.ArKernelRefineryPK[ code=" + code + ", date=" + date + ", trailer=" + trailer + ", lori=" + lori + " ]";
    }
    
}
