/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ar;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "sl_inv_ffb")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ArFFB.findAll", query = "SELECT a FROM ArFFB a"),
    @NamedQuery(name = "ArFFB.findById", query = "SELECT a FROM ArFFB a WHERE a.id = :id"),
    @NamedQuery(name = "ArFFB.findByFfb", query = "SELECT a FROM ArFFB a WHERE a.ffb = :ffb"),
    @NamedQuery(name = "ArFFB.findByMpobpricecpo", query = "SELECT a FROM ArFFB a WHERE a.mpobpricecpo = :mpobpricecpo"),
    @NamedQuery(name = "ArFFB.findByMpobpricepk", query = "SELECT a FROM ArFFB a WHERE a.mpobpricepk = :mpobpricepk"),
    @NamedQuery(name = "ArFFB.findByCess", query = "SELECT a FROM ArFFB a WHERE a.cess = :cess"),
    @NamedQuery(name = "ArFFB.findByMillcost", query = "SELECT a FROM ArFFB a WHERE a.millcost = :millcost"),
    @NamedQuery(name = "ArFFB.findByOer", query = "SELECT a FROM ArFFB a WHERE a.oer = :oer"),
    @NamedQuery(name = "ArFFB.findByKer", query = "SELECT a FROM ArFFB a WHERE a.ker = :ker"),
    @NamedQuery(name = "ArFFB.findByDiscount", query = "SELECT a FROM ArFFB a WHERE a.discount = :discount"),
    @NamedQuery(name = "ArFFB.findByWinfall", query = "SELECT a FROM ArFFB a WHERE a.winfall = :winfall"),
    @NamedQuery(name = "ArFFB.findByTransport", query = "SELECT a FROM ArFFB a WHERE a.transport = :transport"),
    @NamedQuery(name = "ArFFB.findByExternalffb", query = "SELECT a FROM ArFFB a WHERE a.externalffb = :externalffb"),
    @NamedQuery(name = "ArFFB.findBySessionid", query = "SELECT a FROM ArFFB a WHERE a.sessionid = :sessionid")})
public class ArFFB implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ffb")
    private double ffb;
    @Basic(optional = false)
    @NotNull
    @Column(name = "mpobpricecpo")
    private double mpobpricecpo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "mpobpricepk")
    private double mpobpricepk;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cess")
    private double cess;
    @Basic(optional = false)
    @NotNull
    @Column(name = "millcost")
    private double millcost;
    @Basic(optional = false)
    @NotNull
    @Column(name = "oer")
    private double oer;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ker")
    private double ker;
    @Basic(optional = false)
    @NotNull
    @Column(name = "discount")
    private double discount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "winfall")
    private double winfall;
    @Basic(optional = false)
    @NotNull
    @Column(name = "transport")
    private double transport;
    @Basic(optional = false)
    @NotNull
    @Column(name = "externalffb")
    private double externalffb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "sessionid")
    private String sessionid;
    private double nettcpo;
    private double costcpo;
    private double costpk;
    private double totalcost;
    private double priceperton;
    private double total;
    private double gst;
    private double nettamount;
    private String year;
    private String month;
    private String invref;
    
    
    

    public ArFFB() {
    }

    public ArFFB(Integer id) {
        this.id = id;
    }

    public ArFFB(Integer id, double ffb, double mpobpricecpo, double mpobpricepk, double cess, double millcost, double oer, double ker, double discount, double winfall, double transport, double externalffb, String sessionid,double nettcpo,double costcpo,double costpk,double totalcost,double priceperton,double total,double gst,double nettamount, String year, String month, String invref) {
        this.id = id;
        this.ffb = ffb;
        this.mpobpricecpo = mpobpricecpo;
        this.mpobpricepk = mpobpricepk;
        this.cess = cess;
        this.millcost = millcost;
        this.oer = oer;
        this.ker = ker;
        this.discount = discount;
        this.winfall = winfall;
        this.transport = transport;
        this.externalffb = externalffb;
        this.sessionid = sessionid;
        
        this.nettcpo = nettcpo;
        this.costcpo = costcpo;
        this.costpk = costpk;
        this.totalcost = totalcost;
        this.priceperton = priceperton;
        this.total = total;
        this.gst = gst;
        this.nettamount = nettamount;
        this.year = year;
        this.month = month;
        this.invref = invref;
    }

    public String getInvref() {
        return invref;
    }

    public void setInvref(String invref) {
        this.invref = invref;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public double getNettcpo() {
        return nettcpo;
    }

    public void setNettcpo(double nettcpo) {
        this.nettcpo = nettcpo;
    }

    public double getCostcpo() {
        return costcpo;
    }

    public void setCostcpo(double costcpo) {
        this.costcpo = costcpo;
    }

    public double getCostpk() {
        return costpk;
    }

    public void setCostpk(double costpk) {
        this.costpk = costpk;
    }

    public double getTotalcost() {
        return totalcost;
    }

    public void setTotalcost(double totalcost) {
        this.totalcost = totalcost;
    }

    public double getPriceperton() {
        return priceperton;
    }

    public void setPriceperton(double priceperton) {
        this.priceperton = priceperton;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getGst() {
        return gst;
    }

    public void setGst(double gst) {
        this.gst = gst;
    }

    public double getNettamount() {
        return nettamount;
    }

    public void setNettamount(double nettamount) {
        this.nettamount = nettamount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getFfb() {
        return ffb;
    }

    public void setFfb(double ffb) {
        this.ffb = ffb;
    }

    public double getMpobpricecpo() {
        return mpobpricecpo;
    }

    public void setMpobpricecpo(double mpobpricecpo) {
        this.mpobpricecpo = mpobpricecpo;
    }

    public double getMpobpricepk() {
        return mpobpricepk;
    }

    public void setMpobpricepk(double mpobpricepk) {
        this.mpobpricepk = mpobpricepk;
    }

    public double getCess() {
        return cess;
    }

    public void setCess(double cess) {
        this.cess = cess;
    }

    public double getMillcost() {
        return millcost;
    }

    public void setMillcost(double millcost) {
        this.millcost = millcost;
    }

    public double getOer() {
        return oer;
    }

    public void setOer(double oer) {
        this.oer = oer;
    }

    public double getKer() {
        return ker;
    }

    public void setKer(double ker) {
        this.ker = ker;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getWinfall() {
        return winfall;
    }

    public void setWinfall(double winfall) {
        this.winfall = winfall;
    }

    public double getTransport() {
        return transport;
    }

    public void setTransport(double transport) {
        this.transport = transport;
    }

    public double getExternalffb() {
        return externalffb;
    }

    public void setExternalffb(double externalffb) {
        this.externalffb = externalffb;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArFFB)) {
            return false;
        }
        ArFFB other = (ArFFB) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.ar.ArFFB[ id=" + id + " ]";
    }
    
}
