/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "cb_official_invoice")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ORInvoice.findAll", query = "SELECT o FROM ORInvoice o"),
    @NamedQuery(name = "ORInvoice.findById", query = "SELECT o FROM ORInvoice o WHERE o.id = :id"),
    @NamedQuery(name = "ORInvoice.findByOrnno", query = "SELECT o FROM ORInvoice o WHERE o.ornno = :ornno"),
    @NamedQuery(name = "ORInvoice.findByInvno", query = "SELECT o FROM ORInvoice o WHERE o.invno = :invno"),
    @NamedQuery(name = "ORInvoice.findByOrnrefno", query = "SELECT o FROM ORInvoice o WHERE o.ornrefno = :ornrefno"),
    @NamedQuery(name = "ORInvoice.findByAmount", query = "SELECT o FROM ORInvoice o WHERE o.amount = :amount")})
public class ORInvoice implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "ornno")
    private String ornno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "invno")
    private String invno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 19)
    @Column(name = "ornrefno")
    private String ornrefno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;

    public ORInvoice() {
    }

    public ORInvoice(Integer id) {
        this.id = id;
    }

    public ORInvoice(Integer id, String ornno, String invno, String ornrefno, double amount) {
        this.id = id;
        this.ornno = ornno;
        this.invno = invno;
        this.ornrefno = ornrefno;
        this.amount = amount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrnno() {
        return ornno;
    }

    public void setOrnno(String ornno) {
        this.ornno = ornno;
    }

    public String getInvno() {
        return invno;
    }

    public void setInvno(String invno) {
        this.invno = invno;
    }

    public String getOrnrefno() {
        return ornrefno;
    }

    public void setOrnrefno(String ornrefno) {
        this.ornrefno = ornrefno;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ORInvoice)) {
            return false;
        }
        ORInvoice other = (ORInvoice) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.cashbook.ORInvoice[ id=" + id + " ]";
    }
    
}
