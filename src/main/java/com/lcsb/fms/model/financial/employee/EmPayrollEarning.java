/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.employee;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "em_payroll_earning")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EmPayrollEarning.findAll", query = "SELECT e FROM EmPayrollEarning e"),
    @NamedQuery(name = "EmPayrollEarning.findByRefer", query = "SELECT e FROM EmPayrollEarning e WHERE e.refer = :refer"),
    @NamedQuery(name = "EmPayrollEarning.findByYear", query = "SELECT e FROM EmPayrollEarning e WHERE e.year = :year"),
    @NamedQuery(name = "EmPayrollEarning.findByPeriod", query = "SELECT e FROM EmPayrollEarning e WHERE e.period = :period"),
    @NamedQuery(name = "EmPayrollEarning.findByStaffid", query = "SELECT e FROM EmPayrollEarning e WHERE e.staffid = :staffid"),
    @NamedQuery(name = "EmPayrollEarning.findByEarncode", query = "SELECT e FROM EmPayrollEarning e WHERE e.earncode = :earncode"),
    @NamedQuery(name = "EmPayrollEarning.findByEarndesc", query = "SELECT e FROM EmPayrollEarning e WHERE e.earndesc = :earndesc"),
    @NamedQuery(name = "EmPayrollEarning.findByAmount", query = "SELECT e FROM EmPayrollEarning e WHERE e.amount = :amount"),
    @NamedQuery(name = "EmPayrollEarning.findByStaffname", query = "SELECT e FROM EmPayrollEarning e WHERE e.staffname = :staffname"),
    @NamedQuery(name = "EmPayrollEarning.findByEpf", query = "SELECT e FROM EmPayrollEarning e WHERE e.epf = :epf"),
    @NamedQuery(name = "EmPayrollEarning.findBySocso", query = "SELECT e FROM EmPayrollEarning e WHERE e.socso = :socso"),
    @NamedQuery(name = "EmPayrollEarning.findByBasicflag", query = "SELECT e FROM EmPayrollEarning e WHERE e.basicflag = :basicflag"),
    @NamedQuery(name = "EmPayrollEarning.findByLoclevel", query = "SELECT e FROM EmPayrollEarning e WHERE e.loclevel = :loclevel"),
    @NamedQuery(name = "EmPayrollEarning.findByDaymth", query = "SELECT e FROM EmPayrollEarning e WHERE e.daymth = :daymth"),
    @NamedQuery(name = "EmPayrollEarning.findByFlag", query = "SELECT e FROM EmPayrollEarning e WHERE e.flag = :flag"),
    @NamedQuery(name = "EmPayrollEarning.findByPosition", query = "SELECT e FROM EmPayrollEarning e WHERE e.position = :position"),
    @NamedQuery(name = "EmPayrollEarning.findByEarntype", query = "SELECT e FROM EmPayrollEarning e WHERE e.earntype = :earntype"),
    @NamedQuery(name = "EmPayrollEarning.findByMinwday", query = "SELECT e FROM EmPayrollEarning e WHERE e.minwday = :minwday"),
    @NamedQuery(name = "EmPayrollEarning.findByEstatecode", query = "SELECT e FROM EmPayrollEarning e WHERE e.estatecode = :estatecode"),
    @NamedQuery(name = "EmPayrollEarning.findByEstatename", query = "SELECT e FROM EmPayrollEarning e WHERE e.estatename = :estatename"),
    @NamedQuery(name = "EmPayrollEarning.findByStafftype", query = "SELECT e FROM EmPayrollEarning e WHERE e.stafftype = :stafftype"),
    @NamedQuery(name = "EmPayrollEarning.findBySalary", query = "SELECT e FROM EmPayrollEarning e WHERE e.salary = :salary"),
    @NamedQuery(name = "EmPayrollEarning.findByGuna", query = "SELECT e FROM EmPayrollEarning e WHERE e.guna = :guna"),
    @NamedQuery(name = "EmPayrollEarning.findByIncludeinp", query = "SELECT e FROM EmPayrollEarning e WHERE e.includeinp = :includeinp")})
public class EmPayrollEarning implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "period")
    private String period;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "staffid")
    private String staffid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "earncode")
    private String earncode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "earndesc")
    private String earndesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "staffname")
    private String staffname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "epf")
    private String epf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "socso")
    private String socso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "basicflag")
    private String basicflag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loclevel")
    private String loclevel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "daymth")
    private String daymth;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "flag")
    private String flag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "position")
    private String position;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "earntype")
    private String earntype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "minwday")
    private String minwday;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatecode")
    private String estatecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatename")
    private String estatename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "stafftype")
    private String stafftype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "salary")
    private String salary;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "guna")
    private String guna;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "includeinp")
    private String includeinp;

    public EmPayrollEarning() {
    }

    public EmPayrollEarning(String refer) {
        this.refer = refer;
    }

    public EmPayrollEarning(String refer, String year, String period, String staffid, String earncode, String earndesc, double amount, String staffname, String epf, String socso, String basicflag, String loclevel, String daymth, String flag, String position, String earntype, String minwday, String estatecode, String estatename, String stafftype, String salary, String guna, String includeinp) {
        this.refer = refer;
        this.year = year;
        this.period = period;
        this.staffid = staffid;
        this.earncode = earncode;
        this.earndesc = earndesc;
        this.amount = amount;
        this.staffname = staffname;
        this.epf = epf;
        this.socso = socso;
        this.basicflag = basicflag;
        this.loclevel = loclevel;
        this.daymth = daymth;
        this.flag = flag;
        this.position = position;
        this.earntype = earntype;
        this.minwday = minwday;
        this.estatecode = estatecode;
        this.estatename = estatename;
        this.stafftype = stafftype;
        this.salary = salary;
        this.guna = guna;
        this.includeinp = includeinp;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    public String getEarncode() {
        return earncode;
    }

    public void setEarncode(String earncode) {
        this.earncode = earncode;
    }

    public String getEarndesc() {
        return earndesc;
    }

    public void setEarndesc(String earndesc) {
        this.earndesc = earndesc;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getStaffname() {
        return staffname;
    }

    public void setStaffname(String staffname) {
        this.staffname = staffname;
    }

    public String getEpf() {
        return epf;
    }

    public void setEpf(String epf) {
        this.epf = epf;
    }

    public String getSocso() {
        return socso;
    }

    public void setSocso(String socso) {
        this.socso = socso;
    }

    public String getBasicflag() {
        return basicflag;
    }

    public void setBasicflag(String basicflag) {
        this.basicflag = basicflag;
    }

    public String getLoclevel() {
        return loclevel;
    }

    public void setLoclevel(String loclevel) {
        this.loclevel = loclevel;
    }

    public String getDaymth() {
        return daymth;
    }

    public void setDaymth(String daymth) {
        this.daymth = daymth;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getEarntype() {
        return earntype;
    }

    public void setEarntype(String earntype) {
        this.earntype = earntype;
    }

    public String getMinwday() {
        return minwday;
    }

    public void setMinwday(String minwday) {
        this.minwday = minwday;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getEstatename() {
        return estatename;
    }

    public void setEstatename(String estatename) {
        this.estatename = estatename;
    }

    public String getStafftype() {
        return stafftype;
    }

    public void setStafftype(String stafftype) {
        this.stafftype = stafftype;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getGuna() {
        return guna;
    }

    public void setGuna(String guna) {
        this.guna = guna;
    }

    public String getIncludeinp() {
        return includeinp;
    }

    public void setIncludeinp(String includeinp) {
        this.includeinp = includeinp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmPayrollEarning)) {
            return false;
        }
        EmPayrollEarning other = (EmPayrollEarning) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.employee.EmPayrollEarning[ refer=" + refer + " ]";
    }
    
}
