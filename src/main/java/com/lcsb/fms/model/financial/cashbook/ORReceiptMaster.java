/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class ORReceiptMaster {
    
    private OfficialReceipt master;
    private List<OfficialCreditItem> listInvoice;

    public OfficialReceipt getMaster() {
        return master;
    }

    public void setMaster(OfficialReceipt master) {
        this.master = master;
    }

    public List<OfficialCreditItem> getListInvoice() {
        return listInvoice;
    }

    public void setListInvoice(List<OfficialCreditItem> listInvoice) {
        this.listInvoice = listInvoice;
    }
    
    
}
