/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author fadhilfahmi
 */
@Embeddable
public class BankReconcilePK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "codebank")
    private String codebank;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "period")
    private String period;

    public BankReconcilePK() {
    }

    public BankReconcilePK(String codebank, String year, String period) {
        this.codebank = codebank;
        this.year = year;
        this.period = period;
    }

    public String getCodebank() {
        return codebank;
    }

    public void setCodebank(String codebank) {
        this.codebank = codebank;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codebank != null ? codebank.hashCode() : 0);
        hash += (year != null ? year.hashCode() : 0);
        hash += (period != null ? period.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BankReconcilePK)) {
            return false;
        }
        BankReconcilePK other = (BankReconcilePK) object;
        if ((this.codebank == null && other.codebank != null) || (this.codebank != null && !this.codebank.equals(other.codebank))) {
            return false;
        }
        if ((this.year == null && other.year != null) || (this.year != null && !this.year.equals(other.year))) {
            return false;
        }
        if ((this.period == null && other.period != null) || (this.period != null && !this.period.equals(other.period))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.cashbook.bankreport.reconcile.BankReconcilePK[ codebank=" + codebank + ", year=" + year + ", period=" + period + " ]";
    }
    
}
