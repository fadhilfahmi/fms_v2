/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.tx;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "tx_gst03")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TxGst03.findAll", query = "SELECT t FROM TxGst03 t"),
    @NamedQuery(name = "TxGst03.findByRefer", query = "SELECT t FROM TxGst03 t WHERE t.refer = :refer"),
    @NamedQuery(name = "TxGst03.findByYear", query = "SELECT t FROM TxGst03 t WHERE t.year = :year"),
    @NamedQuery(name = "TxGst03.findByTaxperiod", query = "SELECT t FROM TxGst03 t WHERE t.taxperiod = :taxperiod"),
    @NamedQuery(name = "TxGst03.findByTaxyear", query = "SELECT t FROM TxGst03 t WHERE t.taxyear = :taxyear"),
    @NamedQuery(name = "TxGst03.findByEstcode", query = "SELECT t FROM TxGst03 t WHERE t.estcode = :estcode"),
    @NamedQuery(name = "TxGst03.findByPeriod", query = "SELECT t FROM TxGst03 t WHERE t.period = :period"),
    @NamedQuery(name = "TxGst03.findByLoclevel", query = "SELECT t FROM TxGst03 t WHERE t.loclevel = :loclevel"),
    @NamedQuery(name = "TxGst03.findByLoccode", query = "SELECT t FROM TxGst03 t WHERE t.loccode = :loccode"),
    @NamedQuery(name = "TxGst03.findByLocname", query = "SELECT t FROM TxGst03 t WHERE t.locname = :locname"),
    @NamedQuery(name = "TxGst03.findByTaxstart", query = "SELECT t FROM TxGst03 t WHERE t.taxstart = :taxstart"),
    @NamedQuery(name = "TxGst03.findByTaxdue", query = "SELECT t FROM TxGst03 t WHERE t.taxdue = :taxdue"),
    @NamedQuery(name = "TxGst03.findByDate", query = "SELECT t FROM TxGst03 t WHERE t.date = :date"),
    @NamedQuery(name = "TxGst03.findByTaxend", query = "SELECT t FROM TxGst03 t WHERE t.taxend = :taxend"),
    @NamedQuery(name = "TxGst03.findByGstid", query = "SELECT t FROM TxGst03 t WHERE t.gstid = :gstid"),
    @NamedQuery(name = "TxGst03.findByAmend", query = "SELECT t FROM TxGst03 t WHERE t.amend = :amend"),
    @NamedQuery(name = "TxGst03.findByPreid", query = "SELECT t FROM TxGst03 t WHERE t.preid = :preid"),
    @NamedQuery(name = "TxGst03.findByPredate", query = "SELECT t FROM TxGst03 t WHERE t.predate = :predate"),
    @NamedQuery(name = "TxGst03.findByPrename", query = "SELECT t FROM TxGst03 t WHERE t.prename = :prename"),
    @NamedQuery(name = "TxGst03.findByPredesign", query = "SELECT t FROM TxGst03 t WHERE t.predesign = :predesign"),
    @NamedQuery(name = "TxGst03.findByAppid", query = "SELECT t FROM TxGst03 t WHERE t.appid = :appid"),
    @NamedQuery(name = "TxGst03.findByAppname", query = "SELECT t FROM TxGst03 t WHERE t.appname = :appname"),
    @NamedQuery(name = "TxGst03.findByAppdate", query = "SELECT t FROM TxGst03 t WHERE t.appdate = :appdate"),
    @NamedQuery(name = "TxGst03.findByAppdesign", query = "SELECT t FROM TxGst03 t WHERE t.appdesign = :appdesign"),
    @NamedQuery(name = "TxGst03.findByC1", query = "SELECT t FROM TxGst03 t WHERE t.c1 = :c1"),
    @NamedQuery(name = "TxGst03.findByC2", query = "SELECT t FROM TxGst03 t WHERE t.c2 = :c2"),
    @NamedQuery(name = "TxGst03.findByC3", query = "SELECT t FROM TxGst03 t WHERE t.c3 = :c3"),
    @NamedQuery(name = "TxGst03.findByC4", query = "SELECT t FROM TxGst03 t WHERE t.c4 = :c4"),
    @NamedQuery(name = "TxGst03.findByB5", query = "SELECT t FROM TxGst03 t WHERE t.b5 = :b5"),
    @NamedQuery(name = "TxGst03.findByC6", query = "SELECT t FROM TxGst03 t WHERE t.c6 = :c6"),
    @NamedQuery(name = "TxGst03.findByC7", query = "SELECT t FROM TxGst03 t WHERE t.c7 = :c7"),
    @NamedQuery(name = "TxGst03.findByC8", query = "SELECT t FROM TxGst03 t WHERE t.c8 = :c8"),
    @NamedQuery(name = "TxGst03.findByC9", query = "SELECT t FROM TxGst03 t WHERE t.c9 = :c9"),
    @NamedQuery(name = "TxGst03.findByC10", query = "SELECT t FROM TxGst03 t WHERE t.c10 = :c10"),
    @NamedQuery(name = "TxGst03.findByC11", query = "SELECT t FROM TxGst03 t WHERE t.c11 = :c11"),
    @NamedQuery(name = "TxGst03.findByC12", query = "SELECT t FROM TxGst03 t WHERE t.c12 = :c12"),
    @NamedQuery(name = "TxGst03.findByC13", query = "SELECT t FROM TxGst03 t WHERE t.c13 = :c13"),
    @NamedQuery(name = "TxGst03.findByC15", query = "SELECT t FROM TxGst03 t WHERE t.c15 = :c15"),
    @NamedQuery(name = "TxGst03.findByC17", query = "SELECT t FROM TxGst03 t WHERE t.c17 = :c17"),
    @NamedQuery(name = "TxGst03.findByC19", query = "SELECT t FROM TxGst03 t WHERE t.c19 = :c19"),
    @NamedQuery(name = "TxGst03.findByC21", query = "SELECT t FROM TxGst03 t WHERE t.c21 = :c21"),
    @NamedQuery(name = "TxGst03.findByC23", query = "SELECT t FROM TxGst03 t WHERE t.c23 = :c23"),
    @NamedQuery(name = "TxGst03.findByC24", query = "SELECT t FROM TxGst03 t WHERE t.c24 = :c24"),
    @NamedQuery(name = "TxGst03.findByI14", query = "SELECT t FROM TxGst03 t WHERE t.i14 = :i14"),
    @NamedQuery(name = "TxGst03.findByI16", query = "SELECT t FROM TxGst03 t WHERE t.i16 = :i16"),
    @NamedQuery(name = "TxGst03.findByI18", query = "SELECT t FROM TxGst03 t WHERE t.i18 = :i18"),
    @NamedQuery(name = "TxGst03.findByI20", query = "SELECT t FROM TxGst03 t WHERE t.i20 = :i20"),
    @NamedQuery(name = "TxGst03.findByI22", query = "SELECT t FROM TxGst03 t WHERE t.i22 = :i22"),
    @NamedQuery(name = "TxGst03.findByPay", query = "SELECT t FROM TxGst03 t WHERE t.pay = :pay"),
    @NamedQuery(name = "TxGst03.findByClaim", query = "SELECT t FROM TxGst03 t WHERE t.claim = :claim"),
    @NamedQuery(name = "TxGst03.findByPrcnt1", query = "SELECT t FROM TxGst03 t WHERE t.prcnt1 = :prcnt1"),
    @NamedQuery(name = "TxGst03.findByPrcnt2", query = "SELECT t FROM TxGst03 t WHERE t.prcnt2 = :prcnt2"),
    @NamedQuery(name = "TxGst03.findByPrcnt3", query = "SELECT t FROM TxGst03 t WHERE t.prcnt3 = :prcnt3"),
    @NamedQuery(name = "TxGst03.findByPrcnt4", query = "SELECT t FROM TxGst03 t WHERE t.prcnt4 = :prcnt4"),
    @NamedQuery(name = "TxGst03.findByPrcnt5", query = "SELECT t FROM TxGst03 t WHERE t.prcnt5 = :prcnt5"),
    @NamedQuery(name = "TxGst03.findByPrcnt6", query = "SELECT t FROM TxGst03 t WHERE t.prcnt6 = :prcnt6"),
    @NamedQuery(name = "TxGst03.findByNewIC", query = "SELECT t FROM TxGst03 t WHERE t.newIC = :newIC"),
    @NamedQuery(name = "TxGst03.findByOldIC", query = "SELECT t FROM TxGst03 t WHERE t.oldIC = :oldIC"),
    @NamedQuery(name = "TxGst03.findByPassNo", query = "SELECT t FROM TxGst03 t WHERE t.passNo = :passNo"),
    @NamedQuery(name = "TxGst03.findByNationality", query = "SELECT t FROM TxGst03 t WHERE t.nationality = :nationality"),
    @NamedQuery(name = "TxGst03.findByCoacode", query = "SELECT t FROM TxGst03 t WHERE t.coacode = :coacode"),
    @NamedQuery(name = "TxGst03.findByCoadescp", query = "SELECT t FROM TxGst03 t WHERE t.coadescp = :coadescp"),
    @NamedQuery(name = "TxGst03.findByGenjvref", query = "SELECT t FROM TxGst03 t WHERE t.genjvref = :genjvref"),
    @NamedQuery(name = "TxGst03.findByVoucherref", query = "SELECT t FROM TxGst03 t WHERE t.voucherref = :voucherref"),
    @NamedQuery(name = "TxGst03.findByOrRef", query = "SELECT t FROM TxGst03 t WHERE t.orRef = :orRef"),
    @NamedQuery(name = "TxGst03.findBySatype", query = "SELECT t FROM TxGst03 t WHERE t.satype = :satype"),
    @NamedQuery(name = "TxGst03.findBySacode", query = "SELECT t FROM TxGst03 t WHERE t.sacode = :sacode"),
    @NamedQuery(name = "TxGst03.findBySadesc", query = "SELECT t FROM TxGst03 t WHERE t.sadesc = :sadesc")})
public class TxGst03 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "taxperiod")
    private String taxperiod;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "taxyear")
    private String taxyear;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "estcode")
    private String estcode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "period")
    private int period;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loclevel")
    private String loclevel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loccode")
    private String loccode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "locname")
    private String locname;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxstart")
    @Temporal(TemporalType.DATE)
    private Date taxstart;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxdue")
    @Temporal(TemporalType.DATE)
    private Date taxdue;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxend")
    @Temporal(TemporalType.DATE)
    private Date taxend;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "gstid")
    private String gstid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "amend")
    private String amend;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "preid")
    private String preid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "predate")
    @Temporal(TemporalType.DATE)
    private Date predate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prename")
    private String prename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "predesign")
    private String predesign;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appid")
    private String appid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appname")
    private String appname;
    @Basic(optional = false)
    @NotNull
    @Column(name = "appdate")
    @Temporal(TemporalType.DATE)
    private Date appdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appdesign")
    private String appdesign;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "c1")
    private BigDecimal c1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "c2")
    private BigDecimal c2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "c3")
    private BigDecimal c3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "c4")
    private BigDecimal c4;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "b5")
    private String b5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "c6")
    private BigDecimal c6;
    @Basic(optional = false)
    @NotNull
    @Column(name = "c7")
    private BigDecimal c7;
    @Basic(optional = false)
    @NotNull
    @Column(name = "c8")
    private BigDecimal c8;
    @Basic(optional = false)
    @NotNull
    @Column(name = "c9")
    private BigDecimal c9;
    @Basic(optional = false)
    @NotNull
    @Column(name = "c10")
    private BigDecimal c10;
    @Basic(optional = false)
    @NotNull
    @Column(name = "c11")
    private BigDecimal c11;
    @Basic(optional = false)
    @NotNull
    @Column(name = "c12")
    private BigDecimal c12;
    @Basic(optional = false)
    @NotNull
    @Column(name = "c13")
    private BigDecimal c13;
    @Basic(optional = false)
    @NotNull
    @Column(name = "c15")
    private BigDecimal c15;
    @Basic(optional = false)
    @NotNull
    @Column(name = "c17")
    private BigDecimal c17;
    @Basic(optional = false)
    @NotNull
    @Column(name = "c19")
    private BigDecimal c19;
    @Basic(optional = false)
    @NotNull
    @Column(name = "c21")
    private BigDecimal c21;
    @Basic(optional = false)
    @NotNull
    @Column(name = "c23")
    private BigDecimal c23;
    @Basic(optional = false)
    @NotNull
    @Column(name = "c24")
    private BigDecimal c24;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "i14")
    private String i14;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "i16")
    private String i16;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "i18")
    private String i18;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "i20")
    private String i20;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "i22")
    private String i22;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pay")
    private BigDecimal pay;
    @Basic(optional = false)
    @NotNull
    @Column(name = "claim")
    private BigDecimal claim;
    @Basic(optional = false)
    @NotNull
    @Column(name = "prcnt1")
    private BigDecimal prcnt1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "prcnt2")
    private BigDecimal prcnt2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "prcnt3")
    private BigDecimal prcnt3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "prcnt4")
    private BigDecimal prcnt4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "prcnt5")
    private BigDecimal prcnt5;
    @Basic(optional = false)
    @NotNull
    @Column(name = "prcnt6")
    private BigDecimal prcnt6;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NewIC")
    private String newIC;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "OldIC")
    private String oldIC;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "PassNo")
    private String passNo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nationality")
    private String nationality;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coacode")
    private String coacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coadescp")
    private String coadescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "genjvref")
    private String genjvref;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "voucherref")
    private String voucherref;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "or_ref")
    private String orRef;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "satype")
    private String satype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "sacode")
    private String sacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "sadesc")
    private String sadesc;

    public TxGst03() {
    }

    public TxGst03(String refer) {
        this.refer = refer;
    }

    public TxGst03(String refer, String year, String taxperiod, String taxyear, String estcode, int period, String loclevel, String loccode, String locname, Date taxstart, Date taxdue, Date date, Date taxend, String gstid, String amend, String preid, Date predate, String prename, String predesign, String appid, String appname, Date appdate, String appdesign, BigDecimal c1, BigDecimal c2, BigDecimal c3, BigDecimal c4, String b5, BigDecimal c6, BigDecimal c7, BigDecimal c8, BigDecimal c9, BigDecimal c10, BigDecimal c11, BigDecimal c12, BigDecimal c13, BigDecimal c15, BigDecimal c17, BigDecimal c19, BigDecimal c21, BigDecimal c23, BigDecimal c24, String i14, String i16, String i18, String i20, String i22, BigDecimal pay, BigDecimal claim, BigDecimal prcnt1, BigDecimal prcnt2, BigDecimal prcnt3, BigDecimal prcnt4, BigDecimal prcnt5, BigDecimal prcnt6, String newIC, String oldIC, String passNo, String nationality, String coacode, String coadescp, String genjvref, String voucherref, String orRef, String satype, String sacode, String sadesc) {
        this.refer = refer;
        this.year = year;
        this.taxperiod = taxperiod;
        this.taxyear = taxyear;
        this.estcode = estcode;
        this.period = period;
        this.loclevel = loclevel;
        this.loccode = loccode;
        this.locname = locname;
        this.taxstart = taxstart;
        this.taxdue = taxdue;
        this.date = date;
        this.taxend = taxend;
        this.gstid = gstid;
        this.amend = amend;
        this.preid = preid;
        this.predate = predate;
        this.prename = prename;
        this.predesign = predesign;
        this.appid = appid;
        this.appname = appname;
        this.appdate = appdate;
        this.appdesign = appdesign;
        this.c1 = c1;
        this.c2 = c2;
        this.c3 = c3;
        this.c4 = c4;
        this.b5 = b5;
        this.c6 = c6;
        this.c7 = c7;
        this.c8 = c8;
        this.c9 = c9;
        this.c10 = c10;
        this.c11 = c11;
        this.c12 = c12;
        this.c13 = c13;
        this.c15 = c15;
        this.c17 = c17;
        this.c19 = c19;
        this.c21 = c21;
        this.c23 = c23;
        this.c24 = c24;
        this.i14 = i14;
        this.i16 = i16;
        this.i18 = i18;
        this.i20 = i20;
        this.i22 = i22;
        this.pay = pay;
        this.claim = claim;
        this.prcnt1 = prcnt1;
        this.prcnt2 = prcnt2;
        this.prcnt3 = prcnt3;
        this.prcnt4 = prcnt4;
        this.prcnt5 = prcnt5;
        this.prcnt6 = prcnt6;
        this.newIC = newIC;
        this.oldIC = oldIC;
        this.passNo = passNo;
        this.nationality = nationality;
        this.coacode = coacode;
        this.coadescp = coadescp;
        this.genjvref = genjvref;
        this.voucherref = voucherref;
        this.orRef = orRef;
        this.satype = satype;
        this.sacode = sacode;
        this.sadesc = sadesc;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTaxperiod() {
        return taxperiod;
    }

    public void setTaxperiod(String taxperiod) {
        this.taxperiod = taxperiod;
    }

    public String getTaxyear() {
        return taxyear;
    }

    public void setTaxyear(String taxyear) {
        this.taxyear = taxyear;
    }

    public String getEstcode() {
        return estcode;
    }

    public void setEstcode(String estcode) {
        this.estcode = estcode;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public String getLoclevel() {
        return loclevel;
    }

    public void setLoclevel(String loclevel) {
        this.loclevel = loclevel;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLocname() {
        return locname;
    }

    public void setLocname(String locname) {
        this.locname = locname;
    }

    public Date getTaxstart() {
        return taxstart;
    }

    public void setTaxstart(Date taxstart) {
        this.taxstart = taxstart;
    }

    public Date getTaxdue() {
        return taxdue;
    }

    public void setTaxdue(Date taxdue) {
        this.taxdue = taxdue;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTaxend() {
        return taxend;
    }

    public void setTaxend(Date taxend) {
        this.taxend = taxend;
    }

    public String getGstid() {
        return gstid;
    }

    public void setGstid(String gstid) {
        this.gstid = gstid;
    }

    public String getAmend() {
        return amend;
    }

    public void setAmend(String amend) {
        this.amend = amend;
    }

    public String getPreid() {
        return preid;
    }

    public void setPreid(String preid) {
        this.preid = preid;
    }

    public Date getPredate() {
        return predate;
    }

    public void setPredate(Date predate) {
        this.predate = predate;
    }

    public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        this.prename = prename;
    }

    public String getPredesign() {
        return predesign;
    }

    public void setPredesign(String predesign) {
        this.predesign = predesign;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public Date getAppdate() {
        return appdate;
    }

    public void setAppdate(Date appdate) {
        this.appdate = appdate;
    }

    public String getAppdesign() {
        return appdesign;
    }

    public void setAppdesign(String appdesign) {
        this.appdesign = appdesign;
    }

    public BigDecimal getC1() {
        return c1;
    }

    public void setC1(BigDecimal c1) {
        this.c1 = c1;
    }

    public BigDecimal getC2() {
        return c2;
    }

    public void setC2(BigDecimal c2) {
        this.c2 = c2;
    }

    public BigDecimal getC3() {
        return c3;
    }

    public void setC3(BigDecimal c3) {
        this.c3 = c3;
    }

    public BigDecimal getC4() {
        return c4;
    }

    public void setC4(BigDecimal c4) {
        this.c4 = c4;
    }

    public String getB5() {
        return b5;
    }

    public void setB5(String b5) {
        this.b5 = b5;
    }

    public BigDecimal getC6() {
        return c6;
    }

    public void setC6(BigDecimal c6) {
        this.c6 = c6;
    }

    public BigDecimal getC7() {
        return c7;
    }

    public void setC7(BigDecimal c7) {
        this.c7 = c7;
    }

    public BigDecimal getC8() {
        return c8;
    }

    public void setC8(BigDecimal c8) {
        this.c8 = c8;
    }

    public BigDecimal getC9() {
        return c9;
    }

    public void setC9(BigDecimal c9) {
        this.c9 = c9;
    }

    public BigDecimal getC10() {
        return c10;
    }

    public void setC10(BigDecimal c10) {
        this.c10 = c10;
    }

    public BigDecimal getC11() {
        return c11;
    }

    public void setC11(BigDecimal c11) {
        this.c11 = c11;
    }

    public BigDecimal getC12() {
        return c12;
    }

    public void setC12(BigDecimal c12) {
        this.c12 = c12;
    }

    public BigDecimal getC13() {
        return c13;
    }

    public void setC13(BigDecimal c13) {
        this.c13 = c13;
    }

    public BigDecimal getC15() {
        return c15;
    }

    public void setC15(BigDecimal c15) {
        this.c15 = c15;
    }

    public BigDecimal getC17() {
        return c17;
    }

    public void setC17(BigDecimal c17) {
        this.c17 = c17;
    }

    public BigDecimal getC19() {
        return c19;
    }

    public void setC19(BigDecimal c19) {
        this.c19 = c19;
    }

    public BigDecimal getC21() {
        return c21;
    }

    public void setC21(BigDecimal c21) {
        this.c21 = c21;
    }

    public BigDecimal getC23() {
        return c23;
    }

    public void setC23(BigDecimal c23) {
        this.c23 = c23;
    }

    public BigDecimal getC24() {
        return c24;
    }

    public void setC24(BigDecimal c24) {
        this.c24 = c24;
    }

    public String getI14() {
        return i14;
    }

    public void setI14(String i14) {
        this.i14 = i14;
    }

    public String getI16() {
        return i16;
    }

    public void setI16(String i16) {
        this.i16 = i16;
    }

    public String getI18() {
        return i18;
    }

    public void setI18(String i18) {
        this.i18 = i18;
    }

    public String getI20() {
        return i20;
    }

    public void setI20(String i20) {
        this.i20 = i20;
    }

    public String getI22() {
        return i22;
    }

    public void setI22(String i22) {
        this.i22 = i22;
    }

    public BigDecimal getPay() {
        return pay;
    }

    public void setPay(BigDecimal pay) {
        this.pay = pay;
    }

    public BigDecimal getClaim() {
        return claim;
    }

    public void setClaim(BigDecimal claim) {
        this.claim = claim;
    }

    public BigDecimal getPrcnt1() {
        return prcnt1;
    }

    public void setPrcnt1(BigDecimal prcnt1) {
        this.prcnt1 = prcnt1;
    }

    public BigDecimal getPrcnt2() {
        return prcnt2;
    }

    public void setPrcnt2(BigDecimal prcnt2) {
        this.prcnt2 = prcnt2;
    }

    public BigDecimal getPrcnt3() {
        return prcnt3;
    }

    public void setPrcnt3(BigDecimal prcnt3) {
        this.prcnt3 = prcnt3;
    }

    public BigDecimal getPrcnt4() {
        return prcnt4;
    }

    public void setPrcnt4(BigDecimal prcnt4) {
        this.prcnt4 = prcnt4;
    }

    public BigDecimal getPrcnt5() {
        return prcnt5;
    }

    public void setPrcnt5(BigDecimal prcnt5) {
        this.prcnt5 = prcnt5;
    }

    public BigDecimal getPrcnt6() {
        return prcnt6;
    }

    public void setPrcnt6(BigDecimal prcnt6) {
        this.prcnt6 = prcnt6;
    }

    public String getNewIC() {
        return newIC;
    }

    public void setNewIC(String newIC) {
        this.newIC = newIC;
    }

    public String getOldIC() {
        return oldIC;
    }

    public void setOldIC(String oldIC) {
        this.oldIC = oldIC;
    }

    public String getPassNo() {
        return passNo;
    }

    public void setPassNo(String passNo) {
        this.passNo = passNo;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getCoacode() {
        return coacode;
    }

    public void setCoacode(String coacode) {
        this.coacode = coacode;
    }

    public String getCoadescp() {
        return coadescp;
    }

    public void setCoadescp(String coadescp) {
        this.coadescp = coadescp;
    }

    public String getGenjvref() {
        return genjvref;
    }

    public void setGenjvref(String genjvref) {
        this.genjvref = genjvref;
    }

    public String getVoucherref() {
        return voucherref;
    }

    public void setVoucherref(String voucherref) {
        this.voucherref = voucherref;
    }

    public String getOrRef() {
        return orRef;
    }

    public void setOrRef(String orRef) {
        this.orRef = orRef;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TxGst03)) {
            return false;
        }
        TxGst03 other = (TxGst03) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.tx.TxGst03[ refer=" + refer + " ]";
    }
    
}
