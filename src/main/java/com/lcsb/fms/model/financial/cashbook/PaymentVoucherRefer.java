/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "cb_payvoucer_refer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PaymentVoucherRefer.findAll", query = "SELECT p FROM PaymentVoucherRefer p"),
    @NamedQuery(name = "PaymentVoucherRefer.findByVoucer", query = "SELECT p FROM PaymentVoucherRefer p WHERE p.voucer = :voucer"),
    @NamedQuery(name = "PaymentVoucherRefer.findByType", query = "SELECT p FROM PaymentVoucherRefer p WHERE p.type = :type"),
    @NamedQuery(name = "PaymentVoucherRefer.findByNo", query = "SELECT p FROM PaymentVoucherRefer p WHERE p.no = :no"),
    @NamedQuery(name = "PaymentVoucherRefer.findByTarikh", query = "SELECT p FROM PaymentVoucherRefer p WHERE p.tarikh = :tarikh"),
    @NamedQuery(name = "PaymentVoucherRefer.findByAmount", query = "SELECT p FROM PaymentVoucherRefer p WHERE p.amount = :amount"),
    @NamedQuery(name = "PaymentVoucherRefer.findByPaid", query = "SELECT p FROM PaymentVoucherRefer p WHERE p.paid = :paid"),
    @NamedQuery(name = "PaymentVoucherRefer.findByBf", query = "SELECT p FROM PaymentVoucherRefer p WHERE p.bf = :bf"),
    @NamedQuery(name = "PaymentVoucherRefer.findByPayment", query = "SELECT p FROM PaymentVoucherRefer p WHERE p.payment = :payment"),
    @NamedQuery(name = "PaymentVoucherRefer.findByCf", query = "SELECT p FROM PaymentVoucherRefer p WHERE p.cf = :cf"),
    @NamedQuery(name = "PaymentVoucherRefer.findByRefer", query = "SELECT p FROM PaymentVoucherRefer p WHERE p.refer = :refer")})
public class PaymentVoucherRefer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "voucer")
    private String voucer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "no")
    private String no;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "tarikh")
    private String tarikh;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paid")
    private String paid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bf")
    private String bf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "payment")
    private String payment;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cf")
    private String cf;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;

    public PaymentVoucherRefer() {
    }

    public PaymentVoucherRefer(String refer) {
        this.refer = refer;
    }

    public PaymentVoucherRefer(String refer, String voucer, String type, String no, String tarikh, String remarks, double amount, String paid, String bf, String payment, String cf) {
        this.refer = refer;
        this.voucer = voucer;
        this.type = type;
        this.no = no;
        this.tarikh = tarikh;
        this.remarks = remarks;
        this.amount = amount;
        this.paid = paid;
        this.bf = bf;
        this.payment = payment;
        this.cf = cf;
    }

    public String getVoucer() {
        return voucer;
    }

    public void setVoucer(String voucer) {
        this.voucer = voucer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getTarikh() {
        return tarikh;
    }

    public void setTarikh(String tarikh) {
        this.tarikh = tarikh;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getBf() {
        return bf;
    }

    public void setBf(String bf) {
        this.bf = bf;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getCf() {
        return cf;
    }

    public void setCf(String cf) {
        this.cf = cf;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentVoucherRefer)) {
            return false;
        }
        PaymentVoucherRefer other = (PaymentVoucherRefer) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.cashbook.PaymentVoucherRefer[ refer=" + refer + " ]";
    }
    
}
