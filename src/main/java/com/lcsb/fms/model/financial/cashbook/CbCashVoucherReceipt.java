/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "cb_receipt")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbCashVoucherReceipt.findAll", query = "SELECT c FROM CbCashVoucherReceipt c")
    , @NamedQuery(name = "CbCashVoucherReceipt.findByNovoucer", query = "SELECT c FROM CbCashVoucherReceipt c WHERE c.novoucer = :novoucer")
    , @NamedQuery(name = "CbCashVoucherReceipt.findByNoreceipt", query = "SELECT c FROM CbCashVoucherReceipt c WHERE c.cbCashVoucherReceiptPK.noreceipt = :noreceipt")
    , @NamedQuery(name = "CbCashVoucherReceipt.findByTarikh", query = "SELECT c FROM CbCashVoucherReceipt c WHERE c.tarikh = :tarikh")
    , @NamedQuery(name = "CbCashVoucherReceipt.findByRemarks", query = "SELECT c FROM CbCashVoucherReceipt c WHERE c.remarks = :remarks")
    , @NamedQuery(name = "CbCashVoucherReceipt.findByTotal", query = "SELECT c FROM CbCashVoucherReceipt c WHERE c.total = :total")
    , @NamedQuery(name = "CbCashVoucherReceipt.findByTrade", query = "SELECT c FROM CbCashVoucherReceipt c WHERE c.trade = :trade")
    , @NamedQuery(name = "CbCashVoucherReceipt.findByRefer", query = "SELECT c FROM CbCashVoucherReceipt c WHERE c.cbCashVoucherReceiptPK.refer = :refer")
    , @NamedQuery(name = "CbCashVoucherReceipt.findByGstid", query = "SELECT c FROM CbCashVoucherReceipt c WHERE c.gstid = :gstid")})
public class CbCashVoucherReceipt implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CbCashVoucherReceiptPK cbCashVoucherReceiptPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "novoucer")
    private String novoucer;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tarikh")
    @Temporal(TemporalType.DATE)
    private Date tarikh;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Column(name = "total")
    private double total;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "trade")
    private String trade;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "gstid")
    private String gstid;

    public CbCashVoucherReceipt() {
    }

    public CbCashVoucherReceipt(CbCashVoucherReceiptPK cbCashVoucherReceiptPK) {
        this.cbCashVoucherReceiptPK = cbCashVoucherReceiptPK;
    }

    public CbCashVoucherReceipt(CbCashVoucherReceiptPK cbCashVoucherReceiptPK, String novoucer, Date tarikh, String remarks, double total, String trade, String gstid) {
        this.cbCashVoucherReceiptPK = cbCashVoucherReceiptPK;
        this.novoucer = novoucer;
        this.tarikh = tarikh;
        this.remarks = remarks;
        this.total = total;
        this.trade = trade;
        this.gstid = gstid;
    }

    public CbCashVoucherReceipt(String noreceipt, String refer) {
        this.cbCashVoucherReceiptPK = new CbCashVoucherReceiptPK(noreceipt, refer);
    }

    public CbCashVoucherReceiptPK getCbCashVoucherReceiptPK() {
        return cbCashVoucherReceiptPK;
    }

    public void setCbCashVoucherReceiptPK(CbCashVoucherReceiptPK cbCashVoucherReceiptPK) {
        this.cbCashVoucherReceiptPK = cbCashVoucherReceiptPK;
    }

    public String getNovoucer() {
        return novoucer;
    }

    public void setNovoucer(String novoucer) {
        this.novoucer = novoucer;
    }

    public Date getTarikh() {
        return tarikh;
    }

    public void setTarikh(Date tarikh) {
        this.tarikh = tarikh;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getTrade() {
        return trade;
    }

    public void setTrade(String trade) {
        this.trade = trade;
    }

    public String getGstid() {
        return gstid;
    }

    public void setGstid(String gstid) {
        this.gstid = gstid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cbCashVoucherReceiptPK != null ? cbCashVoucherReceiptPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbCashVoucherReceipt)) {
            return false;
        }
        CbCashVoucherReceipt other = (CbCashVoucherReceipt) object;
        if ((this.cbCashVoucherReceiptPK == null && other.cbCashVoucherReceiptPK != null) || (this.cbCashVoucherReceiptPK != null && !this.cbCashVoucherReceiptPK.equals(other.cbCashVoucherReceiptPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.cashbook.CbCashVoucherReceipt[ cbCashVoucherReceiptPK=" + cbCashVoucherReceiptPK + " ]";
    }
    
}
