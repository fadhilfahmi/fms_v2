/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.employee;

/**
 *
 * @author fadhilfahmi
 */
public class EmPayrollPCB {
    
    private double P;
    private double M;
    private double R;
    private double B;
    private double Z;
    private double X;
    private double n;
    private double curMonthZakat;

    public double getP() {
        return P;
    }

    public void setP(double P) {
        this.P = P;
    }

   

    public double getM() {
        return M;
    }

    public void setM(double M) {
        this.M = M;
    }

    public double getR() {
        return R;
    }

    public void setR(double R) {
        this.R = R;
    }

    public double getB() {
        return B;
    }

    public void setB(double B) {
        this.B = B;
    }

    public double getZ() {
        return Z;
    }

    public void setZ(double Z) {
        this.Z = Z;
    }

    public double getX() {
        return X;
    }

    public void setX(double X) {
        this.X = X;
    }

    public double getN() {
        return n;
    }

    public void setN(double n) {
        this.n = n;
    }

    public double getCurMonthZakat() {
        return curMonthZakat;
    }

    public void setCurMonthZakat(double curMonthZakat) {
        this.curMonthZakat = curMonthZakat;
    }
    
    
    
}
