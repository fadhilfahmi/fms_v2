/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ar;

/**
 *
 * @author fadhilfahmi
 */
public class ArReportAging {
    
    private String buyerCode;
    private String buyerName;
    private double dayCount1;
    private double dayCount2;
    private double dayCount3;
    private double dayCount4;
    private double dayCount5;
    private double total;

    public String getBuyerCode() {
        return buyerCode;
    }

    public void setBuyerCode(String buyerCode) {
        this.buyerCode = buyerCode;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getDayCount1() {
        return dayCount1;
    }

    public void setDayCount1(double dayCount1) {
        this.dayCount1 = dayCount1;
    }

    public double getDayCount2() {
        return dayCount2;
    }

    public void setDayCount2(double dayCount2) {
        this.dayCount2 = dayCount2;
    }

    public double getDayCount3() {
        return dayCount3;
    }

    public void setDayCount3(double dayCount3) {
        this.dayCount3 = dayCount3;
    }

    public double getDayCount4() {
        return dayCount4;
    }

    public void setDayCount4(double dayCount4) {
        this.dayCount4 = dayCount4;
    }

    public double getDayCount5() {
        return dayCount5;
    }

    public void setDayCount5(double dayCount5) {
        this.dayCount5 = dayCount5;
    }
    
    
}
