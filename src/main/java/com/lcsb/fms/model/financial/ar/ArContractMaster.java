/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ar;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class ArContractMaster {
    
    private List<ArContractInfo> listMaster;

    public List<ArContractInfo> getListMaster() {
        return listMaster;
    }

    public void setListMaster(List<ArContractInfo> listMaster) {
        this.listMaster = listMaster;
    }
    
    
}
