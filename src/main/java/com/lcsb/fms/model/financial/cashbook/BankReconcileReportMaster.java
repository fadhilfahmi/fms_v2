/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class BankReconcileReportMaster {
    
    private boolean doReconcile;
    private BankReconcile bankReconcile;
    private String date;
    private List<ItemMain> listItemMain;

    public BankReconcile getBankReconcile() {
        return bankReconcile;
    }

    public void setBankReconcile(BankReconcile bankReconcile) {
        this.bankReconcile = bankReconcile;
    }

    public boolean isDoReconcile() {
        return doReconcile;
    }

    public void setDoReconcile(boolean doReconcile) {
        this.doReconcile = doReconcile;
    }
    
 
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public List<ItemMain> getListItemMain() {
        return listItemMain;
    }

    public void setListItemMain(List<ItemMain> listItemMain) {
        this.listItemMain = listItemMain;
    }

    
    
}
