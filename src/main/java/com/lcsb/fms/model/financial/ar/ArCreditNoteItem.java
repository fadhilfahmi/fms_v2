/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ar;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ar_creditnote_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ArCreditNoteItem.findAll", query = "SELECT a FROM ArCreditNoteItem a"),
    @NamedQuery(name = "ArCreditNoteItem.findByNoteno", query = "SELECT a FROM ArCreditNoteItem a WHERE a.noteno = :noteno"),
    @NamedQuery(name = "ArCreditNoteItem.findByRefer", query = "SELECT a FROM ArCreditNoteItem a WHERE a.refer = :refer"),
    @NamedQuery(name = "ArCreditNoteItem.findByProdcode", query = "SELECT a FROM ArCreditNoteItem a WHERE a.prodcode = :prodcode"),
    @NamedQuery(name = "ArCreditNoteItem.findByProdname", query = "SELECT a FROM ArCreditNoteItem a WHERE a.prodname = :prodname"),
    @NamedQuery(name = "ArCreditNoteItem.findByCoacode", query = "SELECT a FROM ArCreditNoteItem a WHERE a.coacode = :coacode"),
    @NamedQuery(name = "ArCreditNoteItem.findByCoaname", query = "SELECT a FROM ArCreditNoteItem a WHERE a.coaname = :coaname"),
    @NamedQuery(name = "ArCreditNoteItem.findByQty", query = "SELECT a FROM ArCreditNoteItem a WHERE a.qty = :qty"),
    @NamedQuery(name = "ArCreditNoteItem.findByUnitp", query = "SELECT a FROM ArCreditNoteItem a WHERE a.unitp = :unitp"),
    @NamedQuery(name = "ArCreditNoteItem.findByAmount", query = "SELECT a FROM ArCreditNoteItem a WHERE a.amount = :amount"),
    @NamedQuery(name = "ArCreditNoteItem.findByUnitm", query = "SELECT a FROM ArCreditNoteItem a WHERE a.unitm = :unitm"),
    @NamedQuery(name = "ArCreditNoteItem.findByLoclevel", query = "SELECT a FROM ArCreditNoteItem a WHERE a.loclevel = :loclevel"),
    @NamedQuery(name = "ArCreditNoteItem.findByLoccode", query = "SELECT a FROM ArCreditNoteItem a WHERE a.loccode = :loccode"),
    @NamedQuery(name = "ArCreditNoteItem.findByLocdesc", query = "SELECT a FROM ArCreditNoteItem a WHERE a.locdesc = :locdesc"),
    @NamedQuery(name = "ArCreditNoteItem.findByCttype", query = "SELECT a FROM ArCreditNoteItem a WHERE a.cttype = :cttype"),
    @NamedQuery(name = "ArCreditNoteItem.findByCtcode", query = "SELECT a FROM ArCreditNoteItem a WHERE a.ctcode = :ctcode"),
    @NamedQuery(name = "ArCreditNoteItem.findByCtdesc", query = "SELECT a FROM ArCreditNoteItem a WHERE a.ctdesc = :ctdesc"),
    @NamedQuery(name = "ArCreditNoteItem.findByTaxcode", query = "SELECT a FROM ArCreditNoteItem a WHERE a.taxcode = :taxcode"),
    @NamedQuery(name = "ArCreditNoteItem.findByTaxrate", query = "SELECT a FROM ArCreditNoteItem a WHERE a.taxrate = :taxrate"),
    @NamedQuery(name = "ArCreditNoteItem.findByTaxamt", query = "SELECT a FROM ArCreditNoteItem a WHERE a.taxamt = :taxamt"),
    @NamedQuery(name = "ArCreditNoteItem.findByTaxcoacode", query = "SELECT a FROM ArCreditNoteItem a WHERE a.taxcoacode = :taxcoacode"),
    @NamedQuery(name = "ArCreditNoteItem.findByTaxcoadescp", query = "SELECT a FROM ArCreditNoteItem a WHERE a.taxcoadescp = :taxcoadescp"),
    @NamedQuery(name = "ArCreditNoteItem.findBySatype", query = "SELECT a FROM ArCreditNoteItem a WHERE a.satype = :satype"),
    @NamedQuery(name = "ArCreditNoteItem.findBySacode", query = "SELECT a FROM ArCreditNoteItem a WHERE a.sacode = :sacode"),
    @NamedQuery(name = "ArCreditNoteItem.findBySadesc", query = "SELECT a FROM ArCreditNoteItem a WHERE a.sadesc = :sadesc")})
public class ArCreditNoteItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "noteno")
    private String noteno;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prodcode")
    private String prodcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prodname")
    private String prodname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coacode")
    private String coacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coaname")
    private String coaname;
    @Basic(optional = false)
    @NotNull
    @Column(name = "qty")
    private double qty;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "unitp")
    private String unitp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "unitm")
    private String unitm;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loclevel")
    private String loclevel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loccode")
    private String loccode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "locdesc")
    private String locdesc;
    @Size(max = 100)
    @Column(name = "cttype")
    private String cttype;
    @Size(max = 100)
    @Column(name = "ctcode")
    private String ctcode;
    @Size(max = 100)
    @Column(name = "ctdesc")
    private String ctdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcode")
    private String taxcode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxrate")
    private double taxrate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxamt")
    private double taxamt;
    @Size(max = 100)
    @Column(name = "taxcoacode")
    private String taxcoacode;
    @Size(max = 100)
    @Column(name = "taxcoadescp")
    private String taxcoadescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "satype")
    private String satype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "sacode")
    private String sacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "sadesc")
    private String sadesc;

    public ArCreditNoteItem() {
    }

    public ArCreditNoteItem(String refer) {
        this.refer = refer;
    }

    public ArCreditNoteItem(String refer, String noteno, String prodcode, String prodname, String coacode, String coaname, double qty, String unitp, double amount, String unitm, String remarks, String loclevel, String loccode, String locdesc, String taxcode, double taxrate, double taxamt, String satype, String sacode, String sadesc) {
        this.refer = refer;
        this.noteno = noteno;
        this.prodcode = prodcode;
        this.prodname = prodname;
        this.coacode = coacode;
        this.coaname = coaname;
        this.qty = qty;
        this.unitp = unitp;
        this.amount = amount;
        this.unitm = unitm;
        this.remarks = remarks;
        this.loclevel = loclevel;
        this.loccode = loccode;
        this.locdesc = locdesc;
        this.taxcode = taxcode;
        this.taxrate = taxrate;
        this.taxamt = taxamt;
        this.satype = satype;
        this.sacode = sacode;
        this.sadesc = sadesc;
    }

    public String getNoteno() {
        return noteno;
    }

    public void setNoteno(String noteno) {
        this.noteno = noteno;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getProdcode() {
        return prodcode;
    }

    public void setProdcode(String prodcode) {
        this.prodcode = prodcode;
    }

    public String getProdname() {
        return prodname;
    }

    public void setProdname(String prodname) {
        this.prodname = prodname;
    }

    public String getCoacode() {
        return coacode;
    }

    public void setCoacode(String coacode) {
        this.coacode = coacode;
    }

    public String getCoaname() {
        return coaname;
    }

    public void setCoaname(String coaname) {
        this.coaname = coaname;
    }

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }

    public String getUnitp() {
        return unitp;
    }

    public void setUnitp(String unitp) {
        this.unitp = unitp;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getUnitm() {
        return unitm;
    }

    public void setUnitm(String unitm) {
        this.unitm = unitm;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getLoclevel() {
        return loclevel;
    }

    public void setLoclevel(String loclevel) {
        this.loclevel = loclevel;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLocdesc() {
        return locdesc;
    }

    public void setLocdesc(String locdesc) {
        this.locdesc = locdesc;
    }

    public String getCttype() {
        return cttype;
    }

    public void setCttype(String cttype) {
        this.cttype = cttype;
    }

    public String getCtcode() {
        return ctcode;
    }

    public void setCtcode(String ctcode) {
        this.ctcode = ctcode;
    }

    public String getCtdesc() {
        return ctdesc;
    }

    public void setCtdesc(String ctdesc) {
        this.ctdesc = ctdesc;
    }

    public String getTaxcode() {
        return taxcode;
    }

    public void setTaxcode(String taxcode) {
        this.taxcode = taxcode;
    }

    public double getTaxrate() {
        return taxrate;
    }

    public void setTaxrate(double taxrate) {
        this.taxrate = taxrate;
    }

    public double getTaxamt() {
        return taxamt;
    }

    public void setTaxamt(double taxamt) {
        this.taxamt = taxamt;
    }

    public String getTaxcoacode() {
        return taxcoacode;
    }

    public void setTaxcoacode(String taxcoacode) {
        this.taxcoacode = taxcoacode;
    }

    public String getTaxcoadescp() {
        return taxcoadescp;
    }

    public void setTaxcoadescp(String taxcoadescp) {
        this.taxcoadescp = taxcoadescp;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArCreditNoteItem)) {
            return false;
        }
        ArCreditNoteItem other = (ArCreditNoteItem) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.ar.ArCreditNoteItem[ refer=" + refer + " ]";
    }
    
}
