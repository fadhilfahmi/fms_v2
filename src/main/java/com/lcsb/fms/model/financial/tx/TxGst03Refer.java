/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.tx;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "tx_gst03_refer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TxGst03Refer.findAll", query = "SELECT t FROM TxGst03Refer t"),
    @NamedQuery(name = "TxGst03Refer.findByRefer", query = "SELECT t FROM TxGst03Refer t WHERE t.refer = :refer"),
    @NamedQuery(name = "TxGst03Refer.findByReconrefer", query = "SELECT t FROM TxGst03Refer t WHERE t.reconrefer = :reconrefer"),
    @NamedQuery(name = "TxGst03Refer.findByTaxdebit", query = "SELECT t FROM TxGst03Refer t WHERE t.taxdebit = :taxdebit"),
    @NamedQuery(name = "TxGst03Refer.findByTaxcredit", query = "SELECT t FROM TxGst03Refer t WHERE t.taxcredit = :taxcredit"),
    @NamedQuery(name = "TxGst03Refer.findByTaxstart", query = "SELECT t FROM TxGst03Refer t WHERE t.taxstart = :taxstart"),
    @NamedQuery(name = "TxGst03Refer.findByTaxend", query = "SELECT t FROM TxGst03Refer t WHERE t.taxend = :taxend"),
    @NamedQuery(name = "TxGst03Refer.findByTaxyear", query = "SELECT t FROM TxGst03Refer t WHERE t.taxyear = :taxyear"),
    @NamedQuery(name = "TxGst03Refer.findByTaxperiod", query = "SELECT t FROM TxGst03Refer t WHERE t.taxperiod = :taxperiod")})
public class TxGst03Refer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "reconrefer")
    private String reconrefer;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxdebit")
    private double taxdebit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxcredit")
    private double taxcredit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxstart")
    @Temporal(TemporalType.DATE)
    private Date taxstart;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxend")
    @Temporal(TemporalType.DATE)
    private Date taxend;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "taxyear")
    private String taxyear;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "taxperiod")
    private String taxperiod;

    public TxGst03Refer() {
    }

    public TxGst03Refer(String refer) {
        this.refer = refer;
    }

    public TxGst03Refer(String refer, String reconrefer, double taxdebit, double taxcredit, Date taxstart, Date taxend, String taxyear, String taxperiod) {
        this.refer = refer;
        this.reconrefer = reconrefer;
        this.taxdebit = taxdebit;
        this.taxcredit = taxcredit;
        this.taxstart = taxstart;
        this.taxend = taxend;
        this.taxyear = taxyear;
        this.taxperiod = taxperiod;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getReconrefer() {
        return reconrefer;
    }

    public void setReconrefer(String reconrefer) {
        this.reconrefer = reconrefer;
    }

    public double getTaxdebit() {
        return taxdebit;
    }

    public void setTaxdebit(double taxdebit) {
        this.taxdebit = taxdebit;
    }

    public double getTaxcredit() {
        return taxcredit;
    }

    public void setTaxcredit(double taxcredit) {
        this.taxcredit = taxcredit;
    }

    public Date getTaxstart() {
        return taxstart;
    }

    public void setTaxstart(Date taxstart) {
        this.taxstart = taxstart;
    }

    public Date getTaxend() {
        return taxend;
    }

    public void setTaxend(Date taxend) {
        this.taxend = taxend;
    }

    public String getTaxyear() {
        return taxyear;
    }

    public void setTaxyear(String taxyear) {
        this.taxyear = taxyear;
    }

    public String getTaxperiod() {
        return taxperiod;
    }

    public void setTaxperiod(String taxperiod) {
        this.taxperiod = taxperiod;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TxGst03Refer)) {
            return false;
        }
        TxGst03Refer other = (TxGst03Refer) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.tx.TxGst03Refer[ refer=" + refer + " ]";
    }
    
}
