/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "cb_official_cek")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CbOfficialCek.findAll", query = "SELECT c FROM CbOfficialCek c"),
    @NamedQuery(name = "CbOfficialCek.findByVoucherno", query = "SELECT c FROM CbOfficialCek c WHERE c.voucherno = :voucherno"),
    @NamedQuery(name = "CbOfficialCek.findByCekno", query = "SELECT c FROM CbOfficialCek c WHERE c.cekno = :cekno"),
    @NamedQuery(name = "CbOfficialCek.findByDate", query = "SELECT c FROM CbOfficialCek c WHERE c.date = :date"),
    @NamedQuery(name = "CbOfficialCek.findByBank", query = "SELECT c FROM CbOfficialCek c WHERE c.bank = :bank"),
    @NamedQuery(name = "CbOfficialCek.findByBranch", query = "SELECT c FROM CbOfficialCek c WHERE c.branch = :branch"),
    @NamedQuery(name = "CbOfficialCek.findByAmount", query = "SELECT c FROM CbOfficialCek c WHERE c.amount = :amount"),
    @NamedQuery(name = "CbOfficialCek.findByRefer", query = "SELECT c FROM CbOfficialCek c WHERE c.refer = :refer")})
public class CbOfficialCek implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "voucherno")
    private String voucherno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cekno")
    private String cekno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "date")
    private String date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bank")
    private String bank;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "branch")
    private String branch;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;

    public CbOfficialCek() {
    }

    public CbOfficialCek(String refer) {
        this.refer = refer;
    }

    public CbOfficialCek(String refer, String voucherno, String cekno, String date, String bank, String branch, double amount) {
        this.refer = refer;
        this.voucherno = voucherno;
        this.cekno = cekno;
        this.date = date;
        this.bank = bank;
        this.branch = branch;
        this.amount = amount;
    }

    public String getVoucherno() {
        return voucherno;
    }

    public void setVoucherno(String voucherno) {
        this.voucherno = voucherno;
    }

    public String getCekno() {
        return cekno;
    }

    public void setCekno(String cekno) {
        this.cekno = cekno;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbOfficialCek)) {
            return false;
        }
        CbOfficialCek other = (CbOfficialCek) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.cashbook.CbOfficialCek[ refer=" + refer + " ]";
    }
    
}
