/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

/**
 *
 * @author fadhilfahmi
 */
public class ORPayer {
    
    private String payercode;
    private String payername;
    private int totalInvoice;

    public String getPayercode() {
        return payercode;
    }

    public void setPayercode(String payercode) {
        this.payercode = payercode;
    }

    public String getPayername() {
        return payername;
    }

    public void setPayername(String payername) {
        this.payername = payername;
    }

    public int getTotalInvoice() {
        return totalInvoice;
    }

    public void setTotalInvoice(int totalInvoice) {
        this.totalInvoice = totalInvoice;
    }
    
    
    
}
