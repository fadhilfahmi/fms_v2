/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.fms.model.financial.cashbook;

/**
 *
 * @author Dell
 */
public class BankBook {
    
    private static String bankcoa;
    private static String startPeriod;
    private static String endPeriod;
    private static String currentYear;
    private static String currentPeriod;
    private static String staffid;
    private static String name;
    private static String pposition;
    
    private String pvdate;
    private String pvid;
    private String pvremarks;
    private String listcoa;
    private double pvamount;
    private String listcek;
    private String status;
    private String sacode;
    

    public static String getBankcoa() {
        return bankcoa;
    }

    public static void setBankcoa(String bankcoa) {
        BankBook.bankcoa = bankcoa;
    }

    public static String getStartPeriod() {
        return startPeriod;
    }

    public static void setStartPeriod(String startPeriod) {
        BankBook.startPeriod = startPeriod;
    }

    public static String getEndPeriod() {
        return endPeriod;
    }

    public static void setEndPeriod(String endPeriod) {
        BankBook.endPeriod = endPeriod;
    }

    public static String getCurrentYear() {
        return currentYear;
    }

    public static void setCurrentYear(String currentYear) {
        BankBook.currentYear = currentYear;
    }

    public static String getCurrentPeriod() {
        return currentPeriod;
    }

    public static void setCurrentPeriod(String currentPeriod) {
        BankBook.currentPeriod = currentPeriod;
    }

    public static String getStaffid() {
        return staffid;
    }

    public static void setStaffid(String staffid) {
        BankBook.staffid = staffid;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        BankBook.name = name;
    }

    public static String getPposition() {
        return pposition;
    }

    public static void setPposition(String pposition) {
        BankBook.pposition = pposition;
    }

    public String getPvdate() {
        return pvdate;
    }

    public void setPvdate(String pvdate) {
        this.pvdate = pvdate;
    }

    public String getPvid() {
        return pvid;
    }

    public void setPvid(String pvid) {
        this.pvid = pvid;
    }

    public String getPvremarks() {
        return pvremarks;
    }

    public void setPvremarks(String pvremarks) {
        this.pvremarks = pvremarks;
    }

    public String getListcoa() {
        return listcoa;
    }

    public void setListcoa(String listcoa) {
        this.listcoa = listcoa;
    }

    public double getPvamount() {
        return pvamount;
    }

    public void setPvamount(double pvamount) {
        this.pvamount = pvamount;
    }

   
    public String getListcek() {
        return listcek;
    }

    public void setListcek(String listcek) {
        this.listcek = listcek;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    
    
}
