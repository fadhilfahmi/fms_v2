/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ap;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ap_inv")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VendorInvoice.findAll", query = "SELECT v FROM VendorInvoice v"),
    @NamedQuery(name = "VendorInvoice.findByInvno", query = "SELECT v FROM VendorInvoice v WHERE v.invno = :invno"),
    @NamedQuery(name = "VendorInvoice.findByNogrn", query = "SELECT v FROM VendorInvoice v WHERE v.nogrn = :nogrn"),
    @NamedQuery(name = "VendorInvoice.findByNoinv", query = "SELECT v FROM VendorInvoice v WHERE v.noinv = :noinv"),
    @NamedQuery(name = "VendorInvoice.findByInvtype", query = "SELECT v FROM VendorInvoice v WHERE v.invtype = :invtype"),
    @NamedQuery(name = "VendorInvoice.findByInvrefno", query = "SELECT v FROM VendorInvoice v WHERE v.invrefno = :invrefno"),
    @NamedQuery(name = "VendorInvoice.findByPorefno", query = "SELECT v FROM VendorInvoice v WHERE v.porefno = :porefno"),
    @NamedQuery(name = "VendorInvoice.findByAppid", query = "SELECT v FROM VendorInvoice v WHERE v.appid = :appid"),
    @NamedQuery(name = "VendorInvoice.findByAppname", query = "SELECT v FROM VendorInvoice v WHERE v.appname = :appname"),
    @NamedQuery(name = "VendorInvoice.findByAppdesig", query = "SELECT v FROM VendorInvoice v WHERE v.appdesig = :appdesig"),
    @NamedQuery(name = "VendorInvoice.findByPreid", query = "SELECT v FROM VendorInvoice v WHERE v.preid = :preid"),
    @NamedQuery(name = "VendorInvoice.findByPrename", query = "SELECT v FROM VendorInvoice v WHERE v.prename = :prename"),
    @NamedQuery(name = "VendorInvoice.findByPredate", query = "SELECT v FROM VendorInvoice v WHERE v.predate = :predate"),
    @NamedQuery(name = "VendorInvoice.findByAppdate", query = "SELECT v FROM VendorInvoice v WHERE v.appdate = :appdate"),
    @NamedQuery(name = "VendorInvoice.findByCheckid", query = "SELECT v FROM VendorInvoice v WHERE v.checkid = :checkid"),
    @NamedQuery(name = "VendorInvoice.findByCheckname", query = "SELECT v FROM VendorInvoice v WHERE v.checkname = :checkname"),
    @NamedQuery(name = "VendorInvoice.findByCheckdesig", query = "SELECT v FROM VendorInvoice v WHERE v.checkdesig = :checkdesig"),
    @NamedQuery(name = "VendorInvoice.findByCheckdate", query = "SELECT v FROM VendorInvoice v WHERE v.checkdate = :checkdate"),
    @NamedQuery(name = "VendorInvoice.findByPostdate", query = "SELECT v FROM VendorInvoice v WHERE v.postdate = :postdate"),
    @NamedQuery(name = "VendorInvoice.findByPostflag", query = "SELECT v FROM VendorInvoice v WHERE v.postflag = :postflag"),
    @NamedQuery(name = "VendorInvoice.findByEstatecode", query = "SELECT v FROM VendorInvoice v WHERE v.estatecode = :estatecode"),
    @NamedQuery(name = "VendorInvoice.findByEstatename", query = "SELECT v FROM VendorInvoice v WHERE v.estatename = :estatename"),
    @NamedQuery(name = "VendorInvoice.findByYear", query = "SELECT v FROM VendorInvoice v WHERE v.year = :year"),
    @NamedQuery(name = "VendorInvoice.findByPeriod", query = "SELECT v FROM VendorInvoice v WHERE v.period = :period"),
    @NamedQuery(name = "VendorInvoice.findByDate", query = "SELECT v FROM VendorInvoice v WHERE v.date = :date"),
    @NamedQuery(name = "VendorInvoice.findBySuppcode", query = "SELECT v FROM VendorInvoice v WHERE v.suppcode = :suppcode"),
    @NamedQuery(name = "VendorInvoice.findBySuppname", query = "SELECT v FROM VendorInvoice v WHERE v.suppname = :suppname"),
    @NamedQuery(name = "VendorInvoice.findBySuppaddress", query = "SELECT v FROM VendorInvoice v WHERE v.suppaddress = :suppaddress"),
    @NamedQuery(name = "VendorInvoice.findByAccode", query = "SELECT v FROM VendorInvoice v WHERE v.accode = :accode"),
    @NamedQuery(name = "VendorInvoice.findByRemark", query = "SELECT v FROM VendorInvoice v WHERE v.remark = :remark"),
    @NamedQuery(name = "VendorInvoice.findByTotalamount", query = "SELECT v FROM VendorInvoice v WHERE v.totalamount = :totalamount"),
    @NamedQuery(name = "VendorInvoice.findByPaid", query = "SELECT v FROM VendorInvoice v WHERE v.paid = :paid"),
    @NamedQuery(name = "VendorInvoice.findByAcdesc", query = "SELECT v FROM VendorInvoice v WHERE v.acdesc = :acdesc"),
    @NamedQuery(name = "VendorInvoice.findBySatype", query = "SELECT v FROM VendorInvoice v WHERE v.satype = :satype"),
    @NamedQuery(name = "VendorInvoice.findBySacode", query = "SELECT v FROM VendorInvoice v WHERE v.sacode = :sacode"),
    @NamedQuery(name = "VendorInvoice.findBySadesc", query = "SELECT v FROM VendorInvoice v WHERE v.sadesc = :sadesc"),
    @NamedQuery(name = "VendorInvoice.findByGstid", query = "SELECT v FROM VendorInvoice v WHERE v.gstid = :gstid"),
    @NamedQuery(name = "VendorInvoice.findByRacoacode", query = "SELECT v FROM VendorInvoice v WHERE v.racoacode = :racoacode"),
    @NamedQuery(name = "VendorInvoice.findByRacoadesc", query = "SELECT v FROM VendorInvoice v WHERE v.racoadesc = :racoadesc")})
public class VendorInvoice implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "invno")
    private String invno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nogrn")
    private String nogrn;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "noinv")
    private String noinv;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "invtype")
    private String invtype;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "invrefno")
    private String invrefno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "porefno")
    private String porefno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appid")
    private String appid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appname")
    private String appname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appdesig")
    private String appdesig;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "preid")
    private String preid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prename")
    private String prename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "predate")
    private String predate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appdate")
    private String appdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkid")
    private String checkid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkname")
    private String checkname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkdesig")
    private String checkdesig;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkdate")
    private String checkdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postdate")
    private String postdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postflag")
    private String postflag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatecode")
    private String estatecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatename")
    private String estatename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "period")
    private String period;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "date")
    private String date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "suppcode")
    private String suppcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "suppname")
    private String suppname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "suppaddress")
    private String suppaddress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "accode")
    private String accode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "remark")
    private String remark;
    @Basic(optional = false)
    @NotNull
    @Column(name = "totalamount")
    private double totalamount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "paid")
    private double paid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "acdesc")
    private String acdesc;
    @Size(max = 100)
    @Column(name = "satype")
    private String satype;
    @Size(max = 10)
    @Column(name = "sacode")
    private String sacode;
    @Size(max = 100)
    @Column(name = "sadesc")
    private String sadesc;
    @Size(max = 100)
    @Column(name = "gstid")
    private String gstid;
    @Size(max = 100)
    @Column(name = "racoacode")
    private String racoacode;
    @Size(max = 100)
    @Column(name = "racoadesc")
    private String racoadesc;

    public VendorInvoice() {
    }

    public VendorInvoice(String invrefno) {
        this.invrefno = invrefno;
    }

    public VendorInvoice(String invrefno, String invno, String nogrn, String noinv, String invtype, String porefno, String appid, String appname, String appdesig, String preid, String prename, String predate, String appdate, String checkid, String checkname, String checkdesig, String checkdate, String postdate, String postflag, String estatecode, String estatename, String year, String period, String date, String suppcode, String suppname, String suppaddress, String accode, String remark, double totalamount, double paid, String acdesc) {
        this.invrefno = invrefno;
        this.invno = invno;
        this.nogrn = nogrn;
        this.noinv = noinv;
        this.invtype = invtype;
        this.porefno = porefno;
        this.appid = appid;
        this.appname = appname;
        this.appdesig = appdesig;
        this.preid = preid;
        this.prename = prename;
        this.predate = predate;
        this.appdate = appdate;
        this.checkid = checkid;
        this.checkname = checkname;
        this.checkdesig = checkdesig;
        this.checkdate = checkdate;
        this.postdate = postdate;
        this.postflag = postflag;
        this.estatecode = estatecode;
        this.estatename = estatename;
        this.year = year;
        this.period = period;
        this.date = date;
        this.suppcode = suppcode;
        this.suppname = suppname;
        this.suppaddress = suppaddress;
        this.accode = accode;
        this.remark = remark;
        this.totalamount = totalamount;
        this.paid = paid;
        this.acdesc = acdesc;
    }

    public String getInvno() {
        return invno;
    }

    public void setInvno(String invno) {
        this.invno = invno;
    }

    public String getNogrn() {
        return nogrn;
    }

    public void setNogrn(String nogrn) {
        this.nogrn = nogrn;
    }

    public String getNoinv() {
        return noinv;
    }

    public void setNoinv(String noinv) {
        this.noinv = noinv;
    }

    public String getInvtype() {
        return invtype;
    }

    public void setInvtype(String invtype) {
        this.invtype = invtype;
    }

    public String getInvrefno() {
        return invrefno;
    }

    public void setInvrefno(String invrefno) {
        this.invrefno = invrefno;
    }

    public String getPorefno() {
        return porefno;
    }

    public void setPorefno(String porefno) {
        this.porefno = porefno;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getAppdesig() {
        return appdesig;
    }

    public void setAppdesig(String appdesig) {
        this.appdesig = appdesig;
    }

    public String getPreid() {
        return preid;
    }

    public void setPreid(String preid) {
        this.preid = preid;
    }

    public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        this.prename = prename;
    }

    public String getPredate() {
        return predate;
    }

    public void setPredate(String predate) {
        this.predate = predate;
    }

    public String getAppdate() {
        return appdate;
    }

    public void setAppdate(String appdate) {
        this.appdate = appdate;
    }

    public String getCheckid() {
        return checkid;
    }

    public void setCheckid(String checkid) {
        this.checkid = checkid;
    }

    public String getCheckname() {
        return checkname;
    }

    public void setCheckname(String checkname) {
        this.checkname = checkname;
    }

    public String getCheckdesig() {
        return checkdesig;
    }

    public void setCheckdesig(String checkdesig) {
        this.checkdesig = checkdesig;
    }

    public String getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(String checkdate) {
        this.checkdate = checkdate;
    }

    public String getPostdate() {
        return postdate;
    }

    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }

    public String getPostflag() {
        return postflag;
    }

    public void setPostflag(String postflag) {
        this.postflag = postflag;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getEstatename() {
        return estatename;
    }

    public void setEstatename(String estatename) {
        this.estatename = estatename;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSuppcode() {
        return suppcode;
    }

    public void setSuppcode(String suppcode) {
        this.suppcode = suppcode;
    }

    public String getSuppname() {
        return suppname;
    }

    public void setSuppname(String suppname) {
        this.suppname = suppname;
    }

    public String getSuppaddress() {
        return suppaddress;
    }

    public void setSuppaddress(String suppaddress) {
        this.suppaddress = suppaddress;
    }

    public String getAccode() {
        return accode;
    }

    public void setAccode(String accode) {
        this.accode = accode;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public double getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(double totalamount) {
        this.totalamount = totalamount;
    }

    public double getPaid() {
        return paid;
    }

    public void setPaid(double paid) {
        this.paid = paid;
    }

    public String getAcdesc() {
        return acdesc;
    }

    public void setAcdesc(String acdesc) {
        this.acdesc = acdesc;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    public String getGstid() {
        return gstid;
    }

    public void setGstid(String gstid) {
        this.gstid = gstid;
    }

    public String getRacoacode() {
        return racoacode;
    }

    public void setRacoacode(String racoacode) {
        this.racoacode = racoacode;
    }

    public String getRacoadesc() {
        return racoadesc;
    }

    public void setRacoadesc(String racoadesc) {
        this.racoadesc = racoadesc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (invrefno != null ? invrefno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VendorInvoice)) {
            return false;
        }
        VendorInvoice other = (VendorInvoice) object;
        if ((this.invrefno == null && other.invrefno != null) || (this.invrefno != null && !this.invrefno.equals(other.invrefno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.ap.VendorInvoice[ invrefno=" + invrefno + " ]";
    }
    
}
