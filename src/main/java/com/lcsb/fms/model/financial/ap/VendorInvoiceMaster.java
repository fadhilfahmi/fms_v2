/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ap;

import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class VendorInvoiceMaster {
    
    private VendorInvoice apMain;
    private List<VendorInvoiceDetail> apList;

    public VendorInvoice getApMain() {
        return apMain;
    }

    public void setApMain(VendorInvoice apMain) {
        this.apMain = apMain;
    }

    public List<VendorInvoiceDetail> getApList() {
        return apList;
    }

    public void setApList(List<VendorInvoiceDetail> apList) {
        this.apList = apList;
    }
    
}
