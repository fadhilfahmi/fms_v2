/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */

package com.lcsb.fms.model.financial.gl;

/**
 *
 * @author Dell
 */
public class Prepayment {
    private String refer;
    private String cnt;
    private String actcode;
    private String actdesc;
    private String estatecode;
    private String estatename;
    private String estlevel;
    private String ctcode;
    private String cttype;
    private String ctdesc;
    private String actcode1;
    private String actdesc1;
    private String loccode;
    private String loclevel;
    private String locdesc;
    private String dtcttype;
    private String dtctcode;
    private String dtctdesc;
    private String dtsatype;
    private String dtsacode;
    private String  dtsadesc;
    private String satype;
    private String sacode;
    private String sadesc;
    private String remark;
    private String dttotal;
    private String cttotal;

    public String getDttotal() {
        return dttotal;
    }

    public void setDttotal(String dttotal) {
        this.dttotal = dttotal;
    }

    public String getCttotal() {
        return cttotal;
    }

    public void setCttotal(String cttotal) {
        this.cttotal = cttotal;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getCnt() {
        return cnt;
    }

    public void setCnt(String cnt) {
        this.cnt = cnt;
    }

    public String getActcode() {
        return actcode;
    }

    public void setActcode(String actcode) {
        this.actcode = actcode;
    }

    public String getActdesc() {
        return actdesc;
    }

    public void setActdesc(String actdesc) {
        this.actdesc = actdesc;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getEstatename() {
        return estatename;
    }

    public void setEstatename(String estatename) {
        this.estatename = estatename;
    }

    public String getEstlevel() {
        return estlevel;
    }

    public void setEstlevel(String estlevel) {
        this.estlevel = estlevel;
    }

    public String getCtcode() {
        return ctcode;
    }

    public void setCtcode(String ctcode) {
        this.ctcode = ctcode;
    }

    public String getCttype() {
        return cttype;
    }

    public void setCttype(String cttype) {
        this.cttype = cttype;
    }

    public String getCtdesc() {
        return ctdesc;
    }

    public void setCtdesc(String ctdesc) {
        this.ctdesc = ctdesc;
    }

    public String getActcode1() {
        return actcode1;
    }

    public void setActcode1(String actcode1) {
        this.actcode1 = actcode1;
    }

    public String getActdesc1() {
        return actdesc1;
    }

    public void setActdesc1(String actdesc1) {
        this.actdesc1 = actdesc1;
    }

    public String getLoclevel() {
        return loclevel;
    }

    public void setLoclevel(String loclevel) {
        this.loclevel = loclevel;
    }

    public String getLocdesc() {
        return locdesc;
    }

    public void setLocdesc(String locdesc) {
        this.locdesc = locdesc;
    }

    public String getDtcttype() {
        return dtcttype;
    }

    public void setDtcttype(String dtcttype) {
        this.dtcttype = dtcttype;
    }

    public String getDtctcode() {
        return dtctcode;
    }

    public void setDtctcode(String dtctcode) {
        this.dtctcode = dtctcode;
    }

    public String getDtctdesc() {
        return dtctdesc;
    }

    public void setDtctdesc(String dtctdesc) {
        this.dtctdesc = dtctdesc;
    }

    public String getDtsatype() {
        return dtsatype;
    }

    public void setDtsatype(String dtsatype) {
        this.dtsatype = dtsatype;
    }

    public String getDtsacode() {
        return dtsacode;
    }

    public void setDtsacode(String dtsacode) {
        this.dtsacode = dtsacode;
    }

    public String getDtsadesc() {
        return dtsadesc;
    }

    public void setDtsadesc(String dtsadesc) {
        this.dtsadesc = dtsadesc;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
