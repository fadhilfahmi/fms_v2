/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ap;

/**
 *
 * @author fadhilfahmi
 */
public class VendorPayee {
    
    private String suppcode;
    private String suppname;
    private int totalInvoice;

    public String getSuppcode() {
        return suppcode;
    }

    public void setSuppcode(String suppcode) {
        this.suppcode = suppcode;
    }

    public String getSuppname() {
        return suppname;
    }

    public void setSuppname(String suppname) {
        this.suppname = suppname;
    }

    public int getTotalInvoice() {
        return totalInvoice;
    }

    public void setTotalInvoice(int totalInvoice) {
        this.totalInvoice = totalInvoice;
    }
    
    
    
}
