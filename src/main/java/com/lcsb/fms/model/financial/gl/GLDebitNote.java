/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.gl;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "gl_debitnote")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GLDebitNote.findAll", query = "SELECT g FROM GLDebitNote g"),
    @NamedQuery(name = "GLDebitNote.findByNoteno", query = "SELECT g FROM GLDebitNote g WHERE g.noteno = :noteno"),
    @NamedQuery(name = "GLDebitNote.findByNotedate", query = "SELECT g FROM GLDebitNote g WHERE g.notedate = :notedate"),
    @NamedQuery(name = "GLDebitNote.findByYear", query = "SELECT g FROM GLDebitNote g WHERE g.year = :year"),
    @NamedQuery(name = "GLDebitNote.findByPeriod", query = "SELECT g FROM GLDebitNote g WHERE g.period = :period"),
    @NamedQuery(name = "GLDebitNote.findByAcccode", query = "SELECT g FROM GLDebitNote g WHERE g.acccode = :acccode"),
    @NamedQuery(name = "GLDebitNote.findByAccdesc", query = "SELECT g FROM GLDebitNote g WHERE g.accdesc = :accdesc"),
    @NamedQuery(name = "GLDebitNote.findByEstcode", query = "SELECT g FROM GLDebitNote g WHERE g.estcode = :estcode"),
    @NamedQuery(name = "GLDebitNote.findByEstname", query = "SELECT g FROM GLDebitNote g WHERE g.estname = :estname"),
    @NamedQuery(name = "GLDebitNote.findByCttype", query = "SELECT g FROM GLDebitNote g WHERE g.cttype = :cttype"),
    @NamedQuery(name = "GLDebitNote.findByCtcode", query = "SELECT g FROM GLDebitNote g WHERE g.ctcode = :ctcode"),
    @NamedQuery(name = "GLDebitNote.findByCtdesc", query = "SELECT g FROM GLDebitNote g WHERE g.ctdesc = :ctdesc"),
    @NamedQuery(name = "GLDebitNote.findByTotal", query = "SELECT g FROM GLDebitNote g WHERE g.total = :total"),
    @NamedQuery(name = "GLDebitNote.findByPostflag", query = "SELECT g FROM GLDebitNote g WHERE g.postflag = :postflag"),
    @NamedQuery(name = "GLDebitNote.findByPostdate", query = "SELECT g FROM GLDebitNote g WHERE g.postdate = :postdate"),
    @NamedQuery(name = "GLDebitNote.findByTopost", query = "SELECT g FROM GLDebitNote g WHERE g.topost = :topost"),
    @NamedQuery(name = "GLDebitNote.findByPrepareid", query = "SELECT g FROM GLDebitNote g WHERE g.prepareid = :prepareid"),
    @NamedQuery(name = "GLDebitNote.findByPreparename", query = "SELECT g FROM GLDebitNote g WHERE g.preparename = :preparename"),
    @NamedQuery(name = "GLDebitNote.findByPreparedate", query = "SELECT g FROM GLDebitNote g WHERE g.preparedate = :preparedate"),
    @NamedQuery(name = "GLDebitNote.findByApproveid", query = "SELECT g FROM GLDebitNote g WHERE g.approveid = :approveid"),
    @NamedQuery(name = "GLDebitNote.findByApprovename", query = "SELECT g FROM GLDebitNote g WHERE g.approvename = :approvename"),
    @NamedQuery(name = "GLDebitNote.findByApprovedesig", query = "SELECT g FROM GLDebitNote g WHERE g.approvedesig = :approvedesig"),
    @NamedQuery(name = "GLDebitNote.findByApprovedate", query = "SELECT g FROM GLDebitNote g WHERE g.approvedate = :approvedate"),
    @NamedQuery(name = "GLDebitNote.findByReceivercode", query = "SELECT g FROM GLDebitNote g WHERE g.receivercode = :receivercode"),
    @NamedQuery(name = "GLDebitNote.findByReceivername", query = "SELECT g FROM GLDebitNote g WHERE g.receivername = :receivername"),
    @NamedQuery(name = "GLDebitNote.findBySatype", query = "SELECT g FROM GLDebitNote g WHERE g.satype = :satype"),
    @NamedQuery(name = "GLDebitNote.findBySacode", query = "SELECT g FROM GLDebitNote g WHERE g.sacode = :sacode"),
    @NamedQuery(name = "GLDebitNote.findBySadesc", query = "SELECT g FROM GLDebitNote g WHERE g.sadesc = :sadesc")})
public class GLDebitNote implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "noteno")
    private String noteno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "notedate")
    @Temporal(TemporalType.DATE)
    private Date notedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "period")
    private String period;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "acccode")
    private String acccode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "accdesc")
    private String accdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estcode")
    private String estcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estname")
    private String estname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cttype")
    private String cttype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ctcode")
    private String ctcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ctdesc")
    private String ctdesc;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "remark")
    private String remark;
    @Basic(optional = false)
    @NotNull
    @Column(name = "total")
    private double total;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postflag")
    private String postflag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "postdate")
    @Temporal(TemporalType.DATE)
    private Date postdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "topost")
    private String topost;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prepareid")
    private String prepareid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "preparename")
    private String preparename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "preparedate")
    private String preparedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "approveid")
    private String approveid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "approvename")
    private String approvename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "approvedesig")
    private String approvedesig;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "approvedate")
    private String approvedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "receivercode")
    private String receivercode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "receivername")
    private String receivername;
    @Size(max = 100)
    @Column(name = "satype")
    private String satype;
    @Size(max = 100)
    @Column(name = "sacode")
    private String sacode;
    @Size(max = 100)
    @Column(name = "sadesc")
    private String sadesc;
    private String JVno;
    private String refernoteno;

    public GLDebitNote() {
    }

    public GLDebitNote(String noteno) {
        this.noteno = noteno;
    }

    public GLDebitNote(String noteno, Date notedate, String year, String period, String acccode, String accdesc, String estcode, String estname, String cttype, String ctcode, String ctdesc, String remark, double total, String postflag, Date postdate, String topost, String prepareid, String preparename, String preparedate, String approveid, String approvename, String approvedesig, String approvedate, String receivercode, String receivername, String JVno, String refernoteno) {
        this.noteno = noteno;
        this.notedate = notedate;
        this.year = year;
        this.period = period;
        this.acccode = acccode;
        this.accdesc = accdesc;
        this.estcode = estcode;
        this.estname = estname;
        this.cttype = cttype;
        this.ctcode = ctcode;
        this.ctdesc = ctdesc;
        this.remark = remark;
        this.total = total;
        this.postflag = postflag;
        this.postdate = postdate;
        this.topost = topost;
        this.prepareid = prepareid;
        this.preparename = preparename;
        this.preparedate = preparedate;
        this.approveid = approveid;
        this.approvename = approvename;
        this.approvedesig = approvedesig;
        this.approvedate = approvedate;
        this.receivercode = receivercode;
        this.receivername = receivername;
        this.JVno = JVno;
        this.refernoteno = refernoteno;
    }

    public String getRefernoteno() {
        return refernoteno;
    }

    public void setRefernoteno(String refernoteno) {
        this.refernoteno = refernoteno;
    }

    public String getJVno() {
        return JVno;
    }

    public void setJVno(String JVno) {
        this.JVno = JVno;
    }

    public String getNoteno() {
        return noteno;
    }

    public void setNoteno(String noteno) {
        this.noteno = noteno;
    }

    public Date getNotedate() {
        return notedate;
    }

    public void setNotedate(Date notedate) {
        this.notedate = notedate;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getAcccode() {
        return acccode;
    }

    public void setAcccode(String acccode) {
        this.acccode = acccode;
    }

    public String getAccdesc() {
        return accdesc;
    }

    public void setAccdesc(String accdesc) {
        this.accdesc = accdesc;
    }

    public String getEstcode() {
        return estcode;
    }

    public void setEstcode(String estcode) {
        this.estcode = estcode;
    }

    public String getEstname() {
        return estname;
    }

    public void setEstname(String estname) {
        this.estname = estname;
    }

    public String getCttype() {
        return cttype;
    }

    public void setCttype(String cttype) {
        this.cttype = cttype;
    }

    public String getCtcode() {
        return ctcode;
    }

    public void setCtcode(String ctcode) {
        this.ctcode = ctcode;
    }

    public String getCtdesc() {
        return ctdesc;
    }

    public void setCtdesc(String ctdesc) {
        this.ctdesc = ctdesc;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getPostflag() {
        return postflag;
    }

    public void setPostflag(String postflag) {
        this.postflag = postflag;
    }

    public Date getPostdate() {
        return postdate;
    }

    public void setPostdate(Date postdate) {
        this.postdate = postdate;
    }

    public String getTopost() {
        return topost;
    }

    public void setTopost(String topost) {
        this.topost = topost;
    }

    public String getPrepareid() {
        return prepareid;
    }

    public void setPrepareid(String prepareid) {
        this.prepareid = prepareid;
    }

    public String getPreparename() {
        return preparename;
    }

    public void setPreparename(String preparename) {
        this.preparename = preparename;
    }

    public String getPreparedate() {
        return preparedate;
    }

    public void setPreparedate(String preparedate) {
        this.preparedate = preparedate;
    }

    public String getApproveid() {
        return approveid;
    }

    public void setApproveid(String approveid) {
        this.approveid = approveid;
    }

    public String getApprovename() {
        return approvename;
    }

    public void setApprovename(String approvename) {
        this.approvename = approvename;
    }

    public String getApprovedesig() {
        return approvedesig;
    }

    public void setApprovedesig(String approvedesig) {
        this.approvedesig = approvedesig;
    }

    public String getApprovedate() {
        return approvedate;
    }

    public void setApprovedate(String approvedate) {
        this.approvedate = approvedate;
    }

    public String getReceivercode() {
        return receivercode;
    }

    public void setReceivercode(String receivercode) {
        this.receivercode = receivercode;
    }

    public String getReceivername() {
        return receivername;
    }

    public void setReceivername(String receivername) {
        this.receivername = receivername;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noteno != null ? noteno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GLDebitNote)) {
            return false;
        }
        GLDebitNote other = (GLDebitNote) object;
        if ((this.noteno == null && other.noteno != null) || (this.noteno != null && !this.noteno.equals(other.noteno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.gl.GLDebitNote[ noteno=" + noteno + " ]";
    }
    
}
