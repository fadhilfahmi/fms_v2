/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.gl;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "est_debitnote")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstDebitNote.findAll", query = "SELECT e FROM EstDebitNote e"),
    @NamedQuery(name = "EstDebitNote.findByNoteno", query = "SELECT e FROM EstDebitNote e WHERE e.noteno = :noteno"),
    @NamedQuery(name = "EstDebitNote.findByNotedate", query = "SELECT e FROM EstDebitNote e WHERE e.notedate = :notedate"),
    @NamedQuery(name = "EstDebitNote.findByYear", query = "SELECT e FROM EstDebitNote e WHERE e.year = :year"),
    @NamedQuery(name = "EstDebitNote.findByPeriod", query = "SELECT e FROM EstDebitNote e WHERE e.period = :period"),
    @NamedQuery(name = "EstDebitNote.findByAcccode", query = "SELECT e FROM EstDebitNote e WHERE e.acccode = :acccode"),
    @NamedQuery(name = "EstDebitNote.findByAccdesc", query = "SELECT e FROM EstDebitNote e WHERE e.accdesc = :accdesc"),
    @NamedQuery(name = "EstDebitNote.findByAddress", query = "SELECT e FROM EstDebitNote e WHERE e.address = :address"),
    @NamedQuery(name = "EstDebitNote.findByHqactno", query = "SELECT e FROM EstDebitNote e WHERE e.hqactno = :hqactno"),
    @NamedQuery(name = "EstDebitNote.findByTrxno", query = "SELECT e FROM EstDebitNote e WHERE e.trxno = :trxno"),
    @NamedQuery(name = "EstDebitNote.findByTerms", query = "SELECT e FROM EstDebitNote e WHERE e.terms = :terms"),
    @NamedQuery(name = "EstDebitNote.findByDNno", query = "SELECT e FROM EstDebitNote e WHERE e.dNno = :dNno"),
    @NamedQuery(name = "EstDebitNote.findByMgtcode", query = "SELECT e FROM EstDebitNote e WHERE e.mgtcode = :mgtcode"),
    @NamedQuery(name = "EstDebitNote.findByEstcode", query = "SELECT e FROM EstDebitNote e WHERE e.estcode = :estcode"),
    @NamedQuery(name = "EstDebitNote.findByEstname", query = "SELECT e FROM EstDebitNote e WHERE e.estname = :estname"),
    @NamedQuery(name = "EstDebitNote.findByCttype", query = "SELECT e FROM EstDebitNote e WHERE e.cttype = :cttype"),
    @NamedQuery(name = "EstDebitNote.findByCtcode", query = "SELECT e FROM EstDebitNote e WHERE e.ctcode = :ctcode"),
    @NamedQuery(name = "EstDebitNote.findByCtdesc", query = "SELECT e FROM EstDebitNote e WHERE e.ctdesc = :ctdesc"),
    @NamedQuery(name = "EstDebitNote.findByTotal", query = "SELECT e FROM EstDebitNote e WHERE e.total = :total"),
    @NamedQuery(name = "EstDebitNote.findByTotalword", query = "SELECT e FROM EstDebitNote e WHERE e.totalword = :totalword"),
    @NamedQuery(name = "EstDebitNote.findByPostflag", query = "SELECT e FROM EstDebitNote e WHERE e.postflag = :postflag"),
    @NamedQuery(name = "EstDebitNote.findByPostdate", query = "SELECT e FROM EstDebitNote e WHERE e.postdate = :postdate"),
    @NamedQuery(name = "EstDebitNote.findByTopost", query = "SELECT e FROM EstDebitNote e WHERE e.topost = :topost"),
    @NamedQuery(name = "EstDebitNote.findByPrepareid", query = "SELECT e FROM EstDebitNote e WHERE e.prepareid = :prepareid"),
    @NamedQuery(name = "EstDebitNote.findByPreparename", query = "SELECT e FROM EstDebitNote e WHERE e.preparename = :preparename"),
    @NamedQuery(name = "EstDebitNote.findByPreparedate", query = "SELECT e FROM EstDebitNote e WHERE e.preparedate = :preparedate"),
    @NamedQuery(name = "EstDebitNote.findByCheckname", query = "SELECT e FROM EstDebitNote e WHERE e.checkname = :checkname"),
    @NamedQuery(name = "EstDebitNote.findByCheckdate", query = "SELECT e FROM EstDebitNote e WHERE e.checkdate = :checkdate"),
    @NamedQuery(name = "EstDebitNote.findByCheckid", query = "SELECT e FROM EstDebitNote e WHERE e.checkid = :checkid"),
    @NamedQuery(name = "EstDebitNote.findByApproveid", query = "SELECT e FROM EstDebitNote e WHERE e.approveid = :approveid"),
    @NamedQuery(name = "EstDebitNote.findByApprovename", query = "SELECT e FROM EstDebitNote e WHERE e.approvename = :approvename"),
    @NamedQuery(name = "EstDebitNote.findByApprovedesig", query = "SELECT e FROM EstDebitNote e WHERE e.approvedesig = :approvedesig"),
    @NamedQuery(name = "EstDebitNote.findByApprovedate", query = "SELECT e FROM EstDebitNote e WHERE e.approvedate = :approvedate"),
    @NamedQuery(name = "EstDebitNote.findByRcactdesc", query = "SELECT e FROM EstDebitNote e WHERE e.rcactdesc = :rcactdesc"),
    @NamedQuery(name = "EstDebitNote.findByRcactcode", query = "SELECT e FROM EstDebitNote e WHERE e.rcactcode = :rcactcode"),
    @NamedQuery(name = "EstDebitNote.findByReceivercode", query = "SELECT e FROM EstDebitNote e WHERE e.receivercode = :receivercode"),
    @NamedQuery(name = "EstDebitNote.findByReceivername", query = "SELECT e FROM EstDebitNote e WHERE e.receivername = :receivername"),
    @NamedQuery(name = "EstDebitNote.findByExtractdate", query = "SELECT e FROM EstDebitNote e WHERE e.extractdate = :extractdate")})
public class EstDebitNote implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "noteno")
    private String noteno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "notedate")
    @Temporal(TemporalType.DATE)
    private Date notedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "period")
    private String period;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "acccode")
    private String acccode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "accdesc")
    private String accdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "hqactno")
    private String hqactno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "trxno")
    private String trxno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "terms")
    private String terms;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "DNno")
    private String dNno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "mgtcode")
    private String mgtcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estcode")
    private String estcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estname")
    private String estname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cttype")
    private String cttype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ctcode")
    private String ctcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ctdesc")
    private String ctdesc;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "remark")
    private String remark;
    @Basic(optional = false)
    @NotNull
    @Column(name = "total")
    private double total;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "totalword")
    private String totalword;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postflag")
    private String postflag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "postdate")
    @Temporal(TemporalType.DATE)
    private Date postdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "topost")
    private String topost;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prepareid")
    private String prepareid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "preparename")
    private String preparename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "preparedate")
    private String preparedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkname")
    private String checkname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkdate")
    private String checkdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkid")
    private String checkid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "approveid")
    private String approveid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "approvename")
    private String approvename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "approvedesig")
    private String approvedesig;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "approvedate")
    private String approvedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "rcactdesc")
    private String rcactdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "rcactcode")
    private String rcactcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "receivercode")
    private String receivercode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "receivername")
    private String receivername;
    @Basic(optional = false)
    @NotNull
    @Column(name = "extractdate")
    @Temporal(TemporalType.DATE)
    private Date extractdate;

    public EstDebitNote() {
    }

    public EstDebitNote(String noteno) {
        this.noteno = noteno;
    }

    public EstDebitNote(String noteno, Date notedate, String year, String period, String acccode, String accdesc, String address, String hqactno, String trxno, String terms, String dNno, String mgtcode, String estcode, String estname, String cttype, String ctcode, String ctdesc, String remark, double total, String totalword, String postflag, Date postdate, String topost, String prepareid, String preparename, String preparedate, String checkname, String checkdate, String checkid, String approveid, String approvename, String approvedesig, String approvedate, String rcactdesc, String rcactcode, String receivercode, String receivername, Date extractdate) {
        this.noteno = noteno;
        this.notedate = notedate;
        this.year = year;
        this.period = period;
        this.acccode = acccode;
        this.accdesc = accdesc;
        this.address = address;
        this.hqactno = hqactno;
        this.trxno = trxno;
        this.terms = terms;
        this.dNno = dNno;
        this.mgtcode = mgtcode;
        this.estcode = estcode;
        this.estname = estname;
        this.cttype = cttype;
        this.ctcode = ctcode;
        this.ctdesc = ctdesc;
        this.remark = remark;
        this.total = total;
        this.totalword = totalword;
        this.postflag = postflag;
        this.postdate = postdate;
        this.topost = topost;
        this.prepareid = prepareid;
        this.preparename = preparename;
        this.preparedate = preparedate;
        this.checkname = checkname;
        this.checkdate = checkdate;
        this.checkid = checkid;
        this.approveid = approveid;
        this.approvename = approvename;
        this.approvedesig = approvedesig;
        this.approvedate = approvedate;
        this.rcactdesc = rcactdesc;
        this.rcactcode = rcactcode;
        this.receivercode = receivercode;
        this.receivername = receivername;
        this.extractdate = extractdate;
    }

    public String getNoteno() {
        return noteno;
    }

    public void setNoteno(String noteno) {
        this.noteno = noteno;
    }

    public Date getNotedate() {
        return notedate;
    }

    public void setNotedate(Date notedate) {
        this.notedate = notedate;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getAcccode() {
        return acccode;
    }

    public void setAcccode(String acccode) {
        this.acccode = acccode;
    }

    public String getAccdesc() {
        return accdesc;
    }

    public void setAccdesc(String accdesc) {
        this.accdesc = accdesc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHqactno() {
        return hqactno;
    }

    public void setHqactno(String hqactno) {
        this.hqactno = hqactno;
    }

    public String getTrxno() {
        return trxno;
    }

    public void setTrxno(String trxno) {
        this.trxno = trxno;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getDNno() {
        return dNno;
    }

    public void setDNno(String dNno) {
        this.dNno = dNno;
    }

    public String getMgtcode() {
        return mgtcode;
    }

    public void setMgtcode(String mgtcode) {
        this.mgtcode = mgtcode;
    }

    public String getEstcode() {
        return estcode;
    }

    public void setEstcode(String estcode) {
        this.estcode = estcode;
    }

    public String getEstname() {
        return estname;
    }

    public void setEstname(String estname) {
        this.estname = estname;
    }

    public String getCttype() {
        return cttype;
    }

    public void setCttype(String cttype) {
        this.cttype = cttype;
    }

    public String getCtcode() {
        return ctcode;
    }

    public void setCtcode(String ctcode) {
        this.ctcode = ctcode;
    }

    public String getCtdesc() {
        return ctdesc;
    }

    public void setCtdesc(String ctdesc) {
        this.ctdesc = ctdesc;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getTotalword() {
        return totalword;
    }

    public void setTotalword(String totalword) {
        this.totalword = totalword;
    }

    public String getPostflag() {
        return postflag;
    }

    public void setPostflag(String postflag) {
        this.postflag = postflag;
    }

    public Date getPostdate() {
        return postdate;
    }

    public void setPostdate(Date postdate) {
        this.postdate = postdate;
    }

    public String getTopost() {
        return topost;
    }

    public void setTopost(String topost) {
        this.topost = topost;
    }

    public String getPrepareid() {
        return prepareid;
    }

    public void setPrepareid(String prepareid) {
        this.prepareid = prepareid;
    }

    public String getPreparename() {
        return preparename;
    }

    public void setPreparename(String preparename) {
        this.preparename = preparename;
    }

    public String getPreparedate() {
        return preparedate;
    }

    public void setPreparedate(String preparedate) {
        this.preparedate = preparedate;
    }

    public String getCheckname() {
        return checkname;
    }

    public void setCheckname(String checkname) {
        this.checkname = checkname;
    }

    public String getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(String checkdate) {
        this.checkdate = checkdate;
    }

    public String getCheckid() {
        return checkid;
    }

    public void setCheckid(String checkid) {
        this.checkid = checkid;
    }

    public String getApproveid() {
        return approveid;
    }

    public void setApproveid(String approveid) {
        this.approveid = approveid;
    }

    public String getApprovename() {
        return approvename;
    }

    public void setApprovename(String approvename) {
        this.approvename = approvename;
    }

    public String getApprovedesig() {
        return approvedesig;
    }

    public void setApprovedesig(String approvedesig) {
        this.approvedesig = approvedesig;
    }

    public String getApprovedate() {
        return approvedate;
    }

    public void setApprovedate(String approvedate) {
        this.approvedate = approvedate;
    }

    public String getRcactdesc() {
        return rcactdesc;
    }

    public void setRcactdesc(String rcactdesc) {
        this.rcactdesc = rcactdesc;
    }

    public String getRcactcode() {
        return rcactcode;
    }

    public void setRcactcode(String rcactcode) {
        this.rcactcode = rcactcode;
    }

    public String getReceivercode() {
        return receivercode;
    }

    public void setReceivercode(String receivercode) {
        this.receivercode = receivercode;
    }

    public String getReceivername() {
        return receivername;
    }

    public void setReceivername(String receivername) {
        this.receivername = receivername;
    }

    public Date getExtractdate() {
        return extractdate;
    }

    public void setExtractdate(Date extractdate) {
        this.extractdate = extractdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noteno != null ? noteno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstDebitNote)) {
            return false;
        }
        EstDebitNote other = (EstDebitNote) object;
        if ((this.noteno == null && other.noteno != null) || (this.noteno != null && !this.noteno.equals(other.noteno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.gl.EstDebitNote[ noteno=" + noteno + " ]";
    }
    
}
