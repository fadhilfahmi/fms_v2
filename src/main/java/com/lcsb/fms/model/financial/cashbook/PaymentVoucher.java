/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "cb_payvoucher")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PaymentVoucher.findAll", query = "SELECT p FROM PaymentVoucher p"),
    @NamedQuery(name = "PaymentVoucher.findByEstatecode", query = "SELECT p FROM PaymentVoucher p WHERE p.estatecode = :estatecode"),
    @NamedQuery(name = "PaymentVoucher.findByEstatename", query = "SELECT p FROM PaymentVoucher p WHERE p.estatename = :estatename"),
    @NamedQuery(name = "PaymentVoucher.findByEstateaddress", query = "SELECT p FROM PaymentVoucher p WHERE p.estateaddress = :estateaddress"),
    @NamedQuery(name = "PaymentVoucher.findByEstatecity", query = "SELECT p FROM PaymentVoucher p WHERE p.estatecity = :estatecity"),
    @NamedQuery(name = "PaymentVoucher.findByEstatestate", query = "SELECT p FROM PaymentVoucher p WHERE p.estatestate = :estatestate"),
    @NamedQuery(name = "PaymentVoucher.findByEstatepostcode", query = "SELECT p FROM PaymentVoucher p WHERE p.estatepostcode = :estatepostcode"),
    @NamedQuery(name = "PaymentVoucher.findByEstatephone", query = "SELECT p FROM PaymentVoucher p WHERE p.estatephone = :estatephone"),
    @NamedQuery(name = "PaymentVoucher.findByEstatefax", query = "SELECT p FROM PaymentVoucher p WHERE p.estatefax = :estatefax"),
    @NamedQuery(name = "PaymentVoucher.findByYear", query = "SELECT p FROM PaymentVoucher p WHERE p.year = :year"),
    @NamedQuery(name = "PaymentVoucher.findByPeriod", query = "SELECT p FROM PaymentVoucher p WHERE p.period = :period"),
    @NamedQuery(name = "PaymentVoucher.findByBankcode", query = "SELECT p FROM PaymentVoucher p WHERE p.bankcode = :bankcode"),
    @NamedQuery(name = "PaymentVoucher.findByBankname", query = "SELECT p FROM PaymentVoucher p WHERE p.bankname = :bankname"),
    @NamedQuery(name = "PaymentVoucher.findByVoucherno", query = "SELECT p FROM PaymentVoucher p WHERE p.voucherno = :voucherno"),
    @NamedQuery(name = "PaymentVoucher.findByTarikh", query = "SELECT p FROM PaymentVoucher p WHERE p.tarikh = :tarikh"),
    @NamedQuery(name = "PaymentVoucher.findByPaidtype", query = "SELECT p FROM PaymentVoucher p WHERE p.paidtype = :paidtype"),
    @NamedQuery(name = "PaymentVoucher.findByPaidcode", query = "SELECT p FROM PaymentVoucher p WHERE p.paidcode = :paidcode"),
    @NamedQuery(name = "PaymentVoucher.findByPaidname", query = "SELECT p FROM PaymentVoucher p WHERE p.paidname = :paidname"),
    @NamedQuery(name = "PaymentVoucher.findByPaidaddress", query = "SELECT p FROM PaymentVoucher p WHERE p.paidaddress = :paidaddress"),
    @NamedQuery(name = "PaymentVoucher.findByPaidcity", query = "SELECT p FROM PaymentVoucher p WHERE p.paidcity = :paidcity"),
    @NamedQuery(name = "PaymentVoucher.findByPaidstate", query = "SELECT p FROM PaymentVoucher p WHERE p.paidstate = :paidstate"),
    @NamedQuery(name = "PaymentVoucher.findByPaidpostcode", query = "SELECT p FROM PaymentVoucher p WHERE p.paidpostcode = :paidpostcode"),
    @NamedQuery(name = "PaymentVoucher.findByPost", query = "SELECT p FROM PaymentVoucher p WHERE p.post = :post"),
    @NamedQuery(name = "PaymentVoucher.findByTarikhpost", query = "SELECT p FROM PaymentVoucher p WHERE p.tarikhpost = :tarikhpost"),
    @NamedQuery(name = "PaymentVoucher.findByAmount", query = "SELECT p FROM PaymentVoucher p WHERE p.amount = :amount"),
    @NamedQuery(name = "PaymentVoucher.findByPaymentmode", query = "SELECT p FROM PaymentVoucher p WHERE p.paymentmode = :paymentmode"),
    @NamedQuery(name = "PaymentVoucher.findByCekno", query = "SELECT p FROM PaymentVoucher p WHERE p.cekno = :cekno"),
    @NamedQuery(name = "PaymentVoucher.findByAppid", query = "SELECT p FROM PaymentVoucher p WHERE p.appid = :appid"),
    @NamedQuery(name = "PaymentVoucher.findByAppname", query = "SELECT p FROM PaymentVoucher p WHERE p.appname = :appname"),
    @NamedQuery(name = "PaymentVoucher.findByAppposition", query = "SELECT p FROM PaymentVoucher p WHERE p.appposition = :appposition"),
    @NamedQuery(name = "PaymentVoucher.findByPreid", query = "SELECT p FROM PaymentVoucher p WHERE p.preid = :preid"),
    @NamedQuery(name = "PaymentVoucher.findByPrename", query = "SELECT p FROM PaymentVoucher p WHERE p.prename = :prename"),
    @NamedQuery(name = "PaymentVoucher.findByPreposition", query = "SELECT p FROM PaymentVoucher p WHERE p.preposition = :preposition"),
    @NamedQuery(name = "PaymentVoucher.findByFlag", query = "SELECT p FROM PaymentVoucher p WHERE p.flag = :flag"),
    @NamedQuery(name = "PaymentVoucher.findByRefer", query = "SELECT p FROM PaymentVoucher p WHERE p.refer = :refer"),
    @NamedQuery(name = "PaymentVoucher.findByFlagbank", query = "SELECT p FROM PaymentVoucher p WHERE p.flagbank = :flagbank"),
    @NamedQuery(name = "PaymentVoucher.findByRm", query = "SELECT p FROM PaymentVoucher p WHERE p.rm = :rm"),
    @NamedQuery(name = "PaymentVoucher.findByCheckid", query = "SELECT p FROM PaymentVoucher p WHERE p.checkid = :checkid"),
    @NamedQuery(name = "PaymentVoucher.findByCheckname", query = "SELECT p FROM PaymentVoucher p WHERE p.checkname = :checkname"),
    @NamedQuery(name = "PaymentVoucher.findByCheckposition", query = "SELECT p FROM PaymentVoucher p WHERE p.checkposition = :checkposition"),
    @NamedQuery(name = "PaymentVoucher.findByStmtyear", query = "SELECT p FROM PaymentVoucher p WHERE p.stmtyear = :stmtyear"),
    @NamedQuery(name = "PaymentVoucher.findByStmtperiod", query = "SELECT p FROM PaymentVoucher p WHERE p.stmtperiod = :stmtperiod"),
    @NamedQuery(name = "PaymentVoucher.findByNoagree", query = "SELECT p FROM PaymentVoucher p WHERE p.noagree = :noagree"),
    @NamedQuery(name = "PaymentVoucher.findByPaymentflag", query = "SELECT p FROM PaymentVoucher p WHERE p.paymentflag = :paymentflag"),
    @NamedQuery(name = "PaymentVoucher.findByAdvancetype", query = "SELECT p FROM PaymentVoucher p WHERE p.advancetype = :advancetype"),
    @NamedQuery(name = "PaymentVoucher.findByPreparedate", query = "SELECT p FROM PaymentVoucher p WHERE p.preparedate = :preparedate"),
    @NamedQuery(name = "PaymentVoucher.findByCheckdate", query = "SELECT p FROM PaymentVoucher p WHERE p.checkdate = :checkdate"),
    @NamedQuery(name = "PaymentVoucher.findByApprovedate", query = "SELECT p FROM PaymentVoucher p WHERE p.approvedate = :approvedate"),
    @NamedQuery(name = "PaymentVoucher.findByAdvance", query = "SELECT p FROM PaymentVoucher p WHERE p.advance = :advance"),
    @NamedQuery(name = "PaymentVoucher.findByPostgst", query = "SELECT p FROM PaymentVoucher p WHERE p.postgst = :postgst"),
    @NamedQuery(name = "PaymentVoucher.findByGstid", query = "SELECT p FROM PaymentVoucher p WHERE p.gstid = :gstid"),
    @NamedQuery(name = "PaymentVoucher.findByGstdate", query = "SELECT p FROM PaymentVoucher p WHERE p.gstdate = :gstdate"),
    @NamedQuery(name = "PaymentVoucher.findByRacoacode", query = "SELECT p FROM PaymentVoucher p WHERE p.racoacode = :racoacode"),
    @NamedQuery(name = "PaymentVoucher.findByRacoadesc", query = "SELECT p FROM PaymentVoucher p WHERE p.racoadesc = :racoadesc")})
public class PaymentVoucher implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatecode")
    private String estatecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatename")
    private String estatename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estateaddress")
    private String estateaddress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatecity")
    private String estatecity;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatestate")
    private String estatestate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatepostcode")
    private String estatepostcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatephone")
    private String estatephone;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatefax")
    private String estatefax;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "period")
    private String period;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bankcode")
    private String bankcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bankname")
    private String bankname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "voucherno")
    private String voucherno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "tarikh")
    private String tarikh;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paidtype")
    private String paidtype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paidcode")
    private String paidcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paidname")
    private String paidname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paidaddress")
    private String paidaddress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paidcity")
    private String paidcity;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paidstate")
    private String paidstate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paidpostcode")
    private String paidpostcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "post")
    private String post;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "tarikhpost")
    private String tarikhpost;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paymentmode")
    private String paymentmode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cekno")
    private String cekno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appid")
    private String appid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appname")
    private String appname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appposition")
    private String appposition;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "preid")
    private String preid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prename")
    private String prename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "preposition")
    private String preposition;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "flag")
    private String flag;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "flagbank")
    private String flagbank;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "rm")
    private String rm;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkid")
    private String checkid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkname")
    private String checkname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkposition")
    private String checkposition;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "stmtyear")
    private String stmtyear;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "stmtperiod")
    private String stmtperiod;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "noagree")
    private String noagree;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paymentflag")
    private String paymentflag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "advancetype")
    private String advancetype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "preparedate")
    private String preparedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "checkdate")
    private String checkdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "approvedate")
    private String approvedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "advance")
    private String advance;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postgst")
    private String postgst;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "gstid")
    private String gstid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "gstdate")
    private String gstdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "racoacode")
    private String racoacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "racoadesc")
    private String racoadesc;
    private String sessionID;

    public PaymentVoucher() {
    }

    public PaymentVoucher(String refer) {
        this.refer = refer;
    }

    public PaymentVoucher(String refer, String estatecode, String estatename, String estateaddress, String estatecity, String estatestate, String estatepostcode, String estatephone, String estatefax, String year, String period, String bankcode, String bankname, String voucherno, String tarikh, String paidtype, String paidcode, String paidname, String paidaddress, String paidcity, String paidstate, String paidpostcode, String post, String tarikhpost, String remarks, double amount, String paymentmode, String cekno, String appid, String appname, String appposition, String preid, String prename, String preposition, String flag, String flagbank, String rm, String checkid, String checkname, String checkposition, String stmtyear, String stmtperiod, String noagree, String paymentflag, String advancetype, String preparedate, String checkdate, String approvedate, String advance, String postgst, String gstid, String gstdate, String racoacode, String racoadesc, String sessionID) {
        this.refer = refer;
        this.estatecode = estatecode;
        this.estatename = estatename;
        this.estateaddress = estateaddress;
        this.estatecity = estatecity;
        this.estatestate = estatestate;
        this.estatepostcode = estatepostcode;
        this.estatephone = estatephone;
        this.estatefax = estatefax;
        this.year = year;
        this.period = period;
        this.bankcode = bankcode;
        this.bankname = bankname;
        this.voucherno = voucherno;
        this.tarikh = tarikh;
        this.paidtype = paidtype;
        this.paidcode = paidcode;
        this.paidname = paidname;
        this.paidaddress = paidaddress;
        this.paidcity = paidcity;
        this.paidstate = paidstate;
        this.paidpostcode = paidpostcode;
        this.post = post;
        this.tarikhpost = tarikhpost;
        this.remarks = remarks;
        this.amount = amount;
        this.paymentmode = paymentmode;
        this.cekno = cekno;
        this.appid = appid;
        this.appname = appname;
        this.appposition = appposition;
        this.preid = preid;
        this.prename = prename;
        this.preposition = preposition;
        this.flag = flag;
        this.flagbank = flagbank;
        this.rm = rm;
        this.checkid = checkid;
        this.checkname = checkname;
        this.checkposition = checkposition;
        this.stmtyear = stmtyear;
        this.stmtperiod = stmtperiod;
        this.noagree = noagree;
        this.paymentflag = paymentflag;
        this.advancetype = advancetype;
        this.preparedate = preparedate;
        this.checkdate = checkdate;
        this.approvedate = approvedate;
        this.advance = advance;
        this.postgst = postgst;
        this.gstid = gstid;
        this.gstdate = gstdate;
        this.racoacode = racoacode;
        this.racoadesc = racoadesc;
        this.sessionID = sessionID;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }
    
    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getEstatename() {
        return estatename;
    }

    public void setEstatename(String estatename) {
        this.estatename = estatename;
    }

    public String getEstateaddress() {
        return estateaddress;
    }

    public void setEstateaddress(String estateaddress) {
        this.estateaddress = estateaddress;
    }

    public String getEstatecity() {
        return estatecity;
    }

    public void setEstatecity(String estatecity) {
        this.estatecity = estatecity;
    }

    public String getEstatestate() {
        return estatestate;
    }

    public void setEstatestate(String estatestate) {
        this.estatestate = estatestate;
    }

    public String getEstatepostcode() {
        return estatepostcode;
    }

    public void setEstatepostcode(String estatepostcode) {
        this.estatepostcode = estatepostcode;
    }

    public String getEstatephone() {
        return estatephone;
    }

    public void setEstatephone(String estatephone) {
        this.estatephone = estatephone;
    }

    public String getEstatefax() {
        return estatefax;
    }

    public void setEstatefax(String estatefax) {
        this.estatefax = estatefax;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getVoucherno() {
        return voucherno;
    }

    public void setVoucherno(String voucherno) {
        this.voucherno = voucherno;
    }

    public String getTarikh() {
        return tarikh;
    }

    public void setTarikh(String tarikh) {
        this.tarikh = tarikh;
    }

    public String getPaidtype() {
        return paidtype;
    }

    public void setPaidtype(String paidtype) {
        this.paidtype = paidtype;
    }

    public String getPaidcode() {
        return paidcode;
    }

    public void setPaidcode(String paidcode) {
        this.paidcode = paidcode;
    }

    public String getPaidname() {
        return paidname;
    }

    public void setPaidname(String paidname) {
        this.paidname = paidname;
    }

    public String getPaidaddress() {
        return paidaddress;
    }

    public void setPaidaddress(String paidaddress) {
        this.paidaddress = paidaddress;
    }

    public String getPaidcity() {
        return paidcity;
    }

    public void setPaidcity(String paidcity) {
        this.paidcity = paidcity;
    }

    public String getPaidstate() {
        return paidstate;
    }

    public void setPaidstate(String paidstate) {
        this.paidstate = paidstate;
    }

    public String getPaidpostcode() {
        return paidpostcode;
    }

    public void setPaidpostcode(String paidpostcode) {
        this.paidpostcode = paidpostcode;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getTarikhpost() {
        return tarikhpost;
    }

    public void setTarikhpost(String tarikhpost) {
        this.tarikhpost = tarikhpost;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getPaymentmode() {
        return paymentmode;
    }

    public void setPaymentmode(String paymentmode) {
        this.paymentmode = paymentmode;
    }

    public String getCekno() {
        return cekno;
    }

    public void setCekno(String cekno) {
        this.cekno = cekno;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getAppposition() {
        return appposition;
    }

    public void setAppposition(String appposition) {
        this.appposition = appposition;
    }

    public String getPreid() {
        return preid;
    }

    public void setPreid(String preid) {
        this.preid = preid;
    }

    public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        this.prename = prename;
    }

    public String getPreposition() {
        return preposition;
    }

    public void setPreposition(String preposition) {
        this.preposition = preposition;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getFlagbank() {
        return flagbank;
    }

    public void setFlagbank(String flagbank) {
        this.flagbank = flagbank;
    }

    public String getRm() {
        return rm;
    }

    public void setRm(String rm) {
        this.rm = rm;
    }

    public String getCheckid() {
        return checkid;
    }

    public void setCheckid(String checkid) {
        this.checkid = checkid;
    }

    public String getCheckname() {
        return checkname;
    }

    public void setCheckname(String checkname) {
        this.checkname = checkname;
    }

    public String getCheckposition() {
        return checkposition;
    }

    public void setCheckposition(String checkposition) {
        this.checkposition = checkposition;
    }

    public String getStmtyear() {
        return stmtyear;
    }

    public void setStmtyear(String stmtyear) {
        this.stmtyear = stmtyear;
    }

    public String getStmtperiod() {
        return stmtperiod;
    }

    public void setStmtperiod(String stmtperiod) {
        this.stmtperiod = stmtperiod;
    }

    public String getNoagree() {
        return noagree;
    }

    public void setNoagree(String noagree) {
        this.noagree = noagree;
    }

    public String getPaymentflag() {
        return paymentflag;
    }

    public void setPaymentflag(String paymentflag) {
        this.paymentflag = paymentflag;
    }

    public String getAdvancetype() {
        return advancetype;
    }

    public void setAdvancetype(String advancetype) {
        this.advancetype = advancetype;
    }

    public String getPreparedate() {
        return preparedate;
    }

    public void setPreparedate(String preparedate) {
        this.preparedate = preparedate;
    }

    public String getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(String checkdate) {
        this.checkdate = checkdate;
    }

    public String getApprovedate() {
        return approvedate;
    }

    public void setApprovedate(String approvedate) {
        this.approvedate = approvedate;
    }

    public String getAdvance() {
        return advance;
    }

    public void setAdvance(String advance) {
        this.advance = advance;
    }

    public String getPostgst() {
        return postgst;
    }

    public void setPostgst(String postgst) {
        this.postgst = postgst;
    }

    public String getGstid() {
        return gstid;
    }

    public void setGstid(String gstid) {
        this.gstid = gstid;
    }

    public String getGstdate() {
        return gstdate;
    }

    public void setGstdate(String gstdate) {
        this.gstdate = gstdate;
    }

    public String getRacoacode() {
        return racoacode;
    }

    public void setRacoacode(String racoacode) {
        this.racoacode = racoacode;
    }

    public String getRacoadesc() {
        return racoadesc;
    }

    public void setRacoadesc(String racoadesc) {
        this.racoadesc = racoadesc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentVoucher)) {
            return false;
        }
        PaymentVoucher other = (PaymentVoucher) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.cashbook.PaymentVoucher[ refer=" + refer + " ]";
    }
    
}
