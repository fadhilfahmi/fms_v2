/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.employee;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "em_roc")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EmRoc.findAll", query = "SELECT e FROM EmRoc e"),
    @NamedQuery(name = "EmRoc.findByRefer", query = "SELECT e FROM EmRoc e WHERE e.refer = :refer"),
    @NamedQuery(name = "EmRoc.findByFileno", query = "SELECT e FROM EmRoc e WHERE e.fileno = :fileno"),
    @NamedQuery(name = "EmRoc.findByStaffid", query = "SELECT e FROM EmRoc e WHERE e.staffid = :staffid"),
    @NamedQuery(name = "EmRoc.findByStaffname", query = "SELECT e FROM EmRoc e WHERE e.staffname = :staffname"),
    @NamedQuery(name = "EmRoc.findByStaffdesignation", query = "SELECT e FROM EmRoc e WHERE e.staffdesignation = :staffdesignation"),
    @NamedQuery(name = "EmRoc.findByDateeffective", query = "SELECT e FROM EmRoc e WHERE e.dateeffective = :dateeffective"),
    @NamedQuery(name = "EmRoc.findByRchange", query = "SELECT e FROM EmRoc e WHERE e.rchange = :rchange"),
    @NamedQuery(name = "EmRoc.findByAmount", query = "SELECT e FROM EmRoc e WHERE e.amount = :amount"),
    @NamedQuery(name = "EmRoc.findByRemark", query = "SELECT e FROM EmRoc e WHERE e.remark = :remark")})
public class EmRoc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "refer")
    private String refer;
    @Size(max = 20)
    @Column(name = "fileno")
    private String fileno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "staffid")
    private String staffid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "staffname")
    private String staffname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "staffdesignation")
    private String staffdesignation;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "dateeffective")
    private String dateeffective;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "rchange")
    private String rchange;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Size(max = 300)
    @Column(name = "remark")
    private String remark;

    public EmRoc() {
    }

    public EmRoc(String refer) {
        this.refer = refer;
    }

    public EmRoc(String refer, String staffid, String staffname, String staffdesignation, String dateeffective, String rchange, double amount) {
        this.refer = refer;
        this.staffid = staffid;
        this.staffname = staffname;
        this.staffdesignation = staffdesignation;
        this.dateeffective = dateeffective;
        this.rchange = rchange;
        this.amount = amount;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getFileno() {
        return fileno;
    }

    public void setFileno(String fileno) {
        this.fileno = fileno;
    }

    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    public String getStaffname() {
        return staffname;
    }

    public void setStaffname(String staffname) {
        this.staffname = staffname;
    }

    public String getStaffdesignation() {
        return staffdesignation;
    }

    public void setStaffdesignation(String staffdesignation) {
        this.staffdesignation = staffdesignation;
    }

    public String getDateeffective() {
        return dateeffective;
    }

    public void setDateeffective(String dateeffective) {
        this.dateeffective = dateeffective;
    }

    public String getRchange() {
        return rchange;
    }

    public void setRchange(String rchange) {
        this.rchange = rchange;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmRoc)) {
            return false;
        }
        EmRoc other = (EmRoc) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.employee.EmRoc[ refer=" + refer + " ]";
    }
    
}
