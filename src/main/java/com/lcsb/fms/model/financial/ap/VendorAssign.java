/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ap;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ap_vendor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VendorAssign.findAll", query = "SELECT v FROM VendorAssign v"),
    @NamedQuery(name = "VendorAssign.findBySuppcode", query = "SELECT v FROM VendorAssign v WHERE v.suppcode = :suppcode"),
    @NamedQuery(name = "VendorAssign.findBySuppname", query = "SELECT v FROM VendorAssign v WHERE v.suppname = :suppname"),
    @NamedQuery(name = "VendorAssign.findByCompanycode", query = "SELECT v FROM VendorAssign v WHERE v.companycode = :companycode"),
    @NamedQuery(name = "VendorAssign.findByAcdesc", query = "SELECT v FROM VendorAssign v WHERE v.acdesc = :acdesc"),
    @NamedQuery(name = "VendorAssign.findByAccode", query = "SELECT v FROM VendorAssign v WHERE v.accode = :accode"),
    @NamedQuery(name = "VendorAssign.findByCompanyname", query = "SELECT v FROM VendorAssign v WHERE v.companyname = :companyname"),
    @NamedQuery(name = "VendorAssign.findByGstid", query = "SELECT v FROM VendorAssign v WHERE v.gstid = :gstid")})
public class VendorAssign implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "suppcode")
    private String suppcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "suppname")
    private String suppname;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "suppaddress")
    private String suppaddress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "companycode")
    private String companycode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "acdesc")
    private String acdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "accode")
    private String accode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "companyname")
    private String companyname;
    @Size(max = 20)
    @Column(name = "gstid")
    private String gstid;

    public VendorAssign() {
    }

    public VendorAssign(String suppcode) {
        this.suppcode = suppcode;
    }

    public VendorAssign(String suppcode, String suppname, String suppaddress, String companycode, String acdesc, String accode, String companyname) {
        this.suppcode = suppcode;
        this.suppname = suppname;
        this.suppaddress = suppaddress;
        this.companycode = companycode;
        this.acdesc = acdesc;
        this.accode = accode;
        this.companyname = companyname;
    }

    public String getSuppcode() {
        return suppcode;
    }

    public void setSuppcode(String suppcode) {
        this.suppcode = suppcode;
    }

    public String getSuppname() {
        return suppname;
    }

    public void setSuppname(String suppname) {
        this.suppname = suppname;
    }

    public String getSuppaddress() {
        return suppaddress;
    }

    public void setSuppaddress(String suppaddress) {
        this.suppaddress = suppaddress;
    }

    public String getCompanycode() {
        return companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public String getAcdesc() {
        return acdesc;
    }

    public void setAcdesc(String acdesc) {
        this.acdesc = acdesc;
    }

    public String getAccode() {
        return accode;
    }

    public void setAccode(String accode) {
        this.accode = accode;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getGstid() {
        return gstid;
    }

    public void setGstid(String gstid) {
        this.gstid = gstid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (suppcode != null ? suppcode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VendorAssign)) {
            return false;
        }
        VendorAssign other = (VendorAssign) object;
        if ((this.suppcode == null && other.suppcode != null) || (this.suppcode != null && !this.suppcode.equals(other.suppcode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.ap.VendorAssign[ suppcode=" + suppcode + " ]";
    }
    
}
