/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "cb_bankreconcil")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BankReconcile.findAll", query = "SELECT b FROM BankReconcile b"),
    @NamedQuery(name = "BankReconcile.findByCodebank", query = "SELECT b FROM BankReconcile b WHERE b.bankReconcilePK.codebank = :codebank"),
    @NamedQuery(name = "BankReconcile.findByYear", query = "SELECT b FROM BankReconcile b WHERE b.bankReconcilePK.year = :year"),
    @NamedQuery(name = "BankReconcile.findByPeriod", query = "SELECT b FROM BankReconcile b WHERE b.bankReconcilePK.period = :period"),
    @NamedQuery(name = "BankReconcile.findByA", query = "SELECT b FROM BankReconcile b WHERE b.a = :a"),
    @NamedQuery(name = "BankReconcile.findByB", query = "SELECT b FROM BankReconcile b WHERE b.b = :b"),
    @NamedQuery(name = "BankReconcile.findByC", query = "SELECT b FROM BankReconcile b WHERE b.c = :c"),
    @NamedQuery(name = "BankReconcile.findByD", query = "SELECT b FROM BankReconcile b WHERE b.d = :d"),
    @NamedQuery(name = "BankReconcile.findByE", query = "SELECT b FROM BankReconcile b WHERE b.e = :e"),
    @NamedQuery(name = "BankReconcile.findByF", query = "SELECT b FROM BankReconcile b WHERE b.f = :f"),
    @NamedQuery(name = "BankReconcile.findByPid", query = "SELECT b FROM BankReconcile b WHERE b.pid = :pid"),
    @NamedQuery(name = "BankReconcile.findByPname", query = "SELECT b FROM BankReconcile b WHERE b.pname = :pname"),
    @NamedQuery(name = "BankReconcile.findByPdate", query = "SELECT b FROM BankReconcile b WHERE b.pdate = :pdate"),
    @NamedQuery(name = "BankReconcile.findByCid", query = "SELECT b FROM BankReconcile b WHERE b.cid = :cid"),
    @NamedQuery(name = "BankReconcile.findByCname", query = "SELECT b FROM BankReconcile b WHERE b.cname = :cname"),
    @NamedQuery(name = "BankReconcile.findByCdate", query = "SELECT b FROM BankReconcile b WHERE b.cdate = :cdate"),
    @NamedQuery(name = "BankReconcile.findByAid", query = "SELECT b FROM BankReconcile b WHERE b.aid = :aid"),
    @NamedQuery(name = "BankReconcile.findByAname", query = "SELECT b FROM BankReconcile b WHERE b.aname = :aname"),
    @NamedQuery(name = "BankReconcile.findByAdesignation", query = "SELECT b FROM BankReconcile b WHERE b.adesignation = :adesignation"),
    @NamedQuery(name = "BankReconcile.findByAdate", query = "SELECT b FROM BankReconcile b WHERE b.adate = :adate"),
    @NamedQuery(name = "BankReconcile.findByEstatecode", query = "SELECT b FROM BankReconcile b WHERE b.estatecode = :estatecode"),
    @NamedQuery(name = "BankReconcile.findByEstatename", query = "SELECT b FROM BankReconcile b WHERE b.estatename = :estatename")})
public class BankReconcile implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BankReconcilePK bankReconcilePK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "a")
    private double a;
    @Basic(optional = false)
    @NotNull
    @Column(name = "b")
    private double b;
    @Basic(optional = false)
    @NotNull
    @Column(name = "c")
    private double c;
    @Basic(optional = false)
    @NotNull
    @Column(name = "d")
    private double d;
    @Basic(optional = false)
    @NotNull
    @Column(name = "e")
    private double e;
    @Basic(optional = false)
    @NotNull
    @Column(name = "f")
    private double f;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pid")
    private String pid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pname")
    private String pname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pdate")
    private String pdate;
    @Size(max = 100)
    @Column(name = "cid")
    private String cid;
    @Size(max = 100)
    @Column(name = "cname")
    private String cname;
    @Size(max = 100)
    @Column(name = "cdate")
    private String cdate;
    @Size(max = 100)
    @Column(name = "aid")
    private String aid;
    @Size(max = 100)
    @Column(name = "aname")
    private String aname;
    @Size(max = 100)
    @Column(name = "adesignation")
    private String adesignation;
    @Size(max = 100)
    @Column(name = "adate")
    private String adate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatecode")
    private String estatecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatename")
    private String estatename;

    public BankReconcile() {
    }

    public BankReconcile(BankReconcilePK bankReconcilePK) {
        this.bankReconcilePK = bankReconcilePK;
    }

    public BankReconcile(BankReconcilePK bankReconcilePK, double a, double b, double c, double d, double e, double f, String pid, String pname, String pdate, String estatecode, String estatename) {
        this.bankReconcilePK = bankReconcilePK;
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.pid = pid;
        this.pname = pname;
        this.pdate = pdate;
        this.estatecode = estatecode;
        this.estatename = estatename;
    }

    public BankReconcile(String codebank, String year, String period) {
        this.bankReconcilePK = new BankReconcilePK(codebank, year, period);
    }

    public BankReconcilePK getBankReconcilePK() {
        return bankReconcilePK;
    }

    public void setBankReconcilePK(BankReconcilePK bankReconcilePK) {
        this.bankReconcilePK = bankReconcilePK;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }

    public double getE() {
        return e;
    }

    public void setE(double e) {
        this.e = e;
    }

    public double getF() {
        return f;
    }

    public void setF(double f) {
        this.f = f;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPdate() {
        return pdate;
    }

    public void setPdate(String pdate) {
        this.pdate = pdate;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCdate() {
        return cdate;
    }

    public void setCdate(String cdate) {
        this.cdate = cdate;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getAname() {
        return aname;
    }

    public void setAname(String aname) {
        this.aname = aname;
    }

    public String getAdesignation() {
        return adesignation;
    }

    public void setAdesignation(String adesignation) {
        this.adesignation = adesignation;
    }

    public String getAdate() {
        return adate;
    }

    public void setAdate(String adate) {
        this.adate = adate;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getEstatename() {
        return estatename;
    }

    public void setEstatename(String estatename) {
        this.estatename = estatename;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bankReconcilePK != null ? bankReconcilePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BankReconcile)) {
            return false;
        }
        BankReconcile other = (BankReconcile) object;
        if ((this.bankReconcilePK == null && other.bankReconcilePK != null) || (this.bankReconcilePK != null && !this.bankReconcilePK.equals(other.bankReconcilePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.cashbook.bankreport.reconcile.BankReconcile[ bankReconcilePK=" + bankReconcilePK + " ]";
    }
    
}
