/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.model.financial.gl;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dell
 */
@Entity
@Table(name = "gl_altaccountingperiod")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccountPeriod.findAll", query = "SELECT a FROM AccountPeriod a"),
    @NamedQuery(name = "AccountPeriod.findByPeriod", query = "SELECT a FROM AccountPeriod a WHERE a.period = :period"),
    @NamedQuery(name = "AccountPeriod.findByYear", query = "SELECT a FROM AccountPeriod a WHERE a.year = :year"),
    @NamedQuery(name = "AccountPeriod.findByStartperiod", query = "SELECT a FROM AccountPeriod a WHERE a.startperiod = :startperiod"),
    @NamedQuery(name = "AccountPeriod.findByEndperiod", query = "SELECT a FROM AccountPeriod a WHERE a.endperiod = :endperiod"),
    @NamedQuery(name = "AccountPeriod.findByCurrent", query = "SELECT a FROM AccountPeriod a WHERE a.current = :current"),
    @NamedQuery(name = "AccountPeriod.findByStartyear", query = "SELECT a FROM AccountPeriod a WHERE a.startyear = :startyear"),
    @NamedQuery(name = "AccountPeriod.findByEndyear", query = "SELECT a FROM AccountPeriod a WHERE a.endyear = :endyear"),
    @NamedQuery(name = "AccountPeriod.findByMonth", query = "SELECT a FROM AccountPeriod a WHERE a.month = :month"),
    @NamedQuery(name = "AccountPeriod.findByClosedate", query = "SELECT a FROM AccountPeriod a WHERE a.closedate = :closedate"),
    @NamedQuery(name = "AccountPeriod.findById", query = "SELECT a FROM AccountPeriod a WHERE a.id = :id")})
public class AccountPeriod implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "period")
    private int period;
    @Basic(optional = false)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @Column(name = "startperiod")
    private String startperiod;
    @Basic(optional = false)
    @Column(name = "endperiod")
    private String endperiod;
    @Basic(optional = false)
    @Column(name = "current")
    private String current;
    @Basic(optional = false)
    @Column(name = "startyear")
    private String startyear;
    @Basic(optional = false)
    @Column(name = "endyear")
    private String endyear;
    @Basic(optional = false)
    @Column(name = "month")
    private String month;
    @Basic(optional = false)
    @Column(name = "closedate")
    private String closedate;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    public AccountPeriod() {
    }

    public AccountPeriod(Integer id) {
        this.id = id;
    }

    public AccountPeriod(Integer id, int period, String year, String startperiod, String endperiod, String current, String startyear, String endyear, String month, String closedate) {
        this.id = id;
        this.period = period;
        this.year = year;
        this.startperiod = startperiod;
        this.endperiod = endperiod;
        this.current = current;
        this.startyear = startyear;
        this.endyear = endyear;
        this.month = month;
        this.closedate = closedate;
    }
    
    private List<Period> listPeriod;    

    public List<Period> getListPeriod() {
        return listPeriod;
    }

    public void setListPeriod(List<Period> listPeriod) {
        this.listPeriod = listPeriod;
    }
    
    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getStartperiod() {
        return startperiod;
    }

    public void setStartperiod(String startperiod) {
        this.startperiod = startperiod;
    }

    public String getEndperiod() {
        return endperiod;
    }

    public void setEndperiod(String endperiod) {
        this.endperiod = endperiod;
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public String getStartyear() {
        return startyear;
    }

    public void setStartyear(String startyear) {
        this.startyear = startyear;
    }

    public String getEndyear() {
        return endyear;
    }

    public void setEndyear(String endyear) {
        this.endyear = endyear;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getClosedate() {
        return closedate;
    }

    public void setClosedate(String closedate) {
        this.closedate = closedate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccountPeriod)) {
            return false;
        }
        AccountPeriod other = (AccountPeriod) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fms.model.financial.gl.AccountPeriod[ id=" + id + " ]";
    }
    
}
