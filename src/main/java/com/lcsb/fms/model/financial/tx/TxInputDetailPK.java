/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.tx;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author fadhilfahmi
 */
@Embeddable
public class TxInputDetailPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcode")
    private String taxcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "voucherno")
    private String voucherno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refno")
    private String refno;

    public TxInputDetailPK() {
    }

    public TxInputDetailPK(String refer, String taxcode, String voucherno, String refno) {
        this.refer = refer;
        this.taxcode = taxcode;
        this.voucherno = voucherno;
        this.refno = refno;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getTaxcode() {
        return taxcode;
    }

    public void setTaxcode(String taxcode) {
        this.taxcode = taxcode;
    }

    public String getVoucherno() {
        return voucherno;
    }

    public void setVoucherno(String voucherno) {
        this.voucherno = voucherno;
    }

    public String getRefno() {
        return refno;
    }

    public void setRefno(String refno) {
        this.refno = refno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        hash += (taxcode != null ? taxcode.hashCode() : 0);
        hash += (voucherno != null ? voucherno.hashCode() : 0);
        hash += (refno != null ? refno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TxInputDetailPK)) {
            return false;
        }
        TxInputDetailPK other = (TxInputDetailPK) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        if ((this.taxcode == null && other.taxcode != null) || (this.taxcode != null && !this.taxcode.equals(other.taxcode))) {
            return false;
        }
        if ((this.voucherno == null && other.voucherno != null) || (this.voucherno != null && !this.voucherno.equals(other.voucherno))) {
            return false;
        }
        if ((this.refno == null && other.refno != null) || (this.refno != null && !this.refno.equals(other.refno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.tx.TxInputDetailPK[ refer=" + refer + ", taxcode=" + taxcode + ", voucherno=" + voucherno + ", refno=" + refno + " ]";
    }
    
}
