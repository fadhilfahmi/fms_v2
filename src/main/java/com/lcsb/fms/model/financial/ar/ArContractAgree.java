/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ar;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "sl_contract_aggree")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ArContractAgree.findAll", query = "SELECT a FROM ArContractAgree a"),
    @NamedQuery(name = "ArContractAgree.findByBuyercode", query = "SELECT a FROM ArContractAgree a WHERE a.buyercode = :buyercode"),
    @NamedQuery(name = "ArContractAgree.findByNo", query = "SELECT a FROM ArContractAgree a WHERE a.no = :no"),
    @NamedQuery(name = "ArContractAgree.findByLoccode", query = "SELECT a FROM ArContractAgree a WHERE a.loccode = :loccode"),
    @NamedQuery(name = "ArContractAgree.findByLocname", query = "SELECT a FROM ArContractAgree a WHERE a.locname = :locname"),
    @NamedQuery(name = "ArContractAgree.findByDestination", query = "SELECT a FROM ArContractAgree a WHERE a.destination = :destination"),
    @NamedQuery(name = "ArContractAgree.findByDate", query = "SELECT a FROM ArContractAgree a WHERE a.date = :date"),
    @NamedQuery(name = "ArContractAgree.findByComcd", query = "SELECT a FROM ArContractAgree a WHERE a.comcd = :comcd"),
    @NamedQuery(name = "ArContractAgree.findByComde", query = "SELECT a FROM ArContractAgree a WHERE a.comde = :comde"),
    @NamedQuery(name = "ArContractAgree.findByComacc", query = "SELECT a FROM ArContractAgree a WHERE a.comacc = :comacc"),
    @NamedQuery(name = "ArContractAgree.findByQty", query = "SELECT a FROM ArContractAgree a WHERE a.qty = :qty"),
    @NamedQuery(name = "ArContractAgree.findByMpob", query = "SELECT a FROM ArContractAgree a WHERE a.mpob = :mpob"),
    @NamedQuery(name = "ArContractAgree.findByPrice", query = "SELECT a FROM ArContractAgree a WHERE a.price = :price"),
    @NamedQuery(name = "ArContractAgree.findByDmonth", query = "SELECT a FROM ArContractAgree a WHERE a.dmonth = :dmonth"),
    @NamedQuery(name = "ArContractAgree.findByDyear", query = "SELECT a FROM ArContractAgree a WHERE a.dyear = :dyear"),
    @NamedQuery(name = "ArContractAgree.findByBrokercd", query = "SELECT a FROM ArContractAgree a WHERE a.brokercd = :brokercd"),
    @NamedQuery(name = "ArContractAgree.findByActive", query = "SELECT a FROM ArContractAgree a WHERE a.active = :active"),
    @NamedQuery(name = "ArContractAgree.findByBrokernum", query = "SELECT a FROM ArContractAgree a WHERE a.brokernum = :brokernum"),
    @NamedQuery(name = "ArContractAgree.findByBrokerde", query = "SELECT a FROM ArContractAgree a WHERE a.brokerde = :brokerde"),
    @NamedQuery(name = "ArContractAgree.findByEffectivedate", query = "SELECT a FROM ArContractAgree a WHERE a.effectivedate = :effectivedate"),
    @NamedQuery(name = "ArContractAgree.findById", query = "SELECT a FROM ArContractAgree a WHERE a.id = :id")})
public class ArContractAgree implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "buyercode")
    private String buyercode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "no")
    private String no;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loccode")
    private String loccode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "locname")
    private String locname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "destination")
    private String destination;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "comcd")
    private String comcd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "comde")
    private String comde;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "comacc")
    private String comacc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "qty")
    private double qty;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "mpob")
    private String mpob;
    @Basic(optional = false)
    @NotNull
    @Column(name = "price")
    private double price;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "dmonth")
    private String dmonth;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "dyear")
    private String dyear;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "brokercd")
    private String brokercd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "active")
    private String active;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "brokernum")
    private String brokernum;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "brokerde")
    private String brokerde;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "subgrp")
    private String subgrp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "effectivedate")
    @Temporal(TemporalType.DATE)
    private Date effectivedate;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    public ArContractAgree() {
    }

    public ArContractAgree(Integer id) {
        this.id = id;
    }

    public ArContractAgree(Integer id, String buyercode, String no, String loccode, String locname, String destination, Date date, String comcd, String comde, String comacc, double qty, String mpob, double price, String dmonth, String dyear, String brokercd, String active, String brokernum, String brokerde, String subgrp, Date effectivedate) {
        this.id = id;
        this.buyercode = buyercode;
        this.no = no;
        this.loccode = loccode;
        this.locname = locname;
        this.destination = destination;
        this.date = date;
        this.comcd = comcd;
        this.comde = comde;
        this.comacc = comacc;
        this.qty = qty;
        this.mpob = mpob;
        this.price = price;
        this.dmonth = dmonth;
        this.dyear = dyear;
        this.brokercd = brokercd;
        this.active = active;
        this.brokernum = brokernum;
        this.brokerde = brokerde;
        this.subgrp = subgrp;
        this.effectivedate = effectivedate;
    }

    public String getBuyercode() {
        return buyercode;
    }

    public void setBuyercode(String buyercode) {
        this.buyercode = buyercode;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLocname() {
        return locname;
    }

    public void setLocname(String locname) {
        this.locname = locname;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getComcd() {
        return comcd;
    }

    public void setComcd(String comcd) {
        this.comcd = comcd;
    }

    public String getComde() {
        return comde;
    }

    public void setComde(String comde) {
        this.comde = comde;
    }

    public String getComacc() {
        return comacc;
    }

    public void setComacc(String comacc) {
        this.comacc = comacc;
    }

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }

    public String getMpob() {
        return mpob;
    }

    public void setMpob(String mpob) {
        this.mpob = mpob;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDmonth() {
        return dmonth;
    }

    public void setDmonth(String dmonth) {
        this.dmonth = dmonth;
    }

    public String getDyear() {
        return dyear;
    }

    public void setDyear(String dyear) {
        this.dyear = dyear;
    }

    public String getBrokercd() {
        return brokercd;
    }

    public void setBrokercd(String brokercd) {
        this.brokercd = brokercd;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getBrokernum() {
        return brokernum;
    }

    public void setBrokernum(String brokernum) {
        this.brokernum = brokernum;
    }

    public String getBrokerde() {
        return brokerde;
    }

    public void setBrokerde(String brokerde) {
        this.brokerde = brokerde;
    }

    public String getSubgrp() {
        return subgrp;
    }

    public void setSubgrp(String subgrp) {
        this.subgrp = subgrp;
    }

    public Date getEffectivedate() {
        return effectivedate;
    }

    public void setEffectivedate(Date effectivedate) {
        this.effectivedate = effectivedate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArContractAgree)) {
            return false;
        }
        ArContractAgree other = (ArContractAgree) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.ar.ArContractAgree[ id=" + id + " ]";
    }
    
}
