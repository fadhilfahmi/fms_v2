/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

/**
 *
 * @author fadhilfahmi
 */
public class ORTempCredit {
    
    private String referno;
    private double amounttopay;
    private String frommodule;
    private String sessionid;
    private String partycode;

    public String getReferno() {
        return referno;
    }

    public void setReferno(String referno) {
        this.referno = referno;
    }

    public double getAmounttopay() {
        return amounttopay;
    }

    public void setAmounttopay(double amounttopay) {
        this.amounttopay = amounttopay;
    }

    public String getFrommodule() {
        return frommodule;
    }

    public void setFrommodule(String frommodule) {
        this.frommodule = frommodule;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getPartycode() {
        return partycode;
    }

    public void setPartycode(String partycode) {
        this.partycode = partycode;
    }
    
    
    
}
