/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HP
 */
@Entity
@Table(name = "cb_official_credit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OfficialCreditItem.findAll", query = "SELECT o FROM OfficialCreditItem o"),
    @NamedQuery(name = "OfficialCreditItem.findByVoucherno", query = "SELECT o FROM OfficialCreditItem o WHERE o.voucherno = :voucherno"),
    @NamedQuery(name = "OfficialCreditItem.findByCoacode", query = "SELECT o FROM OfficialCreditItem o WHERE o.coacode = :coacode"),
    @NamedQuery(name = "OfficialCreditItem.findByCoadescp", query = "SELECT o FROM OfficialCreditItem o WHERE o.coadescp = :coadescp"),
    @NamedQuery(name = "OfficialCreditItem.findByLoclevel", query = "SELECT o FROM OfficialCreditItem o WHERE o.loclevel = :loclevel"),
    @NamedQuery(name = "OfficialCreditItem.findByLoccode", query = "SELECT o FROM OfficialCreditItem o WHERE o.loccode = :loccode"),
    @NamedQuery(name = "OfficialCreditItem.findByLocname", query = "SELECT o FROM OfficialCreditItem o WHERE o.locname = :locname"),
    @NamedQuery(name = "OfficialCreditItem.findByChargetype", query = "SELECT o FROM OfficialCreditItem o WHERE o.chargetype = :chargetype"),
    @NamedQuery(name = "OfficialCreditItem.findByChargecode", query = "SELECT o FROM OfficialCreditItem o WHERE o.chargecode = :chargecode"),
    @NamedQuery(name = "OfficialCreditItem.findByChargedescp", query = "SELECT o FROM OfficialCreditItem o WHERE o.chargedescp = :chargedescp"),
    @NamedQuery(name = "OfficialCreditItem.findByAmount", query = "SELECT o FROM OfficialCreditItem o WHERE o.amount = :amount"),
    @NamedQuery(name = "OfficialCreditItem.findByRefer", query = "SELECT o FROM OfficialCreditItem o WHERE o.refer = :refer"),
    @NamedQuery(name = "OfficialCreditItem.findBySatype", query = "SELECT o FROM OfficialCreditItem o WHERE o.satype = :satype"),
    @NamedQuery(name = "OfficialCreditItem.findBySacode", query = "SELECT o FROM OfficialCreditItem o WHERE o.sacode = :sacode"),
    @NamedQuery(name = "OfficialCreditItem.findBySadesc", query = "SELECT o FROM OfficialCreditItem o WHERE o.sadesc = :sadesc"),
    @NamedQuery(name = "OfficialCreditItem.findByTaxcode", query = "SELECT o FROM OfficialCreditItem o WHERE o.taxcode = :taxcode"),
    @NamedQuery(name = "OfficialCreditItem.findByTaxdescp", query = "SELECT o FROM OfficialCreditItem o WHERE o.taxdescp = :taxdescp"),
    @NamedQuery(name = "OfficialCreditItem.findByTaxrate", query = "SELECT o FROM OfficialCreditItem o WHERE o.taxrate = :taxrate"),
    @NamedQuery(name = "OfficialCreditItem.findByTaxamt", query = "SELECT o FROM OfficialCreditItem o WHERE o.taxamt = :taxamt"),
    @NamedQuery(name = "OfficialCreditItem.findByAmtbeforetax", query = "SELECT o FROM OfficialCreditItem o WHERE o.amtbeforetax = :amtbeforetax"),
    @NamedQuery(name = "OfficialCreditItem.findByTaxcoacode", query = "SELECT o FROM OfficialCreditItem o WHERE o.taxcoacode = :taxcoacode"),
    @NamedQuery(name = "OfficialCreditItem.findByTaxcoadescp", query = "SELECT o FROM OfficialCreditItem o WHERE o.taxcoadescp = :taxcoadescp")})
public class OfficialCreditItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "voucherno")
    private String voucherno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coacode")
    private String coacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "coadescp")
    private String coadescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loclevel")
    private String loclevel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loccode")
    private String loccode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "locname")
    private String locname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "chargetype")
    private String chargetype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "chargecode")
    private String chargecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "chargedescp")
    private String chargedescp;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "amount")
    private String amount;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;
    @Size(max = 100)
    @Column(name = "satype")
    private String satype;
    @Size(max = 100)
    @Column(name = "sacode")
    private String sacode;
    @Size(max = 100)
    @Column(name = "sadesc")
    private String sadesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcode")
    private String taxcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxdescp")
    private String taxdescp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxrate")
    private double taxrate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxamt")
    private double taxamt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amtbeforetax")
    private double amtbeforetax;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcoacode")
    private String taxcoacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcoadescp")
    private String taxcoadescp;
    private String invrefno;

    public OfficialCreditItem() {
    }

    public OfficialCreditItem(String refer) {
        this.refer = refer;
    }

    public OfficialCreditItem(String refer, String voucherno, String coacode, String coadescp, String loclevel, String loccode, String locname, String chargetype, String chargecode, String chargedescp, String remarks, String amount, String taxcode, String taxdescp, double taxrate, double taxamt, double amtbeforetax, String taxcoacode, String taxcoadescp, String invrefno) {
        this.refer = refer;
        this.voucherno = voucherno;
        this.coacode = coacode;
        this.coadescp = coadescp;
        this.loclevel = loclevel;
        this.loccode = loccode;
        this.locname = locname;
        this.chargetype = chargetype;
        this.chargecode = chargecode;
        this.chargedescp = chargedescp;
        this.remarks = remarks;
        this.amount = amount;
        this.taxcode = taxcode;
        this.taxdescp = taxdescp;
        this.taxrate = taxrate;
        this.taxamt = taxamt;
        this.amtbeforetax = amtbeforetax;
        this.taxcoacode = taxcoacode;
        this.taxcoadescp = taxcoadescp;
        this.invrefno = invrefno;
    }

    public String getInvrefno() {
        return invrefno;
    }

    public void setInvrefno(String invrefno) {
        this.invrefno = invrefno;
    }

    public String getVoucherno() {
        return voucherno;
    }

    public void setVoucherno(String voucherno) {
        this.voucherno = voucherno;
    }

    public String getCoacode() {
        return coacode;
    }

    public void setCoacode(String coacode) {
        this.coacode = coacode;
    }

    public String getCoadescp() {
        return coadescp;
    }

    public void setCoadescp(String coadescp) {
        this.coadescp = coadescp;
    }

    public String getLoclevel() {
        return loclevel;
    }

    public void setLoclevel(String loclevel) {
        this.loclevel = loclevel;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLocname() {
        return locname;
    }

    public void setLocname(String locname) {
        this.locname = locname;
    }

    public String getChargetype() {
        return chargetype;
    }

    public void setChargetype(String chargetype) {
        this.chargetype = chargetype;
    }

    public String getChargecode() {
        return chargecode;
    }

    public void setChargecode(String chargecode) {
        this.chargecode = chargecode;
    }

    public String getChargedescp() {
        return chargedescp;
    }

    public void setChargedescp(String chargedescp) {
        this.chargedescp = chargedescp;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    public String getTaxcode() {
        return taxcode;
    }

    public void setTaxcode(String taxcode) {
        this.taxcode = taxcode;
    }

    public String getTaxdescp() {
        return taxdescp;
    }

    public void setTaxdescp(String taxdescp) {
        this.taxdescp = taxdescp;
    }

    public double getTaxrate() {
        return taxrate;
    }

    public void setTaxrate(double taxrate) {
        this.taxrate = taxrate;
    }

    public double getTaxamt() {
        return taxamt;
    }

    public void setTaxamt(double taxamt) {
        this.taxamt = taxamt;
    }

    public double getAmtbeforetax() {
        return amtbeforetax;
    }

    public void setAmtbeforetax(double amtbeforetax) {
        this.amtbeforetax = amtbeforetax;
    }

    public String getTaxcoacode() {
        return taxcoacode;
    }

    public void setTaxcoacode(String taxcoacode) {
        this.taxcoacode = taxcoacode;
    }

    public String getTaxcoadescp() {
        return taxcoadescp;
    }

    public void setTaxcoadescp(String taxcoadescp) {
        this.taxcoadescp = taxcoadescp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OfficialCreditItem)) {
            return false;
        }
        OfficialCreditItem other = (OfficialCreditItem) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.ar.OfficialCreditItem[ refer=" + refer + " ]";
    }
    
}
