/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.tx;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "tx_reconcile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TxReconcile.findAll", query = "SELECT t FROM TxReconcile t"),
    @NamedQuery(name = "TxReconcile.findByRefer", query = "SELECT t FROM TxReconcile t WHERE t.txReconcilePK.refer = :refer"),
    @NamedQuery(name = "TxReconcile.findByYear", query = "SELECT t FROM TxReconcile t WHERE t.year = :year"),
    @NamedQuery(name = "TxReconcile.findByPeriod", query = "SELECT t FROM TxReconcile t WHERE t.period = :period"),
    @NamedQuery(name = "TxReconcile.findByTaxperiodfrom", query = "SELECT t FROM TxReconcile t WHERE t.taxperiodfrom = :taxperiodfrom"),
    @NamedQuery(name = "TxReconcile.findByTaxperiodto", query = "SELECT t FROM TxReconcile t WHERE t.taxperiodto = :taxperiodto"),
    @NamedQuery(name = "TxReconcile.findByTaxreturndate", query = "SELECT t FROM TxReconcile t WHERE t.taxreturndate = :taxreturndate"),
    @NamedQuery(name = "TxReconcile.findByName", query = "SELECT t FROM TxReconcile t WHERE t.name = :name"),
    @NamedQuery(name = "TxReconcile.findByDate", query = "SELECT t FROM TxReconcile t WHERE t.date = :date"),
    @NamedQuery(name = "TxReconcile.findByTaxcode", query = "SELECT t FROM TxReconcile t WHERE t.txReconcilePK.taxcode = :taxcode"),
    @NamedQuery(name = "TxReconcile.findByTaxrate", query = "SELECT t FROM TxReconcile t WHERE t.taxrate = :taxrate"),
    @NamedQuery(name = "TxReconcile.findByInamt", query = "SELECT t FROM TxReconcile t WHERE t.inamt = :inamt"),
    @NamedQuery(name = "TxReconcile.findByOutamt", query = "SELECT t FROM TxReconcile t WHERE t.outamt = :outamt"),
    @NamedQuery(name = "TxReconcile.findByInrefno", query = "SELECT t FROM TxReconcile t WHERE t.inrefno = :inrefno"),
    @NamedQuery(name = "TxReconcile.findByOutrefno", query = "SELECT t FROM TxReconcile t WHERE t.outrefno = :outrefno"),
    @NamedQuery(name = "TxReconcile.findByTaxreturn", query = "SELECT t FROM TxReconcile t WHERE t.taxreturn = :taxreturn"),
    @NamedQuery(name = "TxReconcile.findByPreid", query = "SELECT t FROM TxReconcile t WHERE t.preid = :preid"),
    @NamedQuery(name = "TxReconcile.findByPrename", query = "SELECT t FROM TxReconcile t WHERE t.prename = :prename"),
    @NamedQuery(name = "TxReconcile.findByPredate", query = "SELECT t FROM TxReconcile t WHERE t.predate = :predate"),
    @NamedQuery(name = "TxReconcile.findByPredesign", query = "SELECT t FROM TxReconcile t WHERE t.predesign = :predesign"),
    @NamedQuery(name = "TxReconcile.findByCheckid", query = "SELECT t FROM TxReconcile t WHERE t.checkid = :checkid"),
    @NamedQuery(name = "TxReconcile.findByCheckname", query = "SELECT t FROM TxReconcile t WHERE t.checkname = :checkname"),
    @NamedQuery(name = "TxReconcile.findByCheckdate", query = "SELECT t FROM TxReconcile t WHERE t.checkdate = :checkdate"),
    @NamedQuery(name = "TxReconcile.findByCheckdesign", query = "SELECT t FROM TxReconcile t WHERE t.checkdesign = :checkdesign"),
    @NamedQuery(name = "TxReconcile.findByAppid", query = "SELECT t FROM TxReconcile t WHERE t.appid = :appid"),
    @NamedQuery(name = "TxReconcile.findByAppname", query = "SELECT t FROM TxReconcile t WHERE t.appname = :appname"),
    @NamedQuery(name = "TxReconcile.findByAppdate", query = "SELECT t FROM TxReconcile t WHERE t.appdate = :appdate"),
    @NamedQuery(name = "TxReconcile.findByAppdesign", query = "SELECT t FROM TxReconcile t WHERE t.appdesign = :appdesign"),
    @NamedQuery(name = "TxReconcile.findByLoccode", query = "SELECT t FROM TxReconcile t WHERE t.loccode = :loccode"),
    @NamedQuery(name = "TxReconcile.findByLocdesc", query = "SELECT t FROM TxReconcile t WHERE t.locdesc = :locdesc"),
    @NamedQuery(name = "TxReconcile.findByPostgst", query = "SELECT t FROM TxReconcile t WHERE t.postgst = :postgst"),
    @NamedQuery(name = "TxReconcile.findByLockedby", query = "SELECT t FROM TxReconcile t WHERE t.lockedby = :lockedby"),
    @NamedQuery(name = "TxReconcile.findByLockeddesc", query = "SELECT t FROM TxReconcile t WHERE t.lockeddesc = :lockeddesc"),
    @NamedQuery(name = "TxReconcile.findByLockeddate", query = "SELECT t FROM TxReconcile t WHERE t.lockeddate = :lockeddate"),
    @NamedQuery(name = "TxReconcile.findByTaxcoacode", query = "SELECT t FROM TxReconcile t WHERE t.taxcoacode = :taxcoacode"),
    @NamedQuery(name = "TxReconcile.findByTaxcoadesc", query = "SELECT t FROM TxReconcile t WHERE t.taxcoadesc = :taxcoadesc"),
    @NamedQuery(name = "TxReconcile.findByAmount", query = "SELECT t FROM TxReconcile t WHERE t.amount = :amount"),
    @NamedQuery(name = "TxReconcile.findByBaddebt", query = "SELECT t FROM TxReconcile t WHERE t.baddebt = :baddebt"),
    @NamedQuery(name = "TxReconcile.findByRefno", query = "SELECT t FROM TxReconcile t WHERE t.refno = :refno")})
public class TxReconcile implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TxReconcilePK txReconcilePK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "year")
    private int year;
    @Basic(optional = false)
    @NotNull
    @Column(name = "period")
    private int period;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxperiodfrom")
    @Temporal(TemporalType.DATE)
    private Date taxperiodfrom;
    @Column(name = "taxperiodto")
    @Temporal(TemporalType.DATE)
    private Date taxperiodto;
    @Column(name = "taxreturndate")
    @Temporal(TemporalType.DATE)
    private Date taxreturndate;
    @Size(max = 200)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "taxdescp")
    private String taxdescp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxrate")
    private double taxrate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "inamt")
    private double inamt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "outamt")
    private double outamt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "inrefno")
    private String inrefno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "outrefno")
    private String outrefno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxreturn")
    private double taxreturn;
    @Size(max = 100)
    @Column(name = "preid")
    private String preid;
    @Size(max = 100)
    @Column(name = "prename")
    private String prename;
    @Column(name = "predate")
    @Temporal(TemporalType.DATE)
    private Date predate;
    @Size(max = 100)
    @Column(name = "predesign")
    private String predesign;
    @Size(max = 100)
    @Column(name = "checkid")
    private String checkid;
    @Size(max = 100)
    @Column(name = "checkname")
    private String checkname;
    @Column(name = "checkdate")
    @Temporal(TemporalType.DATE)
    private Date checkdate;
    @Size(max = 100)
    @Column(name = "checkdesign")
    private String checkdesign;
    @Size(max = 100)
    @Column(name = "appid")
    private String appid;
    @Size(max = 100)
    @Column(name = "appname")
    private String appname;
    @Column(name = "appdate")
    @Temporal(TemporalType.DATE)
    private Date appdate;
    @Size(max = 100)
    @Column(name = "appdesign")
    private String appdesign;
    @Size(max = 10)
    @Column(name = "loccode")
    private String loccode;
    @Size(max = 100)
    @Column(name = "locdesc")
    private String locdesc;
    @Size(max = 100)
    @Column(name = "postgst")
    private String postgst;
    @Size(max = 100)
    @Column(name = "lockedby")
    private String lockedby;
    @Size(max = 100)
    @Column(name = "lockeddesc")
    private String lockeddesc;
    @Column(name = "lockeddate")
    @Temporal(TemporalType.DATE)
    private Date lockeddate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcoacode")
    private String taxcoacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "taxcoadesc")
    private String taxcoadesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Size(max = 100)
    @Column(name = "baddebt")
    private String baddebt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refno")
    private String refno;

    public TxReconcile() {
    }

    public TxReconcile(TxReconcilePK txReconcilePK) {
        this.txReconcilePK = txReconcilePK;
    }

    public TxReconcile(TxReconcilePK txReconcilePK, int year, int period, Date taxperiodfrom, Date date, String taxdescp, double taxrate, double inamt, double outamt, String inrefno, String outrefno, double taxreturn, String taxcoacode, String taxcoadesc, double amount, String refno) {
        this.txReconcilePK = txReconcilePK;
        this.year = year;
        this.period = period;
        this.taxperiodfrom = taxperiodfrom;
        this.date = date;
        this.taxdescp = taxdescp;
        this.taxrate = taxrate;
        this.inamt = inamt;
        this.outamt = outamt;
        this.inrefno = inrefno;
        this.outrefno = outrefno;
        this.taxreturn = taxreturn;
        this.taxcoacode = taxcoacode;
        this.taxcoadesc = taxcoadesc;
        this.amount = amount;
        this.refno = refno;
    }

    public TxReconcile(String refer, String taxcode) {
        this.txReconcilePK = new TxReconcilePK(refer, taxcode);
    }

    public TxReconcilePK getTxReconcilePK() {
        return txReconcilePK;
    }

    public void setTxReconcilePK(TxReconcilePK txReconcilePK) {
        this.txReconcilePK = txReconcilePK;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public Date getTaxperiodfrom() {
        return taxperiodfrom;
    }

    public void setTaxperiodfrom(Date taxperiodfrom) {
        this.taxperiodfrom = taxperiodfrom;
    }

    public Date getTaxperiodto() {
        return taxperiodto;
    }

    public void setTaxperiodto(Date taxperiodto) {
        this.taxperiodto = taxperiodto;
    }

    public Date getTaxreturndate() {
        return taxreturndate;
    }

    public void setTaxreturndate(Date taxreturndate) {
        this.taxreturndate = taxreturndate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTaxdescp() {
        return taxdescp;
    }

    public void setTaxdescp(String taxdescp) {
        this.taxdescp = taxdescp;
    }

    public double getTaxrate() {
        return taxrate;
    }

    public void setTaxrate(double taxrate) {
        this.taxrate = taxrate;
    }

    public double getInamt() {
        return inamt;
    }

    public void setInamt(double inamt) {
        this.inamt = inamt;
    }

    public double getOutamt() {
        return outamt;
    }

    public void setOutamt(double outamt) {
        this.outamt = outamt;
    }

    public String getInrefno() {
        return inrefno;
    }

    public void setInrefno(String inrefno) {
        this.inrefno = inrefno;
    }

    public String getOutrefno() {
        return outrefno;
    }

    public void setOutrefno(String outrefno) {
        this.outrefno = outrefno;
    }

    public double getTaxreturn() {
        return taxreturn;
    }

    public void setTaxreturn(double taxreturn) {
        this.taxreturn = taxreturn;
    }

    public String getPreid() {
        return preid;
    }

    public void setPreid(String preid) {
        this.preid = preid;
    }

    public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        this.prename = prename;
    }

    public Date getPredate() {
        return predate;
    }

    public void setPredate(Date predate) {
        this.predate = predate;
    }

    public String getPredesign() {
        return predesign;
    }

    public void setPredesign(String predesign) {
        this.predesign = predesign;
    }

    public String getCheckid() {
        return checkid;
    }

    public void setCheckid(String checkid) {
        this.checkid = checkid;
    }

    public String getCheckname() {
        return checkname;
    }

    public void setCheckname(String checkname) {
        this.checkname = checkname;
    }

    public Date getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(Date checkdate) {
        this.checkdate = checkdate;
    }

    public String getCheckdesign() {
        return checkdesign;
    }

    public void setCheckdesign(String checkdesign) {
        this.checkdesign = checkdesign;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public Date getAppdate() {
        return appdate;
    }

    public void setAppdate(Date appdate) {
        this.appdate = appdate;
    }

    public String getAppdesign() {
        return appdesign;
    }

    public void setAppdesign(String appdesign) {
        this.appdesign = appdesign;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLocdesc() {
        return locdesc;
    }

    public void setLocdesc(String locdesc) {
        this.locdesc = locdesc;
    }

    public String getPostgst() {
        return postgst;
    }

    public void setPostgst(String postgst) {
        this.postgst = postgst;
    }

    public String getLockedby() {
        return lockedby;
    }

    public void setLockedby(String lockedby) {
        this.lockedby = lockedby;
    }

    public String getLockeddesc() {
        return lockeddesc;
    }

    public void setLockeddesc(String lockeddesc) {
        this.lockeddesc = lockeddesc;
    }

    public Date getLockeddate() {
        return lockeddate;
    }

    public void setLockeddate(Date lockeddate) {
        this.lockeddate = lockeddate;
    }

    public String getTaxcoacode() {
        return taxcoacode;
    }

    public void setTaxcoacode(String taxcoacode) {
        this.taxcoacode = taxcoacode;
    }

    public String getTaxcoadesc() {
        return taxcoadesc;
    }

    public void setTaxcoadesc(String taxcoadesc) {
        this.taxcoadesc = taxcoadesc;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getBaddebt() {
        return baddebt;
    }

    public void setBaddebt(String baddebt) {
        this.baddebt = baddebt;
    }

    public String getRefno() {
        return refno;
    }

    public void setRefno(String refno) {
        this.refno = refno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (txReconcilePK != null ? txReconcilePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TxReconcile)) {
            return false;
        }
        TxReconcile other = (TxReconcile) object;
        if ((this.txReconcilePK == null && other.txReconcilePK != null) || (this.txReconcilePK != null && !this.txReconcilePK.equals(other.txReconcilePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.tx.TxReconcile[ txReconcilePK=" + txReconcilePK + " ]";
    }
    
}
