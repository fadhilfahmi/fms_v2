/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ap;

import com.lcsb.fms.model.financial.cashbook.PaymentVoucher;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem;
import java.util.List;

/**
 *
 * @author fadhilfahmi
 */
public class ApPaymentMaster {
    
    private PaymentVoucher master;
    private List<PaymentVoucherItem> listInvoice;

    public PaymentVoucher getMaster() {
        return master;
    }

    public void setMaster(PaymentVoucher master) {
        this.master = master;
    }

    public List<PaymentVoucherItem> getListInvoice() {
        return listInvoice;
    }

    public void setListInvoice(List<PaymentVoucherItem> listInvoice) {
        this.listInvoice = listInvoice;
    }
    
    
    
}
