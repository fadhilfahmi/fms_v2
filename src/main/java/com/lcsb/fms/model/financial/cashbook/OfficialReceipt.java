/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.cashbook;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "cb_official")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OfficialReceipt.findAll", query = "SELECT o FROM OfficialReceipt o"),
    @NamedQuery(name = "OfficialReceipt.findByEstatecode", query = "SELECT o FROM OfficialReceipt o WHERE o.estatecode = :estatecode"),
    @NamedQuery(name = "OfficialReceipt.findByEstatename", query = "SELECT o FROM OfficialReceipt o WHERE o.estatename = :estatename"),
    @NamedQuery(name = "OfficialReceipt.findByEstateaddress", query = "SELECT o FROM OfficialReceipt o WHERE o.estateaddress = :estateaddress"),
    @NamedQuery(name = "OfficialReceipt.findByEstatecity", query = "SELECT o FROM OfficialReceipt o WHERE o.estatecity = :estatecity"),
    @NamedQuery(name = "OfficialReceipt.findByEstatestate", query = "SELECT o FROM OfficialReceipt o WHERE o.estatestate = :estatestate"),
    @NamedQuery(name = "OfficialReceipt.findByEstatepostcode", query = "SELECT o FROM OfficialReceipt o WHERE o.estatepostcode = :estatepostcode"),
    @NamedQuery(name = "OfficialReceipt.findByEstatephone", query = "SELECT o FROM OfficialReceipt o WHERE o.estatephone = :estatephone"),
    @NamedQuery(name = "OfficialReceipt.findByEstatefax", query = "SELECT o FROM OfficialReceipt o WHERE o.estatefax = :estatefax"),
    @NamedQuery(name = "OfficialReceipt.findByYear", query = "SELECT o FROM OfficialReceipt o WHERE o.year = :year"),
    @NamedQuery(name = "OfficialReceipt.findByPeriod", query = "SELECT o FROM OfficialReceipt o WHERE o.period = :period"),
    @NamedQuery(name = "OfficialReceipt.findByBankcode", query = "SELECT o FROM OfficialReceipt o WHERE o.bankcode = :bankcode"),
    @NamedQuery(name = "OfficialReceipt.findByBankname", query = "SELECT o FROM OfficialReceipt o WHERE o.bankname = :bankname"),
    @NamedQuery(name = "OfficialReceipt.findByVoucherno", query = "SELECT o FROM OfficialReceipt o WHERE o.voucherno = :voucherno"),
    @NamedQuery(name = "OfficialReceipt.findByRefer", query = "SELECT o FROM OfficialReceipt o WHERE o.refer = :refer"),
    @NamedQuery(name = "OfficialReceipt.findByDate", query = "SELECT o FROM OfficialReceipt o WHERE o.date = :date"),
    @NamedQuery(name = "OfficialReceipt.findByPaidtype", query = "SELECT o FROM OfficialReceipt o WHERE o.paidtype = :paidtype"),
    @NamedQuery(name = "OfficialReceipt.findByPaidcode", query = "SELECT o FROM OfficialReceipt o WHERE o.paidcode = :paidcode"),
    @NamedQuery(name = "OfficialReceipt.findByPaidaddress", query = "SELECT o FROM OfficialReceipt o WHERE o.paidaddress = :paidaddress"),
    @NamedQuery(name = "OfficialReceipt.findByPaidname", query = "SELECT o FROM OfficialReceipt o WHERE o.paidname = :paidname"),
    @NamedQuery(name = "OfficialReceipt.findByPaidcity", query = "SELECT o FROM OfficialReceipt o WHERE o.paidcity = :paidcity"),
    @NamedQuery(name = "OfficialReceipt.findByPaidstate", query = "SELECT o FROM OfficialReceipt o WHERE o.paidstate = :paidstate"),
    @NamedQuery(name = "OfficialReceipt.findByPaidpostcode", query = "SELECT o FROM OfficialReceipt o WHERE o.paidpostcode = :paidpostcode"),
    @NamedQuery(name = "OfficialReceipt.findByPost", query = "SELECT o FROM OfficialReceipt o WHERE o.post = :post"),
    @NamedQuery(name = "OfficialReceipt.findByPostdate", query = "SELECT o FROM OfficialReceipt o WHERE o.postdate = :postdate"),
    @NamedQuery(name = "OfficialReceipt.findByAmount", query = "SELECT o FROM OfficialReceipt o WHERE o.amount = :amount"),
    @NamedQuery(name = "OfficialReceipt.findByReceivemode", query = "SELECT o FROM OfficialReceipt o WHERE o.receivemode = :receivemode"),
    @NamedQuery(name = "OfficialReceipt.findByAppid", query = "SELECT o FROM OfficialReceipt o WHERE o.appid = :appid"),
    @NamedQuery(name = "OfficialReceipt.findByAppname", query = "SELECT o FROM OfficialReceipt o WHERE o.appname = :appname"),
    @NamedQuery(name = "OfficialReceipt.findByAppposition", query = "SELECT o FROM OfficialReceipt o WHERE o.appposition = :appposition"),
    @NamedQuery(name = "OfficialReceipt.findByPreid", query = "SELECT o FROM OfficialReceipt o WHERE o.preid = :preid"),
    @NamedQuery(name = "OfficialReceipt.findByPrename", query = "SELECT o FROM OfficialReceipt o WHERE o.prename = :prename"),
    @NamedQuery(name = "OfficialReceipt.findByPreposition", query = "SELECT o FROM OfficialReceipt o WHERE o.preposition = :preposition"),
    @NamedQuery(name = "OfficialReceipt.findByFlag", query = "SELECT o FROM OfficialReceipt o WHERE o.flag = :flag"),
    @NamedQuery(name = "OfficialReceipt.findByRm", query = "SELECT o FROM OfficialReceipt o WHERE o.rm = :rm"),
    @NamedQuery(name = "OfficialReceipt.findByStmtyear", query = "SELECT o FROM OfficialReceipt o WHERE o.stmtyear = :stmtyear"),
    @NamedQuery(name = "OfficialReceipt.findByStmtperiod", query = "SELECT o FROM OfficialReceipt o WHERE o.stmtperiod = :stmtperiod"),
    @NamedQuery(name = "OfficialReceipt.findByFlagbank", query = "SELECT o FROM OfficialReceipt o WHERE o.flagbank = :flagbank"),
    @NamedQuery(name = "OfficialReceipt.findByDeposit", query = "SELECT o FROM OfficialReceipt o WHERE o.deposit = :deposit"),
    @NamedQuery(name = "OfficialReceipt.findByPreparedate", query = "SELECT o FROM OfficialReceipt o WHERE o.preparedate = :preparedate"),
    @NamedQuery(name = "OfficialReceipt.findByCheckdate", query = "SELECT o FROM OfficialReceipt o WHERE o.checkdate = :checkdate"),
    @NamedQuery(name = "OfficialReceipt.findByApprovedate", query = "SELECT o FROM OfficialReceipt o WHERE o.approvedate = :approvedate"),
    @NamedQuery(name = "OfficialReceipt.findByCheckid", query = "SELECT o FROM OfficialReceipt o WHERE o.checkid = :checkid"),
    @NamedQuery(name = "OfficialReceipt.findByCheckname", query = "SELECT o FROM OfficialReceipt o WHERE o.checkname = :checkname"),
    @NamedQuery(name = "OfficialReceipt.findByCheckposition", query = "SELECT o FROM OfficialReceipt o WHERE o.checkposition = :checkposition"),
    @NamedQuery(name = "OfficialReceipt.findByNoagree", query = "SELECT o FROM OfficialReceipt o WHERE o.noagree = :noagree"),
    @NamedQuery(name = "OfficialReceipt.findByPostgst", query = "SELECT o FROM OfficialReceipt o WHERE o.postgst = :postgst"),
    @NamedQuery(name = "OfficialReceipt.findByGstdate", query = "SELECT o FROM OfficialReceipt o WHERE o.gstdate = :gstdate"),
    @NamedQuery(name = "OfficialReceipt.findByGstid", query = "SELECT o FROM OfficialReceipt o WHERE o.gstid = :gstid"),
    @NamedQuery(name = "OfficialReceipt.findByRacoacode", query = "SELECT o FROM OfficialReceipt o WHERE o.racoacode = :racoacode"),
    @NamedQuery(name = "OfficialReceipt.findByRacoadesc", query = "SELECT o FROM OfficialReceipt o WHERE o.racoadesc = :racoadesc"),
    @NamedQuery(name = "OfficialReceipt.findByInvno", query = "SELECT o FROM OfficialReceipt o WHERE o.invno = :invno")})
public class OfficialReceipt implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatecode")
    private String estatecode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatename")
    private String estatename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estateaddress")
    private String estateaddress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatecity")
    private String estatecity;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatestate")
    private String estatestate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatepostcode")
    private String estatepostcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatephone")
    private String estatephone;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estatefax")
    private String estatefax;
    @Basic(optional = false)
    @NotNull
    @Column(name = "year")
    private int year;
    @Basic(optional = false)
    @NotNull
    @Column(name = "period")
    private int period;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bankcode")
    private String bankcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "bankname")
    private String bankname;
    @Basic(optional = false)
    @NotNull
    @Column(name = "voucherno")
    private int voucherno;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "refer")
    private String refer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "date")
    private String date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paidtype")
    private String paidtype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paidcode")
    private String paidcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paidaddress")
    private String paidaddress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paidname")
    private String paidname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paidcity")
    private String paidcity;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paidstate")
    private String paidstate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paidpostcode")
    private String paidpostcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "post")
    private String post;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postdate")
    private String postdate;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "receivemode")
    private String receivemode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appid")
    private String appid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appname")
    private String appname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "appposition")
    private String appposition;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "preid")
    private String preid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "prename")
    private String prename;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "preposition")
    private String preposition;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "flag")
    private String flag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "rm")
    private String rm;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "stmtyear")
    private String stmtyear;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "stmtperiod")
    private String stmtperiod;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "flagbank")
    private String flagbank;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "deposit")
    private String deposit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "preparedate")
    private String preparedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "checkdate")
    private String checkdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "approvedate")
    private String approvedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkid")
    private String checkid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkname")
    private String checkname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkposition")
    private String checkposition;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "noagree")
    private String noagree;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "postgst")
    private String postgst;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "gstdate")
    private String gstdate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "gstid")
    private String gstid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "racoacode")
    private String racoacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "racoadesc")
    private String racoadesc;
    @Size(max = 15)
    @Column(name = "invno")
    private String invno;
    private String sessionID;
    private String paymentmode;
    private String chequeno;

    public OfficialReceipt() {
    }

    public OfficialReceipt(String refer) {
        this.refer = refer;
    }

    public OfficialReceipt(String refer, String estatecode, String estatename, String estateaddress, String estatecity, String estatestate, String estatepostcode, String estatephone, String estatefax, int year, int period, String bankcode, String bankname, int voucherno, String date, String paidtype, String paidcode, String paidaddress, String paidname, String paidcity, String paidstate, String paidpostcode, String post, String postdate, String remarks, double amount, String receivemode, String appid, String appname, String appposition, String preid, String prename, String preposition, String flag, String rm, String stmtyear, String stmtperiod, String flagbank, String deposit, String preparedate, String checkdate, String approvedate, String checkid, String checkname, String checkposition, String noagree, String postgst, String gstdate, String gstid, String racoacode, String racoadesc, String sessionID, String paymentmode, String chequeno) {
        this.refer = refer;
        this.estatecode = estatecode;
        this.estatename = estatename;
        this.estateaddress = estateaddress;
        this.estatecity = estatecity;
        this.estatestate = estatestate;
        this.estatepostcode = estatepostcode;
        this.estatephone = estatephone;
        this.estatefax = estatefax;
        this.year = year;
        this.period = period;
        this.bankcode = bankcode;
        this.bankname = bankname;
        this.voucherno = voucherno;
        this.date = date;
        this.paidtype = paidtype;
        this.paidcode = paidcode;
        this.paidaddress = paidaddress;
        this.paidname = paidname;
        this.paidcity = paidcity;
        this.paidstate = paidstate;
        this.paidpostcode = paidpostcode;
        this.post = post;
        this.postdate = postdate;
        this.remarks = remarks;
        this.amount = amount;
        this.receivemode = receivemode;
        this.appid = appid;
        this.appname = appname;
        this.appposition = appposition;
        this.preid = preid;
        this.prename = prename;
        this.preposition = preposition;
        this.flag = flag;
        this.rm = rm;
        this.stmtyear = stmtyear;
        this.stmtperiod = stmtperiod;
        this.flagbank = flagbank;
        this.deposit = deposit;
        this.preparedate = preparedate;
        this.checkdate = checkdate;
        this.approvedate = approvedate;
        this.checkid = checkid;
        this.checkname = checkname;
        this.checkposition = checkposition;
        this.noagree = noagree;
        this.postgst = postgst;
        this.gstdate = gstdate;
        this.gstid = gstid;
        this.racoacode = racoacode;
        this.racoadesc = racoadesc;
        this.sessionID = sessionID;
        this.paymentmode = paymentmode;
        this.chequeno = chequeno;
    }

    public String getPaymentmode() {
        return paymentmode;
    }

    public void setPaymentmode(String paymentmode) {
        this.paymentmode = paymentmode;
    }

    public String getChequeno() {
        return chequeno;
    }

    public void setChequeno(String chequeno) {
        this.chequeno = chequeno;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getEstatecode() {
        return estatecode;
    }

    public void setEstatecode(String estatecode) {
        this.estatecode = estatecode;
    }

    public String getEstatename() {
        return estatename;
    }

    public void setEstatename(String estatename) {
        this.estatename = estatename;
    }

    public String getEstateaddress() {
        return estateaddress;
    }

    public void setEstateaddress(String estateaddress) {
        this.estateaddress = estateaddress;
    }

    public String getEstatecity() {
        return estatecity;
    }

    public void setEstatecity(String estatecity) {
        this.estatecity = estatecity;
    }

    public String getEstatestate() {
        return estatestate;
    }

    public void setEstatestate(String estatestate) {
        this.estatestate = estatestate;
    }

    public String getEstatepostcode() {
        return estatepostcode;
    }

    public void setEstatepostcode(String estatepostcode) {
        this.estatepostcode = estatepostcode;
    }

    public String getEstatephone() {
        return estatephone;
    }

    public void setEstatephone(String estatephone) {
        this.estatephone = estatephone;
    }

    public String getEstatefax() {
        return estatefax;
    }

    public void setEstatefax(String estatefax) {
        this.estatefax = estatefax;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public int getVoucherno() {
        return voucherno;
    }

    public void setVoucherno(int voucherno) {
        this.voucherno = voucherno;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPaidtype() {
        return paidtype;
    }

    public void setPaidtype(String paidtype) {
        this.paidtype = paidtype;
    }

    public String getPaidcode() {
        return paidcode;
    }

    public void setPaidcode(String paidcode) {
        this.paidcode = paidcode;
    }

    public String getPaidaddress() {
        return paidaddress;
    }

    public void setPaidaddress(String paidaddress) {
        this.paidaddress = paidaddress;
    }

    public String getPaidname() {
        return paidname;
    }

    public void setPaidname(String paidname) {
        this.paidname = paidname;
    }

    public String getPaidcity() {
        return paidcity;
    }

    public void setPaidcity(String paidcity) {
        this.paidcity = paidcity;
    }

    public String getPaidstate() {
        return paidstate;
    }

    public void setPaidstate(String paidstate) {
        this.paidstate = paidstate;
    }

    public String getPaidpostcode() {
        return paidpostcode;
    }

    public void setPaidpostcode(String paidpostcode) {
        this.paidpostcode = paidpostcode;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getPostdate() {
        return postdate;
    }

    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getReceivemode() {
        return receivemode;
    }

    public void setReceivemode(String receivemode) {
        this.receivemode = receivemode;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getAppposition() {
        return appposition;
    }

    public void setAppposition(String appposition) {
        this.appposition = appposition;
    }

    public String getPreid() {
        return preid;
    }

    public void setPreid(String preid) {
        this.preid = preid;
    }

    public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        this.prename = prename;
    }

    public String getPreposition() {
        return preposition;
    }

    public void setPreposition(String preposition) {
        this.preposition = preposition;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getRm() {
        return rm;
    }

    public void setRm(String rm) {
        this.rm = rm;
    }

    public String getStmtyear() {
        return stmtyear;
    }

    public void setStmtyear(String stmtyear) {
        this.stmtyear = stmtyear;
    }

    public String getStmtperiod() {
        return stmtperiod;
    }

    public void setStmtperiod(String stmtperiod) {
        this.stmtperiod = stmtperiod;
    }

    public String getFlagbank() {
        return flagbank;
    }

    public void setFlagbank(String flagbank) {
        this.flagbank = flagbank;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getPreparedate() {
        return preparedate;
    }

    public void setPreparedate(String preparedate) {
        this.preparedate = preparedate;
    }

    public String getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(String checkdate) {
        this.checkdate = checkdate;
    }

    public String getApprovedate() {
        return approvedate;
    }

    public void setApprovedate(String approvedate) {
        this.approvedate = approvedate;
    }

    public String getCheckid() {
        return checkid;
    }

    public void setCheckid(String checkid) {
        this.checkid = checkid;
    }

    public String getCheckname() {
        return checkname;
    }

    public void setCheckname(String checkname) {
        this.checkname = checkname;
    }

    public String getCheckposition() {
        return checkposition;
    }

    public void setCheckposition(String checkposition) {
        this.checkposition = checkposition;
    }

    public String getNoagree() {
        return noagree;
    }

    public void setNoagree(String noagree) {
        this.noagree = noagree;
    }

    public String getPostgst() {
        return postgst;
    }

    public void setPostgst(String postgst) {
        this.postgst = postgst;
    }

    public String getGstdate() {
        return gstdate;
    }

    public void setGstdate(String gstdate) {
        this.gstdate = gstdate;
    }

    public String getGstid() {
        return gstid;
    }

    public void setGstid(String gstid) {
        this.gstid = gstid;
    }

    public String getRacoacode() {
        return racoacode;
    }

    public void setRacoacode(String racoacode) {
        this.racoacode = racoacode;
    }

    public String getRacoadesc() {
        return racoadesc;
    }

    public void setRacoadesc(String racoadesc) {
        this.racoadesc = racoadesc;
    }

    public String getInvno() {
        return invno;
    }

    public void setInvno(String invno) {
        this.invno = invno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OfficialReceipt)) {
            return false;
        }
        OfficialReceipt other = (OfficialReceipt) object;
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.cashbook.OfficialReceipt[ refer=" + refer + " ]";
    }
    
}
