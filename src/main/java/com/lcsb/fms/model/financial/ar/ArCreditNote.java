/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ar;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ar_creditnote")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ArCreditNote.findAll", query = "SELECT a FROM ArCreditNote a"),
    @NamedQuery(name = "ArCreditNote.findByNoteno", query = "SELECT a FROM ArCreditNote a WHERE a.noteno = :noteno"),
    @NamedQuery(name = "ArCreditNote.findByJvrefno", query = "SELECT a FROM ArCreditNote a WHERE a.jvrefno = :jvrefno"),
    @NamedQuery(name = "ArCreditNote.findByYear", query = "SELECT a FROM ArCreditNote a WHERE a.year = :year"),
    @NamedQuery(name = "ArCreditNote.findByPeriod", query = "SELECT a FROM ArCreditNote a WHERE a.period = :period"),
    @NamedQuery(name = "ArCreditNote.findByNotedate", query = "SELECT a FROM ArCreditNote a WHERE a.notedate = :notedate"),
    @NamedQuery(name = "ArCreditNote.findByBuyercode", query = "SELECT a FROM ArCreditNote a WHERE a.buyercode = :buyercode"),
    @NamedQuery(name = "ArCreditNote.findByBuyername", query = "SELECT a FROM ArCreditNote a WHERE a.buyername = :buyername"),
    @NamedQuery(name = "ArCreditNote.findByProdcode", query = "SELECT a FROM ArCreditNote a WHERE a.prodcode = :prodcode"),
    @NamedQuery(name = "ArCreditNote.findByProdname", query = "SELECT a FROM ArCreditNote a WHERE a.prodname = :prodname"),
    @NamedQuery(name = "ArCreditNote.findByProdacc", query = "SELECT a FROM ArCreditNote a WHERE a.prodacc = :prodacc"),
    @NamedQuery(name = "ArCreditNote.findByKontrak", query = "SELECT a FROM ArCreditNote a WHERE a.kontrak = :kontrak"),
    @NamedQuery(name = "ArCreditNote.findByPostflag", query = "SELECT a FROM ArCreditNote a WHERE a.postflag = :postflag"),
    @NamedQuery(name = "ArCreditNote.findByPid", query = "SELECT a FROM ArCreditNote a WHERE a.pid = :pid"),
    @NamedQuery(name = "ArCreditNote.findByPname", query = "SELECT a FROM ArCreditNote a WHERE a.pname = :pname"),
    @NamedQuery(name = "ArCreditNote.findByPdate", query = "SELECT a FROM ArCreditNote a WHERE a.pdate = :pdate"),
    @NamedQuery(name = "ArCreditNote.findByCheckid", query = "SELECT a FROM ArCreditNote a WHERE a.checkid = :checkid"),
    @NamedQuery(name = "ArCreditNote.findByCheckname", query = "SELECT a FROM ArCreditNote a WHERE a.checkname = :checkname"),
    @NamedQuery(name = "ArCreditNote.findByCheckdate", query = "SELECT a FROM ArCreditNote a WHERE a.checkdate = :checkdate"),
    @NamedQuery(name = "ArCreditNote.findByAppid", query = "SELECT a FROM ArCreditNote a WHERE a.appid = :appid"),
    @NamedQuery(name = "ArCreditNote.findByAppname", query = "SELECT a FROM ArCreditNote a WHERE a.appname = :appname"),
    @NamedQuery(name = "ArCreditNote.findByAppdate", query = "SELECT a FROM ArCreditNote a WHERE a.appdate = :appdate"),
    @NamedQuery(name = "ArCreditNote.findByAppdesign", query = "SELECT a FROM ArCreditNote a WHERE a.appdesign = :appdesign"),
    @NamedQuery(name = "ArCreditNote.findByEstcode", query = "SELECT a FROM ArCreditNote a WHERE a.estcode = :estcode"),
    @NamedQuery(name = "ArCreditNote.findByEstname", query = "SELECT a FROM ArCreditNote a WHERE a.estname = :estname"),
    @NamedQuery(name = "ArCreditNote.findByTotal", query = "SELECT a FROM ArCreditNote a WHERE a.total = :total"),
    @NamedQuery(name = "ArCreditNote.findBySatype", query = "SELECT a FROM ArCreditNote a WHERE a.satype = :satype"),
    @NamedQuery(name = "ArCreditNote.findBySacode", query = "SELECT a FROM ArCreditNote a WHERE a.sacode = :sacode"),
    @NamedQuery(name = "ArCreditNote.findBySadecs", query = "SELECT a FROM ArCreditNote a WHERE a.sadecs = :sadecs"),
    @NamedQuery(name = "ArCreditNote.findByPostdate", query = "SELECT a FROM ArCreditNote a WHERE a.postdate = :postdate"),
    @NamedQuery(name = "ArCreditNote.findByGstid", query = "SELECT a FROM ArCreditNote a WHERE a.gstid = :gstid"),
    @NamedQuery(name = "ArCreditNote.findByPostgst", query = "SELECT a FROM ArCreditNote a WHERE a.postgst = :postgst")})
public class ArCreditNote implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "noteno")
    private String noteno;
    @Size(max = 100)
    @Column(name = "jvrefno")
    private String jvrefno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "period")
    private String period;
    @Basic(optional = false)
    @NotNull
    @Column(name = "notedate")
    @Temporal(TemporalType.DATE)
    private Date notedate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "buyercode")
    private String buyercode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "buyername")
    private String buyername;
    @Size(max = 100)
    @Column(name = "prodcode")
    private String prodcode;
    @Size(max = 100)
    @Column(name = "prodname")
    private String prodname;
    @Size(max = 100)
    @Column(name = "prodacc")
    private String prodacc;
    @Size(max = 100)
    @Column(name = "kontrak")
    private String kontrak;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "postflag")
    private String postflag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pid")
    private String pid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pname")
    private String pname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "pdate")
    private String pdate;
    @Size(max = 100)
    @Column(name = "checkid")
    private String checkid;
    @Size(max = 100)
    @Column(name = "checkname")
    private String checkname;
    @Size(max = 100)
    @Column(name = "checkdate")
    private String checkdate;
    @Size(max = 100)
    @Column(name = "appid")
    private String appid;
    @Size(max = 100)
    @Column(name = "appname")
    private String appname;
    @Size(max = 100)
    @Column(name = "appdate")
    private String appdate;
    @Size(max = 100)
    @Column(name = "appdesign")
    private String appdesign;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "estcode")
    private String estcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "estname")
    private String estname;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "remark")
    private String remark;
    @Basic(optional = false)
    @NotNull
    @Column(name = "total")
    private double total;
    @Size(max = 20)
    @Column(name = "satype")
    private String satype;
    @Size(max = 4)
    @Column(name = "sacode")
    private String sacode;
    @Size(max = 100)
    @Column(name = "sadecs")
    private String sadecs;
    @Column(name = "postdate")
    @Temporal(TemporalType.DATE)
    private Date postdate;
    @Size(max = 20)
    @Column(name = "gstid")
    private String gstid;
    @Size(max = 5)
    @Column(name = "postgst")
    private String postgst;

    public ArCreditNote() {
    }

    public ArCreditNote(String noteno) {
        this.noteno = noteno;
    }

    public ArCreditNote(String noteno, String year, String period, Date notedate, String buyercode, String buyername, String postflag, String pid, String pname, String pdate, String estcode, String estname, String remark, double total) {
        this.noteno = noteno;
        this.year = year;
        this.period = period;
        this.notedate = notedate;
        this.buyercode = buyercode;
        this.buyername = buyername;
        this.postflag = postflag;
        this.pid = pid;
        this.pname = pname;
        this.pdate = pdate;
        this.estcode = estcode;
        this.estname = estname;
        this.remark = remark;
        this.total = total;
    }

    public String getNoteno() {
        return noteno;
    }

    public void setNoteno(String noteno) {
        this.noteno = noteno;
    }

    public String getJvrefno() {
        return jvrefno;
    }

    public void setJvrefno(String jvrefno) {
        this.jvrefno = jvrefno;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public Date getNotedate() {
        return notedate;
    }

    public void setNotedate(Date notedate) {
        this.notedate = notedate;
    }

    public String getBuyercode() {
        return buyercode;
    }

    public void setBuyercode(String buyercode) {
        this.buyercode = buyercode;
    }

    public String getBuyername() {
        return buyername;
    }

    public void setBuyername(String buyername) {
        this.buyername = buyername;
    }

    public String getProdcode() {
        return prodcode;
    }

    public void setProdcode(String prodcode) {
        this.prodcode = prodcode;
    }

    public String getProdname() {
        return prodname;
    }

    public void setProdname(String prodname) {
        this.prodname = prodname;
    }

    public String getProdacc() {
        return prodacc;
    }

    public void setProdacc(String prodacc) {
        this.prodacc = prodacc;
    }

    public String getKontrak() {
        return kontrak;
    }

    public void setKontrak(String kontrak) {
        this.kontrak = kontrak;
    }

    public String getPostflag() {
        return postflag;
    }

    public void setPostflag(String postflag) {
        this.postflag = postflag;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPdate() {
        return pdate;
    }

    public void setPdate(String pdate) {
        this.pdate = pdate;
    }

    public String getCheckid() {
        return checkid;
    }

    public void setCheckid(String checkid) {
        this.checkid = checkid;
    }

    public String getCheckname() {
        return checkname;
    }

    public void setCheckname(String checkname) {
        this.checkname = checkname;
    }

    public String getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(String checkdate) {
        this.checkdate = checkdate;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getAppdate() {
        return appdate;
    }

    public void setAppdate(String appdate) {
        this.appdate = appdate;
    }

    public String getAppdesign() {
        return appdesign;
    }

    public void setAppdesign(String appdesign) {
        this.appdesign = appdesign;
    }

    public String getEstcode() {
        return estcode;
    }

    public void setEstcode(String estcode) {
        this.estcode = estcode;
    }

    public String getEstname() {
        return estname;
    }

    public void setEstname(String estname) {
        this.estname = estname;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadecs() {
        return sadecs;
    }

    public void setSadecs(String sadecs) {
        this.sadecs = sadecs;
    }

    public Date getPostdate() {
        return postdate;
    }

    public void setPostdate(Date postdate) {
        this.postdate = postdate;
    }

    public String getGstid() {
        return gstid;
    }

    public void setGstid(String gstid) {
        this.gstid = gstid;
    }

    public String getPostgst() {
        return postgst;
    }

    public void setPostgst(String postgst) {
        this.postgst = postgst;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noteno != null ? noteno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArCreditNote)) {
            return false;
        }
        ArCreditNote other = (ArCreditNote) object;
        if ((this.noteno == null && other.noteno != null) || (this.noteno != null && !this.noteno.equals(other.noteno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.ar.ArCreditNote[ noteno=" + noteno + " ]";
    }
    
}
