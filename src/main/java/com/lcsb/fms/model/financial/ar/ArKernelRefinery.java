/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ar;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "qc_kernel_grade")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ArKernelRefinery.findAll", query = "SELECT a FROM ArKernelRefinery a"),
    @NamedQuery(name = "ArKernelRefinery.findByMt", query = "SELECT a FROM ArKernelRefinery a WHERE a.mt = :mt"),
    @NamedQuery(name = "ArKernelRefinery.findByCode", query = "SELECT a FROM ArKernelRefinery a WHERE a.arKernelRefineryPK.code = :code"),
    @NamedQuery(name = "ArKernelRefinery.findByDate", query = "SELECT a FROM ArKernelRefinery a WHERE a.arKernelRefineryPK.date = :date"),
    @NamedQuery(name = "ArKernelRefinery.findByLot", query = "SELECT a FROM ArKernelRefinery a WHERE a.lot = :lot"),
    @NamedQuery(name = "ArKernelRefinery.findByContract", query = "SELECT a FROM ArKernelRefinery a WHERE a.contract = :contract"),
    @NamedQuery(name = "ArKernelRefinery.findByTrailer", query = "SELECT a FROM ArKernelRefinery a WHERE a.arKernelRefineryPK.trailer = :trailer"),
    @NamedQuery(name = "ArKernelRefinery.findByAid", query = "SELECT a FROM ArKernelRefinery a WHERE a.aid = :aid"),
    @NamedQuery(name = "ArKernelRefinery.findByAdate", query = "SELECT a FROM ArKernelRefinery a WHERE a.adate = :adate"),
    @NamedQuery(name = "ArKernelRefinery.findByAname", query = "SELECT a FROM ArKernelRefinery a WHERE a.aname = :aname"),
    @NamedQuery(name = "ArKernelRefinery.findByCid", query = "SELECT a FROM ArKernelRefinery a WHERE a.cid = :cid"),
    @NamedQuery(name = "ArKernelRefinery.findByCname", query = "SELECT a FROM ArKernelRefinery a WHERE a.cname = :cname"),
    @NamedQuery(name = "ArKernelRefinery.findByAdesign", query = "SELECT a FROM ArKernelRefinery a WHERE a.adesign = :adesign"),
    @NamedQuery(name = "ArKernelRefinery.findByCdesign", query = "SELECT a FROM ArKernelRefinery a WHERE a.cdesign = :cdesign"),
    @NamedQuery(name = "ArKernelRefinery.findByCdate", query = "SELECT a FROM ArKernelRefinery a WHERE a.cdate = :cdate"),
    @NamedQuery(name = "ArKernelRefinery.findByLori", query = "SELECT a FROM ArKernelRefinery a WHERE a.arKernelRefineryPK.lori = :lori"),
    @NamedQuery(name = "ArKernelRefinery.findByMtRefinery", query = "SELECT a FROM ArKernelRefinery a WHERE a.mtRefinery = :mtRefinery"),
    @NamedQuery(name = "ArKernelRefinery.findByDispatchNo", query = "SELECT a FROM ArKernelRefinery a WHERE a.dispatchNo = :dispatchNo"),
    @NamedQuery(name = "ArKernelRefinery.findByDriver", query = "SELECT a FROM ArKernelRefinery a WHERE a.driver = :driver"),
    @NamedQuery(name = "ArKernelRefinery.findByTransporter", query = "SELECT a FROM ArKernelRefinery a WHERE a.transporter = :transporter"),
    @NamedQuery(name = "ArKernelRefinery.findByDay", query = "SELECT a FROM ArKernelRefinery a WHERE a.day = :day"),
    @NamedQuery(name = "ArKernelRefinery.findByDateRefinery", query = "SELECT a FROM ArKernelRefinery a WHERE a.dateRefinery = :dateRefinery"),
    @NamedQuery(name = "ArKernelRefinery.findByDispatchNoRefinery", query = "SELECT a FROM ArKernelRefinery a WHERE a.dispatchNoRefinery = :dispatchNoRefinery")})
public class ArKernelRefinery implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ArKernelRefineryPK arKernelRefineryPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "mt")
    private String mt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "lot")
    private String lot;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "contract")
    private String contract;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "aid")
    private String aid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "adate")
    private String adate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "aname")
    private String aname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cid")
    private String cid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cname")
    private String cname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "adesign")
    private String adesign;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cdesign")
    private String cdesign;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cdate")
    private String cdate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "mt_refinery")
    private double mtRefinery;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "dispatch_no")
    private String dispatchNo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "driver")
    private String driver;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "transporter")
    private String transporter;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "day")
    private String day;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_refinery")
    @Temporal(TemporalType.DATE)
    private Date dateRefinery;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "dispatch_no_refinery")
    private String dispatchNoRefinery;

    public ArKernelRefinery() {
    }

    public ArKernelRefinery(ArKernelRefineryPK arKernelRefineryPK) {
        this.arKernelRefineryPK = arKernelRefineryPK;
    }

    public ArKernelRefinery(ArKernelRefineryPK arKernelRefineryPK, String mt, String lot, String contract, String aid, String adate, String aname, String cid, String cname, String adesign, String cdesign, String cdate, double mtRefinery, String dispatchNo, String driver, String transporter, String day, Date dateRefinery, String dispatchNoRefinery) {
        this.arKernelRefineryPK = arKernelRefineryPK;
        this.mt = mt;
        this.lot = lot;
        this.contract = contract;
        this.aid = aid;
        this.adate = adate;
        this.aname = aname;
        this.cid = cid;
        this.cname = cname;
        this.adesign = adesign;
        this.cdesign = cdesign;
        this.cdate = cdate;
        this.mtRefinery = mtRefinery;
        this.dispatchNo = dispatchNo;
        this.driver = driver;
        this.transporter = transporter;
        this.day = day;
        this.dateRefinery = dateRefinery;
        this.dispatchNoRefinery = dispatchNoRefinery;
    }

    public ArKernelRefinery(String code, Date date, String trailer, String lori) {
        this.arKernelRefineryPK = new ArKernelRefineryPK(code, date, trailer, lori);
    }

    public ArKernelRefineryPK getArKernelRefineryPK() {
        return arKernelRefineryPK;
    }

    public void setArKernelRefineryPK(ArKernelRefineryPK arKernelRefineryPK) {
        this.arKernelRefineryPK = arKernelRefineryPK;
    }

    public String getMt() {
        return mt;
    }

    public void setMt(String mt) {
        this.mt = mt;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getAdate() {
        return adate;
    }

    public void setAdate(String adate) {
        this.adate = adate;
    }

    public String getAname() {
        return aname;
    }

    public void setAname(String aname) {
        this.aname = aname;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getAdesign() {
        return adesign;
    }

    public void setAdesign(String adesign) {
        this.adesign = adesign;
    }

    public String getCdesign() {
        return cdesign;
    }

    public void setCdesign(String cdesign) {
        this.cdesign = cdesign;
    }

    public String getCdate() {
        return cdate;
    }

    public void setCdate(String cdate) {
        this.cdate = cdate;
    }

    public double getMtRefinery() {
        return mtRefinery;
    }

    public void setMtRefinery(double mtRefinery) {
        this.mtRefinery = mtRefinery;
    }

    public String getDispatchNo() {
        return dispatchNo;
    }

    public void setDispatchNo(String dispatchNo) {
        this.dispatchNo = dispatchNo;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getTransporter() {
        return transporter;
    }

    public void setTransporter(String transporter) {
        this.transporter = transporter;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Date getDateRefinery() {
        return dateRefinery;
    }

    public void setDateRefinery(Date dateRefinery) {
        this.dateRefinery = dateRefinery;
    }

    public String getDispatchNoRefinery() {
        return dispatchNoRefinery;
    }

    public void setDispatchNoRefinery(String dispatchNoRefinery) {
        this.dispatchNoRefinery = dispatchNoRefinery;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (arKernelRefineryPK != null ? arKernelRefineryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArKernelRefinery)) {
            return false;
        }
        ArKernelRefinery other = (ArKernelRefinery) object;
        if ((this.arKernelRefineryPK == null && other.arKernelRefineryPK != null) || (this.arKernelRefineryPK != null && !this.arKernelRefineryPK.equals(other.arKernelRefineryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.ar.ArKernelRefinery[ arKernelRefineryPK=" + arKernelRefineryPK + " ]";
    }
    
}
