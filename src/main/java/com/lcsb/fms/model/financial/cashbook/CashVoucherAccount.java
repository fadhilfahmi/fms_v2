/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.model.financial.cashbook;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dell
 */
@Entity
@Table(name = "cb_cashvoucher_account")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CashVoucherAccouont.findAll", query = "SELECT c FROM CashVoucherAccouont c"),
    @NamedQuery(name = "CashVoucherAccouont.findByNovoucher", query = "SELECT c FROM CashVoucherAccouont c WHERE c.cashVoucherAccouontPK.novoucher = :novoucher"),
    @NamedQuery(name = "CashVoucherAccouont.findByLoccode", query = "SELECT c FROM CashVoucherAccouont c WHERE c.cashVoucherAccouontPK.loccode = :loccode"),
    @NamedQuery(name = "CashVoucherAccouont.findByLocname", query = "SELECT c FROM CashVoucherAccouont c WHERE c.locname = :locname"),
    @NamedQuery(name = "CashVoucherAccouont.findByLoclevel", query = "SELECT c FROM CashVoucherAccouont c WHERE c.loclevel = :loclevel"),
    @NamedQuery(name = "CashVoucherAccouont.findByCoacode", query = "SELECT c FROM CashVoucherAccouont c WHERE c.cashVoucherAccouontPK.coacode = :coacode"),
    @NamedQuery(name = "CashVoucherAccouont.findByCoadescp", query = "SELECT c FROM CashVoucherAccouont c WHERE c.coadescp = :coadescp"),
    @NamedQuery(name = "CashVoucherAccouont.findByChargetype", query = "SELECT c FROM CashVoucherAccouont c WHERE c.chargetype = :chargetype"),
    @NamedQuery(name = "CashVoucherAccouont.findByChargecode", query = "SELECT c FROM CashVoucherAccouont c WHERE c.cashVoucherAccouontPK.chargecode = :chargecode"),
    @NamedQuery(name = "CashVoucherAccouont.findByChargedescp", query = "SELECT c FROM CashVoucherAccouont c WHERE c.chargedescp = :chargedescp"),
    @NamedQuery(name = "CashVoucherAccouont.findByDebit", query = "SELECT c FROM CashVoucherAccouont c WHERE c.debit = :debit"),
    @NamedQuery(name = "CashVoucherAccouont.findByRefer", query = "SELECT c FROM CashVoucherAccouont c WHERE c.cashVoucherAccouontPK.refer = :refer"),
    @NamedQuery(name = "CashVoucherAccouont.findBySatype", query = "SELECT c FROM CashVoucherAccouont c WHERE c.satype = :satype"),
    @NamedQuery(name = "CashVoucherAccouont.findBySacode", query = "SELECT c FROM CashVoucherAccouont c WHERE c.sacode = :sacode"),
    @NamedQuery(name = "CashVoucherAccouont.findBySadesc", query = "SELECT c FROM CashVoucherAccouont c WHERE c.sadesc = :sadesc"),
    @NamedQuery(name = "CashVoucherAccouont.findByTaxcode", query = "SELECT c FROM CashVoucherAccouont c WHERE c.taxcode = :taxcode"),
    @NamedQuery(name = "CashVoucherAccouont.findByTaxrate", query = "SELECT c FROM CashVoucherAccouont c WHERE c.taxrate = :taxrate"),
    @NamedQuery(name = "CashVoucherAccouont.findByAmtbeforetax", query = "SELECT c FROM CashVoucherAccouont c WHERE c.amtbeforetax = :amtbeforetax"),
    @NamedQuery(name = "CashVoucherAccouont.findByTaxcoacode", query = "SELECT c FROM CashVoucherAccouont c WHERE c.taxcoacode = :taxcoacode"),
    @NamedQuery(name = "CashVoucherAccouont.findByTaxcoadescp", query = "SELECT c FROM CashVoucherAccouont c WHERE c.taxcoadescp = :taxcoadescp"),
    @NamedQuery(name = "CashVoucherAccouont.findByTaxamt", query = "SELECT c FROM CashVoucherAccouont c WHERE c.taxamt = :taxamt")})
public class CashVoucherAccount implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CashVoucherAccountPK cashVoucherAccouontPK;
    @Basic(optional = false)
    @Column(name = "locname")
    private String locname;
    @Basic(optional = false)
    @Column(name = "loclevel")
    private String loclevel;
    @Basic(optional = false)
    @Column(name = "coadescp")
    private String coadescp;
    @Basic(optional = false)
    @Column(name = "chargetype")
    private String chargetype;
    @Basic(optional = false)
    @Column(name = "chargedescp")
    private String chargedescp;
    @Basic(optional = false)
    @Column(name = "debit")
    private String debit;
    @Basic(optional = false)
    @Lob
    @Column(name = "remarks")
    private String remarks;
    @Basic(optional = false)
    @Column(name = "satype")
    private String satype;
    @Basic(optional = false)
    @Column(name = "sacode")
    private String sacode;
    @Column(name = "sadesc")
    private String sadesc;
    @Basic(optional = false)
    @Column(name = "taxcode")
    private String taxcode;
    @Basic(optional = false)
    @Lob
    @Column(name = "taxdescp")
    private String taxdescp;
    @Basic(optional = false)
    @Column(name = "taxrate")
    private double taxrate;
    @Basic(optional = false)
    @Column(name = "amtbeforetax")
    private double amtbeforetax;
    @Basic(optional = false)
    @Column(name = "taxcoacode")
    private String taxcoacode;
    @Basic(optional = false)
    @Column(name = "taxcoadescp")
    private String taxcoadescp;
    @Basic(optional = false)
    @Column(name = "taxamt")
    private double taxamt;

    public CashVoucherAccount() {
    }

    public CashVoucherAccount(CashVoucherAccountPK cashVoucherAccouontPK) {
        this.cashVoucherAccouontPK = cashVoucherAccouontPK;
    }

    public CashVoucherAccount(CashVoucherAccountPK cashVoucherAccouontPK, String locname, String loclevel, String coadescp, String chargetype, String chargedescp, String debit, String remarks, String satype, String sacode, String taxcode, String taxdescp, double taxrate, double amtbeforetax, String taxcoacode, String taxcoadescp, double taxamt) {
        this.cashVoucherAccouontPK = cashVoucherAccouontPK;
        this.locname = locname;
        this.loclevel = loclevel;
        this.coadescp = coadescp;
        this.chargetype = chargetype;
        this.chargedescp = chargedescp;
        this.debit = debit;
        this.remarks = remarks;
        this.satype = satype;
        this.sacode = sacode;
        this.taxcode = taxcode;
        this.taxdescp = taxdescp;
        this.taxrate = taxrate;
        this.amtbeforetax = amtbeforetax;
        this.taxcoacode = taxcoacode;
        this.taxcoadescp = taxcoadescp;
        this.taxamt = taxamt;
    }
    
    public String refer,novoucher,loccode,coacode;

    public String getCoacode() {
        return coacode;
    }

    public void setCoacode(String coacode) {
        this.coacode = coacode;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getNovoucher() {
        return novoucher;
    }

    public void setNovoucher(String novoucher) {
        this.novoucher = novoucher;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }
    
    

    public CashVoucherAccount(String novoucher, String loccode, String coacode, String chargecode, String refer) {
        this.cashVoucherAccouontPK = new CashVoucherAccountPK(novoucher, loccode, coacode, chargecode, refer);
    }

    public CashVoucherAccountPK getCashVoucherAccouontPK() {
        return cashVoucherAccouontPK;
    }

    public void setCashVoucherAccouontPK(CashVoucherAccountPK cashVoucherAccouontPK) {
        this.cashVoucherAccouontPK = cashVoucherAccouontPK;
    }

    public String getLocname() {
        return locname;
    }

    public void setLocname(String locname) {
        this.locname = locname;
    }

    public String getLoclevel() {
        return loclevel;
    }

    public void setLoclevel(String loclevel) {
        this.loclevel = loclevel;
    }

    public String getCoadescp() {
        return coadescp;
    }

    public void setCoadescp(String coadescp) {
        this.coadescp = coadescp;
    }

    public String getChargetype() {
        return chargetype;
    }

    public void setChargetype(String chargetype) {
        this.chargetype = chargetype;
    }

    public String getChargedescp() {
        return chargedescp;
    }

    public void setChargedescp(String chargedescp) {
        this.chargedescp = chargedescp;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    public String getTaxcode() {
        return taxcode;
    }

    public void setTaxcode(String taxcode) {
        this.taxcode = taxcode;
    }

    public String getTaxdescp() {
        return taxdescp;
    }

    public void setTaxdescp(String taxdescp) {
        this.taxdescp = taxdescp;
    }

    public double getTaxrate() {
        return taxrate;
    }

    public void setTaxrate(double taxrate) {
        this.taxrate = taxrate;
    }

    public double getAmtbeforetax() {
        return amtbeforetax;
    }

    public void setAmtbeforetax(double amtbeforetax) {
        this.amtbeforetax = amtbeforetax;
    }

    public String getTaxcoacode() {
        return taxcoacode;
    }

    public void setTaxcoacode(String taxcoacode) {
        this.taxcoacode = taxcoacode;
    }

    public String getTaxcoadescp() {
        return taxcoadescp;
    }

    public void setTaxcoadescp(String taxcoadescp) {
        this.taxcoadescp = taxcoadescp;
    }

    public double getTaxamt() {
        return taxamt;
    }

    public void setTaxamt(double taxamt) {
        this.taxamt = taxamt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cashVoucherAccouontPK != null ? cashVoucherAccouontPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CashVoucherAccount)) {
            return false;
        }
        CashVoucherAccount other = (CashVoucherAccount) object;
        if ((this.cashVoucherAccouontPK == null && other.cashVoucherAccouontPK != null) || (this.cashVoucherAccouontPK != null && !this.cashVoucherAccouontPK.equals(other.cashVoucherAccouontPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fms.model.financial.cashbook.CashVoucherAccouont[ cashVoucherAccouontPK=" + cashVoucherAccouontPK + " ]";
    }
    
}
