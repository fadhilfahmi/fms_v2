/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.ap;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "ap_inv_detail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VendorInvoiceDetail.findAll", query = "SELECT v FROM VendorInvoiceDetail v"),
    @NamedQuery(name = "VendorInvoiceDetail.findByNo", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.no = :no"),
    @NamedQuery(name = "VendorInvoiceDetail.findByInvrefno", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.invrefno = :invrefno"),
    @NamedQuery(name = "VendorInvoiceDetail.findByMatcode", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.matcode = :matcode"),
    @NamedQuery(name = "VendorInvoiceDetail.findByMatdesc", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.matdesc = :matdesc"),
    @NamedQuery(name = "VendorInvoiceDetail.findByCvunitprice", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.cvunitprice = :cvunitprice"),
    @NamedQuery(name = "VendorInvoiceDetail.findByCvquantity", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.cvquantity = :cvquantity"),
    @NamedQuery(name = "VendorInvoiceDetail.findByCvunitmeasure", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.cvunitmeasure = :cvunitmeasure"),
    @NamedQuery(name = "VendorInvoiceDetail.findByConversion", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.conversion = :conversion"),
    @NamedQuery(name = "VendorInvoiceDetail.findByQuantity", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.quantity = :quantity"),
    @NamedQuery(name = "VendorInvoiceDetail.findByUnitprice", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.unitprice = :unitprice"),
    @NamedQuery(name = "VendorInvoiceDetail.findByUnitmeasure", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.unitmeasure = :unitmeasure"),
    @NamedQuery(name = "VendorInvoiceDetail.findByAcdesc", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.acdesc = :acdesc"),
    @NamedQuery(name = "VendorInvoiceDetail.findByAccode", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.accode = :accode"),
    @NamedQuery(name = "VendorInvoiceDetail.findByCtdesc", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.ctdesc = :ctdesc"),
    @NamedQuery(name = "VendorInvoiceDetail.findByCtcode", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.ctcode = :ctcode"),
    @NamedQuery(name = "VendorInvoiceDetail.findByCttype", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.cttype = :cttype"),
    @NamedQuery(name = "VendorInvoiceDetail.findByLocdesc", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.locdesc = :locdesc"),
    @NamedQuery(name = "VendorInvoiceDetail.findByLoccode", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.loccode = :loccode"),
    @NamedQuery(name = "VendorInvoiceDetail.findByLoclevel", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.loclevel = :loclevel"),
    @NamedQuery(name = "VendorInvoiceDetail.findByRemark", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.remark = :remark"),
    @NamedQuery(name = "VendorInvoiceDetail.findByPounitmeasure", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.pounitmeasure = :pounitmeasure"),
    @NamedQuery(name = "VendorInvoiceDetail.findByPoquantity", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.poquantity = :poquantity"),
    @NamedQuery(name = "VendorInvoiceDetail.findByPounitprice", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.pounitprice = :pounitprice"),
    @NamedQuery(name = "VendorInvoiceDetail.findByPoamount", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.poamount = :poamount"),
    @NamedQuery(name = "VendorInvoiceDetail.findByCvpounitprice", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.cvpounitprice = :cvpounitprice"),
    @NamedQuery(name = "VendorInvoiceDetail.findByCvpoquantity", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.cvpoquantity = :cvpoquantity"),
    @NamedQuery(name = "VendorInvoiceDetail.findByCvpoconversion", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.cvpoconversion = :cvpoconversion"),
    @NamedQuery(name = "VendorInvoiceDetail.findByCvpounitmeasure", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.cvpounitmeasure = :cvpounitmeasure"),
    @NamedQuery(name = "VendorInvoiceDetail.findByVarian", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.varian = :varian"),
    @NamedQuery(name = "VendorInvoiceDetail.findByAmount", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.amount = :amount"),
    @NamedQuery(name = "VendorInvoiceDetail.findByBil", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.bil = :bil"),
    @NamedQuery(name = "VendorInvoiceDetail.findByTaxcoacode", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.taxcoacode = :taxcoacode"),
    @NamedQuery(name = "VendorInvoiceDetail.findByTaxcoadescp", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.taxcoadescp = :taxcoadescp"),
    @NamedQuery(name = "VendorInvoiceDetail.findBySatype", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.satype = :satype"),
    @NamedQuery(name = "VendorInvoiceDetail.findBySacode", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.sacode = :sacode"),
    @NamedQuery(name = "VendorInvoiceDetail.findBySadesc", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.sadesc = :sadesc"),
    @NamedQuery(name = "VendorInvoiceDetail.findByTaxrate", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.taxrate = :taxrate"),
    @NamedQuery(name = "VendorInvoiceDetail.findByTaxamt", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.taxamt = :taxamt"),
    @NamedQuery(name = "VendorInvoiceDetail.findByTaxcode", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.taxcode = :taxcode"),
    @NamedQuery(name = "VendorInvoiceDetail.findByTaxdescp", query = "SELECT v FROM VendorInvoiceDetail v WHERE v.taxdescp = :taxdescp")})
public class VendorInvoiceDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "no")
    private String no;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "invrefno")
    private String invrefno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "matcode")
    private String matcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "matdesc")
    private String matdesc;
    @Size(max = 100)
    @Column(name = "cvunitprice")
    private String cvunitprice;
    @Size(max = 100)
    @Column(name = "cvquantity")
    private String cvquantity;
    @Size(max = 100)
    @Column(name = "cvunitmeasure")
    private String cvunitmeasure;
    @Size(max = 100)
    @Column(name = "conversion")
    private String conversion;
    @Size(max = 100)
    @Column(name = "quantity")
    private String quantity;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "unitprice")
    private String unitprice;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "unitmeasure")
    private String unitmeasure;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "acdesc")
    private String acdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "accode")
    private String accode;
    @Size(max = 100)
    @Column(name = "ctdesc")
    private String ctdesc;
    @Size(max = 100)
    @Column(name = "ctcode")
    private String ctcode;
    @Size(max = 100)
    @Column(name = "cttype")
    private String cttype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "locdesc")
    private String locdesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loccode")
    private String loccode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "loclevel")
    private String loclevel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "remark")
    private String remark;
    @Size(max = 100)
    @Column(name = "pounitmeasure")
    private String pounitmeasure;
    @Size(max = 100)
    @Column(name = "poquantity")
    private String poquantity;
    @Size(max = 100)
    @Column(name = "pounitprice")
    private String pounitprice;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "poamount")
    private Double poamount;
    @Size(max = 100)
    @Column(name = "cvpounitprice")
    private String cvpounitprice;
    @Size(max = 100)
    @Column(name = "cvpoquantity")
    private String cvpoquantity;
    @Size(max = 100)
    @Column(name = "cvpoconversion")
    private String cvpoconversion;
    @Size(max = 100)
    @Column(name = "cvpounitmeasure")
    private String cvpounitmeasure;
    @Size(max = 100)
    @Column(name = "varian")
    private String varian;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Basic(optional = false)
    @Column(name = "bil")
    private long bil;
    @Size(max = 100)
    @Column(name = "taxcoacode")
    private String taxcoacode;
    @Size(max = 100)
    @Column(name = "taxcoadescp")
    private String taxcoadescp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "satype")
    private String satype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "sacode")
    private String sacode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "sadesc")
    private String sadesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxrate")
    private double taxrate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "taxamt")
    private double taxamt;
    @Size(max = 100)
    @Column(name = "taxcode")
    private String taxcode;
    @Size(max = 100)
    @Column(name = "taxdescp")
    private String taxdescp;

    public VendorInvoiceDetail() {
    }

    public VendorInvoiceDetail(String invrefno) {
        this.invrefno = invrefno;
    }

    public VendorInvoiceDetail(String invrefno, String no, String matcode, String matdesc, String unitprice, String unitmeasure, String acdesc, String accode, String locdesc, String loccode, String loclevel, String remark, double amount, long bil, String satype, String sacode, String sadesc, double taxrate, double taxamt) {
        this.invrefno = invrefno;
        this.no = no;
        this.matcode = matcode;
        this.matdesc = matdesc;
        this.unitprice = unitprice;
        this.unitmeasure = unitmeasure;
        this.acdesc = acdesc;
        this.accode = accode;
        this.locdesc = locdesc;
        this.loccode = loccode;
        this.loclevel = loclevel;
        this.remark = remark;
        this.amount = amount;
        this.bil = bil;
        this.satype = satype;
        this.sacode = sacode;
        this.sadesc = sadesc;
        this.taxrate = taxrate;
        this.taxamt = taxamt;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getInvrefno() {
        return invrefno;
    }

    public void setInvrefno(String invrefno) {
        this.invrefno = invrefno;
    }

    public String getMatcode() {
        return matcode;
    }

    public void setMatcode(String matcode) {
        this.matcode = matcode;
    }

    public String getMatdesc() {
        return matdesc;
    }

    public void setMatdesc(String matdesc) {
        this.matdesc = matdesc;
    }

    public String getCvunitprice() {
        return cvunitprice;
    }

    public void setCvunitprice(String cvunitprice) {
        this.cvunitprice = cvunitprice;
    }

    public String getCvquantity() {
        return cvquantity;
    }

    public void setCvquantity(String cvquantity) {
        this.cvquantity = cvquantity;
    }

    public String getCvunitmeasure() {
        return cvunitmeasure;
    }

    public void setCvunitmeasure(String cvunitmeasure) {
        this.cvunitmeasure = cvunitmeasure;
    }

    public String getConversion() {
        return conversion;
    }

    public void setConversion(String conversion) {
        this.conversion = conversion;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(String unitprice) {
        this.unitprice = unitprice;
    }

    public String getUnitmeasure() {
        return unitmeasure;
    }

    public void setUnitmeasure(String unitmeasure) {
        this.unitmeasure = unitmeasure;
    }

    public String getAcdesc() {
        return acdesc;
    }

    public void setAcdesc(String acdesc) {
        this.acdesc = acdesc;
    }

    public String getAccode() {
        return accode;
    }

    public void setAccode(String accode) {
        this.accode = accode;
    }

    public String getCtdesc() {
        return ctdesc;
    }

    public void setCtdesc(String ctdesc) {
        this.ctdesc = ctdesc;
    }

    public String getCtcode() {
        return ctcode;
    }

    public void setCtcode(String ctcode) {
        this.ctcode = ctcode;
    }

    public String getCttype() {
        return cttype;
    }

    public void setCttype(String cttype) {
        this.cttype = cttype;
    }

    public String getLocdesc() {
        return locdesc;
    }

    public void setLocdesc(String locdesc) {
        this.locdesc = locdesc;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLoclevel() {
        return loclevel;
    }

    public void setLoclevel(String loclevel) {
        this.loclevel = loclevel;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPounitmeasure() {
        return pounitmeasure;
    }

    public void setPounitmeasure(String pounitmeasure) {
        this.pounitmeasure = pounitmeasure;
    }

    public String getPoquantity() {
        return poquantity;
    }

    public void setPoquantity(String poquantity) {
        this.poquantity = poquantity;
    }

    public String getPounitprice() {
        return pounitprice;
    }

    public void setPounitprice(String pounitprice) {
        this.pounitprice = pounitprice;
    }

    public Double getPoamount() {
        return poamount;
    }

    public void setPoamount(Double poamount) {
        this.poamount = poamount;
    }

    public String getCvpounitprice() {
        return cvpounitprice;
    }

    public void setCvpounitprice(String cvpounitprice) {
        this.cvpounitprice = cvpounitprice;
    }

    public String getCvpoquantity() {
        return cvpoquantity;
    }

    public void setCvpoquantity(String cvpoquantity) {
        this.cvpoquantity = cvpoquantity;
    }

    public String getCvpoconversion() {
        return cvpoconversion;
    }

    public void setCvpoconversion(String cvpoconversion) {
        this.cvpoconversion = cvpoconversion;
    }

    public String getCvpounitmeasure() {
        return cvpounitmeasure;
    }

    public void setCvpounitmeasure(String cvpounitmeasure) {
        this.cvpounitmeasure = cvpounitmeasure;
    }

    public String getVarian() {
        return varian;
    }

    public void setVarian(String varian) {
        this.varian = varian;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getBil() {
        return bil;
    }

    public void setBil(long bil) {
        this.bil = bil;
    }

    public String getTaxcoacode() {
        return taxcoacode;
    }

    public void setTaxcoacode(String taxcoacode) {
        this.taxcoacode = taxcoacode;
    }

    public String getTaxcoadescp() {
        return taxcoadescp;
    }

    public void setTaxcoadescp(String taxcoadescp) {
        this.taxcoadescp = taxcoadescp;
    }

    public String getSatype() {
        return satype;
    }

    public void setSatype(String satype) {
        this.satype = satype;
    }

    public String getSacode() {
        return sacode;
    }

    public void setSacode(String sacode) {
        this.sacode = sacode;
    }

    public String getSadesc() {
        return sadesc;
    }

    public void setSadesc(String sadesc) {
        this.sadesc = sadesc;
    }

    public double getTaxrate() {
        return taxrate;
    }

    public void setTaxrate(double taxrate) {
        this.taxrate = taxrate;
    }

    public double getTaxamt() {
        return taxamt;
    }

    public void setTaxamt(double taxamt) {
        this.taxamt = taxamt;
    }

    public String getTaxcode() {
        return taxcode;
    }

    public void setTaxcode(String taxcode) {
        this.taxcode = taxcode;
    }

    public String getTaxdescp() {
        return taxdescp;
    }

    public void setTaxdescp(String taxdescp) {
        this.taxdescp = taxdescp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (invrefno != null ? invrefno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VendorInvoiceDetail)) {
            return false;
        }
        VendorInvoiceDetail other = (VendorInvoiceDetail) object;
        if ((this.invrefno == null && other.invrefno != null) || (this.invrefno != null && !this.invrefno.equals(other.invrefno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.ap.VendorInvoiceDetail[ invrefno=" + invrefno + " ]";
    }
    
}
