/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.model.financial.cashbook;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Dell
 */
@Embeddable
public class CashVoucherAccountPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "novoucher")
    private String novoucher;
    @Basic(optional = false)
    @Column(name = "loccode")
    private String loccode;
    @Basic(optional = false)
    @Column(name = "coacode")
    private String coacode;
    @Basic(optional = false)
    @Column(name = "chargecode")
    private String chargecode;
    @Basic(optional = false)
    @Column(name = "refer")
    private String refer;

    public CashVoucherAccountPK() {
    }

    public CashVoucherAccountPK(String novoucher, String loccode, String coacode, String chargecode, String refer) {
        this.novoucher = novoucher;
        this.loccode = loccode;
        this.coacode = coacode;
        this.chargecode = chargecode;
        this.refer = refer;
    }

    public String getNovoucher() {
        return novoucher;
    }

    public void setNovoucher(String novoucher) {
        this.novoucher = novoucher;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getCoacode() {
        return coacode;
    }

    public void setCoacode(String coacode) {
        this.coacode = coacode;
    }

    public String getChargecode() {
        return chargecode;
    }

    public void setChargecode(String chargecode) {
        this.chargecode = chargecode;
    }

    public String getRefer() {
        return refer;
    }

    public void setRefer(String refer) {
        this.refer = refer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (novoucher != null ? novoucher.hashCode() : 0);
        hash += (loccode != null ? loccode.hashCode() : 0);
        hash += (coacode != null ? coacode.hashCode() : 0);
        hash += (chargecode != null ? chargecode.hashCode() : 0);
        hash += (refer != null ? refer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CashVoucherAccountPK)) {
            return false;
        }
        CashVoucherAccountPK other = (CashVoucherAccountPK) object;
        if ((this.novoucher == null && other.novoucher != null) || (this.novoucher != null && !this.novoucher.equals(other.novoucher))) {
            return false;
        }
        if ((this.loccode == null && other.loccode != null) || (this.loccode != null && !this.loccode.equals(other.loccode))) {
            return false;
        }
        if ((this.coacode == null && other.coacode != null) || (this.coacode != null && !this.coacode.equals(other.coacode))) {
            return false;
        }
        if ((this.chargecode == null && other.chargecode != null) || (this.chargecode != null && !this.chargecode.equals(other.chargecode))) {
            return false;
        }
        if ((this.refer == null && other.refer != null) || (this.refer != null && !this.refer.equals(other.refer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fms.model.financial.cashbook.CashVoucherAccouontPK[ novoucher=" + novoucher + ", loccode=" + loccode + ", coacode=" + coacode + ", chargecode=" + chargecode + ", refer=" + refer + " ]";
    }
    
}
