/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.employee;

/**
 *
 * @author fadhilfahmi
 */
public class EmPayrollPCBforP {
    
    private double Y;
    private double K;
    private double Y1;
    private double K1;
    private double Y2;
    private double K2;
    private int n;
    private double Yt;
    private double Kt;
    private double D;
    private double S;
    private double Du;
    private double Su;
    private double Q;
    private int C;
    private double ELP;
    private double LP1;

    public int getC() {
        return C;
    }

    public void setC(int C) {
        this.C = C;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public double getY() {
        return Y;
    }

    public void setY(double Y) {
        this.Y = Y;
    }

    public double getK() {
        return K;
    }

    public void setK(double K) {
        this.K = K;
    }

    public double getY1() {
        return Y1;
    }

    public void setY1(double Y1) {
        this.Y1 = Y1;
    }

    public double getK1() {
        return K1;
    }

    public void setK1(double K1) {
        this.K1 = K1;
    }

    public double getY2() {
        return Y2;
    }

    public void setY2(double Y2) {
        this.Y2 = Y2;
    }

    public double getK2() {
        return K2;
    }

    public void setK2(double K2) {
        this.K2 = K2;
    }


    public double getYt() {
        return Yt;
    }

    public void setYt(double Yt) {
        this.Yt = Yt;
    }

    public double getKt() {
        return Kt;
    }

    public void setKt(double Kt) {
        this.Kt = Kt;
    }

    public double getD() {
        return D;
    }

    public void setD(double D) {
        this.D = D;
    }

    public double getS() {
        return S;
    }

    public void setS(double S) {
        this.S = S;
    }

    public double getDu() {
        return Du;
    }

    public void setDu(double Du) {
        this.Du = Du;
    }

    public double getSu() {
        return Su;
    }

    public void setSu(double Su) {
        this.Su = Su;
    }

    public double getQ() {
        return Q;
    }

    public void setQ(double Q) {
        this.Q = Q;
    }


    public double getELP() {
        return ELP;
    }

    public void setELP(double ELP) {
        this.ELP = ELP;
    }

    public double getLP1() {
        return LP1;
    }

    public void setLP1(double LP1) {
        this.LP1 = LP1;
    }
    
    
    
}
