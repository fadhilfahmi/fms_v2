/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.model.financial.tx;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fadhilfahmi
 */
@Entity
@Table(name = "tx_output")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TxOutput.findAll", query = "SELECT t FROM TxOutput t"),
    @NamedQuery(name = "TxOutput.findByRefno", query = "SELECT t FROM TxOutput t WHERE t.refno = :refno"),
    @NamedQuery(name = "TxOutput.findByYear", query = "SELECT t FROM TxOutput t WHERE t.year = :year"),
    @NamedQuery(name = "TxOutput.findByPeriod", query = "SELECT t FROM TxOutput t WHERE t.period = :period"),
    @NamedQuery(name = "TxOutput.findByTaxperiodfrom", query = "SELECT t FROM TxOutput t WHERE t.taxperiodfrom = :taxperiodfrom"),
    @NamedQuery(name = "TxOutput.findByTaxperiodto", query = "SELECT t FROM TxOutput t WHERE t.taxperiodto = :taxperiodto"),
    @NamedQuery(name = "TxOutput.findByTotalAmt", query = "SELECT t FROM TxOutput t WHERE t.totalAmt = :totalAmt"),
    @NamedQuery(name = "TxOutput.findByTotalTaxamt", query = "SELECT t FROM TxOutput t WHERE t.totalTaxamt = :totalTaxamt"),
    @NamedQuery(name = "TxOutput.findByTaxreturndate", query = "SELECT t FROM TxOutput t WHERE t.taxreturndate = :taxreturndate"),
    @NamedQuery(name = "TxOutput.findByName", query = "SELECT t FROM TxOutput t WHERE t.name = :name"),
    @NamedQuery(name = "TxOutput.findByPrepareid", query = "SELECT t FROM TxOutput t WHERE t.prepareid = :prepareid"),
    @NamedQuery(name = "TxOutput.findByPrepareby", query = "SELECT t FROM TxOutput t WHERE t.prepareby = :prepareby"),
    @NamedQuery(name = "TxOutput.findByPreparedate", query = "SELECT t FROM TxOutput t WHERE t.preparedate = :preparedate"),
    @NamedQuery(name = "TxOutput.findByLockedby", query = "SELECT t FROM TxOutput t WHERE t.lockedby = :lockedby"),
    @NamedQuery(name = "TxOutput.findByLockeddate", query = "SELECT t FROM TxOutput t WHERE t.lockeddate = :lockeddate"),
    @NamedQuery(name = "TxOutput.findByLoccode", query = "SELECT t FROM TxOutput t WHERE t.loccode = :loccode"),
    @NamedQuery(name = "TxOutput.findByLocdesc", query = "SELECT t FROM TxOutput t WHERE t.locdesc = :locdesc"),
    @NamedQuery(name = "TxOutput.findByCheckedid", query = "SELECT t FROM TxOutput t WHERE t.checkedid = :checkedid"),
    @NamedQuery(name = "TxOutput.findByCheckedby", query = "SELECT t FROM TxOutput t WHERE t.checkedby = :checkedby"),
    @NamedQuery(name = "TxOutput.findByCheckeddate", query = "SELECT t FROM TxOutput t WHERE t.checkeddate = :checkeddate"),
    @NamedQuery(name = "TxOutput.findByApproveid", query = "SELECT t FROM TxOutput t WHERE t.approveid = :approveid"),
    @NamedQuery(name = "TxOutput.findByApproveby", query = "SELECT t FROM TxOutput t WHERE t.approveby = :approveby"),
    @NamedQuery(name = "TxOutput.findByApprovedate", query = "SELECT t FROM TxOutput t WHERE t.approvedate = :approvedate"),
    @NamedQuery(name = "TxOutput.findByApprovedesignation", query = "SELECT t FROM TxOutput t WHERE t.approvedesignation = :approvedesignation"),
    @NamedQuery(name = "TxOutput.findByReconRefno", query = "SELECT t FROM TxOutput t WHERE t.reconRefno = :reconRefno")})
public class TxOutput implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "refno")
    private String refno;
    @Column(name = "year")
    private Integer year;
    @Column(name = "period")
    private Integer period;
    @Column(name = "taxperiodfrom")
    @Temporal(TemporalType.DATE)
    private Date taxperiodfrom;
    @Column(name = "taxperiodto")
    @Temporal(TemporalType.DATE)
    private Date taxperiodto;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "total_amt")
    private Double totalAmt;
    @Column(name = "total_taxamt")
    private Double totalTaxamt;
    @Column(name = "taxreturndate")
    @Temporal(TemporalType.DATE)
    private Date taxreturndate;
    @Size(max = 100)
    @Column(name = "name")
    private String name;
    @Size(max = 50)
    @Column(name = "prepareid")
    private String prepareid;
    @Size(max = 100)
    @Column(name = "prepareby")
    private String prepareby;
    @Column(name = "preparedate")
    @Temporal(TemporalType.DATE)
    private Date preparedate;
    @Size(max = 100)
    @Column(name = "lockedby")
    private String lockedby;
    @Column(name = "lockeddate")
    @Temporal(TemporalType.DATE)
    private Date lockeddate;
    @Size(max = 4)
    @Column(name = "loccode")
    private String loccode;
    @Size(max = 100)
    @Column(name = "locdesc")
    private String locdesc;
    @Size(max = 100)
    @Column(name = "checkedid")
    private String checkedid;
    @Size(max = 100)
    @Column(name = "checkedby")
    private String checkedby;
    @Column(name = "checkeddate")
    @Temporal(TemporalType.DATE)
    private Date checkeddate;
    @Size(max = 100)
    @Column(name = "approveid")
    private String approveid;
    @Size(max = 100)
    @Column(name = "approveby")
    private String approveby;
    @Column(name = "approvedate")
    @Temporal(TemporalType.DATE)
    private Date approvedate;
    @Size(max = 100)
    @Column(name = "approvedesignation")
    private String approvedesignation;
    @Size(max = 100)
    @Column(name = "recon_refno")
    private String reconRefno;

    public TxOutput() {
    }

    public TxOutput(String refno) {
        this.refno = refno;
    }

    public String getRefno() {
        return refno;
    }

    public void setRefno(String refno) {
        this.refno = refno;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public Date getTaxperiodfrom() {
        return taxperiodfrom;
    }

    public void setTaxperiodfrom(Date taxperiodfrom) {
        this.taxperiodfrom = taxperiodfrom;
    }

    public Date getTaxperiodto() {
        return taxperiodto;
    }

    public void setTaxperiodto(Date taxperiodto) {
        this.taxperiodto = taxperiodto;
    }

    public Double getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(Double totalAmt) {
        this.totalAmt = totalAmt;
    }

    public Double getTotalTaxamt() {
        return totalTaxamt;
    }

    public void setTotalTaxamt(Double totalTaxamt) {
        this.totalTaxamt = totalTaxamt;
    }

    public Date getTaxreturndate() {
        return taxreturndate;
    }

    public void setTaxreturndate(Date taxreturndate) {
        this.taxreturndate = taxreturndate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrepareid() {
        return prepareid;
    }

    public void setPrepareid(String prepareid) {
        this.prepareid = prepareid;
    }

    public String getPrepareby() {
        return prepareby;
    }

    public void setPrepareby(String prepareby) {
        this.prepareby = prepareby;
    }

    public Date getPreparedate() {
        return preparedate;
    }

    public void setPreparedate(Date preparedate) {
        this.preparedate = preparedate;
    }

    public String getLockedby() {
        return lockedby;
    }

    public void setLockedby(String lockedby) {
        this.lockedby = lockedby;
    }

    public Date getLockeddate() {
        return lockeddate;
    }

    public void setLockeddate(Date lockeddate) {
        this.lockeddate = lockeddate;
    }

    public String getLoccode() {
        return loccode;
    }

    public void setLoccode(String loccode) {
        this.loccode = loccode;
    }

    public String getLocdesc() {
        return locdesc;
    }

    public void setLocdesc(String locdesc) {
        this.locdesc = locdesc;
    }

    public String getCheckedid() {
        return checkedid;
    }

    public void setCheckedid(String checkedid) {
        this.checkedid = checkedid;
    }

    public String getCheckedby() {
        return checkedby;
    }

    public void setCheckedby(String checkedby) {
        this.checkedby = checkedby;
    }

    public Date getCheckeddate() {
        return checkeddate;
    }

    public void setCheckeddate(Date checkeddate) {
        this.checkeddate = checkeddate;
    }

    public String getApproveid() {
        return approveid;
    }

    public void setApproveid(String approveid) {
        this.approveid = approveid;
    }

    public String getApproveby() {
        return approveby;
    }

    public void setApproveby(String approveby) {
        this.approveby = approveby;
    }

    public Date getApprovedate() {
        return approvedate;
    }

    public void setApprovedate(Date approvedate) {
        this.approvedate = approvedate;
    }

    public String getApprovedesignation() {
        return approvedesignation;
    }

    public void setApprovedesignation(String approvedesignation) {
        this.approvedesignation = approvedesignation;
    }

    public String getReconRefno() {
        return reconRefno;
    }

    public void setReconRefno(String reconRefno) {
        this.reconRefno = reconRefno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refno != null ? refno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TxOutput)) {
            return false;
        }
        TxOutput other = (TxOutput) object;
        if ((this.refno == null && other.refno != null) || (this.refno != null && !this.refno.equals(other.refno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lcsb.fms.model.financial.tx.TxOutput[ refno=" + refno + " ]";
    }
    
}
