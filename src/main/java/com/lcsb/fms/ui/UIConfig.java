/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.ui;

import com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO;
import com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO;
import com.lcsb.fms.dao.financial.cashbook.PaymentVoucherDAO;
import com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.JournalVoucherDAO;
import com.lcsb.fms.dao.financial.gl.PostDAO;
import com.lcsb.fms.ui.button.Button;
import com.lcsb.fms.util.dao.GetStatusDAO;
import com.lcsb.fms.util.dao.ModuleDAO;
import com.lcsb.fms.util.model.LoginProfile;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class UIConfig {

    public static String renderMainButton(LoginProfile log, String refer, String moduleid) throws Exception {

        String moduledesc = ModuleDAO.getModule(log, moduleid).getModuleDesc();
        String status = GetStatusDAO.getStatus(log, refer);
        String render = "";

        if (!status.equals("Approved") && !status.equals("Canceled")) {

            if ((GetStatusDAO.isCheck(log, refer) && log.getListUserAccess().get(2).getAccess())) {

                render += Button.approveButton(moduleid, refer);

            } else if (!GetStatusDAO.isApprove(log, refer) && log.getListUserAccess().get(1).getAccess()) {

                render += Button.checkButton(moduleid, refer);

            }
            render += Button.viewGL(moduleid, refer)
                    + Button.replicateButton(moduleid, refer)
                    + Button.viewPrintedButton(moduleid, refer)
                    + Button.updateButton(moduleid, refer)
                    + Button.cancelButton(moduleid, refer)
                    + Button.deleteMainButton(moduleid, refer);

        } else if (status.equals("Canceled")) {

        } else {

            render += Button.viewGL(moduleid, refer)
                    + Button.replicateButton(moduleid, refer)
                    + Button.viewPrintedButton(moduleid, refer);

        }

        return render;

    }

    public static String renderFirstSubButton(LoginProfile log, String refer, String moduleid) throws Exception {

        String moduledesc = ModuleDAO.getModule(log, moduleid).getModuleDesc();
        String status = GetStatusDAO.getStatus(log, refer);
        String render = "";

        if (!status.equals("Approved") && !status.equals("Canceled")) {

            if (refer.substring(0, 3).equals("ORN")) {
                render += Button.addSubEditListButton(moduleid, refer, "adddebitnote", "adddebitnote", "From Debit Note", "add");
                render += Button.addSubEditListButton(moduleid, refer, "addcredit", "addcredit", "Credit Account", "add");
            }else if (refer.substring(0, 3).equals("CVN")) {
                //render += Button.addSubEditListButton(moduleid, refer, "adddebitnote", "adddebitnote", "From Debit Note", "add");
                render += Button.addSubEditListButton(moduleid, refer, "addaccount", "addaccount", "Account", "add");
            }
            
        }

        return render;
    }

    public static String renderSecondSubButton(LoginProfile log, String refer, String moduleid) throws Exception {

        String moduledesc = ModuleDAO.getModule(log, moduleid).getModuleDesc();
        String status = GetStatusDAO.getStatus(log, refer);
        String render = "";

        if (!status.equals("Approved") && !status.equals("Canceled")) {

            if (refer.substring(0, 3).equals("ORN")) {
                render += Button.addSubEditListButton(moduleid, refer, "addcheque", "addcheque", "Cheque", "add");
            }else if (refer.substring(0, 3).equals("CVN")) {
                render += Button.addSubEditListButton(moduleid, refer, "addreceipt", "addreceipt", "Receipt", "add");
            }

        }

        return render;
    }
    
    public static String renderActionGroupButton(LoginProfile log, String refer, String moduleid, boolean[] array, String[] arrayClass , String[] arrayName) throws Exception {

        String moduledesc = ModuleDAO.getModule(log, moduleid).getModuleDesc();
        String status = GetStatusDAO.getStatus(log, refer);
        String render = "<div class=\"btn-group btn-group-xs\" role=\"group\" aria-label=\"...\">";

        if (!status.equals("Approved") && !status.equals("Canceled")) {
            
            if(array[0]){
                render += Button.addSubEditListButton(moduleid, refer, arrayClass[0], arrayName[0], "", "add");
            }
            if(array[1]){
                render += Button.addSubEditListButton(moduleid, refer, arrayClass[1], arrayName[1], "", "edit");
            }
            if(array[2]){
                render += Button.addSubEditListButton(moduleid, refer, arrayClass[2], arrayName[2], "", "delete");
            }
            
        }
        
        render += "</div>";
        
        return render;
    }
    
    public static String renderBanner(LoginProfile log, String refer, String moduleid) throws Exception {

        String moduledesc = ModuleDAO.getModule(log, moduleid).getModuleDesc();
        String status = GetStatusDAO.getStatus(log, refer);
        String render = "";

        String str = "";
        
        boolean hasDCNote = false;
        boolean isComplete = false;
        String type = "";
        
        if (refer.substring(0, 3).equals("ORN")) {
            type = "OR";
            hasDCNote = OfficialReceiptDAO.hasDCNote(log, refer);
            isComplete = OfficialReceiptDAO.isComplete(log, refer);
        }else if (refer.substring(0, 3).equals("INV")) {
            type = "SNV";
            hasDCNote = SalesInvoiceDAO.hasDCNote(log, refer);
            isComplete = SalesInvoiceDAO.isComplete(log, refer);
        }else if (refer.substring(0, 3).equals("JVN") || refer.substring(0, 3).equals("JTX")) {
            type = "JV";
            hasDCNote = JournalVoucherDAO.hasDCNote(log, refer);
            isComplete = JournalVoucherDAO.isComplete(log, refer);
        }else if (refer.substring(0, 3).equals("PVN")) {
            type = "PV";
            hasDCNote = PaymentVoucherDAO.hasDCNote(log, refer);
            isComplete = PaymentVoucherDAO.isComplete(log, refer);
        }
        
        if(hasDCNote){
            Logger.getLogger(JournalVoucherDAO.class.getName()).log(Level.INFO, "****Has Credit/Debit Note****");
        }else{
            Logger.getLogger(JournalVoucherDAO.class.getName()).log(Level.INFO, "****Does not has Credit/Debit Note****");
        }

        if (!DebitCreditNoteDAO.hasDebitCreditNote(log, refer) && hasDCNote) {
            str = "Credit/Debit Note will be generated, you can create Credit/Debit Note by approving this " + moduledesc + " or  <strong><a title=\"" + moduleid + "\" id=\"" + refer + "\" class=\"thelink createnote\">create Credit/Debit Note now. </a></strong>";

        } else if (hasDCNote) {
            str = "Credit/Debit Note has been generated,  <strong>" + DebitCreditNoteDAO.getDebitCreditNoteLink(log, refer, moduleid) + "</strong>";
        }

        if (isComplete && !PostDAO.checkReadyToPost(log, refer, type) && !status.equals("Approved") && !status.equals("Canceled")) {

            if (PostDAO.isInPeriod(log, refer, type)) {
                render += "<div class=\"alert alert-success alert-dismissible fade in\" role=\"alert\">"
                        + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>"
                        + "<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp; This " + moduledesc + " is ready. " + str + "</div>";

            } else {

                render += "<div class=\"alert alert-info alert-dismissible fade in\" role=\"alert\">"
                        + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>"
                        + "<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp; This " + moduledesc + " is ready, but not in current Accounting Period. " + str + "</div>";

            }
        } else if (!status.equals("Approved") && !status.equals("Canceled")) {
            render += "<div class=\"alert alert-warning alert-dismissible fade in\" role=\"alert\">"
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>"
                    + "<i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp; " + moduledesc + " is not complete yet.</div>";

        } else if (status.equals("Canceled")) {
            render += "<div class=\"alert alert-default alert-dismissible fade in\" role=\"alert\">"
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>"
                    + "<i class=\"fa fa-ban\" aria-hidden=\"true\"></i>&nbsp;&nbsp; " + moduledesc + " was canceled.</div>";

        }else if (status.equals("Approved")) {
            render += "<div class=\"alert alert-success alert-dismissible fade in\" role=\"alert\">"
                        + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>"
                        + "<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp; This " + moduledesc + " was <strong>approved</strong>. " + str + "</div>";

        }

        return render;

    }


}
