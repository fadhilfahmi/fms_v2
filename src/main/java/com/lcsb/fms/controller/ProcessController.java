/*
 * This is FMS for Finance Management System 
 * Licensed to LKPP Corporation SDN BHD * 
 */
package com.lcsb.fms.controller;

import com.lcsb.fms.dao.administration.om.UserDAO;
import com.lcsb.fms.dao.development.PostTransDAO;
import com.lcsb.fms.dao.development.UnapproveDAO;
import com.lcsb.fms.dao.financial.ap.VendorInvoiceDAO;
import com.lcsb.fms.dao.financial.ap.VendorPaymentDAO;
import com.lcsb.fms.dao.financial.ar.ArContractDAO;
import com.lcsb.fms.dao.financial.ar.ArCreditNoteDAO;
import com.lcsb.fms.dao.financial.ar.ArDebitNoteDAO;
import com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO;
import com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO;
import com.lcsb.fms.dao.financial.cashbook.PaymentVoucherDAO;
import com.lcsb.fms.dao.financial.employee.EmPayrollDAO;
import com.lcsb.fms.dao.financial.gl.AccountPeriodDAO;
import com.lcsb.fms.dao.financial.gl.CnOPeriodDAO;
import com.lcsb.fms.dao.financial.gl.CreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.DebitNoteDAO;
import com.lcsb.fms.dao.financial.gl.DistributeDAO;
import com.lcsb.fms.dao.financial.tx.TaxMasterDAO;
import com.lcsb.fms.dao.management.activity.LotActivityDAO;
import com.lcsb.fms.dao.setup.company.ManagementCpDAO;
import com.lcsb.fms.dao.setup.company.MillDAO;
import com.lcsb.fms.dao.setup.configuration.BuyerDAO;
import com.lcsb.fms.dao.setup.configuration.LotMemberDAO;
import com.lcsb.fms.dao.setup.configuration.SupplierDAO;
import com.lcsb.fms.dashboard.DashboardDonutUsage;
import com.lcsb.fms.dashboard.DashboardDonutUsageDAO;
import com.lcsb.fms.financial.gl.post.Distribute;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.ap.ApTempInvoiceAmount;
import com.lcsb.fms.model.financial.ar.ArContractAgree;
import com.lcsb.fms.model.financial.ar.ArFFB;
import com.lcsb.fms.model.financial.ar.ArTempMaster;
import com.lcsb.fms.model.financial.ar.ArTempRefinery;
import com.lcsb.fms.model.financial.cashbook.ORTempCredit;
import com.lcsb.fms.model.financial.cashbook.ORTempInvoiceAmount;
import com.lcsb.fms.model.financial.cashbook.OfficialReceipt;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucher;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherTempDebitAccount;
import com.lcsb.fms.model.management.activity.LotProfitCalc;
import com.lcsb.fms.model.management.activity.LotTempMember;
import com.lcsb.fms.model.setup.configuration.LotMember;
import com.lcsb.fms.sys.log.SysLog;
import com.lcsb.fms.sys.log.SysLogDAO;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.ext.ParseSafely;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Dell
 */
@WebServlet(name = "ProcessController", urlPatterns = {"/ProcessController"})
public class ProcessController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String moduleid = request.getParameter("moduleid");
        String process = request.getParameter("process");
        HttpSession session = request.getSession();
        LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

        SysLog sys = new SysLog();

        sys.setModuleid(moduleid);
        sys.setProcess(process);
        sys.setUserid(log.getUserID());
        sys.setDate(AccountingPeriod.getCurrentTimeStamp());
        sys.setTime(AccountingPeriod.getCurrentTime());
        sys.setLog(false);

        try {
            switch (moduleid) {
                case "000000"://Debit Note Receive
                    switch (process) {
                        case "checkDCnote":
                            out.println(DebitCreditNoteDAO.checkDCNoteExist(log, request.getParameter("referno")));
                            break;
                        case "unapprove":
                            out.println(UnapproveDAO.unapproveNow(log, request.getParameter("refer")));
                            break;
                        case "connecttomill":
                            try {

                                String stat = String.valueOf(ConnectionUtil.createMillConnection(MillDAO.getDefaultMill(log).getCode()));
                                if (stat.equals("null")) {
                                    out.println(0);
                                } else {
                                    out.println(1);
                                }
                            } catch (SQLException e) {
                                e.printStackTrace();
                                out.println(0);
                            }
                            break;
                        case "postall":
                            out.println(PostTransDAO.postAll(log));
                            break;
                        case "monthactivity":
                            out.println(UserDAO.getMonthActivity(log));
                            break;
                        case "donutchart":
                            out.println(UserDAO.getDonutChart(log));
                            break;
                        default:
                            break;
                    }
                    break;
                case "000001"://Module Breadcrumb
                    out.println(GeneralTerm.getModuleBreadcrumb(log, process));

                    break;
                case "000002"://Activity
                    switch (process) {
                        case "loadactivity":
                            out.println(SysLogDAO.viewLog(log, Integer.parseInt(request.getParameter("page"))));
                            break;
                        default:
                            break;
                    }

                    break;
                case "080302"://Login Log
                    switch (process) {
                        case "loadloginlog":
                            out.println(SysLogDAO.viewLoginLog(log, Integer.parseInt(request.getParameter("page"))));
                            break;
                        default:
                            break;
                    }

                    break;
                case "020110":
            //Close / Open Period
            switch (process) {
                case "finalize":
                    Logger.getLogger(ProcessController.class.getName()).log(Level.INFO, "---Finalizing in process---");
                    out.println(CnOPeriodDAO.finalizingAccount(log));
                    sys.setLog(true);
                    sys.setRemark("Account Finalized");
                    break;
                case "confirm":
                    out.println(CnOPeriodDAO.confirmClose(log));
                    sys.setLog(true);
                    sys.setRemark("Period Closed & Changed");
                    break;
                case "listmodule":
                    out.println(CnOPeriodDAO.getModuleList(log));
                    break;
                case "scantransactions":
                    out.println(CnOPeriodDAO.getModuleList(log));
                    break;
                case "modulearray":
                    out.println(CnOPeriodDAO.getModuleArray(log));
                    break;
                case "scanmodule":
                    out.println(CnOPeriodDAO.checkTransactionForModule(log, request.getParameter("modulescan")));
                    break;
                default:
                    break;
            }
                    break;
                case "020101":
                    //Close / Open Period
                    if (process.equals("setnewperiod")) {

                        out.println(AccountPeriodDAO.setNewPeriod(log, request.getParameter("year"), request.getParameter("period")));
                        sys.setLog(true);
                        sys.setRemark("New Period Set (" + request.getParameter("year") + "," + request.getParameter("period") + ")");

                    }
                    break;
                case "020902":
                    //Close / Open Period
                    switch (process) {
                        case "getDiff":
                            out.println(ArContractDAO.getDiffCount(log, MillDAO.getDefaultMill(log).getCode()));
                            break;
                        case "getDiffMill":
                            out.println(ArContractDAO.getDiffCountAgreement(log, MillDAO.getDefaultMill(log).getCode(), request.getParameter("buyercode")));
                            break;
                        default:
                            break;
                    }
                    break;
                case "020903"://Sales Invoice
                    switch (process) {
                        case "tempmaster":
                            ArTempMaster am = new ArTempMaster();

                            am.setAddress(request.getParameter("address"));
                            am.setAmountstring(request.getParameter("amountstring"));
                            am.setBuyercode(request.getParameter("buyercode"));
                            am.setBuyername(request.getParameter("buyername"));
                            am.setCity(request.getParameter("bcity"));
                            am.setCoacode(request.getParameter("coacode"));
                            am.setCoadescp(request.getParameter("coadescp"));
                            am.setCompanycode(request.getParameter("companycode"));
                            am.setCompanyname(request.getParameter("companyname"));
                            am.setContractno(request.getParameter("contractno"));
                            am.setDate(request.getParameter("date"));
                            am.setGstid(request.getParameter("gstid"));
                            am.setPeriod(request.getParameter("period"));
                            am.setPostcode(request.getParameter("postcode"));
                            am.setRacoacode(request.getParameter("racoacode"));
                            am.setRacoadescp(request.getParameter("racoadescp"));
                            am.setRemark(request.getParameter("remark"));
                            am.setState(request.getParameter("state"));
                            am.setTotalamount(Double.parseDouble(request.getParameter("totalamount")));
                            am.setYear(request.getParameter("year"));
                            am.setLoccode(request.getParameter("loccode"));
                            am.setLoclevel(request.getParameter("loclevel"));
                            am.setLocdesc(request.getParameter("locdesc"));
                            am.setUnitprice(Double.parseDouble(request.getParameter("unitprice")));
                            am.setPid(request.getParameter("pid"));
                            am.setPname(request.getParameter("pname"));
                            am.setPdate(request.getParameter("pdate"));
                            am.setProdcode(request.getParameter("prodcode"));
                            am.setProdname(request.getParameter("prodname"));
                            am.setSessionid(request.getParameter("sessionid"));
                            am.setOldinvno(request.getParameter("oldinvno"));
                            am.setOrderno(request.getParameter("orderno"));

                            out.println(SalesInvoiceDAO.saveIntoTempMaster(log, am));
                            break;
                        case "temprefinery":
                            String[] values = request.getParameterValues("dispatchno");
                            List<ArTempRefinery> at = new ArrayList();

                            for (int i = 0; i < values.length; i++) {

                                if (!values[i].equals("")) {
                                    Logger.getLogger(ProcessController.class.getName()).log(Level.INFO, "------" + values[i]);

                                    ArTempRefinery ar = new ArTempRefinery();

                                    ar.setSessionid(request.getParameter("sessionid"));
                                    ar.setContractno(request.getParameter("contractno"));
                                    ar.setDate(request.getParameter("date" + values[i]));
                                    ar.setDispatchno(request.getParameter("dispatchno" + values[i]));
                                    ar.setMtMill(ParseSafely.parseDoubleSafely(request.getParameter("mt_mill" + values[i])));
                                    ar.setMtRefinery(ParseSafely.parseDoubleSafely(request.getParameter("mt_refinery" + values[i])));
                                    ar.setTrailer(request.getParameter("trailer" + values[i]));

                                    at.add(ar);
                                }

                            }

                            out.println(SalesInvoiceDAO.saveIntoTempRefinery(log, at));
                            break;
                        case "generatedisno":
                            out.println(SalesInvoiceDAO.generateDispatchNo(log, request.getParameter("prodcode")));
                            break;
                        case "updateprice":
                            SalesInvoiceDAO.updatePrice(log, ParseSafely.parseDoubleSafely(request.getParameter("price")), request.getParameter("contractno"));
                            break;
                        case "setnewprice":
                            out.println(SalesInvoiceDAO.setNewPrice(log, ParseSafely.parseDoubleSafely(request.getParameter("price")), request.getParameter("refer")));
                            break;
                        case "calculateffb":

                            ArFFB ar = new ArFFB();
                            ar.setCess(ParseSafely.parseDoubleSafely(request.getParameter("cess")));
                            ar.setDiscount(ParseSafely.parseDoubleSafely(request.getParameter("discount")));
                            ar.setFfb(ParseSafely.parseDoubleSafely(request.getParameter("ffb")));
                            ar.setKer(ParseSafely.parseDoubleSafely(request.getParameter("ker")));
                            ar.setMpobpricepk(ParseSafely.parseDoubleSafely(request.getParameter("mpobpricepk")));
                            ar.setMillcost(ParseSafely.parseDoubleSafely(request.getParameter("millcost")));
                            ar.setMpobpricecpo(ParseSafely.parseDoubleSafely(request.getParameter("mpobpricecpo")));
                            ar.setOer(ParseSafely.parseDoubleSafely(request.getParameter("oer")));
                            ar.setTransport(ParseSafely.parseDoubleSafely(request.getParameter("transport")));
                            ar.setWinfall(ParseSafely.parseDoubleSafely(request.getParameter("winfall")));
                            ar.setExternalffb(ParseSafely.parseDoubleSafely(request.getParameter("externalffb")));
                            ar.setYear(request.getParameter("year"));
                            ar.setMonth(request.getParameter("month"));
                            out.println(SalesInvoiceDAO.saveFFB(log, ar, request.getParameter("sessionid")));
                            break;
                        case "syncrefinery":
                            out.println(SalesInvoiceDAO.syncRefinery(log));
                            break;
                        case "updaterefinery":
                            out.println(SalesInvoiceDAO.updateRefineryAmount(log, request.getParameter("dispatchno"), ParseSafely.parseDoubleSafely(request.getParameter("newvalue")), request.getParameter("prodcode")));
                            break;
                        case "check": {
                            SalesInvoiceDAO.checkVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname());
                            DashboardDonutUsageDAO.saveUsage(log, moduleid, request.getParameter("referno"), process);
                            out.println(1);
                            break;
                        }
                        case "approve": {
                            Distribute dist = (Distribute) DistributeDAO.distributeNow(request.getParameter("referno"), "SNV", log);
                            session.setAttribute("post_detail", dist);
                            if (dist.isSuccess()) {
                                out.println(0);
                            } else {
                                SalesInvoiceDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());

                                DashboardDonutUsageDAO.saveUsage(log, moduleid, request.getParameter("referno"), process);
                                out.println(1);
                            }

                            break;
                        }
                        default:
                            break;
                    }
                    break;
                case "020908"://Sales Debit Note
                    switch (process) {
                        case "check":
                            ArDebitNoteDAO.checkVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname());
                            DashboardDonutUsageDAO.saveUsage(log, moduleid, request.getParameter("referno"), process);
                            out.println(1);
                            break;
                        case "approve":
                            Distribute dist = (Distribute) DistributeDAO.distributeNow(request.getParameter("referno"), "DNR", log);
                            session.setAttribute("post_detail", dist);
                            if (dist.isSuccess()) {
                                out.println(0);
                            } else {
                                ArDebitNoteDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());
                                DashboardDonutUsageDAO.saveUsage(log, moduleid, request.getParameter("referno"), process);
                                out.println(1);
                            }

                            break;
                        default:
                            break;
                    }
                    break;
                case "020909"://Sales Credit Note
                    switch (process) {
                        case "check":
                            ArCreditNoteDAO.checkVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname());
                            DashboardDonutUsageDAO.saveUsage(log, moduleid, request.getParameter("referno"), process);
                            out.println(1);
                            break;
                        case "approve":
                            Distribute dist = (Distribute) DistributeDAO.distributeNow(request.getParameter("referno"), "CNR", log);
                            session.setAttribute("post_detail", dist);
                            if (dist.isSuccess()) {
                                out.println(0);
                            } else {
                                ArCreditNoteDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());
                                DashboardDonutUsageDAO.saveUsage(log, moduleid, request.getParameter("referno"), process);
                                out.println(1);
                            }

                            break;
                        default:
                            break;
                    }
                    break;
                case "021009"://Vendor Payment
                    switch (process) {
                        case "tempmaster":
                            PaymentVoucher data = new PaymentVoucher();
                            data.setTarikh(request.getParameter("tarikh"));
                            data.setPaidtype(request.getParameter("paidtype"));
                            data.setPaidcode(request.getParameter("paidcode"));
                            data.setPaidname(request.getParameter("paidname"));
                            data.setPaidaddress(request.getParameter("paidaddress"));
                            data.setPaidcity(request.getParameter("paidcity"));
                            data.setPaidstate(request.getParameter("paidstate"));
                            data.setPaidpostcode(request.getParameter("paidpostcode"));
                            data.setGstid(request.getParameter("gstid"));
                            data.setRemarks(request.getParameter("remarks"));
                            data.setRacoacode(request.getParameter("racoacode"));
                            data.setRacoadesc(request.getParameter("racoadesc"));
                            data.setAmount(Double.parseDouble(request.getParameter("amount")));
                            data.setRm(request.getParameter("rm"));
                            data.setPaymentmode(request.getParameter("paymentmode"));
                            data.setBankcode(request.getParameter("bankcode"));
                            data.setBankname(request.getParameter("bankname"));
                            data.setCekno(request.getParameter("cekno"));
                            data.setYear(request.getParameter("year"));
                            data.setPeriod(request.getParameter("period"));
                            data.setEstatecode(request.getParameter("estatecode"));
                            data.setEstatename(request.getParameter("estatename"));
                            data.setFlagbank("NO");
                            data.setSessionID(request.getParameter("sessionid"));

                            out.println(VendorPaymentDAO.saveIntoTempMaster(log, data));
                            break;
                        case "tempamount":
                            int row = Integer.parseInt(request.getParameter("totalrow"));
                            List<ApTempInvoiceAmount> at = new ArrayList();

                            for (int i = 1; i < row + 1; i++) {

                                ApTempInvoiceAmount ar = new ApTempInvoiceAmount();

                                ar.setSessionid(request.getParameter("sessionid"));
                                ar.setSuppcode(request.getParameter("suppcode"));
                                ar.setInvrefno(request.getParameter("invrefno" + i));
                                ar.setAmounttopay(GeneralTerm.amountFormattoSave(request.getParameter("amount" + i)));
                                at.add(ar);

                            }

                            out.println(VendorPaymentDAO.saveIntoTempInvoice(log, at));
                            break;
                        case "check":
                            VendorPaymentDAO.checkVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname());
                            out.println(1);
                            break;
                        case "approve":
                            Distribute dist = (Distribute) DistributeDAO.distributeNow(request.getParameter("referno"), "PVN", log);
                            session.setAttribute("post_detail", dist);
                            if (dist.isSuccess()) {
                                out.println(0);
                            } else {
                                VendorPaymentDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());
                                out.println(1);
                            }

                            break;
                        default:
                            break;
                    }
                    break;
                case "021008"://Vendor Invoice
                    switch (process) {
                        case "check":
                            VendorInvoiceDAO.checkVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname());
                            out.println(1);
                            break;
                        case "approve":
                            Distribute dist = (Distribute) DistributeDAO.distributeNow(request.getParameter("referno"), "PNV", log);
                            session.setAttribute("post_detail", dist);
                            if (dist.isSuccess()) {
                                out.println(0);
                            } else {
                                VendorInvoiceDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());
                                out.println(1);
                            }

                            break;
                        default:
                            break;
                    }
                    break;
                case "020204"://Payment Voucher
                    switch (process) {
                        case "check":
                            VendorPaymentDAO.checkVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname());
                            out.println(1);
                            break;
                        case "approve":
                            Distribute dist = (Distribute) DistributeDAO.distributeNow(request.getParameter("referno"), "PV", log);
                            session.setAttribute("post_detail", dist);
                            if (dist.isSuccess()) {
                                out.println(0);
                            } else {
                                VendorPaymentDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());
                                out.println(1);
                            }

                            break;
                        default:
                            break;
                    }
                    break;
                case "020207"://(Bank Report) Bank's Voucher or/pv
                    switch (process) {
                        case "tempmaster":
                            PaymentVoucher data = new PaymentVoucher();
                            data.setTarikh(request.getParameter("tarikh"));
                            data.setPaidtype(request.getParameter("paidtype"));
                            data.setPaidcode(request.getParameter("paidcode"));
                            data.setPaidname(request.getParameter("paidname"));
                            data.setPaidaddress(request.getParameter("paidaddress"));
                            data.setPaidcity(request.getParameter("paidcity"));
                            data.setPaidstate(request.getParameter("paidstate"));
                            data.setPaidpostcode(request.getParameter("paidpostcode"));
                            data.setGstid(request.getParameter("gstid"));
                            data.setRemarks(request.getParameter("remarks"));
                            data.setRacoacode(request.getParameter("racoacode"));
                            data.setRacoadesc(request.getParameter("racoadesc"));
                            data.setAmount(Double.parseDouble(request.getParameter("amount")));
                            data.setRm(request.getParameter("rm"));
                            data.setPaymentmode(request.getParameter("paymentmode"));
                            data.setBankcode(request.getParameter("bankcode"));
                            data.setBankname(request.getParameter("bankname"));
                            data.setCekno(request.getParameter("cekno"));
                            data.setYear(request.getParameter("year"));
                            data.setPeriod(request.getParameter("period"));
                            data.setEstatecode(request.getParameter("estatecode"));
                            data.setEstatename(request.getParameter("estatename"));
                            data.setFlagbank("YES");
                            data.setSessionID(request.getParameter("sessionid"));

                            out.println(PaymentVoucherDAO.saveIntoTempMaster(log, data));
                            break;
                        case "tempamount":

                            List<PaymentVoucherTempDebitAccount> at = new ArrayList();
                            PaymentVoucherTempDebitAccount ar = new PaymentVoucherTempDebitAccount();

                            ar.setSessionid(request.getParameter("sessionid"));
                            ar.setPartyCode(request.getParameter("bankcode"));
                            ar.setAmounttopay(GeneralTerm.amountFormattoSave(request.getParameter("amount")));
                            ar.setFrommodule(moduleid);
                            at.add(ar);

                            out.println(PaymentVoucherDAO.saveDebitAccIntoTemp(log, at));
                            break;

                        case "tempmasterOR":
                            OfficialReceipt data1 = new OfficialReceipt();
                            data1.setDate(request.getParameter("date"));
                            data1.setPaidtype(request.getParameter("paidtype"));
                            data1.setPaidcode(request.getParameter("paidcode"));
                            data1.setPaidname(request.getParameter("paidname"));
                            data1.setPaidaddress(request.getParameter("paidaddress"));
                            data1.setPaidcity(request.getParameter("paidcity"));
                            data1.setPaidstate(request.getParameter("paidstate"));
                            data1.setPaidpostcode(request.getParameter("paidpostcode"));
                            data1.setGstid(request.getParameter("gstid"));
                            data1.setRemarks(request.getParameter("remarks"));
                            data1.setRacoacode(request.getParameter("racoacode"));
                            data1.setRacoadesc(request.getParameter("racoadesc"));
                            data1.setAmount(Double.parseDouble(request.getParameter("amount")));
                            data1.setRm(request.getParameter("rm"));
                            data1.setReceivemode(request.getParameter("receivemode"));
                            data1.setBankcode(request.getParameter("bankcode"));
                            data1.setBankname(request.getParameter("bankname"));
                            data1.setYear(Integer.parseInt(request.getParameter("year")));
                            data1.setPeriod(Integer.parseInt(request.getParameter("period")));
                            data1.setStmtyear(request.getParameter("year"));
                            data1.setStmtperiod(request.getParameter("period"));
                            data1.setEstatecode(request.getParameter("estatecode"));
                            data1.setEstatename(request.getParameter("estatename"));
                            data1.setSessionID(request.getParameter("sessionid"));
                            data1.setFlagbank("YES");

                            out.println(OfficialReceiptDAO.saveIntoTempMaster(log, data1));
                            break;
                        case "tempamountOR":
                            List<ORTempCredit> at1 = new ArrayList();

                            ORTempCredit ar1 = new ORTempCredit();

                            ar1.setSessionid(request.getParameter("sessionid"));
                            ar1.setPartycode(request.getParameter("bankcode"));
                            ar1.setAmounttopay(GeneralTerm.amountFormattoSave(request.getParameter("amount")));
                            ar1.setFrommodule(moduleid);
                            at1.add(ar1);

                            out.println(OfficialReceiptDAO.saveIntoTempCredit(log, at1));
                            break;
                        default:
                            break;
                    }
                    break;

                case "020203"://Official Receipt
                    switch (process) {
                        case "tempmaster":
                            OfficialReceipt data = new OfficialReceipt();
                            data.setDate(request.getParameter("date"));
                            data.setPaidtype(request.getParameter("paidtype"));
                            data.setPaidcode(request.getParameter("paidcode"));
                            data.setPaidname(request.getParameter("paidname"));
                            data.setPaidaddress(request.getParameter("paidaddress"));
                            data.setPaidcity(request.getParameter("paidcity"));
                            data.setPaidstate(request.getParameter("paidstate"));
                            data.setPaidpostcode(request.getParameter("paidpostcode"));
                            data.setGstid(request.getParameter("gstid"));
                            data.setRemarks(request.getParameter("remarks"));
                            data.setRacoacode(request.getParameter("racoacode"));
                            data.setRacoadesc(request.getParameter("racoadesc"));
                            data.setAmount(Double.parseDouble(request.getParameter("amount")));
                            data.setRm(request.getParameter("rm"));
                            data.setReceivemode(request.getParameter("receivemode"));
                            data.setBankcode(request.getParameter("bankcode"));
                            data.setBankname(request.getParameter("bankname"));
                            data.setYear(Integer.parseInt(request.getParameter("year")));
                            data.setPeriod(Integer.parseInt(request.getParameter("period")));
                            data.setEstatecode(request.getParameter("estatecode"));
                            data.setEstatename(request.getParameter("estatename"));
                            data.setSessionID(request.getParameter("sessionid"));

                            out.println(OfficialReceiptDAO.saveIntoTempMaster(log, data));
                            break;
                        case "tempamount":
                            int row = Integer.parseInt(request.getParameter("totalrow"));
                            List<ORTempInvoiceAmount> at = new ArrayList();

                            for (int i = 1; i < row + 1; i++) {

                                ORTempInvoiceAmount ar = new ORTempInvoiceAmount();

                                ar.setSessionid(request.getParameter("sessionid"));
                                ar.setBuyercode(request.getParameter("buyercode"));
                                ar.setInvrefno(request.getParameter("invrefno" + i));
                                ar.setAmounttopay(GeneralTerm.amountFormattoSave(request.getParameter("amount" + i)));
                                at.add(ar);

                            }

                            out.println(OfficialReceiptDAO.saveIntoTempInvoice(log, at));
                            break;
                        case "check":
                            OfficialReceiptDAO.checkVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname());
                            out.println(1);
                            break;
                        case "approve":
                            Distribute dist = (Distribute) DistributeDAO.distributeNow(request.getParameter("referno"), "OR", log);
                            session.setAttribute("post_detail", dist);
                            if (dist.isSuccess()) {
                                out.println(0);
                            } else {
                                OfficialReceiptDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());
                                out.println(1);
                            }

                            break;
                        default:
                            break;
                    }
                    break;
                case "010204"://Management CP Info
                    switch (process) {
                        case "delete_bank":
                            out.println(ManagementCpDAO.deleteBank(log, request.getParameter("referno")));
                            break;
                        case "delete_petty":
                            out.println(ManagementCpDAO.deletePetty(log, request.getParameter("referno")));
                            break;
                        default:
                            break;
                    }
                    break;
                case "010118"://Supplier Info
                    switch (process) {
                        case "checkexisting":
                            out.println(SupplierDAO.checkExist(log, request.getParameter("refer")));
                            break;
                        case "retrieve":
                            out.println(SupplierDAO.retrieveData(log, request.getParameter("refer")));
                            break;
                        default:
                            break;
                    }
                    break;
                case "010117"://Buyer Info
                    switch (process) {
                        case "checkexisting":
                            out.println(BuyerDAO.checkExist(log, request.getParameter("refer")));
                            break;
                        case "retrieve":
                            out.println(BuyerDAO.retrieveData(log, request.getParameter("refer")));
                            break;
                        default:
                            break;
                    }
                    break;
                case "020119"://Debit Note Receive
                    switch (process) {
                        case "retrieve":
                            out.println(DebitNoteDAO.retrieveData(log, request.getParameter("refer"), request.getParameter("year"), request.getParameter("period")));
                            break;
                        default:
                            break;
                    }
                    break;
                case "020120"://Credit Note Receive
                    switch (process) {
                        case "retrieve":
                            out.println(CreditNoteDAO.retrieveData(log, request.getParameter("refer"), request.getParameter("year"), request.getParameter("period")));
                            break;
                        default:
                            break;
                    }
                    break;
                case "010127"://Lot Member
                    switch (process) {
                        case "delete_lot":
                            out.println(LotMemberDAO.deleteLot(log, request.getParameter("referno")));
                            break;
                        case "updatememberinfo":
                            LotMember lt = new LotMember();

                            lt.setAcc(request.getParameter("accno"));
                            lt.setAddress1(request.getParameter("address1"));
                            lt.setAddress2(request.getParameter("address2"));
                            lt.setAddress3(request.getParameter("address3"));
                            lt.setAddress4(request.getParameter("address4"));
                            lt.setAddress5(request.getParameter("address5"));
                            lt.setBank(request.getParameter("bank"));
                            lt.setIc(request.getParameter("ic"));
                            lt.setMemName(request.getParameter("name"));
                            lt.setMemCode(request.getParameter("code"));
                            lt.setPaymethod(request.getParameter("payment"));
                            lt.setStatus(request.getParameter("status"));

                            out.println(LotMemberDAO.updateMemberInfo(log, lt));
                            break;
                        default:
                            break;
                    }
                    break;
                case "021105":
                    //Close / Open Period
                    switch (process) {
                        case "confirm":
                            out.println(TaxMasterDAO.confirmClose(log));
                            break;
                        case "undotax":
                            out.println(TaxMasterDAO.confirmUndo(log));
                            break;
                        default:
                            break;
                    }
                    break;
                case "030103"://Sales Invoice
                    switch (process) {
                        case "tempmember":
                            String[] values = request.getParameterValues("memcode");
                            List<LotTempMember> at = new ArrayList();

                            for (int i = 0; i < values.length; i++) {

                                if (!values[i].equals("")) {
                                    Logger.getLogger(ProcessController.class.getName()).log(Level.INFO, "------" + values[i]);

                                    LotTempMember ar = new LotTempMember();

                                    ar.setSessionid(request.getParameter("sessionid"));
                                    ar.setLotcode(request.getParameter("lotcode"));
                                    ar.setYear(request.getParameter("year"));
                                    ar.setCode(values[i]);
                                    ar.setName(LotMemberDAO.getInfo(log, values[i]).getMemName());
                                    ar.setPeriod(request.getParameter("period"));
                                    ar.setQuarter(request.getParameter("quarter"));
                                    ar.setPrepareid(log.getUserID());

                                    at.add(ar);
                                }
                            }

                            out.println(LotActivityDAO.saveIntoTempMember(log, at));
                            break;
                        case "savecalculation":

                            LotProfitCalc ar = new LotProfitCalc();

                            ar.setSessionid(request.getParameter("sessionid"));
                            ar.setLotcode(request.getParameter("lotcode"));
                            ar.setLotdesc(request.getParameter("lotdesc"));
                            ar.setYear(request.getParameter("year"));
                            ar.setPeriod(request.getParameter("period"));
                            ar.setQuarter(request.getParameter("quarter"));
                            ar.setBalafterdist(ParseSafely.parseDoubleSafely(request.getParameter("bal-afterdist")));
                            ar.setCurrevenue(ParseSafely.parseDoubleSafely(request.getParameter("cur-profit")));
                            ar.setCuryearreplanting(ParseSafely.parseDoubleSafely(request.getParameter("year-prov")));
                            ar.setDistributableprofit(ParseSafely.parseDoubleSafely(request.getParameter("distributable-prof")));
                            ar.setDistributed(ParseSafely.parseDoubleSafely(request.getParameter("pre-dist")));
                            ar.setDistsuggest(ParseSafely.parseDoubleSafely(request.getParameter("suggested")));
                            ar.setLkpprevenue(ParseSafely.parseDoubleSafely(request.getParameter("lkpp-profit")));
                            ar.setPrerevenue(ParseSafely.parseDoubleSafely(request.getParameter("pre-profit")));
                            ar.setPreyearreplanting(ParseSafely.parseDoubleSafely(request.getParameter("preyear-prov")));
                            ar.setProfitperhect(ParseSafely.parseDoubleSafely(request.getParameter("prof-perhect")));
                            ar.setRollingcapital(ParseSafely.parseDoubleSafely(request.getParameter("rolling-prov")));
                            ar.setTotaldistributed(ParseSafely.parseDoubleSafely(request.getParameter("tot-distprofit")));
                            ar.setTotalrevenue(ParseSafely.parseDoubleSafely(request.getParameter("tot-profit")));

                            out.println(LotActivityDAO.saveProfitCalculated(log, ar));
                            break;
                        default:
                            break;
                    }
                    break;
                case "020309":
                    switch (process) {
                        case "deleteearning":
                            out.println(EmPayrollDAO.deleteEarning(log, request.getParameter("refer"), request.getParameter("code")));
                            break;
                        case "deletededuction":
                            out.println(EmPayrollDAO.deleteDeduction(log, request.getParameter("refer"), request.getParameter("code")));
                            break;
                        case "saveEarning":
                            out.println(EmPayrollDAO.saveAllEarning(log, request.getParameter("refer")));
                            break;
                        case "saveDeduction":
                            out.println(EmPayrollDAO.saveAllDeduction(log, request.getParameter("refer")));
                            break;
                        case "saveonspot":
                            out.println(EmPayrollDAO.updateChangedAmount(log, request.getParameter("refer"), request.getParameter("staffid"), request.getParameter("earncode"), ParseSafely.parseDoubleSafely(request.getParameter("amount")), request.getParameter("type")));
                            break;
                        case "confirmslip":
                            out.println(EmPayrollDAO.confirmSlip(log, request.getParameter("refer"), request.getParameter("staffid")));
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        } finally {
            out.close();
        }

        SysLogDAO.saveLog(log, sys);
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (Exception ex) {
            Logger.getLogger(ProcessController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (Exception ex) {
            Logger.getLogger(ProcessController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
