/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.management.contract;

import com.lcsb.fms.controller.PathController;
import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.management.contract.AgreementDAO;
import com.lcsb.fms.model.management.contract.Agreement;
import com.lcsb.fms.model.management.contract.AgreementJob;
import com.lcsb.fms.util.ext.ParseSafely;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_Agreement", urlPatterns = {"/Path_Agreement"})
public class Path_Agreement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";
            
            try {
                    Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------gggggg---" + urlsend);
                    Agreement data = new Agreement();
                    data.setAddress(request.getParameter("address"));
                    data.setAppdate(request.getParameter("appdate"));
                    data.setCode(request.getParameter("code"));
                    data.setConfirmagree(request.getParameter("confirmagree"));
                    data.setDate(request.getParameter("date"));
                    data.setDatecheck(request.getParameter("datecheck"));
                    data.setDateend(request.getParameter("dateend"));
                    data.setEstatecode(request.getParameter("estatecode"));
                    data.setEstatename(request.getParameter("estatename"));
                    data.setFlag(request.getParameter("flag"));
                    data.setGstid(request.getParameter("gstid"));
                    data.setHnoagree(request.getParameter("hnoagree"));
                    data.setIc(request.getParameter("ic"));
                    data.setIcpengurus(request.getParameter("icpengurus"));
                    data.setName(request.getParameter("name"));
                    data.setOwner(request.getParameter("owner"));
                    data.setPeriod(request.getParameter("period"));
                    data.setPredate(request.getParameter("predate"));
                    data.setPredesign(request.getParameter("predesign"));
                    data.setPreid(request.getParameter("preid"));
                    data.setPrename(request.getParameter("prename"));
                    data.setPtype(request.getParameter("ptype"));
                    data.setRefer(request.getParameter("refer"));
                    data.setSaksi(request.getParameter("saksi"));
                    data.setSaksiic(request.getParameter("saksiic"));
                    data.setSaksiid(request.getParameter("saksiid"));
                    data.setSdesignation(request.getParameter("sdesignation"));
                    data.setTkipercent(request.getParameter("tkipercent"));
                    data.setTypeagree(request.getParameter("typeagree"));
                    data.setYear(request.getParameter("year"));

                    AgreementJob job = new AgreementJob();
                    job.setAccode(request.getParameter("accode"));
                    job.setAcdesc(request.getParameter("acdesc"));
                    job.setEntercode(request.getParameter("entercode"));
                    job.setEnterdesc(request.getParameter("enterdesc"));
                    job.setLoccode(request.getParameter("fieldcode"));
                    job.setLocdesc(request.getParameter("fieldname"));
                    job.setFlag(request.getParameter("flag"));
                    job.setJobcode(request.getParameter("jobcode"));
                    job.setJobdescp(request.getParameter("jobdescp"));
                    job.setLoclevel(request.getParameter("level"));
                    job.setNo(request.getParameter("no"));
                    job.setOutputcode(request.getParameter("outputcode"));
                    job.setOutputdesc(request.getParameter("outputdesc"));
                    job.setPriceunit(request.getParameter("priceunit"));
                    job.setPricework(ParseSafely.parseDoubleSafely(request.getParameter("pricework")));
                    job.setPvenddate(request.getParameter("pvenddate"));
                    job.setPvpenalty(request.getParameter("pvpenalty"));
                    job.setPvpercent(request.getParameter("pvpercent"));
                    job.setPvqty(request.getParameter("pvqty"));
                    job.setPvstartdate(request.getParameter("pvstartdate"));
                    job.setRefer(request.getParameter("refer"));
                    job.setRemark(request.getParameter("remark"));
                    job.setStartdate(request.getParameter("startdate"));
                    job.setTaxamt(ParseSafely.parseDoubleSafely(request.getParameter("taxamt")));
                    job.setTaxcoacode(request.getParameter("taxcoacode"));
                    job.setTaxcoadescp(request.getParameter("taxcoadescp"));
                    job.setTaxcode(request.getParameter("taxcode"));
                    job.setTaxdescp(request.getParameter("taxdescp"));
                    job.setTaxrate(ParseSafely.parseDoubleSafely(request.getParameter("taxrate")));
                    job.setType(request.getParameter("type"));
                    job.setUnit(request.getParameter("unit"));
                    job.setUnitmeasure(request.getParameter("unitmeasure"));
                    job.setVotype(request.getParameter("votype"));

                    path = (PathModel) AgreementDAO.PathTo(log, moduleid, process, data, job, request.getParameter("referno"));
                    urlsend = path.getUrlSend();

                } catch (Exception ex) {
                    ex.printStackTrace();
                    Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------bbbb---" + ex);
                }
            
            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
