/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.ar;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.administration.om.UserDAO;
import com.lcsb.fms.dao.financial.ar.ArContractDAO;
import com.lcsb.fms.dao.setup.company.MillDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.model.administration.om.UserAccount;
import com.lcsb.fms.model.financial.ar.ArContractAgree;
import com.lcsb.fms.model.financial.ar.ArContractInfo;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_ARAgreement", urlPatterns = {"/Path_ARAgreement"})
public class Path_ARAgreement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/ar_agreement_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/ar_agreement_add.jsp?id=" + moduleid;
                    break;
                case "addagree":
                    urlsend = "/ar_agreement_add_agree.jsp?id=" + moduleid;
                    break;
                case "view":
                    urlsend = "/om_user_view.jsp?id=" + moduleid;
                    break;
                case "edit_st":
                    urlsend = "/ar_agreement_edit_agree.jsp?id=" + moduleid;
                    break;
                case "addprocess": {
                    ArContractInfo ua = new ArContractInfo();
                    ua.setActcode(request.getParameter("actcode"));
                    ua.setActdescp(request.getParameter("actdescp"));
                    ua.setActive(request.getParameter("active"));
                    ua.setAddress(request.getParameter("address"));
                    ua.setCode(request.getParameter("code"));
                    ua.setLoccode(request.getParameter("loccode"));
                    ua.setLocname(request.getParameter("locname"));
                    ua.setDescp(request.getParameter("descp"));
                    ArContractDAO.saveNew(log, ua);
                    urlsend = "/ar_agreement_list.jsp?id=" + moduleid;
                    break;
                }
                case "addagreeprocess": {
                    ArContractAgree ua = new ArContractAgree();
                    ua.setActive(request.getParameter("active"));
                    ua.setBrokercd(request.getParameter("brokercd"));
                    ua.setBrokerde(request.getParameter("brokerde"));
                    ua.setBrokernum(request.getParameter("brokernum"));
                    ua.setBuyercode(request.getParameter("buyercode"));
                    ua.setComacc(request.getParameter("comacc"));
                    ua.setComcd(request.getParameter("comcd"));
                    ua.setComde(request.getParameter("comde"));
                    ua.setDate(AccountingPeriod.convertStringtoDate(request.getParameter("date")));
                    ua.setDmonth(request.getParameter("dmonth"));
                    ua.setDyear(request.getParameter("dyear"));
                    ua.setEffectivedate(AccountingPeriod.convertStringtoDate(request.getParameter("effectivedate")));
                    ua.setLoccode(request.getParameter("loccode"));
                    ua.setLocname(request.getParameter("locname"));
                    ua.setMpob(request.getParameter("mpob"));
                    ua.setNo(request.getParameter("no"));
                    ua.setPrice(Double.parseDouble(request.getParameter("price")));
                    ua.setQty(Double.parseDouble(request.getParameter("qty")));
                    ArContractDAO.saveNewAgree(log, ua, log.getEstateCode(), log.getEstateDescp());
                    urlsend = "/ar_agreement_list.jsp?id=" + moduleid;
                    break;
                }
                case "editagreeprocess": {
                    ArContractAgree ua = new ArContractAgree();
                    ua.setId(Integer.parseInt(request.getParameter("id")));
                    ua.setActive(request.getParameter("active"));
                    ua.setBrokercd(request.getParameter("brokercd"));
                    ua.setBrokerde(request.getParameter("brokerde"));
                    ua.setBrokernum(request.getParameter("brokernum"));
                    ua.setBuyercode(request.getParameter("buyercode"));
                    ua.setComacc(request.getParameter("comacc"));
                    ua.setComcd(request.getParameter("comcd"));
                    ua.setComde(request.getParameter("comde"));
                    ua.setDate(AccountingPeriod.convertStringtoDate(request.getParameter("date")));
                    ua.setDmonth(request.getParameter("dmonth"));
                    ua.setDyear(request.getParameter("dyear"));
                    ua.setEffectivedate(AccountingPeriod.convertStringtoDate(request.getParameter("effectivedate")));
                    ua.setLoccode(request.getParameter("loccode"));
                    ua.setLocname(request.getParameter("locname"));
                    ua.setMpob(request.getParameter("mpob"));
                    ua.setNo(request.getParameter("no"));
                    ua.setPrice(Double.parseDouble(request.getParameter("price")));
                    ua.setQty(Double.parseDouble(request.getParameter("qty")));
                    ArContractDAO.updateAgree(log, ua);
                    urlsend = "/ar_agreement_view_list.jsp?refer=" + request.getParameter("buyercode");
                    break;
                }
                case "edit":
                    urlsend = "/om_user_edit.jsp?referenceno=" + request.getParameter("referenceno");
                    break;
                case "editprocess": {
                    UserAccount ua = new UserAccount();
                    ua.setId(Integer.parseInt(request.getParameter("id")));
                    ua.setLevel(request.getParameter("level"));
                    ua.setPassword(request.getParameter("password"));
                    ua.setStaffId(request.getParameter("staff_id"));
                    ua.setStaffName(request.getParameter("staff_name"));
                    ua.setUser(request.getParameter("user"));
                    UserDAO.updateItem(log, ua);
                    urlsend = "/om_user_list.jsp?id=" + moduleid;
                    //urlsend = "/cp_board_list.jsp?referenceno="+request.getParameter("referenceno");
                    break;
                }
                case "editpage":
                    urlsend = "/gl_edit_acc.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "viewdetail":
                    urlsend = "/cb_chequebook_view.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "delete":
                    UserDAO.deleteItem(log, request.getParameter("referenceno"));
                    urlsend = "/om_user_list.jsp?id=" + moduleid;
                    break;
                case "retrievemill":
                    ArContractDAO.saveNewAgreement(log, MillDAO.getDefaultMill(log).getCode());
                    urlsend = "/ar_agreement_list.jsp?id=" + moduleid;
                    break;
                case "retrievemillagreement":
                    ArContractDAO.saveNewAgreementListOnly(log, MillDAO.getDefaultMill(log).getCode(), request.getParameter("buyercode"));
                    urlsend = "/ar_agreement_view_list.jsp?refer=" + request.getParameter("buyercode");
                    break;
                case "agreelist":
                    urlsend = "/ar_agreement_view_list.jsp?refer=" + request.getParameter("referno");
                    break;
                case "uncheckmodule":
                    UserDAO.uncheckModule(log, request.getParameter("user_id"), request.getParameter("modulechecked"));
                    //urlsend = "/om_user_list.jsp?id="+moduleid;
                    break;
                default:
                    break;
            }

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_ARAgreement.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_ARAgreement.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
