/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.employee;

import com.lcsb.fms.controller.PathController;
import com.lcsb.fms.controller.financial.ar.Path_ARInvoice;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO;
import com.lcsb.fms.dao.financial.employee.EmPayrollDAO;
import com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.DistributeDAO;
import com.lcsb.fms.dao.financial.gl.PostDAO;
import com.lcsb.fms.financial.gl.post.Distribute;
import com.lcsb.fms.financial.gl.post.Master;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.model.financial.ar.ArTempRefinery;
import com.lcsb.fms.model.financial.ar.SalesInvoice;
import com.lcsb.fms.model.financial.ar.SalesInvoiceItem;
import com.lcsb.fms.model.financial.employee.EmPayroll;
import com.lcsb.fms.sys.log.SysLog;
import com.lcsb.fms.sys.log.SysLogDAO;
import com.lcsb.fms.util.dao.ModuleDAO;
import com.lcsb.fms.util.ext.ParseSafely;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_Payroll", urlPatterns = {"/Path_Payroll"})
public class Path_Payroll extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            SysLog sys = new SysLog();

            sys.setModuleid(moduleid);
            sys.setProcess(process);
            sys.setUserid(log.getUserID());
            sys.setDate(AccountingPeriod.getCurrentTimeStamp());
            sys.setTime(AccountingPeriod.getCurrentTime());
            sys.setLog(false);
            sys.setIp(request.getRemoteAddr());

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/em_payroll_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/em_payroll_add.jsp?sessionid=" + session.getId() + AccountingPeriod.getCurrentTimeStamp() + AccountingPeriod.getCurrentTime();
                    break;
                case "addpayroll": {
                    EmPayroll data = new EmPayroll();
                    data.setPeriod(request.getParameter("period"));
                    data.setYear(request.getParameter("year"));
                    data.setCompanycode(log.getEstateCode());
                    data.setCompanydescp(log.getEstateDescp());

                    String newrefer = EmPayrollDAO.saveMain(log, data);
                    urlsend = "/em_payroll_list.jsp?refer=" + newrefer;
                    
                    sys.setLog(true);
                    sys.setReferno(newrefer);
                    sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " added");
                    break;
                }
                case "editlist":
                    urlsend = "/em_payroll_nonexec.jsp?refer=" + request.getParameter("referno");
                    break;
                case "viewearning":
                    urlsend = "/em_payroll_list_earning.jsp?refer=" + request.getParameter("refer") +"&flag=view";
                    break;
                case "viewdeduction":
                    urlsend = "/em_payroll_list_deduction.jsp?refer=" + request.getParameter("refer") +"&flag=view";
                    break;
                case "getliststaff":
                    urlsend = "/em_payroll_list_staff.jsp?refer=" + request.getParameter("refer") +"&code=" + request.getParameter("earncode") +"&type=" + request.getParameter("type") +"&view=" + request.getParameter("view");
                    break;
                case "getlistpayslip":
                    urlsend = "/em_payroll_list_payslip.jsp?refer=" + request.getParameter("refer");
                    break;
                case "viewpaysheet":
                    urlsend = "/em_payroll_paysheet.jsp?refer=" + request.getParameter("refer");
                    break;
                case "delete":
                    SalesInvoiceDAO.deleteExist(log, request.getParameter("referno"));
                    urlsend = "/ar_inv_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno"));
                    sys.setRemark("Deleted");
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("100");//nullpointer
                    ErrorIO.setType("data");
                    urlsend = "/error.jsp";

                    sys.setLog(true);
                    sys.setProcess("error");
                    break;
            }

            SysLogDAO.saveLog(log, sys);

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_Payroll.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_Payroll.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
