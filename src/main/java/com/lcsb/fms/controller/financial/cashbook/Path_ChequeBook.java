/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.cashbook;

import com.lcsb.fms.controller.PathController;
import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.dao.financial.cashbook.ChequeBookDAO;
import com.lcsb.fms.dao.financial.gl.DistributeDAO;
import com.lcsb.fms.financial.gl.post.Distribute;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.cashbook.CbCekbook;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_ChequeBook", urlPatterns = {"/Path_ChequeBook"})
public class Path_ChequeBook extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";

            //Cheque Book
            switch (process) {
                case "viewlist":
                    urlsend = "/cb_chequebook_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/cb_chequebook_add.jsp?id=" + moduleid;
                    break;
                case "addprocess":
                    {
                        CbCekbook cb = new CbCekbook();
                        cb.setActive(request.getParameter("active"));
                        cb.setBankcode(request.getParameter("bankcode"));
                        cb.setBankname(request.getParameter("bankname"));
                        cb.setEndcek(request.getParameter("endcek"));
                        cb.setNo(request.getParameter("no"));
                        cb.setNocek(Integer.parseInt(request.getParameter("nocek")));
                        cb.setStartcek(request.getParameter("startcek"));
                        cb.setTarikh(request.getParameter("tarikh"));
                        ChequeBookDAO.saveData(log, cb);
                        urlsend = "/cb_chequebook_list.jsp?id=" + moduleid;
                        break;
                    }
                case "edit":
                    urlsend = "/cb_chequebook_edit.jsp?referenceno=" + request.getParameter("referenceno");
                    break;
                case "editprocess":
                    {
                        CbCekbook cb = new CbCekbook();
                        cb.setActive(request.getParameter("active"));
                        cb.setBankcode(request.getParameter("bankcode"));
                        cb.setBankname(request.getParameter("bankname"));
                        cb.setEndcek(request.getParameter("endcek"));
                        cb.setNo(request.getParameter("no"));
                        cb.setNocek(Integer.parseInt(request.getParameter("nocek")));
                        cb.setStartcek(request.getParameter("startcek"));
                        cb.setTarikh(request.getParameter("tarikh"));
                        ChequeBookDAO.updateItem(log, cb);
                        urlsend = "/cb_chequebook_list.jsp?referenceno=" + request.getParameter("referenceno");
                        break;
                    }
                case "editpage":
                    urlsend = "/gl_edit_acc.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "viewdetail":
                    urlsend = "/cb_chequebook_view.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "delete":
                    ChequeBookDAO.deleteItem(log, request.getParameter("referenceno"));
                    urlsend = "/cb_chequebook_list.jsp?id=" + moduleid;
                    break;
                default:
                    break;
            }

            Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_ChequeBook.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_ChequeBook.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
