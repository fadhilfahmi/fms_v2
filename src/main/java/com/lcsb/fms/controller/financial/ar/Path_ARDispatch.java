/* 
 * To change this license header, choose License Headers in Project Properties. 
 * To change this template file, choose Tools | Templates 
 * and open the template in the editor. 
 */ 
package com.lcsb.fms.controller.financial.ar; 
 
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount; 
import com.lcsb.fms.dao.financial.ar.ArDispatchDAO;
import com.lcsb.fms.general.AccountingPeriod; 
import com.lcsb.fms.general.ErrorIO; 
import com.lcsb.fms.model.financial.ar.ArCPORefinery;
import com.lcsb.fms.sys.log.SysLog; 
import com.lcsb.fms.sys.log.SysLogDAO; 
import com.lcsb.fms.util.model.LoginProfile; 
import java.io.IOException; 
import java.io.PrintWriter; 
import java.util.logging.Level; 
import java.util.logging.Logger; 
import javax.servlet.RequestDispatcher; 
import javax.servlet.ServletException; 
import javax.servlet.annotation.WebServlet; 
import javax.servlet.http.HttpServlet; 
import javax.servlet.http.HttpServletRequest; 
import javax.servlet.http.HttpServletResponse; 
import javax.servlet.http.HttpSession; 
 
/** 
 * 
 * @author fadhilfahmi 
 */ 
@WebServlet(name = "Path_ARDispatch", urlPatterns = {"/Path_ARDispatch"}) 
public class Path_ARDispatch extends HttpServlet { 
 
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> 
     * methods. 
     * 
     * @param request servlet request 
     * @param response servlet response 
     * @throws ServletException if a servlet-specific error occurs 
     * @throws IOException if an I/O error occurs 
     */ 
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException, Exception { 
        response.setContentType("text/html;charset=UTF-8"); 
        try (PrintWriter out = response.getWriter()) { 
             
             
            HttpSession session = request.getSession(); 
            String moduleid = request.getParameter("moduleid"); 
            String process = request.getParameter("process"); 
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail"); 
            SysLog sys = new SysLog(); 
 
            sys.setModuleid(moduleid); 
            sys.setProcess(process); 
            sys.setUserid(log.getUserID()); 
            sys.setDate(AccountingPeriod.getCurrentTimeStamp()); 
            sys.setTime(AccountingPeriod.getCurrentTime()); 
            sys.setLog(false); 
            sys.setIp(request.getRemoteAddr()); 
 
            String urlsend = ""; 
            String notify = ""; 
 
            switch (process) { 
                case "cpo_local": 
                    urlsend = "/ar_dispatch_viewtable.jsp?type=cpo&location=local"; 
                    break; 
                case "cpo_mill": 
                    urlsend = "/ar_dispatch_viewtable.jsp?type=cpo&location=mill"; 
                    break; 
                case "pk_local": 
                    urlsend = "/ar_dispatch_viewtable.jsp?type=kernel&location=local"; 
                    break; 
                case "pk_mill": 
                    urlsend = "/ar_dispatch_viewtable.jsp?type=kernel&location=mill"; 
                    break; 
                case "dispatchlist": 
                    urlsend = "/ar_dispatch_list.jsp?type=" + request.getParameter("type"); 
                    break; 
                case "updatedispatch":
                    
                    ArCPORefinery i = new ArCPORefinery();
                    i.setDispatchNo(request.getParameter("dispatch_no"));
                    i.setContract(request.getParameter("contract"));
                    i.setTrailer(request.getParameter("trailer"));
                    i.setDriver(request.getParameter("driver"));
                    i.setMt(request.getParameter("mt"));
                    i.setMtRefinery(Double.parseDouble(request.getParameter("mt_refinery")));
                    
                    ArDispatchDAO.updateRefinery(log, i, request.getParameter("old_dispatch_no"), request.getParameter("type"));
                    
                    urlsend = "/ar_dispatch_list.jsp?type=" + request.getParameter("type"); 
                    break; 
                default: 
                    ErrorIO.setError(String.valueOf("Error : Invalid Path.")); 
                    ErrorIO.setCode("100");//nullpointer 
                    ErrorIO.setType("data"); 
                    urlsend = "/error.jsp"; 
 
                    sys.setLog(true); 
                    sys.setProcess("error"); 
                    break; 
            } 
 
            SysLogDAO.saveLog(log, sys); 
 
            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend); 
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend); 
            dispatcher.forward(request, response); 
        } 
    } 
 
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code."> 
    /** 
     * Handles the HTTP <code>GET</code> method. 
     * 
     * @param request servlet request 
     * @param response servlet response 
     * @throws ServletException if a servlet-specific error occurs 
     * @throws IOException if an I/O error occurs 
     */ 
    @Override 
    protected void doGet(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException { 
        try { 
            processRequest(request, response); 
        } catch (Exception ex) { 
            Logger.getLogger(Path_ARDispatch.class.getName()).log(Level.SEVERE, null, ex); 
        } 
    } 
 
    /** 
     * Handles the HTTP <code>POST</code> method. 
     * 
     * @param request servlet request 
     * @param response servlet response 
     * @throws ServletException if a servlet-specific error occurs 
     * @throws IOException if an I/O error occurs 
     */ 
    @Override 
    protected void doPost(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException { 
        try { 
            processRequest(request, response); 
        } catch (Exception ex) { 
            Logger.getLogger(Path_ARDispatch.class.getName()).log(Level.SEVERE, null, ex); 
        } 
    } 
 
    /** 
     * Returns a short description of the servlet. 
     * 
     * @return a String containing servlet description 
     */ 
    @Override 
    public String getServletInfo() { 
        return "Short description"; 
    }// </editor-fold> 
 
} 