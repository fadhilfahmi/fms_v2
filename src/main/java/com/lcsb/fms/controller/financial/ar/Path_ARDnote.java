/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.ar;

import com.lcsb.fms.controller.PathController;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.financial.ar.ArDebitNoteDAO;
import com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.DistributeDAO;
import com.lcsb.fms.dao.financial.gl.PostDAO;
import com.lcsb.fms.financial.gl.post.Distribute;
import com.lcsb.fms.financial.gl.post.Master;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.model.financial.ar.ArDebitNote;
import com.lcsb.fms.model.financial.ar.ArDebitNoteItem;
import com.lcsb.fms.sys.log.SysLog;
import com.lcsb.fms.sys.log.SysLogDAO;
import com.lcsb.fms.util.dao.ModuleDAO;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_ARDnote", urlPatterns = {"/Path_ARDnote"})
public class Path_ARDnote extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            SysLog sys = new SysLog();

            sys.setModuleid(moduleid);
            sys.setProcess(process);
            sys.setUserid(log.getUserID());
            sys.setDate(AccountingPeriod.getCurrentTimeStamp());
            sys.setTime(AccountingPeriod.getCurrentTime());
            sys.setLog(false);
            sys.setIp(request.getRemoteAddr());

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/ar_debitnote_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/ar_debitnote_add.jsp";
                    break;
                case "additem":
                    urlsend = "/ar_debitnote_add_item.jsp?refer=" + request.getParameter("refer");
                    break;
                case "edititem":
                    urlsend = "/ar_debitnote_edit_item.jsp?refer=" + request.getParameter("referno");
                    break;
                case "addprocess": {
                    ArDebitNote data = new ArDebitNote();
                    data.setTotal(Double.parseDouble(request.getParameter("total")));
                    data.setBuyercode(request.getParameter("buyercode"));
                    data.setBuyername(request.getParameter("buyername"));
                    data.setEstcode(request.getParameter("estcode"));
                    data.setEstname(request.getParameter("estname"));
                    data.setNotedate(AccountingPeriod.convertStringtoDate(request.getParameter("notedate")));
                    data.setPeriod(request.getParameter("period"));
                    data.setPdate(request.getParameter("pdate"));
                    data.setPid(request.getParameter("pid"));
                    data.setPname(request.getParameter("pname"));
                    data.setRemark(request.getParameter("remark"));
                    data.setYear(request.getParameter("year"));

                    String newrefer = ArDebitNoteDAO.saveMain(log, data);
                    urlsend = "/ar_debitnote_edit_list.jsp?refer=" + newrefer;

                    sys.setLog(true);
                    sys.setReferno(newrefer);
                    sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " added.");
                    break;
                }
                case "additemprocess":
                    try {
                        ArDebitNoteItem data = new ArDebitNoteItem();

                        data.setAmount(Double.parseDouble(request.getParameter("amount")));
                        data.setCoacode(request.getParameter("coacode"));
                        data.setCoaname(request.getParameter("coaname"));
                        data.setNoteno(request.getParameter("noteno"));
                        data.setLoccode(request.getParameter("loccode"));
                        data.setLocdesc(request.getParameter("locdesc"));
                        data.setLoclevel(request.getParameter("loclevel"));
                        data.setProdcode(request.getParameter("prodcode"));
                        data.setProdname(request.getParameter("prodname"));
                        data.setQty(Double.parseDouble(request.getParameter("qty")));
                        data.setRefer(request.getParameter("refer"));
                        data.setRemarks(request.getParameter("remarks"));
                        data.setSacode(request.getParameter("sacode"));
                        data.setSadesc(request.getParameter("sadesc"));
                        data.setSatype(request.getParameter("satype"));
                        data.setTaxamt(Double.parseDouble(request.getParameter("taxamt")));
                        data.setTaxcoacode(request.getParameter("taxcoacode"));
                        data.setTaxcoadescp(request.getParameter("taxcoadescp"));
                        data.setTaxcode(request.getParameter("taxcode"));
                        data.setTaxrate(Double.parseDouble(request.getParameter("taxrate")));
                        data.setUnitm(request.getParameter("unitm"));
                        data.setUnitp(request.getParameter("unitp"));

                        ArDebitNoteDAO.saveItem(log, data);

                        urlsend = "/ar_debitnote_edit_list.jsp?refer=" + request.getParameter("noteno");

                        sys.setLog(true);
                        sys.setReferno(request.getParameter("noteno"));
                        sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " Item added.");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "edititemprocess":
                    try {
                        ArDebitNoteItem data = new ArDebitNoteItem();

                        data.setAmount(Double.parseDouble(request.getParameter("amount")));
                        data.setCoacode(request.getParameter("coacode"));
                        data.setCoaname(request.getParameter("coaname"));
                        data.setNoteno(request.getParameter("noteno"));
                        data.setLoccode(request.getParameter("loccode"));
                        data.setLocdesc(request.getParameter("locdesc"));
                        data.setLoclevel(request.getParameter("loclevel"));
                        data.setProdcode(request.getParameter("prodcode"));
                        data.setProdname(request.getParameter("prodname"));
                        data.setQty(Double.parseDouble(request.getParameter("qty")));
                        data.setRefer(request.getParameter("refer"));
                        data.setRemarks(request.getParameter("remarks"));
                        data.setSacode(request.getParameter("sacode"));
                        data.setSadesc(request.getParameter("sadesc"));
                        data.setSatype(request.getParameter("satype"));
                        data.setTaxamt(Double.parseDouble(request.getParameter("taxamt")));
                        data.setTaxcoacode(request.getParameter("taxcoacode"));
                        data.setTaxcoadescp(request.getParameter("taxcoadescp"));
                        data.setTaxcode(request.getParameter("taxcode"));
                        data.setTaxrate(Double.parseDouble(request.getParameter("taxrate")));
                        data.setUnitm(request.getParameter("unitm"));
                        data.setUnitp(request.getParameter("unitp"));

                        ArDebitNoteDAO.updateItem(log, data, request.getParameter("refer"));

                        urlsend = "/ar_debitnote_edit_list.jsp?refer=" + request.getParameter("noteno");

                        sys.setLog(true);
                        sys.setReferno(request.getParameter("noteno"));
                        sys.setRemark("Debit Note Item updated (" + request.getParameter("refer") + ").");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "editlist":
                    urlsend = "/ar_debitnote_edit_list.jsp?refer=" + request.getParameter("referno");
                    break;
                case "viewor":
                    urlsend = "/cb_official_view_voucher.jsp?referno=" + request.getParameter("refer");
                    break;
                case "editmain":
                    urlsend = "/ar_debitnote_edit.jsp?referno=" + request.getParameter("referno");
                    break;
                case "editmainprocess": {
                    ArDebitNote data = new ArDebitNote();
                    data.setNoteno(request.getParameter("noteno"));
                    data.setTotal(Double.parseDouble(request.getParameter("total")));
                    data.setBuyercode(request.getParameter("buyercode"));
                    data.setBuyername(request.getParameter("buyername"));
                    data.setNotedate(AccountingPeriod.convertStringtoDate(request.getParameter("notedate")));
                    data.setPname(request.getParameter("pname"));
                    data.setRemark(request.getParameter("remark"));
                    data.setYear(request.getParameter("year"));
                    data.setEstcode(request.getParameter("estcode"));
                    data.setEstname(request.getParameter("estname"));
                    data.setPeriod(request.getParameter("period"));

                    ArDebitNoteDAO.updateMain(log, data, request.getParameter("noteno"));
                    urlsend = "/ar_debitnote_edit_list.jsp?refer=" + request.getParameter("noteno");
                    
                    sys.setLog(true);
                    sys.setReferno(request.getParameter("noteno"));
                    sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " updated.");
                    
                    break;
                }
                case "dist":
                    Distribute dist = (Distribute) DistributeDAO.distributeNow(request.getParameter("referno"), "DNR", log);
                    session.setAttribute("post_detail", dist);

                    Logger.getLogger(PathController.class.getName()).log(Level.INFO, "------------" + String.valueOf(dist.isSuccess()));
                    if (dist.isSuccess()) {

                        urlsend = "/gl_post_status.jsp";
                    } else {
                        ArDebitNoteDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());
                        urlsend = "/gl_post_status.jsp";
                        
                        sys.setLog(true);
                        sys.setReferno(request.getParameter("referno"));
                        sys.setRemark("Approved & Posted");
                    }

                    break;
                case "delete":
                    ArDebitNoteDAO.deleteExist(log, request.getParameter("referno"));
                    urlsend = "/ar_debitnote_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");
                    
                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno"));
                    sys.setRemark("Deleted.");
                    
                    break;
                case "deleteitem":
                    //Logger.getLogger(PathController.class.getName()).log(Level.INFO, request.getParameter("referno").substring(0,15));
                    ArDebitNoteDAO.deleteItem(log, request.getParameter("referno"));
                    urlsend = "/ar_debitnote_edit_list.jsp?refer=" + request.getParameter("referno").substring(0, 15);
                    
                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno").substring(0, 15));
                    sys.setRemark("Debit Note Item deleted (" + request.getParameter("referno") + ").");
                    
                    break;
                case "check":
                    urlsend = "/check_voucher.jsp?referno=" + request.getParameter("referno") + "&vtype=DNR";
                    break;
                case "checkprocess":
                    ArDebitNoteDAO.checkVoucher(log, request.getParameter("referno"), log.getUserID(), log.getUserName());
                    urlsend = "/ar_debitnote_edit_list.jsp?refer=" + request.getParameter("referno");
                    
                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno"));
                    sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " Checked.");
                    
                    break;
                case "viewprinted":
                    urlsend = "/print_voucher.jsp?refer=" + request.getParameter("refer");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("View Printed Format");
                    break;
                    
                case "summarydnote":
                    urlsend = "/ar_inv_summary_dnote.jsp?refer=" + request.getParameter("refer");

                    //sys.setLog(true);
                    //sys.setReferno(request.getParameter("refer"));
                    //sys.setRemark("View Printed Format");
                    break;
                    
                case "printpreview":
                    urlsend = "/print_voucher_approve.jsp?refer=" + request.getParameter("refer");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("View Printed Format");
                    break;
                case "distformanager":
                    Distribute distx = (Distribute) DistributeDAO.distributeNow(request.getParameter("referno"), "DNR", log);
                    session.setAttribute("post_detail", distx);
                    if (distx.isSuccess()) {
                        urlsend = "/start_2.jsp";
                    } else {
                        ArDebitNoteDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());
                        urlsend = "/gl_post_status.jsp";
                        
                        
                    }

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno"));
                    sys.setRemark("Approved & Posted");
                    break;
                case "createnote":
                    //String newrefer = "PVN991217040003";
                    Master post = (Master) PostDAO.getInfo(log, request.getParameter("refer"), "DNR");
                    DebitCreditNoteDAO.generateDebitCredit(post, log);
                    
                    notify = "&status=success&text=Credit Note created.";
                    urlsend = "/ar_debitnote_edit_list.jsp?refer=" + request.getParameter("refer")+notify;

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("Debit Note created for "+request.getParameter("refer")+".");
                    break;
                case "viewsummary":
                    urlsend = "/ar_debitnote_summary_view.jsp?refer=" + request.getParameter("referno");

                    break;
                case "viewprinted2":
                    urlsend = "/ar_inv_summary_view_printed.jsp?refer=" + request.getParameter("refer") + "&status="+request.getParameter("status");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("View Printed Format");
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("100");//nullpointer
                    ErrorIO.setType("data");
                    urlsend = "/error.jsp";
                    break;
            }
            
            SysLogDAO.saveLog(log, sys);

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_ARDnote.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_ARDnote.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
