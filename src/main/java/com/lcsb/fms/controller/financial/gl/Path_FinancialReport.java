/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.gl;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.report.financial.balancesheet.BSParam;
import com.lcsb.fms.report.financial.balancesheet.BalanceSheetDAO;
import com.lcsb.fms.report.financial.gl.GListParam;
import com.lcsb.fms.report.financial.trialbalance.TBParam;
import com.lcsb.fms.report.financial.trialbalance.TBalDAO;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_FinancialReport", urlPatterns = {"/Path_FinancialReport"})
public class Path_FinancialReport extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/gl_Financial_Report.jsp?id=" + moduleid;
                    break;
                case "balancesheet":
                    urlsend = "/gl_fr_balancesheet_v2.jsp?id=" + moduleid;
                    break;
                case "trialbalance":
                    urlsend = "/gl_fr_trial_balance.jsp?id=" + moduleid;
                    break;
                case "gllisting":
                    urlsend = "/gl_fr_gllisting.jsp?id=" + moduleid;
                    break;
                case "viewreport":
                    urlsend = "/gl_fr_balancesheet_v2_view.jsp?id=" + moduleid;
                    break;
                case "savejournal":
                    break;
                case "viewjournal":
                    urlsend = "/gl_prepay_viewjournal.jsp?id=" + moduleid;
                    break;
                case "balancesheet_option":
                    urlsend = "/gl_fr_balancesheet_v2.jsp?id=" + moduleid;
                    break;
                case "generateGL":

                    GListParam g = new GListParam();
                    g.setCreditopt(request.getParameter("creditopt"));
                    g.setDebitopt(request.getParameter("debitopt"));
                    g.setFcode(request.getParameter("fcode"));
                    g.setFperiod(request.getParameter("fperiod"));
                    g.setFyear(request.getParameter("fyear"));
                    g.setLoccode(request.getParameter("loccode"));
                    g.setLocdesc(request.getParameter("locdesc"));
                    g.setLoclevel(request.getParameter("loclevel"));
                    g.setSacode(request.getParameter("sacode"));
                    g.setSadesc(request.getParameter("sadesc"));
                    g.setSatype(request.getParameter("satype"));
                    g.setTcode(request.getParameter("tcode"));
                    g.setTperiod(request.getParameter("tperiod"));
                    g.setTyear(request.getParameter("tyear"));
                    g.setViewby(request.getParameter("viewby"));
                    session.setAttribute("gl_param", g);

                    urlsend = "/gl_fr_gllisting_view.jsp?id=" + moduleid;
                    break;
                case "generateTB":

                    TBParam t = new TBParam();
                    t.setYear(request.getParameter("year"));
                    t.setLoccode(request.getParameter("loccode"));
                    t.setLocdesc(request.getParameter("locdesc"));
                    t.setLoclevel(request.getParameter("loclevel"));
                    t.setOtype(request.getParameter("otype"));
                    t.setPeriod(request.getParameter("period"));
                    session.setAttribute("tb_param", t);
                    
                    TBalDAO.generateTrialBalance(log, t);

                    urlsend = "/gl_fr_trial_balance_view.jsp?id=" + moduleid;
                    break;
                case "generateBS":

                    BSParam b = new BSParam();
                    b.setYear(request.getParameter("year"));
                    b.setLoccode(request.getParameter("loccode"));
                    b.setLocdesc(request.getParameter("locdesc"));
                    b.setLoclevel(request.getParameter("loclevel"));
                    b.setOtype(request.getParameter("otype"));
                    b.setPeriod(request.getParameter("period"));
                    b.setVby(Integer.parseInt(request.getParameter("vby")));
                    session.setAttribute("bs_param", b);

                    urlsend = "/gl_fr_balancesheet_v2_view.jsp?id=" + moduleid;
                    break;
                case "statement":
                    urlsend = "/ar_rpt_statement.jsp?id=" + moduleid;
                    break;
                case "aginganalysis":
                    urlsend = "/ar_rpt_aging.jsp?id=" + moduleid;
                    break;
                case "summaryinvoice":
                    urlsend = "/ar_rpt_invsummary.jsp?id=" + moduleid;
                    break;
                case "reportconfig":
                    urlsend = "/gl_fr_config.jsp?id=" + moduleid;
                    break;
                case "balancesheet_config":
                    urlsend = "/gl_fr_config_balancesheet.jsp?id=" + moduleid;
                    break;
                default:
                    break;
            }

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_FinancialReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_FinancialReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
