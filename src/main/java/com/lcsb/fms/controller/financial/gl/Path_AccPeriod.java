/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.gl;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.financial.cashbook.ChequeBookDAO;
import com.lcsb.fms.dao.financial.gl.AccountPeriodDAO;
import com.lcsb.fms.dao.setup.configuration.ChartofAccountDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.model.financial.gl.AccountPeriod;
import com.lcsb.fms.model.financial.gl.Period;
import com.lcsb.fms.model.setup.configuration.ChartofAccount;
import com.lcsb.fms.sys.log.SysLog;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_AccPeriod", urlPatterns = {"/Path_AccPeriod"})
public class Path_AccPeriod extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

            SysLog sys = new SysLog();

            sys.setModuleid(moduleid);
            sys.setProcess(process);
            sys.setUserid(log.getUserID());
            sys.setDate(AccountingPeriod.getCurrentTimeStamp());
            sys.setTime(AccountingPeriod.getCurrentTime());

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/gl_accountperiod_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/gl_accountperiod_add.jsp?id=" + moduleid;
                    break;
                case "addprocess":
                    List<Period> pr = new ArrayList();
                    AccountPeriod ap = new AccountPeriod();
                    ap.setYear(request.getParameter("year"));
                    ap.setStartyear(request.getParameter("startyear"));
                    ap.setEndyear(request.getParameter("endyear"));
                    ap.setCurrent(request.getParameter("current"));
                    for (int i = 1; i < 13; i++) {
                        Period pd = new Period();
                        String start = request.getParameter("range_" + i).substring(0, 10);
                        String end = request.getParameter("range_" + i).substring(13, 23);

                        pd.setStartperiod(start);
                        pd.setEndperiod(end);
                        pd.setMonth(i);
                        pd.setPeriod(i);
                        pr.add(pd);

                    }
                    ap.setListPeriod(pr);
                    AccountPeriodDAO.saveData(log, ap);
                    urlsend = "/gl_accountperiod_list.jsp?id=" + moduleid;
                    break;
                case "edit":
                    urlsend = "/cf_coa_edit.jsp?referenceno=" + request.getParameter("referenceno");
                    break;
                case "editprocess":
                    ChartofAccount cb = new ChartofAccount();
                    cb.setActive(request.getParameter("active"));
                    cb.setApplevel(request.getParameter("applevel"));
                    cb.setApptype(request.getParameter("apptype"));
                    cb.setAttype(request.getParameter("attype"));
                    cb.setCode(request.getParameter("code"));
                    cb.setDescp(request.getParameter("descp"));
                    cb.setFinallvl(request.getParameter("finallvl"));
                    cb.setReport(request.getParameter("report"));
                    cb.setType(request.getParameter("type"));
                    ChartofAccountDAO.updateItem(log, cb);
                    urlsend = "/gl_accountperiod_list.jsp?referenceno=" + request.getParameter("referenceno");
                    break;
                case "delete":
                    ChequeBookDAO.deleteItem(log, request.getParameter("referenceno"));
                    urlsend = "/cb_chequebook_list.jsp?id=" + moduleid;
                    break;
                case "setnewperiod":
                    AccountPeriodDAO.setNewPeriod(log, request.getParameter("year"), request.getParameter("period"));
                    urlsend = "/cb_chequebook_list.jsp?id=" + moduleid;
                    break;
                default:
                    break;
            }

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_AccPeriod.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_AccPeriod.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
