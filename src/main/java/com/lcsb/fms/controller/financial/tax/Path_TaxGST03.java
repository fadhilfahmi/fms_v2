/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.tax;

import com.lcsb.fms.controller.PathController;
import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.financial.ap.VendorInvoiceDAO;
import com.lcsb.fms.dao.financial.gl.DebitNoteDAO;
import com.lcsb.fms.dao.financial.tx.TaxGst03DAO;
import com.lcsb.fms.dao.financial.tx.TaxMasterDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.tx.TxGst03;
import com.lcsb.fms.model.financial.tx.TxGst03Refer;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_TaxGST03", urlPatterns = {"/Path_TaxGST03"})
public class Path_TaxGST03 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/tx_gst03_list.jsp?id=" + moduleid;
                    break;
                case "addgst03":
                    urlsend = "/tx_gst03_add.jsp";
                    break;
                case "viewgst03":
                    urlsend = "/tx_gst03_view_gst03.jsp?refer="+ request.getParameter("refer");
                    break;
                case "savegst":

                    TxGst03 tg = new TxGst03();

                    tg.setAmend(request.getParameter("amend"));
                    tg.setB5(request.getParameter("b5"));
                    tg.setC1(new BigDecimal(request.getParameter("c1")));
                    tg.setC10(new BigDecimal(request.getParameter("c10")));
                    tg.setC11(new BigDecimal(request.getParameter("c11")));
                    tg.setC12(new BigDecimal(request.getParameter("c12")));
                    tg.setC13(new BigDecimal(request.getParameter("c13")));
                    tg.setC15(new BigDecimal(request.getParameter("c15")));
                    tg.setC17(new BigDecimal(request.getParameter("c17")));
                    tg.setC19(new BigDecimal(request.getParameter("c19")));
                    tg.setC2(new BigDecimal(request.getParameter("c2")));
                    tg.setC21(new BigDecimal(request.getParameter("c21")));
                    tg.setC23(new BigDecimal(request.getParameter("c23")));
                    tg.setC24(new BigDecimal(request.getParameter("c24")));
                    tg.setC3(new BigDecimal(request.getParameter("c3")));
                    tg.setC4(new BigDecimal(request.getParameter("c4")));
                    tg.setC6(new BigDecimal(request.getParameter("c6")));
                    tg.setC7(new BigDecimal(request.getParameter("c7")));
                    tg.setC8(new BigDecimal(request.getParameter("c8")));
                    tg.setC9(new BigDecimal(request.getParameter("c9")));
                    tg.setClaim(new BigDecimal(request.getParameter("claim")));
                    tg.setCoacode(request.getParameter("coacode"));
                    tg.setCoadescp(request.getParameter("coadescp"));
                    tg.setEstcode(request.getParameter("estcode"));
                    tg.setGstid(request.getParameter("gstid"));
                    tg.setI14(request.getParameter("i14"));
                    tg.setI16(request.getParameter("i16"));
                    tg.setI18(request.getParameter("i18"));
                    tg.setI20(request.getParameter("i20"));
                    tg.setI22(request.getParameter("i22"));
                    tg.setLoccode(request.getParameter("loccode"));
                    tg.setLoclevel(request.getParameter("loclevel"));
                    tg.setLocname(request.getParameter("locname"));
                    tg.setPay(new BigDecimal(request.getParameter("pay")));
                    tg.setPeriod(Integer.parseInt(request.getParameter("period")));
                    tg.setPrcnt1(new BigDecimal(request.getParameter("p15")));
                    tg.setPrcnt2(new BigDecimal(request.getParameter("p17")));
                    tg.setPrcnt3(new BigDecimal(request.getParameter("p19")));
                    tg.setPrcnt4(new BigDecimal(request.getParameter("p21")));
                    tg.setPrcnt5(new BigDecimal(request.getParameter("p23")));
                    tg.setPrcnt6(new BigDecimal(request.getParameter("p24")));
                    tg.setSacode(request.getParameter("sacode"));
                    tg.setSadesc(request.getParameter("sadesc"));
                    tg.setSatype(request.getParameter("satype"));
                    Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------date>>>" + request.getParameter("taxdue"));
                    tg.setTaxdue(AccountingPeriod.convertStringtoDate(request.getParameter("taxdue")));
                    tg.setTaxend(AccountingPeriod.convertStringtoDate(request.getParameter("taxend")));
                    tg.setTaxperiod(request.getParameter("taxperiod"));
                    tg.setTaxstart(AccountingPeriod.convertStringtoDate(request.getParameter("taxstart")));
                    tg.setTaxyear(request.getParameter("taxyear"));
                    tg.setYear(request.getParameter("year"));

                    String newrefer = AutoGenerate.getReferenceNox(log, "refer", "tx_gst03", "TGS", log.getEstateCode(), String.valueOf(TaxMasterDAO.getNextTaxPeriod(log).getYear()), String.valueOf(TaxMasterDAO.getTaxPeriod(log).getPeriod()));

                    TaxGst03DAO.saveGst03(tg, log, newrefer);

                    String[] reconrefer = request.getParameterValues("reconrefer");
                    String[] taxdebit = request.getParameterValues("taxdebit");
                    String[] taxcredit = request.getParameterValues("taxcredit");

                    for (int i = 0; i < reconrefer.length; i++) {

                        TxGst03Refer tr = new TxGst03Refer();

                        tr.setRefer(newrefer);
                        tr.setTaxcredit(GeneralTerm.amountFormattoSave(taxcredit[i]));
                        tr.setTaxdebit(GeneralTerm.amountFormattoSave(taxdebit[i]));
                        tr.setTaxend(AccountingPeriod.convertStringtoDate(request.getParameter("taxend")));
                        tr.setTaxperiod(String.valueOf(TaxMasterDAO.getTaxPeriod(log).getPeriod()));
                        tr.setTaxstart(AccountingPeriod.convertStringtoDate(request.getParameter("taxstart")));
                        tr.setTaxyear(String.valueOf(TaxMasterDAO.getNextTaxPeriod(log).getYear()));

                        TaxGst03DAO.saveGst03Refer(log, tr);
                    }

                    urlsend = "/tx_gst03_view.jsp?refer=" + newrefer;//+"&referenceno="+request.getParameter("referenceno");

                    break;
                case "delete":
                    TaxGst03DAO.deleteExist(log, request.getParameter("referno"));
                    urlsend = "/tx_gst03_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");
                    break;
                case "viewdetail":
                    urlsend = "/tx_gst03_view.jsp?refer=" + request.getParameter("refer");
                    break;
                case "viewpdf":
                    urlsend = "/GST?refer=" + request.getParameter("refer");
                    break;
                case "generateJV":
                    String refer = TaxGst03DAO.generateJV(request.getParameter("referno"), log);

                    //if (request.getParameter("referno").equals("main")) {
                    //    urlsend = "/gl_creditnotereceive_list.jsp?id=" + moduleid;
                    //} else {
                    urlsend = "/tx_gst03_view.jsp?refer=" + request.getParameter("referno");
                    //}

                    break;
                case "approve":
                    TaxGst03DAO.approveVoucher(request.getParameter("referno"), log);
                    urlsend = "/tx_gst03_view.jsp?refer=" + request.getParameter("referno");
                    break;
                case "gotoJV":
                    urlsend = "/gl_jv_edit_list.jsp?refer=" + request.getParameter("referno");
                    break;
                case "undotax":
                    TaxMasterDAO.confirmUndo(log);
                    urlsend = "/tx_gst03_list.jsp?refer=" + request.getParameter("referno");
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("100");//nullpointer
                    ErrorIO.setType("data");
                    urlsend = "/error.jsp";
                    break;
            }

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_TaxGST03.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_TaxGST03.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
