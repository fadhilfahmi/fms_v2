/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.tax;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.financial.ap.VendorPaymentDAO;
import com.lcsb.fms.dao.financial.gl.DebitNoteDAO;
import com.lcsb.fms.dao.financial.tx.TaxOutputDAO;
import com.lcsb.fms.dao.financial.tx.TaxReconcileDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.model.financial.tx.TxOutputDetail;
import com.lcsb.fms.model.financial.tx.TxReconcileMaster;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_TaxReconcile", urlPatterns = {"/Path_TaxReconcile"})
public class Path_TaxReconcile extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/tx_recon_list.jsp?id=" + moduleid;
                    break;
                case "addrecon":
                    urlsend = "/tx_recon_add.jsp";
                    break;
                case "addreconprocess":

                    TxReconcileMaster tx = new TxReconcileMaster();
                    tx.setName(request.getParameter("name"));
                    tx.setPeriod(Integer.parseInt(request.getParameter("period")));
                    tx.setTaxperiodfrom(AccountingPeriod.convertStringtoDate(request.getParameter("taxperiodfrom")));
                    tx.setTaxperiodto(AccountingPeriod.convertStringtoDate(request.getParameter("taxperiodto")));
                    tx.setTaxreturndate(AccountingPeriod.convertStringtoDate(request.getParameter("taxreturndate")));
                    tx.setYear(Integer.parseInt(request.getParameter("year")));

                    String refer = TaxReconcileDAO.saveMain(tx, log);
                    urlsend = "/tx_recon_list.jsp?refer=" + request.getParameter("refer");
                    break;
                case "addrecondetail":

                    String[] cnt = request.getParameterValues("checkboxdisno");

                    for (int i = 0; i < cnt.length; i++) {

                        String referno = cnt[i];
                        String trxtype = request.getParameter("trxtype" + cnt[i]);

                        TxOutputDetail toSave = TaxOutputDAO.getSelectedTaxDetail(log, referno, trxtype);
                        TaxOutputDAO.saveDetail(log, toSave, request.getParameter("refno"));
                    }

                    //String refer = TaxInputDAO.saveMain(tx, log);
                    urlsend = "/tx_recon_list.jsp?refer=" + request.getParameter("refer");
                    break;
                case "savereconcile":
                    String[] cnt_output = request.getParameterValues("check-output");
                    String[] cnt_input = request.getParameterValues("check-input");
                    String txr_refno = request.getParameter("txr_refno");

                    for (int i = 0; i < cnt_output.length; i++) {
                        String referno = cnt_output[i];
                        TaxReconcileDAO.updateOutputInput(log, referno, txr_refno, "tx_output");
                    }

                    for (int i = 0; i < cnt_input.length; i++) {
                        String referno = cnt_input[i];
                        TaxReconcileDAO.updateOutputInput(log, referno, txr_refno, "tx_input");
                    }

                    TaxReconcileDAO.saveReconcileOutput(txr_refno, log);
                    TaxReconcileDAO.saveReconcileInput(txr_refno, log);
                    TaxReconcileDAO.updateReconcileMaster(txr_refno, log);
                    
                    notify = "&status=success&text=Tax Reconcile has been updated.";

                    urlsend = "/tx_recon_view.jsp?refer=" + request.getParameter("txr_refno") + notify;
                    break;
                case "generateJV":

                    break;
                case "delete":
                    DebitNoteDAO.deleteExist(log, request.getParameter("referno"));
                    urlsend = "/gl_debitnote_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");
                    break;
                case "viewdetail":
                    urlsend = "/tx_recon_view.jsp?refer=" + request.getParameter("refer");
                    break;
                case "checkprocess":
                    VendorPaymentDAO.checkVoucher(log, request.getParameter("referno"), log.getUserID(), log.getUserName());
                    urlsend = "/ap_pv_edit_list.jsp?refer=" + request.getParameter("referno");
                    break;
                case "deletedetail":
                    TaxReconcileDAO.deleteReconcileDetail(log, request.getParameter("refer"));
                    urlsend = "/tx_recon_view.jsp?refer=" + request.getParameter("refer");
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("100");//nullpointer
                    ErrorIO.setType("data");
                    urlsend = "/error.jsp";
                    break;
            }

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_TaxReconcile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_TaxReconcile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
