/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.ap;

import com.lcsb.fms.controller.PathController;
import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.financial.ap.VendorInvoiceDAO;
import com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.DistributeDAO;
import com.lcsb.fms.dao.financial.gl.PostDAO;
import com.lcsb.fms.financial.gl.post.Distribute;
import com.lcsb.fms.financial.gl.post.Master;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.ap.VendorInvoice;
import com.lcsb.fms.model.financial.ap.VendorInvoiceDetail;
import com.lcsb.fms.model.financial.ap.VendorInvoiceRefer;
import com.lcsb.fms.sys.log.SysLog;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_VendorInvoice", urlPatterns = {"/Path_VendorInvoice"})
public class Path_VendorInvoice extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";

            SysLog sys = new SysLog();

            sys.setModuleid(moduleid);
            sys.setProcess(process);
            sys.setUserid(log.getUserID());
            sys.setDate(AccountingPeriod.getCurrentTimeStamp());
            sys.setTime(AccountingPeriod.getCurrentTime());
            sys.setLog(false);
            sys.setIp(request.getRemoteAddr());

            switch (process) {
                case "viewlist":
                    urlsend = "/ap_inv_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/ap_inv_add.jsp";
                    break;
                case "addffb":
                    urlsend = "/ap_inv_add_ffb.jsp";
                    break;
                case "addinv": {
                    VendorInvoice data = new VendorInvoice();
                    data.setAccode(request.getParameter("accode"));
                    data.setAcdesc(request.getParameter("acdesc"));
                    data.setDate(request.getParameter("date"));
                    data.setEstatecode(request.getParameter("estatecode"));
                    data.setEstatename(request.getParameter("estatename"));
                    data.setGstid(request.getParameter("gstid"));
                    data.setInvno(request.getParameter("invno"));
                    data.setInvrefno(request.getParameter("invrefno"));
                    data.setInvtype(request.getParameter("invtype"));
                    data.setNogrn(request.getParameter("nogrn"));
                    data.setNoinv(request.getParameter("noinv"));
                    //data.setPaid(Double.parseDouble(request.getParameter("paid")));
                    data.setPeriod(request.getParameter("period"));
                    data.setPorefno(request.getParameter("porefno"));
                    data.setPredate(request.getParameter("predate"));
                    data.setPreid(request.getParameter("preid"));
                    data.setPrename(request.getParameter("prename"));
                    data.setRacoacode(request.getParameter("racoacode"));
                    data.setRacoadesc(request.getParameter("racoadesc"));
                    data.setRemark(request.getParameter("remark"));
                    data.setSuppaddress(request.getParameter("suppaddress"));
                    data.setSuppcode(request.getParameter("suppcode"));
                    data.setSuppname(request.getParameter("suppname"));
                    data.setTotalamount(Double.parseDouble(request.getParameter("totalamount")));
                    data.setYear(request.getParameter("year"));
                    String refer = VendorInvoiceDAO.saveMain(log, data);
                    urlsend = "/ap_inv_edit_list.jsp?refer=" + refer;
                    break;
                }
                case "addffbprocess": {
//                    ArFFB ar = new ArFFB();a
//                    ar.setCess(ParseSafely.parseDoubleSafely(request.getParameter("cess")));
//                    ar.setDiscount(ParseSafely.parseDoubleSafely(request.getParameter("discount")));
//                    ar.setFfb(ParseSafely.parseDoubleSafely(request.getParameter("ffb")));
//                    ar.setKer(ParseSafely.parseDoubleSafely(request.getParameter("ker")));
//                    ar.setMpobpricepk(ParseSafely.parseDoubleSafely(request.getParameter("mpobpricepk")));
//                    ar.setMillcost(ParseSafely.parseDoubleSafely(request.getParameter("millcost")));
//                    ar.setMpobpricecpo(ParseSafely.parseDoubleSafely(request.getParameter("mpobpricecpo")));
//                    ar.setOer(ParseSafely.parseDoubleSafely(request.getParameter("oer")));
//                    ar.setTransport(ParseSafely.parseDoubleSafely(request.getParameter("transport")));
//                    ar.setWinfall(ParseSafely.parseDoubleSafely(request.getParameter("winfall")));
//                    ar.setExternalffb(ParseSafely.parseDoubleSafely(request.getParameter("externalffb")));
//                    ar.setYear(request.getParameter("year"));
//                    ar.setMonth(request.getParameter("month"));
//                    out.println(SalesInvoiceDAO.saveFFB(log, ar, request.getParameter("sessionid")));
//                    urlsend = "/ap_inv_edit_list.jsp?refer=" + refer;
                    break;
                }
                case "addmaterial":
                    urlsend = "/ap_inv_add_item.jsp?refer=" + request.getParameter("refer");
                    break;
                case "addinvmaterial":
                    try {

                        VendorInvoiceDetail data = new VendorInvoiceDetail();

                        data.setAccode(request.getParameter("accode"));
                        data.setAcdesc(request.getParameter("acdesc"));
                        data.setAmount(GeneralTerm.amountFormattoSave(request.getParameter("amount")));
                        data.setInvrefno(request.getParameter("loclevel"));//----
                        data.setLoccode(request.getParameter("loccode"));
                        data.setLocdesc(request.getParameter("locdesc"));
                        data.setLoclevel(request.getParameter("loclevel"));
                        data.setMatcode(request.getParameter("matcode"));
                        data.setMatdesc(request.getParameter("matdesc"));
                        data.setNo(request.getParameter("no"));
                        data.setQuantity(request.getParameter("quantity"));
                        data.setRemark(request.getParameter("remark"));
                        data.setSacode(request.getParameter("sacode"));
                        data.setSadesc(request.getParameter("sadesc"));
                        data.setSatype(request.getParameter("satype"));
                        data.setTaxamt(Double.parseDouble(request.getParameter("taxamt")));
                        data.setTaxcoacode(request.getParameter("taxcoacode"));
                        data.setTaxcoadescp(request.getParameter("taxcoadescp"));
                        data.setTaxcode(request.getParameter("taxcode"));
                        data.setTaxdescp(request.getParameter("taxdescp"));
                        data.setTaxrate(Double.parseDouble(request.getParameter("taxrate")));
                        data.setUnitmeasure(request.getParameter("unitmeasure"));
                        data.setUnitprice(request.getParameter("unitprice"));

                        Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------mark #1.7 end before save---");
                        VendorInvoiceDAO.saveItem(log, data);
                        urlsend = "/ap_inv_edit_list.jsp?refer=" + request.getParameter("no");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "addrefer":
                    urlsend = "/ap_inv_add_refer.jsp?refer=" + request.getParameter("refer");
                    break;
                case "saverefer":
                    try {

                        VendorInvoiceRefer data = new VendorInvoiceRefer();

                        data.setAmount(Double.parseDouble(request.getParameter("amount")));
                        data.setNo(request.getParameter("no"));
                        data.setRefer(request.getParameter("refer"));
                        data.setReferid(request.getParameter("referid"));
                        data.setRemark(request.getParameter("remark"));
                        data.setTarikh(request.getParameter("tarikh"));
                        data.setType(request.getParameter("type"));

                        VendorInvoiceDAO.saveRefer(log, data);
                        urlsend = "/ap_inv_edit_list.jsp?refer=" + request.getParameter("refer");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "updaterefer":
                    try {

                        VendorInvoiceRefer data = new VendorInvoiceRefer();

                        data.setAmount(Double.parseDouble(request.getParameter("amount")));
                        data.setNo(request.getParameter("no"));
                        data.setRefer(request.getParameter("refer"));
                        data.setReferid(request.getParameter("referid"));
                        data.setRemark(request.getParameter("remark"));
                        data.setTarikh(request.getParameter("tarikh"));
                        data.setType(request.getParameter("type"));

                        VendorInvoiceDAO.updateRefer(log, data, request.getParameter("referid"));
                        urlsend = "/ap_inv_edit_list.jsp?refer=" + request.getParameter("refer");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "editlist":
                    urlsend = "/ap_inv_edit_list.jsp?refer=" + request.getParameter("referno");
                    break;
                case "viewprinted":
                    urlsend = "/print_voucher.jsp?refer=" + request.getParameter("refer");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("View Printed Format");
                    break;
                case "editmain":
                    urlsend = "/ap_inv_edit_main.jsp?refer=" + request.getParameter("referno");
                    break;
                case "edit_st":
                    urlsend = "/ap_inv_edit_item.jsp?referno=" + request.getParameter("referno");
                    break;
                case "edit_nd":
                    urlsend = "/ap_inv_edit_refer.jsp?referno=" + request.getParameter("referno");
                    break;
                case "editmainprocess": {
                    VendorInvoice data = new VendorInvoice();
                    data.setAccode(request.getParameter("accode"));
                    data.setAcdesc(request.getParameter("acdesc"));
                    data.setEstatecode(request.getParameter("estatecode"));
                    data.setEstatename(request.getParameter("estatename"));
                    data.setDate(request.getParameter("date"));
                    data.setGstid(request.getParameter("gstid"));
                    data.setInvno(request.getParameter("invno"));
                    data.setInvrefno(request.getParameter("invrefno"));
                    data.setInvtype(request.getParameter("invtype"));
                    data.setNogrn(request.getParameter("nogrn"));
                    data.setNoinv(request.getParameter("noinv"));
                    //data.setPaid(Double.parseDouble(request.getParameter("paid")));
                    data.setPeriod(request.getParameter("period"));
                    data.setPorefno(request.getParameter("porefno"));
                    data.setPredate(request.getParameter("predate"));
                    data.setPreid(request.getParameter("preid"));
                    data.setPrename(request.getParameter("prename"));
                    data.setRacoacode(request.getParameter("racoacode"));
                    data.setRacoadesc(request.getParameter("racoadesc"));
                    data.setRemark(request.getParameter("remark"));
                    data.setSuppaddress(request.getParameter("suppaddress"));
                    data.setSuppcode(request.getParameter("suppcode"));
                    data.setSuppname(request.getParameter("suppname"));
                    data.setTotalamount(Double.parseDouble(request.getParameter("totalamount")));
                    data.setYear(request.getParameter("year"));
                    Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------mark #1.6 end before save---");
                    VendorInvoiceDAO.updateMain(log, data, request.getParameter("invrefno"));
                    urlsend = "/ap_inv_edit_list.jsp?refer=" + request.getParameter("invrefno");
                    break;
                }
                case "edititemprocess": {
                    VendorInvoiceDetail data = new VendorInvoiceDetail();
                    data.setAccode(request.getParameter("accode"));
                    data.setAcdesc(request.getParameter("acdesc"));
                    data.setAmount(GeneralTerm.amountFormattoSave(request.getParameter("amount")));
                    data.setInvrefno(request.getParameter("loclevel"));//----
                    data.setLoccode(request.getParameter("loccode"));
                    data.setLocdesc(request.getParameter("locdesc"));
                    data.setLoclevel(request.getParameter("loclevel"));
                    data.setMatcode(request.getParameter("matcode"));
                    data.setMatdesc(request.getParameter("matdesc"));
                    data.setNo(request.getParameter("no"));
                    data.setQuantity(request.getParameter("quantity"));
                    data.setRemark(request.getParameter("remark"));
                    data.setSacode(request.getParameter("sacode"));
                    data.setSadesc(request.getParameter("sadesc"));
                    data.setSatype(request.getParameter("satype"));
                    data.setTaxamt(Double.parseDouble(request.getParameter("taxamt")));
                    data.setTaxcoacode(request.getParameter("taxcoacode"));
                    data.setTaxcoadescp(request.getParameter("taxcoadescp"));
                    data.setTaxcode(request.getParameter("taxcode"));
                    data.setTaxdescp(request.getParameter("taxdescp"));
                    data.setTaxrate(Double.parseDouble(request.getParameter("taxrate")));
                    data.setUnitmeasure(request.getParameter("unitmeasure"));
                    data.setUnitprice(request.getParameter("unitprice"));
                    VendorInvoiceDAO.updateItem(log, data, request.getParameter("invrefno"));
                    urlsend = "/ap_inv_edit_list.jsp?refer=" + request.getParameter("no");
                    break;
                }
                case "edititem":
                    urlsend = "/ap_inv_edit_item.jsp?referno=" + request.getParameter("referno");
                    break;
                case "post":
                    //JournalVoucherDAO.deleteJournalItem(request.getParameter("referno"),request.getParameter("referno").substring(0,15));
                    urlsend = "/gl_post.jsp?referno=" + request.getParameter("referno") + "&vtype=PV";
                    break;
                case "dist":
                    Distribute dist = (Distribute) DistributeDAO.distributeNow(request.getParameter("referno"), "PNV", log);
                    session.setAttribute("post_detail", dist);

                    Logger.getLogger(PathController.class.getName()).log(Level.INFO, "------------" + String.valueOf(dist.isSuccess()));
                    if (dist.isSuccess()) {

                        urlsend = "/gl_post_status.jsp";
                    } else {
                        VendorInvoiceDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());
                        urlsend = "/gl_post_status.jsp";
                    }

                    break;
                case "delete":
                    VendorInvoiceDAO.deleteExist(log, request.getParameter("referno"));
                    urlsend = "/ap_inv_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");
                    break;
                case "check":
                    urlsend = "/check_voucher.jsp?referno=" + request.getParameter("referno") + "&vtype=PNV";
                    break;
                case "checkprocess":
                    VendorInvoiceDAO.checkVoucher(log, request.getParameter("referno"), log.getUserID(), log.getUserName());
                    urlsend = "/ap_inv_edit_list.jsp?refer=" + request.getParameter("referno");
                    break;
                case "approve":
                    VendorInvoiceDAO.approveVoucher(log, request.getParameter("refer"), log.getUserID(), log.getFullname(), log.getPosition());
                    urlsend = "/ap_pv_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");
                    break;
                case "delete_nd":
                    VendorInvoiceDAO.deleteRefer(log, request.getParameter("referno"), request.getParameter("referno").substring(0, 15));
                    urlsend = "/ap_inv_edit_list.jsp?refer=" + request.getParameter("referno").substring(0, 15);
                    break;
                case "delete_st":
                    VendorInvoiceDAO.deleteItem(log, request.getParameter("referno"), request.getParameter("referno").substring(0, 15));
                    urlsend = "/ap_inv_edit_list.jsp?refer=" + request.getParameter("referno").substring(0, 15);
                    break;
                case "createnote":
                    //String newrefer = "PVN991217040003";
                    Master post = (Master) PostDAO.getInfo(log, request.getParameter("refer"), "PNV");
                    DebitCreditNoteDAO.generateDebitCredit(post, log);
                    
                    notify = "&status=success&text=Debit Note created.";
                    urlsend = "/ap_inv_edit_list.jsp?refer=" + request.getParameter("refer")+notify;

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("Debit Note created for "+request.getParameter("refer")+".");
                    break;
                case "replicate":
                    //String newrefer = "PVN991217040003";
                    String newrefer = VendorInvoiceDAO.replicateData(log, request.getParameter("refer"), request.getParameter("replicatedate"), request.getParameter("replicateyear"), request.getParameter("replicateperiod"));
                    notify = "&status=success&text=Vendor Invoice has been replicated.";
                    urlsend = "/ap_inv_edit_list.jsp?moduleid="+moduleid+"&refer=" + newrefer+notify;

                    sys.setLog(true);
                    sys.setReferno(newrefer);
                    sys.setRemark("Payment Voucher replicated from "+request.getParameter("refer")+".");
                    break;
                case "viewsummary":
                    urlsend = "/ap_inv_summary_view.jsp?refer=" + request.getParameter("referno");

                    break;
                case "viewprinted2":
                    urlsend = "/ar_inv_summary_view_printed.jsp?refer=" + request.getParameter("refer") + "&status="+request.getParameter("status");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("View Printed Format");
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("100");//nullpointer
                    ErrorIO.setType("data");
                    urlsend = "/error.jsp";
                    break;
            }

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_VendorInvoice.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_VendorInvoice.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
