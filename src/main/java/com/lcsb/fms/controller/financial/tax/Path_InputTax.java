/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.tax;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.financial.ap.VendorPaymentDAO;
import com.lcsb.fms.dao.financial.gl.DebitNoteDAO;
import com.lcsb.fms.dao.financial.tx.TaxInputDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.model.financial.tx.TxInput;
import com.lcsb.fms.model.financial.tx.TxInputDetail;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_InputTax", urlPatterns = {"/Path_InputTax"})
public class Path_InputTax extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/tx_input_list.jsp?id=" + moduleid;
                    break;
                case "addinput":
                    urlsend = "/tx_input_add.jsp";
                    break;
                case "addinputprocess":

                    TxInput tx = new TxInput();
                    tx.setName(request.getParameter("name"));
                    tx.setPeriod(Integer.parseInt(request.getParameter("period")));
                    tx.setTaxperiodfrom(AccountingPeriod.convertStringtoDate(request.getParameter("taxperiodfrom")));
                    tx.setTaxperiodto(AccountingPeriod.convertStringtoDate(request.getParameter("taxperiodto")));
                    tx.setTaxreturndate(AccountingPeriod.convertStringtoDate(request.getParameter("taxreturndate")));
                    tx.setYear(Integer.parseInt(request.getParameter("year")));

                    String refer = TaxInputDAO.saveMain(tx, log);
                    urlsend = "/tx_input_list.jsp?refer=" + request.getParameter("refer");
                    break;
                case "addinputdetail":

                    String[] cnt = request.getParameterValues("checkboxdisno");
                    
                    int x = 0;
                    for (int i = 0; i < cnt.length; i++) {

                        String referno = cnt[i];
                        String trxtype = request.getParameter("trxtype" + cnt[i]);

                        TxInputDetail toSave = TaxInputDAO.getSelectedTaxDetail(log, referno, trxtype);
                        TaxInputDAO.saveDetail(log, toSave, request.getParameter("refno"));
                        
                        x++;
                    }
                    
                    if(x>0){
                        TaxInputDAO.updateTaxStatus(log, request.getParameter("refno"), "Generated");
                    }
                    //String refer = TaxInputDAO.saveMain(tx, log);
                    urlsend = "/tx_input_view.jsp?refer=" + request.getParameter("refno");
                    break;
                case "generateJV":

                    break;
                case "delete":
                    DebitNoteDAO.deleteExist(log, request.getParameter("referno"));
                    urlsend = "/gl_debitnote_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");
                    break;
                case "viewdetail":
                    urlsend = "/tx_input_view.jsp?refer=" + request.getParameter("refer");
                    break;
                case "deletedetail":
                    TaxInputDAO.deleteTaxDetail(log, request.getParameter("refer"));
                    urlsend = "/tx_input_view.jsp?refer=" + request.getParameter("refer");
                    break;
                case "checkprocess":
                    VendorPaymentDAO.checkVoucher(log, request.getParameter("referno"), log.getUserID(), log.getUserName());
                    urlsend = "/ap_pv_edit_list.jsp?refer=" + request.getParameter("referno");
                    break;
                case "gotoJV":
                    urlsend = "/gl_jv_edit_list.jsp?refer=" + request.getParameter("referno");
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("100");//nullpointer
                    ErrorIO.setType("data");
                    urlsend = "/error.jsp";
                    break;
            }

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_InputTax.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_InputTax.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
