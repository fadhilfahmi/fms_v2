/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.cashbook;

import com.lcsb.fms.controller.PathController;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.financial.ap.VendorPaymentDAO;
import com.lcsb.fms.dao.financial.cashbook.PaymentVoucherDAO;
import com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.DistributeDAO;
import com.lcsb.fms.dao.financial.gl.PostDAO;
import com.lcsb.fms.financial.gl.post.Distribute;
import com.lcsb.fms.financial.gl.post.Master;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucher;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherRefer;
import com.lcsb.fms.sys.log.SysLog;
import com.lcsb.fms.sys.log.SysLogDAO;
import com.lcsb.fms.util.dao.ModuleDAO;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_PaymentVoucher", urlPatterns = {"/Path_PaymentVoucher"})
public class Path_PaymentVoucher extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            SysLog sys = new SysLog();

            sys.setModuleid(moduleid);
            sys.setProcess(process);
            sys.setUserid(log.getUserID());
            sys.setDate(AccountingPeriod.getCurrentTimeStamp());
            sys.setTime(AccountingPeriod.getCurrentTime());
            sys.setLog(false);
            sys.setIp(request.getRemoteAddr());

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/cb_pv_list.jsp?id=" + moduleid;
                    break;
                case "addnew":

                    urlsend = "/cb_pv_add.jsp";
                    break;
                case "addpv": {
                    PaymentVoucher data = new PaymentVoucher();
                    data.setTarikh(request.getParameter("tarikh"));
                    data.setPaidtype(request.getParameter("paidtype"));
                    data.setPaidcode(request.getParameter("paidcode"));
                    data.setPaidname(request.getParameter("paidname"));
                    data.setPaidaddress(request.getParameter("paidaddress"));
                    data.setPaidcity(request.getParameter("paidcity"));
                    data.setPaidstate(request.getParameter("paidstate"));
                    data.setPaidpostcode(request.getParameter("paidpostcode"));
                    data.setGstid(request.getParameter("gstid"));
                    data.setRemarks(request.getParameter("remarks"));
                    data.setRacoacode(request.getParameter("racoacode"));
                    data.setRacoadesc(request.getParameter("racoadesc"));
                    data.setAmount(Double.parseDouble(request.getParameter("amount")));
                    data.setRm(request.getParameter("rm"));
                    data.setPaymentmode(request.getParameter("paymentmode"));
                    data.setBankcode(request.getParameter("bankcode"));
                    data.setBankname(request.getParameter("bankname"));
                    data.setCekno(request.getParameter("cekno"));
                    data.setYear(request.getParameter("year"));
                    data.setPeriod(request.getParameter("period"));
                    data.setEstatecode(request.getParameter("estatecode"));
                    data.setEstatename(request.getParameter("estatename"));
                    data.setPreid(log.getUserID());
                    data.setPrename(log.getFullname());
                    data.setPreparedate(AccountingPeriod.getCurrentTimeStamp());
                    data.setPreposition(log.getPosition());
                    //String newrefer = OfficialReceiptDAO.saveMain(data);
                    String newrefer = VendorPaymentDAO.saveMain(log, data);
                    urlsend = "/ap_pv_edit_list.jsp?refer=" + newrefer;

                    sys.setLog(true);
                    sys.setReferno(newrefer);
                    sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " added");
                    break;
                }
                case "adddebit":
                    urlsend = "/ap_pv_add_item.jsp?refer=" + request.getParameter("refer");
                    break;
                case "addrefer":
                    urlsend = "/ap_pv_add_refer.jsp?refer=" + request.getParameter("refer");
                    break;
                case "addpvitem":
                    try {

                        PaymentVoucherItem data = new PaymentVoucherItem();

                        //data.setAmount(Double.parseDouble(request.getParameter("amount")));
                        data.setLoclevel(request.getParameter("loclevel"));
                        data.setLoccode(request.getParameter("loccode"));
                        data.setLocname(request.getParameter("locname"));
                        data.setCoacode(request.getParameter("coacode"));
                        data.setCoadescp(request.getParameter("coadescp"));
                        data.setSatype(request.getParameter("satype"));
                        data.setSacode(request.getParameter("sacode"));
                        data.setSadesc(request.getParameter("sadesc"));
                        data.setRemarks(request.getParameter("remarks"));
                        Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------mark #1 for remarks---" + request.getParameter("remarks"));
                        data.setAmount(Double.parseDouble(request.getParameter("amount")));
                        data.setTaxcode(request.getParameter("taxcode"));
                        data.setTaxdescp(request.getParameter("taxdescp"));
                        data.setTaxrate(Double.parseDouble(request.getParameter("taxrate")));
                        data.setTaxamt(Double.parseDouble(request.getParameter("taxamt")));
                        data.setRefer(request.getParameter("refer"));
                        data.setVoucer(request.getParameter("voucer"));
                        data.setTaxcoacode(request.getParameter("taxcoacode"));
                        data.setTaxcoadescp(request.getParameter("taxcoadescp"));

                        Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------mark #1.6 end before save---");
                        VendorPaymentDAO.saveItem(log, data);
                        //urlsend = "/ap_pv_list.jsp?id="+moduleid;//+"&referenceno="+request.getParameter("referenceno");
                        urlsend = "/ap_pv_edit_list.jsp?moduleid="+moduleid+"&refer=" + request.getParameter("refer");

                        sys.setLog(true);
                        sys.setReferno(request.getParameter("refer"));
                        sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " Debit Account added.");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "saverefer":
                    try {

                        PaymentVoucherRefer data = new PaymentVoucherRefer();

                        data.setAmount(Double.parseDouble(request.getParameter("amount")));
                        data.setBf(request.getParameter("bf"));
                        data.setCf(request.getParameter("cf"));
                        data.setNo(request.getParameter("no"));
                        data.setPaid(request.getParameter("paid"));
                        data.setPayment(request.getParameter("payment"));
                        data.setRefer(request.getParameter("refer"));
                        data.setRemarks(request.getParameter("remarks"));
                        data.setTarikh(request.getParameter("tarikh"));
                        data.setType(request.getParameter("type"));
                        data.setVoucer(request.getParameter("voucer"));

                        VendorPaymentDAO.saveRefer(log, data);
                        urlsend = "/ap_pv_edit_list.jsp?refer=" + request.getParameter("voucer");

                        sys.setLog(true);
                        sys.setReferno(request.getParameter("voucer"));
                        sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " Reference added.");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "updaterefer":
                    try {

                        PaymentVoucherRefer data = new PaymentVoucherRefer();

                        data.setAmount(Double.parseDouble(request.getParameter("amount")));
                        data.setBf(request.getParameter("bf"));
                        data.setCf(request.getParameter("cf"));
                        data.setNo(request.getParameter("no"));
                        data.setPaid(request.getParameter("paid"));
                        data.setPayment(request.getParameter("payment"));
                        data.setRefer(request.getParameter("refer"));
                        data.setRemarks(request.getParameter("remarks"));
                        data.setTarikh(request.getParameter("tarikh"));
                        data.setType(request.getParameter("type"));
                        data.setVoucer(request.getParameter("voucer"));

                        VendorPaymentDAO.updateRefer(log, data, request.getParameter("refer"));
                        urlsend = "/ap_pv_edit_list.jsp?refer=" + request.getParameter("voucer");

                        sys.setLog(true);
                        sys.setReferno(request.getParameter("voucer"));
                        sys.setRemark("Reference updated (" + request.getParameter("refer") + ").");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "editlist":
                    urlsend = "/ap_pv_edit_list.jsp?refer=" + request.getParameter("referno") + "&moduleid=" + request.getParameter("moduleid");
                    break;
                case "edit_nd":
                    urlsend = "/ap_pv_edit_refer.jsp?refer=" + request.getParameter("referno");
                    break;
                case "viewprinted":
                    urlsend = "/print_voucher.jsp?refer=" + request.getParameter("refer");
                    break;
                case "editmain":
                    urlsend = "/cb_pv_edit_main.jsp?refer=" + request.getParameter("referno");
                    break;
                case "editmainprocess": {
                    PaymentVoucher data = new PaymentVoucher();
                    data.setRefer(request.getParameter("refer"));
                    data.setTarikh(request.getParameter("tarikh"));
                    data.setPaidtype(request.getParameter("paidtype"));
                    data.setPaidcode(request.getParameter("paidcode"));
                    data.setPaidname(request.getParameter("paidname"));
                    data.setPaidaddress(request.getParameter("paidaddress"));
                    data.setPaidcity(request.getParameter("paidcity"));
                    data.setPaidstate(request.getParameter("paidstate"));
                    data.setPaidpostcode(request.getParameter("paidpostcode"));
                    data.setGstid(request.getParameter("gstid"));
                    data.setRemarks(request.getParameter("remarks"));
                    data.setRacoacode(request.getParameter("racoacode"));
                    data.setRacoadesc(request.getParameter("racoadesc"));
                    data.setAmount(Double.parseDouble(request.getParameter("amount")));
                    data.setRm(request.getParameter("rm"));
                    data.setPaymentmode(request.getParameter("paymentmode"));
                    data.setBankcode(request.getParameter("bankcode"));
                    data.setBankname(request.getParameter("bankname"));
                    data.setCekno(request.getParameter("cekno"));
                    data.setYear(request.getParameter("year"));
                    data.setPeriod(request.getParameter("period"));
                    data.setEstatecode(request.getParameter("estatecode"));
                    data.setEstatename(request.getParameter("estatename"));
                    data.setVoucherno(request.getParameter("voucherno"));
                    VendorPaymentDAO.updateMain(log, data, request.getParameter("refer"));
                    urlsend = "/ap_pv_edit_list.jsp?moduleid="+moduleid+"&refer=" + request.getParameter("refer");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " updated (" + request.getParameter("voucherno") + ").");

                    break;
                }
                case "edititemprocess": {
                    PaymentVoucherItem data = new PaymentVoucherItem();
                    data.setLoclevel(request.getParameter("loclevel"));
                    data.setLoccode(request.getParameter("loccode"));
                    data.setLocname(request.getParameter("locname"));
                    data.setCoacode(request.getParameter("coacode"));
                    data.setCoadescp(request.getParameter("coadescp"));
                    data.setSatype(request.getParameter("satype"));
                    data.setSacode(request.getParameter("sacode"));
                    data.setSadesc(request.getParameter("sadesc"));
                    data.setRemarks(request.getParameter("remarks"));
                    data.setAmount(Double.parseDouble(request.getParameter("amount")));
                    data.setTaxcode(request.getParameter("taxcode"));
                    data.setTaxdescp(request.getParameter("taxdescp"));
                    data.setTaxrate(Double.parseDouble(request.getParameter("taxrate")));
                    data.setTaxamt(Double.parseDouble(request.getParameter("taxamt")));
                    data.setRefer(request.getParameter("refer"));
                    data.setVoucer(request.getParameter("voucer"));
                    VendorPaymentDAO.updateItem(log, data, request.getParameter("voucer"));
                    urlsend = "/ap_pv_edit_list.jsp?refer=" + request.getParameter("refer");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("Debit account updated (" + request.getParameter("voucer") + ").");
                    break;
                }
                case "edit_st":
                    urlsend = "/ap_pv_edit_item.jsp?referno=" + request.getParameter("referno");
                    break;
                case "post":
                    //JournalVoucherDAO.deleteJournalItem(request.getParameter("referno"),request.getParameter("referno").substring(0,15));
                    urlsend = "/gl_post.jsp?referno=" + request.getParameter("referno") + "&vtype=PV";
                    break;
                case "dist":
                    Distribute dist = (Distribute) DistributeDAO.distributeNow(request.getParameter("referno"), "PV", log);
                    session.setAttribute("post_detail", dist);

                    Logger.getLogger(PathController.class.getName()).log(Level.INFO, "------------" + String.valueOf(dist.isSuccess()));
                    if (dist.isSuccess()) {

                        urlsend = "/gl_post_status.jsp";
                    } else {
                        VendorPaymentDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());
                        urlsend = "/gl_post_status.jsp";

                        sys.setLog(true);
                        sys.setReferno(request.getParameter("referno"));
                        sys.setRemark("Approved & Posted");
                    }

                    break;
                case "delete":
                    VendorPaymentDAO.deleteExist(log, request.getParameter("referno"));
                    urlsend = "/ap_pv_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");
                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno"));
                    sys.setRemark("Deleted.");
                    break;
                case "check":
                    urlsend = "/check_voucher.jsp?referno=" + request.getParameter("referno") + "&vtype=PV";
                    break;
                case "checkprocess":
                    VendorPaymentDAO.checkVoucher(log, request.getParameter("referno"), log.getUserID(), log.getUserName());
                    urlsend = "/ap_pv_edit_list.jsp?refer=" + request.getParameter("referno");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno"));
                    sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " Checked.");
                    break;
                case "approve":
                    VendorPaymentDAO.approveVoucher(log, request.getParameter("refer"), log.getUserID(), log.getFullname(), log.getPosition());
                    urlsend = "/ap_pv_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");
                    break;
                case "delete_nd":
                    VendorPaymentDAO.deleteRefer(log, request.getParameter("referno"), request.getParameter("referno").substring(0, 15));
                    urlsend = "/ap_pv_edit_list.jsp?refer=" + request.getParameter("referno").substring(0, 15);

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno").substring(0, 15));
                    sys.setRemark("Reference deleted (" + request.getParameter("referno") + ").");
                    break;
                case "delete_st":
                    VendorPaymentDAO.deleteItem(log, request.getParameter("referno"), request.getParameter("referno").substring(0, 15));
                    urlsend = "/ap_pv_edit_list.jsp?refer=" + request.getParameter("referno").substring(0, 15);

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno").substring(0, 15));
                    sys.setRemark("Debit Account deleted (" + request.getParameter("referno") + ").");
                    break;
                case "savevoucher":
                    String refer = PaymentVoucherDAO.generateVoucher(log, request.getParameter("sessionid"), log.getEstateCode(), log.getEstateDescp());
                    notify = "&status=success&text=Payment Voucher has been added.";
                    urlsend = "/ap_pv_edit_list.jsp?refer=" + refer + notify;

                    sys.setLog(true);
                    sys.setReferno(refer);
                    sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " added.");
                    break;
                case "printcheque":
                    
                    notify = "&status=success&text=Payment Voucher has been added.";
                    urlsend = "/print_cheque.jsp";

                    sys.setLog(true);
                    //sys.setReferno(refer);
                    //sys.setRemark(ModuleDAO.getModule(moduleid).getModuleDesc() + " added.");
                    break;
                case "replicate":
                    //String newrefer = "PVN991217040003";
                    String newrefer = PaymentVoucherDAO.replicateData(log, request.getParameter("refer"), request.getParameter("replicatedate"), request.getParameter("replicateyear"), request.getParameter("replicateperiod"));
                    notify = "&status=success&text=Payment Voucher has been replicated.";
                    urlsend = "/ap_pv_edit_list.jsp?moduleid="+moduleid+"&refer=" + newrefer+notify;

                    sys.setLog(true);
                    sys.setReferno(newrefer);
                    sys.setRemark("Payment Voucher replicated from "+request.getParameter("refer")+".");
                    break;
                case "createnote":
                    //String newrefer = "PVN991217040003";
                    Master post = (Master) PostDAO.getInfo(log, request.getParameter("refer"), "PV");
                    DebitCreditNoteDAO.generateDebitCredit(post, log);
                    
                    notify = "&status=success&text=Debit Note created.";
                    urlsend = "/ap_pv_edit_list.jsp?moduleid="+moduleid+"&refer=" + request.getParameter("refer")+notify;

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("Debit Note created for "+request.getParameter("refer")+".");
                    break;
                case "viewsummary":
                    urlsend = "/ap_pv_summary_view.jsp?refer=" + request.getParameter("referno");

                    break;
                case "viewprinted2":
                    urlsend = "/ar_inv_summary_view_printed.jsp?refer=" + request.getParameter("refer") + "&status="+request.getParameter("status");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("View Printed Format");
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("100");//nullpointer
                    ErrorIO.setType("data");
                    urlsend = "/error.jsp";
                    break;
            }

            SysLogDAO.saveLog(log, sys);

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);

        }
    }
    
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_PaymentVoucher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_PaymentVoucher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
