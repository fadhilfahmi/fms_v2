/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.cashbook;

import com.lcsb.fms.controller.PathController;
import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.financial.cashbook.CashVoucherDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.cashbook.CashVoucher;
import com.lcsb.fms.model.financial.cashbook.CashVoucherAccount;
import com.lcsb.fms.model.financial.cashbook.CbCashVoucherReceipt;
import com.lcsb.fms.model.financial.cashbook.CbCashVoucherReceiptPK;
import com.lcsb.fms.util.ext.ParseSafely;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_CashVoucher", urlPatterns = {"/Path_CashVoucher"})
public class Path_CashVoucher extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/cb_cv_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/cb_cv_add.jsp";
                    break;
                case "addcv": {
                    CashVoucher data = new CashVoucher();
                    data.setVoucherdate(request.getParameter("voucherdate"));
                    data.setYear(request.getParameter("year"));
                    data.setPeriod(request.getParameter("period"));
                    data.setPayto(request.getParameter("payto"));
                    data.setTotal(ParseSafely.parseDoubleSafely(request.getParameter("total")));
                    data.setAmount(request.getParameter("amount"));
                    data.setReason(request.getParameter("reason"));
                    data.setReceivetype(request.getParameter("receivetype"));
                    data.setReceiveid(request.getParameter("receiveid"));
                    data.setReceivebyname(request.getParameter("receivebyname"));
                    CashVoucherDAO.saveMain(log, data);
                    urlsend = "/cb_cv_list.jsp";
                    break;
                }
                case "updatecv": {
                    CashVoucher data = new CashVoucher();
                    data.setVoucherdate(request.getParameter("voucherdate"));
                    data.setYear(request.getParameter("year"));
                    data.setPeriod(request.getParameter("period"));
                    data.setPayto(request.getParameter("payto"));
                    data.setTotal(ParseSafely.parseDoubleSafely(request.getParameter("total")));
                    data.setAmount(request.getParameter("amount"));
                    data.setReason(request.getParameter("reason"));
                    data.setReceivetype(request.getParameter("receivetype"));
                    data.setReceiveid(request.getParameter("receiveid"));
                    data.setReceivebyname(request.getParameter("receivebyname"));
                    data.setEstatecode(request.getParameter("estatecode"));
                    data.setPrepareid(request.getParameter("prepareid"));
                    data.setPreparedate(request.getParameter("preparedate"));
                    data.setPreparename(request.getParameter("preparename"));
                    CashVoucherDAO.updateMain(log, data, request.getParameter("refer"));
                    urlsend = "/cb_cv_edit_list.jsp?referno=" + request.getParameter("refer");
                    break;
                }
                case "addaccount":
                    urlsend = "/cb_cv_add_item.jsp?referno=" + request.getParameter("refer");
                    break;
                case "addreceipt":
                    urlsend = "/cb_cv_add_receipt.jsp?referno=" + request.getParameter("refer");
                    break;
                case "addcvitem":
                    try {
                        Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------dfgdfgdfgd---");
                        CashVoucherAccount dataCV = new CashVoucherAccount();

                        dataCV.setRefer(request.getParameter("refer"));
                        dataCV.setDebit(String.valueOf(GeneralTerm.amountFormattoSave(request.getParameter("debit"))));
                        dataCV.setLoclevel(request.getParameter("loclevel"));
                        dataCV.setLoccode(request.getParameter("loccode"));
                        dataCV.setLocname(request.getParameter("locname"));
                        dataCV.setCoacode(request.getParameter("coacode"));
                        dataCV.setCoadescp(request.getParameter("coadescp"));
                        dataCV.setRemarks(request.getParameter("remarks"));
                        dataCV.setSatype(request.getParameter("satype"));
                        dataCV.setSacode(request.getParameter("sacode"));
                        dataCV.setSadesc(request.getParameter("sadesc"));
                        dataCV.setTaxcode(request.getParameter("taxcode"));
                        dataCV.setTaxdescp(request.getParameter("taxdescp"));
                        dataCV.setTaxrate(Double.parseDouble(request.getParameter("taxrate")));
                        dataCV.setTaxamt(Double.parseDouble(request.getParameter("taxamt")));
                        dataCV.setAmtbeforetax(GeneralTerm.amountFormattoSave(request.getParameter("amtbeforetax")));

                        CashVoucherDAO.saveItem(log, dataCV);
                        urlsend = "/cb_cv_edit_list.jsp?referno=" + request.getParameter("refer");
                        
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "editlist":
                    urlsend = "/cb_cv_edit_list.jsp?referno=" + request.getParameter("referno");
                    break;
                case "editmain":
                    Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------ddddddddddd---");
                    urlsend = "/cb_cv_edit_main.jsp?referno=" + request.getParameter("referno");
                    break;
                case "editcvitem":
                    urlsend = "/cb_cv_edit_item.jsp?referno=" + request.getParameter("referno");
                    break;
                case "updatecvitem":
                    try {
                        Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------dfgdfgdfgd---");
                        CashVoucherAccount dataCV = new CashVoucherAccount();

                        dataCV.setRefer(request.getParameter("refer"));
                        dataCV.setDebit(String.valueOf(GeneralTerm.amountFormattoSave(request.getParameter("debit"))));
                        dataCV.setLoclevel(request.getParameter("loclevel"));
                        dataCV.setLoccode(request.getParameter("loccode"));
                        dataCV.setLocname(request.getParameter("locname"));
                        dataCV.setCoacode(request.getParameter("coacode"));
                        dataCV.setCoadescp(request.getParameter("coadescp"));
                        dataCV.setRemarks(request.getParameter("remarks"));
                        dataCV.setSatype(request.getParameter("satype"));
                        dataCV.setSacode(request.getParameter("sacode"));
                        dataCV.setSadesc(request.getParameter("sadesc"));
                        dataCV.setTaxcode(request.getParameter("taxcode"));
                        dataCV.setTaxdescp(request.getParameter("taxdescp"));
                        dataCV.setTaxrate(Double.parseDouble(request.getParameter("taxrate")));
                        dataCV.setTaxamt(Double.parseDouble(request.getParameter("taxamt")));
                        dataCV.setAmtbeforetax(GeneralTerm.amountFormattoSave(request.getParameter("amtbeforetax")));

                        CashVoucherDAO.updateItem(log, dataCV, request.getParameter("novoucher"));
                        urlsend = "/cb_cv_edit_list.jsp?referno=" + request.getParameter("refer");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "viewcv":
                    urlsend = "/cb_cv_view_voucher.jsp?referno=" + request.getParameter("referno");
                    break;
                case "deletecv":
                    CashVoucherDAO.deleteExist(log, request.getParameter("referno"));
                    urlsend = "/cb_cv_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");
                    break;
                case "deletecvitem":
                    //Logger.getLogger(PathController.class.getName()).log(Level.INFO, request.getParameter("referno").substring(0,15));
                    CashVoucherDAO.deleteItem(log, request.getParameter("referno"), request.getParameter("referno").substring(0, 15));
                    urlsend = "/cb_cv_edit_list.jsp?referno=" + request.getParameter("referno").substring(0, 15);
                    break;
                case "addreceiptprocess": {
                    CbCashVoucherReceipt data = new CbCashVoucherReceipt();
                    CbCashVoucherReceiptPK dataPK = new CbCashVoucherReceiptPK();
                    
                    dataPK.setNoreceipt(request.getParameter("noreceipt"));
                    dataPK.setRefer(request.getParameter("refer"));
                    data.setCbCashVoucherReceiptPK(dataPK);
                    data.setGstid(request.getParameter("gstid"));
                    data.setNovoucer(request.getParameter("novoucer"));
                    data.setRemarks(request.getParameter("remarks"));
                    data.setTarikh(AccountingPeriod.convertStringtoDate(request.getParameter("tarikh")));
                    data.setTotal(Double.parseDouble(request.getParameter("total")));
                    data.setTrade(request.getParameter("trade"));
                    CashVoucherDAO.saveReceipt(log, data);
                    urlsend = "/cb_cv_edit_list.jsp?referno=" + request.getParameter("refer");
                    break;
                }
                default:
                    break;
            }

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_CashVoucher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_CashVoucher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
