/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.cashbook;

import com.lcsb.fms.controller.PathController;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.financial.ap.VendorPaymentDAO;
import com.lcsb.fms.dao.financial.cashbook.BankReportDAO;
import com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO;
import com.lcsb.fms.dao.financial.cashbook.PaymentVoucherDAO;
import com.lcsb.fms.dao.financial.gl.DistributeDAO;
import com.lcsb.fms.financial.gl.post.Distribute;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.model.financial.cashbook.BankReconcilation;
import com.lcsb.fms.model.financial.cashbook.BankReconcilationPK;
import com.lcsb.fms.model.financial.cashbook.BankReportParam;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucher;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem;
import com.lcsb.fms.model.financial.cashbook.PaymentVoucherRefer;
import com.lcsb.fms.model.financial.cashbook.BankReconcile;
import com.lcsb.fms.model.financial.cashbook.BankReconcilePK;
import com.lcsb.fms.sys.log.SysLog;
import com.lcsb.fms.sys.log.SysLogDAO;
import com.lcsb.fms.util.dao.ModuleDAO;
import com.lcsb.fms.util.ext.ParseSafely;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_BankReport", urlPatterns = {"/Path_BankReport"})
public class Path_BankReport extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            SysLog sys = new SysLog();

            sys.setModuleid(moduleid);
            sys.setProcess(process);
            sys.setUserid(log.getUserID());
            sys.setDate(AccountingPeriod.getCurrentTimeStamp());
            sys.setTime(AccountingPeriod.getCurrentTime());
            sys.setLog(false);
            sys.setIp(request.getRemoteAddr());

            String urlsend = "";
            String notify = "";

            BankReportParam prm = (BankReportParam) session.getAttribute("br_param");

            switch (process) {
                case "viewlist":
                    urlsend = "/cb_bankreport.jsp?id=" + moduleid;
                    break;
                case "addtrans":
                    urlsend = "/cb_bankreport_step.jsp?id=" + moduleid + "&bankcode=" + prm.getBankcode() + "&sessionid=" + session.getId() + AccountingPeriod.getCurrentTimeStamp() + AccountingPeriod.getCurrentTime();
                    break;
                case "viewbankreport":
                    urlsend = "/cb_bankreport_view.jsp?id=" + moduleid;
                    break;
                case "reconcilereport":
                    urlsend = "/cb_bankreport_recouncil.jsp?id=" + moduleid;
                    break;
                case "savereconcile": {

                    BankReconcile t = new BankReconcile();
                    BankReconcilePK tPK = new BankReconcilePK();
                    t.setA(ParseSafely.parseDoubleSafely(request.getParameter("statementbal")));
                    t.setAdate(request.getParameter("adate"));
                    t.setAdesignation(request.getParameter("adesignation"));
                    t.setAid(request.getParameter("aid"));
                    t.setAname(request.getParameter("aname"));
                    t.setB(ParseSafely.parseDoubleSafely(request.getParameter("totalg-outstanding-issued-cheque")));
                    t.setC(ParseSafely.parseDoubleSafely(request.getParameter("totalg-outstanding-received-cheque")));
                    t.setCdate(request.getParameter("cdate"));
                    t.setCid(request.getParameter("cid"));
                    t.setCname(request.getParameter("cname"));
                    tPK.setCodebank(request.getParameter("codebank"));
                    t.setD(ParseSafely.parseDoubleSafely(request.getParameter("totalg-bankcharges-expense")));
                    t.setE(ParseSafely.parseDoubleSafely(request.getParameter("totalg-bankcharges-receive")));
                    t.setEstatecode(request.getParameter("estatecode"));
                    t.setEstatename(request.getParameter("estatename"));
                    t.setF(ParseSafely.parseDoubleSafely(request.getParameter("totalg-hibah")));
                    tPK.setPeriod(request.getParameter("period"));
                    tPK.setYear(request.getParameter("year"));
                    
                    t.setBankReconcilePK(tPK);

                    //session.setAttribute("br_param", t);
                    BankReportDAO.updateBankReconcileMaster(log, t);

                    urlsend = "/cb_bankreport_view.jsp?id=" + moduleid;
                    break;
                }
                case "generateBankReport":

                    BankReportParam t = new BankReportParam();
                    t.setYear(request.getParameter("year"));
                    t.setPeriod(request.getParameter("period"));
                    t.setBankcode(request.getParameter("bankcode"));
                    t.setBankname(request.getParameter("bankname"));
                    t.setMonth(request.getParameter("month"));
                    t.setCarry(request.getParameter("carry"));
                    session.setAttribute("br_param", t);

                    BankReportDAO.runBankReport(log, t);

                    urlsend = "/cb_bankreport_view.jsp?id=" + moduleid;
                    break;
                case "savetrans":

                    BankReportDAO.updateBankStatusBeforeSave(log, prm);

                    List<BankReconcilation> at = new ArrayList();

                    List<BankReconcilation> listx = (List<BankReconcilation>) BankReportDAO.getAll(log, prm);
                    for (BankReconcilation j : listx) {

                        BankReconcilationPK pk = j.getBankReconcilationPK();

                        //Logger.getLogger(Path_BankReport.class.getName()).log(Level.INFO, "--------sssss to>>>" + request.getParameter("updateflag" + pk.getRefer()));


                            j.setAmountclear(ParseSafely.parseDoubleSafely(request.getParameter("amtclear" + pk.getRefer())));
                            j.setDateclear(request.getParameter("dateclear" + pk.getRefer()));
                            j.setStatus(request.getParameter("status" + pk.getRefer()));
                        
                        BankReportDAO.updateBankStatus(log, j, prm);
                    }
                    
                    notify = "&status=success&text=Bank Report Detail Updated.";
                    urlsend = "/cb_bankreport_view.jsp?id=" + moduleid + notify;

                    break;
                case "savevoucher":

                    String refer = "";

                    if (request.getParameter("type").equals("pv")) {

                        refer = PaymentVoucherDAO.generateVoucher(log, request.getParameter("sessionid"), log.getEstateCode(), log.getEstateDescp());
                        notify = "&status=success&text=Payment Voucher has been added.";

                    } else if (request.getParameter("type").equals("or")) {

                        refer = OfficialReceiptDAO.generateReceiptGeneral(request.getParameter("sessionid"), log);
                        notify = "&status=success&text=Official Receipt has been added.";

                    }

                    BankReportDAO.runBankReport(log, prm);

                    urlsend = "/cb_bankreport_view.jsp?id=" + moduleid;

                    sys.setLog(true);
                    sys.setReferno(refer);
                    sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " added.");
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("100");//nullpointer
                    ErrorIO.setType("data");
                    urlsend = "/error.jsp";
                    break;
            }

            SysLogDAO.saveLog(log, sys);

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_BankReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_BankReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
