/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.ar;

import com.lcsb.fms.controller.PathController;
import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO;
import com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.DistributeDAO;
import com.lcsb.fms.dao.financial.gl.PostDAO;
import com.lcsb.fms.financial.gl.post.Distribute;
import com.lcsb.fms.financial.gl.post.Master;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.model.financial.ar.ArTempRefinery;
import com.lcsb.fms.model.financial.ar.SalesInvoice;
import com.lcsb.fms.model.financial.ar.SalesInvoiceItem;
import com.lcsb.fms.sys.log.SysLog;
import com.lcsb.fms.sys.log.SysLogDAO;
import com.lcsb.fms.util.ext.ParseSafely;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_ARInvoice", urlPatterns = {"/Path_ARInvoice"})
public class Path_ARInvoice extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            SysLog sys = new SysLog();

            sys.setModuleid(moduleid);
            sys.setProcess(process);
            sys.setUserid(log.getUserID());
            sys.setDate(AccountingPeriod.getCurrentTimeStamp());
            sys.setTime(AccountingPeriod.getCurrentTime());
            sys.setLog(false);
            sys.setIp(request.getRemoteAddr());

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/ar_inv_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    if(request.getParameter("type").equals("auto")){
                         urlsend = "/ar_inv_add.jsp?sessionid=" + session.getId() + AccountingPeriod.getCurrentTimeStamp() + AccountingPeriod.getCurrentTime();
                    }else if(request.getParameter("type").equals("manual")){
                        urlsend = "/ar_inv_add_manual.jsp?sessionid=" + session.getId() + AccountingPeriod.getCurrentTimeStamp() + AccountingPeriod.getCurrentTime();
                    }
                   
                    break;
                case "addinv": {
                    SalesInvoice data = new SalesInvoice();
                    data.setProdcode(request.getParameter("prodcode"));
                    data.setProdname(request.getParameter("prodname"));
                    data.setInvdate(request.getParameter("invdate"));
                    data.setLoclevel(request.getParameter("loclevel"));
                    data.setLoccode(request.getParameter("loccode"));
                    data.setLocdesc(request.getParameter("locdesc"));
                    data.setBtype(request.getParameter("btype"));
                    data.setBcode(request.getParameter("bcode"));
                    data.setBname(request.getParameter("bname"));
                    data.setBcity(request.getParameter("bcity"));
                    data.setBaddress(request.getParameter("baddress"));
                    data.setBpostcode(request.getParameter("bpostcode"));
                    data.setBstate(request.getParameter("bstate"));
                    data.setAmountno(ParseSafely.parseDoubleSafely(request.getParameter("amountno")));
                    data.setAmountstr(request.getParameter("amountstr"));
                    data.setRemarks(request.getParameter("remarks"));
                    data.setYear(ParseSafely.parseIntegerSafely(request.getParameter("year")));
                    data.setPeriod(ParseSafely.parseIntegerSafely(request.getParameter("period")));
                    data.setEstcode(request.getParameter("estcode"));
                    data.setEstname(request.getParameter("estname"));
                    data.setCoacode(request.getParameter("coacode"));
                    data.setCoadesc(request.getParameter("coadesc"));
                    data.setDono(request.getParameter("dono"));
                    data.setGstid(request.getParameter("gstid"));
                    data.setBtype("None");
                    data.setSacode(request.getParameter("bcode"));
                    data.setSadesc(request.getParameter("bname"));
                    data.setSatype("Buyer");
                    data.setPid(log.getUserID());
                    data.setPname(log.getFullname());
                    data.setPdate(AccountingPeriod.getCurrentTimeStamp());
                    
                    String newrefer = SalesInvoiceDAO.saveMain(log, data);
                    urlsend = "/ar_inv_add_item.jsp?referno=" + newrefer + "&prodcode"+request.getParameter("prodcode")+"&prodname="+request.getParameter("prodname");

                    sys.setLog(true);
                    sys.setReferno(newrefer);
                    sys.setRemark("Invoice added");
                    break;
                }
                case "adddetail":
                    urlsend = "/ar_inv_add_item.jsp?referno=" + request.getParameter("refer");
                    break;
                case "addinvitem":
                    try {
                        Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------dfgdfgdfgd---");
                        SalesInvoiceItem data = new SalesInvoiceItem();

                        data.setAmount(Double.parseDouble(request.getParameter("amount")));
                        data.setCoacode(request.getParameter("coacode"));
                        data.setCoaname(request.getParameter("coaname"));
                        data.setIvno(request.getParameter("ivno"));
                        data.setIvref(request.getParameter("ivref"));
                        data.setLoccode(request.getParameter("loccode"));
                        data.setLocdesc(request.getParameter("locdesc"));
                        data.setLoclevel(request.getParameter("loclevel"));
                        data.setProdcode(request.getParameter("prodcode"));
                        data.setProdname(request.getParameter("prodname"));
                        data.setQty(Double.parseDouble(request.getParameter("qty")));
                        data.setRefer(request.getParameter("refer"));
                        data.setRemarks(request.getParameter("remarks"));
                        data.setSacode(request.getParameter("sacode"));
                        data.setSadesc(request.getParameter("sadesc"));
                        data.setSatype(request.getParameter("satype"));
                        data.setTaxamt(Double.parseDouble(request.getParameter("taxamt")));
                        data.setTaxcoacode(request.getParameter("taxcoacode"));
                        data.setTaxcoadescp(request.getParameter("taxcoadescp"));
                        data.setTaxcode(request.getParameter("taxcode"));
                        data.setTaxrate(Double.parseDouble(request.getParameter("taxrate")));
                        data.setUnitm(request.getParameter("unitm"));
                        data.setUnitp(request.getParameter("unitp"));

                        SalesInvoiceDAO.saveItem(log, data);
                        urlsend = "/ar_inv_edit_list.jsp?referno=" + request.getParameter("ivref");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "editlist":
                    urlsend = "/ar_inv_edit_list.jsp?referno=" + request.getParameter("referno");

                    break;
                case "viewvoucher":
                    urlsend = "/ar_inv_view_voucher.jsp?referno=" + request.getParameter("refer");
                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    break;
                case "editmain":
                    urlsend = "/ar_inv_edit_main.jsp?referno=" + request.getParameter("referno");
                    break;
                case "editmainprocess": {
                    SalesInvoice data = new SalesInvoice();
                    data.setInvdate(request.getParameter("invdate"));
                    data.setLoclevel(request.getParameter("loclevel"));
                    data.setLoccode(request.getParameter("loccode"));
                    data.setLocdesc(request.getParameter("locdesc"));
                    data.setBtype(request.getParameter("btype"));
                    data.setBcode(request.getParameter("bcode"));
                    data.setBname(request.getParameter("bname"));
                    data.setBaddress(request.getParameter("baddress"));
                    data.setBpostcode(request.getParameter("bpostcode"));
                    data.setBstate(request.getParameter("bstate"));
                    data.setAmountno(Double.parseDouble(request.getParameter("amountno")));
                    data.setAmountstr(request.getParameter("amountstr"));
                    data.setRemarks(request.getParameter("remarks"));
                    data.setYear(Integer.parseInt(request.getParameter("year")));
                    data.setPeriod(Integer.parseInt(request.getParameter("period")));
                    data.setEstcode(request.getParameter("estcode"));
                    data.setEstname(request.getParameter("estname"));
                    data.setCoacode(request.getParameter("coacode"));
                    data.setCoadesc(request.getParameter("coadesc"));
                    data.setDono(request.getParameter("dono"));
                    data.setGstid(request.getParameter("gstid"));
                    data.setSatype(request.getParameter("satype"));
                    data.setSacode(request.getParameter("sacode"));
                    data.setSadesc(request.getParameter("sadesc"));
                    data.setRacoacode(request.getParameter("racoacode"));
                    data.setRacoadesc(request.getParameter("racoadesc"));

                    data.setOrderno(request.getParameter("orderno"));
                    
                    SalesInvoiceDAO.updateMain(log, data, request.getParameter("invref"));
                    urlsend = "/ar_inv_edit_list.jsp?referno=" + request.getParameter("invref");
                    sys.setLog(true);
                    sys.setReferno(request.getParameter("invref"));
                    sys.setRemark("Invoice Updated");
                    break;
                }
                case "edititemprocess": {
                    SalesInvoiceItem data = new SalesInvoiceItem();
                    data.setAmount(Double.parseDouble(request.getParameter("amount")));
                    data.setCoacode(request.getParameter("coacode"));
                    data.setCoaname(request.getParameter("coaname"));
                    data.setIvno(request.getParameter("ivno"));
                    data.setIvref(request.getParameter("ivref"));
                    data.setLoccode(request.getParameter("loccode"));
                    data.setLocdesc(request.getParameter("locdesc"));
                    data.setLoclevel(request.getParameter("loclevel"));
                    data.setProdcode(request.getParameter("prodcode"));
                    data.setProdname(request.getParameter("prodname"));
                    data.setQty(Double.parseDouble(request.getParameter("qty")));
                    data.setRefer(request.getParameter("refer"));
                    data.setRemarks(request.getParameter("remarks"));
                    data.setSacode(request.getParameter("sacode"));
                    data.setSadesc(request.getParameter("sadesc"));
                    data.setSatype(request.getParameter("satype"));
                    data.setTaxamt(Double.parseDouble(request.getParameter("taxamt")));
                    data.setTaxcoacode(request.getParameter("taxcoacode"));
                    data.setTaxcoadescp(request.getParameter("taxcoadescp"));
                    data.setTaxcode(request.getParameter("taxcode"));
                    data.setTaxrate(Double.parseDouble(request.getParameter("taxrate")));
                    data.setUnitm(request.getParameter("unitm"));
                    data.setUnitp(request.getParameter("unitp"));
                    data.setDispatchno(request.getParameter("dispatchno"));
                    SalesInvoiceDAO.updateItem(log, data, request.getParameter("refer"));
                    urlsend = "/ar_inv_edit_list.jsp?referno=" + request.getParameter("ivref");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("ivref"));
                    sys.setRemark("Sub Item Updated (" + request.getParameter("refer") + ")");
                    break;
                }
                case "edit_st":
                    urlsend = "/ar_inv_edit_item.jsp?referno=" + request.getParameter("referno");
                    break;
                case "delete_st":
                    SalesInvoiceDAO.deleteItem(log, request.getParameter("referno"), request.getParameter("referno").substring(0, 15));
                    urlsend = "/ar_inv_edit_list.jsp?referno=" + request.getParameter("referno").substring(0, 15);

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno").substring(0, 15));
                    sys.setRemark("Sub Item Deleted (" + request.getParameter("referno") + ")");
                    break;
                case "post":
                    //JournalVoucherDAO.deleteJournalItem(request.getParameter("referno"),request.getParameter("referno").substring(0,15));
                    urlsend = "/gl_post.jsp?referno=" + request.getParameter("referno") + "&vtype=SNV";
                    break;
                case "dist":
                    Distribute dist = (Distribute) DistributeDAO.distributeNow(request.getParameter("referno"), "SNV", log);
                    session.setAttribute("post_detail", dist);
                    if (dist.isSuccess()) {
                        urlsend = "/gl_post_status.jsp";
                    } else {
                        SalesInvoiceDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());
                        urlsend = "/gl_post_status.jsp";
                    }

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno"));
                    sys.setRemark("Approved & Posted");
                    break;
                case "delete":
                    SalesInvoiceDAO.deleteExist(log, request.getParameter("referno"));
                    urlsend = "/ar_inv_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno"));
                    sys.setRemark("Deleted");
                    break;
                case "check":
                    urlsend = "/check_voucher.jsp?referno=" + request.getParameter("referno") + "&vtype=SNV";
                    break;
                case "checkprocess":
                    SalesInvoiceDAO.checkVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname());
                    urlsend = "/ar_inv_edit_list.jsp?referno=" + request.getParameter("referno");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno"));
                    sys.setRemark("Invoice Checked");
                    break;
                case "approve":
                    SalesInvoiceDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());
                    urlsend = "/ar_inv_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");
                    break;
                case "tempmaster":

                    urlsend = "/ar_inv_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");
                    break;
                case "temprefinery":
                    SalesInvoiceDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());
                    urlsend = "/ar_inv_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");
                    break;
                case "saveinvoice":
                    String newrefer = SalesInvoiceDAO.generateInvoice(log, request.getParameter("sessionid"));
                    urlsend = "/ar_inv_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");

                    sys.setLog(true);
                    sys.setReferno(newrefer);
                    sys.setRemark("Invoice added");
                    break;
                case "viewprinted":
                    urlsend = "/print_voucher.jsp?refer=" + request.getParameter("refer");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("View Printed Format");
                    break;
                case "printffb":
                    urlsend = "/print_ffb.jsp?refer=" + request.getParameter("refer");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("View Printed Format");
                    break;
                case "adddispatch":
                    String[] disno = request.getParameterValues("dispatchno");
                    List<ArTempRefinery> atx = new ArrayList();

                    for (int i = 0; i < disno.length; i++) {

                        if (!disno[i].equals("")) {
                            Logger.getLogger(Path_ARInvoice.class.getName()).log(Level.INFO, "------" + disno[i]);

                            ArTempRefinery ar = new ArTempRefinery();

                            ar.setSessionid(request.getParameter("sessionid"));
                            ar.setContractno(request.getParameter("contractno"));
                            ar.setDate(request.getParameter("date" + disno[i]));
                            ar.setDispatchno(request.getParameter("dispatchno" + disno[i]));
                            ar.setMtMill(ParseSafely.parseDoubleSafely(request.getParameter("mt_mill" + disno[i])));
                            ar.setMtRefinery(ParseSafely.parseDoubleSafely(request.getParameter("mt_refinery" + disno[i])));
                            ar.setTrailer(request.getParameter("trailer" + disno[i]));

                            atx.add(ar);
                        }

                    }

                    SalesInvoiceDAO.saveFromAddDispatchNo(log, atx, request.getParameter("refer"), Double.parseDouble(request.getParameter("price")));
                    notify = "&status=success&text=Sales Invoice has been updated.";
                    urlsend = "/ar_inv_edit_list.jsp?referno=" + request.getParameter("refer")+notify;
                    break;
                    
                case "summaryinvoice":
                    urlsend = "/ar_inv_summary_invoice.jsp?refer=" + request.getParameter("refer");

                    //sys.setLog(true);
                    //sys.setReferno(request.getParameter("refer"));
                    //sys.setRemark("View Printed Format");
                    break;
                    
                case "printpreview":
                    urlsend = "/print_voucher_approve.jsp?refer=" + request.getParameter("refer");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("View Printed Format");
                    break;
                case "distformanager":
                    Distribute distx = (Distribute) DistributeDAO.distributeNow(request.getParameter("referno"), "SNV", log);
                    session.setAttribute("post_detail", distx);
                    if (distx.isSuccess()) {
                        urlsend = "/start_2.jsp";
                    } else {
                        SalesInvoiceDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());
                        urlsend = "/start_2.jsp";
                    }

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno"));
                    sys.setRemark("Approved & Posted");
                    break;
                case "createnote":
                    //String newrefer = "PVN991217040003";
                    Master post = (Master) PostDAO.getInfo(log, request.getParameter("refer"), "SNV");
                    DebitCreditNoteDAO.generateDebitCredit(post, log);
                    
                    notify = "&status=success&text=Credit Note created.";
                    urlsend = "/ar_inv_edit_list.jsp?referno=" + request.getParameter("refer")+notify;

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("Debit Note created for "+request.getParameter("refer")+".");
                    break;
                case "cancel":
                    //String newrefer = "PVN991217040003";
                    SalesInvoiceDAO.cancelThis(log, request.getParameter("refer"));
                    
                    notify = "&status=success&text=Sales Invoice canceled.";
                    urlsend = "/ar_inv_edit_list.jsp?moduleid="+moduleid+"&referno=" + request.getParameter("refer")+notify;

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("Sales Invoice canceled for "+request.getParameter("refer")+".");
                    break;
                case "viewsummary":
                    urlsend = "/ar_inv_summary_view.jsp?referno=" + request.getParameter("referno");

                    break;
                case "viewprinted2":
                    urlsend = "/ar_inv_summary_view_printed.jsp?refer=" + request.getParameter("refer") + "&status="+request.getParameter("status");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("View Printed Format");
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("100");//nullpointer
                    ErrorIO.setType("data");
                    urlsend = "/error.jsp";

                    sys.setLog(true);
                    sys.setProcess("error");
                    break;
            }

            SysLogDAO.saveLog(log, sys);

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_ARInvoice.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_ARInvoice.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
