/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.ap;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.financial.ap.VendorAssignDAO;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.model.financial.ap.VendorAssign;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_APAssign", urlPatterns = {"/Path_APAssign"})
public class Path_APAssign extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";

            VendorAssign data = new VendorAssign();
            data.setAccode(request.getParameter("accode"));
            data.setAcdesc(request.getParameter("acdesc"));
            data.setCompanycode(request.getParameter("companycode"));
            data.setCompanyname(request.getParameter("companyname"));
            data.setSuppaddress(request.getParameter("suppaddress"));
            data.setSuppcode(request.getParameter("suppcode"));
            data.setSuppname(request.getParameter("suppname"));
            data.setGstid(request.getParameter("gstid"));

            switch (process) {
                case "viewlist":
                    urlsend = "/ap_assign_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/ap_assign_add.jsp";
                    break;
                case "view":
                    urlsend = "/ap_assign_view.jsp";
                    break;
                case "edit":
                    urlsend = "/ap_assign_edit.jsp?refer=" + request.getParameter("referno");
                    break;
                case "save":
                    VendorAssignDAO.saveMain(log, data);
                    urlsend = "/ap_assign_list.jsp?id=" + moduleid;
                    break;
                case "update":
                    VendorAssignDAO.updateMain(log, data, request.getParameter("suppcode"));
                    urlsend = "/ap_assign_list.jsp?id=" + moduleid;
                    break;
                case "delete":
                    VendorAssignDAO.delete(log, request.getParameter("referno"));

                    urlsend = "/ap_assign_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");

                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("100");//nullpointer
                    ErrorIO.setType("data");
                    urlsend = "/error.jsp";
                    break;
            }

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_APAssign.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_APAssign.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
