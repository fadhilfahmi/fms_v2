/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.gl;

import com.lcsb.fms.controller.PathController;
import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.financial.ap.VendorPaymentDAO;
import com.lcsb.fms.dao.financial.gl.CreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.DistributeDAO;
import com.lcsb.fms.financial.gl.post.Distribute;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.sys.log.SysLog;
import com.lcsb.fms.sys.log.SysLogDAO;
import com.lcsb.fms.util.dao.ModuleDAO;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_CreditNote", urlPatterns = {"/Path_CreditNote"})
public class Path_CreditNote extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            SysLog sys = new SysLog();

            sys.setModuleid(moduleid);
            sys.setProcess(process);
            sys.setUserid(log.getUserID());
            sys.setDate(AccountingPeriod.getCurrentTimeStamp());
            sys.setTime(AccountingPeriod.getCurrentTime());
            sys.setLog(false);
            sys.setIp(request.getRemoteAddr());

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/gl_creditnote_list.jsp?id=" + moduleid;
                    break;
                case "view":
                    urlsend = "/gl_creditnote_view.jsp?refer=" + request.getParameter("referno");
                    break;
                case "viewfromother":
                    urlsend = "/gl_creditnote_view.jsp?refer=" + request.getParameter("referno") +"&referother="+ request.getParameter("referother") +"&topath="+ request.getParameter("topath") +"&moduleid="+ request.getParameter("frommodule");
                    break;
                case "viewpv":
                    urlsend = "/cb_pv_view_voucher.jsp?refer=" + request.getParameter("refer");
                    break;
                case "viewprinted":
                    urlsend = "/print_voucher.jsp?refer=" + request.getParameter("refer");
                    break;
                case "dist":
                    Distribute dist = (Distribute) DistributeDAO.distributeNow(request.getParameter("referno"), "CN", log);
                    session.setAttribute("post_detail", dist);

                    Logger.getLogger(PathController.class.getName()).log(Level.INFO, "------------{0}", String.valueOf(dist.isSuccess()));
                    if (dist.isSuccess()) {

                        urlsend = "/gl_post_status.jsp";
                    } else {
                        CreditNoteDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());
                        urlsend = "/gl_post_status.jsp";
                        
                        sys.setLog(true);
                        sys.setReferno(request.getParameter("referno"));
                        sys.setRemark("Approved & Posted");
                    }

                    break;
                case "delete":
                    CreditNoteDAO.deleteExist(log, request.getParameter("referno"));
                    urlsend = "/gl_creditnote_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");
                    
                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno"));
                    sys.setRemark("Deleted.");
                    
                    break;
                case "check":
                    urlsend = "/check_voucher.jsp?referno=" + request.getParameter("referno") + "&vtype=PV";
                    break;
                case "checkprocess":
                    VendorPaymentDAO.checkVoucher(log, request.getParameter("referno"), log.getUserID(), log.getUserName());
                    urlsend = "/ap_pv_edit_list.jsp?refer=" + request.getParameter("referno");
                    
                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno"));
                    sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " Checked.");
                    
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("100");//nullpointer
                    ErrorIO.setType("data");
                    urlsend = "/error.jsp";
                    break;
            }
            
            SysLogDAO.saveLog(log, sys);

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_CreditNote.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_CreditNote.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
