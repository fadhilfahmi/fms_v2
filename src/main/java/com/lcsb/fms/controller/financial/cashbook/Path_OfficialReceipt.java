/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.cashbook;

import com.lcsb.fms.controller.PathController;
import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO;
import com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.DistributeDAO;
import com.lcsb.fms.dao.financial.gl.PostDAO;
import com.lcsb.fms.financial.gl.post.Distribute;
import com.lcsb.fms.financial.gl.post.Master;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.cashbook.CbOfficialCek;
import com.lcsb.fms.model.financial.cashbook.OfficialCreditItem;
import com.lcsb.fms.model.financial.cashbook.OfficialReceipt;
import com.lcsb.fms.sys.log.SysLog;
import com.lcsb.fms.sys.log.SysLogDAO;
import com.lcsb.fms.util.dao.ModuleDAO;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_OfficialReceipt", urlPatterns = {"/Path_OfficialReceipt"})
public class Path_OfficialReceipt extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            SysLog sys = new SysLog();

            sys.setModuleid(moduleid);
            sys.setProcess(process);
            sys.setUserid(log.getUserID());
            sys.setDate(AccountingPeriod.getCurrentTimeStamp());
            sys.setTime(AccountingPeriod.getCurrentTime());
            sys.setLog(false);
            sys.setIp(request.getRemoteAddr());

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/cb_official_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/cb_official_add.jsp";
                    break;
                case "addinvoice":
                    urlsend = "/cb_official_add_invoice.jsp?refer=" + request.getParameter("refer");
                    break;
                case "addfrominvoice":
                    urlsend = "/cb_official_step.jsp?sessionid=" + session.getId() + AccountingPeriod.getCurrentTimeStamp() + AccountingPeriod.getCurrentTime();
                    break;
                case "addinv": {
                    OfficialReceipt data = new OfficialReceipt();
                    data.setAmount(Double.parseDouble(request.getParameter("amountno")));
                    data.setBankcode(request.getParameter("bankcode"));
                    data.setBankname(request.getParameter("bankname"));
                    data.setDate(request.getParameter("date"));
                    data.setEstatecode(request.getParameter("estatecode"));
                    data.setEstatename(request.getParameter("estatename"));
                    data.setGstid(request.getParameter("gstid"));
                    data.setPaidaddress(request.getParameter("paidaddress"));
                    data.setPaidcity(request.getParameter("paidcity"));
                    data.setPaidcode(request.getParameter("paidcode"));
                    data.setPaidname(request.getParameter("paidname"));
                    data.setPaidpostcode(request.getParameter("paidpostcode"));
                    data.setPaidstate(request.getParameter("paidstate"));
                    data.setPaidtype(request.getParameter("paidtype"));
                    data.setPeriod(Integer.parseInt(request.getParameter("period")));
                    data.setRacoacode(request.getParameter("racoacode"));
                    data.setRacoadesc(request.getParameter("racoadesc"));
                    data.setReceivemode(request.getParameter("receivemode"));
                    data.setRemarks(request.getParameter("remarks"));
                    data.setRm(request.getParameter("amountstr"));
                    data.setYear(Integer.parseInt(request.getParameter("year")));
                    data.setInvno(request.getParameter("invno"));
                    data.setPreid(log.getUserID());
                    data.setPrename(log.getFullname());
                    data.setPreparedate(AccountingPeriod.getCurrentTimeStamp());
                    data.setPreposition(log.getPosition());
                    data.setPaymentmode(request.getParameter("paymentmode"));
                    data.setChequeno(request.getParameter("chequeno"));

                    String newrefer = OfficialReceiptDAO.saveMain(log, data);
                    urlsend = "/cb_official_edit_list.jsp?refer=" + newrefer;
                    
                    sys.setLog(true);
                    sys.setReferno(newrefer);
                    sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " added");
                    break;
                }
                case "addcredit":
                    //addnewitem
                    urlsend = "/cb_official_add_credit.jsp?refer=" + request.getParameter("refer");
                    break;
                case "addcheque":
                    //addnewitem
                    urlsend = "/cb_official_add_cheque.jsp?refer=" + request.getParameter("refer");
                    break;
                case "addcreditprocess":
                    try {
                        Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------Add Official Receipt Credit---");
                        OfficialCreditItem data = new OfficialCreditItem();

                        data.setAmount(String.valueOf(GeneralTerm.amountFormattoSave(request.getParameter("amount"))));
                        data.setCoacode(request.getParameter("coacode"));
                        data.setCoadescp(request.getParameter("coadescp"));
                        data.setLoccode(request.getParameter("loccode"));
                        data.setLoclevel(request.getParameter("loclevel"));
                        data.setLocname(request.getParameter("locname"));
                        data.setRefer(request.getParameter("refer"));
                        data.setRemarks(request.getParameter("remarks"));
                        data.setSacode(request.getParameter("sacode"));
                        data.setSadesc(request.getParameter("sadesc"));
                        data.setSatype(request.getParameter("satype"));
                        data.setTaxamt(Double.parseDouble(request.getParameter("taxamt")));
                        data.setTaxcode(request.getParameter("taxcode"));
                        data.setTaxdescp(request.getParameter("taxdescp"));
                        data.setTaxrate(Double.parseDouble(request.getParameter("taxrate")));
                        data.setVoucherno(request.getParameter("voucherno"));
                        data.setTaxcoacode(request.getParameter("taxcoacode"));
                        data.setTaxcoadescp(request.getParameter("taxcoadescp"));

                        OfficialReceiptDAO.saveItem(log, data);

                        urlsend = "/cb_official_edit_list.jsp?refer=" + request.getParameter("refer");
                        
                        sys.setLog(true);
                        sys.setReferno(request.getParameter("refer"));
                        sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " Credit Account added.");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "savecheque":
                    try {
                        Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------dfgdfgdfgd---");
                        CbOfficialCek data = new CbOfficialCek();

                        data.setAmount(Double.parseDouble(request.getParameter("amount")));
                        data.setBank(request.getParameter("bank"));
                        data.setBranch(request.getParameter("branch"));
                        data.setCekno(request.getParameter("cekno"));
                        data.setDate(request.getParameter("date"));
                        data.setVoucherno(request.getParameter("voucherno"));

                        OfficialReceiptDAO.saveCheque(log, data);
                        urlsend = "/cb_official_edit_list.jsp?refer=" + request.getParameter("voucherno");
                        
                        sys.setLog(true);
                        sys.setReferno(request.getParameter("voucherno"));
                        sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " Cheque added.");
                        
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "updatecheque":
                    try {
                        Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------dfgdfgdfgd---");
                        CbOfficialCek data = new CbOfficialCek();

                        data.setAmount(Double.parseDouble(request.getParameter("amount")));
                        data.setBank(request.getParameter("bank"));
                        data.setBranch(request.getParameter("branch"));
                        data.setCekno(request.getParameter("cekno"));
                        data.setDate(request.getParameter("date"));
                        data.setVoucherno(request.getParameter("voucherno"));
                        data.setRefer(request.getParameter("refer"));

                        OfficialReceiptDAO.updateCheque(log, data, request.getParameter("refer"));
                        urlsend = "/cb_official_edit_list.jsp?refer=" + request.getParameter("voucherno");
                        
                        sys.setLog(true);
                        sys.setReferno(request.getParameter("voucherno"));
                        sys.setRemark("Cheque updated (" + request.getParameter("refer") + ").");
                        
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "editlist":
                    urlsend = "/cb_official_edit_list.jsp?refer=" + request.getParameter("referno");
                    break;
                case "viewprinted":
                    urlsend = "/print_voucher.jsp?refer=" + request.getParameter("refer");
                    break;
                case "editmain":
                    urlsend = "/cb_official_edit_main.jsp?referno=" + request.getParameter("referno");
                    break;
                case "editmainprocess": {
                    OfficialReceipt data = new OfficialReceipt();
                    
                    data.setAmount(Double.parseDouble(request.getParameter("amountno")));
                    data.setBankcode(request.getParameter("bankcode"));
                    data.setBankname(request.getParameter("bankname"));
                    data.setDate(request.getParameter("date"));
                    data.setEstatecode(request.getParameter("estatecode"));
                    data.setEstatename(request.getParameter("estatename"));
                    data.setGstid(request.getParameter("gstid"));
                    data.setPaidaddress(request.getParameter("paidaddress"));
                    data.setPaidcity(request.getParameter("paidcity"));
                    data.setPaidcode(request.getParameter("paidcode"));
                    data.setPaidname(request.getParameter("paidname"));
                    data.setPaidpostcode(request.getParameter("paidpostcode"));
                    data.setPaidstate(request.getParameter("paidstate"));
                    data.setPaidtype(request.getParameter("paidtype"));
                    data.setPeriod(Integer.parseInt(request.getParameter("period")));
                    data.setRacoacode(request.getParameter("racoacode"));
                    data.setRacoadesc(request.getParameter("racoadesc"));
                    data.setReceivemode(request.getParameter("receivemode"));
                    data.setRemarks(request.getParameter("remarks"));
                    data.setRm(request.getParameter("amountstr"));
                    data.setYear(Integer.parseInt(request.getParameter("year")));
                    data.setInvno(request.getParameter("invno"));
                    data.setPaymentmode(request.getParameter("paymentmode"));
                    data.setChequeno(request.getParameter("chequeno"));
                    data.setRefer(request.getParameter("refer"));
                    data.setVoucherno(Integer.parseInt(request.getParameter("voucherno")));
                    
                    OfficialReceiptDAO.updateMain(log, data, request.getParameter("refer"));
                    urlsend = "/cb_official_edit_list.jsp?referno=" + request.getParameter("refer");
                    
                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " updated.");
                    
                    break;
                }
                case "edititemprocess": {
                    OfficialCreditItem data = new OfficialCreditItem();
                    data.setVoucherno(request.getParameter("voucherno"));
                    data.setAmount(String.valueOf(GeneralTerm.amountFormattoSave(request.getParameter("amount"))));
                    data.setCoacode(request.getParameter("coacode"));
                    data.setCoadescp(request.getParameter("coadescp"));
                    data.setLoccode(request.getParameter("loccode"));
                    data.setLoclevel(request.getParameter("loclevel"));
                    data.setLocname(request.getParameter("locname"));
                    data.setRefer(request.getParameter("refer"));
                    data.setRemarks(request.getParameter("remarks"));
                    data.setSacode(request.getParameter("sacode"));
                    data.setSadesc(request.getParameter("sadesc"));
                    data.setSatype(request.getParameter("satype"));
                    data.setTaxamt(Double.parseDouble(request.getParameter("taxamt")));
                    data.setTaxcode(request.getParameter("taxcode"));
                    data.setTaxdescp(request.getParameter("taxdescp"));
                    data.setTaxrate(Double.parseDouble(request.getParameter("taxrate")));
                    OfficialReceiptDAO.updateItem(log, data, request.getParameter("refer"));
                    urlsend = "/cb_official_edit_list.jsp?refer=" + request.getParameter("refer").substring(0, 15);
                    
                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer").substring(0, 15));
                    sys.setRemark("Credit account updated (" + request.getParameter("refer") + ").");
                    
                    break;
                }
                case "edit_st":
                    urlsend = "/cb_official_edit_item.jsp?referno=" + request.getParameter("referno");
                    break;
                case "edit_nd":
                    urlsend = "/cb_official_edit_cheque.jsp?referno=" + request.getParameter("referno");
                    break;
                case "post":
                    //JournalVoucherDAO.deleteJournalItem(request.getParameter("referno"),request.getParameter("referno").substring(0,15));
                    urlsend = "/gl_post.jsp?referno=" + request.getParameter("referno") + "&vtype=OR";
                    break;
                case "dist":
                    Distribute dist = (Distribute) DistributeDAO.distributeNow(request.getParameter("referno"), "OR", log);
                    session.setAttribute("post_detail", dist);

                    Logger.getLogger(PathController.class.getName()).log(Level.INFO, "------------" + String.valueOf(dist.isSuccess()));
                    if (dist.isSuccess()) {

                        urlsend = "/gl_post_status.jsp";
                    } else {
                        OfficialReceiptDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());
                        urlsend = "/gl_post_status.jsp";
                        
                        sys.setLog(true);
                        sys.setReferno(request.getParameter("referno"));
                        sys.setRemark("Approved & Posted");
                    }

                    break;
                case "delete":
                    OfficialReceiptDAO.deleteExist(log, request.getParameter("referno"));
                    urlsend = "/cb_official_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");
                    
                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno"));
                    sys.setRemark("Deleted.");
                    break;
                case "delete_st":
                    //Logger.getLogger(PathController.class.getName()).log(Level.INFO, request.getParameter("referno").substring(0,15));
                    OfficialReceiptDAO.deleteItem(log, request.getParameter("referno"), request.getParameter("referno").substring(0, 15));
                    urlsend = "/cb_official_edit_list.jsp?refer=" + request.getParameter("referno").substring(0, 15);
                    
                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno").substring(0, 15));
                    sys.setRemark("Credit Account deleted (" + request.getParameter("referno") + ").");
                    break;
                case "delete_nd":
                    //Logger.getLogger(PathController.class.getName()).log(Level.INFO, request.getParameter("referno").substring(0,15));
                    OfficialReceiptDAO.deleteCheque(log, request.getParameter("referno"), request.getParameter("referno").substring(0, 15));
                    urlsend = "/cb_official_edit_list.jsp?refer=" + request.getParameter("referno").substring(0, 15);
                    
                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno").substring(0, 15));
                    sys.setRemark("Cheque deleted (" + request.getParameter("referno") + ").");
                    break;
                case "check":
                    urlsend = "/check_voucher.jsp?referno=" + request.getParameter("referno") + "&vtype=OR";
                    break;
                case "checkprocess":
                    OfficialReceiptDAO.checkVoucher(log, request.getParameter("referno"), log.getUserID(), log.getUserName());
                    urlsend = "/cb_official_edit_list.jsp?refer=" + request.getParameter("referno");
                    
                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno"));
                    sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " Checked.");
                    break;
                case "savevoucher":
                    String refer = OfficialReceiptDAO.generateReceipt(request.getParameter("sessionid"), log);
                    notify = "&status=success&text=Official Receipt has been added.";
                    urlsend = "/cb_official_edit_list.jsp?refer=" + refer + notify;
                    
                    sys.setLog(true);
                    sys.setReferno(refer);
                    sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " added from Invoice.");
                    break;
                case "adddebitnote":
                    int success = OfficialReceiptDAO.generateFromDebitNote(log, request.getParameter("referno"), request.getParameter("noteno"));
                    notify = "&status=success&text=Official Receipt has been updated.";
                    urlsend = "/cb_official_edit_list.jsp?refer=" + request.getParameter("referno") + notify;
                    
                    sys.setLog(true);
                    sys.setReferno(request.getParameter("referno"));
                    sys.setRemark("From Debit Note added ("+request.getParameter("noteno")+").");
                    break;
                case "replicate":
                    //String newrefer = "PVN991217040003";
                    String newrefer = OfficialReceiptDAO.replicateData(log, request.getParameter("refer"), request.getParameter("replicatedate"), request.getParameter("replicateyear"), request.getParameter("replicateperiod"));
                    notify = "&status=success&text=Official Receipt has been replicated.";
                    urlsend = "/cb_official_edit_list.jsp?moduleid="+moduleid+"&refer=" + newrefer+notify;

                    sys.setLog(true);
                    sys.setReferno(newrefer);
                    sys.setRemark("Official Receipt replicated from "+request.getParameter("refer")+".");
                    break;
                case "createnote":
                    //String newrefer = "PVN991217040003";
                    Master post = (Master) PostDAO.getInfo(log, request.getParameter("refer"), "OR");
                    DebitCreditNoteDAO.generateDebitCredit(post, log);
                    
                    notify = "&status=success&text=Credit Note created.";
                    urlsend = "/cb_official_edit_list.jsp?moduleid="+moduleid+"&refer=" + request.getParameter("refer")+notify;

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("Debit Note created for "+request.getParameter("refer")+".");
                    break;
                case "cancel":
                    //String newrefer = "PVN991217040003";
                    OfficialReceiptDAO.cancelThis(log, request.getParameter("refer"));
                    
                    notify = "&status=success&text=Official Receipt canceled.";
                    urlsend = "/cb_official_edit_list.jsp?moduleid="+moduleid+"&refer=" + request.getParameter("refer")+notify;

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("Official Receipt canceled for "+request.getParameter("refer")+".");
                    break;
                case "viewsummary":
                    urlsend = "/cb_official_summary_view.jsp?refer=" + request.getParameter("referno");

                    break;
                case "viewprinted2":
                    urlsend = "/ar_inv_summary_view_printed.jsp?refer=" + request.getParameter("refer") + "&status="+request.getParameter("status");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("View Printed Format");
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("100");//nullpointer
                    ErrorIO.setType("data");
                    urlsend = "/error.jsp";
                    break;
            }
            
            SysLogDAO.saveLog(log, sys);

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_OfficialReceipt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_OfficialReceipt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
