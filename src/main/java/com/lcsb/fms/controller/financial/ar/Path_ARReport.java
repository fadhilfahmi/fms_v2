/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.ar;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.model.financial.ar.ArGLParam;
import com.lcsb.fms.model.financial.ar.ArReportAgingParam;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_ARReport", urlPatterns = {"/Path_ARReport"})
public class Path_ARReport extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/ar_report_list.jsp?id=" + moduleid;
                    break;
                case "statement":
                    urlsend = "/ar_rpt_statement.jsp?id=" + moduleid;
                    break;
                case "aging":
                    urlsend = "/ar_rpt_aging.jsp?id=" + moduleid;
                    break;
                case "summaryinvoice":
                    urlsend = "/ar_rpt_invsummary.jsp?id=" + moduleid;
                    break;
                case "viewstatement":
                    ArGLParam b = new ArGLParam();
                    //b.setStartDate(request.getParameter("range").substring(0, 10));
                    //b.setEndDate(request.getParameter("range").substring(13, 23));
                    b.setStartDate(request.getParameter("startdate"));
                    b.setEndDate(request.getParameter("enddate"));
                    b.setBuyercode(request.getParameter("buyercode"));
                    b.setBuyername(request.getParameter("buyername"));
                    b.setFperiod(request.getParameter("range"));
                    b.setFyear(request.getParameter("fyear"));
                    b.setTperiod(request.getParameter("tperiod"));
                    b.setTyear(request.getParameter("tyear"));
                    b.setModuleid(moduleid);
                    session.setAttribute("gl_posting_param", b);

                    urlsend = "/ar_rpt_statement_view.jsp?id=" + moduleid;
                    break;
                case "viewaging":
                    ArReportAgingParam pr = new ArReportAgingParam();
                    pr.setBuyerCode(request.getParameter("bcode"));
                    pr.setBuyerName(request.getParameter("bname"));
                    pr.setDate(request.getParameter("todate"));
                    pr.setType(request.getParameter("viewby"));
                    session.setAttribute("ar_aging_param", pr);

                    urlsend = "/ar_print_report_aging.jsp?id=" + moduleid;
                    break;
                case "printreport":
                    urlsend = "/ar_print_report.jsp?id=" + moduleid;
                    break;
                case "gotopage":
                    urlsend = "/"+request.getParameter("origin")+".jsp?id=" + moduleid;
                    break;
                default:
                    break;
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
