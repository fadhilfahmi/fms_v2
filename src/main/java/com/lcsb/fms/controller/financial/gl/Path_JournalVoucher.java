/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.gl;

import com.lcsb.fms.controller.PathController;
import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO;
import com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO;
import com.lcsb.fms.dao.financial.gl.DistributeDAO;
import com.lcsb.fms.dao.financial.gl.JournalVoucherDAO;
import com.lcsb.fms.dao.financial.gl.PostDAO;
import com.lcsb.fms.financial.gl.post.Distribute;
import com.lcsb.fms.financial.gl.post.Master;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.general.GeneralTerm;
import com.lcsb.fms.model.financial.gl.JournalVoucher;
import com.lcsb.fms.model.financial.gl.JournalVoucherItem;
import com.lcsb.fms.sys.log.SysLog;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_JournalVoucher", urlPatterns = {"/Path_JournalVoucher"})
public class Path_JournalVoucher extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();
            
            SysLog sys = new SysLog();

            sys.setModuleid(moduleid);
            sys.setProcess(process);
            sys.setUserid(log.getUserID());
            sys.setDate(AccountingPeriod.getCurrentTimeStamp());
            sys.setTime(AccountingPeriod.getCurrentTime());
            sys.setLog(false);
            sys.setIp(request.getRemoteAddr());


            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/gl_jv_list.jsp?id=" + moduleid;
                    break;
                case "addjv":
                    JournalVoucher dataJV = new JournalVoucher();
                    dataJV.setJVno(request.getParameter("JVno"));
                    dataJV.setJVdate(request.getParameter("JVdate"));
                    dataJV.setReflexcb(request.getParameter("reflexcb"));
                    dataJV.setReason(request.getParameter("reason"));
                    dataJV.setYear(request.getParameter("year"));
                    dataJV.setCurperiod(request.getParameter("curperiod"));
                    dataJV.setJVrefno(request.getParameter("JVrefno"));
                    dataJV.setEstatecode(request.getParameter("estatecode"));
                    dataJV.setEstatename(request.getParameter("estatename"));
                    dataJV.setPreparedbyid(request.getParameter("preparedbyid"));
                    dataJV.setPreparedbyname(request.getParameter("preparedbyname"));
                    dataJV.setPreparedate(request.getParameter("preparedate"));
                    dataJV.setTodebit(request.getParameter("todebit"));
                    dataJV.setTocredit(request.getParameter("tocredit"));
                    JournalVoucherDAO.saveJournalMain(log, dataJV);
                    urlsend = "/gl_jv_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");
                    break;
                case "updatejv":
                    JournalVoucher data = new JournalVoucher();
                    data.setJVno(request.getParameter("JVno"));
                    data.setJVdate(request.getParameter("JVdate"));
                    data.setReflexcb(request.getParameter("reflexcb"));
                    data.setReason(request.getParameter("reason"));
                    data.setYear(request.getParameter("year"));
                    data.setCurperiod(request.getParameter("curperiod"));
                    data.setJVrefno(request.getParameter("JVrefno"));
                    JournalVoucherDAO.updateMain(log, data);
                    urlsend = "/gl_jv_edit_list.jsp?refer=" + request.getParameter("JVrefno");
                    break;
                case "addaccount":
                    urlsend = "/gl_jv_add_item.jsp?referno=" + request.getParameter("refer");
                    break;
                case "editmain":
                    urlsend = "/gl_jv_edit_main.jsp?referno=" + request.getParameter("referno");
                    break;
                case "addnew":
                    urlsend = "/gl_jv_add_new.jsp";
                    break;
                case "edit_st":
                    urlsend = "/gl_jv_edit_item.jsp?referno=" + request.getParameter("referno");
                    break;
                case "viewprinted":
                    urlsend = "/print_voucher.jsp?refer=" + request.getParameter("refer");
                    break;
                case "editlist":
                    urlsend = "/gl_jv_edit_list.jsp?refer=" + request.getParameter("referno");
                    break;
                case "delete":
                    JournalVoucherDAO.deleteExistJournal(log, request.getParameter("referno"));
                    urlsend = "/gl_jv_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");
                    break;
                case "delete_st":
                    Logger.getLogger(PathController.class.getName()).log(Level.INFO, request.getParameter("referno").substring(0, 15));
                    JournalVoucherDAO.deleteJournalItem(log, request.getParameter("referno"), request.getParameter("referno").substring(0, 15));
                    urlsend = "/gl_jv_edit_list.jsp?refer=" + request.getParameter("referno").substring(0, 15);
                    break;
                case "addjvitem":
                    try {
                        Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------dfgdfgdfgd---");
                        JournalVoucherItem dataJVs = new JournalVoucherItem();

                        dataJVs.setActcode(request.getParameter("actcode"));
                        dataJVs.setActdesc(request.getParameter("actdesc"));
                        dataJVs.setLoclevel(request.getParameter("loclevel"));
                        dataJVs.setLoccode(request.getParameter("loccode"));
                        dataJVs.setLocdesc(request.getParameter("locdesc"));
                        dataJVs.setRemark(request.getParameter("remark"));
                        dataJVs.setDebit(GeneralTerm.amountFormattoSave(request.getParameter("debit")));
                        dataJVs.setCredit(GeneralTerm.amountFormattoSave(request.getParameter("credit")));
                        dataJVs.setJVrefno(request.getParameter("JVrefno"));
                        dataJVs.setSatype(request.getParameter("satype"));
                        dataJVs.setSacode(request.getParameter("sacode"));
                        dataJVs.setSadesc(request.getParameter("sadesc"));
                        dataJVs.setTaxcode(request.getParameter("taxcode"));
                        dataJVs.setTaxdescp(request.getParameter("taxdescp"));
                        dataJVs.setTaxrate(request.getParameter("taxrate"));
                        dataJVs.setAmtbeforetax(Double.parseDouble(request.getParameter("amtbeforetax")));
                        dataJVs.setGstid(request.getParameter("gstid"));
                        Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------aaaaaaaaaaaaaaaaaa---");
                        JournalVoucherDAO.saveJournalItem(log, dataJVs);

                        urlsend = "/gl_jv_edit_list.jsp?refer=" + request.getParameter("JVrefno");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "updatejvitem":
                    try {
                        Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------dfgdfgdfgd---");
                        JournalVoucherItem dataJVi = new JournalVoucherItem();

                        dataJVi.setActcode(request.getParameter("actcode"));
                        dataJVi.setActdesc(request.getParameter("actdesc"));
                        dataJVi.setLoclevel(request.getParameter("loclevel"));
                        dataJVi.setLoccode(request.getParameter("loccode"));
                        dataJVi.setLocdesc(request.getParameter("locdesc"));
                        dataJVi.setRemark(request.getParameter("remark"));
                        dataJVi.setDebit(GeneralTerm.amountFormattoSave(request.getParameter("debit")));
                        dataJVi.setCredit(GeneralTerm.amountFormattoSave(request.getParameter("credit")));
                        dataJVi.setJVrefno(request.getParameter("JVrefno"));
                        dataJVi.setSatype(request.getParameter("satype"));
                        dataJVi.setSacode(request.getParameter("sacode"));
                        dataJVi.setSadesc(request.getParameter("sadesc"));
                        dataJVi.setTaxcode(request.getParameter("taxcode"));
                        dataJVi.setTaxdescp(request.getParameter("taxdescp"));
                        dataJVi.setTaxrate(request.getParameter("taxrate"));
                        dataJVi.setAmtbeforetax(Double.parseDouble(request.getParameter("amtbeforetax")));
                        dataJVi.setGstid(request.getParameter("gstid"));
                        dataJVi.setJvid(request.getParameter("Jvid"));
                        Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------aaaaaaaaaaaaaaaaaa---");
                        JournalVoucherDAO.updateJournalItem(log, dataJVi, request.getParameter("JVid"));
                        urlsend = "/gl_jv_edit_list.jsp?refer=" + request.getParameter("JVrefno");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "postjv":
                    //JournalVoucherDAO.deleteJournalItem(request.getParameter("referno"),request.getParameter("referno").substring(0,15));
                    urlsend = "/gl_post.jsp?referno=" + request.getParameter("referno") + "&vtype=JV";
                    break;
                case "dist":

                    Distribute dist = (Distribute) DistributeDAO.distributeNow(request.getParameter("referno"), "JV", log);
                    session.setAttribute("post_detail", dist);

                    Logger.getLogger(PathController.class.getName()).log(Level.INFO, "------------" + String.valueOf(dist.isSuccess()));
                    if (dist.isSuccess()) {

                        urlsend = "/gl_post_status.jsp";
                    } else {
                        JournalVoucherDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());
                        urlsend = "/gl_post_status.jsp";
                    }
                    break;
                case "check":
                    urlsend = "/check_voucher.jsp?referno=" + request.getParameter("referno") + "&vtype=JV";
                    break;
                case "checkprocess":
                    JournalVoucherDAO.checkVoucher(log, request.getParameter("referno"), log.getUserID(), log.getUserName());
                    urlsend = "/gl_jv_edit_list.jsp?refer=" + request.getParameter("referno");
                    break;
                case "approve":
                    JournalVoucherDAO.approveVoucher(log, request.getParameter("referno"), log.getUserID(), log.getFullname(), log.getPosition());
                    urlsend = "/gl_jv_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");
                    break;
                case "replicate":
                    //String newrefer = "PVN991217040003";
                    String newrefer = JournalVoucherDAO.replicateData(log, request.getParameter("refer"), request.getParameter("replicatedate"), request.getParameter("replicateyear"), request.getParameter("replicateperiod"));
                    notify = "&status=success&text=Journal Voucher has been replicated.";
                    urlsend = "/gl_jv_edit_list.jsp?moduleid="+moduleid+"&refer=" + newrefer+notify;

                    sys.setLog(true);
                    sys.setReferno(newrefer);
                    sys.setRemark("Journal Voucher replicated from "+request.getParameter("refer")+".");
                    break;
                case "createnote":
                    //String newrefer = "PVN991217040003";
                    Master post = (Master) PostDAO.getInfo(log, request.getParameter("refer"), "JV");
                    DebitCreditNoteDAO.generateDebitCredit(post, log);
                    
                    notify = "&status=success&text=Debit/Credit Note created.";
                    urlsend = "/gl_jv_edit_list.jsp?moduleid="+moduleid+"&refer=" + request.getParameter("refer")+notify;

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("Debit/Credit Note created for "+request.getParameter("refer")+".");
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("100");//nullpointer
                    ErrorIO.setType("data");
                    urlsend = "/error.jsp";
                    break;
            }

            Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_JournalVoucher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_JournalVoucher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
