/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.financial.employee;

import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.financial.employee.EmPayrollDAO;
import com.lcsb.fms.dao.financial.employee.EmROCDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.model.financial.employee.EmPayroll;
import com.lcsb.fms.model.financial.employee.EmRoc;
import com.lcsb.fms.sys.log.SysLog;
import com.lcsb.fms.sys.log.SysLogDAO;
import com.lcsb.fms.util.dao.ModuleDAO;
import com.lcsb.fms.util.ext.ParseSafely;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_ROC", urlPatterns = {"/Path_ROC"})
public class Path_ROC extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            SysLog sys = new SysLog();

            sys.setModuleid(moduleid);
            sys.setProcess(process);
            sys.setUserid(log.getUserID());
            sys.setDate(AccountingPeriod.getCurrentTimeStamp());
            sys.setTime(AccountingPeriod.getCurrentTime());
            sys.setLog(false);
            sys.setIp(request.getRemoteAddr());

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/em_roc_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/em_roc_add.jsp?sessionid=" + session.getId() + AccountingPeriod.getCurrentTimeStamp() + AccountingPeriod.getCurrentTime();
                    break;
                case "addroc": {
                    EmRoc data = new EmRoc();
                    data.setAmount(ParseSafely.parseDoubleSafely(request.getParameter("amount")));
                    data.setDateeffective(request.getParameter("dateeffective"));
                    data.setFileno(request.getParameter("fileno"));
                    data.setRchange(request.getParameter("rchange"));
                    data.setRefer(request.getParameter("refer"));
                    data.setRemark(request.getParameter("remark"));
                    data.setStaffdesignation(request.getParameter("staffdesignation"));
                    data.setStaffid(request.getParameter("staffid"));
                    data.setStaffname(request.getParameter("staffname"));

                    String newrefer = EmROCDAO.saveMain(log, data);
                    urlsend = "/em_roc_list.jsp?refer=" + newrefer;
                    
                    sys.setLog(true);
                    sys.setReferno(newrefer);
                    sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " added");
                    break;
                }
                case "updateroc": {
                    EmRoc data = new EmRoc();
                    data.setAmount(ParseSafely.parseDoubleSafely(request.getParameter("amount")));
                    data.setDateeffective(request.getParameter("dateeffective"));
                    data.setFileno(request.getParameter("fileno"));
                    data.setRchange(request.getParameter("rchange"));
                    data.setRefer(request.getParameter("refer"));
                    data.setRemark(request.getParameter("remark"));
                    data.setStaffdesignation(request.getParameter("staffdesignation"));
                    data.setStaffid(request.getParameter("staffid"));
                    data.setStaffname(request.getParameter("staffname"));

                    EmROCDAO.updateMain(log, data);
                    urlsend = "/em_roc_list.jsp";
                    
                    sys.setLog(true);
                    sys.setReferno(data.getRefer());
                    sys.setRemark(ModuleDAO.getModule(log, moduleid).getModuleDesc() + " updated");
                    break;
                }
                case "edit":
                    urlsend = "/em_roc_edit.jsp?refer=" + request.getParameter("refer");
                    break;
                case "delete":
                    EmROCDAO.deleteROC(log, request.getParameter("refer"));
                    urlsend = "/em_roc_list.jsp?id=" + moduleid;//+"&referenceno="+request.getParameter("referenceno");

                    sys.setLog(true);
                    sys.setReferno(request.getParameter("refer"));
                    sys.setRemark("Deleted");
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("100");//nullpointer
                    ErrorIO.setType("data");
                    urlsend = "/error.jsp";

                    sys.setLog(true);
                    sys.setProcess("error");
                    break;
            }

            SysLogDAO.saveLog(log, sys);

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_ROC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_ROC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
