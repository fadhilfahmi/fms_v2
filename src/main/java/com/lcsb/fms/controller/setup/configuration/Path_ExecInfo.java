/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.setup.configuration;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.dao.setup.configuration.ExecDAO;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.model.setup.configuration.Executive;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_ExecInfo", urlPatterns = {"/Path_ExecInfo"})
public class Path_ExecInfo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";
            
            switch (process) {
                case "viewlist":
                    urlsend = "/cf_exec_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/cf_exec_add.jsp?id=" + moduleid;
                    break;
                case "addprocess":
                    {
                        Executive vn = new Executive();
                        vn.setAddress(request.getParameter("address"));
                        vn.setBirth(request.getParameter("birth"));
                        vn.setBlood(request.getParameter("blood"));
                        vn.setCitizen(request.getParameter("citizen"));
                        vn.setCity(request.getParameter("city"));
                        vn.setEmail(request.getParameter("email"));
                        vn.setFax(request.getParameter("fax"));
                        vn.setHp(request.getParameter("hp"));
                        vn.setId(request.getParameter("id"));
                        vn.setMarital(request.getParameter("marital"));
                        vn.setName(request.getParameter("name"));
                        vn.setNewic(request.getParameter("newic"));
                        vn.setOldic(request.getParameter("oldic"));
                        vn.setPassport(request.getParameter("passport"));
                        vn.setPhone(request.getParameter("phone"));
                        vn.setPostcode(request.getParameter("postcode"));
                        vn.setRace(request.getParameter("race"));
                        vn.setReligion(request.getParameter("religion"));
                        vn.setSex(request.getParameter("sex"));
                        vn.setState(request.getParameter("state"));
                        vn.setStatus(request.getParameter("status"));
                        ExecDAO.saveData(log, vn);
                        urlsend = "/cf_exec_list.jsp?id=" + moduleid;
                        break;
                    }
                case "edit":
                    urlsend = "/cf_exec_edit.jsp?referenceno=" + request.getParameter("referenceno");
                    break;
                case "editprocess":
                    {
                        Executive vn = new Executive();
                        vn.setAddress(request.getParameter("address"));
                        vn.setBirth(request.getParameter("birth"));
                        vn.setBlood(request.getParameter("blood"));
                        vn.setCitizen(request.getParameter("citizen"));
                        vn.setCity(request.getParameter("city"));
                        vn.setEmail(request.getParameter("email"));
                        vn.setFax(request.getParameter("fax"));
                        vn.setHp(request.getParameter("hp"));
                        vn.setId(request.getParameter("id"));
                        vn.setMarital(request.getParameter("marital"));
                        vn.setName(request.getParameter("name"));
                        vn.setNewic(request.getParameter("newic"));
                        vn.setOldic(request.getParameter("oldic"));
                        vn.setPassport(request.getParameter("passport"));
                        vn.setPhone(request.getParameter("phone"));
                        vn.setPostcode(request.getParameter("postcode"));
                        vn.setRace(request.getParameter("race"));
                        vn.setReligion(request.getParameter("religion"));
                        vn.setSex(request.getParameter("sex"));
                        vn.setState(request.getParameter("state"));
                        vn.setStatus(request.getParameter("status"));
                        ExecDAO.updateItem(log, vn);
                        urlsend = "/cf_exec_list.jsp?referenceno=" + request.getParameter("referenceno");
                        break;
                    }
                case "editpage":
                    urlsend = "/gl_edit_acc.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "viewdetail":
                    urlsend = "/cb_chequebook_view.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "delete":
                    ExecDAO.deleteItem(log, request.getParameter("referenceno"));
                    urlsend = "/cf_exec_list.jsp?id=" + moduleid;
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("100");//nullpointer
                    ErrorIO.setType("data");
                    urlsend = "/error.jsp";
                    break;
            }
            
            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_ExecInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_ExecInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
