/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.setup.company;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.setup.company.StaffDAO;
import com.lcsb.fms.dao.setup.configuration.LotMemberDAO;
import com.lcsb.fms.general.ErrorDAO;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.model.setup.company.staff.Staff;
import com.lcsb.fms.model.setup.company.staff.StaffBank;
import com.lcsb.fms.model.setup.company.staff.StaffChild;
import com.lcsb.fms.model.setup.company.staff.StaffDesignation;
import com.lcsb.fms.model.setup.company.staff.StaffSpouse;
import com.lcsb.fms.model.setup.configuration.LotMember;
import com.lcsb.fms.model.setup.configuration.LotMemberLot;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_StaffInfo", urlPatterns = {"/Path_StaffInfo"})
public class Path_StaffInfo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/co_staff_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/co_staff_add_basis.jsp?id=" + moduleid;
                    break;
                case "viewall":
                    urlsend = "/co_staff_viewall.jsp?id=" + moduleid;
                    break;
                case "addlot":
                    urlsend = "/cf_lotmem_add_lot.jsp?referno=" + request.getParameter("refer");
                    break;
                case "viewdesignation":
                    urlsend = "/co_staff_designation_list.jsp?refer=" + request.getParameter("refer");
                    break;
                case "viewbank":
                    urlsend = "/co_staff_bank_list.jsp?refer=" + request.getParameter("refer");
                    break;
                case "viewbasic":
                    urlsend = "/co_staff_basis_view.jsp?refer=" + request.getParameter("refer");
                    break;
                case "viewspouse":
                    urlsend = "/co_staff_spouse_list.jsp?refer=" + request.getParameter("refer");
                    break;
                case "viewchild":
                    urlsend = "/co_staff_child_list.jsp?refer=" + request.getParameter("refer");
                    break;
                case "adddesignation":
                    urlsend = "/co_staff_designation_add.jsp?refer=" + request.getParameter("refer");
                    break;
                case "addbank":
                    urlsend = "/co_staff_bank_add.jsp?refer=" + request.getParameter("refer");
                    break;
                case "addchild":
                    urlsend = "/co_staff_child_add.jsp?refer=" + request.getParameter("refer");
                    break;
                case "addspouse":
                    urlsend = "/co_staff_spouse_add.jsp?refer=" + request.getParameter("refer");
                    break;
                case "addprocess": {
                    Staff st = new Staff();
                    st.setAddress(request.getParameter("address"));
                    st.setBirth(request.getParameter("birth"));
                    st.setCitizen(request.getParameter("citizen"));
                    st.setCity(request.getParameter("city"));
                    st.setEmail(request.getParameter("email"));
                    st.setFax(request.getParameter("fax"));
                    st.setFlag(request.getParameter("flag"));
                    st.setHp(request.getParameter("hp"));
                    st.setIc(request.getParameter("ic"));
                    st.setMarital(request.getParameter("marital"));
                    st.setName(request.getParameter("name"));
                    st.setPhone(request.getParameter("phone"));
                    st.setPostcode(request.getParameter("postcode"));
                    st.setRace(request.getParameter("race"));
                    st.setReligion(request.getParameter("religion"));
                    st.setRemarks(request.getParameter("remarks"));
                    st.setSex(request.getParameter("sex"));
                    st.setStaffid(request.getParameter("staffid"));
                    st.setState(request.getParameter("state"));
                    st.setStatus(request.getParameter("status"));
                    st.setTitle(request.getParameter("title"));
                    st.setDatejoin(request.getParameter("datejoin"));
                    
                    StaffDAO.saveData(log, st);
                    urlsend = "/co_staff_list.jsp?id=" + moduleid;
                    break;
                }
                case "editbasisprocess": {
                    Staff st = new Staff();
                    st.setAddress(request.getParameter("address"));
                    st.setBirth(request.getParameter("birth"));
                    st.setCitizen(request.getParameter("citizen"));
                    st.setCity(request.getParameter("city"));
                    st.setEmail(request.getParameter("email"));
                    st.setFax(request.getParameter("fax"));
                    st.setFlag(request.getParameter("flag"));
                    st.setHp(request.getParameter("hp"));
                    st.setIc(request.getParameter("ic"));
                    st.setMarital(request.getParameter("marital"));
                    st.setName(request.getParameter("name"));
                    st.setPhone(request.getParameter("phone"));
                    st.setPostcode(request.getParameter("postcode"));
                    st.setRace(request.getParameter("race"));
                    st.setReligion(request.getParameter("religion"));
                    st.setRemarks(request.getParameter("remarks"));
                    st.setSex(request.getParameter("sex"));
                    st.setStaffid(request.getParameter("staffid"));
                    st.setState(request.getParameter("state"));
                    st.setStatus(request.getParameter("status"));
                    st.setTitle(request.getParameter("title"));
                    st.setPobirth(request.getParameter("pobirth"));
                    st.setDatejoin(request.getParameter("datejoin"));
                    st.setOldic(request.getParameter("oldic"));
                    
                    StaffDAO.updateBasis(log, st);
                    urlsend = "/co_staff_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("staffid") + "&tab=viewbasic";
                    break;
                }
                case "editmain":
                    urlsend = "/co_staff_edit.jsp?refer=" + request.getParameter("referno");
                    break;
                case "editprocess": {
                    LotMember lm = new LotMember();
                    lm.setMemCode(request.getParameter("mem_Code"));
                    lm.setTitle(request.getParameter("title"));
                    lm.setUsetitle(request.getParameter("usetitle"));
                    lm.setMemName(request.getParameter("mem_Name"));
                    lm.setIc(request.getParameter("ic"));
                    lm.setAddress(request.getParameter("address"));
                    lm.setPostcode(request.getParameter("postcode"));
                    lm.setCity(request.getParameter("city"));//
                    lm.setState(request.getParameter("state"));
                    lm.setBank(request.getParameter("bank"));
                    lm.setAcc(request.getParameter("acc"));
                    lm.setPaymethod(request.getParameter("paymethod"));
                    lm.setStatus(request.getParameter("status"));
                    lm.setNotel(request.getParameter("notel"));
                    lm.setDateInactive(request.getParameter("date_inactive"));
                    lm.setPreOwner(request.getParameter("pre_owner"));//
                    lm.setRelation(request.getParameter("relation"));
                    lm.setDateActive(request.getParameter("date_active"));
                    lm.setNewOwner(request.getParameter("new_owner"));
                    lm.setRemark(request.getParameter("remark"));
                    LotMemberDAO.updateItem(log, lm);
                    urlsend = "/cf_lotmem_edit_list.jsp?referno=" + request.getParameter("mem_Code");
                    break;
                }
                case "editlist":
                    urlsend = "/co_staff_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("referno");
                    break;
                case "editbasis":
                    urlsend = "/co_staff_edit_basis.jsp?id=" + moduleid + "&refer=" + request.getParameter("refer");
                    break;
                case "editdesignation":
                    urlsend = "/co_staff_designation_edit.jsp?id=" + moduleid + "&refer=" + request.getParameter("refer");
                    break;
                case "editbank":
                    urlsend = "/co_staff_bank_edit.jsp?id=" + moduleid + "&refer=" + request.getParameter("refer");
                    break;
                case "editspouse":
                    urlsend = "/co_staff_spouse_edit.jsp?id=" + moduleid + "&refer=" + request.getParameter("refer");
                    break;
                case "editchild":
                    urlsend = "/co_staff_child_edit.jsp?id=" + moduleid + "&refer=" + request.getParameter("refer");
                    break;
                case "deletedesignation":
                    StaffDAO.deleteDesignation(log, request.getParameter("id"));
                    urlsend = "/co_staff_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("refer") + "&tab=viewdesignation";
                    break;
                case "deletebank":
                    StaffDAO.deleteBank(log, request.getParameter("id"));
                    urlsend = "/co_staff_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("refer") + "&tab=viewbank";
                    break;
                case "deletespouse":
                    StaffDAO.deleteSpouse(log, request.getParameter("id"));
                    urlsend = "/co_staff_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("refer") + "&tab=viewspouse";
                    break;
                case "deletechild":
                    StaffDAO.deleteChild(log, request.getParameter("id"));
                    urlsend = "/co_staff_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("refer") + "&tab=viewchild";
                    break;
                case "delete":
                    LotMemberDAO.deleteItem(log, request.getParameter("referenceno"));
                    urlsend = "/cf_lotmem_list.jsp?id=" + moduleid;
                    break;
                case "adddesignationprocess": {
                    
                    StaffDesignation std = new StaffDesignation();
                    
                    std.setDateend(request.getParameter("dateend"));
                    std.setDatejoin(request.getParameter("datejoin"));
                    std.setDeptCode(request.getParameter("dept_code"));
                    std.setDeptDescp(request.getParameter("dept_descp"));
                    std.setDesignation(request.getParameter("designation"));
                    std.setStaffid(request.getParameter("staffid"));
                    std.setStatus(request.getParameter("status"));
                    std.setWorklocation(request.getParameter("worklocation"));
                    
                    
                    StaffDAO.saveDesignation(log, std);
                    urlsend = "/co_staff_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("staffid") + "&tab=viewdesignation";
                    break;
                }
                case "addbankprocess": {
                    
                    StaffBank std = new StaffBank();
                    
                    std.setAccno(request.getParameter("accno"));
                    std.setBankcode(request.getParameter("bankcode"));
                    std.setBankdesc(request.getParameter("bankdesc"));
                    std.setStaffid(request.getParameter("staffid"));
                    std.setStatus(request.getParameter("status"));
                    
                    
                    StaffDAO.saveBank(log, std);
                    urlsend = "/co_staff_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("staffid") + "&tab=viewbank";
                    break;
                }
                case "addspouseprocess": {
                    
                    StaffSpouse sts = new StaffSpouse();
                    
                    sts.setCitizen(request.getParameter("citizen"));
                    sts.setDob(request.getParameter("dob"));
                    sts.setIc(request.getParameter("ic"));
                    sts.setName(request.getParameter("name"));
                    sts.setPassport(request.getParameter("passport"));
                    sts.setRace(request.getParameter("race"));
                    sts.setReligion(request.getParameter("religion"));
                    sts.setStaffid(request.getParameter("staffid"));
                    sts.setWorking(request.getParameter("working"));
                    
                    
                    StaffDAO.saveSpouse(log, sts);
                    urlsend = "/co_staff_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("staffid") + "&tab=viewspouse";
                    break;
                }
                case "addchildprocess": {
                    
                    StaffChild sts = new StaffChild();
                    
                    sts.setAlive(request.getParameter("alive"));
                    sts.setBirthno(request.getParameter("birthno"));
                    sts.setCitizen(request.getParameter("citizen"));
                    sts.setDob(request.getParameter("dob"));
                    sts.setGender(request.getParameter("gender"));
                    sts.setIc(request.getParameter("ic"));
                    sts.setMotherid(request.getParameter("motherid"));
                    sts.setName(request.getParameter("name"));
                    sts.setNo(request.getParameter("no"));
                    sts.setRace(request.getParameter("race"));
                    sts.setReligion(request.getParameter("religion"));
                    sts.setStaffid(request.getParameter("staffid"));
                    sts.setTaxdependent(((request.getParameter("taxdependent").equals("Yes")) ? true : false));
                    
                    
                    StaffDAO.saveChild(log, sts);
                    urlsend = "/co_staff_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("staffid") + "&tab=viewchild";
                    break;
                }
                case "editchildprocess": {
                    
                    StaffChild sts = new StaffChild();
                    
                    sts.setAlive(request.getParameter("alive"));
                    sts.setBirthno(request.getParameter("birthno"));
                    sts.setCitizen(request.getParameter("citizen"));
                    sts.setDob(request.getParameter("dob"));
                    sts.setGender(request.getParameter("gender"));
                    sts.setIc(request.getParameter("ic"));
                    sts.setMotherid(request.getParameter("motherid"));
                    sts.setName(request.getParameter("name"));
                    sts.setNo(request.getParameter("no"));
                    sts.setRace(request.getParameter("race"));
                    sts.setReligion(request.getParameter("religion"));
                    sts.setStaffid(request.getParameter("staffid"));
                    sts.setId(Integer.parseInt(request.getParameter("id")));
                    sts.setTaxdependent(((request.getParameter("taxdependent").equals("Yes")) ? true : false));
                    
                    
                    
                    StaffDAO.updateChild(log, sts);
                    urlsend = "/co_staff_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("staffid") + "&tab=viewchild";
                    break;
                }
                case "editspouseprocess": {
                    
                    StaffSpouse sts = new StaffSpouse();
                    
                    sts.setCitizen(request.getParameter("citizen"));
                    sts.setDob(request.getParameter("dob"));
                    sts.setIc(request.getParameter("ic"));
                    sts.setName(request.getParameter("name"));
                    sts.setPassport(request.getParameter("passport"));
                    sts.setRace(request.getParameter("race"));
                    sts.setReligion(request.getParameter("religion"));
                    sts.setStaffid(request.getParameter("staffid"));
                    sts.setWorking(request.getParameter("working"));
                    sts.setId(Integer.parseInt(request.getParameter("id")));
                    
                    
                    StaffDAO.updateSpouse(log, sts);
                    urlsend = "/co_staff_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("staffid") + "&tab=viewspouse";
                    break;
                }
                case "editdesignationprocess": {
                    
                    StaffDesignation std = new StaffDesignation();
                    
                    std.setId(Integer.parseInt(request.getParameter("id")));
                    std.setDateend(request.getParameter("dateend"));
                    std.setDatejoin(request.getParameter("datejoin"));
                    std.setDeptCode(request.getParameter("dept_code"));
                    std.setDeptDescp(request.getParameter("dept_descp"));
                    std.setDesignation(request.getParameter("designation"));
                    std.setStaffid(request.getParameter("staffid"));
                    std.setStatus(request.getParameter("status"));
                    std.setWorklocation(request.getParameter("worklocation"));
                    
                    StaffDAO.updateDesignation(log, std);
                    urlsend = "/co_staff_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("staffid") + "&tab=viewdesignation";
                    break;
                }
                case "editbankprocess": {
                    
                    StaffBank std = new StaffBank();
                    
                    std.setAccno(request.getParameter("accno"));
                    std.setBankcode(request.getParameter("bankcode"));
                    std.setBankdesc(request.getParameter("bankdesc"));
                    std.setStaffid(request.getParameter("staffid"));
                    std.setStatus(request.getParameter("status"));
                    std.setId(Integer.parseInt(request.getParameter("id")));
                    
                    StaffDAO.updateBank(log, std);
                    urlsend = "/co_staff_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("staffid") + "&tab=viewbank";
                    break;
                }
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("200");//nullpointer
                    ErrorIO.setType("data");
                    ErrorIO.setComcode(log.getEstateCode());
                    ErrorIO.setLoggedbyid(log.getUserID());
                    ErrorIO.setLoggedbyname(log.getFullname());
                    ErrorIO.setModuleid(moduleid);
                    ErrorIO.setProcess(process);
                    ErrorDAO.logError(log);
                    urlsend = "/error.jsp?moduleid=" + moduleid + "&refer=" + request.getParameter("referno");
                    break;
            }

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_StaffInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_StaffInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
