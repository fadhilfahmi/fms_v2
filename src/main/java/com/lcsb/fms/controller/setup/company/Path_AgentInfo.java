/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.setup.company;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.setup.company.AgentDAO;
import com.lcsb.fms.dao.setup.configuration.LotMemberDAO;
import com.lcsb.fms.dao.setup.configuration.PortfolioDAO;
import com.lcsb.fms.general.ErrorDAO;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.model.setup.company.Agent;
import com.lcsb.fms.model.setup.configuration.LotMemberLot;
import com.lcsb.fms.model.setup.configuration.Portfolio;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_AgentInfo", urlPatterns = {"/Path_AgentInfo"})
public class Path_AgentInfo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/co_agent_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/co_agent_add.jsp?id=" + moduleid;
                    break;
                case "edit":
                    urlsend = "/co_agent_edit.jsp?refer=" + request.getParameter("refer");
                    break;
                case "addprocess": {
                    Agent lm = new Agent();
                    lm.setAddress(request.getParameter("address"));
                    lm.setCity(request.getParameter("city"));
                    lm.setCoa(request.getParameter("coa"));
                    lm.setCoadescp(request.getParameter("coadescp"));
                    lm.setCode(request.getParameter("code"));
                    lm.setCoregister(request.getParameter("coregister"));
                    lm.setCountry(request.getParameter("country"));
                    lm.setDescp(request.getParameter("descp"));//
                    lm.setFax(request.getParameter("fax"));
                    lm.setGstid(request.getParameter("gstid"));
                    lm.setPhone(request.getParameter("phone"));
                    lm.setPostcode(request.getParameter("postcode"));
                    lm.setRemarks(request.getParameter("remarks"));
                    lm.setState(request.getParameter("status"));
                    AgentDAO.saveData(log, lm);
                    urlsend = "/co_agent_list.jsp?id=" + moduleid;
                    break;
                }
                case "editprocess": {
                    Agent lm = new Agent();
                    lm.setAddress(request.getParameter("address"));
                    lm.setCity(request.getParameter("city"));
                    lm.setCoa(request.getParameter("coa"));
                    lm.setCoadescp(request.getParameter("coadescp"));
                    lm.setCode(request.getParameter("code"));
                    lm.setCoregister(request.getParameter("coregister"));
                    lm.setCountry(request.getParameter("country"));
                    lm.setDescp(request.getParameter("descp"));//
                    lm.setFax(request.getParameter("fax"));
                    lm.setGstid(request.getParameter("gstid"));
                    lm.setPhone(request.getParameter("phone"));
                    lm.setPostcode(request.getParameter("postcode"));
                    lm.setRemarks(request.getParameter("remarks"));
                    lm.setState(request.getParameter("status"));
                    AgentDAO.updateData(log, lm);
                    urlsend = "/co_agent_list.jsp?id=" + moduleid;
                    break;
                }
                case "delete":
                    AgentDAO.deleteData(log, request.getParameter("refer"));
                    urlsend = "/co_agent_list.jsp?id=" + moduleid;
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("200");//nullpointer
                    ErrorIO.setType("data");
                    ErrorIO.setComcode(log.getEstateCode());
                    ErrorIO.setLoggedbyid(log.getUserID());
                    ErrorIO.setLoggedbyname(log.getFullname());
                    ErrorIO.setModuleid(moduleid);
                    ErrorIO.setProcess(process);
                    ErrorDAO.logError(log);
                    urlsend = "/error.jsp?moduleid=" + moduleid + "&refer=" + request.getParameter("referno");
                    break;
            }

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_AgentInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_AgentInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
