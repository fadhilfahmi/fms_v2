/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.setup.company;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.setup.company.BankDAO;
import com.lcsb.fms.model.setup.company.Bank;
import com.lcsb.fms.model.setup.company.BankBranch;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_BankInfoCompany", urlPatterns = {"/Path_BankInfoCompany"})
public class Path_BankInfoCompany extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/co_bank_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/co_bank_add.jsp?id=" + moduleid;
                    break;
                case "addbranch":
                    urlsend = "/co_bank_add_branch.jsp?id=" + moduleid + "&referno=" + request.getParameter("referno");
                    break;
                case "editlist":
                    urlsend = "/co_bank_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("referno");
                    break;
                case "addprocess": {
                    Bank bn = new Bank();
                    bn.setCode(request.getParameter("code"));
                    bn.setName(request.getParameter("name"));
                    bn.setAbbreviation(request.getParameter("abbreviation"));
                    bn.setCoa(request.getParameter("coa"));
                    bn.setCoadescp(request.getParameter("coadescp"));
                    bn.setOverdue(Integer.parseInt(request.getParameter("overdue")));
                    bn.setRemarks(request.getParameter("remarks"));
                    BankDAO.saveData(log, bn);
                    urlsend = "/co_bank_list.jsp?id=" + moduleid;
                    break;
                }
                case "addbranchprocess": {
                    BankBranch bnb = new BankBranch();
                    bnb.setAddress(request.getParameter("address"));
                    bnb.setBankcode(request.getParameter("bankcode"));
                    bnb.setBankname(request.getParameter("bankname"));
                    bnb.setBranchcode(request.getParameter("branchcode"));
                    bnb.setBranchname(request.getParameter("branchname"));
                    bnb.setCity(request.getParameter("city"));
                    bnb.setEmail(request.getParameter("email"));
                    bnb.setFax(request.getParameter("fax"));
                    bnb.setHp(request.getParameter("hp"));
                    bnb.setPerson(request.getParameter("person"));
                    bnb.setPhone(request.getParameter("phone"));
                    bnb.setPosition(request.getParameter("position"));
                    bnb.setPostcode(request.getParameter("postcode"));
                    bnb.setRemarks(request.getParameter("remarks"));
                    bnb.setState(request.getParameter("state"));
                    bnb.setTitle(request.getParameter("title"));
                    bnb.setUrl(request.getParameter("url"));
                    BankDAO.saveDataBranch(log, bnb);
                    //urlsend = "/co_bank_edit_list.jsp?id="+moduleid+"&referno="+request.getParameter("bankcode");

                    notify = "&status=success&text=New Branch has been added.";
                    urlsend = "/co_bank_edit_list.jsp?referno=" + request.getParameter("bankcode") + notify;
                    break;
                }
                case "editbranchprocess": {
                    BankBranch bnb = new BankBranch();
                    bnb.setAddress(request.getParameter("address"));
                    bnb.setBankcode(request.getParameter("bankcode"));
                    bnb.setBankname(request.getParameter("bankname"));
                    bnb.setBranchcode(request.getParameter("branchcode"));
                    bnb.setBranchname(request.getParameter("branchname"));
                    bnb.setCity(request.getParameter("city"));
                    bnb.setEmail(request.getParameter("email"));
                    bnb.setFax(request.getParameter("fax"));
                    bnb.setHp(request.getParameter("hp"));
                    bnb.setPerson(request.getParameter("person"));
                    bnb.setPhone(request.getParameter("phone"));
                    bnb.setPosition(request.getParameter("position"));
                    bnb.setPostcode(request.getParameter("postcode"));
                    bnb.setRemarks(request.getParameter("remarks"));
                    bnb.setState(request.getParameter("state"));
                    bnb.setTitle(request.getParameter("title"));
                    bnb.setUrl(request.getParameter("url"));
                    BankDAO.editDataBranch(log, bnb);
                    urlsend = "/co_bank_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("bankcode");
                    break;
                }
                case "edit":
                    urlsend = "/co_bank_edit.jsp?referenceno=" + request.getParameter("referenceno");
                    break;
                case "edit_st":
                    urlsend = "/co_bank_edit_branch.jsp?referno=" + request.getParameter("referno");
                    break;
                case "editprocess": {
                    Bank bn = new Bank();
                    bn.setCode(request.getParameter("code"));
                    bn.setName(request.getParameter("name"));
                    bn.setAbbreviation(request.getParameter("abbreviation"));
                    bn.setCoa(request.getParameter("coa"));
                    bn.setCoadescp(request.getParameter("coadescp"));
                    bn.setOverdue(Integer.parseInt(request.getParameter("overdue")));
                    bn.setRemarks(request.getParameter("remarks"));
                    BankDAO.updateItem(log, bn);
                    urlsend = "/co_bank_list.jsp?referenceno=" + request.getParameter("referenceno");
                    break;
                }
                case "editpage":
                    urlsend = "/gl_edit_acc.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "viewdetail":
                    urlsend = "/cb_chequebook_view.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "delete":
                    BankDAO.deleteBank(log, request.getParameter("referno"));
                    urlsend = "/co_bank_list.jsp?id=" + moduleid;
                    break;
                case "delete_st":
                    BankDAO.deleteBranch(log, request.getParameter("referno"));
                    notify = "&status=info&text=Branch has been deleted.";
                    urlsend = "/co_bank_edit_list.jsp?referno=" + request.getParameter("referno").substring(0, 2) + notify;
                    break;
                default:
                    break;
            }

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_BankInfoCompany.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_BankInfoCompany.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
