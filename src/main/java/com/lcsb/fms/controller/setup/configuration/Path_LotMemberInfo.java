/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.setup.configuration;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.dao.setup.configuration.LotMemberDAO;
import com.lcsb.fms.general.ErrorDAO;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.model.setup.configuration.LotInformation;
import com.lcsb.fms.model.setup.configuration.LotMember;
import com.lcsb.fms.model.setup.configuration.LotMemberLot;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_LotMemberInfo", urlPatterns = {"/Path_LotMemberInfo"})
public class Path_LotMemberInfo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";

            switch (process) {
                case "viewlist":
                    urlsend = "/cf_lotmem_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/cf_lotmem_add.jsp?id=" + moduleid;
                    break;
                case "viewall":
                    urlsend = "/cf_lotmem_viewall.jsp?id=" + moduleid;
                    break;
                case "addlot":
                    urlsend = "/cf_lotmem_add_lot.jsp?referno=" + request.getParameter("refer");
                    break;
                case "edit_lot":
                    urlsend = "/cf_lotmem_edit_lot.jsp?referno=" + request.getParameter("refer");
                    break;
                case "addprocess": {
                    LotMember lm = new LotMember();
                    lm.setMemCode(request.getParameter("mem_Code"));
                    lm.setTitle(request.getParameter("title"));
                    lm.setUsetitle(request.getParameter("usetitle"));
                    lm.setMemName(request.getParameter("mem_Name"));
                    lm.setIc(request.getParameter("ic"));
                    lm.setAddress(request.getParameter("address"));
                    lm.setPostcode(request.getParameter("postcode"));
                    lm.setCity(request.getParameter("city"));//
                    lm.setState(request.getParameter("state"));
                    lm.setBank(request.getParameter("bank"));
                    lm.setAcc(request.getParameter("acc"));
                    lm.setPaymethod(request.getParameter("paymethod"));
                    lm.setStatus(request.getParameter("status"));
                    lm.setNotel(request.getParameter("notel"));
                    lm.setDateInactive(request.getParameter("date_inactive"));
                    lm.setPreOwner(request.getParameter("pre_owner"));//
                    lm.setRelation(request.getParameter("relation"));
                    lm.setDateActive(request.getParameter("date_active"));
                    lm.setNewOwner(request.getParameter("new_owner"));
                    lm.setRemark(request.getParameter("remark"));
                    LotMemberDAO.saveData(log, lm);
                    urlsend = "/cf_lotmem_list.jsp?id=" + moduleid;
                    break;
                }
                case "editmain":
                    urlsend = "/cf_lotmem_edit.jsp?refer=" + request.getParameter("referno");
                    break;
                case "editprocess": {
                    LotMember lm = new LotMember();
                    lm.setMemCode(request.getParameter("mem_Code"));
                    lm.setTitle(request.getParameter("title"));
                    lm.setUsetitle(request.getParameter("usetitle"));
                    lm.setMemName(request.getParameter("mem_Name"));
                    lm.setIc(request.getParameter("ic"));
                    lm.setAddress(request.getParameter("address"));
                    lm.setPostcode(request.getParameter("postcode"));
                    lm.setCity(request.getParameter("city"));//
                    lm.setState(request.getParameter("state"));
                    lm.setBank(request.getParameter("bank"));
                    lm.setAcc(request.getParameter("acc"));
                    lm.setPaymethod(request.getParameter("paymethod"));
                    lm.setStatus(request.getParameter("status"));
                    lm.setNotel(request.getParameter("notel"));
                    lm.setDateInactive(request.getParameter("date_inactive"));
                    lm.setPreOwner(request.getParameter("pre_owner"));//
                    lm.setRelation(request.getParameter("relation"));
                    lm.setDateActive(request.getParameter("date_active"));
                    lm.setNewOwner(request.getParameter("new_owner"));
                    lm.setRemark(request.getParameter("remark"));
                    LotMemberDAO.updateItem(log, lm);
                    urlsend = "/cf_lotmem_edit_list.jsp?referno=" + request.getParameter("mem_Code");
                    break;
                }
                case "editlist":
                    urlsend = "/cf_lotmem_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("referno");
                    break;
                case "delete":
                    LotMemberDAO.deleteItem(log, request.getParameter("referenceno"));
                    urlsend = "/cf_lotmem_list.jsp?id=" + moduleid;
                    break;
                case "savelot":
                    
                    LotMemberLot lt = new LotMemberLot();
                    
                    lt.setAcre(request.getParameter("acre"));
                    lt.setHectarage(request.getParameter("hectarage"));
                    lt.setLotCode(request.getParameter("lot_Code"));
                    lt.setLotDescp(request.getParameter("lot_Descp"));
                    lt.setMemCode(request.getParameter("mem_Code"));
                    lt.setNopetak(request.getParameter("nopetak"));
                    
                    LotMemberDAO.saveLot(log, lt);
                    urlsend = "/cf_lotmem_edit_list.jsp?referno=" + request.getParameter("mem_Code");
                    break;
                case "updatelot":
                    
                    LotMemberLot l = new LotMemberLot();
                    
                    l.setId(Integer.parseInt(request.getParameter("id")));
                    l.setAcre(request.getParameter("acre"));
                    l.setHectarage(request.getParameter("hectarage"));
                    l.setLotCode(request.getParameter("lot_Code"));
                    l.setLotDescp(request.getParameter("lot_Descp"));
                    l.setMemCode(request.getParameter("mem_Code"));
                    l.setNopetak(request.getParameter("nopetak"));
                    
                    LotMemberDAO.updateLot(log, l);
                    urlsend = "/cf_lotmem_edit_list.jsp?referno=" + request.getParameter("mem_Code");
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("200");//nullpointer
                    ErrorIO.setType("data");
                    ErrorIO.setComcode(log.getEstateCode());
                    ErrorIO.setLoggedbyid(log.getUserID());
                    ErrorIO.setLoggedbyname(log.getFullname());
                    ErrorIO.setModuleid(moduleid);
                    ErrorIO.setProcess(process);
                    ErrorDAO.logError(log);
                    urlsend = "/error.jsp?moduleid=" + moduleid + "&refer=" + request.getParameter("referno");
                    break;
            }

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_LotMemberInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_LotMemberInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
