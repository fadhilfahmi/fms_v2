/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.setup.configuration;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.dao.setup.configuration.ContractorDAO;
import com.lcsb.fms.model.setup.configuration.Contractor;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_ContractorInfo", urlPatterns = {"/Path_ContractorInfo"})
public class Path_ContractorInfo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";
            
            switch (process) {
                case "viewlist":
                    urlsend = "/cf_contractor_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/cf_contractor_add.jsp?id=" + moduleid;
                    break;
                case "addprocess":
                    {
                        Contractor cn = new Contractor();
                        cn.setCode(request.getParameter("code"));
                        cn.setAddress(request.getParameter("address"));
                        cn.setBank(request.getParameter("bank"));
                        cn.setBankaccount(request.getParameter("bankaccount"));
                        cn.setBankdesc(request.getParameter("bankdesc"));
                        cn.setBumiputra(request.getParameter("bumiputra"));
                        cn.setCoa(request.getParameter("coa"));
                        cn.setCoadescp(request.getParameter("coadescp"));
                        cn.setCity(request.getParameter("city"));
                        cn.setCompanyname(request.getParameter("companyname"));
                        cn.setContactperson(request.getParameter("contactperson"));
                        cn.setContacttitle(request.getParameter("contacttitle"));
                        cn.setContactposition(request.getParameter("contactposition"));
                        cn.setCountry(request.getParameter("country"));
                        cn.setDepositcoa(request.getParameter("depositcoa"));
                        cn.setDepositcoadescp(request.getParameter("depositcoadescp"));
                        cn.setEmail(request.getParameter("email"));
                        cn.setFax(request.getParameter("fax"));
                        cn.setHp(request.getParameter("hp"));
                        cn.setKelas(request.getParameter("kelas"));
                        cn.setNationality(request.getParameter("nationality"));
                        cn.setOwneric(request.getParameter("owneric"));
                        cn.setOwnername(request.getParameter("ownername"));
                        cn.setPayment(request.getParameter("payment"));
                        cn.setPermitcode(request.getParameter("permitcode"));
                        cn.setPermitdesc(request.getParameter("permitdesc"));
                        cn.setPhone(request.getParameter("phone"));
                        cn.setPostcode(request.getParameter("postcode"));
                        cn.setRegister(request.getParameter("register"));
                        cn.setRemarks(request.getParameter("remarks"));
                        cn.setRetention(request.getParameter("retention"));
                        cn.setRetentiondescp(request.getParameter("retentiondescp"));
                        cn.setState(request.getParameter("state"));
                        cn.setUrl(request.getParameter("url"));
                        cn.setVendor(request.getParameter("vendor"));
                        ContractorDAO.saveData(log, cn);
                        urlsend = "/cf_contractor_list.jsp?id=" + moduleid;
                        break;
                    }
                case "edit":
                    urlsend = "/cf_contractor_edit.jsp?referenceno=" + request.getParameter("referenceno");
                    break;
                case "editprocess":
                    {
                        Contractor cn = new Contractor();
                        cn.setCode(request.getParameter("code"));
                        cn.setAddress(request.getParameter("address"));
                        cn.setBank(request.getParameter("bank"));
                        cn.setBankaccount(request.getParameter("bankaccount"));
                        cn.setBankdesc(request.getParameter("bankdesc"));
                        cn.setBumiputra(request.getParameter("bumiputra"));
                        cn.setCoa(request.getParameter("coa"));
                        cn.setCoadescp(request.getParameter("coadescp"));
                        cn.setCity(request.getParameter("city"));
                        cn.setCompanyname(request.getParameter("companyname"));
                        cn.setContactperson(request.getParameter("contactperson"));
                        cn.setContacttitle(request.getParameter("contacttitle"));
                        cn.setContactposition(request.getParameter("contactposition"));
                        cn.setCountry(request.getParameter("country"));
                        cn.setDepositcoa(request.getParameter("depositcoa"));
                        cn.setDepositcoadescp(request.getParameter("depositcoadescp"));
                        cn.setEmail(request.getParameter("email"));
                        cn.setFax(request.getParameter("fax"));
                        cn.setHp(request.getParameter("hp"));
                        cn.setKelas(request.getParameter("kelas"));
                        cn.setNationality(request.getParameter("nationality"));
                        cn.setOwneric(request.getParameter("owneric"));
                        cn.setOwnername(request.getParameter("ownername"));
                        cn.setPayment(request.getParameter("payment"));
                        cn.setPermitcode(request.getParameter("permitcode"));
                        cn.setPermitdesc(request.getParameter("permitdesc"));
                        cn.setPhone(request.getParameter("phone"));
                        cn.setPostcode(request.getParameter("postcode"));
                        cn.setRegister(request.getParameter("register"));
                        cn.setRemarks(request.getParameter("remarks"));
                        cn.setRetention(request.getParameter("retention"));
                        cn.setRetentiondescp(request.getParameter("retentiondescp"));
                        cn.setState(request.getParameter("state"));
                        cn.setUrl(request.getParameter("url"));
                        cn.setVendor(request.getParameter("vendor"));
                        ContractorDAO.updateItem(log, cn);
                        urlsend = "/cf_contractor_list.jsp?referenceno=" + request.getParameter("referenceno");
                        break;
                    }
                case "editpage":
                    urlsend = "/gl_edit_acc.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "viewdetail":
                    urlsend = "/cb_chequebook_view.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "delete":
                    ContractorDAO.deleteItem(log, request.getParameter("referenceno"));
                    urlsend = "/cf_contractor_list.jsp?id=" + moduleid;
                    break;
                default:
                    break;
            }
            
            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_ContractorInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_ContractorInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
