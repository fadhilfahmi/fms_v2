/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.setup.company;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.controller.setup.configuration.Path_ChartofAccount;
import com.lcsb.fms.dao.setup.company.ManagementCpDAO;
import com.lcsb.fms.dao.setup.configuration.ProductInfoDAO;
import com.lcsb.fms.model.setup.company.CeManage;
import com.lcsb.fms.model.setup.company.CeManageBank;
import com.lcsb.fms.model.setup.company.CeManageBankPK;
import com.lcsb.fms.model.setup.company.CeManagePetty;
import com.lcsb.fms.util.ext.ParseSafely;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_ManageInfoCompany", urlPatterns = {"/Path_ManageInfoCompany"})
public class Path_ManageInfoCompany extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";

            CeManage cb = new CeManage();
            cb.setBacd(request.getParameter("bacd"));
            cb.setBcac(request.getParameter("bcac"));
            cb.setCashbill(request.getParameter("cashbill"));
            cb.setCoa(request.getParameter("coa"));
            cb.setCoadescp(request.getParameter("coadescp"));
            cb.setCode(request.getParameter("code"));
            cb.setHac(request.getParameter("hac"));
            cb.setHad(request.getParameter("had"));
            cb.setMaksimum(request.getParameter("maksimum"));
            cb.setName(request.getParameter("name"));
            cb.setRemarks(request.getParameter("remarks"));

            switch (process) {
                case "viewlist":
                    urlsend = "/cp_management_list.jsp?id=" + moduleid;
                    break;
                case "editlist":
                    urlsend = "/cp_management_edit_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/cp_management_add.jsp?id=" + moduleid;
                    break;
                case "addbank":
                    urlsend = "/cp_management_addbank.jsp?id=" + moduleid + "&referno=" + request.getParameter("refer");
                    break;
                case "addpetty":
                    urlsend = "/cp_management_addpetty.jsp?id=" + moduleid + "&referno=" + request.getParameter("refer");
                    break;
                case "addprocess": {
                    //ProductInfoDAO.saveData(cb);

                    break;
                }
                case "addbankprocess": {
                    CeManageBankPK pk = new CeManageBankPK();
                    pk.setBranchcode(request.getParameter("branchcode"));
                    pk.setCode(request.getParameter("code"));
                    pk.setManagecode(request.getParameter("managecode"));

                    CeManageBank mb = new CeManageBank();
                    mb.setActive(request.getParameter("active"));
                    mb.setBankaccno(request.getParameter("bankaccno"));
                    mb.setBranchname(request.getParameter("branchname"));
                    mb.setCeManageBankPK(pk);
                    mb.setCoacode(request.getParameter("coacode"));
                    mb.setCoadesc(request.getParameter("coadesc"));
                    mb.setFtype(request.getParameter("ftype"));
                    mb.setMajor(request.getParameter("major"));
                    mb.setName(request.getParameter("name"));
                    mb.setRemarks(request.getParameter("remarks"));
                    mb.setManagename(request.getParameter("managename"));

                    ManagementCpDAO.saveBank(log, mb);
                    urlsend = "/cp_management_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("managecode");
                    break;
                }
                case "editbankprocess": {
                    CeManageBankPK pk = new CeManageBankPK();
                    pk.setBranchcode(request.getParameter("branchcode"));
                    pk.setCode(request.getParameter("code"));
                    pk.setManagecode(request.getParameter("managecode"));

                    CeManageBank mb = new CeManageBank();
                    mb.setActive(request.getParameter("active"));
                    mb.setBankaccno(request.getParameter("bankaccno"));
                    mb.setBranchname(request.getParameter("branchname"));
                    mb.setCeManageBankPK(pk);
                    mb.setCoacode(request.getParameter("coacode"));
                    mb.setCoadesc(request.getParameter("coadesc"));
                    mb.setFtype(request.getParameter("ftype"));
                    mb.setMajor(request.getParameter("major"));
                    mb.setName(request.getParameter("name"));
                    mb.setRemarks(request.getParameter("remarks"));
                    mb.setManagename(request.getParameter("managename"));

                    ManagementCpDAO.updateBank(log, mb);
                    urlsend = "/cp_management_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("managecode");
                    break;
                }
                case "addpettyprocess": {

                    CeManagePetty mb = new CeManagePetty();
                    mb.setCoacode(request.getParameter("coacode"));
                    mb.setCoadesc(request.getParameter("coadesc"));
                    mb.setCode(request.getParameter("code"));
                    mb.setDescp(request.getParameter("descp"));
                    mb.setMaxcash(ParseSafely.parseDoubleSafely(request.getParameter("maxcash")));
                    mb.setMaxfloat(ParseSafely.parseDoubleSafely(request.getParameter("maxfloat")));
                    mb.setRemarks(request.getParameter("remarks"));
                    mb.setManagecode(request.getParameter("managecode"));
                    ManagementCpDAO.savePetty(log, mb);
                    urlsend = "/cp_management_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("managecode");
                    break;
                }
                case "editpettyprocess": {

                    CeManagePetty mb = new CeManagePetty();
                    mb.setCoacode(request.getParameter("coacode"));
                    mb.setCoadesc(request.getParameter("coadesc"));
                    mb.setCode(request.getParameter("code"));
                    mb.setDescp(request.getParameter("descp"));
                    mb.setMaxcash(ParseSafely.parseDoubleSafely(request.getParameter("maxcash")));
                    mb.setMaxfloat(ParseSafely.parseDoubleSafely(request.getParameter("maxfloat")));
                    mb.setRemarks(request.getParameter("remarks"));
                    mb.setManagecode(request.getParameter("managecode"));
                    ManagementCpDAO.updatePetty(log, mb);
                    urlsend = "/cp_management_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("managecode");
                    break;
                }
                case "edit":
                    urlsend = "/cf_product_edit.jsp?referenceno=" + request.getParameter("referenceno");
                    break;
                case "edit_bank":
                    urlsend = "/cp_management_editbank.jsp?referno=" + request.getParameter("referno");
                    break;
                case "edit_petty":
                    urlsend = "/cp_management_editpetty.jsp?referno=" + request.getParameter("referno");
                    break;
                case "delete_bank":
                    urlsend = "/cp_management_edit_list.jsp?id=" + moduleid + "&referno=" + request.getParameter("referno");
                    break;
                case "editprocess": {
                    //ProductInfoDAO.updateItem(cb);
                    urlsend = "/cf_product_list.jsp?referenceno=" + request.getParameter("referenceno");
                    break;
                }
                case "delete":
                    ProductInfoDAO.deleteItem(log, request.getParameter("referenceno"));
                    urlsend = "/cf_product_list.jsp?id=" + moduleid;
                    break;
                default:
                    break;
            }

            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_ManageInfoCompany.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_ManageInfoCompany.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
