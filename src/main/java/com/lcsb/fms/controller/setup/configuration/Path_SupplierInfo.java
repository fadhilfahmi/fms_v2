/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.setup.configuration;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.dao.setup.configuration.SupplierDAO;
import com.lcsb.fms.model.setup.configuration.Supplier;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_SupplierInfo", urlPatterns = {"/Path_SupplierInfo"})
public class Path_SupplierInfo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";
            
            switch (process) {
                    case "viewlist":
                        urlsend = "/cf_supplier_list.jsp?id=" + moduleid;
                        break;
                    case "addnew":
                        urlsend = "/cf_supplier_add.jsp?id=" + moduleid;
                        break;
                    case "addprocess": {
                        Supplier sp = new Supplier();
                        sp.setCode(request.getParameter("code"));
                        sp.setVendor(request.getParameter("vendor"));
                        sp.setCompanyname(request.getParameter("companyname"));
                        sp.setCoregister(request.getParameter("coregister"));
                        sp.setBumiputra(request.getParameter("bumiputra"));
                        sp.setHqactdesc(request.getParameter("hqactdesc"));
                        sp.setHqactcode(request.getParameter("hqactcode"));
                        sp.setPerson(request.getParameter("person"));
                        sp.setTitle(request.getParameter("title"));
                        sp.setPosition(request.getParameter("position"));
                        sp.setHp(request.getParameter("hp"));
                        sp.setAddress(request.getParameter("address"));
                        sp.setCity(request.getParameter("city"));
                        sp.setState(request.getParameter("state"));
                        sp.setPostcode(request.getParameter("postcode"));
                        sp.setCountry(request.getParameter("country"));
                        sp.setPhone(request.getParameter("phone"));
                        sp.setFax(request.getParameter("fax"));
                        sp.setEmail(request.getParameter("email"));
                        sp.setUrl(request.getParameter("url"));
                        sp.setBank(request.getParameter("bank"));
                        sp.setBankaccount(request.getParameter("bankaccount"));
                        sp.setPayment(request.getParameter("payment"));
                        sp.setRemarks(request.getParameter("remarks"));
                        sp.setBankdesc(request.getParameter("bankdesc"));
                        sp.setDepositcode(request.getParameter("depositcode"));
                        sp.setDepositdesc(request.getParameter("depositdesc"));
                        sp.setSuspensecode(request.getParameter("suspensecode"));
                        sp.setSuspensedesc(request.getParameter("suspensedesc"));
                        sp.setCoa(request.getParameter("coa"));
                        sp.setCoadescp(request.getParameter("coadescp"));
                        sp.setGstid(request.getParameter("gstid"));
                        if (request.getParameter("transfer").equals("yes")) {

                            SupplierDAO.saveDataToHQ(log, sp);
                            //BuyerDAO.saveData(vn);

                        } else {
                            //BuyerDAO.saveData(vn);
                        }
                        urlsend = "/cf_supplier_list.jsp?id=" + moduleid;
                        break;
                    }
                    case "edit":
                        urlsend = "/cf_supplier_edit.jsp?referenceno=" + request.getParameter("referenceno");
                        break;
                    case "editprocess": {
                        Supplier sp = new Supplier();
                        sp.setCode(request.getParameter("code"));
                        sp.setVendor(request.getParameter("vendor"));
                        sp.setCompanyname(request.getParameter("companyname"));
                        sp.setCoregister(request.getParameter("coregister"));
                        sp.setBumiputra(request.getParameter("bumiputra"));
                        sp.setHqactdesc(request.getParameter("hqactdesc"));
                        sp.setHqactcode(request.getParameter("hqactcode"));
                        sp.setPerson(request.getParameter("person"));
                        sp.setTitle(request.getParameter("title"));
                        sp.setPosition(request.getParameter("position"));
                        sp.setHp(request.getParameter("hp"));
                        sp.setAddress(request.getParameter("address"));
                        sp.setCity(request.getParameter("city"));
                        sp.setState(request.getParameter("state"));
                        sp.setPostcode(request.getParameter("postcode"));
                        sp.setCountry(request.getParameter("country"));
                        sp.setPhone(request.getParameter("phone"));
                        sp.setFax(request.getParameter("fax"));
                        sp.setEmail(request.getParameter("email"));
                        sp.setUrl(request.getParameter("url"));
                        sp.setBank(request.getParameter("bank"));
                        sp.setBankaccount(request.getParameter("bankaccount"));
                        sp.setPayment(request.getParameter("payment"));
                        sp.setRemarks(request.getParameter("remarks"));
                        sp.setBankdesc(request.getParameter("bankdesc"));
                        sp.setDepositcode(request.getParameter("depositcode"));
                        sp.setDepositdesc(request.getParameter("depositdesc"));
                        sp.setSuspensecode(request.getParameter("suspensecode"));
                        sp.setSuspensedesc(request.getParameter("suspensedesc"));
                        sp.setCoa(request.getParameter("coa"));
                        sp.setCoadescp(request.getParameter("coadescp"));
                        SupplierDAO.updateItem(log, sp);
                        urlsend = "/cf_supplier_list.jsp?referenceno=" + request.getParameter("referenceno");
                        break;
                    }
                    case "editpage":
                        urlsend = "/gl_edit_acc.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                        break;
                    case "viewdetail":
                        urlsend = "/cb_chequebook_view.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                        break;
                    case "delete":
                        SupplierDAO.deleteItem(log, request.getParameter("referenceno"));
                        urlsend = "/cf_supplier_list.jsp?id=" + moduleid;
                        break;
                    default:
                        break;
                }
            
            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_SupplierInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_SupplierInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
