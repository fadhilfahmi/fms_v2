/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.setup.configuration;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.dao.setup.configuration.AccrualDAO;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.model.setup.configuration.Accrual;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_AccrualInfo", urlPatterns = {"/Path_AccrualInfo"})
public class Path_AccrualInfo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";
            
            switch (process) {
                case "viewlist":
                    urlsend = "/cf_accrual_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/cf_accrual_add.jsp?id=" + moduleid;
                    break;
                case "addprocess":
                    {
                        Accrual vn = new Accrual();
                        vn.setAddress(request.getParameter("address"));
                        vn.setBank(request.getParameter("bank"));
                        vn.setBankaccount(request.getParameter("bankaccount"));
                        vn.setBankdesc(request.getParameter("bankdesc"));
                        vn.setBumiputra(request.getParameter("bumiputra"));
                        vn.setCity(request.getParameter("city"));
                        vn.setCoa(request.getParameter("coa"));
                        vn.setCoadescp(request.getParameter("coadescp"));
                        vn.setCode(request.getParameter("code"));
                        vn.setCompanyname(request.getParameter("companyname"));
                        vn.setCoregister(request.getParameter("coregister"));
                        vn.setCountry(request.getParameter("country"));
                        vn.setDepositcode(request.getParameter("depositcode"));
                        vn.setDepositdesc(request.getParameter("depositdesc"));
                        vn.setEmail(request.getParameter("email"));
                        vn.setFax(request.getParameter("fax"));
                        vn.setHp(request.getParameter("hp"));
                        vn.setPayment(request.getParameter("payment"));
                        vn.setPerson(request.getParameter("person"));
                        vn.setPhone(request.getParameter("phone"));
                        vn.setPosition(request.getParameter("position"));
                        vn.setPostcode(request.getParameter("postcode"));
                        vn.setRemarks(request.getParameter("remarks"));
                        vn.setState(request.getParameter("state"));
                        vn.setSuspensecode(request.getParameter("suspensecode"));
                        vn.setSuspensedesc(request.getParameter("suspensedesc"));
                        vn.setTitle(request.getParameter("title"));
                        vn.setUrl(request.getParameter("url"));
                        AccrualDAO.saveData(log, vn);
                        urlsend = "/cf_accrual_list.jsp?id=" + moduleid;
                        break;
                    }
                case "edit":
                    urlsend = "/cf_accrual_edit.jsp?referenceno=" + request.getParameter("referenceno");
                    break;
                case "editprocess":
                    {
                        Accrual vn = new Accrual();
                        vn.setAddress(request.getParameter("address"));
                        vn.setBank(request.getParameter("bank"));
                        vn.setBankaccount(request.getParameter("bankaccount"));
                        vn.setBankdesc(request.getParameter("bankdesc"));
                        vn.setBumiputra(request.getParameter("bumiputra"));
                        vn.setCity(request.getParameter("city"));
                        vn.setCoa(request.getParameter("coa"));
                        vn.setCoadescp(request.getParameter("coadescp"));
                        vn.setCode(request.getParameter("code"));
                        vn.setCompanyname(request.getParameter("companyname"));
                        vn.setCoregister(request.getParameter("coregister"));
                        vn.setCountry(request.getParameter("country"));
                        vn.setDepositcode(request.getParameter("depositcode"));
                        vn.setDepositdesc(request.getParameter("depositdesc"));
                        vn.setEmail(request.getParameter("email"));
                        vn.setFax(request.getParameter("fax"));
                        vn.setHp(request.getParameter("hp"));
                        vn.setPayment(request.getParameter("payment"));
                        vn.setPerson(request.getParameter("person"));
                        vn.setPhone(request.getParameter("phone"));
                        vn.setPosition(request.getParameter("position"));
                        vn.setPostcode(request.getParameter("postcode"));
                        vn.setRemarks(request.getParameter("remarks"));
                        vn.setState(request.getParameter("state"));
                        vn.setSuspensecode(request.getParameter("suspensecode"));
                        vn.setSuspensedesc(request.getParameter("suspensedesc"));
                        vn.setTitle(request.getParameter("title"));
                        vn.setUrl(request.getParameter("url"));
                        AccrualDAO.updateItem(log, vn);
                        urlsend = "/cf_accrual_list.jsp?referenceno=" + request.getParameter("referenceno");
                        break;
                    }
                case "editpage":
                    urlsend = "/gl_edit_acc.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "viewdetail":
                    urlsend = "/cb_chequebook_view.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "delete":
                    AccrualDAO.deleteItem(log, request.getParameter("referenceno"));
                    urlsend = "/cf_accrual_list.jsp?id=" + moduleid;
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("100");//nullpointer
                    ErrorIO.setType("data");
                    urlsend = "/error.jsp";
                    break;
            }
            
            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_AccrualInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_AccrualInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
