/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.setup.configuration;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.dao.setup.configuration.ChartofMaterialDAO;
import com.lcsb.fms.general.ErrorIO;
import com.lcsb.fms.model.setup.configuration.ChartofMaterial;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_ChartofMaterial", urlPatterns = {"/Path_ChartofMaterial"})
public class Path_ChartofMaterial extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";
            
            switch (process) {
                case "viewlist":
                    urlsend = "/cf_com_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/cf_com_add.jsp?id=" + moduleid;
                    break;
                case "addprocess":
                    {
                        ChartofMaterial vn = new ChartofMaterial();
                        vn.setAccountcode(request.getParameter("accountcode"));
                        vn.setAccountdescp(request.getParameter("accountdescp"));
                        vn.setActive(request.getParameter("active"));
                        vn.setCode(request.getParameter("code"));
                        vn.setDescp(request.getParameter("descp"));
                        vn.setFinallvl(request.getParameter("finallvl"));
                        vn.setMatgroup(request.getParameter("matgroup"));
                        vn.setType(request.getParameter("type"));
                        vn.setUnitmeasure(request.getParameter("unitmeasure"));
                        ChartofMaterialDAO.saveData(log, vn);
                        urlsend = "/cf_com_list.jsp?id=" + moduleid;
                        break;
                    }
                case "edit":
                    urlsend = "/cf_com_edit.jsp?referenceno=" + request.getParameter("referenceno");
                    break;
                case "editprocess":
                    {
                        ChartofMaterial vn = new ChartofMaterial();
                        vn.setAccountcode(request.getParameter("accountcode"));
                        vn.setAccountdescp(request.getParameter("accountdescp"));
                        vn.setActive(request.getParameter("active"));
                        vn.setCode(request.getParameter("code"));
                        vn.setDescp(request.getParameter("descp"));
                        vn.setFinallvl(request.getParameter("finallvl"));
                        vn.setMatgroup(request.getParameter("matgroup"));
                        vn.setType(request.getParameter("type"));
                        vn.setUnitmeasure(request.getParameter("unitmeasure"));
                        ChartofMaterialDAO.updateItem(log, vn);
                        urlsend = "/cf_com_list.jsp?referenceno=" + request.getParameter("referenceno");
                        break;
                    }
                case "editpage":
                    urlsend = "/gl_edit_acc.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "viewdetail":
                    urlsend = "/cb_chequebook_view.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "delete":
                    ChartofMaterialDAO.deleteItem(log, request.getParameter("referenceno"));
                    urlsend = "/cf_com_list.jsp?id=" + moduleid;
                    break;
                default:
                    ErrorIO.setError(String.valueOf("Error : Invalid Path."));
                    ErrorIO.setCode("100");//nullpointer
                    ErrorIO.setType("data");
                    urlsend = "/error.jsp";
                    break;
            }
            
            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_ChartofMaterial.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_ChartofMaterial.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
