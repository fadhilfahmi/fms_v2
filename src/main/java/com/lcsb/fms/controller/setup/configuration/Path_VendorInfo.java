/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.setup.configuration;

import com.lcsb.fms.controller.PathController;
import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.dao.setup.configuration.VendorDAO;
import com.lcsb.fms.model.setup.configuration.Vendor;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_VendorInfo", urlPatterns = {"/Path_VendorInfo"})
public class Path_VendorInfo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";
            
            switch (process) {
                case "viewlist":
                    urlsend = "/cf_vendor_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/cf_vendor_add.jsp?id=" + moduleid;
                    break;
                case "addprocess":
                    {
                        Vendor vn = new Vendor();
                        vn.setActive(request.getParameter("active"));
                        vn.setBank(request.getParameter("bank"));
                        vn.setBankaccount(request.getParameter("bankaccount"));
                        vn.setBumiputra(request.getParameter("bumiputra"));
                        vn.setCity(request.getParameter("city"));
                        vn.setCountry(request.getParameter("country"));
                        vn.setEmail(request.getParameter("email"));
                        vn.setFax(request.getParameter("fax"));
                        vn.setHp(request.getParameter("hp"));
                        vn.setPayment(request.getParameter("payment"));
                        vn.setPerson(request.getParameter("person"));
                        vn.setPhone(request.getParameter("phone"));
                        vn.setPosition(request.getParameter("position"));
                        vn.setPostcode(request.getParameter("postcode"));
                        vn.setRegisterNo(request.getParameter("register_no"));
                        vn.setRemarks(request.getParameter("remarks"));
                        vn.setState(request.getParameter("state"));
                        vn.setTitle(request.getParameter("title"));
                        vn.setVendorAddress(request.getParameter("vendor_address"));
                        vn.setVendorName(request.getParameter("vendor_name"));
                        VendorDAO.saveData(log, vn);
                        urlsend = "/cf_vendor_list.jsp?id=" + moduleid;
                        break;
                    }
                case "edit":
                    urlsend = "/cf_vendor_edit.jsp?referenceno=" + request.getParameter("referenceno");
                    break;
                case "editprocess":
                    {
                        Vendor vn = new Vendor();
                        vn.setCode(request.getParameter("code"));
                        vn.setActive(request.getParameter("active"));
                        vn.setBank(request.getParameter("bank"));
                        vn.setBankaccount(request.getParameter("bankaccount"));
                        vn.setBumiputra(request.getParameter("bumiputra"));
                        vn.setCity(request.getParameter("city"));
                        vn.setCountry(request.getParameter("country"));
                        vn.setEmail(request.getParameter("email"));
                        vn.setFax(request.getParameter("fax"));
                        vn.setHp(request.getParameter("hp"));
                        vn.setPayment(request.getParameter("payment"));
                        vn.setPerson(request.getParameter("person"));
                        vn.setPhone(request.getParameter("phone"));
                        vn.setPosition(request.getParameter("position"));
                        vn.setPostcode(request.getParameter("postcode"));
                        vn.setRegisterNo(request.getParameter("register_no"));
                        vn.setRemarks(request.getParameter("remarks"));
                        vn.setState(request.getParameter("state"));
                        vn.setTitle(request.getParameter("title"));
                        vn.setVendorAddress(request.getParameter("vendor_address"));
                        vn.setVendorName(request.getParameter("vendor_name"));
                        VendorDAO.updateItem(log, vn);
                        urlsend = "/cf_vendor_list.jsp?referenceno=" + request.getParameter("referenceno");
                        break;
                    }
                case "editpage":
                    urlsend = "/gl_edit_acc.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "viewdetail":
                    urlsend = "/cb_chequebook_view.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "delete":
                    VendorDAO.deleteItem(log, request.getParameter("referenceno"));
                    urlsend = "/cf_vendor_list.jsp?id=" + moduleid;
                    break;
                case "gethq":
                    if (ConnectionUtil.getHQConnection() != null) {
                        ConnectionUtil.closeHQConnection();
                    }
                    ConnectionUtil.createHQConnection();
                    urlsend = "/cf_vendor_gethq.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "listsearch":
                    urlsend = "/cf_vendor_gethq_add.jsp?id=" + moduleid + "&type=" + request.getParameter("type") + "&code=" + request.getParameter("code");
                    break;
                default:
                    break;
            }
            
            Logger.getLogger(PathController.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_VendorInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_VendorInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
