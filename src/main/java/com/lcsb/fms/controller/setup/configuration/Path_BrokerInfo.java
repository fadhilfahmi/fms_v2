/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.controller.setup.configuration;

import com.lcsb.fms.controller.PathModel;
import com.lcsb.fms.dao.setup.configuration.BrokerDAO;
import com.lcsb.fms.model.setup.configuration.Broker;
import com.lcsb.fms.util.model.LoginProfile;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fadhilfahmi
 */
@WebServlet(name = "Path_BrokerInfo", urlPatterns = {"/Path_BrokerInfo"})
public class Path_BrokerInfo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            HttpSession session = request.getSession();
            String moduleid = request.getParameter("moduleid");
            String process = request.getParameter("process");
            LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
            PathModel path = new PathModel();

            String urlsend = "";
            String notify = "";
            
            switch (process) {
                case "viewlist":
                    urlsend = "/cf_broker_list.jsp?id=" + moduleid;
                    break;
                case "addnew":
                    urlsend = "/cf_broker_add.jsp?id=" + moduleid;
                    break;
                case "addprocess":
                    {
                        Broker bk = new Broker();
                        bk.setCode(request.getParameter("code"));
                        bk.setCompanyname(request.getParameter("companyname"));
                        bk.setCoregister(request.getParameter("coregister"));
                        bk.setBumiputra(request.getParameter("bumiputra"));
                        bk.setHqactdesc(request.getParameter("hqactdesc"));
                        bk.setHqactcode(request.getParameter("hqactcode"));
                        bk.setCoa(request.getParameter("coa"));
                        bk.setCoadescp(request.getParameter("coadescp"));
                        bk.setPerson(request.getParameter("person"));
                        bk.setTitle(request.getParameter("title"));
                        bk.setPosition(request.getParameter("position"));
                        bk.setHp(request.getParameter("hp"));
                        bk.setAddress(request.getParameter("address"));
                        bk.setCity(request.getParameter("city"));
                        bk.setState(request.getParameter("state"));
                        bk.setPostcode(request.getParameter("postcode"));
                        bk.setCountry(request.getParameter("country"));
                        bk.setPhone(request.getParameter("phone"));
                        bk.setFax(request.getParameter("fax"));
                        bk.setEmail(request.getParameter("email"));
                        bk.setUrl(request.getParameter("url"));
                        bk.setBank(request.getParameter("bank"));
                        bk.setBankaccount(request.getParameter("bankaccount"));
                        bk.setPayment(request.getParameter("payment"));
                        bk.setRemarks(request.getParameter("remarks"));
                        bk.setBankdesc(request.getParameter("bankdesc"));
                        bk.setActive(request.getParameter("active"));
                        BrokerDAO.saveData(log, bk);
                        urlsend = "/cf_broker_list.jsp?id=" + moduleid;
                        break;
                    }
                case "edit":
                    urlsend = "/cf_broker_edit.jsp?referenceno=" + request.getParameter("referenceno");
                    break;
                case "editprocess":
                    {
                        Broker bk = new Broker();
                        bk.setCode(request.getParameter("code"));
                        bk.setCompanyname(request.getParameter("companyname"));
                        bk.setCoregister(request.getParameter("coregister"));
                        bk.setBumiputra(request.getParameter("bumiputra"));
                        bk.setHqactdesc(request.getParameter("hqactdesc"));
                        bk.setHqactcode(request.getParameter("hqactcode"));
                        bk.setCoa(request.getParameter("coa"));
                        bk.setCoadescp(request.getParameter("coadescp"));
                        bk.setPerson(request.getParameter("person"));
                        bk.setTitle(request.getParameter("title"));
                        bk.setPosition(request.getParameter("position"));
                        bk.setHp(request.getParameter("hp"));
                        bk.setAddress(request.getParameter("address"));
                        bk.setCity(request.getParameter("city"));
                        bk.setState(request.getParameter("state"));
                        bk.setPostcode(request.getParameter("postcode"));
                        bk.setCountry(request.getParameter("country"));
                        bk.setPhone(request.getParameter("phone"));
                        bk.setFax(request.getParameter("fax"));
                        bk.setEmail(request.getParameter("email"));
                        bk.setUrl(request.getParameter("url"));
                        bk.setBank(request.getParameter("bank"));
                        bk.setBankaccount(request.getParameter("bankaccount"));
                        bk.setPayment(request.getParameter("payment"));
                        bk.setRemarks(request.getParameter("remarks"));
                        bk.setBankdesc(request.getParameter("bankdesc"));
                        bk.setActive(request.getParameter("active"));
                        BrokerDAO.updateItem(log, bk);
                        urlsend = "/cf_broker_list.jsp?referenceno=" + request.getParameter("referenceno");
                        break;
                    }
                case "editpage":
                    urlsend = "/gl_edit_acc.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "viewdetail":
                    urlsend = "/cb_chequebook_view.jsp?id=" + moduleid + "&referenceno=" + request.getParameter("referenceno");
                    break;
                case "delete":
                    BrokerDAO.deleteItem(log, request.getParameter("referenceno"));
                    urlsend = "/cf_broker_list.jsp?id=" + moduleid;
                    break;
                default:
                    break;
            }
            
            Logger.getLogger(Path_ChartofAccount.class.getName()).log(Level.INFO, "--------redirect to>>>" + urlsend);
            RequestDispatcher dispatcher = request.getRequestDispatcher(urlsend);
            dispatcher.forward(request, response);
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_BrokerInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Path_BrokerInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
