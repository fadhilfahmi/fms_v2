/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lcsb.fms.report.financial;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Dell
 */
public class ProfitLoss {
    
    private Connection con;
    
    public ProfitLoss (Connection con){
        this.con=con;
    }
    
    public ArrayList getCOA(){
        ArrayList<String> x;
        x = new ArrayList<String>();
        x.add("20");
        x.add("22");
        return x;
        
    } public ArrayList getTaxCode(){
        ArrayList<String> x;
        x = new ArrayList<String>();
        x.add("2908");
        x.add("135107");
                
        return x;
    }
    
    public ArrayList AnalyseCOA(String code,int main) throws SQLException {
        ArrayList<String> x;
        x = new ArrayList<String>();
        boolean check;
        
        String ext = "";
        if(main==1){
            ext = "select * from chartofacccount where code like '"+code+"%'  and code <> '"+code+"'";
        }else if(main==2){
            
           /* if(filterCode(code)==true){
                 ext = "select * from chartofacccount where code like '"+code+"%' and code <> '"+code+"'";
            }else{
                
                 ext = "select * from chartofacccount where code like '"+code+"%' and code <> '"+code+"' and length(code) = '"+getMaxLength(code)+"'";
            }*/
           
        }
        
        try{
        Statement statement;
        statement = con.createStatement();
        ResultSet rs = statement.executeQuery(ext);
         //stmt.setString(1, code);
         while (rs.next()) {
             String m = rs.getString("code");
              x.add(m);
         }
        rs.close();
        statement.close();
        }
          catch (SQLException e) {
              
            }
       
        
        return x;
    }
    
    public Double getTotal(String year,String period,String otype, String vby, String code, String time){
        Double total = 0.00;
        
        String table = "";
        if(otype.equalsIgnoreCase("1"))	
	{
            if(time.equalsIgnoreCase("now")){
                table=" from gl_openingbalance ";
            }else if(time.equalsIgnoreCase("then")){
                table=" from gl_closingbalance ";
                year=String.valueOf(Integer.parseInt(year)-1);
            }
            else if(time.equalsIgnoreCase("thenend")){
                table=" from gl_closingbalance ";
                year=String.valueOf(Integer.parseInt(year)-1);
                period="12";
            }
            
            
	}
	else if(otype.equalsIgnoreCase("2"))	
	{
             if(time.equalsIgnoreCase("now")){
                table=" from gl_openingbalance_detail ";
            }else if(time.equalsIgnoreCase("then")){
                table=" from gl_openingbalance_detail ";
                year=String.valueOf(Integer.parseInt(year)-1);
            }
             else if(time.equalsIgnoreCase("thenend")){
                table=" from gl_openingbalance_detail ";
                year=String.valueOf(Integer.parseInt(year)-1);
                period="12";
            }
	}
        
        try{
        Statement statement;
        statement = con.createStatement();
        ResultSet rs = statement.executeQuery("select (ifnull(sum(debit),0) - ifnull(sum(credit),0)) as total  "+table+" where concat(year,period)='"+year.concat(period)+"' and coacode like '"+code+"%'");
         while (rs.next()) {
           total = rs.getDouble("total");
         }
         
         statement.close();
         rs.close();
        }
          catch (SQLException e) {
              
            }
        
        return total;
    }
    
    public double getSales(String year,String period,String acccode, String type, String range) throws Exception {
        
       
        
        String location="";
        String table = "";
        String periodRange = "";
        String columnsyntax = "";
        double sumsales=0;
        
        if(type.equalsIgnoreCase("actual")){
            
            columnsyntax = "ifnull(credit-debit,0) as sumsales";
            
            if(range.equalsIgnoreCase("thismonth")){
                table = "gl_openingbalance_detail";
                periodRange = "concat(year,period)=concat('"+year+"','"+period+"')";
            }else if(range.equalsIgnoreCase("todate")){
                table = "gl_closingbalance";
                periodRange = "concat(year,period)=concat('"+year+"','"+period+"')";
            }else if(range.equalsIgnoreCase("prevyear")){
                int pyear=Integer.parseInt(year);
		pyear--;
		year=String.valueOf(pyear);
                
                table = "gl_closingbalance";
                periodRange = "concat(year,period)=concat('"+year+"','"+period+"')";
            }else if(range.equalsIgnoreCase("prevallyear")){
                int pyear=Integer.parseInt(year);
		pyear--;
		year=String.valueOf(pyear);
                
                table = "gl_closingbalance";
                periodRange = "concat(year,period)=concat('"+ year +"','12')";
            }    
            
        }else if(type.equalsIgnoreCase("budget")){
            
             columnsyntax = "ifnull(amount,0) as sumsales";
             
             table = "gl_budget";
             if(range.equalsIgnoreCase("thismonth")){
                periodRange = "concat(year,period)=concat('"+year+"','"+period+"')";
            }else if(range.equalsIgnoreCase("todate")){
                periodRange = "year='"+ year +"' and (period between 1 and "+ period +")";
            }else if(range.equalsIgnoreCase("year")){
                periodRange = "year='"+ year +"'";
            }  
             
        }
        
        Statement stment = con.createStatement();
        //test+="select ifnull(credit-debit,0) as sumsales from gl_openingbalance_detail as t1 left join product_info as t2 on t2.entercode=right(t1.loccode,2) left join enterprise_info as t3 on t3.code=right(t1.loccode,2) where t1.coacode like '"+ acccode +"%' "+ location +" and concat(year,period)=concat('"+ year +"','"+ period +"')  "+ tempt1 +" "+ tempt2 +" "+ syarat +" group by t1.itemid<br>";
        //ResultSet setent =stment.executeQuery("select ifnull(credit-debit,0) as sumsales from gl_openingbalance_detail as t1 left join product_info as t2 on t2.entercode=right(t1.loccode,2) left join enterprise_info as t3 on t3.code=right(t1.loccode,2) where t1.coacode like '"+ acccode +"%' "+ location +" and concat(year,period)=concat('"+ year +"','"+ period +"')  "+ tempt1 +" "+ tempt2 +" "+ syarat +" group by t1.itemid");
        ResultSet setent =stment.executeQuery(" select "+columnsyntax+" from "+table+" where coacode like '"+acccode+"%' and "+periodRange+"  group by itemid");
       
        while(setent.next()) {
            sumsales+=setent.getDouble("sumsales");
        }
        
        stment.close();
        setent.close();
        return sumsales;
    }
    
     public int getLength(String code){
        int x = 0;
        
        try{
        Statement statement;
        statement = con.createStatement();
        ResultSet rs = statement.executeQuery("SELECT LENGTH(code) as length FROM chartofacccount where code = '"+code+"'");
         //stmt.setString(1, code);
            if (rs.next()) {
                x = rs.getInt("length");
            }
        statement.close();
        rs.close();
        }catch (SQLException e) {
              
        }
        return x;
    }
     
    public int haveChild(String code){
        int x = 0;
        
        try{
        Statement statement;
        statement = con.createStatement();
        ResultSet rs = statement.executeQuery("SELECT count(*) as cnt FROM chartofacccount where code like '"+code+"%' and code <> '"+code+"'");
         //stmt.setString(1, code);
            if (rs.next()) {
                x = rs.getInt("cnt");
            }
        statement.close();
        rs.close();
        }catch (SQLException e) {
              
        }
        return x;
    }
    
}
