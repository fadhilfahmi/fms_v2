/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dashboard;

import com.lcsb.fms.model.financial.gl.JournalVoucherItem;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author fadhilfahmi
 */
public class VoucherSummary {
    
    private int jvApproval;
    private int pnvApproval;
    private int pvApproval;
    private int orApproval;

    public int getOrApproval(LoginProfile log) throws Exception {
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from cb_official where appid='' and checkid<>''");
        //stmt.setString(1, Jvid);
        rs = stmt.executeQuery();
        if (rs.next()) {
            orApproval = rs.getInt("cnt");
        }
        return orApproval;
    }

    public int getPvApproval(LoginProfile log) throws Exception {
        int cnt = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from cb_payvoucher where appid='' and checkid<>''");
        //stmt.setString(1, Jvid);
        rs = stmt.executeQuery();
        if (rs.next()) {
            pvApproval = rs.getInt("cnt");
        }
        return pvApproval;
    }

    public int getPnvApproval(LoginProfile log) throws Exception {
        int cnt = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from ap_inv where appid='' and checkid<>''");
        //stmt.setString(1, Jvid);
        rs = stmt.executeQuery();
        if (rs.next()) {
            pnvApproval = rs.getInt("cnt");
        }
        return pnvApproval;
    }

    public int getJvApproval(LoginProfile log) throws Exception {
        int cnt = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from gl_jv where approvebyid='' and checkbyid<>''");
        //stmt.setString(1, Jvid);
        rs = stmt.executeQuery();
        if (rs.next()) {
            jvApproval = rs.getInt("cnt");
        }
        return jvApproval;
    }

    public void setJvApproval(int jvApproval) {
        
        this.jvApproval = jvApproval;
    }
    
    public int getINVApproval(LoginProfile log) throws Exception {
        int cnt = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from sl_inv where aid='' and cid<>''");
        //stmt.setString(1, Jvid);
        rs = stmt.executeQuery();
        if (rs.next()) {
            jvApproval = rs.getInt("cnt");
        }
        return jvApproval;
    }
    
    public int getCNRApproval(LoginProfile log) throws Exception {
        int cnt = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from ar_creditnote where appid='' and checkid<>''");
        //stmt.setString(1, Jvid);
        rs = stmt.executeQuery();
        if (rs.next()) {
            jvApproval = rs.getInt("cnt");
        }
        return jvApproval;
    }
    
    public int getDNRApproval(LoginProfile log) throws Exception {
        int cnt = 0;
        ResultSet rs = null;
        PreparedStatement stmt = log.getCon().prepareStatement("select count(*) as cnt from ar_debitnote where appid='' and checkid<>''");
        //stmt.setString(1, Jvid);
        rs = stmt.executeQuery();
        if (rs.next()) {
            jvApproval = rs.getInt("cnt");
        }
        return jvApproval;
    }


    
}
