/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.dashboard;

import com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.AutoGenerate;
import com.lcsb.fms.model.financial.cashbook.CbOfficialCek;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fadhilfahmi
 */
public class DashboardDonutUsageDAO {
    
    public static void saveUsage(LoginProfile log, String moduleid, String refer, String process) throws Exception {

        try {
            String q = ("insert into dashboard_donut_usage(userid,moduleid,date,time,refer,type) values (?,?,?,?,?,?)");
            PreparedStatement ps = log.getCon().prepareStatement(q);

            ps.setString(1, log.getUserID());
            ps.setString(2, moduleid);
            ps.setString(3, AccountingPeriod.getCurrentTimeStamp());
            ps.setString(4, AccountingPeriod.getCurrentTime());
            ps.setString(5, refer);
            ps.setString(6, process);

            ps.executeUpdate();
            Logger.getLogger(OfficialReceiptDAO.class.getName()).log(Level.INFO, String.valueOf(ps));
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
}
