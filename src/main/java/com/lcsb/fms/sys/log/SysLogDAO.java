/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lcsb.fms.sys.log;

import com.lcsb.fms.dao.administration.om.UserDAO;
import com.lcsb.fms.general.AccountingPeriod;
import com.lcsb.fms.general.DateAndTime;
import com.lcsb.fms.util.dao.ConnectionUtil;
import com.lcsb.fms.util.dao.ModuleDAO;
import com.lcsb.fms.util.model.LoginProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ocpsoft.prettytime.PrettyTime;

/**
 *
 * @author fadhilfahmi
 */
public class SysLogDAO {

    public static void saveLog(LoginProfile log, SysLog item) throws Exception {


        if (item.isLog()) {
            try {
                
                java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
                
                String q = ("insert into sys_log(moduleid,userid,referno,process,date,time,remark,ip, hostname) values (?,?,?,?,?,?,?,?,?)");
                PreparedStatement ps = log.getCon().prepareStatement(q);
                ps.setString(1, item.getModuleid());
                ps.setString(2, item.getUserid());
                ps.setString(3, item.getReferno());
                ps.setString(4, item.getProcess());
                ps.setString(5, item.getDate());
                ps.setString(6, item.getTime());
                ps.setString(7, item.getRemark());
                ps.setString(8, item.getIp());
                ps.setString(9, String.valueOf(localMachine));
                ps.executeUpdate();

                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public static String viewLog(LoginProfile log, int page_number) throws Exception {

        int item_per_page = 25;
        int position = ((page_number - 1) * item_per_page);

        String output = "";
        //output+="<strong>SELECT COACode, COADesc, Remark FROM gl_posting_distribute ORDER BY id DESC LIMIT " + position + ", " + item_per_page + "</strong>";
        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM sys_log ORDER BY id DESC LIMIT ?, ?");
            stmt.setInt(1, position);
            stmt.setInt(2, item_per_page);
            rs = stmt.executeQuery();
            while (rs.next()) {

                //PrettyTime p = new PrettyTime();
                //p.format(rs.getDate("date"));
                output += "<a href=\"#\" class=\"list-group-item\">\n"
                        + getIcon(rs.getString("remark"))+ "&nbsp;<strong>" + ModuleDAO.getModule(log, rs.getString("moduleid")).getModuleDesc() + "</strong> , " + rs.getString("referno") + " , " + rs.getString("remark") + "\n"
                        + "                <span class=\"pull-right text-muted small\"><em>" + rs.getDate("date") + " " + rs.getString("time") + " by <strong>" + UserDAO.getInfoByStaffID(log, rs.getString("userid")).getStaffName() + "</strong></em>\n"
                        + "                </span>\n"
                        + "            </a>";

            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return output;

    }

    private static String getIcon(String action) {

        String icon = "";

        if(action.toLowerCase().indexOf("delete".toLowerCase()) != -1) {

            icon = "<i class=\"fa fa-trash\" aria-hidden=\"true\"  style=\"color: #d61734;\"></i> ";

        }else if (action.toLowerCase().indexOf("update".toLowerCase()) != -1) {

            icon = "<i class=\"fa fa-pencil-square\" aria-hidden=\"true\" style=\"color: #db8e13;\"></i> ";

        }else if (action.toLowerCase().indexOf("approve".toLowerCase()) != -1) {

            icon = "<i class=\"fa fa-check-square\" aria-hidden=\"true\" style=\"color: #00a341;\"></i> ";

        }else if (action.toLowerCase().indexOf("period".toLowerCase()) != -1) {

            icon = "<i class=\"fa fa-calendar\" aria-hidden=\"true\"></i> ";

        }else if (action.toLowerCase().indexOf("add".toLowerCase()) != -1) {

            icon = "<i class=\"fa fa-plus-square\" aria-hidden=\"true\" style=\"color: #1387db;\"></i> ";

        }else if (action.toLowerCase().indexOf("check".toLowerCase()) != -1) {

            icon = "<i class=\"fa fa-check-square-o\" aria-hidden=\"true\" style=\"color: #00a341;\"></i> ";

        }else if (action.toLowerCase().indexOf("account finalize".toLowerCase()) != -1) {

            icon = "<i class=\"fa fa-calculator\" aria-hidden=\"true\"></i> ";

        }else if (action.toLowerCase().indexOf("replicated".toLowerCase()) != -1) {

            icon = "<i class=\"fa fa-files-o\" aria-hidden=\"true\"></i> ";

        }else if (action.toLowerCase().indexOf("printed".toLowerCase()) != -1) {

            icon = "<i class=\"fa fa-file-text-o\" aria-hidden=\"true\"></i> ";

        }else if (action.toLowerCase().indexOf("canceled".toLowerCase()) != -1) {

            icon = "<i class=\"fa fa-ban\" aria-hidden=\"true\"></i> ";

        }else {
            icon = "<i class=\"fa fa-comment fa-fw\"></i> ";
        }

        
        return icon;

    }
    
    public static String viewLoginLog(LoginProfile log, int page_number) throws Exception {

        int item_per_page = 25;
        int position = ((page_number - 1) * item_per_page);

        String output = "";
        //output+="<strong>SELECT COACode, COADesc, Remark FROM gl_posting_distribute ORDER BY id DESC LIMIT " + position + ", " + item_per_page + "</strong>";
        try {
            ResultSet rs = null;
            PreparedStatement stmt = log.getCon().prepareStatement("SELECT * FROM sys_log_login ORDER BY id DESC LIMIT ?, ?");
            stmt.setInt(1, position);
            stmt.setInt(2, item_per_page);
            rs = stmt.executeQuery();
            while (rs.next()) {
                String style = "";
                String font = "";
                if(AccountingPeriod.convertDatetoString(rs.getDate("date")).equals(AccountingPeriod.getCurrentTimeStamp())){
                    style = "style=\"background-color:#4cad73\"";
                    font = "style=\"color:#f4f9f6\"";
                }

                //PrettyTime p = new PrettyTime();
                //p.format(rs.getDate("date"));
                output += "<a class=\"list-group-item\" "+style+">\n"
                        + "&nbsp; <font "+font+">" +rs.getString("userid")+" - " +UserDAO.getInfoByStaffID(log, rs.getString("userid")).getStaffName() + "\n"
                        + "                </font><span class=\"pull-right text-muted\"><font "+font+">"+ AccountingPeriod.fullDateMonth(AccountingPeriod.convertDatetoString(rs.getDate("date"))) + " , " + DateAndTime.addTime(rs.getString("time")) + "</strong>\n"
                        + "                </font></span>\n"
                        + "            </a>";

            }

            rs.close();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return output;

    }

}
