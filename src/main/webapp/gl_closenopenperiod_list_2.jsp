<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%response.setHeader("Cache-Control", "no-cache");

%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style1 {font-family: Arial, Helvetica, sans-serif}
-->
</style>
</head>
 <style type="text/css">

#blueblock{
width: 300px;
border-right: 1px solid #000;
padding: 0 0 1em 0;
margin-bottom: 1em;
font-family: 'Trebuchet MS', 'Lucida Grande', Arial, sans-serif;
font-size: 90%;
background-color: #90bade;
color: #333;
}

* html #blueblock{ /*IE 6 only */
w\idth: 367px; /*Box model bug: 180px minus all left and right paddings for #blueblock */
}

#blueblock ul{
list-style: none;
margin: 0;
padding: 0;
border: none;
}

#blueblock li {
border-bottom: 1px solid #90bade;
margin: 0;
}

#blueblock li a{
display: block;
padding: 5px 5px 5px 8px;
border-left: 10px solid #1958b7;
border-right: 10px solid #508fc4;
background-color: #2175bc;
color: #fff;
text-decoration: none;
width: 100%;
}

html>body #blueblock li a{ /*Non IE6 width*/
width: auto;
}

#blueblock li a:hover{
border-left: 10px solid #1c64d1;
border-right: 10px solid #5ba3e0;
background-color: #2586d7;
color: #fff;
}

 </style>

<body>
<div align="center"><br>
  <font size="5" face="Arial, Helvetica, sans-serif">CLOSED AND OPEN NEW PERIOD STEP LIST</font><br>
  <br>
</div>
<table border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td colspan="2"><div align="center" class="style1">Verify Voucher And Trial-Closing</div></td>
  </tr>
  
  <tr> 
    <td colspan="2">
<div id="blueblock">
	<ul> 	
		<li><a href="gl_pre_post.jsp" target="mainFrame">1. Pre - Posting</a></li>
		<li><a href="gl_pre_gllisting.jsp" target="mainFrame">2. Pre - General Ledger Listing</a></li>
		<li><a href="gl_pre_trialbalance.jsp" target="mainFrame">3. Pre - Trial Balance</a></li>
	</ul>
</div>    </td>
  </tr>
  <tr> 
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="2"><div align="center" class="style1">CLOSE AND OPEN NEW PERIOD</div></td>
  </tr>
  <tr bgcolor="#FFFF00"> 
    <td><div align="center" class="style1"><font color="#FF0000">*</font></div></td>
    <td><span class="style1">Please Backup Now Before Proceed</span></td>
  </tr>
  <tr> 
    <td colspan="2">
<div id="blueblock">
	<ul> 	
		<li><a href="gl_closedmodule.jsp" target="_self">1. Closed Module</a></li>
		<li><a href="st_stockbalance_close.jsp" target="_self">2. Close Stock </a></li>
	</ul>
</div>	</td>
  </tr>
  <tr bgcolor="#FFFF00">
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="2">
<div id="blueblock">
	<ul> 		
	<li><a href="gl_machinery_dist_cost.jsp" target="_self">3. Distribute Machinery And Workshop Cost</a> </li>
	<%--<li><a href="gl_distribute_ent_cost.jsp" target="_self">4. Distribute Enterprise Cost</a></li>--%>
	<%--<li><a href="gl_distribute_loc_cost.jsp">5. Distribute Location Cost</a></li>--%>
	<li><a href="gl_alloc_gc.jsp">6. Allocate General Charges Cost</a></li>
	<li><a href="gl_predebitcreditnote.jsp" target="_self">7. Prepare Estate's Debit And Credit Note</a></li>
	<%--<li><a href="gl_transferlivestock_cost.jsp" target="_self">8. Transfer Livestock Cost</a></li>--%>
	<li><a href="gl_finalizing_acc_data.jsp" target="_self">9. Finalizing Accounting Data</a></li>
	</ul>
</div>	
	</td>
  </tr>
  <tr bgcolor="#FFFF00"> 
    <td><div align="center" class="style1"><font color="#FF0000">*</font></div></td>
    <td><span class="style1">Please Backup Again</span></td>
  </tr>
  <tr> 
    <td colspan="2">
<div id="blueblock">
	<ul> 	
		<li><a href="gl_closenopenperiod.jsp" target="_self">10. Confirm Close &amp; Create New Period</a></li>
	</ul>
</div>	</td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
