<%-- 
    Document   : testpage
    Created on : Dec 2, 2016, 10:09:31 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.setup.configuration.LotMemberDAO"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.logging.Level"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFCell"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFRow"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFSheet"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@page import="org.apache.poi.poifs.filesystem.POIFSFileSystem"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.BufferedInputStream"%>
<%@page import="com.lcsb.fms.util.ext.ReadExcelFile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/pvoucher.css?1" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div>
            <%
                Connection con = ConnectionUtil.getConnection();
                LotMemberDAO.transferDataLot();
                
            %>
        </div>
    </body>
</html>
