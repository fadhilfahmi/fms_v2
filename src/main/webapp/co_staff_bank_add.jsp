<%-- 
    Document   : co_staff_designation_add
    Created on : Apr 6, 2017, 10:32:08 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.setup.company.staff.Staff"%>
<%@page import="com.lcsb.fms.dao.setup.company.StaffDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) StaffDAO.getModule();
    Staff v = (Staff) StaffDAO.getInfo(log, request.getParameter("refer"));

%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $('#tab_content .datepicker').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        });


        $('#backtolist').click(function (e) {
            var path = $(this).attr('title');
            var process = $(this).attr('name');
            var refer = $(this).attr('type');
            $.ajax({
                url: "PathController?moduleid=" + path + "&process=" + process + "&refer=" + refer,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#tab_content').empty().html(result).hide().fadeIn(100);
                }
            });
            return false;
        });

        
        $('#getbankgeneral').click(function (e) {

            var a = 'bankcode';
            var b = 'bankname';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Bank',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_bank_general.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });
    });

</script>

<div class="col-sm-6">
    <p></p>
    <div class="row">
        <div class="col-sm-12">
            <span class="pull-left">
                <button id="backtolist" class="btn btn-default btn-xs" title="<%= mod.getModuleID()%>" name="viewbank" type="<%= v.getStaffid()%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                <button id="savebutton"  class="btn btn-default btn-xs" title="<%= mod.getModuleID()%>" name="addbankprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>

        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <div class="well">
                <form data-toggle="validator" role="form" id="saveform">
                    <input type="hidden" name="staffid" value="<%= v.getStaffid()%>">
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Bank Code&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="bankcode" name="bankcode" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="getbankgeneral"><i class="fa fa-cog"></i> Get Bank</button>
                                    </span>
                                </div>    
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Bank Name &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="bankname" name="bankdesc" placeholder="" autocomplete="off" >   
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Account Number&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="accno" name="accno" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Status Active?<span id="res_code"></span></label>
                                <select class="form-control input-sm" id="status" name="status">
                                    <%= ParameterDAO.parameterList(log, "YesNo Type", "")%>
                                </select>
                            </div>
                        </div>
                    </div>
                    

                </form>

            </div>
        </div>
    </div>
</div>


