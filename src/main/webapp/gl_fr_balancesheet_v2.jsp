
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.dao.financial.gl.FinancialReportDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) FinancialReportDAO.getModule();
%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $("#viewreport").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewreport&vby=" + $('#vby').val() + "&year=" + $('#year').val() + "&period=" + $('#period').val() + "&otype=" + $('#otype').val(),
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });

        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });


    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
                </tr>
            </table>
            <br>
            <label>Balance Sheet</label>
            <form data-toggle="validator" role="form" id="saveform">
                <div class="row">
                    <div class="col-lg-10">
                        <div class="well">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="inputName" class="control-label">Year</label>
                                    <div class="form-group">
                                        <select class="form-control input-sm" id="year" name="year">
                                            <%= ParameterDAO.parameterList(log,"Year", AccountingPeriod.getCurYear(log))%>
                                        </select>
                                    </div>   
                                </div>
                                <div class="col-sm-3">
                                    <label for="inputName" class="control-label">Period</label>
                                    <select class="form-control input-sm" id="period" name="period">
                                        <%= ParameterDAO.parameterList(log,"Period", AccountingPeriod.getCurPeriod(log))%>
                                    </select>  
                                </div>
                                <div class="col-sm-3">
                                    <label for="inputName" class="control-label">Type</label>
                                    <select class="form-control input-sm" id="otype" name="otype">
                                        <%= ParameterDAO.parameterList(log,"Balance Sheet Type", "To Date")%>
                                    </select>  
                                </div>
                                <div class="col-sm-3">
                                    <label for="inputName" class="control-label">Type</label>
                                    <select class="form-control input-sm" id="vby" name="vby">
                                        <!--<option value="1">Level 1</option>-->
                                        <option value="2" selected>Level 2</option>
                                        <option value="3">Level 3</option>
                                    </select>  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label for="inputName" class="control-label">View By Location</label>
                                    <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="loclevel" value="Company">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-sm" type="button" id="getlocation"><i class="fa fa-cog"></i> Location Level</button>
                                        </span>
                                    </div>   
                                </div>
                                <div class="col-sm-4">
                                    <label for="inputName" class="control-label">Location Code</label>
                                    <input type="text" class="form-control input-sm" id="loccode" value="<%= log.getEstateCode()%>"> 
                                </div>
                                <div class="col-sm-4">
                                    <label for="inputName" class="control-label">Location Name</label>
                                    <input type="text" class="form-control input-sm" id="locdesc" value="<%= log.getEstateDescp()%>"> 
                                </div>
                            </div> 

                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group-sm input-group">
                                    
                                </div>   
                            </div>
                            <div class="col-sm-8">  
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group-sm input-group pull-right">
                                    <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="generateBS">Generate Report</button>
                                </div>   
                            </div>
                        </div>   
                    </div>
                </div>
            </form>



            <br>
            <!--       <div class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons " tabindex="-1" role="dialog" aria-describedby="checkjv_dialog" aria-labelledby="ui-id-1" style="position: absolute; height: auto; width: 500px; top: 0px; display: block;  margin: 0 auto;"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"><span id="ui-id-1" class="ui-dialog-title">Balance Sheet Report</span></div><div id="checkjv_dialog" class="ui-dialog-content ui-widget-content" style="display: block; width: auto; min-height: 0px; max-height: none; height: 300px;">
              <form name="" action="gl_fr_balancesheet_v2_view.jsp" method="post">     
             <table id="maintbl" width="100%" cellspacing="0" cellpadding="5"><tbody>
             
             <tr><td>Year</td><td><input name="year" type="text" id="year" size="20" value="<%= AccountingPeriod.getCurYear(log)%>"></td></tr>
             <tr><td>Period</td><td><select name="period" id="period">
                                           <option value="<%//=(String)session.getAttribute("period")%>" selected="selected"><%//=(String)session.getAttribute("period")%></option>
                     <option value="1" >1</option>
                     <option value="2">2</option>
                     <option value="3">3</option>
                     <option value="4">4</option>
                     <option value="5">5</option>
                     <option value="6">6</option>
                     <option value="7">7</option>
                     <option value="8">8</option>
                     <option value="9">9</option>
                     <option value="10">10</option>
                     <option value="11">11</option>
                     <option value="12">12</option>
                   </select></td></tr>
             <tr><td>View By</td><td><select name="vby" id="vby" onChange="tukar()">
                     <option value="1">Summary</option>
                     <option value="2">Detail</option>
                   </select>	</td></tr>
                   
                  
                   
                   <tr><td>Corporation</td><td>  <select name="loccode2" id="loccode2">
            <%
                Statement stmest = log.getCon().createStatement();
                ResultSet setest = stmest.executeQuery("select code,name from ce_estate order by code");
                while (setest.next()) {
                    out.println("<option value=\"" + setest.getString("code") + "\">" + setest.getString("code") + " - " + setest.getString("name") + "</option>");
                }
            %>
      </select>		</td></tr>
    <tr><td>Type</td><td><select name="otype" id="otype">
      <option value="1" selected>Opening Balance</option>
      <option value="2">This Month</option>
      <option value="3" >To Date</option>
    </select>	</td></tr>
    <tr><td>Page</td><td><input name="pg" type="text" id="pg" value=""></td></tr>
    <tr><td></td><td>
              <input type="button" name="Submit3" onClick="location.href='gl_fr_managercomment_appendix.jsp'" value="<<">
                    <input type="button" name="Button" id="viewreport" value="Generate Report">
                    <input type="button" name="Submit4" onClick="location.href='gl_Financial_Report.jsp'" value="Return To Index">
                    
                    <input type="hidden" name="view" value="view"></td></tr>

</tbody></table>
</form>
</div><div class="ui-resizable-handle ui-resizable-n" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-w" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-sw" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-ne" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-nw" style="z-index: 90;"></div></div>
            -->

        </div>
    </div>
</div>

