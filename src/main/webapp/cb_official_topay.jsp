<%-- 
    Document   : ap_inv_topay
    Created on : Oct 19, 2016, 9:55:45 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoice"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    String[] values = request.getParameterValues("invno");

%>
<label>&nbsp;<i class="fa fa-info-circle" aria-hidden="true"></i> Confirm the amount to pay. </label>
<form data-toggle="validator" role="form" id="saveamount">
    <table class="table table-bordered table-striped table-hover" id="list1">
        <thead>
            <tr>
                <th></th>
                <th>Invoice No</th>
                <th>Date</th>
                <th>Payer</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            <%
            int r = 0;
            for (int i = 0; i < values.length - 1; i++) {
                r++;
                SalesInvoice vi = (SalesInvoice) SalesInvoiceDAO.getNoPaidINVEach(log,values[i]);%>
            <tr id="<%//= j.getInvrefno()%>" class="activerowy">
                <td width="3%"><%=i + 1%></td>
                <td class="tdrow"><%= vi.getInvref()%></td>
                <td class="tdrow"><%= vi.getInvdate()%></td>
                <td class="tdrow"><%= vi.getBcode()%> - <%= vi.getBname()%></td>
                <td class="tdrow">
                    <input type="hidden" name="invrefno<%=r%>" value="<%= vi.getInvref()%>">
                    <input type="text" class="form-control input-sm" id="amount<%=r%>" name="amount<%=r%>" value="<%= GeneralTerm.currencyFormat((vi.getAmountno()) - vi.getPaid()) %>" required></td>
            </tr>
            <%}%>
        <input type="hidden" name="totalrow" value="<%=r%>">
        </tbody>
    </table>
</form>