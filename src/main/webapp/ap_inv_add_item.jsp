<%-- 
    Document   : ap_inv_add_item
    Created on : May 13, 2016, 12:01:58 PM
    Author     : user
--%>

<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.dao.financial.ap.VendorInvoiceDAO"%>
<%@page import="com.lcsb.fms.model.financial.ap.VendorInvoice"%>
<%@page import="com.lcsb.fms.model.financial.ap.VendorInvoiceDetail"%>

<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    VendorInvoiceDetail v = (VendorInvoiceDetail) VendorInvoiceDAO.getItem(request.getParameter("refer"));
    VendorInvoice vm = (VendorInvoice) VendorInvoiceDAO.getPNV(log, request.getParameter("refer"));
    Module mod = (Module) VendorInvoiceDAO.getModule();
%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<link href="css/table_style.css" rel="stylesheet" type="text/css" />
<link href="css/pvoucher.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="jsfunction/tether.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>

<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });

        

        $('.getmaterial').click(function (e) {
            var a = $(this).attr('id');
            var b = $(this).attr('id1');
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Material',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_material.jsp?code=' + a + '&descp=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getsub').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Sub Account Detail',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_sub.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#gettax').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Taxation',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_tax.jsp');
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        var a = $('#amount').val();
                        a = a.replace(/\,/g, '');
                        var b = $('#taxrate').val();
                        var c = parseFloat(b) * parseFloat(a) / 100;
                        $('#taxamt').val(parseFloat(c).toFixed(2));


                        event.preventDefault();
                    });

                    return $content;
                }
            });



            return false;
        });

        $('.calculate').keyup(function (e) {
            var todeduct = $(this).val();
            var theval = $(this).attr('id');


            if (todeduct != '') {
                var tot = 0;
                if (theval == 'debit') {
                    $('#credit').val(0.0);
                }
                if (theval == 'credit') {
                    $('#debit').val(0.0);
                }

            }

        });

        $('.totalup').keyup(function (e) {

            var qty = $('#qty').val();
            var unitp = $('#unitp').val();
            var taxrate = $('#taxrate').val();

            var total = qty * unitp;
            var taxamt = taxrate * total / 100;
            $('#taxamt').val(taxamt);
            $('#amount').val(total);

        });

        $('.calculate_gst').change(function (e) {
            alert(9);
            var amt = $('#amount').val();
            var taxrate = $('#taxrate').val();

            var total = taxrate * 100 / amt;

            $('#taxamt').val(total);

        });

        $('.form-control').focusout(function (e) {
            if (($('#debit').val() == 0.00) || ($('#loccode').val() == '') || ($('#actdesc').val() == '') || ($('#remarks').val() == '')) {
                $('#savebutton').prop('disabled', true);
            } else {
                $('#savebutton').prop('disabled', false);
            }
        });

        $('#getproduct').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Product Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_product.jsp?name=prodname&code=prodcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('.calc_amount').keyup(function (e) {
            var qty = $('#quantity').val();
            var unitp = $('#unitprice').val();

            var total = qty * unitp;

            $('#amount').val(total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));


        });


    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="editlist" type="<%= vm.getInvrefno()%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addinvmaterial" disabled><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>



            <form data-toggle="validator" role="form" id="saveform">
                <input type="hidden" name="prepareid" id="prepareid" value="<%= log.getUserID()%>">
                <input type="hidden" name="preparename" id="preparename" value="<%= log.getFullname()%>">
                <input type="hidden" name="preparedate" id="preparedate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
                <div class="well">
                    <div class="row">

                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Reference No &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="no" name="no" placeholder="Auto Generated" autocomplete="off" value="<%= vm.getInvrefno()%>" required readonly>   
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Company Name &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="suppname" name="" placeholder="Auto Generated" autocomplete="off" value="<%= vm.getSuppname()%>" required readonly>   
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Date &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="refer" name="" placeholder="Auto Generated" autocomplete="off" value="<%= vm.getDate()%>" required readonly>   
                            </div>
                        </div>
                    </div></div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Material Code&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="matcode" name="matcode" value="<%= vm.getAccode()%>" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getmaterial" type="button" id="matcode" id1="matdesc"><i class="fa fa-cog"></i> Get Code</button>
                                    </span>
                                </div>    
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Material Description&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="matdesc" name="matdesc" placeholder="" autocomplete="off" value="<%= vm.getAcdesc()%>" readonly >   
                            </div>
                        </div></div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Unit Of Measure &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="unitmeasure" name="unitmeasure">
                                    <%= ParameterDAO.parameterList(log, "Unit Measure", "Unit")%>
                                </select>
                            </div></div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Quantity&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm calc_amount" id="quantity" name="quantity" placeholder="" value="0" autocomplete="off"  >   
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Unit Price&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm calc_amount" id="unitprice" name="unitprice" placeholder="" autocomplete="off" value="0.00" >   
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Amount &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="amount" name="amount" placeholder="0.00" autocomplete="off" value="0.00" readonly> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Tax Code &nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="hidden" name="taxcoacode" id="taxcoacode">
                                    <input type="hidden" name="taxcoadescp" id="taxcoadescp">
                                    <input type="text" class="form-control input-sm calculate_gst" id="taxcode" name="taxcode" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="gettax"><i class="fa fa-cog"></i> Tax Type</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Tax Description &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="taxdescp" name="taxdescp" autocomplete="off" readonly> 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Tax Rate &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="taxrate" name="taxrate" value="0.00" autocomplete="off" readonly> 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Tax Amount &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="taxamt" name="taxamt" placeholder="0.00" autocomplete="off" value="0.00" readonly > 
                            </div>
                        </div>
                    </div><div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Location Level&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="loclevel" name="loclevel" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="getlocation"><i class="fa fa-cog"></i> Location</button>
                                    </span>
                                </div>    
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Location Code&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="loccode" name="loccode"  autocomplete="off" readonly>   
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Location Name&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="locdesc" name="locdesc" placeholder="" autocomplete="off" readonly >   
                            </div>
                        </div>
                    </div>


                </div>

                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Account Code&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span id="res_digit" class="res_style"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="accode" name="accode" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="accode" id1="acdesc"><i class="fa fa-cog"></i> Account</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Account Description&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="acdesc" name="acdesc" readonly>
                            </div>
                        </div>

                    </div> 
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Sub Account Type&nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="satype" name="satype" value="None" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="getsub"><i class="fa fa-cog"></i> Sub Account</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Sub Account Code &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="sacode" name="sacode" placeholder="" autocomplete="off" value="00" readonly> 
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Sub Account Description &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="sadesc" name="sadesc" placeholder="" autocomplete="off" value="Not Applicable" readonly > 
                            </div>
                        </div></div>
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Remarks&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <textarea class="form-control" rows="3"  id="remark" name="remark"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </form>  
        </div>
    </div>
</div>