<%-- 
    Document   : dev_unapprove_viewgl
    Created on : Jan 9, 2017, 11:41:05 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.development.Unapprove"%>
<%@page import="com.lcsb.fms.model.financial.gl.GeneralLedger"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.dao.development.UnapproveDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    String refer = request.getParameter("refer");
    Unapprove un = (Unapprove) UnapproveDAO.getMaster(log,refer);
%>
<div class="row">
    <div class="col-sm-12"><p>Status : <%= GeneralTerm.getStatusLabel(un.getPostStatus()) %></p></div>
</div>
<table class="table table-bordered table-striped table-hover" id="list-bank">
    <thead>
        <tr>
            <th>Date</th>
            <th>Acc. Code</th>
            <th>Acc. Desc</th>
            <th>Debit</th>
            <th>Credit</th>
        </tr>
    </thead>
    <tbody>
        <%
            List<GeneralLedger> list = (List<GeneralLedger>) un.getListGL();
            
            if(list.isEmpty()){
                %>
                <tr>
            <td colspan="5">&nbsp;Voucher not yet post to General Ledger.</td>
        </tr>  
                <%
            }

            for (GeneralLedger c : list) {

        %>
        <tr>
            <td class="tdrow">&nbsp;<%= c.getTarikh()%></td>
            <td class="tdrow">&nbsp;<%= c.getCOACode()%></td>
            <td class="tdrow">&nbsp;<%= c.getCOADesc()%></td>
            <td class="tdrow" align="right">&nbsp;<%= c.getDebit()%></td>
            <td class="tdrow" align="right">&nbsp;<%= c.getCredit()%></td>
        </tr>

        <%}%>
    </tbody>
</table>