<%-- 
    Document   : cb_cv_edit_main
    Created on : Mar 24, 2016, 9:40:22 AM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoice"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    SalesInvoice v = (SalesInvoice) SalesInvoiceDAO.getINV(log,request.getParameter("referno"));
    Module mod = (Module) SalesInvoiceDAO.getModule();
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });
        //$('#JVdate').datepicker({ dateFormat: 'yyyy-mm-dd' });

        $('#JVdate').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        });

        $("#total").keyup(function () {
            var val = $(this).val();
            var to = 'amount';
            //alert(val);
            //alert(checkRegexp(val,to));
            if (checkRegexp(val, to)) {
                $('#res_digit').empty();
                $('#div_digit').removeClass('has-error');
                $('#div_digit').addClass('has-success');
                $('#res_digit').append("<i class=\"fa fa-check-circle right-green\"></i>&nbsp;&nbsp;");
                $('#savebutton').prop('disabled', false);
                convertRM(val, to);
            } else {
                $('#res_digit').empty();
                $('#div_digit').removeClass('has-success');
                $('#div_digit').addClass('has-error');
                $('#res_digit').append("<i class=\"fa fa-exclamation-circle fail-red\"></i>&nbsp;&nbsp;Numeric Formatted Only");
                $('#savebutton').prop('disabled', true);
            }
            //
        });

        $('#gettype').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Receiver',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_receiver.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

    });

</script>
<div id ="maincontainer">
    <div class="partition">
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="editlist" type="<%= v.getInvref()%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="editmainprocess" ><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>



            <form data-toggle="validator" role="form" id="saveform">
                <input type="hidden" name="prepareid" id="pid" value="<%= log.getUserID()%>">
                <input type="hidden" name="preparename" id="pname" value="<%= log.getFullname()%>">
                <input type="hidden" name="preparedate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
                <input type="hidden" name="coacode" id="coacode" value="<%= v.getCoacode()%>">
                <input type="hidden" name="coadesc" id="coadesc" value="<%= v.getCoadesc()%>">
                <input type="hidden" name="sacode" id="sacode" value="<%= v.getSacode()%>">
                <input type="hidden" name="sadesc" id="sadesc" value="<%= v.getSadesc()%>">
                <input type="hidden" name="satype" id="satype" value="<%= v.getSatype()%>">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Invoice No &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="invno" name="invno" value="<%= v.getInvno()%>" autocomplete="off" required readonly>   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Reference No &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="invref" name="invref" value="<%= v.getInvref()%>" autocomplete="off" readonly >   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Date &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="invdate" name="invdate" value="<%= v.getInvdate()%>" autocomplete="off" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">   
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Year &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="year" name="year" placeholder="Code" autocomplete="off" value="<%= v.getYear()%>">   
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Period &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="period" name="period"  autocomplete="off" value="<%= v.getPeriod()%>">   
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">DO No &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="dono" name="dono" value="<%= v.getDono()%>" autocomplete="off" >   
                            </div>
                        </div>
                            
            <div class="col-sm-3">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Order No &nbsp&nbsp<span class="res_code"></span></label>
                    <input type="text" class="form-control input-sm" id="orderno" name="orderno" value="<%= v.getOrderno()%>" autocomplete="off" >   
                </div>
            </div>
                    </div>
                </div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group form-group-sm">
                                <label for="inputName" class="control-label">Buyer Type</label>
                                <select class="form-control input-sm" id="btype" name="btype">
                                    <%= ParameterDAO.parameterList(log,"Buyer Type", v.getBtype())%>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Buyer Code&nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="bcode" name="bcode" value="<%= v.getBcode()%>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="getbuyer"><i class="fa fa-cog"></i> Info</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Buyer Name&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="bname" name="bname" value="<%= v.getBname()%>" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">GST ID&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="gstid" name="gstid" placeholder="" autocomplete="off" value="<%= v.getGstid()%>"> 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Address &nbsp&nbsp<span class="res_code"></span></label>
                                <textarea class="form-control" rows="3"  id="baddress" name="baddress"><%= v.getBaddress()%></textarea>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Postcode &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="bpostcode" name="bpostcode" placeholder="" autocomplete="off" value="<%= v.getBpostcode()%>"> 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">City &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="bcity" name="bcity" placeholder="" autocomplete="off"> 
                            </div>
                        </div><div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">State &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="bstate" name="bstate" placeholder="" autocomplete="off" value="<%= v.getBstate()%>"> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Location Level&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="loclevel" name="loclevel" value="<%= v.getLoclevel()%>" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="getlocation"><i class="fa fa-cog"></i> Location</button>
                                    </span>
                                </div>    
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Location Code&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="loccode" name="loccode"  autocomplete="off" value="<%= v.getLoccode()%>" readonly>   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Location Name&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="locdesc" name="locdesc" placeholder="" value="<%= v.getLocdesc()%>" autocomplete="off" readonly >   
                            </div>
                        </div>
                    </div>
                </div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">RM (Digit) &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="total" name="amountno" placeholder="0.00" autocomplete="off" value="<%= v.getAmountno()%>" > 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">RM (In Words) &nbsp&nbsp<span class="res_code"></span></label>
                                <textarea class="form-control" rows="3"  id="amount" name="amountstr"><%= v.getAmountstr()%></textarea>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Remarks &nbsp&nbsp<span class="res_code"></span></label>
                                <textarea class="form-control" rows="3"  id="remarks" name="remarks"><%= v.getRemarks()%></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="well">         
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Rounding Adjustment Code&nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="racoacode" name="racoacode" value="<%= v.getRacoacode()%>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="racoacode" id1="racoadesc"><i class="fa fa-cog"></i> Account</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Rounding Adjustment Description&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="racoadesc" name="racoadesc" placeholder="0.00" autocomplete="off" value="<%= v.getRacoadesc()%>"> 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Company Code&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="estcode" name="estcode" placeholder="0.00" autocomplete="off" value="<%= log.getEstateCode()%>" readonly > 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Company Name&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="estname" name="estname" placeholder="0.00" autocomplete="off" value="<%= log.getEstateDescp()%>" readonly> 
                            </div>
                        </div>
                    </div>
                </div>    
            </form>
        </div>
    </div>
</div>