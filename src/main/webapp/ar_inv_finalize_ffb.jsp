<%-- 
    Document   : ar_inv_finalize
    Created on : Oct 14, 2016, 9:59:54 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.financial.ar.ArFFB"%>
<%@page import="com.lcsb.fms.util.dao.EstateDAO"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArTempRefinery"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArTempMaster"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    ArTempMaster am = (ArTempMaster) SalesInvoiceDAO.getAllInfo(log,request.getParameter("sessionid"));
%>
<!DOCTYPE html>
<table class="table table-bordered table-striped  table-hover">
    <thead>
        <tr>
            <th colspan="4">List of Invoice
                <!--<small><span class="label label-primary">Preparing</span></small>-->

            </th>
        </tr>
    </thead>
    <tbody class="activerowm tdrow">
        <tr>
            <th width="30%">Product</th>
            <td width="70%"><%= request.getParameter("prodcode")%> - <%= request.getParameter("prodname")%></td>
        </tr>
        <tr>
            <th>Account</th>
            <td><%= EstateDAO.getEstateInfo(log,"2411", "estatecode").getHqsuspence()%> - <%= EstateDAO.getEstateInfo(log,"2411", "estatecode").getHqsuspencedescp()%></td>
        </tr>
        <tr>
            <th>Location</th>
            <td>Company / <%= log.getEstateCode()%> / <%= log.getEstateDescp()%></td>
        </tr>
        <tr>
            <th>Buyer</th>
            <td><%= am.getBuyercode()%> - <%= am.getBuyername()%></td>
        </tr>

    </tbody>
</table>

<%ArFFB j = (ArFFB) am.getListFFB();%>

<table class="table table-bordered table-striped  table-hover">

    <tbody class="activerowm tdrow">
        <tr>
            <th width="15%">FFB Supplied</th>
            <td width="35%"><%= j.getFfb()%> MT</td>
            <th width="15%">Price Per MT</th>
            <td width="35%"><%= j.getPriceperton()%></td>
        </tr>
        <tr>
            <th>Total</th>
            <td><%= GeneralTerm.currencyFormat(j.getTotal())%></td>
            <th>Tax</th>
            <td><%= GeneralTerm.currencyFormat(j.getGst())%></td>
        </tr>
        <tr>
            <th>Nett Amount</th>
            <td><%= GeneralTerm.currencyFormat(j.getNettamount())%></td>
            <th></th>
            <td></td>
        </tr>


    </tbody>
</table>
