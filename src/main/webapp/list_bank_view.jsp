<%-- 
    Document   : list_bank_view
    Created on : Mar 4, 2016, 4:06:59 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.model.setup.company.CeManageBankPK"%>
<%@page import="com.lcsb.fms.model.setup.company.CeManageBank"%>
<%@page import="com.lcsb.fms.util.dao.BankDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<div class="list-group"><%
            
            String keyword = request.getParameter("keyword");
            List<CeManageBank> slist = (List<CeManageBank>) BankDAO.getAllBankCA(log,keyword);
            for (CeManageBank c : slist) { 
                CeManageBankPK p = c.getCeManageBankPK();
               %>
       
            <a href="<%= c.getName()%>" id="<%= p.getCode() %>" title="" class="list-group-item thisresult">
                <div id="left_div"><%= p.getCode() %></div>
                <div id="right_div"><%= c.getName()%></div>
            </a>

               <%    
            } 
        %>
</div>