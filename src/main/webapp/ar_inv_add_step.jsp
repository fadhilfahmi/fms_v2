<%-- 
    Document   : ar_inv_add_step
    Created on : Oct 13, 2016, 8:47:39 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.financial.ar.ArContractDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArContractAgree"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        var oTable = $('#list').DataTable({
            responsive: true,
            "aaSorting": [[3, "desc"]],
        });

        $('#list tbody').on('click', 'tr', function (e) {
            var data = oTable.row(this).data();

            $('#selected-contract').val(data[1]);
            $('#broker-number').val(data[2]);
            $('#inputprice').val(data[6]);
            e.preventDefault();

        });
    });
</script>
<input type="hidden" id="selected-contract">
<input type="hidden" id="broker-number">
<input type="hidden" name="inputprice" id="inputprice" value="">
<table class="table table-bordered table-striped table-hover" id="list">
    <thead>

        <tr>
            <th>#</th>
            <th>Contract No</th>
            <th>Broker No</th>
            <th>Date</th>
            <th>Delivery Month</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Active</th>
        </tr>
    </thead>
    <tbody>
        <%List<ArContractAgree> listx = (List<ArContractAgree>) ArContractDAO.getAllAgreementByProduct(log,request.getParameter("buyercode"), request.getParameter("prodcode"));

                        for (ArContractAgree j : listx) {%>
        <tr class="activerowy" id="<%= j.getId()%>">
            <td class="tdrow"><%= j.getId()%></td>
            <td class="tdrow"><%= j.getNo()%></td>
            <td class="tdrow"><%= j.getBrokernum()%></td>
            <td class="tdrow">&nbsp;<%= j.getDate()%></td>
            <td class="tdrow">&nbsp;<%= j.getDmonth()%> - <%= j.getDyear()%></td>
            <td class="tdrow">&nbsp;<%= j.getQty()%></td>
            <td class="tdrow"><%= j.getPrice()%>

            </td>
            <td class="tdrow" align="right">&nbsp;<%= j.getActive()%></td>
        </tr>
        <%}%>
    </tbody>

</table>
