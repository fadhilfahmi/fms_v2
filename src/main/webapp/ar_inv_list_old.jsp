

<%@page import="com.lcsb.fms.util.model.ListTable"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.ModuleItem"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.ConnectionModel"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.CashVoucherDAO"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,java.util.*" errorPage="" isThreadSafe="false" %>
<%@ page import="java.util.Date"%>
<%
    Connection con = ConnectionUtil.getConnection();
    String year = AccountingPeriod.getCurYear();
    String period = AccountingPeriod.getCurPeriod();
    Module mod = (Module) SalesInvoiceDAO.getModule();

    response.setHeader("Cache-Control", "no-cache");
%>

<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/tether.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>

<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        var updateTable = '<%= mod.getMainTable()%>';

    <%
        List<ListTable> mlist = (List<ListTable>) SalesInvoiceDAO.tableList();
        for (ListTable m : mlist) {
            //if(m.getList_View().equals("1")){
%>
        columntoView.push('<%= m.getColumnName()%>');
        columnName.push('<%= m.getTitleName()%>');
    <%
            //}

        }
    %>

        var colsTosend = 'cols=';
        for (m = 0; m < columntoView.length; ++m) {//put value from database to variable jget
            if (m == columntoView.length - 1) {
                colsTosend += columntoView[m];
            } else {
                colsTosend += columntoView[m] + '&cols=';
            }
        }


        $("#add-new").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addnew",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        $("#check-button").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=check",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        var orderby = 'order by voucherid desc';
        var oTable = $('#example').DataTable({
            destroy: true,
            "aaSorting": [[0, "desc"]],
            "responsive": true,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "retrievetable.jsp?table=" + updateTable + "&code=<%= mod.getReferID_Master()%>&" + colsTosend + "&order=" + orderby,
            "aoColumns": [
                {"sTitle": columnName[0], "mData": columntoView[0], "sWidth": "9%", "bSearchable": true},
                {"sTitle": columnName[1], "mData": columntoView[1], "sWidth": "9%"},
                {"sTitle": columnName[2], "mData": columntoView[2], "sWidth": "25%"},
                {"sTitle": columnName[3], "mData": columntoView[3], "sWidth": "10%"},
                {"sTitle": columnName[4], "mData": columntoView[4], "sWidth": "15%", "bVisible": false},
                {"sTitle": columnName[5], "mData": columntoView[5], "sWidth": "15%", "bVisible": false},
                {"sTitle": columnName[6], "mData": columntoView[6], "sWidth": "15%", "bVisible": false},
                {"sTitle": "Status", "mData": null, "sWidth": "10%"},
                {"sTitle": "Action", "mData": null, "bSortable": false, "bSearchable": false, "sWidth": "13%"}

            ],
            "aoColumnDefs": [{
                    "aTargets": [8],
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {

                        //var a = $(icon('addacc',oData.checkbyid,oData.approvebyid,oData.postflag));
                        //var b = $('<img src="image/icon/view.png" title="View" width="22" style="vertical-align:middle;cursor:pointer">');
                        //var c = $('<img src="image/icon/edit_2.png" title="Edit" width="22" style="vertical-align:middle;cursor:pointer">');
                        //var c = $(icon('edit',oData.checkbyid,oData.approvebyid,oData.postflag));
                        //var e = $(icon('delete',oData.checkbyid,oData.approvebyid,oData.postflag));
                        //var f = $(icon('post',oData.checkbyid,oData.approvebyid,oData.postflag));
                        //var g = $(icon('cancel',oData.checkbyid,oData.approvebyid,oData.postflag));

                        var c = $(icon('edit', oData.cid, oData.aid, oData.postflag));
                        var e = $(icon('delete', oData.cid, oData.aid, oData.postflag));
                        var f = $(icon('post', oData.cid, oData.aid, oData.postflag));
                        var a = $(icon('adddetail', oData.cid, oData.aid, oData.postflag));
                        var b = $('<button type="button" class="btn btn-default btn-xs"><i class="fa fa-folder-open"></i></button>');
                        var k = $(icon('check', oData.cid, oData.aid, oData.postflag));
                        var g = $(icon('cancel', oData.cid, oData.aid, oData.postflag));

                        a.on('click', function () {
                            //console.log(oData);
                            if (iconClick('edit', oData.cid, oData.aid, oData.postflag) == 0) {
                                //addSubform(oData.JVrefno,subTable,'<%= mod.getModuleID()%>',module_abb,oTable);
                                //addDebitCredit(oData.JVrefno);

                                $.ajax({
                                    url: "PathController?moduleid=<%= mod.getModuleID()%>&process=adddetail&refer=" + oData.invref,
                                    success: function (result) {
                                        // $("#haha").html(result);
                                        $('#herey').empty().html(result).hide().fadeIn(300);
                                    }});
                                return false;
                            }

                            return false;
                        });
                        b.on('click', function () {
                            //console.log(oData);
                            $.ajax({
                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewinv&referno=" + oData.invref,
                                success: function (result) {
                                    // $("#haha").html(result);
                                    $('#herey').empty().html(result).hide().fadeIn(300);
                                }});
                            return false;
                        });
                        c.on('click', function () {
                            //console.log(oData);
                            if (iconClick('edit', oData.cid, oData.aid, oData.postflag) == 0) {
                                //editRowx(oData.JVrefno,updateTable,subTable,<%= mod.getModuleID()%>,module_abb,oTable);

                                $.ajax({
                                    url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editlist&referno=" + oData.invref,
                                    success: function (result) {
                                        // $("#haha").html(result);
                                        $('#herey').empty().html(result).hide().fadeIn(300);
                                    }});
                                return false;
                            }

                            //$("#dialog-form-update").dialog("open");
                            //$( "#dialog-form-update" ).dialog( "open" );
                            return false;
                        });
                        e.on('click', function () {
                            //console.log(oData);
                            if (iconClick('delete', oData.cid, oData.aid, oData.postflag) == 0) {
                                //deleteRow(oData.JVrefno);

                                BootstrapDialog.confirm({
                                    title: 'Confirmation',
                                    message: 'Are you sure to delete?',
                                    type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                    closable: true, // <-- Default value is false
                                    draggable: true, // <-- Default value is false
                                    btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                    btnOKLabel: 'Delete', // <-- Default value is 'OK',
                                    btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                                    callback: function (result) {
                                        // result will be true if button was click, while it will be false if users close the dialog directly.
                                        if (result) {
                                            $.ajax({
                                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&referno=" + oData.invref,
                                                success: function (result) {
                                                    // $("#haha").html(result);
                                                    setTimeout(function () {
                                                        oTable.fnClearTable();
                                                    }, 500);
                                                }});
                                        }
                                    }
                                });


                                return false;
                            }

                            return false;
                        });
                        f.on('click', function () {
                            //console.log(oData);
                            if (iconClick('post', oData.cid, oData.aid, oData.postflag) == 0) {
                                //postThis(oData.JVrefno);
                                $.ajax({
                                    url: "PathController?moduleid=<%= mod.getModuleID()%>&process=post&referno=" + oData.invref,
                                    success: function (result) {
                                        // $("#haha").html(result);
                                        $('#herey').empty().html(result).hide().fadeIn(300);
                                    }});
                            }

                            return false;
                        });
                        g.on('click', function () {
                            //console.log(oData);
                            if (iconClick('cancel', oData.cid, oData.aid, oData.postflag) == 0) {
                                cancel(oData.JVrefno);
                            }

                            return false;
                        });

                        k.on('click', function () {
                            //console.log(oData);
                            if (iconClick('delete', oData.cid, oData.aid, oData.postflag) == 0) {
                                //deleteRow(oData.JVrefno);

                                BootstrapDialog.confirm({
                                    title: 'Confirmation',
                                    message: 'Are you sure to delete?',
                                    type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                    closable: true, // <-- Default value is false
                                    draggable: true, // <-- Default value is false
                                    btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                    btnOKLabel: 'Delete', // <-- Default value is 'OK',
                                    btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                                    callback: function (result) {
                                        // result will be true if button was click, while it will be false if users close the dialog directly.
                                        if (result) {
                                            $.ajax({
                                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&referno=" + oData.invref,
                                                success: function (result) {
                                                    // $("#haha").html(result);
                                                    setTimeout(function () {
                                                        oTable.fnClearTable();
                                                    }, 500);
                                                }});
                                        }
                                    }
                                });


                                return false;
                            }

                            return false;
                        });


                        $(nTd).empty();
                        $(nTd).attr("class", 'btn-group');
                        $(nTd).attr("align", 'right');
                        $(nTd).prepend(a, b, c, e);
                    }

                },
                {
                    "aTargets": [7], //special rules for determine a status
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {


                        var stat = '<span class="label label-primary">Preparing</span>';

                        if (oData.cid.length > 0) {
                            stat = '<span class="label label-default">Checked</span>';
                        }
                        if (oData.aid.length > 0) {
                            stat = '<span class="label label-success">Approved</span>';
                        }
                        if (oData.postflag == 'Cancel') {
                            stat = 'Cancelled';
                        }

                        var dc = '';
                        $.ajax({
                            async: false,
                            url: "ProcessController?moduleid=000000&process=checkDCnote&referno=" + oData.invref,
                            success: function (result) {
                                // $("#haha").html(result);
                                dc = result;

                            }
                        });

                        $(nTd).empty();
                        //$(nTd).attr("id",'btntest');
                        $(nTd).prepend(stat, dc);
                    }

                }]



        });

        $('#example tbody').on('click', 'tr', function () {
            var data = oTable.row(this).data();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editlist&referno=" + data.invref,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $('body').on('click', '#refresh', function (e) {
            console.log('Refreshing');
            oTable.draw();
            return false;
        });

        $("#syncmill").click(function () {
            var cont = '<i class="fa fa-cog fa-spin fa-fw"></i><span> Connecting...</span>';
            $('#syncmill').empty().html(cont).hide().fadeIn(300);
            $.ajax({
                async: true,
                url: "ProcessController?moduleid=000000&process=connecttomill",
                success: function (result) {
                    // $("#haha").html(result);
                    console.log(result);
                    if (result == 1) {
                        //$('#herey').empty().html(result).hide().fadeIn(300);
                        var conts = '<i class="fa fa-check-circle success" aria-hidden="true"></i> Connection Successful';
                        $('#syncmill').empty().html(conts).hide().fadeIn(300);

                        conts = '<i class="fa fa-cog fa-spin fa-fw"></i><span> Syncing...';
                        $('#syncmill').empty().html(conts).hide().fadeIn(300);

                        $.ajax({
                            async: true,
                            url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=syncrefinery",
                            success: function (result) {
                                // $("#haha").html(result);
                                //$('#herey').empty().html(result).hide().fadeIn(300);
                                var conts = '<i class="fa fa-check-circle success" aria-hidden="true"></i> Synced';
                                $('#syncmill').empty().html(conts).hide().fadeIn(300);
                            },
                            error: function () {
                                var conts = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Failed';
                                $('#syncmill').empty().html(conts).hide().fadeIn(300);
                            }
                        });
                    } else {
                        var conts = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Connection Failed';
                        $('#syncmill').empty().html(conts).hide().fadeIn(300);
                    }

                },
                error: function () {
                    var conts = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Connection Failed';
                    $('#syncmill').empty().html(conts).hide().fadeIn(300);
                }
            });
            return false;
        });


    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <button id="add-new" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> Add <%= mod.getModuleDesc()%></button>
            <button id="syncmill" class="btn btn-default btn-sm syncmill"><i class="fa fa-cloud-download" aria-hidden="true"></i> Sync Refinery Mill Data</button>
            <div class="pull-right">
            <button id="filter" class="btn btn-default btn-sm filter"><i class="fa fa-filter" aria-hidden="true"></i> Filter</button>
            <button id="refresh" class="btn btn-default btn-sm refresh"><i class="fa fa-refresh"></i></button>
            </div>
            <br><br>       
            <div class="dataTable_wrapper"> 
                <table class="table table-striped table-bordered table-hover" id="example">
                </table>
            </div>
        </div>
    </div>
</div>
