<%-- 
    Document   : tx_gst03_list
    Created on : Nov 24, 2016, 11:26:21 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.administration.om.UserDAO"%>
<%@page import="com.lcsb.fms.dao.financial.tx.TaxMasterDAO"%>
<%@page import="com.lcsb.fms.dao.financial.tx.TaxGst03DAO"%>
<%@page import="com.lcsb.fms.util.model.ListTable"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.ModuleItem"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.ConnectionModel"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,java.util.*" errorPage="" isThreadSafe="false" %>
<%@ page import="java.util.Date"%>
<%
    Module mod = (Module) TaxGst03DAO.getModule();
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    response.setHeader("Cache-Control", "no-cache");
%>

<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/tether.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>

<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        var updateTable = '<%= mod.getMainTable()%>';

    <%
        List<ListTable> mlist = (List<ListTable>) TaxGst03DAO.tableList();
        for (ListTable m : mlist) {
            //if(m.getList_View().equals        ("1")){
    %>
        columntoView.push('<%= m.getColumnName()%>');
        columnName.push('<%= m.getTitleName()%>');
    <%
            //}

        }
    %>

        var colsTosend = 'cols=';
        for (m = 0; m < columntoView.length; ++m) {//put value from database to variable jget
            if (m == columntoView.length - 1) {
                colsTosend += columntoView[m];
            } else {
                colsTosend += columntoView[m] + '&cols=';
            }
        }


        $("#add-new").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addgst03",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        $("#check-button").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=check",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        var orderby = 'order by voucherid desc';
        var oTable = $('#example').DataTable({
            destroy: true,
            "aaSorting": [[0, "desc"]],
            "responsive": true,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "retrievetable.jsp?table=" + updateTable + "&code=<%= mod.getReferID_Master()%>&" + colsTosend + "&order=" + orderby,
            "aoColumns": [
                {"sTitle": columnName[0], "mData": columntoView[0], "sWidth": "15%", "bSearchable": true},
                {"sTitle": columnName[1], "mData": columntoView[1], "sWidth": "10%"},
                {"sTitle": columnName[2], "mData": columntoView[2], "sWidth": "10%"},
                {"sTitle": columnName[3], "mData": columntoView[3], "sWidth": "10%"},
                {"sTitle": columnName[4], "mData": columntoView[4], "sWidth": "10%"},
                {"sTitle": columnName[5], "mData": columntoView[5], "sWidth": "15%", "bVisible": false},
                {"sTitle": columnName[6], "mData": columntoView[6], "sWidth": "15%", "bVisible": false},
                {"sTitle": "Status", "mData": null, "sWidth": "10%"},
                {"sTitle": "Action", "mData": null, "bSortable": false, "bSearchable": false, "sWidth": "15%"}

            ],
            "aoColumnDefs": [{
                    "aTargets": [8],
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {



                        var c = $(icon('edit', oData.lockedby, oData.recon_refno, ''));
                        var e = $(icon('delete', oData.lockedby, oData.recon_refno, ''));
                        var f = $(icon('post', oData.lockedby, oData.recon_refno, ''));
                        var a = $('<button type="button" class="btn btn-default btn-xs"><i class="fa fa-plus-circle"></i></button>');
                        var b = $('<button type="button" class="btn btn-default btn-xs"><i class="fa fa-folder-open"></i></button>');
                        var k = $(icon('check', oData.lockedby, oData.recon_refno, ''));
                        var g = $(icon('cancel', oData.lockedby, oData.recon_refno, ''));

                        /*a.on('click', function() {
                         //console.log(oData);
                         if(iconClick('edit',oData.checkid,oData.appid,oData.post            )==0){
                         //addSubform(oData.JVrefno,subTable,'<%= mod.getModuleID()%>',module_abb,oTable);
                         //addDebitCredit(oData.JVrefno);
                         
                         $.ajax({
                         url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addnewitem&referno="+oData.refer,
                         success: function (result) {
                         // $("#haha").html(result);
                         $('#herey').empty().html(result).hide().fadeIn(300);
                         }});
                         return false;
                         }
                         
                         return false;
                         });*/
                        b.on('click', function () {
                            //console.log(oData);
                            $.ajax({
                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewor&refer=" + oData.refno,
                                success: function (result) {
                                    // $("#haha").html(result);
                                    $('#herey').empty().html(result).hide().fadeIn(300);
                                }});
                            return false;
                        });
                        c.on('click', function () {
                            //console.log(oData);
                            if (iconClick('edit', oData.lockedby, oData.recon_refno, '') == 0) {
                                //editRowx(oData.JVrefno,updateTable,subTable,<%= mod.getModuleID()%>,module_abb,oTable);

                                $.ajax({
                                    url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editlist&referno=" + oData.refno,
                                    success: function (result) {
                                        // $("#haha").html(result);
                                        $('#herey').empty().html(result).hide().fadeIn(300);
                                    }});
                                return false;
                            }

                            //$("#dialog-form-update").dialog("open");
                            //$( "#dialog-form-update" ).dialog( "open" );
                            return false;
                        });
                        e.on('click', function () {
                            //console.log(oData);
                            if (iconClick('delete', oData.lockedby, oData.recon_refno, '') == 0) {
                                //deleteRow(oData.JVrefno);

                                BootstrapDialog.confirm({
                                    title: 'Confirmation',
                                    message: 'Are you sure to delete?',
                                    type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                    closable: true, // <-- Default value is false
                                    draggable: true, // <-- Default value is false
                                    btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                    btnOKLabel: 'Delete', // <-- Default value is 'OK',
                                    btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                                    callback: function (result) {
                                        // result will be true if button was click, while it will be false if users close the dialog directly.
                                        if (result) {
                                            $.ajax({
                                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&referno=" + oData.refno,
                                                success: function (result) {
                                                    // $("#haha").html(result);
                                                    setTimeout(function () {
                                                        oTable.draw();
                                                    }, 500);
                                                }});
                                        }
                                    }
                                });


                                return false;
                            }

                            return false;
                        });






                        $(nTd).empty();
                        $(nTd).attr("class", 'btn-group');
                        $(nTd).attr("align", 'right');
                        $(nTd).prepend(a, b, c, e);
                    }

                },
                {
                    "aTargets": [7], //special rules for determine a status
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        var stat = '<span class="label label-primary">Preparing</span>';


                        if (oData.appid.length > 0) {
                            stat = '<span class="label label-success">Approved</span>';
                        }

                        $(nTd).empty();
                        //$(nTd).attr("id",'btntest');
                        $(nTd).prepend(stat);
                    }

                }]



        });

        $('#example tbody').on('click', 'tr', function () {
            var data = oTable.row(this).data();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewdetail&refer=" + data.refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $('#example tbody').on('click', '#addcredit', function (e) {
            e.preventDefault();
            var refer = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addcredit&refer=" + data.refno,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $('#example tbody').on('click', '#addcheque', function (e) {
            e.preventDefault();
            var refer = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addcheque&refer=" + data.refno,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $('#add-invoice').click(function (e) {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addfrominvoice",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });

        $('#closeperiod').click(function (e) {
            e.preventDefault();

            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                size: BootstrapDialog.SIZE_WIDE,
                //animate: false,
                title: 'Confirm Close and Open New Period',
                message: function (dialog) {
                    var $content = $('<body></body>').load('tx_closeopenperiod_view.jsp');
                    //$('body').on('click', '.thisresult_nd', function(event){
                    //    dialog.close();
                    //    event.preventDefault();
                    //});
                    return $content;
                },
                buttons: [{
                        //icon: 'fa fa-circle-o-notch',
                        label: 'Confirm & Close',
                        cssClass: 'btn-primary',
                        autospin: true,
                        action: function (dialogRef) {
                            var $button = this;
                            dialogRef.enableButtons(false);
                            dialogRef.setClosable(false);
                            //dialogRef.getModalBody().html('<span style="font-size:14px">Finalizing in progress...</span>');

                            $.ajax({
                                async: false,
                                url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=confirm",
                                success: function (result) {
                                    // $("#haha").html(result);
                                    //$('#herey').empty().html(result).hide().fadeIn(300);
                                    if (result == 1) {
                                        setTimeout(function () {
                                            //dialogRef.close();
                                            dialogRef.enableButtons(true);
                                            dialogRef.setClosable(true);
                                            $button.disable();
                                            $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                            dialogRef.getModalBody().html('<i class="fa fa-check right-green"></i>&nbsp;&nbsp;<span style="font-size:14px">Success!</span>');
                                        }, 3000);
                                    } else {
                                        setTimeout(function () {
                                            //dialogRef.close();
                                            dialogRef.enableButtons(true);
                                            dialogRef.setClosable(true);
                                            //$button.disable();
                                            $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                            dialogRef.getModalBody().html('<i class="fa fa-times fail-red"></i>&nbsp;&nbsp;<span style="font-size:14px">Failed!</span>');
                                        }, 3000);
                                    }
                                }
                            });

                        }
                    }, {
                        label: 'Close',
                        action: function (dialogRef) {
                            dialogRef.close();
                        }
                    }]
            });

            /* BootstrapDialog.show({
             type:BootstrapDialog.TYPE_DEFAULT,
             title: 'Finalizing Account',
             message: function(dialog) {
             var $content = $('<body></body>').load('gl_accountperiod_set.jsp?');
             //$('body').on('click', '.thisresult_nd', function(event){
             //    dialog.close();
             //    event.preventDefault();
             //});
             return $content;
             }
             });*/
        });

        $('body').one('click', '#refresh', function (e) {
            oTable.draw();
            return false;
        });

        $('#undotax').click(function (e) {
            e.preventDefault();

            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                //animate: false,
                title: 'Undo Taxation Process',
                message: 'Proceed with caution. Tax Period : <%= TaxMasterDAO.getTaxPeriod(log).getPeriod()%> - Year : <%= TaxMasterDAO.getTaxPeriod(log).getYear()%>',
                buttons: [{
                        //icon: 'fa fa-circle-o-notch',
                        label: 'Undo',
                        cssClass: 'btn-danger',
                        autospin: true,
                        action: function (dialogRef) {
                            var $button = this;
                            dialogRef.enableButtons(false);
                            dialogRef.setClosable(false);
                            dialogRef.getModalBody().html('<span style="font-size:12px">Deleting all data and updating transactions...</span>');

                            $.ajax({
                                async: false,
                                url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=undotax",
                                success: function (result) {
                                    // 
                                    var obj = jQuery.parseJSON(result);
                                    var i = 0;
                                    $.each(obj, function (key, value) {

                                        var modid = value.moduleid;
                                        var moddesc = value.moduledesc;

                                        function changeIcon(i) {
                                            var icon = '';

                                            if (i == 1) {
                                                icon = '<i style="color:#1d9d73" class="fa fa-check-circle" aria-hidden="true"></i>';

                                            } else if (i == 0) {
                                                icon = '<i style="color:#f44242" class="fa fa-exclamation-circle" aria-hidden="true"></i>';
                                            }
                                            return icon;

                                        }

                                        var render = '<p>' + changeIcon(value.deleteGst03) + ' Deleting GST 03</p>'
                                                + '<p>' + changeIcon(value.deleteReconcile) + ' Deleting Reconcile</p>'
                                                + '<p>' + changeIcon(value.deleteInput) + ' Deleting Input</p>'
                                                + '<p>' + changeIcon(value.deleteOutput) + ' Deleting Output</p>'
                                                + '<p>' + changeIcon(value.updateInput) + ' Updating Input</p>'
                                                + '<p>' + changeIcon(value.updateOutput) + ' Updating Output</p>';
                                        setTimeout(function () {
                                            dialogRef.enableButtons(true);
                                            dialogRef.setClosable(true);
                                            $button.disable();
                                            $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                            dialogRef.getModalBody().html(render);
                                        }, 2000);
                                        /*if (result == 6) {
                                         setTimeout(function () {
                                         //dialogRef.close();
                                         dialogRef.enableButtons(true);
                                         dialogRef.setClosable(true);
                                         $button.disable();
                                         $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                         dialogRef.getModalBody().html('<i class="fa fa-check right-green"></i>&nbsp;&nbsp;<span style="font-size:14px">Success!</span>');
                                         }, 2000);
                                         } else if (result > 1 && result < 6) {
                                         setTimeout(function () {
                                         //dialogRef.close();
                                         dialogRef.enableButtons(true);
                                         dialogRef.setClosable(true);
                                         $button.disable();
                                         $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                         dialogRef.getModalBody().html('<i class="fa fa-check right-green"></i>&nbsp;&nbsp;<span style="font-size:14px">Almost successful</span>');
                                         }, 2000);
                                         } else if (result == 0) {
                                         setTimeout(function () {
                                         //dialogRef.close();
                                         dialogRef.enableButtons(true);
                                         dialogRef.setClosable(true);
                                         //$button.disable();
                                         $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                         dialogRef.getModalBody().html('<i class="fa fa-times fail-red"></i>&nbsp;&nbsp;<span style="font-size:14px">Undo failed!</span>');
                                         }, 2000);
                                         }*/

                                    });
                                },
                                error: function () {
                                    dialogRef.enableButtons(true);
                                    dialogRef.setClosable(true);
                                    //$button.disable();
                                    $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                    dialogRef.getModalBody().html('<i class="fa fa-times fail-red"></i>&nbsp;&nbsp;<span style="font-size:14px">Error Occured!</span>');
                                }
                            });

                        }
                    }, {
                        label: 'Close',
                        action: function (dialogRef) {
                            dialogRef.close();
                            $.ajax({
                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewlist",
                                success: function (result) {
                                    // $("#haha").html(result);
                                    $('#herey').empty().html(result).hide().fadeIn(300);
                                }});
                        }
                    }]
            });
        });



    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <p><label>Current Tax Period : <%= TaxMasterDAO.getTaxPeriod(log).getPeriod()%>, <%= TaxMasterDAO.getTaxPeriod(log).getYear()%></label></p>
                        <button id="add-new" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> Add <%= mod.getModuleDesc()%></button>
                        <button id="closeperiod" class="btn btn-default btn-sm"><i class="fa fa-calendar" aria-hidden="true"></i> Close / Open Tax Period</button>
                        <button id="refresh" class="btn btn-default btn-sm refresh"><i class="fa fa-refresh"></i> Refresh</button>
                        <%

                            if (UserDAO.getInfoByStaffID(log, log.getUserID()).getLevelId() == 0) {
                        %>
                        <button id="undotax" class="btn btn-danger btn-sm"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Undo Taxation Process</button>
                        <%
                            }
                        %>
                        <!--<button id="add-debitnote" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> From Sales Debit Note</button>-->
                    </td>
                </tr>
            </table>
            <br>       
            <div class="dataTable_wrapper"> 
                <table class="table table-striped table-bordered table-hover" id="example">
                </table>
            </div>

        </div>
    </div>
</div>
