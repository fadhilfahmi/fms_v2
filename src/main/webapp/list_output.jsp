<%-- 
    Document   : list_location
    Created on : Mar 2, 2016, 8:37:17 AM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
            
             $.ajax({
                        url: "list_parameter.jsp?type=Output Type",
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
            }});
            
            $('#keyword').keyup(function(){
                //var l = $('input[name=searchby]:checked').val();
                var keyword = $(this).val();
                console.log(keyword);
                
                var level = $('#searchlevel').val();
                var url = '';
                var by = $('#type').val();
                if(level=='first'){
                    url = "list_parameter.jsp?type=Output Type&keyword="+keyword;
                }else if(level=='second'){
                    url = "list_output_by.jsp?by="+by+"&keyword="+keyword;
                }
                 $.ajax({
                        url: url,
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
                }});
            });
            
            
            
            $('#result').on('click', '.thisresult', function(e) { 
                var a = $(this).attr('href');
                var b = $(this).attr('id');
                
                $('#type').val(b);
                $('#searchlevel').val('second');
                $.ajax({
                        url: "list_output_by.jsp?by="+b,
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
                }});
                e.preventDefault();
            });
            
            $('#result').on('click', '.thisresult_nd', function(e) { 
                var a = $(this).attr('href');
                var b = $(this).attr('id');
                var c = $(this).attr('unitmeasure');
                
                $('#outputcode').val(b);
                $('#outputdesc').val(a);
                $('#unitmeasure').val(c);
                $.ajax({
                        url: "list_output_by.jsp?by="+b,
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
                }});
                e.preventDefault();
            });
            
            $('#result').on('click', '#keyword', function(e) { 
                //var l = $('input[name=searchby]:checked').val();
                var keyword = $(this).val();
                console.log(keyword);
                 $.ajax({
                        url: "list_output_by.jsp?table="+table+"&keyword="+keyword,
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
                }});
            });
            
        });
        </script>
    </head>
    <body>
         
        
        <div class="form-group">
            <div class="form-group input-group">
                <input type="text" class="form-control" id="keyword">
                <input type="hidden" class="form-control" id="searchlevel" value="first">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
        
        <div id="result">
       
        </div>
        
         <div id="buttonhere">
       <button class="btn btn-default btn-sm" type="button"><i class="fa fa-cog"></i>
           Location
                    </button>
        </div>   
    </body>
</html>