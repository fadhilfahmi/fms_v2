<%-- 
    Document   : cf_coj_edit_item
    Created on : Jun 1, 2016, 4:14:48 PM
    Author     : user
--%>

<%@page import="com.lcsb.fms.model.setup.configuration.ChartofJob"%>
<%@page import="com.lcsb.fms.model.setup.configuration.ChartofJobItem"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ChartofJobDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
ChartofJobItem v = (ChartofJobItem) ChartofJobDAO.getCJIitem(log,request.getParameter("referenceno"));
//ChartofJob vm = (ChartofJob) ChartofJobDAO.getCJ(request.getParameter("referenceno"));
Module mod = (Module) ChartofJobDAO.getModule();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="jsfunction/jquery-1.12.0.min.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
        <link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
        <script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
           //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });
          
         $('.getmaterial').click(function(e){
              var a = $(this).attr('id');
              var b = $(this).attr('id1');
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Chart of Material',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_material.jsp?code='+a+'&descp='+b);
                        $('body').on('click', '.thisresult', function(event){
                            dialog.close();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
           
           $('#getlocation').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Location',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_location.jsp');
                        $('body').on('click', '.thisresult_nd', function(event){
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
           $('#getsub').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Sub Account Detail',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_sub.jsp');
                        $('body').on('click', '.thisresult_nd', function(event){
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
           $('#gettax').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Taxation',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_tax.jsp');
                        $('body').on('click', '.thisresult', function(event){
                            dialog.close();
                            var a = $('#debit').val();
                            a = a.replace(/\,/g,'');
                            var b = $('#taxrate').val();
                            var c = parseFloat(b) * parseFloat(a) / 100;
                            $('#taxamt').val(parseFloat(c).toFixed(2));
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
                
                
               
               return false;
           });
           
           $('.calculate').keyup(function(e){
               var todeduct = $(this).val();
               var theval = $(this).attr('id');
               
               
               if(todeduct!=''){
                   var tot = 0;
                   if(theval=='debit'){
                       $('#credit').val(0.0);
                   }
                   if(theval=='credit'){
                       $('#debit').val(0.0);
                   }
                   
               }
               
           });
           
           $('.form-control').focusout(function(e){
               if(($('#debit').val()==0.00) || ($('#loccode').val()=='') || ($('#actdesc').val()=='') || ($('#remarks').val()=='')){
                   $('#savebutton').prop('disabled', true);
               }else{
                   $('#savebutton').prop('disabled', false);
               }
           });
           
           $('.calculate').keyup(function(e){
               var todeduct = $(this).val();
               var theval = $(this).attr('id');
               
               
               if(todeduct!=''){
                   var tot = 0;
                   if(theval=='debit'){
                       $('#credit').val(0.0);
                   }
                   if(theval=='credit'){
                       $('#debit').val(0.0);
                   }
                   
               }
               
           });
           
           $('.totalup').keyup(function(e){
               
               var qty = $('#quantity').val();
               var unitp = $('#unitprice').val();
               var taxrate = $('#taxrate').val();
               
               var total = qty * unitp;
               var taxamt = taxrate * total / 100;
               $('#taxamt').val(taxamt);
               $('#amount').val(total);
               
           });
           
           $('.calculate_gst').change(function(e){
               alert(9);
               var amt = $('#amount').val();
               var taxrate = $('#taxrate').val();
               
               var total = taxrate * 100 / amt;
               
               $('#taxamt').val(total);
               
           });
           $('#getsupplier').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Supplier Info',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_supplier.jsp?name=sname&code=scode&address=saddress');
                        $('body').on('click', '.thisresult', function(event){
                            console.log('tutup');
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
            $('#getproduct').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Product Info',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_product.jsp?name=prodname&code=prodcode');
                        $('body').on('click', '.thisresult', function(event){
                            console.log('tutup');
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
           
           
        });
        
       </script>
    </head>
    <body>
        <div id ="maincontainer">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
              <td width="74%" class="borderbot" align="left"><span class="bigfonttitle"><%= mod.getModuleDesc() %></span>&nbsp;&nbsp;<span class="midfonttitle">Edit <%= mod.getModuleDesc() %></span></td>
		<td width="26%" class="borderbot" align="right">&nbsp;</td>
	  </tr>
  </table>
	<br>
   <div class="partition"> 
       <div class="headofpartition"></div>
       <div class="bodyofpartition">
  <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
			<td valign="middle" align="left">
				<button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                                <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="edititemprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
			</td>
		</tr>
  </table>
  <br>
  

  <form data-toggle="validator" role="form" id="saveform">
     <input type="hidden" name="prepareid" id="prepareid" value="<%= log.getUserID()%>">
    <input type="hidden" name="preparename" id="preparename" value="<%= log.getFullname()%>">
    <input type="hidden" name="preparedate" id="preparedate" value="<%= AccountingPeriod.getCurrentTimeStamp() %>">
    <input type="hidden" name="id" id="id" value="<%= v.getId()%>">
    <div class="well">
     <div class="row">
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Job Code &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="jobcode" name="jobcode"  autocomplete="off" required readonly value="<%= v.getJobcode()%>">   
             </div>
        </div>
       
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Account Code&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="accountcode" name="accountcode" readonly value="<%= v.getAccountcode()%>">
                                        <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="accountcode" id1="accountdescp"><i class="fa fa-cog"></i> Get Account </button>
                                        </span>
                                    </div>    
             </div>
        </div>
        <div class="col-sm-8">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Account Description&nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="accountdescp" name="accountdescp"  autocomplete="off" readonly value="<%= v.getAccountdescp()%>">   
             </div>
        </div></div><div class="row">
         <div class="col-sm-12">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Remarks&nbsp&nbsp<span class="res_code"></span></label>
                <textarea class="form-control" rows="3"  id="remarks" name="remarks"><%= v.getRemarks()%></textarea>
             </div>
        </div>
       
    </div>
   
        
    </div>
    
 
    
   
    
    
    
    <!--<div class="form-group-sm input-group">
    <input type="text" class="form-control input-sm">
    <span class="input-group-btn">
    <button class="btn btn-default btn-sm" type="button"><i class="fa fa-cog"></i> Get Account Code</button>
    </span>
    </div>-->
    
</form>

  
 
     </div>
     </div>
      </div>
      

    </body>
</html>