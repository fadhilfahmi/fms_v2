<%-- 
    Document   : list_payer_view
    Created on : Oct 19, 2016, 3:41:36 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.ORPayer"%>
<%@page import="com.lcsb.fms.dao.financial.ap.VendorPaymentDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
%>
<!DOCTYPE html>

<div class="list-group"><%
    String keyword = request.getParameter("keyword");
    List<ORPayer> slist = (List<ORPayer>) OfficialReceiptDAO.getPayerHasInvoice(log, keyword);

                                    if (slist.isEmpty()) {%>
    <a class="list-group-item thisresult-payee">
        <div id="leftdiv>"<span class="font-small-red"><strong>No invoice available.</strong></span></div>
    </a>
    <%}
        for (ORPayer c : slist) {
    %>

    <a href="<%= c.getPayername()%>" id="<%= c.getPayercode()%>"   title="" class="list-group-item thisresult-payee">
        <div id="left_div"><%= c.getPayercode()%></div>
        <div id="right_div"><%= c.getPayername()%><span class="badge pull-right"><%= c.getTotalInvoice()%></span></div>
    </a>

    <%
        }
    %>
</div>