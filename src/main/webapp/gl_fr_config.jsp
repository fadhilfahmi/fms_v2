<%-- 
    Document   : gl_fr_config
    Created on : Mar 2, 2017, 9:59:34 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.financial.gl.FinancialReportDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ChartofAccountDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage=""%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) FinancialReportDAO.getModule();

%>


<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $(".goto").click(function () {
            var link = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + link,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".setting-button").click(function () {

            var link = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + link,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <div class="row">
                <div class="col-lg-6">
                    <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-6"><label>Report's Setting &nbsp;&nbsp;&nbsp;&nbsp;</label><small class="pull-right"> <i class="fa fa-info-circle" aria-hidden="true"></i> Set the content of reports and positioning the layout.</small></div>
            </div>
            
            <div class="row">
                <div class="col-lg-6">
                    <div class="list-group">

                        <a href="balancesheet_config" class="list-group-item goto">
                            <i class="fa fa-cog" aria-hidden="true"></i>&nbsp;&nbsp;Balance Sheet - Sheet No 1
                        </a>
                        <a href="gllisting" class="list-group-item goto">
                            <i class="fa fa-cog" aria-hidden="true"></i>&nbsp;&nbsp;General Ledger Listing
                        </a>
                        <a href="trialbalance" class="list-group-item goto">
                            <i class="fa fa-cog" aria-hidden="true"></i>&nbsp;&nbsp;Trial Balance
                        </a>
                    </div>
                </div>

            </div>        


            <br>


        </div>
    </div>
</div>
