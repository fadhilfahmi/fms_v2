<%-- 
    Document   : cf_coa_add
    Created on : Mar 11, 2016, 8:58:06 AM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ChartofAccountDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) ChartofAccountDAO.getModule();

%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/formValidation.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        var tab = sessionStorage.getItem(1);
        //$('#backto').attr('id', 'backto' + tab);
        //$('#savebutton').attr('id', 'savebutton' + tab);
        /*$('#saveform').formValidation({
         message: 'This value is not valid',
         icon: {
         valid: 'glyphicon glyphicon-ok',
         invalid: 'glyphicon glyphicon-remove',
         validating: 'fa fa-refresh'
         },
         fields: {
                 
         code: {
         required:true,
         validators: {
         notEmpty: {
         message: 'The Account Code is required'
         },
         digits: {
         message: 'The value can contain only digits'
         }
         }
         }
         }
         });*/

        $('#code').focusout(function (e) {

            var value = $(this).val();

            if (value == '') {
                $('#res_code').empty();
                $('#div_code').removeClass('has-success');
                $('#div_code').addClass('has-error');
                $('#res_code').append("<i class=\"fa fa-exclamation-circle fail-red\"></i>&nbsp;&nbsp;Account Code is required.");
                $('#savebutton').prop('disabled', true);
                return false;
            }
            if (numberOnly(value)) {
                $.ajax({
                    url: "ValidationController?process=checkcode&keyword=" + value,
                    success: function (result) {
                        // $("#haha").html(result);
                        //result = "<i class=\"fa fa-exclamation-circle fail-red\"></i>&nbsp;&nbsp;Code not available";
                        //result = "<i class=\"fa fa-check-circle right-green\"></i>&nbsp;&nbsp;Code available";
                        console.log(result);
                        $('#res_code').empty();
                        //$('#res_code').removeClass('has-error');
                        //$('#res_code').removeClass('has-success');
                        if (result != 1) {
                            $('#div_code').removeClass('has-error');
                            $('#div_code').addClass('has-success');
                            $('#res_code').append("<i class=\"fa fa-check-circle right-green\"></i>&nbsp;&nbsp;Code available");
                            $('#savebutton').prop('disabled', false);

                        } else {
                            $('#div_code').removeClass('has-success');
                            $('#div_code').addClass('has-error');
                            $('#res_code').append("<i class=\"fa fa-exclamation-circle fail-red\"></i>&nbsp;&nbsp;Code not available");
                            $('#savebutton').prop('disabled', true);
                        }


                    }
                });
            } else {
                $('#res_code').empty();
                $('#div_code').removeClass('has-success');
                $('#div_code').addClass('has-error');
                $('#res_code').append("<i class=\"fa fa-exclamation-circle fail-red\"></i>&nbsp;&nbsp;The code must be in numeric format");
                $('#savebutton').prop('disabled', true);
            }


            return false;
        });
    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addprocess" disabled><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>

            <div class="well">

                <form data-toggle="validator" role="form" id="saveform">
                    <div class="form-group" id="div_code">
                        <label for="inputName" class="control-label">Account &nbsp&nbsp<span id="res_code"></span></label>
                        <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" required>
                        <p></p>
                        <input type="text" class="form-control input-sm" id="descp" name="descp" placeholder="Description" required>
                    </div>

                    <div class="form-group form-group-sm">
                        <label for="inputName" class="control-label">Account Type</label>
                        <select class="form-control input-sm" id="type" name="type">
                            <%= ParameterDAO.parameterList(log,"Account Type", "")%>
                        </select>
                    </div>

                    <div class="form-group form-group-sm">
                        <label for="inputName" class="control-label">Apportion to Type</label>
                        <select class="form-control input-sm" id="apptype" name="apptype">
                            <%= ParameterDAO.parameterList(log,"Apportion Type", "")%>
                        </select>
                    </div>

                    <div class="form-group form-group-sm">
                        <label for="inputName" class="control-label">Apportion to Level</label>
                        <select class="form-control input-sm" id="applevel" name="applevel">
                            <%= ParameterDAO.parameterList(log,"Apportion to Level", "")%>
                        </select>
                    </div>

                    <div class="form-group form-group-sm">
                        <label for="inputName" class="control-label">Account to Type</label>
                        <select class="form-control input-sm" id="attype" name="attype">
                            <%= ParameterDAO.parameterList(log,"Account To Type", "")%>
                        </select>
                    </div>

                    <div class="form-group form-group-sm">
                        <label for="inputName" class="control-label">Final Level</label>
                        <select class="form-control input-sm" id="finallvl" name="finallvl">
                            <%= ParameterDAO.parameterList(log,"YesNo Type", "")%>
                        </select>
                    </div>

                    <div class="form-group form-group-sm">
                        <label for="inputName" class="control-label">Active</label>
                        <select class="form-control input-sm" id="active" name="active">
                            <%= ParameterDAO.parameterList(log,"YesNo Type", "")%>
                        </select>
                    </div>

                    <!--<div class="form-group-sm input-group">
                    <input type="text" class="form-control input-sm">
                    <span class="input-group-btn">
                    <button class="btn btn-default btn-sm" type="button"><i class="fa fa-cog"></i> Get Account Code</button>
                    </span>
                    </div>-->

                </form>
            </div>

        </div>
    </div>
</div>