<%-- 
    Document   : list_distribute
    Created on : Oct 25, 2016, 4:57:04 PM
    Author     : fadhilfahmi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
            
            $('#keyword').keyup(function(){
                var l = $('input[name=searchby]:checked').val();
                var m = $('input[name=method]:checked').val();
                var keyword = $(this).val();
                console.log(keyword);
                 $.ajax({
                        url: "list_search.jsp?table=chartofacccount&column="+l+"&keyword="+keyword+"&method="+m+"&code=code&descp=descp",
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
                }});
            });
            
            $('#result').on('click', '.thisresult', function(e) { 
                var a = $(this).attr('href');
                var b = $(this).attr('id');
                
                $('#<%= request.getParameter("code") %>').val(a);
                $('#<%= request.getParameter("descp") %>').val(b);
                e.preventDefault();
            });
            
        });
        </script>
    </head>
    <body>
         <div class="well">
            <form>
                <div class="row">
                    <div class="col-sm-12">
                        <label for="year" class="control-label input-group">Suspence Account has been used. Create Debit/Credit Note?</label>
                        <div class = "btn-toolbar" role = "toolbar">
                            
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default btn-sm">
                                    <input name="searchby" id="searchby" value="yes" type="radio" checked>Yes
                                </label>
                                <label class="btn btn-default btn-sm">
                                    <input name="searchby" id="searchby" value="no" type="radio">Not Now
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row">&nbsp;</div>
            
            <div id="result">

            </div>
            
        
         </div>   
    </body>
</html>
