<%-- 
    Document   : em_payroll_paysheet_print
    Created on : Jun 14, 2017, 1:09:20 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.template.voucher.Paysheet_Template"%>
<%@page import="com.lcsb.fms.util.dao.EstateDAO"%>
<%@page import="com.lcsb.fms.util.model.Estate"%>
<%@page import="com.lcsb.fms.model.setup.company.staff.Staff"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.employee.EmPayrollType"%>
<%@page import="com.lcsb.fms.model.financial.employee.EmPaySheetList"%>
<%@page import="com.lcsb.fms.dao.financial.employee.EmPayrollDAO"%>
<%@page import="com.lcsb.fms.template.voucher.FormattedVoucher"%>
<%@page import="com.lcsb.fms.template.voucher.VoucherMaster"%>
<%@page import="com.lcsb.fms.template.voucher.PaymentVoucher_Template"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="java.util.List"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Estate est = EstateDAO.getEstateInfo(log, log.getEstateCode(), "estatecode");
    String refer = request.getParameter("refer");

%>

<link href="css/print_landscape.css?1" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $('#print').click(function (e) {

            printLandscape($("#viewjv").html());

        });

    });
</script>



<div id ="maincontainer">
    <button id="print" class="btn btn-default btn-sm" title="" name="viewlist"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
    <p></p>
    <table id="maintbl" width="100%" cellspacing="0" cellpadding="10" border="0">
        <tr>
            <td class="greybg">
                <div id="viewjv" style="overflow-y: scroll; height:500px;">

                    <%= Paysheet_Template.printT(refer, log).getOutput()%>
                </div>

            </td>
        </tr>
    </table>
</div>

