<%@page import="com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO"%>
<%@page import="com.lcsb.fms.model.financial.ap.VendorPayee"%>
<%@page import="com.lcsb.fms.util.dao.SupplierDAO"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) OfficialReceiptDAO.getModule();
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>

<link href="bootstrap-form-wizard/form-wizard.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="bootstrap-form-wizard/cb-officialreceipt-step.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });
        //$('#JVdate').datepicker({ dateFormat: 'yyyy-mm-dd' });

        $('#tarikh').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        });

        

        $("#total").keyup(function () {
            var val = $(this).val();
            var to = 'amount';
            //alert(val);
            //alert(checkRegexp(val,to));
            if (checkRegexp(val, to)) {
                $('#res_digit').empty();
                $('#div_digit').removeClass('has-error');
                $('#div_digit').addClass('has-success');
                $('#res_digit').append("<i class=\"fa fa-check-circle right-green\"></i>&nbsp;&nbsp;");
                $('#savebutton').prop('disabled', false);
                convertRM(val, to);
            } else {
                $('#res_digit').empty();
                $('#div_digit').removeClass('has-success');
                $('#div_digit').addClass('has-error');
                $('#res_digit').append("<i class=\"fa fa-exclamation-circle fail-red\"></i>&nbsp;&nbsp;Numeric Formatted Only");
                $('#savebutton').prop('disabled', true);
            }
            //
        });

        $('#getbuyer').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Buyer Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_buyer.jsp?name=bname&code=bcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });
        $('#getbank').click(function (e) {

            var a = 'bankcode';
            var b = 'bankname';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Bank',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_bank.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getcheque').click(function (e) {

            var a = 'cekno';
            var bankcode = $('#bankcode').val();
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Cheque No',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_cheque.jsp?bankcode=' + bankcode + '&cekcolumn=' + a);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#gettype').click(function (e) {
            //e.preventDefault();

            var a = 'paidcode';
            var b = 'paidname';
            var c = 'paidaddress';
            var d = 'paidpostcode';
            var e = 'paidcity';
            var f = 'paidstate';
            var g = 'gstid';

            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Paid Type',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_type.jsp?code=' + a + '&name=' + b + '&address=' + c + '&postcode=' + d + '&city=' + e + '&state=' + f + '&gstid=' + g);
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $("#paymentmode").change(function () {

            var thisval = $(this).val();

            if (thisval == 'Cheque') {
                $('#getcheque').prop('disabled', false);
            } else {
                $('#getcheque').prop('disabled', true);
            }

        });

        $.ajax({
            url: "list_payer.jsp",
            success: function (result) {
                $('#list-payee').empty().html(result).hide().fadeIn(300);

            }});

        $('#saveall').click(function (e) {
            //e.preventDefault();
            var sessionID = $('#sessionid').val();
            $.ajax({
                url: "PathController?moduleid=020203&process=savevoucher&sessionid=" + sessionID,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });


    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addinv" ><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>

            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                        <p>Choose Payer</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                        <p>Choose Invoice(s)</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                        <p>Amount to Pay</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                        <p>Prepare Receipt</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                        <p>Finalizing Items</p>
                    </div>
                </div>
            </div>





            <input type="hidden" id="millcode" value="<%= log.getEstateCode()%>">
            <input type="hidden" id="sessionid" value="<%= request.getParameter("sessionid")%>">
            <input type="hidden" id="buyercode">


            <div class="row setup-content" id="step-1">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            List of Payee
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="row">
                                    <!--list of supplier-->
                                    <div class="well">

                                        <div id="list-payee">

                                        </div>

                                    </div>
                                    <!--list of supplier-->
                                </div>
                                <button class="btn btn-default nextBtn btn-sm pull-right payeeBtn" type="button" disabled>Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row setup-content" id="step-2">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Choose Contract
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div id="list-invoice">

                                </div>
                                <button class="btn btn-default nextBtn btn-sm pull-right" id="invoice-btn" type="button" disabled>Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-3">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Complete Refinery
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div id="amount-to-pay">
                                    <span class="text-center"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i>
                                        Please wait, downloading refinery data from mill...</span>
                                </div>
                                <button class="btn btn-default nextBtn btn-sm pull-right" type="button" >Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-4">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Prepare Voucher
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div id="prepare-voucher">


                                </div>
                                <button class="btn btn-default nextBtn btn-sm pull-right" id="prepare-btn" type="button" disabled >Next</button>
                            </div>
                        </div></div></div>
            </div>
            <div class="row setup-content" id="step-5">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Finalizing
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div id="finalize-voucher">


                                </div>
                                <button class="btn btn-success btn-sm pull-right" id="saveall" type="submit">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


