<%-- 
    Document   : calculatedate
    Created on : Oct 11, 2016, 5:12:05 PM
    Author     : fadhilfahmi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="http://code.jquery.com/jquery-2.0.2.min.js" integrity="sha256-TZWGoHXwgqBP1AF4SZxHIBKzUdtMGk0hCQegiR99itk=" crossorigin="anonymous"></script>
        <script   src="http://code.jquery.com/ui/1.10.4/jquery-ui.min.js"   integrity="sha256-oTyWrNiP6Qftu4vs2g0RPCKr3g1a6QTlITNgoebxRc4="   crossorigin="anonymous"></script>
        <script type="text/javascript" charset="utf-8">
     
	$(document).ready(function() {
        $(".date").datepicker({ dateFormat: 'dd/mm/yy' });
$('#CalculateDate').on('click', function(){
    var interval = parseInt($('.interval').val());
    var unit = $('#DateUnit').val();
    var $date = $('.date').datepicker("getDate");
    var $result = $('#result');
    var resultDate = new Date();
    switch(unit){
        case '1':      
            resultDate.setDate($date.getDate() + interval);
            $result.html(resultDate.toString());
            break;        
        case '2':
            //alert(interval);
            //alert($date);    
            var daysToAdd = 7 * interval;
            resultDate.setDate($date.getDate() + daysToAdd);
            $result.html(resultDate.toString());
            break;
        case '3':
            //alert(interval);
            //alert($date);    
            resultDate.setMonth($date.getMonth() + interval);
            $result.html(resultDate.toString());
            break;
        case '4':
            //alert(interval);
            //alert($date); 
            resultDate.setYear($date.getYear() + interval);
            $result.html(resultDate.toString());   
            break;
    }
});
    });
        </script>    
        
    </head>
    <body>
        Date: <input type="text" class="date" />
<br/>
Interval: <input type="text" class="interval"/>
<br />
Unit: <select name="option" id="DateUnit">
	<option value="1">Days</option>
	<option value="2">Weeks</option>
	<option value="3">Months</option>
	<option value="4">Years</option>
</select>
<br/>
<button id="CalculateDate">Calculate</button></br />
Next due date: <span id="result">test</span>
    </body>
</html>
