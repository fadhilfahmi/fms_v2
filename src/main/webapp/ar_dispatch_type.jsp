<%--  
    Document   : ar_dispatch_list 
    Created on : Aug 1, 2017, 3:20:26 PM 
    Author     : fadhilfahmi 
--%> 
 
<%@page import="com.lcsb.fms.dao.financial.ar.ArDispatchDAO"%> 
<%@page import="com.lcsb.fms.dao.financial.gl.FinancialReportDAO"%> 
<%@page import="com.lcsb.fms.util.model.Module"%> 
<%@page import="com.lcsb.fms.dao.setup.configuration.ChartofAccountDAO"%> 
<%@page import="com.lcsb.fms.general.AccountingPeriod"%> 
<%@page import="com.lcsb.fms.util.model.LoginProfile"%> 
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%> 
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage=""%> 
<% 
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail"); 
    Module mod = (Module) ArDispatchDAO.getModule(); 
 
%> 
 
 
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script> 
<script type="text/javascript" charset="utf-8"> 
    $(document).ready(function () { 
        $(".goto").click(function () { 
            var link = $(this).attr('href'); 
            $.ajax({ 
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=dispatchlist&type=" + link, 
                success: function (result) { 
                    // $("#haha").html(result); 
                    $('#maincontainer').empty().html(result).hide().fadeIn(300); 
                } 
            }); 
            return false; 
        }); 
 
        $(".setting-button").click(function () { 
 
            var link = $(this).attr('id'); 
            $.ajax({ 
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + link, 
                success: function (result) { 
                    $('#herey').empty().html(result).hide().fadeIn(300); 
                } 
            }); 
 
            return false; 
        }); 
 
    }); 
</script> 
 
<div id ="maincontainer"> 
    <div class="partition">  
        <div class="bodyofpartition"> 
             
            <div class="row"> 
                <div class="col-lg-6"><label>Dispatch Note Type &nbsp;&nbsp;&nbsp;&nbsp;</label><small class="pull-right"> <i class="fa fa-info-circle" aria-hidden="true"></i> Set the content of reports and positioning the layout.</small></div> 
            </div> 
            <div class="row"> 
                <div class="col-lg-6"> 
                    <div class="list-group"> 
                        <a href="cpo" class="list-group-item goto"> 
                            <i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp;&nbsp;CPO 
                        </a> 
                        <a href="palmkernel" class="list-group-item goto"> 
                            <i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp;&nbsp;Palm Kernel 
                        </a> 
                        <a href="ffb" class="list-group-item goto"> 
                            <i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp;&nbsp;FFB 
                        </a> 
                        <a href="sludge" class="list-group-item goto"> 
                            <i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp;&nbsp;Sludge Oil 
                        </a> 
                        <a href="shell" class="list-group-item goto"> 
                            <i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp;&nbsp;Shell 
                        </a> 
                         
                    </div> 
                </div> 
            </div> 
            <br> 
        </div> 
    </div> 
</div> 