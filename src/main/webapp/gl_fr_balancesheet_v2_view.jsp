<%-- 
    Document   : gl_fr_balancesheet_v2_view
    Created on : Oct 5, 2015, 12:02:22 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.report.financial.balancesheet.BSParam"%>
<%@page import="com.lcsb.fms.report.financial.balancesheet.BsItem"%>
<%@page import="com.lcsb.fms.report.financial.balancesheet.BsMaster"%>
<%@page import="com.lcsb.fms.report.financial.balancesheet.BalanceSheetDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.model.report.BalancesheetModel"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) BalanceSheetDAO.getModule();
    BSParam prm = (BSParam) session.getAttribute("bs_param");
    BsMaster mt = BalanceSheetDAO.generateBalanceSheet(prm, log);


%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#print').click(function (e) {
            printReport($("#viewprint").html());
        });

        $(".goto").click(function () {
            var link = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + link,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


    });
</script>
<link href="css/table_style.css" rel="stylesheet" type="text/css" />
<link href="css/pvoucher.css" rel="stylesheet" type="text/css" />
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="balancesheet_option"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="print"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addinv" ><i class="fa fa-floppy-o"></i>&nbsp;Print</button>
                    </td>
                </tr>
            </table>
            <br>
            <div id="viewprint">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead
                    <tr>
                        <td colspan="4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="50px"><img src="image/logo_lkpp.jpg" width="40px" style="vertical-align:middle" ></td>
                                    <td><span class="bigtitlebold"><%= log.getEstateDescp()%></span><span class="pull-right"></span><p class="font_inheader_normal">BALANCE SHEET AS AT <%= AccountingPeriod.fullDateMonth(AccountingPeriod.getEndofPeriod(log,prm.getYear(), prm.getPeriod())) %></p></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="border_bottom"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td width="15%" align="right"><p class="column_head_style">THIS YEAR</p></td>
                        <td width="15%" align="right"><p class="column_head_style">PREVIOUS YEAR</p></td>
                    </tr>
                    </thead>
                    <tbody>
                    <%
                        //BsMaster mt = BalanceSheetDAO.getMaster(year, period, otype, vby, "12", log);
                        List<BsItem> listAll = (List<BsItem>) mt.getListItem();

                        for (BsItem j : listAll) {
                            String indent = "";

                            if (j.getLevel() == 2) {
                                indent = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                            } else if (j.getLevel() == 3) {
                                indent = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                            } else {
                                indent = "&nbsp;&nbsp;&nbsp;";
                            }
                    %>
                    <tr>
                        <td><%=indent%><span class="title_row_font_sub"><%//= j.getCoacode()%> <%= j.getCoadescp()%></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"><%= j.getCurrentAmount()%></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"><%= j.getPrevAmount()%></span></td>
                    </tr>
                  
                        <%} %>
                    <tr>
                        <td>&nbsp;<span class="title_row_font_main"></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"></span></td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td>&nbsp;<span class="title_row_font_main"></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"></span></td>
                        <td>&nbsp;</td>
                    </tr>
                    <!--<tr>
                        <td>&nbsp;<span class="total_row_font_bold">NET CURRENT ASSET</span></td>
                        <td align="right" >&nbsp;<span class="total_row_font_bold"></span></td>
                        <td align="right" >&nbsp;<span class="total_row_font_bold"></td>
                    </tr>-->

                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
</div>