<%-- 
    Document   : ar_inv_summary_view_printed
    Created on : Sep 5, 2017, 9:44:15 AM
    Author     : fadhilfahmi
--%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.template.voucher.VoucherMaster"%>
<%@page import="com.lcsb.fms.template.voucher.FormattedVoucher"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    FormattedVoucher v = (FormattedVoucher) VoucherMaster.getMaster(request.getParameter("refer"), log);
    String token = request.getParameter("status");
%>

<link href="css/pvoucher.css?1" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $('#print').click(function (e) {

            printElement($("#viewjv").html());

        });

        

    });
</script>
<table width="100%" cellspacing="0" border="0">
    <tr>
        <td valign="middle" width="33%" align="left">
            <div class="btn-group btn-group-sm" role="group" aria-label="...">


                <div class="btn-group" role="group" aria-label="...">
                    <button id="<%= token %>" class="btn btn-default btn-xs backtosummary" title="<%= v.getModuleid()%>" name="viewsummary" type="<%= request.getParameter("refer")%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;Back</button>

                    <button id="print" class="btn btn-default btn-xs" title="<%//= mod.getModuleID()%>" name="viewlist"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
                    <%
                        if (token.equals("Prepared")) {
                    %>
                    <button id="<%= request.getParameter("refer") %>" class="btn btn-success btn-xs checknow" title="<%= request.getParameter("moduleid") %>"  name="<%//= topath%>" type="<%= token%>"><i class="fa fa-check-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;Check</button>
                    <%
                    } else if (token.equals("Checked")) {
                    %>
                    <button id="<%= request.getParameter("refer") %>" class="btn btn-success btn-xs approvenow" title="<%= request.getParameter("moduleid") %>"  name="<%//= topath%>" type="<%= token%>"><i class="fa fa-check-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;Approve</button>
                    <%
                        }
                    %>

                </div>
            </div>
            <span>&nbsp;&nbsp;<strong><%= v.getRefer()%></strong> | Printable Page : <%= v.getPage()%></span>
        </td>
        <td width="60%" style="vertical-align: middle;">
            <!--<div class="form-group" id="div_code">
                
                <select class="form-control input-xs" id="reflexcb" name="reflexcb">
            <%//= ParameterDAO.parameterList("YesNo Type", "No")%>
        </select>   
    </div>-->
        </td>
    </tr>
</table>
<br>
<table id="maintbl" width="100%" cellspacing="0" border="0">
    <tr>
        <td class="greybg">
            <table id="table_left" width="100%" cellspacing="0">
                <tr>
                    <td width="100%">
                        <div id="viewjv" style="overflow-y: scroll; height:600px;">
                            <%= v.getOutput()%>
                        </div>
            </table>
        </td>
    </tr>
    <input type="hidden" id="checkbyid" value="">
    <input type="hidden" id="checkbyname" value="">
    <input type="hidden" id="checkbydesign" value="">
    <input type="hidden" id="checkdate" value="">
</table>