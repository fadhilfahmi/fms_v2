<%-- 
    Document   : ar_inv_check_contract
    Created on : Oct 13, 2016, 10:58:26 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArCPORefinery"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) SalesInvoiceDAO.getModule();
%>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        //var oTable = $('#list1').DataTable( {
        //    responsive: true,
        //    "aaSorting": [[ 2, "desc" ]],
        //} );

        //$('#list1 tbody').on('click', 'tr', function (e) {
        //   var data = oTable.row( this ).data();

        //   $('#selected-contract').val(data[1]);
        //alert(data[0]);
        //e.preventDefault();



        // });
        $('#refinery-body').on('change', '.mt_refinery_gen', function (e) {

            var procCode = $('#prodcode').val();

            var b = $(this).attr('id');
            var v = $(this).val();
            var z = b.substring(11);
            console.log(procCode);

            $('#updatevalstatus' + z).empty().html('<i class="fa fa-spinner fa-spin fa-fw"></i>').hide().fadeIn(100);

            e.preventDefault();
            $.ajax({
                url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=updaterefinery&dispatchno=" + z + "&newvalue=" + v + "&prodcode=" + procCode,
                success: function (result) {
                    if (result == 1) {
                        setTimeout(function () {
                            var newstatusicon = '&nbsp;<i class="fa fa-check-circle" style="color:#139104" aria-hidden="true"></i>';
                            $('#updatevalstatus' + z).empty().html(newstatusicon).hide().fadeIn(100);
                        }, 500);

                    } else {

setTimeout(function () {
                            var newstatusicon = '&nbsp;<i class="fa fa-exclamation-circle" style="color:#c4313d" aria-hidden="true"></i>';
                            $('#updatevalstatus' + z).empty().html(newstatusicon).hide().fadeIn(100);
                        }, 500);
                    }
                    

                }
            });
            return false;

        });

        $('#refinery-body').on('keyup', '.mt_refinery_gen', function (e) {

            var disno = [];
            var x = 0;
            var j = 0;

            $.each($("input[name='checkboxdisno']:checked"), function () {
                disno.push($(this).val());
            });

            console.log(disno);

            for (i = 0; i < disno.length; i++) {
                j = $('#mt_refinery' + disno[i]).val();
                //x = j + x;
                x = (parseFloat(j) + parseFloat(x));
                //console.log(i+'---'+j);
                //console.log($('#mt_refinery'+i).val());
            }



            $('#sum_mt_refinery').val(x.toFixed(2));

            //var number1 = Number(oriamount.replace(/[^0-9\.]+/g,""));
            //    var number2 = Number(newamount.replace(/[^0-9\.]+/g,""));
            //    var bal = parseFloat(number1)-parseFloat(number2);



            //console.log('totalrow='+t);
            console.log('total=' + x);


        });



        $('#refinery-body').on('click', '.tdrow-contract', function (e) {
            //e.preventDefault();
            var b = $(this).attr('id');

            //$("#"+b).toggle();
            //$(this).toggleClass('info');
            $("#" + b).toggleClass('success');


            var checkBox = $('#chkbox' + b);
            checkBox.prop("checked", !checkBox.prop("checked"));

            var input1 = $('#mt_mill' + b);
            var input2 = $('#mt_refinery' + b);

            input1.prop("disabled", !input1.prop("disabled"));
            input2.prop("disabled", !input2.prop("disabled"));
            $('.mt_refinery_gen').trigger('keyup');

        });

        $('.adddispatchno').click(function (e) {

            var procCode = $('#prodcode').val();
            var tbl = '';
            if (procCode == '01') {
                tbl = 'qc_cpo_grade';
            } else if (procCode == '02') {
                tbl = 'qc_kernel_grade';
            }
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Dispatch',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_dispatchno.jsp?table=' + tbl);
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });



    });
</script>
<button  class="btn btn-default btn-xs adddispatchno" title="" id="" name="adddispatchno"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Dispatch No</button> <small class="pull-right">&nbsp;<i class="fa fa-info-circle" aria-hidden="true"></i> Click on the row to select Dispatch</small>
<form data-toggle="validator" role="form" id="saverefinery">
    <input type="hidden" id="selected-contract">

    <p></p>
    <table class="table table-bordered table-striped table-hover" id="list1">
        <thead>

            <tr>
                <th></th>
                <th>Date</th>
                <th>Dispatch No</th>
                <th>Trailer</th>
                <th>Mt from Mill</th>
                <th>Mt from Refinery</th>
            </tr>
        </thead>
        <tbody id="refinery-body">
            <%List<ArCPORefinery> listx = (List<ArCPORefinery>) SalesInvoiceDAO.getListProd(log, request.getParameter("contract"), request.getParameter("millcode"), request.getParameter("prodcode"));
                int i = 0;
                double sumMt = 0.0;
                double sumMtRefinery = 0.0;
                for (ArCPORefinery j : listx) {

                    i++;
                    sumMt += Double.parseDouble(j.getMt());
                    sumMtRefinery += j.getMtRefinery();
            %>
            <tr id="<%= j.getDispatchNo()%>" class="activerowy1">
                <td width="3%" class="tdrow-contract" id="<%= j.getDispatchNo()%>">
                    <input type="checkbox" id="chkbox<%= j.getDispatchNo()%>" name="checkboxdisno" class="chcbox-contract" value="<%= j.getDispatchNo()%>" style="opacity:0;" required>
                </td>
                <td class="tdrow-contract" id="<%= j.getDispatchNo()%>"><input type="hidden" id="date<%= j.getDispatchNo()%>" name="date<%= j.getDispatchNo()%>" value="<%= j.getDate()%>"><%= j.getDate()%></td>
                <td class="tdrow-contract" id="<%= j.getDispatchNo()%>"><input type="hidden" id="dispatchno<%= j.getDispatchNo()%>" name="dispatchno<%= j.getDispatchNo()%>" value="<%= j.getDispatchNo()%>"><%= j.getDispatchNo()%></td>
                <td class="tdrow-contract" id="<%= j.getDispatchNo()%>"><input type="hidden" id="trailer<%= j.getDispatchNo()%>" name="trailer<%= j.getDispatchNo()%>" value="<%= j.getTrailer()%>"><%= j.getTrailer()%></td>
                <td class="tdrow"><input type="text" class="form-control input-sm" id="mt_mill<%= j.getDispatchNo()%>" name="mt_mill<%= j.getDispatchNo()%>" value="<%= j.getMt()%>" required disabled></td>
                <td class="tdrow" style="vertical-align: middle;"><input type="text" class="form-control input-sm mt_refinery_gen" id="mt_refinery<%= j.getDispatchNo()%>" name="mt_refinery<%= j.getDispatchNo()%>" value="<%= j.getMtRefinery()%>" onClick="this.select();" required disabled style="float: left; width:85%"><!--<i class="fa fa-circle-o fa-lg" aria-hidden="true" style="float: right;vertical-align: middle;"></i>--><div style="float: right;vertical-align: middle" width="20%"><span id="updatevalstatus<%= j.getDispatchNo()%>" class="pull-left" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></div></td>
            </tr>
            <%}%>
            <tr id="<%//= j.getId()%>">
                <td></td>
                <td colspan="3"><span class="pull-right"><strong>Total</strong></span></td>
                <td class="tdrow">
                    <input type="hidden" id="totalrow" name="totalrow" value="<%=i%>">
                    <input type="hidden" id="contractno" name="contractno" value="<%= request.getParameter("contract")%>">
                    <input type="text" class="form-control input-sm" id="sum_mt_mill" name="sum_mt_mill" value="<%= GeneralTerm.normalCredit(sumMt)%>" required></td>
                <td class="tdrow"><input type="text" class="form-control input-sm" id="sum_mt_refinery" name="sum_mt_refinery" value="<%= GeneralTerm.normalCredit(sumMtRefinery)%>" required></td>
            </tr>
        </tbody>

    </table>
</form>