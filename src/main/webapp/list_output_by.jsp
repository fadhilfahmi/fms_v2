<%-- 
    Document   : list_location_by
    Created on : Mar 2, 2016, 11:32:25 AM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.OutputDAO"%>
<%@page import="com.lcsb.fms.util.model.Output"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>

<!DOCTYPE html>
<div class="list-group"><%
            
            String by = request.getParameter("by");
            String keyword = request.getParameter("keyword");
            
            List<Output> slist = (List<Output>) OutputDAO.getOutput(log,by,keyword);
            for (Output c : slist) { 
               %>
       
            <a href="<%= c.getOutputdesc()%>" id="<%= c.getOutputcode()%>" unitmeasure="<%= c.getMeasure() %>" class="list-group-item thisresult_nd">
                <div id="left_div"><%= c.getOutputcode()%></div>
                <div id="right_div"><%= c.getOutputdesc()%></div>
            </a>

               <%    
            } 
        %>
</div>
