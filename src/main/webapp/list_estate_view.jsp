<%-- 
    Document   : list_estate_view
    Created on : Mar 4, 2016, 4:06:59 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.Estate"%>
<%@page import="com.lcsb.fms.util.dao.EstateDAO"%>
<%@page import="com.lcsb.fms.util.dao.TaxDAO"%>
<%@page import="com.lcsb.fms.util.model.Tax"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<div class="list-group"><%
            
            String keyword = request.getParameter("keyword");
            List<Estate> slist = (List<Estate>) EstateDAO.getAllEstate(log,keyword);
            for (Estate c : slist) { 
               %>
       
            <a href="<%= c.getEstatedescp()%>" id="<%= c.getEstatecode()%>" title="" class="list-group-item thisresult">
                <div id="left_div"><%= c.getEstatecode()%></div>
                <div id="right_div"><%= c.getEstatedescp()%></div>
            </a>

               <%    
            } 
        %>
</div>