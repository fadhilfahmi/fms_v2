<%-- 
    Document   : gl_accountperiod_add
    Created on : Mar 11, 2016, 2:57:02 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.dao.financial.gl.AccountPeriodDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ChartofAccountDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) AccountPeriodDAO.getModule();

%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" type="text/css" media="all" href="bootstrap-daterangepicker-master/daterangepicker.css" />
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="bootstrap-daterangepicker-master/moment.js"></script>
<script type="text/javascript" src="bootstrap-daterangepicker-master/daterangepicker.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $('.range').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            }
        });
//function(start, end, label) { console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')'); });
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: false
        });

    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>

            <div class="well">

                <form data-toggle="validator" role="form" id="saveform">

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Year &nbsp&nbsp<span id="res_code"></span></label>
                                <select class="form-control input-sm" name="year" id="year">
                                    <%= ParameterDAO.parameterList(log,"Year", AccountingPeriod.getCurYear(log))%>
                                </select> 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Start Year Date &nbsp&nbsp<span id="res_code"></span></label>
                                <input type="text" class="form-control input-sm datepicker" id="startyear" name="startyear"  autocomplete="off">   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">End Year Date &nbsp&nbsp<span id="res_code"></span></label>
                                <input type="text" class="form-control input-sm datepicker" id="endyear" name="endyear"  autocomplete="off" >   
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <label for="inputName" class="control-label">Setup Accounting Period &nbsp&nbsp<span id="res_code"></span></label>
                        </div>
                    </div>

                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Period</th>
                                <th>
                                    Current<small></small>
                                </th>
                                <th>
                                    Range<small></small>
                                </th>
                                <%
                                    for (int i = 1; i < 13; i++) {
                                %>
                            <tr>
                                <td><%= i%></td>
                                <td><input type="radio" name="current" id="current_"<%= i%> value="<%= i%>"></td>
                                <td>
                                    <div class="form-group-sm input-group">
                                        <input type="text" id="range_<%= i%>" name="range_<%= i%>" class="form-control range">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-sm" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </td>
                            </tr>

                            <%
                                }
                            %>                                  

                            </tbody>
                    </table>      
                </form>
            </div>
        </div>
    </div>
</div>
