<%-- 
    Document   : index
    Created on : May 6, 2016, 4:20:22 PM
    Author     : fadhilfahmi
--%>

<%@page import="java.sql.Connection"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.util.model.LogError"%>
<%@page import="com.lcsb.fms.util.dao.CompanyDAO"%>
<%@page import="com.lcsb.fms.util.model.Company"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LogError log = (LogError) session.getAttribute("login_error");

    String alert_class = "";

    if (log != null && log.isHaveError()) {
        if (log.getMessageid().equals("0002")) {
            alert_class = "notice-warning";

        } else if (log.getType().equals("Danger")) {
            alert_class = "notice-danger";
        }

    }

    if (ConnectionUtil.getHQConnection() != null) {
        ConnectionUtil.closeHQConnection();
    }

    if (session.getAttribute("sessionconnection") != null) {
        //SessionConnection sessionConnection = (SessionConnection) session.getAttribute("sessionconnection");
        //ConnectionUtil.closeSessionConnection(session.getAttribute("sessionconnection"));
        session.removeAttribute("sessionconnection");
        session.removeAttribute("sessionuser");
        session.removeAttribute("sessionpassword");
        session.removeAttribute("sessioncompany");
    }

%>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>FMS | Financial Management System</title>


        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">



        <!-- Favicon and touch icons
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png"> -->

        <link rel="apple-touch-icon" sizes="57x57" href="assets/fav/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="assets/fav/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="assets/fav/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="assets/fav/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="assets/fav/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="assets/fav/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="assets/fav/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="assets/fav/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="assets/fav/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="assets/fav/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="assets/fav/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="assets/fav/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="assets/fav/favicon-16x16.png">
        <link rel="manifest" href="assets/fav/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="assets/fav/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <%    Connection con = ConnectionUtil.getTemporaryConnection();
    if (con != null) {%>


        <style>
            .notice {
                padding: 15px;
                background-color: #fafafa;
                border-left: 6px solid #7f7f84;
                text-align: center;
                margin-bottom: 10px;
                -webkit-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
                -moz-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
                box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
            }
            .notice-sm {
                padding: 10px;
                font-size: 80%;
            }

            .notice-danger {
                border-color: #d73814;
            }
            .notice-warning {
                border-color: #FEAF20;
            }
        </style>

        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {

            <%                    if (log != null && log.isHaveError()) {
            %>
                var drawDiv = '<div class="col-sm-6 col-sm-offset-3">';
                drawDiv += '<div class="notice notice-sm <%= alert_class%>">';
                drawDiv += '<strong>Warning</strong> | <%= log.getMessage()%>';
                drawDiv += '</div>';
                drawDiv += '</div>';
                $("#alert").html(drawDiv);
            <%
                }
            %>


            });
        </script>

    </head>

    <body>


        <!-- Top content -->
        <div class="top-content">

            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text" style="text-align: center">
                            <img src="image/logovertical.png" width="350"  style="padding-top:0px">
                        </div>
                    </div>
                    <div class="row" id="alert">

                    </div>
                    <form name="form1" action="SessionLogin" target="_parent" method="post">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3 form-box">
                                <div class="form-top">
                                    <div class="form-top-left">
                                        <h3>Please Login</h3>
                                        <p>Enter your username and password to log on:</p>
                                    </div>
                                    <div class="form-top-right">
                                        <i class="fa fa-lock"></i>
                                    </div>
                                </div>
                                <div class="form-bottom">
                                    <div class="form-group">
                                        <label class="sr-only" for="form-username">Username</label>
                                        <input type="text" name="user" placeholder="Username..." class="form-username form-control" id="form-username">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="form-password">Password</label>
                                        <input type="password" name="password" placeholder="Password..." class="form-password form-control" id="form-password">
                                    </div>
                                    <div class="form-group styled-select">
                                        <select class="form-password form-control input-sm" id="estate" name="estate">
                                            <option value="none" selected>Company Code</option>
                                            <%
                                                List<Company> clist = (List<Company>) CompanyDAO.getAllCompany();
                                                for (Company c : clist) {
                                            %>
                                            <option value="<%= c.getCode()%>"><%= c.getCode()%> - <%= c.getName()%></option>
                                            <%
                                                }
                                            %>

                                        </select>
                                    </div>


                                    <button type="submit" class="btn" id="login">Log in</button>

                                </div>
                            </div>
                        </div>

                    </form>

                </div>
                <%
                    String jspPath
                            = application.getRealPath(request.getServletPath());
                    java.io.File jspFile = new java.io.File(jspPath);
                    java.util.Date lastModified = new java.util.Date(jspFile.lastModified());
                    java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("dd.MM.yyyy hh:mm a");
                    //out.println("Last Modified " +fmt.format(lastModified));
                %>
            </div>

        </div>


        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>



    </body>

    <%} else {%>

    <link href="bower_components/bootstrap.min.css" rel="stylesheet">

    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="jsfunction/jquery-1.12.0.min.js"></script>
    <!-- Custom Theme Style -->
    <link href="css/custom/custom.css" rel="stylesheet">
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <!-- page content -->
            <div class="col-md-12">
                <div class="col-middle">
                    <div class="text-center">
                        <h1 class="error-number"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></h1>
                        <h2>Internal Server Error (500)</h2>
                        <p>Connection to the database failed. Please make sure MySQL server is running.
                        </p>
                        <p>Contact administrator at Ext 162 or email fadhilfahmi@lcsb.com.my</p>

                    </div>
                </div>
            </div>
            <!-- /page content -->
        </div>
    </div>
</body>

<%}%>
</html>
