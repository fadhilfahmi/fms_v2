<%-- 
    Document   : cb_cv_add
    Created on : Mar 17, 2016, 10:33:19 AM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.dao.setup.company.MillDAO"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.CashVoucherDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) SalesInvoiceDAO.getModule();
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>

<link href="bootstrap-form-wizard/form-wizard.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });
        //$('#JVdate').datepicker({ dateFormat: 'yyyy-mm-dd' });

        $('#JVdate').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        });

        $('#saveall').click(function (e) {
            //e.preventDefault();
            var sessionid = $('#sessionid').val();
            $.ajax({
                url: "PathController?moduleid=020903&process=saveinvoice&sessionid=" + sessionid,
                success: function (result) {
                    $('#maincontainer').remove();
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $('#getaccount').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Chart of Account',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_coa.jsp');
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                    });

                    return $content;
                }
            });

            return false;
        });



        $('#getbuyer').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Buyer Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_buyer_mill.jsp?name=bname&code=bcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getproduct').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Product Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_product.jsp?name=prodname&code=prodcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn');

        allWells.hide();
        var ch = 0;
        navListItems.click(function (e) {
            ch++;
            e.preventDefault();
            var $target = $($(this).attr('href')),
                    $item = $(this);
            //console.log($target);
            //console.log($item);
            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url']"),
                    isValid = true;

            if (curStepBtn == 'step-1') {
                //alert(2);

                var procCode = $('#prodcode').val();
                var procName = $('#prodname').val();
                var bCode = $('#bcode').val();
                var bName = $('#bname').val();

                var path = '';

                if (procCode == '03') {
                    path = 'ar_inv_add_ffb_param.jsp';
                    $('#step-2-title').html('FFB Parameter');
                    $('#step-3-title').html('FFB Calculation');
                    $('#step-2-string').html('Enter the FFB Parameter for calculation.');
                    $('#step-3-string').html('Check the calculation.');
                } else {
                    path = 'ar_inv_add_step.jsp';
                    $('#step-2-title').html('Choose Contract');
                    $('#step-2-string').html('Choose <strong>' + procName + '</strong> Contract for <strong>' + bName + '</strong>');
                    $('#step-3-title').html('Complete Refinery');
                    $('#step-3-string').html('Complete Refinery');
                }

                $.ajax({
                    url: path + "?buyercode=" + bCode + "&prodcode=" + procCode,
                    success: function (result) {
                        $('#table-result').empty().html(result).hide().fadeIn(300);

                    }
                });


            }

            if (curStepBtn == 'step-2') {
                var procCode = $('#prodcode').val();
                var millCode = $('#millcode').val();
                var sessionid = $('#sessionid').val();
                var contractNo = '';
                var path = '';
                var next = false;

                if (procCode == '03') {
                    var a = $("#saveffb :input").serialize();
                    $.ajax({
                        async: false,
                        data: a,
                        type: 'POST',
                        url: "ProcessController?moduleid=020903&process=calculateffb&sessionid=" + sessionid,
                        success: function (result) {
                            //$('#maincontainer').empty().html(result).hide().fadeIn(300);
                        }
                    });
                    path = 'ar_inv_add_ffb_calculate.jsp?';
                    next = true;
                } else {
                    contractNo = $('#selected-contract').val();

                    var origin = contractNo.substring(0, 3);

                    if (origin == 'SAG') {
                        $.ajax({
                            async: false,
                            url: "ProcessController?moduleid=020903&process=generatedisno&prodcode=06",
                            success: function (result) {
                                b = result;
                                path = 'ar_inv_add_shell.jsp?dispatchno=' + b + '&';
                                next = true;

                            }
                        });

                    } else {

                        var inputPrice = $('#inputprice').val();
                        if (inputPrice == 0.0) {
                            if (ch == 2) {
                                BootstrapDialog.show({
                                    title: 'Price is missing!',
                                    message: 'Enter the price : <input type="text" class="form-control">',
                                    closeByBackdrop: false,
                                    closeByKeyboard: false,
                                    buttons: [{
                                            label: 'Cancel',
                                            action: function (dialogRef) {
                                                //$('#' + cardid).prependTo('#' + getListDIV(list_origin));
                                                dialogRef.close();
                                            }
                                        },
                                        {
                                            label: 'Update Price',
                                            hotkey: 13, // Enter.
                                            cssClass: 'btn-primary',
                                            action: function (dialogRef) {
                                                var newprice = dialogRef.getModalBody().find('input').val();
                                                if (newprice === '') {
                                                    //$('#'+cardid).prependTo('#progressdiv');
                                                    dialogRef.setTitle('Enter the price!');
                                                    //dialogRef.close();
                                                    //return false;
                                                } else {

                                                    console.log('dalam proses update price');
                                                    //$('#comment' + cardid).empty().html(' ' + newcnt).hide().fadeIn(300);
                                                    $.ajax({
                                                        async: true,
                                                        url: "ProcessController?moduleid=020903&process=updateprice&price=" + newprice + "&contractno=" + contractNo + "&sessionid=" + sessionid,
                                                        success: function (result) {
                                                            //$('#maincontainer').empty().html(result).hide().fadeIn(300);
                                                            console.log('dah berjaya update price');
                                                            path = 'ar_inv_check_contract.jsp?';
                                                            next = true;
                                                            if (next == true) {
                                                                console.log('sending---------');
                                                                $.ajax({
                                                                    async: true,
                                                                    url: path + "contract=" + contractNo + "&millcode=" + millCode + "&prodcode=" + procCode + '&sessionid=' + sessionid,
                                                                    success: function (result) {
                                                                        console.log('table-load');
                                                                        $('#table-result-contract').empty().html(result).hide().fadeIn(300);

                                                                    },
                                                                    error: function () {
                                                                        var conts = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Connection failed to establish. Please try again later.';
                                                                        $('#table-result-contract').empty().html(conts).hide().fadeIn(300);
                                                                    }


                                                                });
                                                            }
                                                            dialogRef.close();
                                                        }
                                                    });

                                                }
                                            }
                                        }]
                                });
                            }
                        } else {
                            path = 'ar_inv_check_contract.jsp?';
                            next = true;
                            //alert('xde price');
                        }

                    }
                }

                console.log('*********' + next);
                //var bCode = $('#bcode').val();
                if (next == true) {
                    console.log('sending---------');
                    $.ajax({
                        async: true,
                        url: path + "contract=" + contractNo + "&millcode=" + millCode + "&prodcode=" + procCode + '&sessionid=' + sessionid,
                        success: function (result) {
                            console.log('table-load');
                            $('#table-result-contract').empty().html(result).hide().fadeIn(300);

                        },
                        error: function () {
                            var conts = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Connection failed to establish. Please try again later.';
                            $('#table-result-contract').empty().html(conts).hide().fadeIn(300);
                        }


                    });
                }




            }

            if (curStepBtn == 'step-3') {

                var sessionid = $('#sessionid').val();
                var bCode = $('#bcode').val();
                var prodCode = $('#prodcode').val();
                var contractNo = 'NONE';
                var sum = '';

                if (prodCode == '03') {

                    sum = $('#nettamount').val();

                } else {
                    contractNo = $('#selected-contract').val();
                    sum = $('#sum_mt_refinery').val();

                    var str = 'dispatchno=';
                    $("input[type=checkbox]:checked").each(function () {

                        str = str + $(this).val() + '&dispatchno=';

                    });
                    console.log(str);

                    //var bCode = $('#bcode').val();

                    var a = $("#saverefinery :input").serialize();
                    $.ajax({
                        data: a,
                        type: 'POST',
                        url: "ProcessController?moduleid=020903&process=temprefinery&sessionid=" + sessionid + "&" + str,
                        success: function (result) {
                            //$('#maincontainer').empty().html(result).hide().fadeIn(300);
                        }
                    });
                }




                $.ajax({
                    async: true,
                    url: "ar_inv_add_invoice.jsp?contract=" + contractNo + "&amount=" + sum + "&prodcode=" + prodCode + "&bcode=" + bCode + "&sessionid=" + sessionid,
                    success: function (result) {
                        console.log('table-load');
                        $('#prepare-invoice').empty().html(result).hide().fadeIn(300);

                    }


                });

                $('#total').trigger('change');


            }

            if (curStepBtn == 'step-4') {
                var prodCode = $('#prodcode').val();
                var sessionid = $('#sessionid').val();
                var contractNo = $('#selected-contract').val();
                var prodName = $('#prodname').val();
                var totalItem = $('#totalrow').val();
                var path = '';

                var a = $("#savemaster :input").serialize();
                $.ajax({
                    data: a,
                    async: false,
                    type: 'POST',
                    url: "ProcessController?moduleid=020903&process=tempmaster&sessionid=" + sessionid,
                    success: function (result) {
                        //$('#maincontainer').empty().html(result).hide().fadeIn(300);
                    }
                });


                if (prodCode == '03') {
                    path = "ar_inv_finalize_ffb.jsp?sessionid=" + sessionid + "&prodcode=" + prodCode + "&prodname=" + prodName;
                } else {
                    path = "ar_inv_finalize.jsp?sessionid=" + sessionid + "&prodcode=" + prodCode + "&prodname=" + prodName;
                }

                $.ajax({
                    async: true,
                    url: path,
                    success: function (result) {
                        console.log('table-load');
                        $('#finalize-invoice').empty().html(result).hide().fadeIn(300);

                    }


                });


            }

            if (curStepBtn == 'step-5') {
                alert('Done');
            }


            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.btn-primary').trigger('click');

        $('body').on('click', '.activerowy', function (e) {
            $('.refineryBtn').trigger('click');
            return false;
        });

    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        
                    </td>
                </tr>
            </table>
            <br>

            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                        <p>Product & Buyer</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                        <p id="step-2-title">Choose Contract</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                        <p id="step-3-title">Complete Refinery</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                        <p>Prepare Invoice</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                        <p>Finalizing Items</p>
                    </div>
                </div>
            </div>





            <input type="hidden" id="millcode" value="<%= MillDAO.getDefaultMill(log).getCode() %>">
            <input type="hidden" id="sessionid" value="<%= request.getParameter("sessionid")%>">


            <div class="row setup-content" id="step-1">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Choose Product
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="inputName" class="control-label">Product Code&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span id="res_digit" class="res_style"></span></label>
                                            <div class="form-group-sm input-group">
                                                <input type="text" class="form-control input-sm" id="prodcode" name="prodcode" required>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default btn-sm" type="button" id="getproduct"><i class="fa fa-cog"></i> Product</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="inputName" class="control-label">Product Description&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                            <input type="text" class="form-control input-sm" id="prodname" name="prodname" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="inputName" class="control-label">Buyer Code&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span id="res_digit" class="res_style"></span></label>
                                            <div class="form-group-sm input-group">
                                                <input type="text" class="form-control input-sm" id="bcode" name="bcode" required>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default btn-sm" type="button" id="getbuyer"><i class="fa fa-cog"></i> Buyer</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="inputName" class="control-label">Buyer Description&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                            <input type="text" class="form-control input-sm" id="bname" name="bname" required>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-default nextBtn btn-sm pull-right" type="button" >Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row setup-content" id="step-2">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span id="step-2-string"></span>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div id="table-result"><div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">MPOB Price of Palm Oil</label>
                                                <input type="text" class="form-control input-sm dateformat" id="date" name="date" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">Transport & CESS</label>
                                                <input type="text" class="form-control input-sm dateformat" id="effectivedate" name="effectivedate" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">OER %</label>
                                                <input type="text" class="form-control input-sm dateformat" id="date" name="date" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">KER %</label>
                                                <input type="text" class="form-control input-sm dateformat" id="effectivedate" name="effectivedate" placeholder="">
                                            </div>
                                        </div>
                                    </div></div>
                                <button class="btn btn-default nextBtn refineryBtn btn-sm pull-right" type="button" >Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-3">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span id="step-3-string"></span>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div id="table-result-contract">
                                    <span class="text-center"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i>
                                        Please wait, downloading refinery data from mill...</span>
                                </div>
                                <button class="btn btn-default nextBtn btn-sm pull-right" type="button" >Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-4">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Prepare Invoice
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div id="prepare-invoice">


                                </div>
                                <button class="btn btn-default nextBtn btn-sm pull-right" type="button" >Next</button>
                            </div>
                        </div></div></div>
            </div>
            <div class="row setup-content" id="step-5">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Finalizing
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div id="finalize-invoice">


                                </div>
                                <button class="btn btn-success btn-sm pull-right" id="saveall" type="submit">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
