<%-- 
    Document   : cheque
    Created on : Mar 2, 2017, 4:25:22 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lowagie.text.Phrase"%>
<%@page import="com.lowagie.text.FontFactory"%>
<%@page import="com.lowagie.text.Font"%>
<%@page import="com.lowagie.text.pdf.FontSelector"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lowagie.text.Document"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.DataOutput"%>
<%@page import="com.lowagie.text.Element"%>
<%@page import="com.lowagie.text.Rectangle"%>
<%@page import="com.lowagie.text.pdf.PdfPCell"%>
<%@page import="com.lowagie.text.Chunk"%>
<%@page import="com.lowagie.text.Paragraph"%>
<%@page import="com.lowagie.text.pdf.PdfPTable"%>
<%@page import="com.lowagie.text.pdf.PdfWriter"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%
    response.setContentType("application/pdf");
%><%
    Rectangle pageSize = new Rectangle(505, 255);
//pageSize.setBackgroundColor
//(new java.awt.Color(959595));
// step 1: creation of a document-object
    Document document = new Document(pageSize);

    String bank = request.getParameter("bank");
    String name = request.getParameter("paidname");
    String datecheque = request.getParameter("datecheque");
    String y1 = datecheque.substring(2, 3);
    String y2 = datecheque.substring(3, 4);
    String m1 = datecheque.substring(5, 6);
    String m2 = datecheque.substring(6, 7);
    String d1 = datecheque.substring(8, 9);
    String d2 = datecheque.substring(9, 10);
    String amount = request.getParameter("amountrm");
    String amount_num = GeneralTerm.currencyFormat(Double.parseDouble(request.getParameter("amountcheque")));

    String date = "31/7/2012";
    //ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    //PdfWriter.getInstance(document, buffer);

    PdfWriter.getInstance(document, response.getOutputStream());

    document.open();

    PdfPTable table = new PdfPTable(2); // Code 1
    table.setWidthPercentage(117);
    float[] columnWidths = {2f, 1f};
    //PdfFont font3 = PdfFontFactory.createFont(FONT, PdfEncodings.IDENTITY_H);
    //Font font3 = new Font(Font.ITALIC, 9);
    //Font font4 = new Font(Font.ITALIC, 12);
    table.setWidths(columnWidths);
//table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

//		// Code 2
    Paragraph paragraph = new Paragraph(20);

    Font fontstyle = new Font(Font.HELVETICA, 10f);

    Chunk chunk = new Chunk("                     " + amount, fontstyle);

    //paragraph.setSpacingBefore(1);
    paragraph.add(chunk);
    //paragraph.setSpacingAfter(10);

    PdfPCell cell1 = new PdfPCell(new Paragraph(" "));
    PdfPCell cell2 = new PdfPCell(new Paragraph(""));

    // Code 3
    PdfPCell cell3 = new PdfPCell(new Paragraph(""));
    PdfPCell cell4 = new PdfPCell(new Paragraph(date));
    PdfPCell cell5 = new PdfPCell(new Paragraph(""));
    PdfPCell cell6 = new PdfPCell(new Paragraph(" "));
    PdfPCell cell7 = new PdfPCell(new Paragraph("" + name, fontstyle));
    PdfPCell cell8 = new PdfPCell(new Paragraph(""));

    PdfPCell cell9 = new PdfPCell(new Paragraph(""));
    PdfPCell cell10 = new PdfPCell(new Paragraph(""));

    // Code 3
    PdfPCell cell11 = new PdfPCell(new Paragraph(" "));
    PdfPCell cell12 = new PdfPCell(new Paragraph(" "));
    PdfPCell cell13 = new PdfPCell(new Paragraph(""));
    PdfPCell cell14 = new PdfPCell(new Paragraph("        " + amount_num, fontstyle));
    PdfPCell cell15 = new PdfPCell(new Paragraph(" "));
    PdfPCell cell16 = new PdfPCell(new Paragraph(" "));
    PdfPCell cell17 = new PdfPCell(new Paragraph(""));
    PdfPCell cell18 = new PdfPCell(new Paragraph(""));

    cell1.setBorder(Rectangle.NO_BORDER);
    cell2.setBorder(Rectangle.NO_BORDER);
    cell3.setBorder(Rectangle.NO_BORDER);
    cell4.setBorder(Rectangle.NO_BORDER);
    cell5.setBorder(Rectangle.NO_BORDER);
    cell6.setBorder(Rectangle.NO_BORDER);
    cell7.setBorder(Rectangle.NO_BORDER);
    cell8.setBorder(Rectangle.NO_BORDER);
    cell9.setBorder(Rectangle.NO_BORDER);
    cell10.setBorder(Rectangle.NO_BORDER);
    cell11.setBorder(Rectangle.NO_BORDER);
    cell12.setBorder(Rectangle.NO_BORDER);
    cell13.setBorder(Rectangle.NO_BORDER);
    cell14.setBorder(Rectangle.NO_BORDER);
    cell15.setBorder(Rectangle.NO_BORDER);
    cell16.setBorder(Rectangle.NO_BORDER);
    cell17.setBorder(Rectangle.NO_BORDER);
    cell18.setBorder(Rectangle.NO_BORDER);
    PdfPCell cell19 = new PdfPCell(new Paragraph(" "));
    PdfPCell cell20 = new PdfPCell(new Paragraph(" "));

    // Code 3
    PdfPCell cell31 = new PdfPCell(new Paragraph(" "));
    PdfPCell cell27 = new PdfPCell(new Paragraph(" "));
    PdfPCell cell21 = new PdfPCell(new Paragraph(d1, fontstyle));
    PdfPCell cell22 = new PdfPCell(new Paragraph(d2, fontstyle));
    PdfPCell cell23 = new PdfPCell(new Paragraph(m1, fontstyle));
    PdfPCell cell24 = new PdfPCell(new Paragraph(m2, fontstyle));
    PdfPCell cell25 = new PdfPCell(new Paragraph(y1, fontstyle));
    PdfPCell cell26 = new PdfPCell(new Paragraph(y2, fontstyle));

    cell27.setPaddingRight(18);
    cell21.setPaddingRight(18);
    cell22.setPaddingRight(18);
    cell23.setPaddingRight(18);
    cell24.setPaddingRight(18);
    cell25.setPaddingRight(18);
    cell26.setPaddingRight(18);

    //setting align untuk nama
    if (name.length() < 35) {
        cell7.setPaddingTop(5);
    }else{
        cell7.setPaddingTop(0);
    }
    
    cell7.setPaddingLeft(90);

    cell21.setHorizontalAlignment(Element.ALIGN_CENTER);
    cell22.setHorizontalAlignment(Element.ALIGN_CENTER);
    cell23.setHorizontalAlignment(Element.ALIGN_CENTER);
    cell24.setHorizontalAlignment(Element.ALIGN_CENTER);
    cell25.setHorizontalAlignment(Element.ALIGN_CENTER);
    cell26.setHorizontalAlignment(Element.ALIGN_CENTER);
    cell21.setBorder(Rectangle.NO_BORDER);
    cell22.setBorder(Rectangle.NO_BORDER);
    cell23.setBorder(Rectangle.NO_BORDER);
    cell24.setBorder(Rectangle.NO_BORDER);
    cell25.setBorder(Rectangle.NO_BORDER);
    cell26.setBorder(Rectangle.NO_BORDER);
    cell27.setBorder(Rectangle.NO_BORDER);
    cell31.setBorder(Rectangle.NO_BORDER);

    PdfPTable nestedTable = new PdfPTable(8);
    nestedTable.setWidthPercentage(100);
    nestedTable.addCell(cell31);
    nestedTable.addCell(cell27);
    nestedTable.addCell(cell21);
    nestedTable.addCell(cell22);
    nestedTable.addCell(cell23);
    nestedTable.addCell(cell24);
    nestedTable.addCell(cell25);
    nestedTable.addCell(cell26);

    cell13.setPaddingLeft(22);
    cell14.setPaddingTop(10);
    cell13.setPaddingRight(15);
    cell13.addElement(paragraph);
    //amount setting align to right
    cell13.setPaddingLeft(44);

    cell14.setVerticalAlignment(Element.ALIGN_TOP);
    //cell7.setHorizontalAlignment(Element.ALIGN_CENTER);
    //cell13.setHorizontalAlignment(Element.ALIGN_CENTER);
    cell14.setFixedHeight(43f);
    //cell14.setPaddingTop(1);
    //cell14.setPaddingBottom(12);
    cell14.setPaddingLeft(24);
    cell14.setUseDescender(true);
    //cell14.setHorizontalAlignment(Element.ALIGN_CENTER);
    //cell14.setVerticalAlignment(Element.ALIGN_MIDDLE);

    cell4.setPaddingTop(8);
    //cell4.setRowspan(2);

    cell4.setFixedHeight(32f);
    cell4.addElement(nestedTable);

    table.addCell(cell17);
    table.addCell(cell18);
    table.addCell(cell1);

    //cell4.setFixedHeight(34f);
    table.addCell(cell4);

    // Code 3
    table.addCell(cell3);
    //bawah tarikh
    table.addCell(cell2);
    table.addCell(cell7);
    table.addCell(cell6);
    
    if (name.length() < 35) {
        table.addCell(cell5);
        table.addCell(cell8);
        table.addCell(cell9);
        table.addCell(cell10);
    }

    // Code 3
    table.addCell(cell13);
    table.addCell(cell14);
    table.addCell(cell11);
    table.addCell(cell12);
    table.addCell(cell15);
    table.addCell(cell16);

    // Code 5
    document.add(table);

    document.close();

// step 6: we output the writer as bytes to the response output

%>