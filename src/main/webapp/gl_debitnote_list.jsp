<%-- 
    Document   : gl_debitnote_list
    Created on : Oct 26, 2016, 10:17:43 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.financial.gl.DebitNoteDAO"%>
<%@page import="com.lcsb.fms.util.model.ListTable"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.ModuleItem"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.ConnectionModel"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,java.util.*" errorPage="" isThreadSafe="false" %>
<%@ page import="java.util.Date"%>
<%

    Module mod = (Module) DebitNoteDAO.getModule("HQ");

    response.setHeader("Cache-Control", "no-cache");
%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/tether.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>

<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        var updateTable = '<%= mod.getMainTable()%>';

    <%
    List<ListTable> mlist = (List<ListTable>) DebitNoteDAO.tableList("HQ");
    for (ListTable m : mlist) {
        //if(m.getList_View().equals("1")){
%>
        columntoView.push('<%= m.getColumnName()%>');
        columnName.push('<%= m.getTitleName()%>');
    <%
                //}

            }
    %>

        var colsTosend = 'cols=';
        for (m = 0; m < columntoView.length; ++m) {//put value from database to variable jget
            if (m == columntoView.length - 1) {
                colsTosend += columntoView[m];
            } else {
                colsTosend += columntoView[m] + '&cols=';
            }
        }


        $("#add-new").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addnew",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        $("#check-button").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=check",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });



        var orderby = 'order by voucherno desc';
        var oTable = $('#example').DataTable({
            destroy: true,
            "aaSorting": [[0, "desc"]],
            "responsive": true,
            "bProcessing": true,
            "stateSave": true,
            "bServerSide": true,
            "sAjaxSource": "retrievetable.jsp?table=" + updateTable + "&code=<%= mod.getReferID_Master()%>&" + colsTosend + "&order=" + orderby,
            "aoColumns": [
                {"sTitle": columnName[0], "mData": columntoView[0], "sWidth": "9%", "bSearchable": true},
                {"sTitle": columnName[1], "mData": columntoView[1], "sWidth": "9%"},
                {"sTitle": columnName[2], "mData": columntoView[2], "sWidth": "15%"},
                {"sTitle": columnName[3], "mData": columntoView[3], "sWidth": "25%"},
                {"sTitle": columnName[4], "mData": columntoView[4], "sWidth": "15%"},
                {"sTitle": columnName[5], "mData": columntoView[5], "sWidth": "15%", "bVisible": false},
                {"sTitle": columnName[6], "mData": columntoView[6], "sWidth": "15%", "bVisible": false},
                {"sTitle": "Status", "mData": null, "sWidth": "15%"},
                {"sTitle": "Action", "mData": null, "bSortable": false, "bSearchable": false, "sWidth": "13%"}

            ],
            "aoColumnDefs": [{
                    "aTargets": [8],
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {


                        var c = $(icon('edit', oData.checkid, oData.approveid, oData.postflag));
                        var e = $(icon('delete', oData.checkid, oData.approveid, oData.postflag));
                        var b = $('<button type="button" class="btn btn-default btn-xs"><i class="fa fa-folder-open"></i></button>');


                        b.on('click', function () {
                            //console.log(oData);
                            $.ajax({
                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewpv&refer=" + oData.refer,
                                success: function (result) {
                                    // $("#haha").html(result);
                                    $('#herey').empty().html(result).hide().fadeIn(300);
                                }});
                            return false;
                        });
                        c.on('click', function () {

                            $.ajax({
                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editlist&referno=" + oData.refer,
                                success: function (result) {
                                    // $("#haha").html(result);
                                    $('#herey').empty().html(result).hide().fadeIn(300);
                                }});
                            return false;

                            return false;
                        });

                        e.on('click', function () {
                            //console.log(oData);
                            if (iconClick('delete', '', oData.approveid, oData.postflag) == 0) {

                                BootstrapDialog.confirm({
                                    title: 'Confirmation',
                                    message: 'Are you sure to delete?',
                                    type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                    closable: true, // <-- Default value is false
                                    draggable: true, // <-- Default value is false
                                    btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                    btnOKLabel: 'Delete', // <-- Default value is 'OK',
                                    btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                                    callback: function (result) {
                                        // result will be true if button was click, while it will be false if users close the dialog directly.
                                        if (result) {
                                            $.ajax({
                                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&referno=" + oData.noteno,
                                                success: function (result) {
                                                    // $("#haha").html(result);
                                                    setTimeout(function () {
                                                        oTable.draw();
                                                    }, 500);
                                                }});
                                        }
                                    }
                                });


                                return false;
                            }

                            return false;
                        });

                        $(nTd).empty();
                        $(nTd).attr("class", 'btn-group');
                        $(nTd).attr("align", 'right');
                        $(nTd).prepend(e);


                    }

                },
                {
                    "aTargets": [7], //special rules for determine a status
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        var stat = '<span class="label label-primary">Preparing</span>';


                        if (oData.approveid.length > 0) {
                            stat = '<span class="label label-success">Approved</span>';
                        }
                        if (oData.postflag == 'Cancel') {
                            stat = 'Cancelled';
                        }

                        $(nTd).empty();
                        //$(nTd).attr("id",'btntest');
                        $(nTd).prepend(stat);
                    }

                },
                {
                    "aTargets": [4], //special rules for determine a status
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        var s = parseFloat(oData.total);
                        var amt = formatCurrency(s);

                        $(nTd).empty();
                        $(nTd).attr("class", 'align-right');
                        $(nTd).prepend(amt);
                    }

                }]



        });

        $('#example tbody').on('click', 'tr', function () {
            var data = oTable.row(this).data();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=view&referno=" + data.noteno,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });


        $('body').on('click', '#refresh', function (e) {
            oTable.draw();
            return false;
        });

        $("#approve-all").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=approveall",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });

    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <button id="refresh" class="btn btn-default btn-sm refresh"><i class="fa fa-refresh"></i> Refresh</button>
            <button id="approve-all" class="btn btn-default btn-sm"><i class="fa fa-check" aria-hidden="true"></i> Approve Current Month</button>
            <br> <br>      
            <div class="dataTable_wrapper"> 
                <table class="table table-striped table-bordered table-hover" id="example">
                </table>
            </div>

        </div>
    </div>
</div>

