<%-- 
    Document   : user_dashboard_chart_dnut
    Created on : Aug 29, 2017, 2:51:03 PM
    Author     : fadhilfahmi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link href="css/print_landscape.css?1" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">

    $(document).ready(function () {

        var canvasDoughnut,
                options = {
                    legend: false,
                    responsive: false
                };



        $.ajax({
            async: false,
            url: "ProcessController?moduleid=000000&process=donutchart",
            success: function (result) {
                // $("#haha").html(result);
                // console.log(result);
                var obj = jQuery.parseJSON(result);
                var i = 0;
                $.each(obj, function (key, value) {

                    var modid = value.moduleid;
                    var moddesc = value.moduledesc;
                    i++;
                    console.log('pre : ' + value.counter_pre + ', check : ' + value.counter_che + ', approve : ' + value.counter_app);

                    var render = '<div class="col-md-4">'
                            + '<canvas id="module' + value.moduleid + '" height="110" width="110" style="margin: 5px 10px 10px 0"></canvas>'
                            + '<h2 style="margin:0">' + value.moduledesc + '</h2>'
                            + '</div>';

                    $('#render-chart').append(render);

                    var dochart = new Chart(document.getElementById("module" + value.moduleid), {
                        type: 'doughnut',
                        tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                        data: {
                            labels: [
                                "Checked",
                                "Approved",
                                "Prepared"
                            ],
                            datasets: [{
                                    data: [value.counter_che, value.counter_app, value.counter_pre],
                                    backgroundColor: [
                                        "#eea345", //orange
                                        "#26B99A", //green
                                        "#3498DB"//blue
                                    ],
                                    hoverBackgroundColor: [
                                        "#f9b868",
                                        "#36CAAB",
                                        "#49A9EA"
                                    ]

                                }]
                        },
                        options: {
                            legend: false,
                            responsive: false,
                            // This chart will not respond to mousemove, etc
                            'onClick': function (evt, item) {
                                console.log('legend onClick', evt, item);
                                var activePoints = dochart.getElementsAtEvent(evt);
                                if (activePoints[0]) {
                                    var chartData = activePoints[0]['_chart'].config.data;
                                    var idx = activePoints[0]['_index'];

                                    var label = chartData.labels[idx];
                                    var value = chartData.datasets[0].data[idx];

                                    var stylelabel = '';

                                    if (label == 'Prepared') {
                                        stylelabel = 'primary';
                                    } else if (label == 'Checked') {
                                        stylelabel = 'default';
                                    } else if (label == 'Approved') {
                                        stylelabel = 'success';
                                    }

                                    BootstrapDialog.show({
                                        type: BootstrapDialog.TYPE_DEFAULT,
                                        size: BootstrapDialog.SIZE_WIDE,
                                        closable: false,
                                        cssClass: 'print-dialog-landscape',
                                        //animate: false,
                                        title: 'View Summary Transaction | <strong>' + moddesc + '</strong> <span class="label label-' + stylelabel + '">' + label + '</span>',
                                        message: function (dialog) {
                                            var $content = $('<body></body>').load('user_dashboard_modal.jsp?moduleid=' + modid + '&status=' + label + '&count=' + value);
                                            //$('body').on('click', '.thisresult_nd', function(event){
                                            //    dialog.close();
                                            //    event.preventDefault();
                                            //});
                                            return $content;
                                        },
                                        buttons: [{
                                                label: 'Close',
                                                action: function (dialogRef) {
                                                    dialogRef.close();
                                                    $.ajax({
                                                        url: "user_dashboard_chart_dnut.jsp",
                                                        success: function (result) {
                                                            $('#dashboard-donut').empty().html(result).hide().fadeIn(300);
                                                        }
                                                    });
                                                }
                                            }]
                                    });
                                }
                            },
                            hover: {
                                onHover: function (e) {
                                    $("#module" + value.moduleid).css("cursor", e[0] ? "pointer" : "default");

                                }
                            }
                        }
                    });

                });
            }});

    });
</script>
<div class="col-md-12">
    <div class="row" id="render-chart" style="text-align: center;">


    </div>
</div>