<%-- 
    Document   : list_workattype
    Created on : Apr 11, 2017, 10:36:03 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {

               

                $('#keyword').keyup(function () {
                    //var l = $('input[name=searchby]:checked').val();
                    var keyword = $(this).val();
                    console.log(keyword);

                    var level = $('#searchlevel').val();
                    var url = '';
                    var by = $('#worklocation').val();
                    if (level == 'first') {
                        url = "list_parameter.jsp?type=Paid Type&keyword=" + keyword;
                    } else if (level == 'second') {
                        url = "list_workattype_by.jsp?by=" + by + "&keyword=" + keyword;
                    }
                    $.ajax({
                        url: url,
                        success: function (result) {
                            $('#result').empty().html(result).hide().fadeIn(300);

                        }});
                });



                $('#result').on('click', '.thisresult', function (e) {
                    e.preventDefault();
                    var a = $(this).attr('href');
                    var b = $(this).attr('id');

                    $('#worklocation').val(b);
                    $('#searchlevel').val('second');
                    $.ajax({
                        url: "list_type_by.jsp?by=" + b,
                        success: function (result) {
                            e.preventDefault();
                            $('#result').empty().html(result).hide().fadeIn(300);

                        }});
                    
                    return false;

                });

                $('#result').on('click', '.thisresult_nd', function (e) {
                    e.preventDefault();
                    var a = $(this).attr('href');
                    var b = $(this).attr('id');


                    var a = $(this).attr('href');
                    var b = $(this).attr('id');
                    var c = $(this).attr('id1');
                    var d = $(this).attr('id2');
                    var e = $(this).attr('id3');
                    var f = $(this).attr('id4');
                    var g = $(this).attr('id5');


                    $('#<%= request.getParameter("name")%>').val(a);
                    $('#<%= request.getParameter("code")%>').val(b);
                    $('#<%= request.getParameter("address")%>').val(c);
                    $('#<%= request.getParameter("city")%>').val(d);
                    $('#<%= request.getParameter("postcode")%>').val(e);
                    $('#<%= request.getParameter("state")%>').val(f);
                    $('#<%= request.getParameter("gstid")%>').val(g);

                    //$.ajax({
                    //        url: "list_type_by.jsp?by="+b,
                    //        success: function (result) {
                    //        $('#result').empty().html(result).hide().fadeIn(300);

                    //}});

                });

                $('#result').on('click', '#keyword', function (e) {
                    e.preventDefault();
                    //var l = $('input[name=searchby]:checked').val();
                    var keyword = $(this).val();
                    console.log(keyword);
                    $.ajax({
                        url: "list_type_by.jsp?table=" + table + "&keyword=" + keyword,
                        success: function (result) {
                            $('#result').empty().html(result).hide().fadeIn(300);

                        }});
                });

            });
        </script>
    </head>
    <body>


        <div class="form-group">
            <div class="form-group input-group">
                <input type="text" class="form-control" id="keyword">
                <input type="hidden" class="form-control" id="searchlevel" value="first">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>

        <div id="result">
            <%
                if (request.getParameter("keyword") == null) {

            %>    
            <div class="list-group"><%    List<Parameter> slist = (List<Parameter>) ParameterDAO.getAllParameter(log, "Work Location");
                for (Parameter c : slist) {
                %>

                <a href="<%= c.getParameter()%>" id="<%= c.getValue()%>" class="list-group-item thisresult">
                    <div id="right_div"><%= c.getValue()%></div>
                </a>

                <%
                    }
                %>
            </div>

            <%
            } else {
            %>    
            <div class="list-group"><%
                List<Parameter> alist = (List<Parameter>) ParameterDAO.searchParameter(log, request.getParameter("type"), request.getParameter("keyword"));
                for (Parameter c : alist) {
                %>

                <a href="<%= c.getParameter()%>" id="<%= c.getValue()%>" class="list-group-item thisresult">
                    <div id="right_div"><%= c.getValue()%></div>
                </a>

                <%
                    }
                %>
            </div>

            <%
                }
            %> 

        </div>

        <div id="buttonhere">
            <button class="btn btn-default btn-sm" type="button"><i class="fa fa-cog"></i>
                Type
            </button>
        </div>   
    </body>
</html>