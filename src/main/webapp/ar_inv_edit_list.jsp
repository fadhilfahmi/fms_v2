<%-- 
    Document   : gl_jv_edit_list
    Created on : Mar 3, 2016, 3:42:15 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.model.financial.ar.ArContractAgree"%>
<%@page import="com.lcsb.fms.dao.financial.ar.ArContractDAO"%>
<%@page import="com.lcsb.fms.util.dao.GetStatusDAO"%>
<%@page import="com.lcsb.fms.ui.UIConfig"%>
<%@page import="com.lcsb.fms.dao.financial.gl.PostDAO"%>
<%@page import="com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoice"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoiceItem"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    //SalesInvoiceItem vi = (SalesInvoiceItem) SalesInvoiceDAO.getINVitem(log, request.getParameter("referno").substring(0, 15));
    SalesInvoice v = (SalesInvoice) SalesInvoiceDAO.getINV(log, request.getParameter("referno"));
    ArContractAgree ag = new ArContractAgree();
    Module mod = (Module) SalesInvoiceDAO.getModule();

    double price = 0.0;
    //out.println(vi.getUnitp());

    if (v.getContractno() == null) {
        price = 0.00;
    } else {

        ag = (ArContractAgree) ArContractDAO.getEachAgreeUsingContractNo(log, v.getContractno());
        if (v.getProdcode().equals("03")) {

            //price = Double.parseDouble(vi.getUnitp());
        } else {
            price = ag.getPrice();
        }
    }

    String status = SalesInvoiceDAO.getStatus(log, v.getInvref());

    String topath = request.getParameter("topath");
    String moduleid = mod.getModuleID();
    String token = request.getParameter("status");

    if (topath == null) {
        topath = "viewlist";
    } else {
        //moduleid = request.getParameter("frommodule");
    }
%>

<style>

    a.thelink {
        cursor: pointer;
        color: #ffffff;
        text-decoration: underline;
    }
    a.thelink:link {
        color: #ffffff;
    }

    /* visited link */
    a.thelink:visited {
        color: #ffffff;
    }

    /* mouse over link */
    a.thelink:hover {
        color: #097a62;
    }

    /* selected link */
    a.thelink:active {
        color: #ffffff;
    }
</style>

<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        window.scrollTo(0, 0);

        callPnotify('<%= request.getParameter("status")%>', '<%= request.getParameter("text")%>');

        $('#print').click(function (e) {
            printElement($("#viewjv").html());
        });



        $('.printffb').click(function (e) {
            var a = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=printffb&refer=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $("#addcredit").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addcredit&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $(".addrefer").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addrefer&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".addfromdispatch").click(function (e) {
            e.preventDefault();
            var prodcode = $('#prodcode').val();
            var contractno = $('#contractno').val();
            var invrefno = $('#invrefno').val();
            var price = $('.price').val();


             BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                size: BootstrapDialog.SIZE_WIDE,
                //animate: false,
                title: 'View Contract Detail',
                message: function (dialog) {
                    // request.getParameter("contract"), request.getParameter("millcode"), request.getParameter("prodcode")
                    var $content = $('<body></body>').load('ar_inv_check_contract.jsp?contract=' + encodeURIComponent(contractno) + '&millcode=&prodcode=' + prodcode + '&price=' + price);
                    //$('body').on('click', '.thisresult_nd', function(event){
                    //    dialog.close();
                    //    event.preventDefault();
                    //});
                    return $content;
                },
                buttons: [{
                        //icon: 'fa fa-circle-o-notch',
                        label: 'Save & Close',
                        cssClass: 'btn-primary btn-sm',
                        autospin: true,
                        action: function (dialogRef) {
                            var $button = this;

                            dialogRef.enableButtons(false);
                            dialogRef.setClosable(false);
                            //dialogRef.getModalBody().html('<span style="font-size:14px">Finalizing in progress...</span>');

                            var str = 'dispatchno=';
                            $("input[type=checkbox]:checked").each(function () {

                                str = str + $(this).val() + '&dispatchno=';

                            });
                            console.log(str);

                            //var bCode = $('#bcode').val();

                            var a = $("#saverefinery :input").serialize();

                            console.log(a);
                            $.ajax({
                                data: a,
                                type: 'POST',
                                url: "PathController?moduleid=020903&process=adddispatch&refer=" + invrefno + "&price=" + price + "&" + str,
                                success: function (result) {
                                    setTimeout(function () {
                                        //dialogRef.close();
                                        dialogRef.enableButtons(true);
                                        dialogRef.setClosable(true);
                                        $button.disable();
                                        $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                        dialogRef.close();
                                        $('#herey').empty().html(result).hide().fadeIn(300);
                                    }, 1000);

                                }
                            });

                            /*$.ajax({
                             async: false,
                             url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=setnewperiod&year=" + newy + "&period=" + newp,
                             success: function (result) {
                             // $("#haha").html(result);
                             //$('#herey').empty().html(result).hide().fadeIn(300);
                             if (result == 1) {
                             setTimeout(function () {
                             //dialogRef.close();
                             dialogRef.enableButtons(true);
                             dialogRef.setClosable(true);
                             $button.disable();
                             $button.stopSpin(); // Equals to $button.toggleSpin(false);
                             dialogRef.getModalBody().html('<i class="fa fa-check right-green"></i>&nbsp;&nbsp;<span style="font-size:14px">Success!</span>');
                             }, 3000);
                             } else {
                             setTimeout(function () {
                             //dialogRef.close();
                             dialogRef.enableButtons(true);
                             dialogRef.setClosable(true);
                             //$button.disable();
                             $button.stopSpin(); // Equals to $button.toggleSpin(false);
                             dialogRef.getModalBody().html('<i class="fa fa-times fail-red"></i>&nbsp;&nbsp;<span style="font-size:14px">Failed!</span>');
                             }, 3000);
                             }
                             }
                             });*/

                        }
                    }, {
                        label: 'Close',
                        cssClass: 'btn-default btn-sm',
                        action: function (dialogRef) {
                            dialogRef.close();
                        }
                    }]
            });
            return false;
        });

        $(".adddetail").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=adddetail&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });


        $("#change-unit-price").click(function (e) {
            e.preventDefault();
            var curprice = $('#price').val();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Set Current Unit Price',
                message: function (dialog) {
                    var $content = $('<body></body>').load('ar_inv_edit_unitprice.jsp?curprice=' + curprice);

                    return $content;
                },
                buttons: [{
                        //icon: 'fa fa-circle-o-notch',
                        label: 'Update & Close',
                        cssClass: 'btn-primary',
                        autospin: true,
                        action: function (dialogRef) {
                            var $button = this;
                            var newp = $('#newprice').val();
                            var refer = $('#invrefno').val();
                            dialogRef.enableButtons(false);
                            dialogRef.setClosable(false);
                            //dialogRef.getModalBody().html('<span style="font-size:14px">Finalizing in progress...</span>');

                            $.ajax({
                                async: false,
                                url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=setnewprice&price=" + newp + "&refer=" + refer,
                                success: function (result) {
                                    // $("#haha").html(result);
                                    //$('#herey').empty().html(result).hide().fadeIn(300);
                                    if (result == 1) {
                                        setTimeout(function () {
                                            //dialogRef.close();
                                            dialogRef.enableButtons(true);
                                            dialogRef.setClosable(true);
                                            $button.disable();
                                            $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                            dialogRef.getModalBody().html('<i class="fa fa-check right-green"></i>&nbsp;&nbsp;<span style="font-size:14px">Success!</span>');
                                            dialogRef.close();
                                            $.ajax({
                                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editlist&referno=" + refer,
                                                success: function (result) {
                                                    // $("#haha").html(result);
                                                    $('#herey').empty().html(result).hide().fadeIn(300);
                                                }});
                                        }, 1000);
                                    } else {
                                        setTimeout(function () {
                                            //dialogRef.close();
                                            dialogRef.enableButtons(true);
                                            dialogRef.setClosable(true);
                                            //$button.disable();
                                            $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                            dialogRef.getModalBody().html('<i class="fa fa-times fail-red"></i>&nbsp;&nbsp;<span style="font-size:14px">Failed!</span>');
                                        }, 1000);
                                    }
                                }
                            });

                        }
                    }, {
                        label: 'Close',
                        action: function (dialogRef) {
                            dialogRef.close();
                        }
                    }]
            });
            return false;
        });



    });
</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID()%>"  name="<%= topath%>" type="<%= token%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
                </tr>
            </table>
            <br>
            <%= UIConfig.renderBanner(log, v.getInvref(), mod.getModuleID())%>
            <table class="table table-bordered table-striped  table-hover">
                <thead>
                    <tr>
                        <th colspan="4">Voucher Information
                            <!--<small><span class="label label-primary">Preparing</span></small>-->
                            <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">

                                <%= UIConfig.renderMainButton(log, v.getInvref(), mod.getModuleID())%>
                                <button id="<%= v.getInvref()%>" class="btn btn-default btn-xs printffb" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i> Print FFB Statement</button>

                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody class="activerowm tdrow">
                    <tr>
                        <th width="25%">Status</th>
                        <td width="25%"><%= GeneralTerm.getStatusLabel(GetStatusDAO.getStatus(log, v.getInvref()))%><%= DebitCreditNoteDAO.checkDCNoteExist(log, v.getInvref())%></td>
                        <th width="25%">Buyer Name</th>
                        <td width="25%">(<%= v.getBcode()%>) <%= v.getBname()%> - <%= v.getBtype()%></td>
                    </tr>
                    <tr>
                        <th>Period</th>
                        <td><%= v.getYear()%>, <%= v.getPeriod()%></td>
                        <th>Location</th>
                        <td><%= v.getLoclevel()%> / <%= v.getLoccode()%> / <%= v.getLocdesc()%></td>
                    </tr>
                    <tr>
                        <th>Invoice No</th>
                        <td><%= v.getInvref()%></td>
                        <th>Amount</th>
                        <td>RM<%= GeneralTerm.currencyFormat(v.getAmountno())%></td>
                    </tr>
                    <tr>
                        <th>Invoice Date</th>
                        <td><%= v.getInvdate()%></td>
                        <th>Remark</th>
                        <td><%= v.getRemarks()%></td>
                    </tr>
                    <tr class="togglerow" style="display: none">
                        <th>Buyer Address</th>
                        <td><%= v.getBaddress()%></td>
                        <th>Buyer Postcode</th>
                        <td><%= v.getBpostcode()%></td>
                    </tr>
                    <tr class="togglerow" style="display: none">
                        <th>Buyer City</th>
                        <td></td>
                        <th>Buyer State</th>
                        <td><%= v.getBstate()%></td>
                    </tr>
                    <tr class="togglerow" style="display: none">
                        <th>Rounding</th>
                        <td><%= v.getRacoacode()%> - <%= v.getRacoadesc()%></td>
                        <th>Contract No</th>
                        <td><%= v.getContractno()%>

                        </td>
                    </tr>
                </tbody>
            </table>
            <input type="hidden" class="price" id="price" name="price" value="<%= price%>">
            <input type="hidden" id="contractno" value="<%= v.getContractno()%>">
            <input type="hidden" id="prodcode" value="<%= v.getProdcode()%>">
            <input type="hidden" id="invrefno" value="<%= v.getInvref()%>">
            <div class="invoice-content">
                <div class="table-responsive">
                    <table class="table table-invoice">
                        <thead>
                            <tr>
                                <th colspan="3">

                                    <span class="pull-left">
                                        Unit Price : <%= GeneralTerm.currencyFormat(Double.parseDouble(SalesInvoiceDAO.getINVitem(log, v.getInvref() + "0001").getUnitp()))%> <br>
                                        <a id="change-unit-price" style="cursor: pointer">
                                            <small style="font-weight:normal"><i class="fa fa-cog" aria-hidden="true"></i> Change Unit Price</small></a>
                                    </span>
                                </th>
                                <th colspan="3">
                                    <span class="pull-right">
                                        <%if (!status.equals("Approved")) {%>
                                        <button  class="btn btn-default btn-xs addfromdispatch" title="<%= mod.getModuleID()%>" id="<%= v.getInvref()%>" name="adddetail"><i class="fa fa-plus-circle" aria-hidden="true"></i> From Dispatch No</button>
                                        <button  class="btn btn-default btn-xs adddetail" title="<%= mod.getModuleID()%>" id="<%= v.getInvref()%>" name="adddetail"><i class="fa fa-plus-circle" aria-hidden="true"></i> Detail</button>
                                        <%}%>
                                    </span>
                                </th>
                            </tr>
                            <tr>
                                <th>SALES INVOICE DETAIL</th>
                                <th><span class="pull-left">ACCOUNT CODE</span></th>
                                <th><span class="pull-right">QUANTITY</span></th>
                                <th><span class="pull-right">TAX</span></th>
                                <th><span class="pull-right">AMOUNT</span></th>
                                <th><span class="pull-right">ACTION</span></th>
                            </tr>
                        </thead>
                        <%List<SalesInvoiceItem> listAll = (List<SalesInvoiceItem>) SalesInvoiceDAO.getAllINVItem(log, v.getInvref());
                            double totalTax = 0.0;
                            double totalAmount = 0.0;
                            double totalQty = 0.0;

                            if (listAll.isEmpty()) {%>
                        <tbody>
                            <tr>
                                <td colspan="5">
                                    <span class="font-small-red">No data available.</span>
                                </td>
                            </tr>
                            <%}
                                for (SalesInvoiceItem j : listAll) {
                                    totalTax += j.getTaxamt();
                                    totalAmount += j.getAmount();
                                    totalQty += j.getQty();

                            %>

                            <tr>
                                <td>
                                    <%= j.getRemarks()%><br />
                                    <small><%= j.getRefer()%></small>
                                </td>
                                <td><span class="pull-left"><%= j.getCoacode()%> - <%= j.getCoaname()%></span><br />
                                    <small><%= j.getSatype()%> / <%= j.getSacode()%> / <%= j.getSadesc()%></small></td>
                                <td><span class="pull-right"><%= j.getQty()%></span></td>
                                <td><span class="pull-right"><%= GeneralTerm.currencyFormat(j.getTaxamt())%></span></td>
                                <td><span class="pull-right"><%= GeneralTerm.currencyFormat(j.getAmount())%></span></td>
                                <td>
                                    <%if (!status.equals("Approved")) {%>
                                    <div class="btn-group btn-group-xs" role="group" aria-label="...">
                                        <button id="<%= j.getRefer()%>" class="btn btn-default btn-xs edititem" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-pencil"></i></button>
                                        <button id="<%= j.getRefer()%>" class="btn btn-default btn-xs deleteitem" title="<%= mod.getModuleID()%>"  name="delete_st"><i class="fa fa-trash-o"></i></button>
                                    </div>
                                    <%}%>
                                </td>
                            </tr>
                            <%}%>

                        </tbody>
                        <thead>
                            <tr>
                                <th>TOTAL</th>
                                <th><span class="pull-right"><%//= GeneralTerm.currencyFormat( totQty )%></span></th>
                                <th><span class="pull-right"><%= GeneralTerm.currencyFormat(totalQty)%></span></th>
                                <th><span class="pull-right"><%= GeneralTerm.currencyFormat(totalTax)%></span></th>
                                <th><span class="pull-right"><%= GeneralTerm.currencyFormat(totalAmount)%></span></th>
                                <th><span class="pull-right"><%//= GeneralTerm.currencyFormat( totGrand )%></span></th>
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>



        </div>
    </div>
</div>     