<%-- 
    Document   : gl_debitnote_view
    Created on : Oct 26, 2016, 11:08:32 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.financial.gl.GLAccCredit"%>
<%@page import="com.lcsb.fms.model.financial.gl.GLCreditNote"%>
<%@page import="com.lcsb.fms.dao.financial.gl.CreditNoteDAO"%>
<%@page import="com.lcsb.fms.model.financial.gl.GLItemCreditNote"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
GLItemCreditNote vi = (GLItemCreditNote) CreditNoteDAO.getCNEitem(log,request.getParameter("refer"));
GLCreditNote v = (GLCreditNote) CreditNoteDAO.getCNE(log,request.getParameter("refer"));
GLAccCredit vx = (GLAccCredit) CreditNoteDAO.getAccCredit(log,request.getParameter("refer"));
Module mod = (Module) CreditNoteDAO.getModule("HQ");

String status = CreditNoteDAO.getStatus(log,v.getNoteno());

String topath = request.getParameter("topath");
String moduleid = mod.getModuleID();

if(topath==null){
    topath = "viewlist";
}else{
    moduleid = request.getParameter("frommodule");
}
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        window.scrollTo(0,0);
        
        callPnotify('<%= request.getParameter("status") %>','<%= request.getParameter("text") %>');
        
        
        $('#print').click(function(e){
            printElement($("#viewjv").html());
        });
      
   
    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= moduleid %>" name="<%= topath %>" type="<%= request.getParameter("referother") %>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
		</tr>
            </table>
            <br>
            <table class="table table-bordered table-striped  table-hover">
                <thead>
                    <tr>
                        <th colspan="4"><%= mod.getModuleDesc() %> Information
                        <!--<small><span class="label label-primary">Preparing</span></small>-->
                            <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
                            
                            <%if(!status.equals("Approved")){
                            if ((!CreditNoteDAO.isApprove(log, v.getNoteno()) && log.getListUserAccess().get(2).getAccess())) {%>
                                
                                <button  class="btn btn-default btn-xs approve" title="<%= mod.getModuleID() %>" id="<%= v.getNoteno()%>"><i class="fa fa-check-square-o" aria-hidden="true"></i> Approve</button>
                                
                                <%}%>
                                <button id="<%= v.getNoteno() %>" class="btn btn-default btn-xs viewvoucher" title="<%= mod.getModuleID() %>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i></button>
                                <button id="<%= v.getNoteno() %>"  class="btn btn-danger btn-xs deletemain" title="<%= mod.getModuleID() %>" id="<%//= pvi.getRefer() %>" name="delete_st"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                            <%}else{%>
                                <button id="<%= v.getNoteno() %>" class="btn btn-default btn-xs viewvoucher" title="<%= mod.getModuleID() %>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i></button>
                            <%}%>    
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody class="activerowm tdrow">
                    <tr>
                        <th width="15%">Status</th>
                        <td width="35%"><%= GeneralTerm.getStatusLabel(CreditNoteDAO.getStatus(log,v.getNoteno())) %></td>
                        <th width="15%">Note No</th>
                        <td width="35%"><%= v.getNoteno()%></td>
                    </tr>
                    <tr>
                        <th>Sender</th>
                        <td><%= v.getEstcode()%> - <%= v.getEstname()%></td>
                        <th>Receiver</th>
                        <td><%= v.getReceivercode()%> - <%= v.getReceivername()%></td>
                    </tr>
                    <tr>
                        <th>Sender Account</th>
                        <td><%= v.getAcccode()%> - <%= v.getAccdesc() %></td>
                        <th>Receiver Account</th>
                        <td><%= vx.getAcccode()%> - <%= vx.getAccdesc()%></td>
                    </tr>
                    <tr>
                        <th>Date</th>
                        <td><%= v.getNotedate()%></td>
                        <th>Period</th>
                        <td><%= v.getYear() %>, <%= v.getPeriod() %></td>
                    </tr>
                    <tr>
                        <th>Total Amount</th>
                        <td>RM<%= GeneralTerm.currencyFormat(v.getTotal()) %></td>
                        <th>Referral Code</th>
                        <td><button id="gotoreferral" class="btn btn-default btn-xs" name="<%= v.getRefernoteno() %>" title="<%= ModuleDAO.getModuleFromReferenceNo(log, v.getRefernoteno()).getModuleID() %>" ><%= v.getRefernoteno() %></button></td>
                    </tr>
                    <tr>
                        <th>Remark</th>
                        <td colspan="3"><%= v.getRemark()%></td>
                    </tr>
                    
                </tbody>
            </table>
                    
            
                
            <table class="table table-bordered table-striped table-hover" id="dataTables">
                <thead>
                    <tr>
                        <th colspan="5">Detail<!--<small>asdada</small>-->
                            
                        </th>
                    </tr>
                    <tr>
                        <th>Reference No</th>
                        <th>Description</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <%List<GLItemCreditNote> listAll = (List<GLItemCreditNote>) CreditNoteDAO.getAllCNEItem(log,v.getNoteno());
                    double total = 0.0;
                    if(listAll.isEmpty()){%>
                <tbody>
                    <tr>
                        <td colspan="3">
                            <span class="font-small-red">No data available.</span>
                        </td>
                    </tr>
                    <%}
                    for (GLItemCreditNote j : listAll) {
                        total+=j.getAmount();
                    %>
                    <tr class="activerowy" id="<%= j.getRefer()%>">
                        <td class="tdrow">&nbsp;<%= j.getRefer()%></td>
                        <td class="tdrow">&nbsp;<%= j.getDescp()%></td>
                        <td class="tdrow" align="right">&nbsp;<%= GeneralTerm.currencyFormat(j.getAmount()) %></td>
                        
                    </tr>
                    
                    <%}%>
                    <tr class="activerowy" >
                        <td class="tdrow">&nbsp;</td>
                        <td class="tdrow">&nbsp;<span class="pull-right"><strong>Total</strong></span></td>
                        <td class="tdrow" align="right">&nbsp;<STRONG><%= GeneralTerm.currencyFormat(total) %></strong></td>
                        
                    </tr>
                    </tbody>
            </table>
        </div>
    </div>
</div>





      

      
