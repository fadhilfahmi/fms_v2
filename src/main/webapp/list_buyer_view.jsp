<%-- 
    Document   : list_bank_view
    Created on : Mar 4, 2016, 4:06:59 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.Buyer"%>
<%@page import="com.lcsb.fms.util.dao.BuyerDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<div class="list-group"><%
            
            String keyword = request.getParameter("keyword");
            List<Buyer> slist = (List<Buyer>) BuyerDAO.getBuyer(log,keyword);
            for (Buyer c : slist) { 
               %>
       
            <a href="<%= c.getBuyerName()%>" id="<%= c.getBuyerCode()%>" id1="<%= c.getBuyerAddress() %>" id2="<%= c.getBuyerCity() %>" id3="<%= c.getBuyerPostcode() %>" id4="<%= c.getBuyerState() %>" id5="<%= c.getBuyerType() %>" id6="<%= c.getGstID() %>" id7="<%= c.getCoa()%>" id8="<%= c.getCoadescp()%>"  title="" class="list-group-item thisresult">
                <div id="left_div"><%= c.getBuyerCode()%></div>
                <div id="right_div"><%= c.getBuyerName()%></div>
            </a>

               <%    
            } 
        %>
</div>