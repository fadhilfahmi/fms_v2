<%-- 
    Document   : cb_cv_add
    Created on : Mar 17, 2016, 10:33:19 AM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.CashVoucherDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) SalesInvoiceDAO.getModule();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

        <link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {
                //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });
                //$('#JVdate').datepicker({ dateFormat: 'yyyy-mm-dd' });

               
                
                   $('#invdate').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        }).change(dateChanged)
                .on('changeDate', dateChanged);
        


    var currentDate = $('#invdate').val();

    var y1 = currentDate.substring(0, 4);
    var p1 = currentDate.substring(5, 7);

    var pshort = currentDate.substring(5, 6);
    console.log(pshort);
    if (pshort == 0) {
        p1 = currentDate.substring(6, 7)
    

    $('#year').val(y1);
    $('#period').val(p1);
}


                $('#getaccount').click(function (e) {
                    //e.preventDefault();
                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_DEFAULT,
                        title: 'Get Chart of Account',
                        //message: $('<body></body>').load('list_coa.jsp')
                        message: function (dialog) {
                            var $content = $('<body></body>').load('list_coa.jsp');
                            $('body').on('click', '.thisresult', function (event) {
                                dialog.close();
                            });

                            return $content;
                        }
                    });

                    return false;
                });

                $("#total").keyup(function () {
                    var val = $(this).val();
                    var to = 'amount';
                    //alert(val);
                    //alert(checkRegexp(val,to));
                    if (checkRegexp(val, to)) {
                        $('#res_digit').empty();
                        $('#div_digit').removeClass('has-error');
                        $('#div_digit').addClass('has-success');
                        $('#res_digit').append("<i class=\"fa fa-check-circle right-green\"></i>&nbsp;&nbsp;");
                        $('#savebutton').prop('disabled', false);
                        convertRM(val, to);
                    } else {
                        $('#res_digit').empty();
                        $('#div_digit').removeClass('has-success');
                        $('#div_digit').addClass('has-error');
                        $('#res_digit').append("<i class=\"fa fa-exclamation-circle fail-red\"></i>&nbsp;&nbsp;Numeric Formatted Only");
                        $('#savebutton').prop('disabled', true);
                    }
                    //
                });

                $('#getbuyer').click(function (e) {
                    //e.preventDefault();
                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_DEFAULT,
                        title: 'Get Buyer Info',
                        //message: $('<body></body>').load('list_coa.jsp')
                        message: function (dialog) {
                            var $content = $('<body></body>').load('list_buyer.jsp?name=bname&code=bcode');
                            $('body').on('click', '.thisresult', function (event) {
                                console.log('tutup');
                                dialog.close();
                                event.preventDefault();
                            });

                            return $content;
                        }
                    });

                    return false;
                });

                $('#getlocation').click(function (e) {
                    //e.preventDefault();
                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_DEFAULT,
                        title: 'Get Location',
                        //message: $('<body></body>').load('list_coa.jsp')
                        message: function (dialog) {
                            var $content = $('<body></body>').load('list_location.jsp');
                            $('body').on('click', '.thisresult_nd', function (event) {
                                dialog.close();
                                event.preventDefault();
                            });

                            return $content;
                        }
                    });

                    return false;
                });
                
                $('#getproduct').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Product Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_product.jsp?name=prodname&code=prodcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });
            });

        </script>
    </head>
    <body>
        <div id ="maincontainer">
            
            <div class="partition"> 
                <div class="bodyofpartition">

                    <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                        <tr>
                            <td valign="middle" align="left">
                                <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                                <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addinv" ><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                            </td>
                        </tr>
                    </table>
                    <br>



                    <form data-toggle="validator" role="form" id="saveform">
                        <input type="hidden" name="prepareid" id="pid" value="<%= log.getUserID()%>">
                        <input type="hidden" name="preparename" id="pname" value="<%= log.getFullname()%>">
                        <input type="hidden" name="preparedate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
                        <input type="hidden" name="coacode" id="coacode" value="">
                        <input type="hidden" name="coadesc" id="coadesc" value="">
                        <div class="well">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Invoice No &nbsp&nbsp<span class="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="invno" name="invno" placeholder="Auto Generated" autocomplete="off" required readonly>   
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Reference No &nbsp&nbsp<span class="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="invref" name="invref" placeholder="Auto Generated" autocomplete="off" readonly >   
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Date &nbsp&nbsp<span class="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="invdate" name="invdate" placeholder="Date of Journal Voucher" autocomplete="off" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">   
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Year &nbsp&nbsp<span class="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="year" name="year" placeholder="Code" autocomplete="off" value="<%= AccountingPeriod.getCurYear(log) %>">   
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Period &nbsp&nbsp<span class="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="period" name="period"  autocomplete="off" value="<%= AccountingPeriod.getCurPeriodByCurrentDate() %>">   
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">DO No &nbsp&nbsp<span class="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="dono" name="dono" autocomplete="off" >   
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="well">
                            <div class="row">
                                <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="inputName" class="control-label">Product Code&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span id="res_digit" class="res_style"></span></label>
                                            <div class="form-group-sm input-group">
                                                <input type="text" class="form-control input-sm" id="prodcode" name="prodcode" required>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default btn-sm" type="button" id="getproduct"><i class="fa fa-cog"></i> Product</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="inputName" class="control-label">Product Description&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                            <input type="text" class="form-control input-sm" id="prodname" name="prodname" required>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Buyer Code&nbsp&nbsp<span class="res_code"></span></label>
                                        <div class="form-group-sm input-group">
                                            <input type="text" class="form-control input-sm" id="bcode" name="bcode">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-sm" type="button" id="getbuyer"><i class="fa fa-cog"></i> Get Buyer</button>
                                            </span>
                                        </div>  
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Buyer Name&nbsp&nbsp<span class="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="bname" name="bname" placeholder="" autocomplete="off" > 
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">GST ID&nbsp&nbsp<span class="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="gstid" name="gstid" placeholder="" autocomplete="off" > 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Address &nbsp&nbsp<span class="res_code"></span></label>
                                        <textarea class="form-control" rows="3"  id="baddress" name="baddress"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Postcode &nbsp&nbsp</label>
                                        <input type="text" class="form-control input-sm" id="bpostcode" name="bpostcode" placeholder="" autocomplete="off" > 
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">City &nbsp&nbsp</label>
                                        <input type="text" class="form-control input-sm" id="bcity" name="bcity" placeholder="" autocomplete="off" > 
                                    </div>
                                </div><div class="col-sm-3">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">State &nbsp&nbsp</label>
                                        <input type="text" class="form-control input-sm" id="bstate" name="bstate" placeholder="" autocomplete="off" > 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="well">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Location Level&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                        <div class="form-group-sm input-group">
                                            <input type="text" class="form-control input-sm" id="loclevel" name="loclevel" value="Company" readonly>
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-sm" type="button" id="getlocation"><i class="fa fa-cog"></i> Location</button>
                                            </span>
                                        </div>    
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Location Code&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="loccode" name="loccode"  autocomplete="off" value="<%= log.getEstateCode() %>" readonly>   
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Location Name&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="locdesc" name="locdesc" placeholder="" autocomplete="off" value="<%= log.getEstateDescp()%>" readonly >   
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="well">
                            <div class="row">
                                <!--<div class="col-sm-4">
                                    <div class="form-group" id="div_digit">
                                        <label for="inputName" class="control-label">RM (Digit) &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                        <input type="text" class="form-control input-sm" id="total" name="amountno" placeholder="0.00" autocomplete="off" > 
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group" id="div_digit">
                                        <label for="inputName" class="control-label">RM (In Words) &nbsp&nbsp<span class="res_code"></span></label>
                                        <textarea class="form-control" rows="3"  id="amount" name="amountstr"></textarea>
                                    </div>
                                </div>-->
                                <div class="col-sm-12">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Remarks &nbsp&nbsp<span class="res_code"></span></label>
                                        <textarea class="form-control" rows="3"  id="remarks" name="remarks"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="well">         
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Rounding Adjustment Code&nbsp&nbsp<span class="res_code"></span></label>
                                        <div class="form-group-sm input-group">
                                            <input type="text" class="form-control input-sm" id="actcode" name="racoacode">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-sm" type="button" id="getaccount"><i class="fa fa-cog"></i> Account</button>
                                            </span>
                                        </div>  
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Rounding Adjustment Description&nbsp&nbsp<span class="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="actdesc" name="racoadesc" placeholder="0.00" autocomplete="off" > 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Company Code&nbsp&nbsp<span class="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="estcode" name="estcode" placeholder="0.00" autocomplete="off" value="<%= log.getEstateCode()%>" readonly > 
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Company Name&nbsp&nbsp<span class="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="estname" name="estname" placeholder="0.00" autocomplete="off" value="<%= log.getEstateDescp()%>" readonly> 
                                    </div>
                                </div>
                            </div>
                        </div>         




                        <!--<div class="form-group-sm input-group">
                        <input type="text" class="form-control input-sm">
                        <span class="input-group-btn">
                        <button class="btn btn-default btn-sm" type="button"><i class="fa fa-cog"></i> Get Account Code</button>
                        </span>
                        </div>-->

                    </form>

                </div>
            </div>
        </div>


    </body>
</html>
