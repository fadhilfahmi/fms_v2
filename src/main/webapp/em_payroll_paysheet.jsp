<%-- 
    Document   : em_payroll_paysheet
    Created on : Jun 13, 2017, 3:19:56 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.template.voucher.Paysheet_Template"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.employee.EmPayrollType"%>
<%@page import="com.lcsb.fms.model.setup.company.staff.Staff"%>
<%@page import="com.lcsb.fms.model.financial.employee.EmPaySheetList"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.financial.employee.EmPayrollDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    
    String refer = request.getParameter("refer");
    EmPaySheetList x = EmPayrollDAO.getPaysheet(log, refer, (Integer.parseInt(request.getParameter("start"))-1) * 4, 4);

%>
<ul class="pagination pagination-sm">
    <% for (int i = 1; i < x.getPageCount()+1; i++) {
        String classActive = "";
        if(i == Integer.parseInt(request.getParameter("start"))){
            classActive = "active";
        }
    %>
    <li class="<%= classActive %>"><a class="pageno" href="<%=i%>"><%=i%></a></li>
    <% }%>
</ul>
<%= Paysheet_Template.getTemplate(refer, log, 0, (Integer.parseInt(request.getParameter("start"))-1) * 4, 4, 0, 0.0, 0.0).getOutput() %>
<br>
    