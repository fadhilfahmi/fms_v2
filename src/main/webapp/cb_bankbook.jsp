<%-- 
    Document   : cb_bankbook
    Created on : May 25, 2017, 12:14:22 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.financial.cashbook.BankBookParam"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.BankBookDAO"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.BankReportDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.dao.financial.gl.FinancialReportDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) BankBookDAO.getModule();
    BankBookParam prm = (BankBookParam) session.getAttribute("bb_param");
    
    String bankcode = "";
    String bankname = "";
    String period = "";
    String year = "";
    
    if(prm!=null){
        bankcode = prm.getBankcode();
        bankname = prm.getBankname();
        period = prm.getPeriod();
        year = prm.getYear();
    }else{
        period = AccountingPeriod.getCurPeriod(log);
        year = AccountingPeriod.getCurYear(log);
    }
%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        
        $("#viewreport").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewreport&vby=" + $('#vby').val() + "&year=" + $('#year').val() + "&period=" + $('#period').val() + "&otype=" + $('#otype').val(),
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            
            <form data-toggle="validator" role="form" id="saveform">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="well">

                            <div class="row self-row">
                                <label for="inputName" class="control-label">Bank Book Parameter</label>
                            </div>


                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group" id="div_digit">
                                        <label for="inputName" class="control-label">Bank Code &nbsp&nbsp<span class="res_code"></span></label>
                                        <div class="form-group-sm input-group">
                                            <input type="text" class="form-control input-sm" id="bankcode" name="bankcode" value="<%= bankcode %>">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-sm" type="button" id="getbank"><i class="fa fa-cog"></i> Bank</button>
                                            </span>
                                        </div>  
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group" id="div_digit">
                                        <label for="inputName" class="control-label">Bank Name &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                        <input class="form-control input-sm" style="font-size:12px" name="bankname" id="bankname" type="text" size="50"  value="<%= bankname %>" >
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Year</label>
                                        <select class="form-control input-sm" id="year" name="year">
                                            <%= ParameterDAO.parameterList(log,"Year", year) %>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Period</label>
                                        <select class="form-control input-sm" id="period" name="period">
                                            <%= ParameterDAO.parameterList(log,"Period", period) %>
                                        </select> 
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <button id="savebutton"  class="btn btn-default btn-sm pull-right" title="<%= mod.getModuleID()%>" name="generateBankBook">Generate Report</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                </div>
            </form>                               

        </div>
    </div>
</div>

