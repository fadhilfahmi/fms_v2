<%-- 
    Document   : cf_vendor_gethq_add
    Created on : Sep 27, 2016, 12:11:26 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Vendor"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.SupplierDAO"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Supplier"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
Vendor vd = new Vendor();

if(request.getParameter("type").equals("supplier")){
    
    Supplier gt = (Supplier) SupplierDAO.getInfo(log,request.getParameter("code"));
    vd.setVendorName(gt.getCompanyname());
    vd.setRegisterNo(gt.getCoregister());
    vd.setVendorAddress(gt.getAddress());
    vd.setPostcode(gt.getPostcode());
    vd.setCity(gt.getCity());
    vd.setState(gt.getState());
    vd.setCountry(gt.getCountry());
    vd.setBumiputra(gt.getCountry());
    vd.setPerson(gt.getPerson());
    vd.setTitle(gt.getTitle());
    vd.setHp(gt.getHp());
    vd.setPhone(gt.getPhone());
    vd.setFax(gt.getFax());
    vd.setEmail(gt.getEmail());
    vd.setUrl(gt.getUrl());
    vd.setBank(gt.getBank());
    vd.setBankaccount(gt.getBankaccount());
    vd.setPayment(gt.getPayment());
    vd.setPosition(gt.getPosition());
    vd.setGstid(gt.getGstid());
    
}

%>    
<!DOCTYPE html>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
            $('#myStateButton').on('click', function () {
                $(this).button('toggle'); // button text will be "finished!"
              })
        });
</script>
<div class="row">
    
    
    <!--<div class="col-sm-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>Save Option</strong>
            </div>
            <div class="panel-body">
                <p>Type = Supplier</p>
                <p>Code = 0001</p>
                
                 <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-default btn-xs">
                  <input type="checkbox" autocomplete="off" id="myStateButton" > Supplier
                </label>
                <label class="btn btn-default btn-xs">
                  <input type="checkbox" autocomplete="off"> Buyer
                </label>
                <label class="btn btn-default btn-xs">
                  <input type="checkbox" autocomplete="off"> Contractor
                </label>
              </div>
            </div>
           
            <div class="panel-footer">
                Panel Footer
            </div>
        </div>
    </div>-->
    
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>Add to Vendor</strong>
            </div>
            <div class="panel-body">
                <form data-toggle="validator" role="form" id="saveform">
    
    <div class="row">
        <div class="col-sm-2">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Code&nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="code" name="code" value="<%= AutoGenerate.get4digitNo(log,"vendor_info", "code") %>" autocomplete="off" required readonly >  
             </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Company Name &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="vendor_name" name="vendor_name" autocomplete="off" >   
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Registration No &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="register_no" name="register_no" autocomplete="off" value="">   
             </div>
        </div>
    </div>
   
    
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Address &nbsp&nbsp<span class="res_code"></span></label>
                <textarea class="form-control" rows="3"  id="vendor_address" name="baddress"></textarea>
             </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">Postcode &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <input type="text" class="form-control input-sm" id="postcode" name="postcode" placeholder="" autocomplete="off" > 
             </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">City &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <input type="text" class="form-control input-sm" id="city" name="city" placeholder="" autocomplete="off" > 
             </div>
        </div><div class="col-sm-3">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">State &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <input type="text" class="form-control input-sm" id="state" name="state" placeholder="" autocomplete="off" > 
             </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Active &nbsp&nbsp<span class="res_code"></span></label>
                <select class="form-control input-sm" id="active" name="active">
                    <%= ParameterDAO.parameterList(log,"YesNo Type","") %>
                </select>
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">Bumiputra &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <select class="form-control input-sm" id="bumiputra" name="bumiputra">
                    <%= ParameterDAO.parameterList(log,"YesNo Type","") %>
                </select> 
             </div>
        </div>
         <div class="col-sm-4">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">Position &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <select class="form-control input-sm" id="position" name="position">
                    <%= ParameterDAO.parameterList(log,"Position","") %>
                </select> 
             </div>
        </div>
        
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">Title &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <select class="form-control input-sm" id="title" name="title">
                    <%= ParameterDAO.parameterList(log,"Title","") %>
                </select> 
             </div>
        </div>
        <div class="col-sm-9">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">Contact Person &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <input type="text" class="form-control input-sm" id="person" name="person" placeholder="" autocomplete="off" > 
             </div>
        </div>
        
    </div>
        <div class="row">
        <div class="col-sm-3">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">HP No &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <select class="form-control input-sm" id="hp" name="hp">
                    <%= ParameterDAO.parameterList(log,"Title","") %>
                </select> 
             </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">Phone No &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <input type="text" class="form-control input-sm" id="phone" name="phone" placeholder="" autocomplete="off" > 
             </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">Fax No &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <input type="text" class="form-control input-sm" id="fax" name="fax" placeholder="" autocomplete="off" > 
             </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">Email &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <input type="text" class="form-control input-sm" id="email" name="email" placeholder="" autocomplete="off" > 
             </div>
        </div>
        
    </div>
         <div class="row">
        <div class="col-sm-3">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">Payment Method &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <select class="form-control input-sm" id="payment" name="payment">
                    <%= ParameterDAO.parameterList(log,"Payment Method","") %>
                </select> 
             </div>
        </div>
        <div class="col-sm-5">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">Bank &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <input type="text" class="form-control input-sm" id="bank" name="bank" placeholder="" autocomplete="off" > 
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">Account No &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <input type="text" class="form-control input-sm" id="bankaccount" name="bankaccount" placeholder="" autocomplete="off" > 
             </div>
        </div>
        
    </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group" id="div_code">
                            <label for="inputName" class="control-label">Remarks &nbsp&nbsp<span class="res_code"></span></label>
                            <textarea class="form-control" rows="3"  id="remarks" name="remarks"></textarea>
                         </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group" id="div_code">
                            <label for="inputName" class="control-label">GST ID &nbsp&nbsp<span class="res_code"></span></label>
                            <input type="text" class="form-control input-sm" id="gstid" name="gstid" placeholder="" autocomplete="off" > 
                         </div>
                    </div>
                </div>
   
    
    
    
</form>
            </div>
            <div class="panel-footer">
                Panel Footer
            </div>
        </div>
    </div>
    
    
</div>
     
