<%-- 
    Document   : list_coa
    Created on : Mar 1, 2016, 5:12:30 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
            
            $('#keyword').keyup(function(){
                var l = $('input[name=searchby]:checked').val();
                var m = $('input[name=method]:checked').val();
                var keyword = $(this).val();
                console.log(keyword);
                 $.ajax({
                        url: "list_search.jsp?table=chartofmaterial&column="+l+"&keyword="+keyword+"&method="+m+"&code=code&descp=descp",
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
                }});
            });
            
            $('#result').on('click', '.thisresult', function(e) { 
                var a = $(this).attr('href');
                var b = $(this).attr('id');
                
                $('#<%= request.getParameter("code") %>').val(a);
                $('#<%= request.getParameter("descp") %>').val(b);
                e.preventDefault();
            });
            
        });
        </script>
    </head>
    <body>
        <div class="well">
            <form>
                <div class="row">
                    <div class="col-sm-12">
                        <label for="year" class="control-label input-group">Refine Your Search</label>
                        <div class = "btn-toolbar" role = "toolbar">
                            
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default btn-sm active">
                                    <input name="searchby" id="searchby" value="code" type="radio" checked>Code
                                </label>
                                <label class="btn btn-default btn-sm">
                                    <input name="searchby" id="searchby" value="descp" type="radio">Description
                                </label>
                            </div>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default btn-sm active">
                                    <input name="method" id="method" value="start" type="radio" checked>Start
                                </label>
                                <label class="btn btn-default btn-sm">
                                    <input name="method" id="method" value="contain" type="radio">Containing
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row">&nbsp;</div>
            <div class="form-group">
                <div class="form-group input-group">
                    <input type="text" class="form-control" id="keyword">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </div>
        
            <div id="result">

            </div>
            <div id="buttonhere">
                <button class="btn btn-default btn-sm" type="button"><i class="fa fa-cog"></i>
                    Chart of Material
                </button>
            </div>
        
         </div>   
    </body>
</html>
