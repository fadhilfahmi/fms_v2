<%-- 
    Document   : list_bank_view
    Created on : Mar 4, 2016, 4:06:59 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ChequeDAO"%>
<%@page import="com.lcsb.fms.util.model.Cheque"%>
<%@page import="com.lcsb.fms.util.model.Bank"%>
<%@page import="com.lcsb.fms.util.dao.BankDAO"%>
<%@page import="com.lcsb.fms.util.dao.TaxDAO"%>
<%@page import="com.lcsb.fms.util.model.Tax"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<div class="list-group"><%
            
            String keyword = request.getParameter("keyword");
            String bankcode = request.getParameter("bankcode");
            List<Cheque> slist = (List<Cheque>) ChequeDAO.getAllCheque(log,keyword,bankcode);
            for (Cheque c : slist) { 
               %>
       
            <a href="#" id="<%= c.getChequeNo()%>" title="" class="list-group-item thisresult">
                <div id="left_div"><%//= c.getChequeNo()%></div>
                <div id="right_div"><%= c.getChequeNo()%></div>
            </a>

               <%    
            } 
        %>
</div>