<%-- 
    Document   : ac_lot_activity_add
    Created on : Dec 6, 2016, 4:41:29 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.management.activity.LotActivityDAO"%>
<%@page import="com.lcsb.fms.model.setup.configuration.LotMember"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.LotMemberDAO"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    //LotMember v = (LotMember) LotMemberDAO.getInfo(request.getParameter("referno"));
//JournalVoucherItem bal = (JournalVoucherItem) JournalVoucherDAO.getJVBal(request.getParameter("referno").substring(0,15));

    Module mod = (Module) LotActivityDAO.getModule();

%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/formValidation.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.js"></script>

<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $('.dateformat').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        });


        $('.getaccount').click(function (e) {
            var a = $(this).attr('id');
            var b = $(this).attr('id1');
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Chart of Account',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_coa.jsp?code=' + a + '&descp=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('.getbank').click(function (e) {

            var a = $(this).attr('id');
            var b = $(this).attr('id1');
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Taxation',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_branch.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });



        $('.getlot').click(function (e) {

            var a = 'lotcode';
            var b = 'lotdesc';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Estate',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_lot.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getbroker').click(function (e) {

            var a = 'brokercd';
            var b = 'brokerde';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Broker',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_broker.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getproduct').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Product Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_product.jsp?name=prodname&code=prodcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getsub').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Sub Account Detail',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_sub.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('.calculate').keyup(function (e) {
            var todeduct = $(this).val();
            var theval = $(this).attr('id');


            if (todeduct != '') {
                var tot = 0;
                if (theval == 'debit') {
                    $('#credit').val(0.0);
                }
                if (theval == 'credit') {
                    $('#debit').val(0.0);
                }

            }
            return false;
        });


    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist" type="<%//= v.getMemCode()%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>

            <div class="well">

                <form data-toggle="validator" role="form" id="saveform">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Reference No&nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="refer" name="refer" placeholder="Auto Generate" readonly>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Lot Code & Name&nbsp&nbsp<span class="res_code"></span></label>
                                    <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="lotcode" name="lotcode">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-sm getlot" type="button" id="lotcode" id1="lotdesc"><i class="fa fa-cog"></i> Lot</button>
                                        </span>
                                    </div>  
                                </div>
                                <div class="form-group" id="div_code">
                                    <input type="text" class="form-control input-sm" id="lotdesc" name="lotdesc" placeholder="" autocomplete="off" > 
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">            
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Year</label>
                                <input type="text" class="form-control input-sm calculate" id="year" name="year" value="<%= AccountingPeriod.getCurYear(log)%>" required>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Period</label>
                                <input type="text" class="form-control input-sm calculate" id="period" name="period" value="<%= AccountingPeriod.getCurPeriod(log)%>" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">            
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Quarter</label>
                                <input type="text" class="form-control input-sm calculate" id="quarter" name="quarter" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Remark &nbsp&nbsp<span id="res_code"></span></label>
                                <textarea class="form-control" rows="3"  id="remark" name="remark"></textarea>
                            </div>
                        </div>
                    </div>


                </form>
            </div>

        </div>
    </div>
</div>

