<%-- 
    Document   : cf_coa_edit
    Created on : Mar 11, 2016, 8:58:23 AM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.model.setup.configuration.ChartofAccount"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ChartofAccountDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) ChartofAccountDAO.getModule();
    ChartofAccount edit = (ChartofAccount) ChartofAccountDAO.getInfo(log, request.getParameter("referenceno"));

%>

<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        var tab = sessionStorage.getItem(1);
        $('#backto').attr('id', 'backto' + tab);
        $('#refresh').attr('id', 'refresh' + tab);

        $('#getbank').click(function (e) {

            var a = 'bankcode';
            var b = 'bankname';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Taxation',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_bank.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });
    });

</script>
<div class="row">
    <div class="form-group pull-left">
        <div class="col-md-12">                                                                                                                        
            <div class="btn-group">
            <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="<%= request.getParameter("from")%>" type="<%= request.getParameter("parent")%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
            <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="editprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                </div>      
        </div>
    </div> 
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Edit <%= mod.getModuleDesc()%></h3>
            </div>
            <div class="panel-body">
                <form data-toggle="validator" role="form" id="saveform">
                    <div class="form-group" id="div_code">
                        <label for="inputName" class="control-label">Account &nbsp&nbsp<span id="res_code"></span></label>
                        <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= edit.getCode()%>" required readonly>
                        <p></p>
                        <input type="text" class="form-control input-sm" id="descp" name="descp" placeholder="Description" value="<%= edit.getDescp()%>" required>
                    </div>

                    <div class="form-group form-group-sm">
                        <label for="inputName" class="control-label">Account Type</label>
                        <select class="form-control input-sm" id="type" name="type">
                            <%= ParameterDAO.parameterList(log, "Account Type", edit.getType())%>
                        </select>
                    </div>

                    <div class="form-group form-group-sm">
                        <label for="inputName" class="control-label">Apportion to Type</label>
                        <select class="form-control input-sm" id="apptype" name="apptype">
                            <%= ParameterDAO.parameterList(log, "Apportion Type", edit.getApptype())%>
                        </select>
                    </div>

                    <div class="form-group form-group-sm">
                        <label for="inputName" class="control-label">Apportion to Level</label>
                        <select class="form-control input-sm" id="applevel" name="applevel">
                            <%= ParameterDAO.parameterList(log, "Apportion to Level", edit.getApplevel())%>
                        </select>
                    </div>

                    <div class="form-group form-group-sm">
                        <label for="inputName" class="control-label">Account to Type</label>
                        <select class="form-control input-sm" id="attype" name="attype">
                            <%= ParameterDAO.parameterList(log, "Account To Type", edit.getAttype())%>
                        </select>
                    </div>

                    <div class="form-group form-group-sm">
                        <label for="inputName" class="control-label">Final Level</label>
                        <select class="form-control input-sm" id="finallvl" name="finallvl">
                            <%= ParameterDAO.parameterList(log, "YesNo Type", edit.getFinallvl())%>
                        </select>
                    </div>

                    <div class="form-group form-group-sm">
                        <label for="inputName" class="control-label">Active</label>
                        <select class="form-control input-sm" id="active" name="active">
                            <%= ParameterDAO.parameterList(log, "YesNo Type", edit.getActive())%>
                        </select>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


