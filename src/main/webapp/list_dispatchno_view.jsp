<%-- 
    Document   : list_dispatchno_view
    Created on : Nov 10, 2016, 2:32:47 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.DispatchDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArKernelRefinery"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<div class="list-group"><%
            
            String keyword = request.getParameter("keyword");
            List<ArKernelRefinery> slist = (List<ArKernelRefinery>) DispatchDAO.getDispatchNo(log,request.getParameter("table"),keyword);
            if (slist.isEmpty()){%>
                                            <a class="list-group-item thisresult-payee">
                                                <div id="leftdiv>"<span class="font-small-red"><strong>No result found.</strong></span></div>
                                            </a>
                                            <%
                
            }
            for (ArKernelRefinery c : slist) { 
               %>
       
            <a href="" id="<%= c.getDispatchNo()%>" id1="<%= c.getArKernelRefineryPK().getDate() %>" id2="<%= c.getArKernelRefineryPK().getTrailer()%>" id3="<%= c.getMt()%>" id4="<%= c.getMtRefinery()%>" title="" class="list-group-item thisresult">
                <div id="left_div"><%= c.getDispatchNo()%></div>
                <div id="right_div"><%= c.getArKernelRefineryPK().getTrailer()%></div>
            </a>

               <%    
            } 
        %>
</div>
