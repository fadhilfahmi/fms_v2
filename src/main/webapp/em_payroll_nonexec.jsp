<%-- 
    Document   : em_payroll
    Created on : May 30, 2017, 9:19:41 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.financial.employee.EmPayrollDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) EmPayrollDAO.getModule();
    String refer = request.getParameter("refer");

%>

<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>

<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />

<link href="bootstrap-form-wizard/form-wizard.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
        var navListItems = $('ul.setup-panel li a'),
                allWells = $('.setup-content');

        allWells.hide();

        navListItems.click(function (e)
        {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                    $item = $(this).closest('li');

            if (!$item.hasClass('disabled')) {
                navListItems.closest('li').removeClass('active');
                $item.addClass('active');
                allWells.hide();
                $target.show();
            }

            //var curStep = $(this).closest(".setup-content"),
            //var curStepBtn = $(this).attr("class");

            //console.log(curStepBtn);

        });

        $('ul.setup-panel li.active a').trigger('click');

        // DEMO ONLY //
        $('#activate-step-2').on('click', function (e) {
            $('ul.setup-panel li:eq(1)').removeClass('disabled');
            $('ul.setup-panel li a[href="#step-2"]').trigger('click');
            $('.save-earning').trigger('click');
            //$(this).remove();
        })
        $('#activate-step-3').on('click', function (e) {
            $('ul.setup-panel li:eq(2)').removeClass('disabled');
            $('ul.setup-panel li a[href="#step-3"]').trigger('click');
            //$(this).remove();
        })
        $('#activate-step-4').on('click', function (e) {
            $('ul.setup-panel li:eq(3)').removeClass('disabled');
            $('ul.setup-panel li a[href="#step-4"]').trigger('click');
            //$(this).remove();
        })

        $('#activate-step-5').on('click', function (e) {
            $('ul.setup-panel li:eq(4)').removeClass('disabled');
            $('ul.setup-panel li a[href="#step-5"]').trigger('click');
            //$(this).remove();
        })

        $('.add').click(function (e) {

            var a = 'code';
            var b = 'descp';
            var ty = $(this).attr('id');

            var em = ty.toLowerCase();
            console.log('#accordion-' + ty);

            e.preventDefault();
            if (e.handled !== true) { //Checking for the event whether it has occurred or not.
                e.handled = true; // Basically setting value that the current event has occurred.
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get ' + ty,
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function (dialog) {

                        var $content = $('<body></body>').load('em_payroll_getcode.jsp?type=' + ty + '&code=' + a + '&descp=' + b + '&special=<%=refer%>');
                        $('body').one('click', '.thisresult', function (event) {

                            event.preventDefault();
                            if (event.handled !== true) { //Checking for the event whether it has occurred or not.
                                event.handled = true; // Basically setting value that the current event has occurred.
                                $('#accordion-' + ty).append('<div id="spinloading" style="width:100px; margin-top:5px; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-2x fa-fw"></i></span><span class="sr-only">Loading...</span></div>').hide().fadeIn(100);


                                var thecode = $('#code').val();
                                var thedescp = $('#descp').val();
                                dialog.close();
                                var $contentx = $('<div class="panel panel-default" id="panel-' + em + '-' + thecode + '"></div>').load('em_payroll_list_' + em + '.jsp?code=' + thecode + "&refer=<%=refer%>&flag=add");
                                $('#spinloading').remove();
                                $('#earn-not').remove();
                                $('#accordion-' + ty).append($contentx);
                            }
                        });

                        return $content;
                    }
                });

            }
        });

        $('#accordion').append('<div id="spinloadingmain" style="width:100px; margin:auto;; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>').hide().fadeIn(100);

        $.ajax({
            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewearning&refer=<%= refer%>",
            success: function (result) {
                $('.accordion-earning').append(result).hide().fadeIn(100);
                $('#spinloadingmain').remove();
            }
        });

        $.ajax({
            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewdeduction&refer=<%= refer%>",
            success: function (result) {
                $('.accordion-deduction').append(result).hide().fadeIn(100);
                $('#spinloadingmain').remove();
            }
        });

        $.ajax({
            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewpaysheet&refer=<%= refer%>&start=1",
            success: function (result) {
                $('#content-paysheet').append(result).hide().fadeIn(100);
                $('#spinloadingmain').remove();
            }
        });

        $('body').on('click', '.deletethis', function (e) {
            e.stopImmediatePropagation();

            e.preventDefault();
            if (e.handled !== true) { //Checking for the event whether it has occurred or not.
                e.handled = true; // Basically setting value that the current event has occurred.
                var c = $(this).attr('id');
                var d = $(this).attr('type');
                var r = $(this).attr('title');

                var em = r.toLowerCase();

                BootstrapDialog.confirm({
                    title: '<span style="color:#d9534f;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp; <strong>Confirmation</strong></span>',
                    message: 'You are about to delete this, proceed?',
                    type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    btnCancelClass: 'btn-primary',
                    btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                    btnOKLabel: 'Proceed', // <-- Default value is 'OK',
                    btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                    callback: function (result) {
                        // result will be true if button was click, while it will be false if users close the dialog directly.
                        $.ajax({
                            url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=delete" + r + "&refer=" + d + "&code=" + c,
                            success: function (result) {

                                if (result == 1) {
                                    $('#panel-' + em + '-' + c).remove();
                                }
                            }
                        });
                    }
                });


            }


            return false;
        });

        $('body').on('change', '.changeAmt', function (e) {
            e.stopImmediatePropagation();

            e.preventDefault();
            if (e.handled !== true) { //Checking for the event whether it has occurred or not.
                e.handled = true; // Basically setting value that the current event has occurred.
                var c = $(this).attr('id');
                var d = $(this).attr('title');
                var r = $('#refer').val();
                var e = $(this).val();

                var ty = $(this).attr('id1');

                console.log(ty);

                $.ajax({
                    url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=saveonspot&refer=" + r + "&earncode=" + d + "&staffid=" + c + "&amount=" + e + "&type=" + ty,
                    success: function (result) {

                        if (result > 0) {
                            $('#applicable-' + c).html('<span class="label label-success">Yes</span>');
                        } else {
                            $('#applicable-' + c).html('<span class="label label-primary">No</span>');
                        }
                    }
                });
            }
            return false;
        });

        $('.save').click(function (e) {

            var a = $(this).attr('id');
            var b = a.toLowerCase();
            $('#result-save-' + b).html('<i class="fa fa-spinner fa-spin fa-1x fa-fw"></i>&nbsp;&nbsp;Saving...').hide().fadeIn(100);

            e.preventDefault();
            if (e.handled !== true) { //Checking for the event whether it has occurred or not.
                e.handled = true; // Basically setting value that the current event has occurred.

                $.ajax({
                    url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=save" + a + "&refer=<%= refer%>&type=",
                    success: function (result) {

                        if (result == 1) {
                            $(".collapse").collapse('hide');
                            $('.panel-' + b + '-list-all').empty();
                            setTimeout(function () {
                                $('#result-save-' + b).html('<i class="fa fa-check-circle" aria-hidden="true" style="color:#1d9d73"></i>&nbsp;&nbsp;Saved');
                            }, 500);

                            setTimeout(function () {
                                $('#result-save-' + b).empty();
                            }, 5000);

                        } else {
                            setTimeout(function () {
                                $('#result-save-' + b).html('<i class="fa fa-exclamation-circle" aria-hidden="true" style="color:tomato"></i>&nbsp;&nbsp;Failed');
                            }, 500);
                        }
                    }
                });

            }
        });

        $('body').on('click', '.viewtype', function (e) {
            e.stopImmediatePropagation();
            $('#accordion').append('<div id="spinloadingmain" style="width:100px; margin:auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>').hide().fadeIn(100);

            e.preventDefault();
            if (e.handled !== true) { //Checking for the event whether it has occurred or not.
                e.handled = true; // Basically setting value that the current event has occurred.
                var c = $(this).attr('id');
                var d = $(this).attr('title');
                var r = $('#refer').val();

                console.log(e);

                $.ajax({
                    url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=viewearning&refer=" + r + "&view=" + c + "&earncode=" + d,
                    success: function (result) {
                        $('.accordion-earning').append(result).hide().fadeIn(100);
                        $('#spinloadingmain').remove();
                    }
                });
            }
            return false;
        });

        $('body').on('click', '.accordionfont', function (e) {
            e.stopImmediatePropagation();
            console.log(e.handled);
            e.preventDefault();
            if (e.handled !== true) { //Checking for the event whether it has occurred or not.
                e.handled = true; // Basically setting value that the current event has occurred.
                var c = $(this).attr('id');
                var d = $(this).attr('title');
                var r = $('#refer').val();

                var ty = d.toLowerCase();
                //console.log('#panel-body-content-list-' + ty + '-' + c);

                $('#loading-spin-' + ty + '-' + c).append('<span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-fw"></i></span>').hide().fadeIn(100);

                //console.log(e);

                $.ajax({
                    url: "PathController?moduleid=<%= mod.getModuleID()%>&process=getliststaff&refer=" + r + "&earncode=" + c + "&type=" + d + "&view=applicable",
                    success: function (result) {
                        if ($('#panel-body-content-list-' + ty + '-' + c).is(':empty')) {
                            console.log('empty');
                            $('.panel-' + ty + '-list-all').empty();
                            $('#panel-body-content-list-' + ty + '-' + c).html(result).hide().fadeIn(100);
                            $(".collapse").collapse('hide');
                            $('#collapse-' + ty + '-' + c).collapse('show');
                        } else {
                            console.log('notempty');
                            $('.panel-' + ty + '-list-all').empty();
                            $(".collapse").collapse('hide');
                        }

                        $('#loading-spin-' + ty + '-' + c).empty();
                    }
                });
            }
            return false;
        });

        $('body').on('click', '#view-list-type', function (e) {
            e.stopImmediatePropagation();
            e.preventDefault();
            if (e.handled !== true) { //Checking for the event whether it has occurred or not.
                e.handled = true; // Basically setting value that the current event has occurred.
                var c = $(this).attr('type');//earncode
                var d = $(this).attr('title');//type earning@deduction;
                var r = $('#refer').val();

                var viewby = '';

                if ($(this).hasClass('view-all')) {
                    console.log('current all');
                    $(this).removeClass('view-all').addClass('view-applicable');
                    viewby = 'applicable';
                    /*navListItems.removeClass('btn-primary').addClass('btn-default');
                     $item.addClass('btn-primary');*/
                } else if ($(this).hasClass('view-applicable')) {
                    console.log('current applicable');
                    $(this).removeClass('view-applicable').addClass('view-all');
                    viewby = 'all';
                }

                var ty = d.toLowerCase();
                console.log('#panel-body-content-list-' + ty + '-' + c);

                $('#loading-spin-' + ty + '-' + c).append('<span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-fw"></i></span>').hide().fadeIn(100);

                console.log(e);

                $.ajax({
                    url: "PathController?moduleid=<%= mod.getModuleID()%>&process=getliststaff&refer=" + r + "&earncode=" + c + "&type=" + d + "&view=" + viewby,
                    success: function (result) {

                        $('#panel-body-content-list-' + ty + '-' + c).html(result).hide().fadeIn(100);



                        $('#loading-spin-' + ty + '-' + c).empty();
                    }
                });

            }
            return false;
        });

        $('#step-payslip').click(function (e) {


            e.preventDefault();
            if (e.handled !== true) { //Checking for the event whether it has occurred or not.
                e.handled = true; // Basically setting value that the current event has occurred.
                var c = $(this).attr('id');
                var d = $(this).attr('title');
                var r = $('#refer').val();

                $('#content-payslip').append('<div id="spinloadingmain" style="width:100px; margin:auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>').hide().fadeIn(100);

                console.log(e);

                $.ajax({
                    url: "PathController?moduleid=<%= mod.getModuleID()%>&process=getlistpayslip&refer=" + r,
                    success: function (result) {
                        $('#content-payslip').html(result).hide().fadeIn(100);
                        //$('#content-payslip').empty();
                    }
                });
            }
            return false;
        });
        
        $('#step-paysheet').click(function (e) {


            e.preventDefault();
            if (e.handled !== true) { //Checking for the event whether it has occurred or not.
                e.handled = true; // Basically setting value that the current event has occurred.
                var c = $(this).attr('id');
                var d = $(this).attr('title');
                var r = $('#refer').val();

                $('#content-paysheet').html('<div id="spinloadingmain" style="width:100px; margin:auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>').hide().fadeIn(100);

                console.log(e);

                $.ajax({
            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewpaysheet&refer="+r+"&start=1",
            success: function (result) {
                $('#content-paysheet').html(result).hide().fadeIn(100);
                $('#spinloadingmain').remove();
            }
        });
            }
            return false;
        });

        $('body').on('click', '.view-payslip', function (e) {
            e.stopImmediatePropagation();
            e.preventDefault();
            if (e.handled !== true) { //Checking for the event whether it has occurred or not.
                e.handled = true; // Basically setting value that the current event has occurred.
                var c = $(this).attr('id');//earncode
                var d = $(this).attr('title');//type earning@deduction;
                var r = $('#refer').val();

                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DEFAULT,
                    size: BootstrapDialog.SIZE_WIDE,
                    //animate: false,
                    title: 'View Pay Slip',
                    message: function (dialog) {
                        var $content = $('<body></body>').load('em_payroll_payslip_view.jsp?refer=' + r + '&staffid=' + c);
                        //$('body').on('click', '.thisresult_nd', function(event){
                        //    dialog.close();
                        //    event.preventDefault();
                        //});
                        return $content;
                    },
                    buttons: [{
                            label: 'Close',
                            action: function (dialogRef) {
                                dialogRef.close();
                            }
                        }]
                });

            }
            return false;
        });

        $('body').on('click', '.pageno', function (e) {
            e.stopImmediatePropagation();
            e.preventDefault();
            if (e.handled !== true) { //Checking for the event whether it has occurred or not.
                e.handled = true; // Basically setting value that the current event has occurred.

                var p = $(this).attr('href');

                $.ajax({
                    url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewpaysheet&refer=<%= refer%>&start=" + p,
                    success: function (result) {
                        $('#content-paysheet').html(result).hide().fadeIn(100);
                        $('#spinloadingmain').remove();
                    }
                });

            }

        });
        
        $('.print-paysheet').click(function (ev) {
            ev.stopImmediatePropagation();
            ev.preventDefault();
            if (ev.handled !== true) { //Checking for the event whether it has occurred or not.
                ev.handled = true; // Basically setting value that the current event has occurred.
                var c = $(this).attr('id');//earncode
                var d = $(this).attr('title');//type earning@deduction;
                var r = $('#refer').val();

                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DEFAULT,
                    size: BootstrapDialog.SIZE_WIDE,
                    title: 'View Pay Slip',
                    cssClass: 'print-dialog-landscape',
                    message: function (dialog) {
                        var $content = $('<body></body>').load('em_payroll_paysheet_print.jsp?refer=' + r + '&start=1');
                        //$('body').on('click', '.thisresult_nd', function(event){
                        //    dialog.close();
                        //    event.preventDefault();
                        //});
                        return $content;
                    },
                    buttons: [{
                            label: 'Close',
                            action: function (dialogRef) {
                                dialogRef.close();
                            }
                        }]
                });

            }
            return false;
        });
    });
</script>


<div class="well">
    <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
        <tr>
            <td valign="middle" align="left">
                <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist" type="<%//= v.getCode() %>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                <p></p>
                <label>Reference No : <%= refer%> | Year : <%= EmPayrollDAO.getPayroll(log, refer).getYear()%> | Period : <%= EmPayrollDAO.getPayroll(log, refer).getPeriod()%></label>

            </td>
        </tr>
    </table>
    <input type="hidden" id="refer" value="<%=refer%>">
    <div class="row form-group">
        <div class="col-xs-12">
            <ul class="nav nav-pills nav-justified thumbnail setup-panel">
                <!--<li class="active"><a href="#step-1">
                        <h2 class="list-group-item-heading">Step 1 </h2>
                        <p class="list-group-item-text">Set Earning</p>
                    </a></li>
                <li class="disabled"><a href="#step-2">
                        <h2 class="list-group-item-heading">Step 2</h2>
                        <p class="list-group-item-text">Set Deduction</p>
                    </a></li>
                <li class="disabled"><a href="#step-3">
                        <h2 class="list-group-item-heading">Step 3</h2>
                        <p class="list-group-item-text">Overtime Claim</p>
                    </a></li>
                <li class="disabled"><a href="#step-4">
                        <h2 class="list-group-item-heading">Step 4</h2>
                        <p class="list-group-item-text">Pay Slip</p>
                    </a></li>
                <li class="disabled"><a href="#step-5">
                        <h2 class="list-group-item-heading">Step 5</h2>
                        <p class="list-group-item-text">Pay Sheet</p>
                    </a></li>
                <li class="disabled"><a href="#step-5">
                        <h2 class="list-group-item-heading">Step 6</h2>
                        <p class="list-group-item-text">Finalize</p>
                    </a></li>-->

                <li class="active"><a href="#step-1" class="closecontent">
                        <h2 class="list-group-item-heading">Step 1 <!--<span style="color: green"><i class="fa fa-check-circle" aria-hidden="true"></i></span>--></h2>
                        <p class="list-group-item-text">Set Earning</p>
                    </a></li>
                <li class=""><a href="#step-2" class="closecontent">
                        <h2 class="list-group-item-heading">Step 2</h2>
                        <p class="list-group-item-text">Set Deduction</p>
                    </a></li>
                <li class=""><a href="#step-3" id="step-payslip" class="closecontent">
                        <h2 class="list-group-item-heading">Step 3</h2>
                        <p class="list-group-item-text">Pay Slip</p>
                    </a></li>
                <li class=""><a href="#step-4" id="step-paysheet" class="closecontent">
                        <h2 class="list-group-item-heading">Step 4</h2>
                        <p class="list-group-item-text">Pay Sheet</p>
                    </a></li>
                <li class=""><a href="#step-5" class="closecontent">
                        <h2 class="list-group-item-heading">Step 5</h2>
                        <p class="list-group-item-text">Finalize</p>
                    </a></li>
            </ul>
        </div>
    </div>
    <div class="row setup-content" id="step-1">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="row">
                            <a href="#" id="Earning" class="add icon-link"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add Earning</a>&nbsp;&nbsp;
                            <a href="#" id="Earning" class="save icon-link save-earning"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save All Earning</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="result-save-earning"></span>

                            <p></p>
                            <div class="panel-group accordion-earning" id="accordion-Earning" role="tablist" aria-multiselectable="true">
                                <!-- earning content -->
                            </div>
                        </div>

                        <button class="btn btn-default nextBtn btn-sm pull-right" id="activate-step-2" type="button" >Next</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="row">
                            <a href="#" id="Deduction" class="add icon-link"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add Deduction</a>&nbsp;&nbsp;
                            <a href="#" id="Deduction" class="save icon-link"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save All Deduction</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="result-save-deduction"></span>

                            <p></p>
                            <div class="panel-group accordion-deduction" id="accordion-Deduction" role="tablist" aria-multiselectable="true">
                                <!-- earning content -->
                            </div>
                        </div>

                        <button class="btn btn-default nextBtn btn-sm pull-right" id="activate-step-3" type="button" >Next</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-3">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="row">
                            <a href="#" id="Deduction" class="add icon-link"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add Deduction</a>&nbsp;&nbsp;
                            <a href="#" id="Deduction" class="save icon-link"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save All Deduction</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="result-save-deduction"></span>

                            <p></p>

                            <div id="content-payslip"></div>

                        </div>

                        <button class="btn btn-default nextBtn btn-sm pull-right" id="activate-step-3" type="button" >Next</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-4">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="row">
                            <a href="#" id="" class="icon-link print-paysheet"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Print Pay Sheet</a>&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;<span id="result-save-deduction"></span>

                            <p></p>

                            <div id="content-paysheet"></div>

                        </div>

                        <button class="btn btn-default nextBtn btn-sm pull-right" id="activate-step-3" type="button" >Next</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row setup-content" id="step-5">
        <div class="col-xs-12">
            <div class="col-md-12 well">
                <h1 class="text-center"> STEP 5</h1>
            </div>
        </div>
    </div>
</div>