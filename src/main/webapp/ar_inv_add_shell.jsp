<%-- 
    Document   : ar_inv_add_shell
    Created on : Nov 11, 2016, 12:50:55 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArCPORefinery"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        var b = '<%= request.getParameter("dispatchno")%>';


        console.log(b);

        var $newrow = '<tr id="' + b + '" class="activerowy1">'
                + '<td width="3%" class="tdrow-contract" id="' + b + '"><label>'
                + '<input type="checkbox" id="chkbox' + b + '" name="checkboxdisno" class="chcbox-contract" value="' + b + '" checked required></label>'
                + '</td>'
                + '<td class="tdrow-contract" id="' + b + '"><input type="text" class="form-control input-sm dateformat" id="date' + b + '" name="date' + b + '" value="" autocomplete="off"></td>'
                + '<td class="tdrow-contract" id="' + b + '"><input type="text" class="form-control input-sm disno" id="dispatchno' + b + '" name="dispatchno' + b + '" value="' + b + '" autocomplete="off"></td>'
                + '<td class="tdrow-contract" id="' + b + '"><input type="text" class="form-control input-sm" id="trailer' + b + '" name="trailer' + b + '" value="" autocomplete="off"></td>'

                + '<td class="tdrow"><input type="text" class="form-control input-sm mt_refinery_gen" id="mt_refinery' + b + '" name="mt_refinery' + b + '" value="0.00" autocomplete="off" required></td>'
                + '</tr>';

        $('#refinery-body').prepend($newrow);
        $('#latestdisno').val(b);



        $('#refinery-body').on('keyup', '.mt_refinery_gen', function (e) {

            var disno = [];
            var x = 0;
            var j = 0;

            $.each($('.disno'), function () {
                disno.push($(this).val());
            });

            console.log(disno);

            for (i = 0; i < disno.length; i++) {
                j = $('#mt_refinery' + disno[i]).val();
                console.log(disno[i] + '--' + j);
                //x = j + x;
                x = (parseFloat(j) + parseFloat(x));
                //console.log(i+'---'+j);
                //console.log($('#mt_refinery'+i).val());
            }



            $('#sum_mt_refinery').val(x.toFixed(2));

            //var number1 = Number(oriamount.replace(/[^0-9\.]+/g,""));
            //    var number2 = Number(newamount.replace(/[^0-9\.]+/g,""));
            //    var bal = parseFloat(number1)-parseFloat(number2);



            //console.log('totalrow='+t);
            console.log('total=' + x);


        });


        /*$('#refinery-body').on('click', '.tdrow-contract', function(e) {
         e.preventDefault();
         var b = $(this).attr('id');
         //$(this).toggleClass('info');
         // $('#chkbox'+b).prop('checked', true);
         var checkBox = $('#chkbox'+b);
         checkBox.prop("checked", !checkBox.prop("checked"));
         
         var input1 = $('#mt_mill'+b);
         var input2 = $('#mt_refinery'+b);
         
         input1.prop("disabled", !input1.prop("disabled"));
         input2.prop("disabled", !input2.prop("disabled"));
         $('.mt_refinery_gen').trigger('keyup');
         
         });*/

        $('.adddispatchno').click(function (e) {



            var latest = $('#latestdisno').val();
            var b = '';



            latest = parseInt(latest) + 1;

            var str = "" + latest;
            var pad = "00000";
            var ans = pad.substring(0, pad.length - str.length) + str;

            b = ans;


            console.log('latest--' + b);

            var $newrow = '<tr id="' + b + '" class="activerowy1">'
                    + '<td width="3%" class="tdrow-contract" id="' + b + '"><label>'
                    + '<input type="checkbox" id="chkbox' + b + '" name="checkboxdisno" class="chcbox-contract" value="' + b + '" checked required></label>'
                    + '</td>'
                    + '<td class="tdrow-contract" id="' + b + '"><input type="text" class="form-control input-sm dateformat" id="date' + b + '" name="date' + b + '" value="" autocomplete="off"></td>'
                    + '<td class="tdrow-contract" id="' + b + '"><input type="text" class="form-control input-sm disno" id="dispatchno' + b + '" name="dispatchno' + b + '" value="' + b + '" autocomplete="off"></td>'
                    + '<td class="tdrow-contract" id="' + b + '"><input type="text" class="form-control input-sm" id="trailer' + b + '" name="trailer' + b + '" value="" autocomplete="off"></td>'

                    + '<td class="tdrow"><input type="text" class="form-control input-sm mt_refinery_gen" id="mt_refinery' + b + '" name="mt_refinery' + b + '" value="0.00" autocomplete="off" required></td>'
                    + '</tr>';

            $('#refinery-body').prepend($newrow);
            $('#latestdisno').val(b);


            //e.preventDefault();
            $('.dateformat').datepicker({
                format: 'yyyy-mm-dd',
                defaultDate: 'now',
                autoclose: true
            });

            return false;
        });



    });
</script>
<button  class="btn btn-default btn-xs adddispatchno" title="" id="" name="adddispatchno"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Dispatch No</button>
<form data-toggle="validator" role="form" id="saverefinery">
    <input type="hidden" id="selected-contract">
    <input type="hidden" id="latestdisno" value="00">

    <p></p>
    <table class="table table-bordered table-striped table-hover" id="list1">
        <thead>

            <tr>
                <th></th>
                <th>Date</th>
                <th>Dispatch No</th>
                <th>Trailer</th>
                <th>Mt</th>
            </tr>
        </thead>
        <tbody id="refinery-body">

            <%List<ArCPORefinery> listx = (List<ArCPORefinery>) SalesInvoiceDAO.getListProd(log,request.getParameter("contract"), request.getParameter("millcode"), request.getParameter("prodcode"));
                int i = 0;
                double sumMt = 0.0;
                double sumMtRefinery = 0.0;
                for (ArCPORefinery j : listx) {

                    i++;
                    sumMt += Double.parseDouble(j.getMt());
                    sumMtRefinery += j.getMtRefinery();
            %>
            <tr id="<%= j.getDispatchNo()%>" class="activerowy1">
                <td width="3%" class="tdrow-contract" id="<%= j.getDispatchNo()%>"><label>
                        <input type="checkbox" id="chkbox<%= j.getDispatchNo()%>" name="checkboxdisno" class="chcbox-contract" value="<%= j.getDispatchNo()%>" required>
                    </label>
                </td>
                <td class="tdrow-contract" id="<%= j.getDispatchNo()%>"><input type="hidden" id="date<%= j.getDispatchNo()%>" name="date<%= j.getDispatchNo()%>" value="<%= j.getDate()%>"><%= j.getDate()%></td>
                <td class="tdrow-contract" id="<%= j.getDispatchNo()%>"><input type="hidden" id="dispatchno<%= j.getDispatchNo()%>" name="dispatchno<%= j.getDispatchNo()%>" value="<%= j.getDispatchNo()%>"><%= j.getDispatchNo()%></td>
                <td class="tdrow-contract" id="<%= j.getDispatchNo()%>"><input type="hidden" id="trailer<%= j.getDispatchNo()%>" name="trailer<%= j.getDispatchNo()%>" value="<%= j.getTrailer()%>"><%= j.getTrailer()%></td>
                <td class="tdrow"><input type="text" class="form-control input-sm mt_refinery_gen" id="mt_refinery<%= j.getDispatchNo()%>" name="mt_refinery<%= j.getDispatchNo()%>" value="<%= j.getMtRefinery()%>" required disabled></td>
            </tr>
            <%}%>
            <tr id="<%//= j.getId()%>">
                <td></td>
                <td colspan="3"><span class="pull-right"><strong>Total</strong></span>
                    <input type="hidden" id="totalrow" name="totalrow" value="<%=i%>">
                    <input type="hidden" id="contractno" name="contractno" value="<%= request.getParameter("contract")%>">
                </td>

                <td class="tdrow"><input type="text" class="form-control input-sm" id="sum_mt_refinery" name="sum_mt_refinery" value="<%= GeneralTerm.normalCredit(sumMtRefinery)%>" required></td>
            </tr>
        </tbody>

    </table>
</form>