<%-- 
    Document   : print_ffb
    Created on : Apr 7, 2017, 10:36:30 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.dao.TaxDAO"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArFFB"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.template.voucher.FormattedVoucher"%>
<%@page import="com.lcsb.fms.template.voucher.VoucherMaster"%>
<%@page import="com.lcsb.fms.template.voucher.PaymentVoucher_Template"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    //FormattedVoucher v = (FormattedVoucher) VoucherMaster.getMaster(request.getParameter("refer"), log);
    Module mod = (Module) SalesInvoiceDAO.getModule();
    ArFFB ar = (ArFFB) SalesInvoiceDAO.getFFBPrint(log, request.getParameter("refer"));
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/pvoucher.css?1" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {

                $('#print').click(function (e) {

                    printElement($("#viewjv").html());

                });

            });
        </script>


    </head>
    <body>
        <div id ="maincontainer">
            <div class="partition"> 
                <div class="headofpartition"></div>
                <div class="bodyofpartition">
                    <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                        <tr>
                            <td valign="middle" align="left">


                            </td>
                        </tr>
                    </table>

                    <table id="table_left" width="100%" cellspacing="0" border="0">
                        <tr>
                            <td valign="middle" width="33%" align="left">
                                <div class="btn-group btn-group-sm" role="group" aria-label="...">


                                    <div class="btn-group" role="group" aria-label="...">
                                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i></button>

                                        <button id="print" class="btn btn-default btn-sm" title="<%//= mod.getModuleID()%>" name="viewlist"><i class="fa fa-print" aria-hidden="true"></i> Print</button>




                                    </div>
                                </div>
                                <span>&nbsp;&nbsp;<strong><%= ar.getInvref()%></strong></span>
                            </td>
                            <td width="60%" style="vertical-align: middle;">
                                <!--<div class="form-group" id="div_code">
                                    
                                    <select class="form-control input-xs" id="reflexcb" name="reflexcb">
                                <%//= ParameterDAO.parameterList("YesNo Type", "No")%>
                            </select>   
                        </div>-->
                            </td>
                        </tr>
                    </table>
                    <table id="maintbl" width="100%" cellspacing="0" border="0">
                        <tr>
                            <td class="greybg">
                                <table id="table_left" width="100%" cellspacing="0">
                                    <tr>
                                        <td width="100%">
                                            <div id="viewjv" style="overflow-y: scroll; height:600px;">
                                                <div class="page">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <table class="table table-bordered " id="list">
                                                                <thead>

                                                                    <tr>
                                                                        <th>TOTAL FFB SUPPLIED</th>
                                                                        <th class="pull-right"><%= ar.getFfb()%> MT</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <p>Average Shipment Month Price of Palm Oil</p>
                                                                            <p>Transport of Palm Oil</p>
                                                                            <p>CESS MPOB</p>
                                                                            <p>AVR Discounted Price for High FFA CPO</p>
                                                                            <p>Nett Price of Crude Palm Oil</p>
                                                                            <p>&nbsp;</p>
                                                                            <p>Average Shipment Month Price of Palm Kernel</p>
                                                                            <p>Extraction Rate of Palm Oil</p>
                                                                            <p>Extraction Rate of Palm Kernel</p>
                                                                            <p>&nbsp;</p>
                                                                            <p>Cost of Palm Oil Per M/Ton FFB</p>
                                                                            <p>Cost of Palm Kernel Per M/Ton FFB</p>
                                                                            <p>Total Cost Per M/Ton FFB</p>
                                                                            <p>&nbsp;</p>
                                                                            <p>Less : a) Processing Cost Per M/Ton FFB</p>
                                                                            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b) External FFB Transport</p>
                                                                            <p>&nbsp;</p>
                                                                            <p>Price Per M/Ton FFB</p>
                                                                            <p>&nbsp;</p>
                                                                        </td>
                                                                        <td class="pull-right">
                                                                            <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getMpobpricecpo())%></span></p>
                                                                            <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getTransport())%></span></p>
                                                                            <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getCess())%></span></p>
                                                                            <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getDiscount())%></span></p>
                                                                            <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getNettcpo())%></span></p>
                                                                            <p>&nbsp;</p>
                                                                            <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getMpobpricepk())%></span></p>
                                                                            <p><span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getOer())%></span></p><p>&nbsp;</p>
                                                                            <p><span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getKer())%></span></p>
                                                                            <p>&nbsp;</p><p>&nbsp;</p>
                                                                            <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getCostcpo())%></span></p>
                                                                            <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getCostpk())%></span></p>
                                                                            <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getTotalcost())%></span></p>
                                                                            <p>&nbsp;</p>
                                                                            <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getMillcost())%></span></p>
                                                                            <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getExternalffb())%></span></p>
                                                                            <p>&nbsp;</p>
                                                                            <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getPriceperton())%></span></p>
                                                                            <p>&nbsp;</p>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>TOTAL</th>
                                                                        <th class="pull-right">RM&nbsp;&nbsp;<%= GeneralTerm.currencyFormat(ar.getTotal())%></th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>GST (<%= TaxDAO.getTax(log, "SR").getTaxrate()%>%)</th>
                                                                        <th class="pull-right">RM&nbsp;&nbsp;<%= GeneralTerm.currencyFormat(ar.getGst())%></th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>NETT AMOUNT</th>
                                                                        <th class="pull-right">RM&nbsp;&nbsp;<%= GeneralTerm.currencyFormat(ar.getNettamount())%>
                                                                            <input type="hidden" id="nettamount" value="<%= ar.getNettamount()%>">
                                                                        </th>
                                                                    </tr>
                                                                </tbody>

                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                </table>
                            </td>
                        </tr>
                        <input type="hidden" id="checkbyid" value="">
                        <input type="hidden" id="checkbyname" value="">
                        <input type="hidden" id="checkbydesign" value="">
                        <input type="hidden" id="checkdate" value="">
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
