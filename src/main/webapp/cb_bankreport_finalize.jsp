<%-- 
    Document   : cb_bankreport_finalize
    Created on : Mar 20, 2017, 3:34:19 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.financial.cashbook.OfficialCreditItem"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.OfficialReceipt"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.ORReceiptMaster"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucher"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.PaymentVoucherDAO"%>
<%@page import="com.lcsb.fms.model.financial.ap.ApPaymentMaster"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    String type = request.getParameter("type");

    if (type.equals("pv")) {
        ApPaymentMaster am = (ApPaymentMaster) PaymentVoucherDAO.getAllInfo(log,request.getParameter("sessionid"), log.getEstateCode(), log.getEstateDescp());
        PaymentVoucher v = am.getMaster();


%>

<table class="table table-bordered table-striped  table-hover">
    <thead>
        <tr class="info">
            <th colspan="4">Voucher Type : Payment Voucher
                <small class="pull-right">Please check voucher below before save.</small>

            </th>
        </tr>
    </thead>
    <tbody class="activerowm tdrow">
        <tr>
            <th width="15%">Payee Info</th>
            <td width="35%"><%= v.getPaidcode()%> - <%= v.getPaidname()%></td>
            <th width="15%">Period</th>
            <td width="35%"><%= v.getPeriod()%>, <%= v.getYear()%></td>
        </tr>
        <tr>
            <th>Paid Type</th>
            <td><%= v.getPaidtype()%></td>
            <th>Bank Info</th>
            <td><%= v.getBankcode()%> - <%= v.getBankname()%></td>
        </tr>
        <tr>
            <th>Cheque No</th>
            <td><%= v.getCekno()%></td>
            <th>Total Amount</th>
            <td><%= GeneralTerm.currencyFormat(v.getAmount())%></td>
        </tr>
        <tr>
            <th>Remark</th>
            <td colspan="3"><%= v.getRemarks()%></td>
        </tr>

    </tbody>
</table>

<%List<PaymentVoucherItem> listAll = (List<PaymentVoucherItem>) am.getListInvoice();
    int i = 0;
    for (PaymentVoucherItem j : listAll) {
        i++;
%>
<label class="control-label"># <%=i%></label>
<table class="table table-bordered table-striped  table-hover">

    <tbody class="activerowm tdrow">
        <tr>
            <th width="15%">Location</th>
            <td width="35%"><%= j.getLoclevel()%> / <%= j.getLoccode()%> / <%= j.getLocname()%></td>
            <th width="15%">Account</th>
            <td width="35%"><%= j.getCoacode()%> - <%= j.getCoadescp()%></td>
        </tr>
        <tr>
            <th>Sub Account</th>
            <td><%= j.getSatype()%> / <%= j.getSacode()%> / <%= j.getSadesc()%></td>
            <th>Amount</th>
            <td><%= GeneralTerm.currencyFormat(j.getAmount())%></td>
        </tr>
        <tr>
            <th>Tax Code</th>
            <td><%= j.getTaxcode()%></td>
            <th>Tax Amount</th>
            <td><%= GeneralTerm.currencyFormat(j.getTaxamt())%></td>
        </tr>
        <tr>
            <th>Remark</th>
            <td colspan="3"><%= j.getRemarks()%></td>
        </tr>


    </tbody>
</table>
<%}
    }else if (type.equals("or")) {

        ORReceiptMaster am = (ORReceiptMaster) OfficialReceiptDAO.getAllInfoGeneral(log,request.getParameter("sessionid"), log.getEstateCode(), log.getEstateDescp());
        OfficialReceipt v = am.getMaster();
%>

<table class="table table-bordered table-striped  table-hover">
    <thead>
        <tr class="info">
            <th colspan="4">Voucher Info
                <small class="pull-right">Please check voucher below before save.</small>

            </th>
        </tr>
    </thead>
    <tbody class="activerowm tdrow">
        <tr>
            <th width="15%">Payer Info</th>
            <td width="35%"><%= v.getPaidcode()%> - <%= v.getPaidname()%></td>
            <th width="15%">Period</th>
            <td width="35%"><%= v.getPeriod()%>, <%= v.getYear()%></td>
        </tr>
        <tr>
            <th>Paid Type</th>
            <td><%= v.getPaidtype()%></td>
            <th>Bank Info</th>
            <td><%= v.getBankcode()%> - <%= v.getBankname()%></td>
        </tr>
        <tr>
            <th>Cheque No</th>
            <td><%//= v.getCekno() %></td>
            <th>Total Amount</th>
            <td><%= GeneralTerm.currencyFormat(v.getAmount())%></td>
        </tr>
        <tr>
            <th>Remark</th>
            <td colspan="3"><%= v.getRemarks()%></td>
        </tr>

    </tbody>
</table>


<%List<OfficialCreditItem> listAll = (List<OfficialCreditItem>) am.getListInvoice();
    int i = 0;
    for (OfficialCreditItem j : listAll) {
        i++;
%>
<label class="control-label"># <%=i%></label>
<table class="table table-bordered table-striped  table-hover">

    <tbody class="activerowm tdrow">
        <tr>
            <th width="15%">Location</th>
            <td width="35%"><%= j.getLoclevel()%> / <%= j.getLoccode()%> / <%= j.getLocname()%></td>
            <th width="15%">Account</th>
            <td width="35%"><%= j.getCoacode()%> - <%= j.getCoadescp()%></td>
        </tr>
        <tr>
            <th>Sub Account</th>
            <td><%= j.getSatype()%> / <%= j.getSacode()%> / <%= j.getSadesc()%></td>
            <th>Amount</th>
            <td><%= GeneralTerm.currencyFormat(Double.parseDouble(j.getAmount()))%></td>
        </tr>
        <tr>
            <th>Tax Code</th>
            <td><%= j.getTaxcode()%></td>
            <th>Tax Amount</th>
            <td><%= GeneralTerm.currencyFormat(j.getTaxamt())%></td>
        </tr>
        <tr>
            <th>Remark</th>
            <td colspan="3"><%= j.getRemarks()%></td>
        </tr>


    </tbody>
</table>
<%}
    }%>