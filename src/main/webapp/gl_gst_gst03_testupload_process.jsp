<%-- 
    Document   : gl_gst_gst03_testupload_process
    Created on : Oct 8, 2014, 3:55:24 PM
    Author     : mahadzir
--%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lowagie.text.pdf.AcroFields"%>
<%@page import="com.lowagie.text.pdf.PdfStamper"%>
<%@page import="com.lowagie.text.pdf.PRStream"%>
<%@page import="com.lowagie.text.pdf.PdfReader"%>
<%@page import="com.lowagie.text.pdf.PdfString"%>
<%@page import="com.lowagie.text.pdf.PdfName"%>
<%@page import="com.lowagie.text.pdf.PdfObject"%>
<%@page import="com.lowagie.text.pdf.PdfDictionary"%>
<%@page import="com.lowagie.text.pdf.AcroFields.Item"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.File"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.apache.commons.fileupload.FileUploadException"%>
<%@page import="java.io.InputStream"%>
<%@page import="org.apache.commons.io.FilenameUtils"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%!
Connection con;
public class AcroFieldJSScanner {


  
	protected ArrayList<String> functions = null;
	
	
	public void getFieldFunctions(Item item) throws IOException{
		
		PdfDictionary dict;
		
		for (int i = 0; i < item.size(); i++) {
			dict = item.getMerged(i);
		
			scanPdfDictionary(dict);
			
//			dict = item.getWidget(i);
//			
//			scanPdfDictionary(dict);
		}
	}
	
	protected void scanPdfDictionary(PdfDictionary dict) throws IOException{
		
		PdfObject objJS = null;
		String func = null;
		
		objJS = dict.get(PdfName.JS);
		if (dict.get(PdfName.S) != null &&  objJS != null && objJS.isString()){
			
			PdfString strJS = (PdfString)objJS;
			if (functions == null){
				functions = new ArrayList<String>();
			}
			
			func = strJS.toString();
			functions.add(func);
		}else if (dict.get(PdfName.S) != null &&  objJS != null){
			
			for(Object obj : dict.getKeys()){
				PdfName pdfName = (PdfName)obj;
			
				PdfObject pdfObj = dict.get(pdfName);
				
				if (pdfObj.isIndirect()){
					PdfObject pdfIndirectObject = PdfReader.getPdfObject(pdfObj);

					func = new String(PdfReader.getStreamBytes((PRStream)pdfIndirectObject));
					
					if (functions == null){
						functions = new ArrayList<String>();
					}
					
					functions.add(func);
				}else{
					scanPdfObject(pdfObj);
				}
				
			}
			
			
		}else{
			for(Object obj : dict.getKeys()){
				PdfName pdfName = (PdfName)obj;

				PdfObject pdfObj = dict.get(pdfName);
				scanPdfObject(pdfObj);
			}
		}

	}
	
	protected void scanPdfObject(PdfObject parentPdfObject) throws IOException{
		
		if (parentPdfObject.isDictionary()){
			scanPdfDictionary((PdfDictionary)parentPdfObject);
		}else if (parentPdfObject.isIndirect()){
			PdfObject pdfObject = PdfReader.getPdfObject(parentPdfObject);
			scanPdfObject(pdfObject);
		}
	}

	public ArrayList<String> getFunctions() {
		return functions;
	}

	public String toString(){
		
		StringBuilder sb = null;
		
		if (getFunctions() != null){
			sb = new StringBuilder();
			
			for (int i =0; i< getFunctions().size();i++) {
				
				sb.append(getFunctions().get(i)).append("\n");		
			} 
		}else{
			return "No functions found";
		}
		
		return sb.toString();
	}
	
}
%>
<%
    
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
con = log.getCon();
    response.setContentType("application/pdf");
    SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
//    String filename = "";
//    String fileExtension = "";
//    String databaseName = "";
//    String tempFileName = "";
//    String destinationDir = "/Users/developerteamitdepartment/Desktop/kosma/test/";
//    String fileNameAfterExtract = "";
    //InputStream filecontent=null;
    //FileItem fi=null;
    //SimpleDateFormat sdf=new SimpleDateFormat("_ddMMyyyy_HHmmss");
   // try {

       // List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
		
		    String uri = request.getScheme() + "://"
                + request.getServerName()
                + ("http".equals(request.getScheme()) && request.getServerPort() == 80 || "https".equals(request.getScheme()) && request.getServerPort() == 443 ? "" : ":" + request.getServerPort())
                + request.getContextPath()
                + "/tx_gst03_view.PDF";
                    
                    System.out.println(uri);

        URL u = new URL(uri);
        URLConnection uc = u.openConnection();
		
        //for (FileItem item : items) {
         //   if (!item.isFormField()) {
                // Process form file field (input type="file").
                //String fieldname = item.getFieldName();
                //PrintWriter pout = response.getWriter();
                //filename = FilenameUtils.getName(item.getName());
                //fileExtension = FilenameUtils.getExtension(item.getName());

               // PdfReader pdfReader = new PdfReader(item.getInputStream());
                //AcroFields acroFields = pdfReader.getAcroFields();
		PdfReader pdfReader = new PdfReader(uc.getInputStream());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                PdfStamper stamper = new PdfStamper(pdfReader, baos);
                //AcroFields acroFields = pdfReader.getAcroFields();
                AcroFields acroFields = stamper.getAcroFields();
                Map<String, AcroFields.Item> fields = acroFields.getFields();
                Set<Entry<String, AcroFields.Item>> entrySet = fields.entrySet();
                
//                for (Entry<String, AcroFields.Item> entry : entrySet) {
//                    System.out.println(entry.getKey());
//                    acroFields.setField(entry.getKey(), entry.getKey());
//                }
                //System.out.println("<br>");
                //System.out.println(fields.entrySet().size());
               
                       // String js = "var f = this.getField('NameBusiness'); \n f.value ='56';"; 
                        //stamper.addJavaScript(js);
						
						String [] fieldname={"GSTNo","NameBusiness","StartDate","EndDate","PaymentDate","totalVal","totalOut","GSTNo1","totalAcquisition","totalInputTax","amountPayable","amountClaimable","chkBox1","chkBox2","totalSupp","totalExport","totalExempt","totalGranted","totalGoods","totalSuspend","totalCapital","totalRelief","totalRecovered","code1","code2","code3","code4","code5","output1","output2","output3","output4","output5","output6","output7","prcnt1","prcnt2","prcnt3","prcnt4","prcnt5","prcnt6","NameApp","NewIC","OldIC","PassNo","Nationality","Date","Signature"};
						
						String [] valueitem={"gstid","locname","taxstart","taxend","taxdue","c1","c2","gstid","c3","c4","pay","claim","","","c6","c7","c8","c9","c10","","c11","c12","c13","i14","i16","i18","i20","i22","c15","c17","c19","c21","c23","c24","","prcnt1","prcnt2","prcnt3","prcnt4","prcnt5","prcnt6","appname","NewIC","OldIC","PassNo","nationality","appdate","",""};
					
						for(int x=0;x<fieldname.length;x++){
						
								if(!valueitem[x].equalsIgnoreCase(""))
						  
								  {
                                                                      
                                                                      //System.out.println("select "+valueitem[x]+" from tx_gst03 where refer='"+request.getParameter("refer")+"'");
                                                                           
								     Statement stmt=con.createStatement();
								     ResultSet st=stmt.executeQuery("select "+valueitem[x]+" from tx_gst03 where refer='"+request.getParameter("refer")+"'"); 
                                                                     //System.out.println("select "+valueitem[x]+" from tx_gst03 where refer='"+request.getParameter("refer")+"';");
								     if(st.next())
								     {
                                                                         System.out.println(st.getString(valueitem[x]).concat(" ").concat(String.valueOf(st.getMetaData().getColumnType(1))));
                                                                         //if(st.getMetaData().getColumnClassName(column))
                                                                         //st.getMetaData().get
                                                                         if(st.getMetaData().getColumnType(1)==91)
                                                                         {
                                                                             try{valueitem[x]=sdf.format(st.getDate(valueitem[x]));}catch(Exception ex){valueitem[x]="";}
                                                                         }
                                                                             else
									    valueitem[x]=st.getString(valueitem[x]);
								     }
								  }
							
						acroFields.setField(fieldname[x], valueitem[x]);	
						//acroFields.setField(fieldname[x], valueitem[x]+"/"+fieldname[x]);	
						
						} //end for
						
						
                stamper.close();
                pdfReader.close();
                OutputStream os = response.getOutputStream();
                baos.writeTo(os);
                os.flush();
                
          //  }
       // }
        System.out.println("Successfully Uploaded, click back to upload another file");
    } catch (FileUploadException e) {
     //   System.out.println("Upload Failed");
      //  throw new ServletException("Cannot parse multipart request.", e);
    }

%>
