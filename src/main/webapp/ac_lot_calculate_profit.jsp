<%-- 
    Document   : ac_lot_calculate_profit
    Created on : Dec 20, 2016, 12:55:29 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.model.setup.configuration.LotInformation"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.LotInformationDAO"%>
<%@page import="com.lcsb.fms.dao.management.activity.LotActivityDAO"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LotInformation lot = (LotInformation) LotInformationDAO.getInfo(request.getParameter("lotcode"));
%>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('.currency-format').change(function (e) {

            var a = $(this).val();
            //console.log(a);
            a = parseFloat(a).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

            $(this).val(a);

            return false;
        });

        $('.calc-profit').keyup(function (e) {

            var a = $('#cur-profit').val().replace(/\,/g, '');
            var b = $('#pre-profit').val().replace(/\,/g, '');

            var c = parseFloat(a) + parseFloat(b);
            //console.log(c);
            c = parseFloat(c).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

            $('#tot-profit').val(c);

            var balafterdist = parseFloat($('#tot-profit').val().replace(/\,/g, '')) - parseFloat($('#tot-distprofit').val().replace(/\,/g, ''));
            $('#bal-afterdist').val(balafterdist.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));

            var d = $('#lkpp-profit').val().replace(/\,/g, '');
            var e = $('#pre-dist').val().replace(/\,/g, '');

            var f = parseFloat(d) + parseFloat(e);
            //console.log(f);
            f = parseFloat(f).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

            $('#tot-distprofit').val(f);

            var balafterdist = parseFloat($('#tot-profit').val().replace(/\,/g, '')) - parseFloat($('#tot-distprofit').val().replace(/\,/g, ''));
            $('#bal-afterdist').val(balafterdist.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));

            var g = $('#rolling-prov').val().replace(/\,/g, '');
            var h = $('#preyear-prov').val().replace(/\,/g, '');
            var i = $('#year-prov').val().replace(/\,/g, '');

            var j = parseFloat(g) + parseFloat(h) + parseFloat(i);
            //console.log(j);
            //c = parseFloat(c).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

            var k = $('#bal-afterdist').val().replace(/\,/g, '');

            var l = parseFloat(k) - j;

            $('#distributable-prof').val(l.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));

            var profperhect = (parseFloat(l) / parseFloat($('#hect').val())).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

            $('#prof-perhect').val(profperhect);

            var m = Math.floor(profperhect.replace(/\,/g, '') / 100);
            console.log(m * 100);

            $('#suggested').val((m * 100).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));

            //String roundedx = dfdx.format(m);
            //tt = Double.parseDouble(roundedx);
            //tt = tt - 1;

            //double rounded = tt*100;




            return false;
        });

    });

</script>
<form data-toggle="validator" role="form" id="savecalculation">
    <input type="hidden" name="acre" id="acre" value="<%= lot.getLotAcre()%>">
    <input type="hidden" name="hect" id="hect" value="<%= lot.getLotHect()%>">
    <table class="table table-bordered table-striped  table-hover">
        <thead>
            <tr>
                <th colspan="4">Voucher Information
                    <!--<small><span class="label label-primary">Preparing</span></small>-->
                    <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">


                    </div>
                </th>
            </tr>
        </thead>
        <tbody class="activerowm tdrow">
            <tr>
                <td width="70%">Keuntungan Semasa</td>
                <td width="30%"><input type="text" class="form-control input-sm text-right currency-format calc-profit" id="cur-profit" name="cur-profit" value="<%= GeneralTerm.currencyFormat(LotActivityDAO.getProfit(lot.getLoccode(), request.getParameter("year"), request.getParameter("period")))%>" required></td>
            </tr>
            <tr>
                <td>Keuntungan Tahun Sebelum</td>
                <td><input type="text" class="form-control input-sm text-right currency-format calc-profit" id="pre-profit" name="pre-profit" value="0.00" required></td>
            </tr>
            <tr>
                <th>Jumlah Keuntungan</th>
                <th><input type="text" class="form-control input-sm  text-right currency-format" id="tot-profit" name="tot-profit" value="0.00" readonly></th>
            </tr>
            <tr>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th>Tolak :</th>
                <th></th>
            </tr>
            <tr>
                <th>Agihan Keuntungan</th>
                <th></th>
            </tr>
            <tr>
                <td>Telah diagihkan sebelum Mac Tahun 2012</td>
                <td><input type="text" class="form-control input-sm text-right currency-format  calc-profit" id="pre-dist" name="pre-dist" value="0.00" required></td>
            </tr>
            <tr>
                <td>Keuntungan Lot LKPP</td>
                <td><input type="text" class="form-control input-sm  text-right currency-format calc-profit" id="lkpp-profit" name="lkpp-profit" value="0.00" required></td>
            </tr>
            <tr>
                <td>Jumlah Agihan Keuntungan</td>
                <td><input type="text" class="form-control input-sm  text-right currency-format" id="tot-distprofit" name="tot-distprofit" value="0.00" readonly></td>
            </tr>
            <tr>
                <th>Baki Selepas Agihan</th>
                <th><input type="text" class="form-control input-sm  text-right currency-format" id="bal-afterdist" name="bal-afterdist" value="0.00" readonly></th>
            </tr>
            <tr>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th>Peruntukan Modal Pusingan</th>
                <th></th>
            </tr>
            <tr>
                <td>RM25 / hektar Tahun 2012</td>
                <td><input type="text" class="form-control input-sm  text-right currency-format calc-profit" id="rolling-prov" name="rolling-prov" value="0.00" required></td>
            </tr>
            <tr>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th>Tanaman Semula</th>
                <th></th>
            </tr>
            <tr>
                <td>Peruntukan Tanam Semula Tahun Lepas</td>
                <td><input type="text" class="form-control input-sm  text-right currency-format calc-profit" id="preyear-prov" name="preyear-prov" value="0.00" required></td>
            </tr>
            <tr>
                <td>Peruntukan Tanam Semula 2012</td>
                <td><input type="text" class="form-control input-sm  text-right currency-format calc-profit" id="year-prov" name="year-prov" value="0.00" required></td>
            </tr>
            <tr>
                <td>Peruntukan Tanam Semula Seekar Setahun</td>
                <td><input type="text" class="form-control input-sm  text-right currency-format" id="peracreyear" name="peracreyear" value="0.00" required></td>
            </tr>
            <tr>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <td>Keuntungan boleh diagihkan</td>
                <td><input type="text" class="form-control input-sm  text-right currency-format" id="distributable-prof" name="distributable-prof" value="0.00" readonly></td>
            </tr>
            <tr>
                <td>Keuntungan sehektar</td>
                <td><input type="text" class="form-control input-sm  text-right currency-format" id="prof-perhect" name="prof-perhect" value="0.00" required></td>
            </tr>
            <tr>
                <th>Cadangan Agihan (Jan - Mac) Sehektar</th>
                <th><input type="text" class="form-control input-sm  text-right currency-format" id="suggested" name="suggested" value="0.00" required></th>
            </tr>

        </tbody>

    </table>
</form>