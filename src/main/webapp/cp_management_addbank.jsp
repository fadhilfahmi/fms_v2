<%-- 
    Document   : cp_management_addbank
    Created on : Nov 1, 2016, 2:46:51 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.setup.company.CeManage"%>
<%@page import="com.lcsb.fms.dao.setup.company.ManagementCpDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    CeManage v = (CeManage) ManagementCpDAO.getManagement(log,request.getParameter("referno"));
    Module mod = (Module) ManagementCpDAO.getModule();

%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/formValidation.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        /*$('#saveform').formValidation({
         message: 'This value is not valid',
         icon: {
         valid: 'glyphicon glyphicon-ok',
         invalid: 'glyphicon glyphicon-remove',
         validating: 'fa fa-refresh'
         },
         fields: {
         
         code: {
         required:true,
         validators: {
         notEmpty: {
         message: 'The Account Code is required'
         },
         digits: {
         message: 'The value can contain only digits'
         }
         }
         }
         }
         });*/


        $('.getbank').click(function (e) {

            var a = $(this).attr('id');
            var b = $(this).attr('id1');
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Taxation',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_branch.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });


    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="editlist" type="<%= v.getCode()%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addbankprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>

            <div class="well">

                <form data-toggle="validator" role="form" id="saveform">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Bank Info &nbsp&nbsp<span id="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Auto Generated (Code)" autocomplete="off" readonly required>
                                <p></p>
                                <input type="text" class="form-control input-sm" id="name" name="name" placeholder="Description" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Management<span id="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="managecode" name="managecode" value="<%= v.getCode()%>" autocomplete="off" readonly required>
                                <p></p>
                                <input type="text" class="form-control input-sm" id="managename" name="managename" value="<%= v.getName()%>" readonly required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Branch&nbsp&nbsp<span class="res_code"></span></label>
                                    <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="branchcode" name="branchcode">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-sm getbank" type="button" id="branchcode" id1="branchname"><i class="fa fa-cog"></i> Branch</button>
                                        </span>
                                    </div>  
                                </div>
                                <div class="form-group" id="div_code">
                                    <input type="text" class="form-control input-sm" id="branchname" name="branchname" placeholder="" autocomplete="off" > 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Account&nbsp&nbsp<span class="res_code"></span></label>
                                    <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="coacode" name="coacode">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-sm getaccount" type="button" id="coacode" id1="coadesc"><i class="fa fa-cog"></i> Account</button>
                                        </span>
                                    </div>  
                                </div>
                                <div class="form-group" id="div_code">
                                    <input type="text" class="form-control input-sm" id="coadesc" name="coadesc" placeholder="" autocomplete="off" > 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <label for="inputName" class="control-label">Fund Type</label>
                                <select class="form-control input-sm" id="ftype" name="ftype">
                                    <%= ParameterDAO.parameterList(log, "Fund Type", "")%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <label for="inputName" class="control-label">Bank Account No.</label>
                                <div class="form-group" id="div_code">
                                    <input type="text" class="form-control input-sm" id="bankaccno" name="bankaccno" placeholder="" autocomplete="off" > 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <label for="inputName" class="control-label">Major Bank/Branch</label>
                                <select class="form-control input-sm" id="major" name="major">
                                    <%= ParameterDAO.parameterList(log,"YesNo Type", "")%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <label for="inputName" class="control-label">Active?</label>
                                <select class="form-control input-sm" id="active" name="active">
                                    <%= ParameterDAO.parameterList(log,"YesNo Type", "")%>
                                </select>
                            </div>
                        </div>
                    </div>            
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Remark</label>
                                <textarea class="form-control" rows="4"  id="remarks" name="remarks"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>