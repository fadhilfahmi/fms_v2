<%-- 
    Document   : cf_coa_edit
    Created on : Mar 11, 2016, 8:58:23 AM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.Product"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ProductInfoDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
Module mod = (Module) ProductInfoDAO.getModule();
Product edit = (Product) ProductInfoDAO.getInfo(log,request.getParameter("referenceno"));

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <script src="jsfunction/jquery-1.12.0.min.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
        });
        
       </script>
    </head>
    <body>
        <div id ="maincontainer">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
              <td width="74%" class="borderbot" align="left"><span class="bigfonttitle"><%= mod.getModuleDesc() %></span>&nbsp;&nbsp;<span class="midfonttitle">Edit <%= mod.getModuleDesc() %></span></td>
		<td width="26%" class="borderbot" align="right">&nbsp;</td>
	  </tr>
  </table>
	<br>
   <div class="partition"> 
       <div class="headofpartition"></div>
       <div class="bodyofpartition">
  <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
			<td valign="middle" align="left">
				<button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                                <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="editprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
			</td>
		</tr>
  </table>
  <br>
  
  <div class="well">
  
<form data-toggle="validator" role="form" id="saveform">
    <div class="form-group" id="div_code">
        <label for="inputName" class="control-label">Product &nbsp&nbsp<span id="res_code"></span></label>
        <input type="text" class="form-control input-sm" id="code" name="code" value="<%= edit.getCode() %>" autocomplete="off" readonly required>
        <p></p>
        <input type="text" class="form-control input-sm" id="descp" name="descp" value="<%= edit.getDescp()%>" required>
    </div>
    
    <div class="form-group form-group-sm">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Account Code&nbsp&nbsp<span class="res_code"></span></label>
                <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="coa" name="coa" value="<%= edit.getCoa()%>">
                                        <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="coa" id1="coadescp"><i class="fa fa-cog"></i> Account</button>
                                        </span>
                                    </div>  
             </div>
       
            <div class="form-group" id="div_code">
                <input type="text" class="form-control input-sm" id="coadescp" name="coadescp" value="<%= edit.getCoadescp()%>" autocomplete="off" > 
             </div>
        
    </div>
    
    <div class="form-group form-group-sm">
        <label for="inputName" class="control-label">Specification</label>
        <div class="form-group" id="div_code">
                <input type="text" class="form-control input-sm" id="spesifik" name="spesifik" value="<%= edit.getSpesifik()%>" autocomplete="off" > 
             </div>
    </div>
    
    <div class="form-group form-group-sm">
        <label for="inputName" class="control-label">Major</label>
        <select class="form-control input-sm" id="major" name="major">
            <%= ParameterDAO.parameterList(log,"YesNo Type", edit.getMajor()) %>
        </select>
    </div>
    
    <div class="form-group form-group-sm">
        <label for="inputName" class="control-label">Account to Type</label>
        <select class="form-control input-sm" id="measure" name="measure">
            <%= ParameterDAO.parameterList(log,"Unit Measure",edit.getMeasure()) %>
        </select>
    </div>
   
    
</form>
</div>
   
     </div>
     </div>
      </div>
      

    </body>
</html>

