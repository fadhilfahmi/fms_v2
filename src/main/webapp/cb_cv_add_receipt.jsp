<%-- 
    Document   : cb_cv_add_receipt
    Created on : Sep 18, 2017, 12:42:09 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.financial.cashbook.CashVoucherAccount"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.CashVoucherDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucherItem"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.financial.gl.JournalVoucherDAO"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    CashVoucherAccount v = (CashVoucherAccount) CashVoucherDAO.getItem(log, request.getParameter("referno"));
    Module mod = (Module) CashVoucherDAO.getModule();
%>

<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });

 $('#tarikh').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        }).change(dateChanged)
                .on('changeDate', dateChanged);


        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getsub').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Sub Account Detail',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_sub.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

       

        $('.calculate').keyup(function (e) {
            var todeduct = $(this).val();
            var theval = $(this).attr('id');


            if (todeduct != '') {
                var tot = 0;
                if (theval == 'debit') {
                    $('#credit').val(0.0);
                }
                if (theval == 'credit') {
                    $('#debit').val(0.0);
                }

            }

        });

        $('.form-control').focusout(function (e) {
            if (($('#debit').val() == 0.00) || ($('#loccode').val() == '') || ($('#actdesc').val() == '') || ($('#remarks').val() == '')) {
                $('#savebutton').prop('disabled', true);
            } else {
                $('#savebutton').prop('disabled', false);
            }
        });


    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="editlist" type="<%= v.getRefer()%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addreceiptprocess" ><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>
            <div class="row">
                <div class="col-sm-6">
                    <div class="well">

                        <form data-toggle="validator" role="form" id="saveform">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Reference No &nbsp;&nbsp;<span id="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="refer" name="refer" value="<%= v.getRefer()%>" autocomplete="off" required readonly>   
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Date &nbsp;&nbsp;<span id="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="tarikh" name="tarikh" placeholder="Date of Journal Voucher" autocomplete="off" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">   
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Receipt No &nbsp;&nbsp;<span id="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="noreceipt" name="noreceipt" autocomplete="off" required>      
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">GST ID &nbsp;&nbsp;<span id="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="gstid" name="gstid" autocomplete="off" >   
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Issued By &nbsp;&nbsp;<span id="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="trade" name="trade" placeholder="Code" autocomplete="off" value="<%= AccountingPeriod.getCurYearByCurrentDate()%>">   
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Amount&nbsp;&nbsp;<span id="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="total" name="total" placeholder="Code" autocomplete="off" value="<%= AccountingPeriod.getCurYearByCurrentDate()%>">   
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Balance &nbsp;&nbsp;<span id="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="balance" name="balance"  autocomplete="off" value="<%= AccountingPeriod.getCurPeriodByCurrentDate()%>">   
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Remark &nbsp;&nbsp;<span id="res_code"></span></label>
                                        <textarea class="form-control" rows="3"  id="remarks" name="remarks"></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>                  
        </div>
    </div>
</div>