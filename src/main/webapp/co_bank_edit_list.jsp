<%-- 
    Document   : co_bank_edit_list
    Created on : Nov 1, 2016, 3:45:09 PM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.fms.model.setup.company.BankBranch"%>
<%@page import="com.lcsb.fms.dao.setup.company.BankDAO"%>
<%@page import="com.lcsb.fms.model.setup.company.Bank"%>
<%@page import="com.lcsb.fms.model.setup.company.CeManageBankPK"%>
<%@page import="com.lcsb.fms.model.setup.company.CeManageBank"%>
<%@page import="com.lcsb.fms.dao.setup.company.ManagementCpDAO"%>
<%@page import="com.lcsb.fms.model.setup.company.CeManage"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.PaymentVoucherDAO"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucherRefer"%>
<%@page import="com.lcsb.fms.dao.financial.ap.VendorPaymentDAO"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucher"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoice"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoiceItem"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
//PaymentVoucherItem vi = (PaymentVoucherItem) VendorPaymentDAO.getPYVitem(request.getParameter("refer"));
Bank v = (Bank) BankDAO.getInfo(log,request.getParameter("referno"));
Module mod = (Module) BankDAO.getModule();

//String status = PaymentVoucherDAO.getStatus(v.getRefer());
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        window.scrollTo(0,0);
        
        callPnotify('<%= request.getParameter("status") %>','<%= request.getParameter("text") %>');
        
        
        $('#print').click(function(e){
            printElement($("#viewjv").html());
        });
        
        $('.viewvoucher').click(function(e){
            var a = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=viewpv&refer="+a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
	});
            
        $( ".editmain" ).click(function() {
            var a = $(this).attr('id');
            var b = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=editmain&referno="+a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });
              
        $( ".edititem" ).click(function() {
            var a = $(this).attr('id');
            var b = $(this).attr('name');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process="+b+"&referno="+a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });
              
        $( ".deleteitem" ).click(function() {
            var a = $(this).attr('id');
            var b = $(this).attr('name');
            
            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are you sure to delete?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function(result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if(result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process="+b+"&referno="+a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            
            return false;
        });
              
        $( ".deletemain" ).click(function() {
            var a = $(this).attr('id');
            var b = $(this).attr('href');
                 
            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'All datas for this Official Receipt will be permanently deleted. Are you sure to proceed?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function(result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if(result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=delete&referno="+a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            return false;
        });
              
        $( "#addcredit" ).click(function(e) {
            e.preventDefault();
            var refer = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=addcredit&refer="+refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
                    
            return false;
        });
                
        $( ".add-st" ).click(function(e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=addbranch&referno="+refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });
                
        $( ".adddebit" ).click(function(e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=adddebit&refer="+refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });
                
        $( ".check" ).click(function(e) {
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=check&referno="+id,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });
            
        $('.activerowm').click(function(e){
            e.preventDefault();
            $(".togglerow").toggle();
            //$(this).toggleClass('info');
            //$("#togglerow_nd"+b).toggleClass('warning');
        });

        $('.activerowx').click(function(e){
            e.preventDefault();
            var b = $(this).attr('id');
            $("#togglerow_nd"+b).toggle();
            $(this).toggleClass('info');
            $("#togglerow_nd"+b).toggleClass('warning');
        });
                
        $('.activerowy').click(function(e){
            e.preventDefault();
            var b = $(this).attr('id');
            $("#togglerow_st"+b).toggle();
            $(this).toggleClass('info');
            $("#togglerow_st"+b).toggleClass('warning');
        });
        
        var oTable = $('#list-bank').DataTable( {
                responsive: true,
                "aaSorting": [[ 0, "desc" ]]
            });
              
    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
		</tr>
            </table>
            <br>
            <table class="table table-bordered table-striped  table-hover">
                <thead>
                    <tr>
                        <th colspan="4">Bank Information
                        <!--<small><span class="label label-primary">Preparing</span></small>-->
                            <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
                                <button id="<%= v.getCode()%>" class="btn btn-default btn-xs viewvoucher" title="<%= mod.getModuleID() %>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i></button>
                                <button id="<%= v.getCode() %>" class="btn btn-default btn-xs editmain" title="<%= mod.getModuleID() %>" name="edit_st"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                <button id="<%= v.getCode() %>"  class="btn btn-danger btn-xs deletemain" title="<%= mod.getModuleID() %>" id="<%//= pvi.getRefer() %>" name="delete_st"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                            
                                <button id="<%= v.getCode() %>" class="btn btn-default btn-xs viewvoucher" title="<%= mod.getModuleID() %>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i></button>
                                
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody class="activerowm tdrow">
                    <tr>
                        <th width="15%">Code</th>
                        <td width="35%"><%= v.getCode() %></td>
                        <th width="15%">Name</th>
                        <td width="35%"><%= v.getName()%></td>
                    </tr>
                    <tr>
                        <th>Account Code</th>
                        <td><%= v.getCoa()%></td>
                        <th>Account Desc.</th>
                        <td><%= v.getCoadescp()%></td>
                    </tr>
                    <tr>
                        <th>Abbreviation</th>
                        <td><%= v.getAbbreviation()%></td>
                        <th>Overdue</th>
                        <td><%= v.getOverdue()%></td>
                    </tr>
                    <tr>
                        <th>Remarks</th>
                        <td colspan="3"><%= v.getRemarks() %></td>
                    </tr>
                    
                </tbody>
            </table>
                    <div class="well">
            <table class="table table-bordered table-striped table-hover" id="list-bank">
                <thead>
                    <tr>
                        <th colspan="3">List of Branches<!--<small>asdada</small>-->
                            <span class="pull-right">
                                <button  class="btn btn-default btn-xs add-st" title="<%= mod.getModuleID() %>" id="<%= v.getCode()%>" name="addrefer"><i class="fa fa-plus-circle" aria-hidden="true"></i> Branch</button>
                            </span>
                        </th>
                    </tr>
                    <tr>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <%List<BankBranch> listBranch = (List<BankBranch>) BankDAO.getAllBranch(log,v.getCode());
                    
                    for (BankBranch c : listBranch) {

                    %>
                    <tr class="activerowx" id="<%= c.getBranchcode()%>">
                        <td class="tdrow">&nbsp;<%= c.getBranchcode()%></td>
                        <td class="tdrow">&nbsp;<%= c.getBranchname()%></td>
                        <td class="tdrow" align="right">&nbsp;
                            <div class="btn-group btn-group-xs" role="group" aria-label="...">
                                <button id="<%= c.getBranchcode() %>" class="btn btn-default btn-xs edititem" title="<%= mod.getModuleID() %>" name="edit_st"><i class="fa fa-pencil"></i></button>
                                <button id="<%= c.getBranchcode() %>"  class="btn btn-default btn-xs deleteitem" title="<%= mod.getModuleID() %>" name="delete_st"><i class="fa fa-trash-o"></i></button>
                            </div>
                        </td>
                    </tr>
                    
                    <%}%>
                </tbody>
            </table>
            </div>    
            
        </div>
    </div>
</div>





      

      
