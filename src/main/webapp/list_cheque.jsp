<%-- 
    Document   : list_bank
    Created on : Mar 4, 2016, 4:06:05 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
            
             $.ajax({
                        url: "list_cheque_view.jsp?bankcode=<%= request.getParameter("bankcode") %>",
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
            }});
            
           $('#keyword').keyup(function(){
                //var l = $('input[name=searchby]:checked').val();
                var keyword = $(this).val();
                
                 $.ajax({
                        url: "list_cheque_view.jsp?bankcode=<%= request.getParameter("bankcode") %>&keyword="+keyword,
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
                }});
            });
            
            
            
            $('#result').on('click', '.thisresult', function(e) {
                var b = $(this).attr('id');
                
                
                $('#<%= request.getParameter("cekcolumn") %>').val(b);
                
                e.preventDefault();
            });
            
           
            
        });
        </script>
    </head>
    <body>
         
        
        <div class="form-group">
            <div class="form-group input-group">
                <input type="text" class="form-control" id="keyword">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
        
        <div id="result">
       
        </div>
        
            
    </body>
</html>