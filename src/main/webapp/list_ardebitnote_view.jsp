<%-- 
    Document   : list_ardebitnote_view
    Created on : Nov 7, 2016, 3:23:06 PM
    Author     : fadhilfahmi
--%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArDebitNote"%>
<%@page import="com.lcsb.fms.dao.financial.ar.ArDebitNoteDAO"%>
<%@page import="com.lcsb.fms.model.financial.ap.VendorPayee"%>
<%@page import="com.lcsb.fms.dao.financial.ap.VendorPaymentDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>

<div class="list-group"><%
String keyword = request.getParameter("keyword");
List<ArDebitNote> slist = (List<ArDebitNote>) ArDebitNoteDAO.getAllRDN(log,keyword);
    if(slist.isEmpty()){%>
        <a class="list-group-item thisresult-payee">
            <div id="leftdiv>"<span class="font-small-red"><strong>No Debit Note available.</strong></span></div>
        </a>
    <%}
    for (ArDebitNote c : slist) { 
    %>
        <a href="<%= c.getBuyername()%>" id="<%= c.getNoteno()%>"   title="" class="list-group-item thisresult-debitnote">
            <div id="left_div"><%= c.getNoteno()%></div>
            <div id="right_div">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= c.getBuyername()%><span class="pull-right"><%= GeneralTerm.currencyFormat(c.getTotal()) %></span></div>
    </a>
    <%}%>
</div>