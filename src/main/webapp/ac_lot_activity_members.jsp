<%-- 
    Document   : ac_lot_activity_members
    Created on : Dec 19, 2016, 11:13:11 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.dao.management.activity.LotActivityDAO"%>
<%@page import="com.lcsb.fms.model.setup.configuration.LotMemberLot"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.LotMemberDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        var oTable = $('#list').DataTable({
            responsive: true,
            "bPaginate": false,
            "aaSorting": [[2, "asc"]],
            "dom": '<"toolbar">frtip',
        });

        $("div.toolbar").html('<button id="checked" class="btn btn-default btn-xs pull-left select-button">Deselect All</button>');
        //$("div.toolbar").append('Total Member');

        $('.select-button').click(function (e) {
            var checkboxes = $(this).closest('form').find(':checkbox');
            var bt = $(this).html();
            console.log(bt);
            if (bt == 'Deselect All') {
                $(this).html('Select All');
                checkboxes.prop('checked', false);
                $('.step2-Btn').prop('disabled', true);
            } else if (bt == 'Select All') {
                $(this).html('Deselect All');
                checkboxes.prop('checked', true);
                $('.step2-Btn').prop('disabled', false);
            }
            e.preventDefault();
        });

        $('#list tbody').on('click', 'tr', function (e) {
            var data = oTable.row(this).data();

            $('#selected-contract').val(data[1]);
            $('#inputprice').val(data[5]);
            //alert(data[0]);
            //e.preventDefault();

        });

        $('.chcbox-contract').click(function (e) {
            var i = 0;
            $.each($("input[name='checkboxmemno']:checked"), function () {
                i++;
                

            });
            
            if(i == 0){
                $('.step2-Btn').prop('disabled', true);
            }else{
                $('.step2-Btn').prop('disabled', false);
            }
        });


    });
</script>
<input type="hidden" id="selected-contract">
<input type="hidden" name="inputprice" id="inputprice" value="">
<form>
    <table class="table table-bordered table-striped table-hover" id="list">
        <thead>

            <tr>
                <th></th>
                <th>Member Code</th>
                <th>Member Name</th>
                <th>Acre</th>
                <th>Hectar</th>
                <th>Lot No</th>

            </tr>
        </thead>
        <tbody>
            <%List<LotMemberLot> listx = (List<LotMemberLot>) LotMemberDAO.getAllMemberByLot(request.getParameter("lotcode"));
                int i = 0;
                for (LotMemberLot j : listx) {
                    i++;
            %>
            <tr  id="<%= j.getId()%>">

                <td class="tdrow" align="right"><input type="checkbox" id="chkbox<%//= j.getDispatchNo()%>" name="checkboxmemno" class="chcbox-contract pull-left" value="<%= j.getMemCode()%>" checked required></td>
                <td class="tdrow"><%= j.getMemCode()%></td>
                <td class="tdrow"><%= j.getMemName()%></td>
                <td class="tdrow">&nbsp;<%= j.getAcre()%></td>
                <td class="tdrow">&nbsp;<%= j.getHectarage()%></td>
                <td class="tdrow">&nbsp;<%= j.getNopetak()%></td>
            </tr>
            <%}%>
        </tbody>

    </table>
</form>