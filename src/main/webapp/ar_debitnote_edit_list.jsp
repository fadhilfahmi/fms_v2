<%-- 
    Document   : ar_debitnote_edit_list
    Created on : Nov 7, 2016, 10:53:30 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.financial.gl.PostDAO"%>
<%@page import="com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArDebitNoteItem"%>
<%@page import="com.lcsb.fms.dao.financial.ar.ArDebitNoteDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArDebitNote"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.PaymentVoucherDAO"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucherRefer"%>
<%@page import="com.lcsb.fms.dao.financial.ap.VendorPaymentDAO"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucher"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoice"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoiceItem"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
//PaymentVoucherItem vi = (PaymentVoucherItem) VendorPaymentDAO.getPYVitem(request.getParameter("refer"));
ArDebitNote v = (ArDebitNote) ArDebitNoteDAO.getMain(log,request.getParameter("refer"));
Module mod = (Module) ArDebitNoteDAO.getModule();

String status = ArDebitNoteDAO.getStatus(log,v.getNoteno());
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        window.scrollTo(0,0);
        
        callPnotify('<%= request.getParameter("status") %>','<%= request.getParameter("text") %>');
        
        
        $('#print').click(function(e){
            printElement($("#viewjv").html());
        });
     
        $( "#addcredit" ).click(function(e) {
            e.preventDefault();
            var refer = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=addcredit&refer="+refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
                    
            return false;
        });
                
        $( ".additem" ).click(function(e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=additem&refer="+refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });
                
        $( ".adddebit" ).click(function(e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=adddebit&refer="+refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });
                
      
            
    
    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
		</tr>
            </table>
            <br>
            <%
                String str = "";
                if (!DebitCreditNoteDAO.hasDebitCreditNote(log, v.getNoteno()) && ArDebitNoteDAO.hasDCNote(log, v.getNoteno())) {
                    str = "Credit Note will be generated, you can create Credit Note by approving this " + mod.getModuleDesc() + " or  <strong><a title=\"" + v.getNoteno()+ "\" id=\"createnote\" class=\"thelink\">create Credit Note now. </a></strong>";

                } else if (ArDebitNoteDAO.hasDCNote(log, v.getNoteno())) {
                    str = "Credit Note has been generated,  <strong>"+ DebitCreditNoteDAO.getDebitCreditNoteLink(log, v.getNoteno(), mod.getModuleID()) +"</strong>";
                }
                if (ArDebitNoteDAO.isComplete(log, v.getNoteno()) && !PostDAO.checkReadyToPost(log, v.getNoteno(), "DNR") && !status.equals("Approved")) {
            %>
            <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp; This <%= mod.getModuleDesc()%> is ready. <%= str%>
            </div>
            <%
            } else if (!status.equals("Approved")) {%>
            <div class="alert alert-warning alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;&nbsp; <%= mod.getModuleDesc()%> is not complete yet.
            </div>

            <%}
            %>
            <table class="table table-bordered table-striped  table-hover">
                <thead>
                    <tr>
                        <th colspan="4">Voucher Information
                        <!--<small><span class="label label-primary">Preparing</span></small>-->
                            <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
                            
                            <%if(!status.equals("Approved")){
                                if((ArDebitNoteDAO.isCheck(log,v.getNoteno()) && log.getListUserAccess().get(2).getAccess())){%>
                                
                                <button  class="btn btn-default btn-xs approve" title="<%= mod.getModuleID() %>" id="<%= v.getNoteno() %>"><i class="fa fa-check-square-o" aria-hidden="true"></i> Approve</button>
                                
                                <%}else if(!ArDebitNoteDAO.isApprove(log,v.getNoteno()) && log.getListUserAccess().get(1).getAccess()){%> 
                                
                                <button  class="btn btn-default btn-xs check" title="<%= mod.getModuleID() %>" id="<%= v.getNoteno() %>" name="addrefer"><i class="fa fa-check-square-o" aria-hidden="true"></i> Check</button>
                                
                                <%}%>
                                
                                <button id="<%= v.getNoteno() %>" class="btn btn-default btn-xs viewvoucher" title="<%= mod.getModuleID() %>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i></button>
                                <button id="<%= v.getNoteno() %>" class="btn btn-default btn-xs editmain" title="<%= mod.getModuleID() %>" name="edit_st"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                <button id="<%= v.getNoteno() %>"  class="btn btn-danger btn-xs deletemain" title="<%= mod.getModuleID() %>" name="delete_st"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                            <%}else{%>
                                <button id="<%= v.getNoteno() %>" class="btn btn-default btn-xs viewvoucher" title="<%= mod.getModuleID() %>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i></button>
                            <%}%>    
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody class="activerowm tdrow">
                    <tr>
                        <th width="25%">Status</th>
                        <td width="25%"><%= GeneralTerm.getStatusLabel(ArDebitNoteDAO.getStatus(log,v.getNoteno())) %></td>
                        <th width="25%">Buyer</th>
                        <td width="25%"><%= v.getBuyercode()%> - <%= v.getBuyername()%></td>
                    </tr>
                    <tr>
                        <th>Period</th>
                        <td><%= v.getYear() %>, <%= v.getPeriod() %></td>
                        <th>Bank</th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Note No</th>
                        <td><%= v.getNoteno()%></td>
                        <th>Amount</th>
                        <td>RM<%= GeneralTerm.currencyFormat(v.getTotal()) %></td>
                    </tr>
                    <tr>
                        <th>Note Date</th>
                        <td><%= v.getNotedate()%></td>
                        <th>Remark</th>
                        <td><%= v.getRemark() %></td>
                    </tr>
                    <tr class="togglerow" style="display: none">
                        <th>Paid Address</th>
                        <td></td>
                        <th>Paid Postcode</th>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th colspan="5">Detail Information<!--<small>asdada</small>-->
                            <span class="pull-right">
                                <%if(!status.equals("Approved")){%>
                                <button  class="btn btn-default btn-xs additem" title="<%= mod.getModuleID() %>" id="<%= v.getNoteno()%>" name="addrefer"><i class="fa fa-plus-circle" aria-hidden="true"></i> Detail</button>
                                <%}%>
                            </span>
                        </th>
                    </tr>
                    <tr>
                        <th>Refer. No</th>
                        <th>Account</th>
                        <th>Remark</th>
                        <th>Amount</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <%List<ArDebitNoteItem> listRefer = (List<ArDebitNoteItem>) ArDebitNoteDAO.getAllItem(log,v.getNoteno());
                    if(listRefer.isEmpty()){%>
                    <tr>
                        <td colspan="5">
                            <span class="font-small-red">No data available.</span>
                        </td>
                    </tr>
                    <%}
                    for (ArDebitNoteItem c : listRefer) {%>
                    <tr class="activerowx" id="<%= c.getRefer()%>">
                        <td class="tdrow">&nbsp;<%= c.getRefer()%></td>
                        <td class="tdrow">&nbsp;<%= c.getCoacode()%> - <%= c.getCoaname()%></td>
                        <td class="tdrow">&nbsp;<%= c.getRemarks()%></td>
                        <td class="tdrow" align="right">&nbsp;<%= GeneralTerm.currencyFormat(c.getAmount()) %></td>
                        <td class="tdrow" align="right">&nbsp;
                            <%if(!status.equals("Approved")){%>
                            <div class="btn-group btn-group-xs" role="group" aria-label="...">
                                <button id="<%= c.getRefer() %>" class="btn btn-default btn-xs edititem" title="<%= mod.getModuleID() %>" name="edititem"><i class="fa fa-pencil"></i></button>
                                <button id="<%= c.getRefer() %>"  class="btn btn-default btn-xs deleteitem" title="<%= mod.getModuleID() %>" name="deleteitem"><i class="fa fa-trash-o"></i></button>
                            </div>
                            <%}%>
                        </td>
                    </tr>
                    <tr id="togglerow_nd<%= c.getRefer() %>" style="display: none">
                        <td colspan="5">
                            <table class="table table-bordered">
                                <thead></thead>
                                <tbody>
                                    <tr>
                                        <th>To Date Paid</th>
                                        <td><%//= c.getPaid() %></td>
                                        <th>B/F Amount</th>
                                        <td><%//= c.getBf() %></td>
                                    </tr>
                                    <tr>
                                        <th>To Paid</th>
                                        <td><%//= c.getPayment() %></td>
                                        <th>C/F Amount</th>
                                        <td><%//= c.getCf() %></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <%}%>
                </tbody>
            </table>
                
           
        </div>
    </div>
</div>





      

      
