<%-- 
    Document   : list_bank_view
    Created on : Mar 4, 2016, 4:06:59 PM
    Author     : Dell
--%>


<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ListVendorDAO"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Vendor"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<div class="list-group"><%
            
            String keyword = request.getParameter("keyword");
            List<Vendor> slist = (List<Vendor>) ListVendorDAO.getAllVendor(log, keyword);
            for (Vendor c : slist) { 
               %>
       
            <a href="<%= c.getVendorName()%>" id="<%= c.getCode()%>" registerno="<%= c.getRegisterNo() %>" bumiputra="<%= c.getBumiputra() %>" ownername="<%= c.getPerson() %>" address="<%= c.getVendorAddress() %>" city="<%= c.getCity() %>" postcode="<%= c.getPostcode() %>" state="<%= c.getState()%>" title="<%= c.getTitle() %>" position="<%= c.getPosition() %>" hp="<%= c.getHp() %>" phone="<%= c.getPhone() %>" fax="<%= c.getFax() %>" email="<%= c.getEmail() %>" bankname="<%= c.getBank() %>" bankaccount="<%= c.getBankaccount() %>" payment="<%= c.getPayment() %>" remarks="<%= c.getRemarks() %>" class="list-group-item thisresult">
                <div id="left_div"><%= c.getCode()%></div>
                <div id="right_div"><%= c.getVendorName()%></div>
            </a>

               <%    
            } 
        %>
</div>