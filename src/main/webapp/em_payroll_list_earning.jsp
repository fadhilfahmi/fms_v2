<%-- 
    Document   : em_payroll_list_earning
    Created on : Jun 9, 2017, 9:26:40 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.financial.employee.EmPayrollType"%>
<%@page import="com.lcsb.fms.dao.financial.employee.EmPayrollDAO"%>
<%@page import="com.lcsb.fms.dao.setup.company.StaffDAO"%>
<%@page import="com.lcsb.fms.model.setup.company.staff.Staff"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoiceItem"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Earning"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.EarningDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    if (request.getParameter("flag").equals("add")) {

        Earning e = (Earning) EarningDAO.getInfo(log, request.getParameter("code"));
        EmPayrollDAO.saveType(log, request.getParameter("refer"), request.getParameter("code"), "Earning");

%>
<div class="panel-heading" role="tab" id="heading<%= e.getCode()%>">
    <a class="collapsed accordionfont" title="Earning" role="button" id="<%= e.getCode()%>" data-toggle="collapse" data-parent="#accordion" href="#collapse<%= e.getCode()%>" aria-expanded="false" aria-controls="collapse<%= e.getCode()%>">
        <span style="font-weight:700"><%= e.getCode()%> - <%= e.getDescp()%></span>
    </a>
    &nbsp;&nbsp;&nbsp;&nbsp;<span id="loading-spin-earning-<%= e.getCode() %>"></span>
    <span class="pull-right">

        <a class="icon-delete"><i class="fa fa-trash-o deletethis" title="earning" aria-hidden="true" id="<%= e.getCode()%>" type="<%= request.getParameter("refer")%>"></i></a>
    </span>
</div>
<div id="collapse-earning-<%= e.getCode()%>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<%= e.getCode()%>">
    <div class="panel-body panel-earning-list-all" id="panel-body-content-list-earning-<%= e.getCode()%>"></div>
</div>
<%
} else {

    List<EmPayrollType> listEarn = (List<EmPayrollType>) EmPayrollDAO.getSavedType(log, request.getParameter("refer"), "Earning");

    if (listEarn.isEmpty()) {%>

<span class="font-small-red" id="earn-not"><strong>No Earning Data available.</strong></span>

<%}
    for (EmPayrollType i : listEarn) {

%>
<div class="panel panel-default" id="panel-earning-<%= i.getEarncode()%>">
    <div class="panel-heading" role="tab" id="heading<%= i.getEarncode()%>">
        <a class="collapsed accordionfont" title="Earning" role="button" id="<%= i.getEarncode()%>" data-toggle="collapse" data-parent="#accordion" href="#collapse<%= i.getEarncode()%>" aria-expanded="false" aria-controls="collapse<%= i.getEarncode()%>">
            <span style="font-weight:700"><%= i.getEarncode()%> - <%= i.getEarndesc()%></span>
        </a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="loading-spin-earning-<%= i.getEarncode()%>"></span>
        <span class="pull-right">

            <a class="icon-delete"><i class="fa fa-trash-o deletethis" title="earning" aria-hidden="true" id="<%= i.getEarncode()%>" type="<%= request.getParameter("refer")%>"></i></a>
        </span>
    </div>
    <div id="collapse-earning-<%= i.getEarncode()%>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<%= i.getEarncode()%>">
        <div class="panel-body panel-earning-list-all" id="panel-body-content-list-earning-<%= i.getEarncode()%>"></div>
    </div>
</div>
<%}
    }%>
