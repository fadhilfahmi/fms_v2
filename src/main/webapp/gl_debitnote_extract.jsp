<%-- 
    Document   : gl_close_period_view
    Created on : Mar 15, 2016, 4:29:21 PM
    Author     : Dell
--%>


<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.model.financial.gl.AccountPeriod"%>
<%@page import="com.lcsb.fms.dao.financial.gl.AccountPeriodDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
AccountPeriod cur = (AccountPeriod) AccountPeriodDAO.getCurPeriod(log);
AccountPeriod pre = (AccountPeriod) AccountPeriodDAO.getPreviousPeriod(log);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <title>JSP Page</title>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
            
            $('#keyword').keyup(function(){
                var l = $('input[name=searchby]:checked').val();
                var m = $('input[name=method]:checked').val();
                var keyword = $(this).val();
                console.log(keyword);
                 $.ajax({
                        url: "list_estatemill.jsp?keyword="+keyword,
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
                }});
            });
            
            $('#result').on('click', '.thisresult', function(e) { 
                var a = $(this).attr('href');
                var b = $(this).attr('id');
                //$('#<%= request.getParameter("code") %>').val(a);
                //$('#<%= request.getParameter("descp") %>').val(b);
                e.preventDefault();
            });
            
        });
        </script>
    </head>
    <body>
        <div class="row">
            <div class="col-sm-6">
                <div class="well">
  
                <form data-toggle="validator" role="form" id="saveform">
                    <div class="row self-row">
                        <label for="inputName" class="control-label">Filter Data &nbsp&nbsp<span id="res_code"></span></label>
                    </div>
                    

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <label for="inputName" class="control-label">Year</label>
                                <select class="form-control input-sm" id="year" name="year">
                                    <%= ParameterDAO.parameterList(log,"Year",AccountingPeriod.getCurYear(log)) %>
                                </select>
                            </div>  
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group form-group-sm">
                                <label for="inputName" class="control-label">Period</label>
                                <select class="form-control input-sm" id="period" name="period">
                                    <%= ParameterDAO.parameterList(log,"Period",AccountingPeriod.getCurPeriod(log)) %>
                                </select>
                            </div>    
                        </div>
                    </div>
                    

                   


                </form>
                        </div></div>
            <div class="col-sm-6">
                <div class="well">
  
                <form data-toggle="validator" role="form" id="saveform">
                    <div class="row self-row">
                        <label for="inputName" class="control-label">Mill/Estate List &nbsp&nbsp<span id="res_code"></span></label>
                    </div>
                    
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="form-group input-group">
                                    <input type="text" class="form-control" id="keyword" autocomplete="off">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>

                            <div id="result">

                            </div>
                        </div>
                    </div>
                    

                </form>
                        </div></div>
        </div>
            
            
    </body>
</html>
