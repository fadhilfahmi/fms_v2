<%-- 
    Document   : co_agree_view
    Created on : Jun 8, 2016, 9:25:55 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.model.management.contract.Agreement"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.model.management.contract.AgreementJob"%>
<%@page import="com.lcsb.fms.dao.management.contract.AgreementDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
AgreementJob vi = (AgreementJob) AgreementDAO.getCAGjob(log,request.getParameter("refer"));
Agreement v = (Agreement) AgreementDAO.getCAG(log,request.getParameter("refer"));
Module mod = (Module) AgreementDAO.getModule();

String checkLink = "";
    String appLink = "";
    
    if(v.getSaksiid().equals("")){
        checkLink = "<a href=\"#\" class=\"action_2\" id=\"check\"><i class=\"fa fa-check\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Check</a>";
        
        appLink = "<a href=\"#\"  id=\"approve\"><i class=\"fa fa-thumbs-o-up\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Approve</a>";
    }else{
        checkLink = "<a href=\"#\" id=\"check\"><i class=\"fa fa-check\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Check</a>";
        appLink = "<a href=\"#\" class=\"action_2\" id=\"approve\"><i class=\"fa fa-thumbs-o-up\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Approve</a>";
    }
    
    if(!v.getSaksiid().equals("") && !v.getPengurusid().equals("")){
        checkLink = "<a href=\"#\"  id=\"check\"><i class=\"fa fa-check\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Check</a>";
        
        appLink = "<a href=\"#\"  id=\"approve\"><i class=\"fa fa-thumbs-o-up\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Approve</a>";
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <title>JSP Page</title>
        <style>
            .invoice-title h2, .invoice-title h3 {
                display: inline-block;
            }

            .table > tbody > tr > .no-line {
                border-top: none;
            }

            .table > thead > tr > .no-line {
                border-bottom: none;
            }

            .table > tbody > tr > .thick-line {
                border-top: 2px solid;
            }
        </style>
    </head>
    <body>
        <div id ="maincontainer">
                
   <div class="partition"> 
       <div class="headofpartition"></div>
       <div class="bodyofpartition">
           
           <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
                    <td valign="middle" align="left">
			<div class="btn-group btn-group-sm" role="group" aria-label="...">
                                
                                
                                <div class="btn-group" role="group" aria-label="...">
                                    <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                                    

                                    <div class="btn-group" role="group">
                                      <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i>&nbsp;
                                        Option
                                        <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu">
                                        <li class="dropdown-font"><a href="#" class="action" id="editinv"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit</a></li>
                                        <li class="dropdown-font"><a href="#" class="action" id="print"><i class="fa fa-print"></i>&nbsp;&nbsp;Print</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li class="dropdown-font"><%= checkLink %></li>
                                        <li class="dropdown-font"><%= appLink %></li>
                                      </ul>
                                    </div>
                                    
                                  </div>
                              </div>
                        
		</td>
		</tr>
  </table>
  <br>
       
            <div class="row">
                <div class="col-md-12">
                        <div class="panel-heading">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab1default" data-toggle="tab">Bill of Quantities</a></li>
                                    <li><a href="#tab2default" data-toggle="tab">View Contract</a></li>
                                    <li><a href="#tab3default" data-toggle="tab">Rationalized Price</a></li>
                                    <!--<li class="dropdown">
                                        <a href="#" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#tab4default" data-toggle="tab">Default 4</a></li>
                                            <li><a href="#tab5default" data-toggle="tab">Default 5</a></li>
                                        </ul>
                                    </li>-->
                                </ul>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab1default">
                                
                                    
                                <!--begin tab1default - Bill of Quantities-->    
                                <div class="row">
                                        <div class="col-md-12">
                                                <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                                <h3 class="panel-title"><strong>Item summary</strong></h3>
                                                        </div>
                                                        <div class="panel-body">
                                                                <div class="table-responsive">
                                                                        <table class="table table-condensed">
                                                                                <thead>
                                                                <tr>
                                                                                    <td><strong>Item</strong></td>
                                                                                    <td><strong>Description</strong></td>
                                                                                    <td class="text-center"><strong>Unit</strong></td>
                                                                                    <td class="text-center"><strong>Qty</strong></td>
                                                                                    <td class="text-right"><strong>Rate(RM)</strong></td>
                                                                                    <td class="text-right"><strong>Amount(RM)</strong></td>
                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                        <%
                                                                                        List<AgreementJob> listAll = (List<AgreementJob>) AgreementDAO.getAllCAGjob(log,v.getRefer());
                                                                                          for (AgreementJob j : listAll) {
                                                                                        %>
                                                                                        <tr>
                                                                                                <td><%= j.getJobcode() %></td>
                                                                                                <td><%= j.getJobdescp()%></td>
                                                                                                <td class="text-center"><%= j.getUnitmeasure()%></td>
                                                                                                <td class="text-center"><%= j.getUnit()%></td>
                                                                                                <td class="text-right"><%= GeneralTerm.currencyFormat(Double.parseDouble(j.getPriceunit()))%></td>
                                                                                                <td class="text-right"><%= GeneralTerm.currencyFormat(j.getPricework())%></td>
                                                                                        </tr>
                                                                                        <%
                                                                                            }
                                                                                        %>
                                                                                        <tr>
                                                                                                <td class="no-line"></td>
                                                                                                <td class="no-line"></td>
                                                                                                <td class="no-line text-center">&nbsp;</td>
                                                                                                <td class="no-line text-center">&nbsp;</td>
                                                                                                <td class="no-line text-right"><strong>Total</strong></td>
                                                                                                <td class="no-line text-right"><%= GeneralTerm.currencyFormat(AgreementDAO.getSum(log,"pricework", v.getRefer())) %></td>
                                                                                        </tr>
                                                               
                                                                                </tbody>
                                                                        </table>
                                                                </div>
                                                        </div>
                                                </div>
                                        </div>
                                    </div>
                                <!--end tab2default - Bill of Quantities--> 
                                
                                    
                                </div>
                                <div class="tab-pane fade" id="tab2default">
                                    
                                    <!-- agreement -->
                                    
                                    <form name="form1" method="post" action="ac_machineryadd.jsp">

    
  <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  
    <tr> 
      <td width="11%" rowspan="7"><div align="center"><font size="1" face="Geneva, Arial, Helvetica, sans-serif"><img src="<%//=CompanyProfile.getCompanyLogo()%>" width="66" height="72"></font></div></td>
      <td height="29"><div align="left"><font size="4" face="Times New Roman, Times, serif"></font>        <font size="4" face="Times New Roman, Times, serif"><strong><%//=CompanyProfile.getCompanyName()%></strong></font></div></td>
      <td height="29">&nbsp;</td>
      <td width="4%" rowspan="7">&nbsp;</td>
    </tr>
    <tr> 
      <td><div align="left"><strong></strong>        <strong><font size="1" face="Times New Roman, Times, serif"><%//=estatename1%></font></strong></div></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td><div align="left"><strong><font size="1" face="Times New Roman, Times, serif"><%//=estateaddress%></font></strong></div></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td><div align="left"><strong><font size="1" face="Times New Roman, Times, serif"><%///=estatepostcode%> <%//=estatecity%>, <%//=estatestate%></font></strong></div></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td height="19"><div align="left"><font size="1" face="Times New Roman, Times, serif"><strong>Tel 
      : <%//=estatephone%> Fax : <%//=estatefax%></strong></font></div></td>
      <td width="23%"><font size="1" face="Times New Roman, Times, serif"><strong><em> 
        &nbsp;&nbsp;&nbsp;&nbsp;No. kontrak. : <%//=ref%>/<%//=hnoagree%> </em></strong></font></td>
    </tr>
    <tr> 
      <td height="19"><font face="Times New Roman, Times, serif"><strong><em><font size="2">MEMORANDUM 
        PERJANJIAN</font> </em></strong></font></td>
      <td>&nbsp;</td>
    </tr>
  </table>
    
  <table width="90%" border="0" align="center" cellpadding="1" cellspacing="1">
    <tr>
      <td>&nbsp;</td>
      <td width="98%">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2"><font size="1" face="Times New Roman, Times, serif"><em><strong>SATU 
        PERJANJIAN telah dibuat pada</strong></em><strong> <%//=data3[0]%></strong><em><strong> di antara <%//=CompanyProfile.getCompanyName()%> 
        di alamat </strong></em><strong><font size="1" face="Times New Roman, Times, serif"><%//=estateaddress%>&nbsp;<%//=estatepostcode%>&nbsp; <%//=estatecity%>, <%//=estatestate%></font></strong><em><strong><font size="1" face="Times New Roman, Times, serif"> 
        .(Kemudian daripada ini disebut &quot;LADANG&quot;) sebagai satu pihak 
            dan </font></strong></em><strong><font size="1" face="Times New Roman, Times, serif"><%//=namapemborong%> </font></strong><em><strong><font size="1" face="Times New Roman, Times, serif">No. 
              K/P</font></strong></em><strong><font size="1" face="Times New Roman, Times, serif"> 
              <%//=ic%></font></strong><em><strong><font size="1" face="Times New Roman, Times, serif"> 
              yang beralamat di</font></strong></em><strong><font size="1" face="Times New Roman, Times, serif"> 
              <%//=address%></font></strong><em><strong><font size="1" face="Times New Roman, Times, serif">&nbsp;. 
                      (Kemudian daripada ini disebut PEMBORONG) sebagai satu pihak lagi dan 
                      berlaku sehingga ke</font></strong></em><strong><font size="1" face="Times New Roman, Times, serif"> 
                      <%//=data3[1]%></font></strong><em><strong><font size="1" face="Times New Roman, Times, serif">.</font></strong></em></font><br> 
        <strong><em><font size="1" face="Times New Roman, Times, serif">PERJANJIAN 
      INI adalah seperti berikut :-</font></em></strong><strong><font size="1" face="Times New Roman, Times, serif"><em>&nbsp;&nbsp;</em></font></strong></td>
    </tr>
    <tr> 
      <td width="2%"><font size="2"><strong>1.</strong></font></td>
      <td><strong><font size="1" face="Times New Roman, Times, serif"><em>Ladang 
        dengan ini menawarkan kerja di <%//=estatename1%> kepada PEMBORONG dan PEMBORONG 
        bersetuju untuk melaksanakan kerja-kerja yang ditawarkan oleh LADANG seperti 
        butir-butir berikut :-</em></font></strong></td>
    </tr>
    <tr> 
      <td height="98" colspan="2">
	  <table width="100%"  border="1" align="center"cellpadding="0" cellspacing="0" style="font-size:7px;border-collapse: collapse">
          <TR bgcolor="#FFFFFF"> 
            <TD width="20%" height="21"><div align="center"><em><font size="1" face="Times New Roman, Times, serif"><b>JOB 
                </b></font></em></div></TD>
            <TD width="18%"><div align="center"><em><font size="1" face="Times New Roman, Times, serif"><b>LOCATION 
                </b></font></em></div></TD>
            <TD width="8%"><div align="center"><em><font size="1" face="Times New Roman, Times, serif"><b>OUTPUT 
                </b></font></em></div></TD>
            <TD width="8%"><div align="center"><em><strong><font size="1" face="Times New Roman, Times, serif"> 
                <strong><em><strong><font size="1" face="Times New Roman, Times, serif">UNIT 
                OF JOB</font></strong></em></strong></font></strong></em></div></TD>
            <TD width="8%"><div align="center"><em><strong><font size="1" face="Times New Roman, Times, serif"><strong><em>UNIT 
                MEASURE</em></strong></font></strong></em></div></TD>
            <TD width="8%"><div align="center"><em><strong><font size="1" face="Times New Roman, Times, serif"><strong><em>GST RATE</em></strong></font></strong></em></div></TD>
            <TD width="8%"><div align="center"><em><strong><font size="1" face="Times New Roman, Times, serif"><strong><em>GST AMOUNT</em></strong></font></strong></em></div></TD>
            <TD width="8%"><div align="center"><em><strong><font size="1" face="Times New Roman, Times, serif">UNIT 
                PRICE OF JOB</font></strong></em></div></TD>
            <TD width="8%"><div align="center"><em><font size="1" face="Times New Roman, Times, serif"><strong>TOTAL 
                PRICE OF JOB</strong></font></em></div></TD>
          </TR>
    <%
    List<AgreementJob> listJob = (List<AgreementJob>) AgreementDAO.getAllCAGjob(log,v.getRefer());
                                                                                          for (AgreementJob j : listJob) {
    %>      
          <tr> 
            <td height="21"><strong><font size="1" face="Times New Roman, Times, serif">&nbsp;<%= j.getJobdescp() %>&nbsp;-<%= j.getRemark() %></font></strong></td>
            <td><strong><font size="1" face="Times New Roman, Times, serif"><%= j.getLocdesc() %>&nbsp;</font></strong></td>
            <td><div align="center"><strong><font size="1" face="Times New Roman, Times, serif"><%= j.getOutputdesc() %>&nbsp;</font></strong></div></td>
            <td><div align="center"><strong><font size="1" face="Times New Roman, Times, serif"><%= j.getUnit() %>&nbsp;</font></strong></div></td>
            <td><div align="center"><strong><font size="1" face="Times New Roman, Times, serif"><%= j.getUnitmeasure() %>&nbsp;</font></strong></div></td>
            <td><div align="center"><strong><font size="1" face="Times New Roman, Times, serif"><%= j.getTaxrate() %>&nbsp;</font></strong></div></td>
            <td><div align="center"><strong><font size="1" face="Times New Roman, Times, serif"><%= j.getTaxamt() %>&nbsp;</font></strong></div></td>
            <td><div align="right"><strong><font size="1" face="Times New Roman, Times, serif"><%= j.getPriceunit() %>&nbsp; </font></strong></div></td>
            <td><div align="right"><strong><font size="1" face="Times New Roman, Times, serif"><%= j.getPricework() %>&nbsp;</font></strong></div></td>
          </tr>
    <%
    }          
    %>
          <tr> 
            <td height="21" colspan="7">&nbsp;</td>
            
            <td><div align="center"><strong><font size="1" face="Times New Roman, Times, serif">TOTAL</font></strong></div></td>
            <td><div align="right"><strong><font size="1" face="Times New Roman, Times, serif"><%= GeneralTerm.currencyFormat(AgreementDAO.getTotalJob(log,v.getRefer())) %></font></strong></div></td>
          </tr>
          <tr>
            <td height="21" colspan="7">&nbsp;</td>
            <td><div align="center"><strong><font size="1" face="Times New Roman, Times, serif">TOTAL GST AMOUNT</font></strong></div></td>
            <td><div align="right"><strong><font size="1" face="Times New Roman, Times, serif"><%= GeneralTerm.currencyFormat(AgreementDAO.getTotalGST(log,v.getRefer())) %></font></strong></div></td>
          </tr>
          <tr>
            <td height="21" colspan="7">&nbsp;</td>
            <td><div align="center"><strong><font size="1" face="Times New Roman, Times, serif">GRAND TOTAL AFTER GST</font></strong></div></td>
            <td><div align="right"><strong><font size="1" face="Times New Roman, Times, serif"><%= GeneralTerm.currencyFormat(AgreementDAO.getGrandTotal(log,v.getRefer())) %></font></strong></div></td>
          </tr>
        </table>
        <strong></strong> </td>
    </tr>
    <tr> 
      <td><font size="2"><strong>2.</strong></font></td>
      <td><div align="left"><font size="1" face="Times New Roman, Times, serif"><em><strong>Syarat-syarat 
          lain dan perincian kerja yang terkandung di dalam perjanjian ini adalah 
          seperti dilampirkan dan menjadi sebahagian daripada perjanjian ini. 
          </strong></em></font></div></td>
    </tr>
    <tr> 
      <td><font size="2"><strong>3.</strong></font></td>
      <td><div align="left"><strong><em><font size="1" face="Times New Roman, Times, serif">PEMBORONG 
          dengan ini bersetuju menyediakan sumber-sumber yang diperlukan seperti 
          tenaga kerja, peralatan dan pengangkutan seperti lain-lain perkara yang 
          mencukupi untuk melaksanakan dan menyiapkan kerja-kerja ini dengan&nbsp;sempurna 
          dan memuaskan LADANG.</font></em></strong></div></td>
    </tr>
    <tr> 
      <td><font size="2"><strong>4.</strong></font></td>
      <td><div align="left"><em><strong><font size="1" face="Times New Roman, Times, serif">Kerja-kerja 
          ini hendaklah dilaksanakan mulai</font></strong></em><strong><font size="1" face="Times New Roman, Times, serif"> 
          <strong><%//=data3[0]%></strong></font></strong><em><strong><font size="1" face="Times New Roman, Times, serif"><em><strong> 
          dan disiapkan dengan sempurna pada</strong></em></font></strong></em><strong><font size="1" face="Times New Roman, Times, serif"><strong> 
          <%//=data3[1]%></strong></font></strong><em><strong><font size="1" face="Times New Roman, Times, serif"><em><strong> 
          . LADANG berhak untuk mengenakan denda/penalti sehingga RM150.00 sehari 
          (Ringgit Malaysia Seratus Lima Puluh Sahaja) ke atas PEMBORONG mulai 
          dari hari pertama tamat tempoh masa perlaksanaan kerja hingga hari yang 
          ketujuh jika kerja-kerja gagal disempurnakan dalam tempoh yang dipersetujui.</strong></em></font></strong></em></div></td>
    </tr>
    <tr> 
      <td><font size="2"><strong>5</strong></font>.</td>
      <td><div align="left"><em><strong><font size="1" face="Times New Roman, Times, serif">Bayaran 
          pendahuluan (Advance Payment) akan hanya dibayar berdasarkan 70% nilai 
          kerja-kerja yang telah dilaksanakan dengan memuaskan pada hari pembayaran 
          itu dan juga setelah diambil kira segala nilai barang/stor atau lain-lain 
          &nbsp;perkara yang dihutang oleh PEMBORONG kepada LADANG.</font></strong></em></div></td>
    </tr>
    <tr> 
      <td><font size="2"><strong>6.</strong></font></td>
      <td><div align="left"><em><strong><font size="1" face="Times New Roman, Times, serif">Pembayaran 
          atas kerja-kerja yang dilaksanakan dengan sempurna akan dijelaskan oleh 
          LADANG kepada PEMBORONG berdasarkan kepada penilaian kerja yang akan 
          dilaksanakan oleh pengurus LADANG atau wakilnya.</font></strong></em></div></td>
    </tr>
    <tr> 
      <td><font size="2"><strong>7.</strong></font></td>
      <td><div align="left"><em><strong><font size="1" face="Times New Roman, Times, serif">Wang 
          tahanan sebanyak 5% dari nilai kerja akan dikenakan ke atas setiap pembayaran 
          dan akan dikembalikan selepas tamat 6 bulan perjanjian ini dengan syarat 
          kontraktor telah mematuhi semua kehendak perjanjian ini.</font></strong></em></div></td>
    </tr>
    <tr> 
      <td><font size="2"><strong>8.</strong></font></td>
      <td rowspan="2"><div align="left"><em><strong><font size="1" face="Times New Roman, Times, serif">PEMBORONG 
          juga dikehendaki dan bersetuju untuk mematuhi syarat-syarat berikut 
          dalam melaksanakan kerja-kerja ini :-</font></strong></em><br>
          <em><strong><font size="1" face="Times New Roman, Times, serif">a) PEMBORONG 
          mesti mematuhi segala peraturan dan perundangan berkaitan pengambilan 
          dan penggajian tenaga kerja dan sumber-sumbernya yang lain.</font></strong></em><br>
          <em><strong><font size="1" face="Times New Roman, Times, serif">&nbsp;b) 
          PEMBORONG tidak dibenarkan menggaji PEKERJA ASING TANPA IZIN.</font></strong></em></div></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
    </tr>
    <tr> 
      
      <td><font size="2"><strong>9.</strong></font></td>
      <td><div align="left"><em><strong><font size="1" face="Times New Roman, Times, serif">PEMBORONG mestilah menyediakan peralatan keselamatan 
(PPE) kepada setiap pekerja dan memastikan pekerja menggunakan 
peralatan yang 
betul seperti mana yang diarahankan oleh Akta Keselamatan 
Dan Kesihatan Pekerja (OSHA) 1994.</font></strong></em></div></td>
     
      <tr> 
      <td><font size="2"><strong>10.</strong></font></td>
      <td rowspan="2"><div align="left"><em><strong><font size="1" face="Times New Roman, Times, serif">Perjanjian 
          kerja ini adalah dianggap dimungkiri dan terbatal apabila :-</font></strong></em><br>
          <em><strong><font size="1" face="Times New Roman, Times, serif"> a) 
          PEMBORONG gagal melaksana dan menyiapkan kerja-kerja ini dengan sempurna 
          mengikut keperluan LADANG dalam masa 7 hari setelah notis peringatan 
          dikeluarkan oleh LADANG kepada PEMBORONG dan atau</font></strong></em><br>
          <em><strong><font size="1" face="Times New Roman, Times, serif">&nbsp;b) 
          PEMBORONG melanggar syarat-syarat dan/atau perincian pekerjaan yang 
          dipersetujui bersama dan/atau</font></strong></em><br>
          <em><strong><font size="1" face="Times New Roman, Times, serif">&nbsp;c) 
          PEMBORONG gagal untuk melaksanakan kerja-kerja ini dengan lancar dan 
          efisien sama ada secara sengaja atau tidak setelah 7 hari notis peringatan 
          dikeluarkan oleh LADANG kepada PEMBORONG.</font></strong></em></div></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td><font size="2"><strong>11.</strong></font></td>
      <td rowspan="2"><div align="left"><em><strong><font size="1" face="Times New Roman, Times, serif">Dalam 
          perkara-perkara di bilangan 10, LADANG berhak mengambil pekerja-pekerja 
          atau peralatan yang lain untuk meneruskan serta menyiapkan kerja-kerja 
          ini dengan sempurna. Sebarang perbelanjaan atau kerugian yang dialami 
          &nbsp;LADANG akibat dari perkara ini adalah ditanggung dan perlu dibayar 
          oleh PEMBORONG kepada LADANG. Wang tahanan 5% (RETENTION FUND) dan wang 
          bayaran penalti yang dikenakan boleh digunakan oleh LADANG bagi &nbsp;menyelesaikan 
      dengan sempurna kerja yang gagal dilaksanakan oleh PEMBORONG</font></strong></em></div></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td><font size="2"><strong>12.</strong></font></td>
      <td rowspan="2"><div align="left"><em><strong><font size="1" face="Times New Roman, Times, serif">LADANG 
          akan memenuhi tanggungjawab ke atas perjanjian ini apabila semua persetujuan 
          di atas dipatuhi.</font></strong></em><br>
          <em><strong><font size="1" face="Times New Roman, Times, serif">&nbsp;Perjanjian 
          ini ditandatangani di hadapan :-</font></strong></em><em><strong><font size="1" face="Times New Roman, Times, serif">&nbsp;&nbsp;&nbsp;&nbsp;</font></strong></em></div></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
    
  <table width="90%" border="0" align="center" cellpadding="1" cellspacing="1">
    <tr> 
      <td width="33%"></td>
      <td width="29%"><div align="center"></div></td>
      <td width="23%"></td>
      <td width="15%">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td><div align="center"><font size="1" face="Times New Roman, Times, serif">.......................................................................................</font></div></td>
      <td width="29%">&nbsp;</td>
      <td width="23%"><div align="center"><font size="1" face="Times New Roman, Times, serif">.......................................................................................</font></div></td>
      <td width="15%">&nbsp;</td>
    </tr>
 
    <tr> 
      <td width="33%"><div align="center"><font size="1" face="Times New Roman, Times, serif"><strong><%//=position%></strong></font></div></td>
      <td>&nbsp;</td>
      <td width="23%"><div align="center"><font size="1" face="Times New Roman, Times, serif"><strong> 
          Pemborong</strong></font></div></td>
      <td width="15%">&nbsp;</td>
    </tr>
    <tr> 
      <td><div align="center"><strong><font size="1" face="Times New Roman, Times, serif"><%//=pengurus%></font></strong></div></td>
      <td><div align="center"><strong></strong></div></td>
      <td><div align="center"><strong><font size="1" face="Times New Roman, Times, serif"><%//=owner%></font></strong></div></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td><div align="center"><strong><font size="1" face="Times New Roman, Times, serif">.......................................................................................</font></strong></div></td>
      <td>&nbsp;</td>
      <td> <div align="center"><strong><font size="1" face="Times New Roman, Times, serif">...................................................................................</font></strong></div></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td><div align="left"><strong><font size="1" face="Times New Roman, Times, serif">Nama 
          &nbsp;&nbsp;:&nbsp;<%//=saksi%>&nbsp;<br>
          </font></strong></div></td>
      <td>&nbsp;</td>
      <td><p align="left"><strong><font size="1" face="Times New Roman, Times, serif">Nama 
          &nbsp; : <br>
          &nbsp;&nbsp;&nbsp; </font></strong></p></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td><div align="left"><strong><font size="1" face="Times New Roman, Times, serif">Saksi</font></strong></div></td>
      <td >&nbsp;</td>
      <td><div align="left"><strong><font size="1" face="Times New Roman, Times, serif">Saksi</font><font size="1" face="Times New Roman, Times, serif"> 
          </font></strong></div></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td id="noprint"><div align="center"><strong><font size="1" face="Times New Roman, Times, serif"><span id="noprint"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
       </font></span> </font></strong></div></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <p align="center"><strong><font size="1" face="Times New Roman, Times, serif"> 
    </font></strong></p>
</form>
                                    
                                    <!-- end of agreement -->
                                
                                </div>
                                <div class="tab-pane fade" id="tab3default">
                                
                                    
                                
                                </div>
                                <div class="tab-pane fade" id="tab4default">Default 4</div>
                                <div class="tab-pane fade" id="tab5default">Default 5</div>
                            </div>
                        </div>
                </div>
                </div>
        <br/>
        </div>
                    </div>
                </div>
    </body>
</html>
