<%-- 
    Document   : list_location_by
    Created on : Mar 2, 2016, 11:32:25 AM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.LocationDAO"%>
<%@page import="com.lcsb.fms.util.model.Location"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<div class="list-group"><%
            
            String by = request.getParameter("by");
            String keyword = request.getParameter("keyword");
            
            List<Location> slist = (List<Location>) LocationDAO.getLocation(log,by,keyword);
            for (Location c : slist) { 
               %>
       
            <a href="<%= c.getLocationDesc()%>" id="<%= c.getLocationCode()%>" class="list-group-item thisresult_nd">
                <div id="left_div"><%= c.getLocationCode()%></div>
                <div id="right_div"><%= c.getLocationDesc()%></div>
            </a>

               <%    
            } 
        %>
</div>
