<%-- 
    Document   : testpage
    Created on : Dec 2, 2016, 10:09:31 AM
    Author     : fadhilfahmi
--%>

<%@page import="java.sql.Connection"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.logging.Level"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFCell"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFRow"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFSheet"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@page import="org.apache.poi.poifs.filesystem.POIFSFileSystem"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.BufferedInputStream"%>
<%@page import="com.lcsb.fms.util.ext.ReadExcelFile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/pvoucher.css?1" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div>
            <%
                Connection con = ConnectionUtil.getConnection();
                InputStream input = new BufferedInputStream(
                        new FileInputStream("/Users/fadhilfahmi/Google Drive/fms_v1/target/fms_v1-1.0/uploadedFiles/SEP_16_12.xls"));
                POIFSFileSystem fs = new POIFSFileSystem(input);
                HSSFWorkbook wb = new HSSFWorkbook(fs);
                HSSFSheet sheet = wb.getSheetAt(1);
            %><table border="1" cellpadding="0" cellspacing="0"><%
                Iterator rows = sheet.rowIterator();
                while (rows.hasNext()) {
                    HSSFRow row = (HSSFRow) rows.next();
                    out.println("<tr>");
                    Iterator cells = row.cellIterator();
                    int c = 0;
                    String str = "";
                    while (cells.hasNext()) {
                        c++;

                %><td><%                    HSSFCell cell = (HSSFCell) cells.next();
                    if (c < 10) {

                        String val = "";

                        if (HSSFCell.CELL_TYPE_NUMERIC == cell.getCellType()) {
                            val = String.valueOf(cell.getNumericCellValue());
                        } else if (HSSFCell.CELL_TYPE_STRING == cell.getCellType()) {
                            val = String.valueOf(cell.getStringCellValue());
                        } else if (HSSFCell.CELL_TYPE_BOOLEAN == cell.getCellType()) {
                            val = String.valueOf(cell.getBooleanCellValue());
                        } else if (HSSFCell.CELL_TYPE_BLANK == cell.getCellType()) {
                            val = "";
                        } else {
                            out.print("N/A");
                        }

                        if (c == 9) {
                            str = str + "\"" + val + "\"";
                        } else if (c > 1 && c < 9) {
                            str = str + "\"" + val + "\",";
                        }

                        out.print(val);

                    %></td><%   }
                        }

                        

                    %></tr>
                <%String query = "INSERT INTO lot_member_extract(name, nopetak, ic, bank, noakaun, notel, ekar, hektar) values (" + str + ")";
                        out.println(query+"<br>");
                        PreparedStatement ps = con.prepareStatement(query);

                        //ps.executeUpdate();
                        //ps.close();     
                }
                %>
            </table>
        </div>
    </body>
</html>
