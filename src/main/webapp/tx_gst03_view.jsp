<%-- 
    Document   : tx_gst03_view
    Created on : Nov 25, 2016, 2:33:30 PM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.fms.model.financial.tx.TxGst03Refer"%>
<%@page import="com.lcsb.fms.dao.financial.tx.TaxGst03DAO"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxGst03"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxInputDetail"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxInput"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxReconcileMaster"%>
<%@page import="com.lcsb.fms.dao.financial.tx.TaxReconcileDAO"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxOutputDetail"%>
<%@page import="com.lcsb.fms.dao.financial.tx.TaxOutputDAO"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxOutput"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxVoucherItem"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxVoucher"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    TxGst03 v = (TxGst03) TaxGst03DAO.getGst03(log, request.getParameter("refer"));
    Module mod = (Module) TaxGst03DAO.getModule();

    String status = TaxGst03DAO.getStatus(log, v.getRefer());

    String distribute = request.getParameter("distribute");
%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/tether.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>

<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        window.scrollTo(0, 0);

        callPnotify('<%= request.getParameter("status")%>', '<%= request.getParameter("text")%>');

        $('#print').click(function (e) {
            printElement($("#viewjv").html());
        });

        $('.viewvoucher').click(function (e) {
            var a = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewor&refer=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $('.gotoJV').click(function (e) {
            var a = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=gotoJV&referno=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".editmain").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editmain&referno=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".edititem").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('name');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + b + "&referno=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".deleteitem").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('name');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are you sure to delete?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + b + "&referno=" + a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });

            return false;
        });

        $(".deletemain").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('href');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'All datas will be permanently deleted. Are you sure to proceed?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&referno=" + a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            return false;
        });


        $(".generatejv").click(function (e) {
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=generateJV&referno=" + id,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $('.activerowm').click(function (e) {
            e.preventDefault();
            $(".togglerow").toggle();
            //$(this).toggleClass('info');
            //$("#togglerow_nd"+b).toggleClass('warning');
        });

        $('.activerowx').click(function (e) {
            e.preventDefault();
            var b = $(this).attr('id');
            $(".togglerow_nd" + b).toggle();
            $(this).toggleClass('info');
            $(".togglerow_nd" + b).toggleClass('warning');
        });

        $('.activerowy').click(function (e) {
            e.preventDefault();
            var b = $(this).attr('id');
            $("#togglerow_st" + b).toggle();
            $(this).toggleClass('info');
            $("#togglerow_st" + b).toggleClass('warning');
        });

        $(".approve").click(function () {
            //var a = $("form").serialize();
            var a = $(this).attr('id');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are you sure to Approve this?',
                type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Approve', // <-- Default value is 'OK',
                btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=approve&referno=" + a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            return false;
        });

        $(".viewpdf").click(function () {
            var a = $(this).attr('id');
           //window.location.href = 'GST?refer='+a;
           window.open('GST?refer='+a);
            return false;
        });
        
        $(".viewgst03").click(function (e) {
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewgst03&refer=" + id,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
                </tr>
            </table>
            <br>
            <table class="table table-bordered table-striped  table-hover">
                <thead>
                    <tr>
                        <th colspan="4">Tax Return - GST 03
                            <!--<small><span class="label label-primary">Preparing</span></small>-->
                            <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
                                <button  class="btn btn-default btn-xs viewgst03" title="<%= mod.getModuleID()%>" id="<%= v.getRefer()%>" name="addrefer"><i class="fa fa-eye" aria-hidden="true"></i> View</button>
                                <%if (!TaxGst03DAO.isApprove(log, v.getRefer()) && log.getListUserAccess().get(1).getAccess()) {%>

                                <%if (!status.equals("JV Created")) {%>
                                 
                                <button  class="btn btn-default btn-xs generatejv" title="<%= mod.getModuleID()%>" id="<%= v.getRefer()%>" name="addrefer"><i class="fa fa-check-square-o" aria-hidden="true"></i> Generate JV</button>
                                <%} else {%>
                                <button  class="btn btn-default btn-xs approve" title="<%= mod.getModuleID()%>" id="<%= v.getRefer()%>" name="addrefer"><i class="fa fa-check-square-o" aria-hidden="true"></i> Approve</button>
                                <button  class="btn btn-default btn-xs gotoJV" title="<%= mod.getModuleID()%>" id="<%= v.getGenjvref()%>" name="addrefer"><%= v.getGenjvref()%></button>
                                <%}%>


                                <%if (!status.equals("JV Created")) {%>
                                <button id="" class="btn btn-default btn-xs editmain" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-pencil" aria-hidden="true"></i></button>

                                <button id="<%= v.getRefer()%>"  class="btn btn-danger btn-xs deletemain" title="<%= mod.getModuleID()%>" id="" name="delete_st"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                    <%}%>
                                    <%} else {%>


                                <%}%>
                                <button  class="btn btn-default btn-xs viewpdf" title="<%= mod.getModuleID()%>" id="<%= v.getRefer()%>" name="addrefer"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> View PDF</button>
                                <button id="" class="btn btn-default btn-xs viewvoucher" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i></button>
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody class="activerowm tdrow">
                    <tr>
                        <th width="15%">Status</th>
                        <td width="35%"><%= GeneralTerm.getStatusLabel(TaxGst03DAO.getStatus(log, v.getRefer()))%></td>
                        <th width="15%">Reference No</th>
                        <td width="35%"><%= v.getRefer()%></td>
                    </tr>
                    <tr>
                        <th>Tax Return Due date</th>
                        <td><%= v.getTaxdue()%></td>
                        <th>Name</th>
                        <td><%= v.getLocname()%></td>
                    </tr>
                    <tr>
                        <th>Taxable Period From</th>
                        <td><%= v.getTaxstart()%></td>
                        <th>Taxable Period To</th>
                        <td><%= v.getTaxend()%></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-bordered table-striped table-hover" id="dataTables">
                <thead>
                    <tr>
                        <th colspan="7">Detail<!--<small>asdada</small>-->

                        </th>
                    </tr>
                    <tr>
                        <th>Reference No</th>
                        <th>Tax Year</th>
                        <th>Tax Period</th>
                        <th>Tax Start</th>
                        <th>Tax End</th>
                        <th>Tax Debit</th>
                        <th>Tax Credit</th>
                    </tr>
                </thead>
                <%List<TxGst03Refer> listAll = (List<TxGst03Refer>) TaxGst03DAO.getGst03ReferAll(log, v.getRefer());
                    if (listAll.isEmpty()) {%>
                <tbody>
                    <tr>
                        <td colspan="3">
                            <span class="font-small-red">No data available.</span>
                        </td>
                    </tr>
                    <%}
                        for (TxGst03Refer j : listAll) {%>
                    <tr class="activerowy" id="<%= j.getRefer()%>">
                        <td class="tdrow">&nbsp;<%= j.getRefer()%></td>
                        <td class="tdrow">&nbsp;<%= j.getTaxyear()%></td>
                        <td class="tdrow">&nbsp;<%= j.getTaxperiod()%></td>
                        <td class="tdrow">&nbsp;<%= j.getTaxstart()%></td>
                        <td class="tdrow">&nbsp;<%= j.getTaxend()%></td>
                        <td class="tdrow" align="right">&nbsp;<%= GeneralTerm.currencyFormat(j.getTaxdebit())%></td>
                        <td class="tdrow" align="right">&nbsp;<%= GeneralTerm.currencyFormat(j.getTaxcredit())%></td>

                    </tr>

                </tbody>
                <%}%>
            </table>
        </div>

    </div>
</div>    
