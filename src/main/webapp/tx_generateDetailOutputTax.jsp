<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page import="java.text.*,java.util.*" %>
<%@ page import="java.util.Date"%>
<%response.setHeader("Cache-Control", "no-cache");

String connTime = (String)session.getAttribute("estateid");
if(connTime!=null) 
{%>
<html>
<%int pos=0;%>
<head>
<title>Untitled Document</title>
<link rel="stylesheet" href="themes/base/jquery.ui.all.css">
	<script src="jquery-1.5.1.js"></script>
	<script src="ui/jquery.ui.core.js"></script>
	<script src="ui/jquery.ui.widget.js"></script>
	<script src="ui/jquery.ui.tabs.js"></script>
	<link rel="stylesheet" href="demos.css">
	<script>
	$(function() {
		$( "#tabs" ).tabs();
	});
	</script>
    
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<link href="css/mainForm.css" rel="stylesheet" type="text/css" />
<link href="css/tableFixed2.css" rel="stylesheet" type="text/css" />
<link href="css/report.css" rel="stylesheet" type="text/css" />
<!-- print style sheet -->
<style type="text/css" media="print">
div.tableContainer {overflow: visible;	}
table>tbody	{overflow: visible; }
td {height: 14pt;} /*adds control for test purposes*/
thead td	{font-size: 11pt;	}
tfoot td	{
	text-align: center;
	font-size: 9pt;
	border-bottom: solid 1px slategray;
	}
	
thead	{display: table-header-group;	}
tfoot	{display: table-footer-group;	}
thead th, thead td	{position: static; } 

thead tr	{position: static; } /*prevent problem if print after scrolling table*/ 
table tfoot tr {     position: static;    }


</style>
<script type="text/javascript" src="js/table_alt.js"></script>
<script language="javascript">
function submitRecord(refno) { 
	document.form1.action = "tx_outputDetail_save.jsp?refno="+refno+"&flag=save";
	form1.submit()
}

</script>

<%!
Connection con;
String test="";

DecimalFormat df=new DecimalFormat("###,###,###,##0.00");

String normalDebit(double val)
{
	String amt=df.format(val);
	if(amt.equalsIgnoreCase("0.00"))
		return amt;
	else
	{
	if(val<0)
		return df.format(val*-1);
	else
		return "(".concat(df.format(val)).concat(")");
	}
}

String normalCredit(double val)
{

	String amt=df.format(val);
	if(amt.equalsIgnoreCase("0.00"))
		return amt;
	else
	{
	if(val<0)
		return "(".concat(df.format(-1*val)).concat(")");
	else
		return df.format(val);
	}
}

%>


<body>
<%
//String output_trans[] = {"Sales Invoice","Sales Provision","Official Receipt","Debit Note","Credit Note","Journal Voucher"};
String output_trans[] = {"Sales Invoice","Debit Note","Credit Note","Journal Voucher","Official Receipt","Creditors Debit Note","Creditors Credit Note","Stock And Store"};

con = (Connection)session.getAttribute("con"); 
DecimalFormat df=new DecimalFormat("###,###,###,##0.00");
DecimalFormat df2=new DecimalFormat("###########0.00");
String refno = request.getParameter("refno");;
String tx_period_from = "";
String tx_period_to = "";
String tx_return_date = "";
String tx_name = "";
String year = "";
String period = "";
String status = request.getParameter("status");

String pid="";
String pname="";
String pdate="";
String cid="";
String cname="";
String cdate="";
String aid="";
String aname="";
String adesignation="";
String adate="";


Statement stmt = con.createStatement();
ResultSet res = stmt.executeQuery("select * from tx_output where refno = '"+refno+"'");
if (res.next()) {
	tx_period_from = res.getString("taxperiodfrom");
	tx_period_to = res.getString("taxperiodto");
	tx_return_date = res.getString("taxreturndate");
	tx_name = res.getString("name");
	year = res.getString("year");
	period = res.getString("period");
}

%>
<form name="form1" method="post" action="">
<h1>Generate Output Tax&nbsp;&nbsp;&nbsp;</h1>
<table cellspacing="2" cellpadding="2" width="100%">
<input type="hidden" value="<%=year%>" name="year">
<input type="hidden" value="<%=period%>" name="period">
<tr>
<td>

<div class="demo">

<div id="tabs">
	<ul>
		<li><a href="#tabs-1">Output Tax</a></li>
	</ul>
	<div id="tabs-1">
    <p>
        	<table cellspacing="0" cellpadding="3" width="100%">
            	<tr align="center" bgcolor="#006699">
                	<td class="border1110" style="color:#FFF">Trx Date</td>
                    <!--td class="border1110" style="color:#FFF">Trx Type</td-->
                    <td class="border1110" style="color:#FFF">Ref. No</td>
                    <td class="border1110" style="color:#FFF">Tax Code</td>
                    <td class="border1110" style="color:#FFF">Tax Desc</td>
                    <td class="border1110" style="color:#FFF">Comp Code</td>
                    <td class="border1110" style="color:#FFF">Comp Name</td>
                    <td class="border1110" style="color:#FFF">GST Id</td>
                    <td class="border1110" style="color:#FFF">Amount (RM)</td>
                    <td class="border1110" style="color:#FFF">Tax (%)</td>
                    <td class="border1110" style="color:#FFF">Remark</td>
                    <td class="border1110" style="color:#FFF">Tax Amount (RM)</td>
                    <td class="border1011" style="color:#FFF">&nbsp;</td>
                </tr>
                
                
                 <% 
				 	double tot_amt = 0.00;double tot_amtInput = 0.00;
					double tot_taxoutput = 0.00;
					double tot_taxInput = 0.00;
					String getDetailTx = "";
					String getMaster = "";
					int y = 0;
					int x = 0;
					for (int m =0; m<=output_trans.length-1;m++ ) { %>
				   <tr>
					  <td colspan="12" bgcolor="dbdbdb"><b><i><%=output_trans[m]%></i></b></td>
				   </tr>
               
               <%
			   		Statement stmt00 = con.createStatement();
					String sqlQuery="";
					///sales invoice
					if(output_trans[m].equalsIgnoreCase("Sales Invoice")) { //left(a.invref,3) as kodVoucher,
						
					//sqlQuery="select invdate as Tarikh, a.invref as novoucher,  b.taxcode, c.descp, sum(b.amount) as amount ,sum(b.taxamt) as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, b.sacode, b.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.invref,3) as kodVoucher from sl_inv a inner join sl_inv_item b on a.invref=b.ivref inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No'  and b.taxamt != 0 and a.postflag='posted' group by b.taxcode, a.gstid, novoucher";//and a.postflag='posted'
					
					sqlQuery="select b.ivno as queue, invdate as Tarikh, a.invref as novoucher,  b.taxcode, c.descp, b.amount as amount ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, b.sacode, b.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.invref,3) as kodVoucher, a.bcode as compcode, a.bname as compname, b.remarks as remark from sl_inv a inner join sl_inv_item b on a.invref=b.ivref inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and a.postflag='posted'";//and a.postflag='posted'
					
					/*} else if(output_trans[m].equalsIgnoreCase("Sales Provision")) {
						
						sqlQuery="select a.date as Tarikh, a.spref as novoucher, b.taxcode, c.descp, sum(b.nett_amount2) as amount ,sum(b.taxamt) as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, b.gstid, b.bcode as sacode, b.bname as sadesc, a.estatecode as estatecode, a.estatename as estatename, left(a.spref,3) as kodVoucher from sl_sp_master a inner join sl_sp b on a.spref=b.spref inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and b.taxamt != 0 and a.postflag='posted'  group by b.taxcode, b.gstid, novoucher";*/
						
					} else if (output_trans[m].equalsIgnoreCase("Official Receipt")) {
						
						//sqlQuery = "select a.date as Tarikh, a.refer as novoucher, b.taxcode, c.descp, sum(b.amtbeforetax) as amount ,sum(b.amount) as taxamt, b.taxrate, b.coacode as taxcoacode,b.coadescp as taxcoadescp, a.gstid, b.sacode, b.sadesc, a.estatecode as estatecode, a.estatename as estatename, left(a.refer,3) as kodVoucher from cb_official a inner join cb_official_credit b on a.refer=b.voucherno inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and a.post='posted'  group by b.taxcode, a.gstid, novoucher";
						
						sqlQuery = "select b.refer as queue, a.date as Tarikh, a.refer as novoucher, b.taxcode, c.descp, b.amount as amount ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, b.sacode, b.sadesc, a.estatecode as estatecode, a.estatename as estatename, left(a.refer,3) as kodVoucher,a.paidcode as compcode,  a.paidname as compname, b.remarks as remark  from cb_official a inner join cb_official_credit b on a.refer=b.voucherno inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and a.post='posted' ";
						
					} else if (output_trans[m].equalsIgnoreCase("Debit Note")) {
						
						//sqlQuery = "select a.notedate as Tarikh, a.noterefer as novoucher, b.taxcode, c.descp, sum(b.amount) as amount ,sum(b.taxamt) as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, a.sacode, a.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.noterefer,3) as kodVoucher from tx_debitnote_master a inner join tx_debitnote b on a.noterefer=b.noterefer inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and a.postflag='posted' group by b.taxcode, a.gstid, novoucher";
						
						sqlQuery = " select b.noteno as queue, a.notedate as Tarikh, a.noterefer as novoucher, b.taxcode, c.descp, b.amount as amount ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, a.sacode, a.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.noterefer,3) as kodVoucher, a.companycode as compcode, a.companyname as compname, b.remark as remark  from tx_debitnote_master a inner join tx_debitnote b on a.noterefer=b.noterefer inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and a.postflag='posted' ";
					
						
					} else if (output_trans[m].equalsIgnoreCase("Credit Note")) {
						
						//sqlQuery = "select a.notedate as Tarikh, a.noterefer as novoucher, b.taxcode, c.descp, sum(b.amount) as amount ,sum(b.taxamt) as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, a.sacode, a.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.noterefer,3) as kodVoucher from tx_creditnote_master a inner join tx_creditnote b on a.noterefer=b.noterefer inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and a.postflag='posted'  group by b.taxcode, a.gstid, novoucher";
						
						sqlQuery = "select b.noteno as queue, a.notedate as Tarikh, a.noterefer as novoucher, b.taxcode, c.descp, b.amount as amount ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, a.sacode, a.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.noterefer,3) as kodVoucher, a.companycode as compcode, a.companyname as compname, b.remark as remark from tx_creditnote_master a inner join tx_creditnote b on a.noterefer=b.noterefer inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and a.postflag='posted'";
						
					} else if (output_trans[m].equalsIgnoreCase("Journal Voucher")) {
						
						//sqlQuery = "select a.jvdate as Tarikh, a.jvrefno as novoucher, b.taxcode, c.descp, sum(b.credit-b.debit) as amount ,sum(b.credit-b.debit) as taxamt, b.taxrate, b.actcode,b.actdesc, b.gstid, b.sacode, b.sadesc, a.estatecode as estatecode, a.estatename as estatename, taxcoacode, taxcoadescp, left(a.jvrefno,3) as kodVoucher from gl_jv a inner join gl_jv_item b on a.jvrefno=b.jvrefno inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and a.postflag='posted'  group by b.taxcode, novoucher";
						
						sqlQuery = "select b.jvid as queue, a.jvdate as Tarikh, a.jvrefno as novoucher, b.taxcode, c.descp, b.amtbeforetax as amount ,b.credit-b.debit as taxamt, b.taxrate, b.actcode,b.actdesc, b.gstid, b.sacode, b.sadesc, a.estatecode as estatecode, a.estatename as estatename, taxcoacode, taxcoadescp, left(a.jvrefno,3) as kodVoucher, '00' as compcode, 'Not Applicable' as compname, b.remark as remark from gl_jv a inner join gl_jv_item b on a.jvrefno=b.jvrefno inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and a.postflag='posted'";
						
					} else if (output_trans[m].equalsIgnoreCase("Creditors Debit Note")) {
						
						//sqlQuery = "select a.notedate as Tarikh, a.noterefer as novoucher, b.taxcode, c.descp, sum(b.amount) as amount ,sum(b.taxamt) as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, a.sacode, a.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.noterefer,3) as kodVoucher from tx_sdn_master a inner join tx_sdn b on a.noterefer=b.noterefer inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and postflag='posted' group by b.taxcode, a.gstid,a.noterefer ";
						
						sqlQuery = "select b.noteno as queue, a.notedate as Tarikh, a.noterefer as novoucher, b.taxcode, c.descp, b.amount as amount ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, a.sacode, a.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.noterefer,3) as kodVoucher, companycode as compcode, companyname as compname, b.remark as remark from tx_sdn_master a inner join tx_sdn b on a.noterefer=b.noterefer inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and postflag='posted' ";
					
						
					} else if (output_trans[m].equalsIgnoreCase("Creditors Credit Note")) {   
						
						//sqlQuery = "select a.notedate as Tarikh, a.noterefer as novoucher, b.taxcode, c.descp, sum(b.amount) as amount ,sum(b.taxamt) as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, a.sacode, a.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.noterefer,3) as kodVoucher from tx_scn_master a inner join tx_scn b on a.noterefer=b.noterefer inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and postflag='posted' group by b.taxcode, a.gstid,a.noterefer ";
						sqlQuery = "select b.noteno as queue, a.notedate as Tarikh, a.noterefer as novoucher, b.taxcode, c.descp, b.amount as amount ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, a.sacode, a.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.noterefer,3) as kodVoucher, companycode as compcode, companyname as compname, b.remark as remark  from tx_scn_master a inner join tx_scn b on a.noterefer=b.noterefer inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and postflag='posted'  ";
						
					}
					 else if (output_trans[m].equalsIgnoreCase("Stock And Store")) {   
						
						//sqlQuery = "select a.notedate as Tarikh, a.noterefer as novoucher, b.taxcode, c.descp, sum(b.amount) as amount ,sum(b.taxamt) as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, a.gstid, a.sacode, a.sadesc, a.estcode as estatecode, a.estname as estatename, left(a.noterefer,3) as kodVoucher from tx_scn_master a inner join tx_scn b on a.noterefer=b.noterefer inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and postflag='posted' group by b.taxcode, a.gstid,a.noterefer ";
						sqlQuery = "select b.sivid as queue, a.date as Tarikh, a.sivref as novoucher, b.taxcode, c.descp, b.amount + b.cgamount as amount ,b.taxamt as taxamt, b.taxrate, b.taxcoacode,b.taxcoadescp, b.gstid, b.dtsacode as sacode, b.dtsadesc as sadesc, a.estatecode, a.estatename, left(a.sivref,3) as kodVoucher, b.dtsacode as compcode, b.dtsadesc as compname, b.remarks as remark  from st_stockout_master a inner join st_stockout_detail b on a.sivref=b.sivref inner join cg_gst c on c.taxcode=b.taxcode and c.taxtype like 'GST%' and c.taxitem='SUPPLY' where a.postgst='No' and post='2'  ";
						
					}
						
					
					ResultSet res00 = stmt00.executeQuery(sqlQuery);
					
					
					while (res00.next()) {
						y+=1;
						x+=1;
						String tarikh = res00.getString("Tarikh");
						String noVoucher = res00.getString("novoucher");
						String kodVoucher2 = res00.getString("kodVoucher");
						String kodVoucher = output_trans[m];
						String taxcode = res00.getString("taxcode");
						String taxDescp = res00.getString("descp"); 
						double amt = res00.getDouble("amount");
						double taxamt = res00.getDouble("taxamt");
						double taxrate = res00.getDouble("taxrate");
						String taxcoacode = res00.getString("taxcoacode");
						String taxcoadescp = res00.getString("taxcoadescp");
						String gstid = res00.getString("gstid");
						//String compcode = res00.getString("sacode");
						//String compname = res00.getString("sadesc");
						String compcode = res00.getString("compcode");
						String compname = res00.getString("compname");
						String estatecode = res00.getString("estatecode");
						String estatename = res00.getString("estatename");
						String remark = res00.getString("remark");
						String queue = res00.getString("queue");
						
							//tot_amtInput +=amt;
							//tot_taxInput +=taxamt;
							String clr ="";
							int a = x % 2;
							if (a==0) clr = "bgcolor='#E1EEF4'";
							if (a==1) clr = "bgcolor='#FFFFFF'";
							
					if (kodVoucher2.equalsIgnoreCase("JSP")) {
						Statement stmt003 = con.createStatement();
						ResultSet res003 = stmt003.executeQuery("select * from sl_sp where genjvref='"+noVoucher+"'");
							if(res003.next()) {
								//amt = amt;//res003.getDouble("NETT_AMOUNT2");
								taxamt = res003.getDouble("taxamt");
								compcode = res003.getString("bcode");
								compname = res003.getString("bname");
							}
					}
					
					if (kodVoucher2.equalsIgnoreCase("JSC")) {
						Statement stmt003 = con.createStatement();
						ResultSet res003 = stmt003.executeQuery("select * from sl_sc where genjvref='"+noVoucher+"'");
							if(res003.next()) {
								amt = res003.getDouble("NETT_AMOUNT2");
								taxamt = res003.getDouble("taxamt");
								compcode = res003.getString("bcode");
								compname = res003.getString("bname");
							}
					}
					
					if (kodVoucher2.equalsIgnoreCase("JSA")) {
						Statement stmt003 = con.createStatement();
						ResultSet res003 = stmt003.executeQuery("select * from sl_sa a inner join sl_sa_item b on a.saref=b.saref where a.genjvref='"+noVoucher+"'");
							if(res003.next()) {
								amt = res003.getDouble("DIF_SALES_TOTAL");
								taxamt = res003.getDouble("dif_taxamt");
							}
					}
					
					if (kodVoucher2.equalsIgnoreCase("JVE")) {
						Statement stmt003 = con.createStatement();
						ResultSet res003 = stmt003.executeQuery("select * from fa_sale where refjv='"+noVoucher+"'");
							if(res003.next()) {
								amt = res003.getDouble("saleprice");
								taxamt = res003.getDouble("taxamt");
							}
					}
					
					
					if (kodVoucher2.equalsIgnoreCase("ORN") && taxamt==0) {
						Statement stmt003 = con.createStatement();
						ResultSet res003 = stmt003.executeQuery("select * from cb_official_credit where refer='"+queue+"' and taxcode='"+taxcode+"' and taxrate>0");
							if(res003.next()) {
								amt = res003.getDouble("amtbeforetax");
								taxamt = res003.getDouble("amount");
								taxcoacode = res003.getString("coacode");
								taxcoadescp = res003.getString("coadescp");
							}
					}
					
					if (kodVoucher2.equalsIgnoreCase("SIV")) {
						Statement stmt003 = con.createStatement();
						ResultSet res003 = stmt003.executeQuery("select gstid from contractor_info where suppliercode='"+compcode+"'");
							if(res003.next()) {
								gstid = res003.getString("gstid");
								
							}
					}
					
					
					String amt_str =""+df.format(amt)+"";
					String taxamt_str = ""+df.format(taxamt)+"";
					
					
					
					if (kodVoucher2.equals("TSC")) { 
						tot_amtInput -=amt;
						tot_taxInput -=taxamt;
						amt_str = normalDebit(amt);
						taxamt_str = normalDebit(taxamt);
						taxamt = -taxamt;
						amt = -amt;
					}
					
					else if (kodVoucher2.equals("TSD")) {
						tot_amtInput +=amt;
						tot_taxInput +=taxamt;
					}
					
					else if (kodVoucher2.equals("TDN")) {
						tot_amtInput +=amt;
						tot_taxInput +=taxamt;
						
					}
					
					else if (kodVoucher2.equals("TCN")) {
						tot_amtInput -=amt;
						tot_taxInput -=taxamt;
						amt_str = normalDebit(amt);
						taxamt_str = normalDebit(taxamt);
						taxamt = -taxamt;
						amt = -amt;
					
					} else if (kodVoucher2.equals("JSA")) {
						tot_amtInput -=amt;
						tot_taxInput -=taxamt;
						amt_str = normalDebit(amt);
						taxamt_str = normalDebit(taxamt);
						taxamt = -taxamt;
						amt = -amt;
						
					} else {
							tot_amtInput +=amt;
							tot_taxInput +=taxamt;
					}
						
							
				%>
                	<tr <%=clr%>>
                    	<td class="border0010"><%=tarikh%></td>
                        <td class="border0010"><%=noVoucher%></td>
                        <td class="border0010" align="center"><%=taxcode%></td>
                        <td class="border0010"><%=taxDescp%></td>
                         <td class="border0010"><%=compcode%></td>
                        <td align="left" class="border0010"><%=compname%></td>
                        <td align="left" class="border0010"><%=gstid%>&nbsp;</td>
                        <td align="right" class="border0010"><%=amt_str%></td>
                        <td align="center" class="border0010"><%=df.format(taxrate)%></td>
                        <td align="left" class="border0010"><%=remark%></td>
                        <td align="right" class="border0010"><%=taxamt_str%></td>
                        <td class="border0010"><input type="checkbox" value="<%=y%>" name="id" checked disabled></td>
                        <input type="hidden" name="noVoucher" value="<%=noVoucher%>">
                        <input type="hidden" name="taxDate" value="<%=tarikh%>">
                        <input type="hidden" name="txAmt" value="<%=df2.format(taxamt)%>">
                        <input type="hidden" name="taxCode" value="<%=taxcode%>">
                        <input type="hidden" name="taxrate" value="<%=taxrate%>">
                        <input type="hidden" name="taxDescp" value="<%=taxDescp%>">
                        <input type="hidden" name="taxcoacode" value="<%=taxcoacode%>">
                        <input type="hidden" name="taxcoadescp" value="<%=taxcoadescp%>">
                        <input type="hidden" name="gstid" value="<%=gstid%>">
                        <input type="hidden" name="compcode" value="<%=compcode%>">
                        <input type="hidden" name="compname" value="<%=compname%>">
                        <input type="hidden" name="estatecode" value="<%=estatecode%>">
                        <input type="hidden" name="estatename" value="<%=estatename%>">
                        <input type="hidden" name="amt" value="<%=amt%>">
                        <input type="hidden" name="kodVoucher" value="<%=kodVoucher%>">
                        <input type="hidden" name="kodVoucher2" value="<%=kodVoucher2%>">
                        <input type="hidden" name="trxtype" value="<%=output_trans[m]%>">
                        <input type="hidden" name="remark" value="<%=remark%>">
                        <input type="hidden" name="queue" value="<%=queue%>">
                    </tr>
		
               		
               <% } // end while %>
               <% } // END FOR i%>    
                
               <input type="hidden" name="bil" value="<%=y%>">     
                
                <tr style="font-weight:bold">
                	<td colspan="7" class="border0010">TOTAL</td>
                    <td class="border0010" align="right"><%=normalCredit(tot_amtInput)%><input type="hidden" name="totamt" value="<%=tot_amtInput%>"></td>
                    <td align="right" class="border0010">&nbsp;</td>
                    <td align="right" class="border0010">&nbsp;</td>
                    <td align="right" class="border0010"><%=normalCredit(tot_taxInput)%><input type="hidden" name="tottaxamt" value="<%=tot_taxInput%>"></td>
                    <td class="border0010">&nbsp;</td>
                </tr>
            </table>
            
           
            </div>
            </p>
	</div>
</div>

</div><!-- End demo -->


</td>
</tr>

<thead>
<tr><td>
<table cellspacing="0" cellpadding="1" width="95%" align="center">
<tr>
	<td width="15%">Ref. No</td>
	<td width="1%">:</td>
	<td width="15%"><%=refno%><input type="hidden" name="name" value="<%=refno%>" readonly></td>
	<td width="15%">&nbsp;</td>
	<td width="15%">Name</td>
	<td width="1%">:</td>
	<td width="38%"><%=tx_name%><input type="hidden" name="name" value="<%=tx_name%>" readonly></td>
</tr>
<tr>
	<td width="15%">Taxable Period From</td>
	<td width="1%">:</td>
	<td width="15%"><%=tx_period_from%><input type="hidden" name="tax_period_from" value="<%=tx_period_from%>"></td>
	<td width="15%">&nbsp;</td>
	<td width="15%">Taxable Period To</td>
	<td width="1%">:</td>
	<td width="38%"><%=tx_period_to%><input type="hidden" name="tax_period_to" value="<%=tx_period_to%>"></td>
</tr>
<tr>
	<td width="15%">Tax Return Due date</td>
	<td width="1%">:</td>
	<td width="15%"><%=tx_return_date%><input type="hidden" name="tax_return_date" value="<%=tx_return_date%>"></td>
</tr>
</table>	
</td></tr>
</thead>

<tr>
	<td align="center">
    <input type="button" name="back" value="   Back   " onClick="history.go(-1);">
    <input type="button" name="saveRecord" value="   Save   " onClick="submitRecord('<%=refno%>');">
    
    </td>
</tr>
</table>
</form>
 
<br>
<br>
</body>
</html>
<% }else { %>
<div align="center"><br>
Your Web Session Has Expired Please <a href="index.jsp" target="_parent">Login</a> Again To Accuire New Web Session
<div align="center"><br>
To Access The System. 
</div>
</div>
<% } %>