<%-- 
    Document   : om_user_view
    Created on : Sep 14, 2016, 12:15:52 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.administration.om.UserFunction"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoiceItem"%>
<%@page import="com.lcsb.fms.model.administration.om.UserAccount"%>
<%@page import="com.lcsb.fms.dao.administration.om.UserDAO"%>
<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
Module mod = (Module) UserDAO.getModule();
UserAccount v = (UserAccount) UserDAO.getInfo(log,request.getParameter("referenceno"));

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <script type="text/javascript" language="javascript" src="js/bootstrap-treeview.js"></script>
        <title>JSP Page</title>
        <style>
            .invoice-title h2, .invoice-title h3 {
                display: inline-block;
            }

            .table > tbody > tr > .no-line {
                border-top: none;
            }

            .table > thead > tr > .no-line {
                border-bottom: none;
            }

            .table > tbody > tr > .thick-line {
                border-top: 2px solid;
            }
        </style>
        
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
        
        var defaultData = [
            
            <%
            List<Module> clist = (List<Module>) UserDAO.getModuleAccess(log,2,null,v.getStaffId());
            for (Module c : clist) {
            %>
            {
                text: '<%= c.getModuleID() %> - <%= c.getModuleDesc()%>',
                href: '#parent2',
                state: {
                    checked: <%= c.isAccess() %>,
                 //   disabled: true,
                 //   expanded: true,
                 //   selected: true
                  },
                tags: ['0'],
                nodes: [
                    
                    <%
                        List<Module> dlist = (List<Module>) UserDAO.getModuleAccess(log,4,c.getModuleID(),v.getStaffId());
                        for (Module d : dlist) {
                    %> 
                        {
                          text: '<%= d.getModuleID() %> - <%= d.getModuleDesc() %>',
                          href: '#child2',
                          state: {
                            checked: <%= d.isAccess() %>,
                         //   disabled: true,
                         //   expanded: true,
                         //   selected: true
                          },
                          tags: ['0'],
                          nodes: [
                            <%
                            List<Module> elist = (List<Module>) UserDAO.getModuleAccess(log,6,d.getModuleID(),v.getStaffId());
                            for (Module e : elist) {
                            %>
                            {
                              text: '<%= e.getModuleID() %> - <%= e.getModuleDesc() %>',
                              href: '#grandchild1',
                              state: {
                                checked: <%= e.isAccess() %>,
                             //   disabled: true,
                             //   expanded: true,
                             //   selected: true
                              },
                              tags: ['0']
                            },
                            <%}%>
                            
                          ]
                        },
                        <%}%>
                            
                 
                  ]
            },
            <%}%>
          
        ];

        var alternateData = [
          {
            text: 'Parent 1',
            tags: ['2'],
            nodes: [
              {
                text: 'Child 1',
                tags: ['3'],
                nodes: [
                  {
                    text: 'Grandchild 1',
                    tags: ['6']
                  },
                  {
                    text: 'Grandchild 2',
                    tags: ['3']
                  }
                ]
              },
              {
                text: 'Child 2',
                tags: ['3']
              }
            ]
          },
          {
            text: 'Parent 2',
            tags: ['7']
          },
          {
            text: 'Parent 3',
            icon: 'glyphicon glyphicon-earphone',
            href: '#demo',
            tags: ['11']
          },
          {
            text: 'Parent 4',
            icon: 'glyphicon glyphicon-cloud-download',
            href: '/demo.html',
            tags: ['19'],
            selected: true
          },
          {
            text: 'Parent 5',
            icon: 'glyphicon glyphicon-certificate',
            color: 'pink',
            backColor: 'red',
            href: 'http://www.tesco.com',
            tags: ['available','0']
          }
        ];

        var json = '[' +
          '{' +
            '"text": "Parent 1",' +
            '"nodes": [' +
              '{' +
                '"text": "Child 1",' +
                '"nodes": [' +
                  '{' +
                    '"text": "Grandchild 1"' +
                  '},' +
                  '{' +
                    '"text": "Grandchild 2"' +
                  '}' +
                ']' +
              '},' +
              '{' +
                '"text": "Child 2"' +
              '}' +
            ']' +
          '},' +
          '{' +
            '"text": "Parent 2"' +
          '},' +
          '{' +
            '"text": "Parent 3"' +
          '},' +
          '{' +
            '"text": "Parent 4"' +
          '},' +
          '{' +
            '"text": "Parent 5"' +
          '}' +
        ']';
        

        $('#treeview1').treeview({
          data: defaultData
        });

        $('#treeview2').treeview({
          levels: 1,
          data: defaultData
        });

        $('#treeview3').treeview({
          levels: 99,
          data: defaultData
        });

        $('#treeview4').treeview({

          color: "#428bca",
          data: defaultData
        });

        $('#treeview5').treeview({
          color: "#428bca",
          expandIcon: 'glyphicon glyphicon-chevron-right',
          collapseIcon: 'glyphicon glyphicon-chevron-down',
          nodeIcon: 'glyphicon glyphicon-bookmark',
          data: defaultData
        });

        $('#treeview6').treeview({
          color: "#428bca",
          expandIcon: "glyphicon glyphicon-stop",
          collapseIcon: "glyphicon glyphicon-unchecked",
          nodeIcon: "glyphicon glyphicon-user",
          showTags: true,
          data: defaultData
        });

        $('#treeview7').treeview({
          color: "#428bca",
          showBorder: false,
          data: defaultData
        });

        $('#treeview8').treeview({
          expandIcon: "glyphicon glyphicon-stop",
          collapseIcon: "glyphicon glyphicon-unchecked",
          nodeIcon: "glyphicon glyphicon-user",
          color: "yellow",
          backColor: "purple",
          onhoverColor: "orange",
          borderColor: "red",
          showBorder: false,
          showTags: true,
          highlightSelected: true,
          selectedColor: "yellow",
          selectedBackColor: "darkorange",
          data: defaultData
        });

        $('#treeview9').treeview({
          expandIcon: "glyphicon glyphicon-stop",
          collapseIcon: "glyphicon glyphicon-unchecked",
          nodeIcon: "glyphicon glyphicon-user",
          color: "yellow",
          backColor: "purple",
          onhoverColor: "orange",
          borderColor: "red",
          showBorder: false,
          showTags: true,
          highlightSelected: true,
          selectedColor: "yellow",
          selectedBackColor: "darkorange",
          data: alternateData
        });

        $('#treeview10').treeview({
          color: "#428bca",
          enableLinks: true,
          data: defaultData
        });



        var $searchableTree = $('#treeview-searchable').treeview({
          data: defaultData,
        });

        var search = function(e) {
          var pattern = $('#input-search').val();
          var options = {
            ignoreCase: $('#chk-ignore-case').is(':checked'),
            exactMatch: $('#chk-exact-match').is(':checked'),
            revealResults: $('#chk-reveal-results').is(':checked')
          };
          var results = $searchableTree.treeview('search', [ pattern, options ]);

          var output = '<p>' + results.length + ' matches found</p>';
          $.each(results, function (index, result) {
            output += '<p>- ' + result.text + '</p>';
          });
          $('#search-output').html(output);
        }

        $('#btn-search').on('click', search);
        $('#input-search').on('keyup', search);

        $('#btn-clear-search').on('click', function (e) {
          $searchableTree.treeview('clearSearch');
          $('#input-search').val('');
          $('#search-output').html('');
        });


        var initSelectableTree = function() {
          return $('#treeview-selectable').treeview({
            data: defaultData,
            multiSelect: $('#chk-select-multi').is(':checked'),
            onNodeSelected: function(event, node) {
              $('#selectable-output').prepend('<p>' + node.text + ' was selected</p>');
            },
            onNodeUnselected: function (event, node) {
              $('#selectable-output').prepend('<p>' + node.text + ' was unselected</p>');
            }
          });
        };
        var $selectableTree = initSelectableTree();

        var findSelectableNodes = function() {
          return $selectableTree.treeview('search', [ $('#input-select-node').val(), { ignoreCase: false, exactMatch: false } ]);
        };
        var selectableNodes = findSelectableNodes();

        $('#chk-select-multi:checkbox').on('change', function () {
          console.log('multi-select change');
          $selectableTree = initSelectableTree();
          selectableNodes = findSelectableNodes();
        });

        // Select/unselect/toggle nodes
        $('#input-select-node').on('keyup', function (e) {
          selectableNodes = findSelectableNodes();
          $('.select-node').prop('disabled', !(selectableNodes.length >= 1));
        });

        $('#btn-select-node.select-node').on('click', function (e) {
          $selectableTree.treeview('selectNode', [ selectableNodes, { silent: $('#chk-select-silent').is(':checked') }]);
        });

        $('#btn-unselect-node.select-node').on('click', function (e) {
          $selectableTree.treeview('unselectNode', [ selectableNodes, { silent: $('#chk-select-silent').is(':checked') }]);
        });

        $('#btn-toggle-selected.select-node').on('click', function (e) {
          $selectableTree.treeview('toggleNodeSelected', [ selectableNodes, { silent: $('#chk-select-silent').is(':checked') }]);
        });



        var $expandibleTree = $('#treeview-expandible').treeview({
          data: defaultData,
          onNodeCollapsed: function(event, node) {
            $('#expandible-output').prepend('<p>' + node.text + ' was collapsed</p>');
          },
          onNodeExpanded: function (event, node) {
            $('#expandible-output').prepend('<p>' + node.text + ' was expanded</p>');
          }
        });

        var findExpandibleNodess = function() {
          return $expandibleTree.treeview('search', [ $('#input-expand-node').val(), { ignoreCase: false, exactMatch: false } ]);
        };
        var expandibleNodes = findExpandibleNodess();

        // Expand/collapse/toggle nodes
        $('#input-expand-node').on('keyup', function (e) {
          expandibleNodes = findExpandibleNodess();
          $('.expand-node').prop('disabled', !(expandibleNodes.length >= 1));
        });

        $('#btn-expand-node.expand-node').on('click', function (e) {
          var levels = $('#select-expand-node-levels').val();
          $expandibleTree.treeview('expandNode', [ expandibleNodes, { levels: levels, silent: $('#chk-expand-silent').is(':checked') }]);
        });

        $('#btn-collapse-node.expand-node').on('click', function (e) {
          $expandibleTree.treeview('collapseNode', [ expandibleNodes, { silent: $('#chk-expand-silent').is(':checked') }]);
        });

        $('#btn-toggle-expanded.expand-node').on('click', function (e) {
          $expandibleTree.treeview('toggleNodeExpanded', [ expandibleNodes, { silent: $('#chk-expand-silent').is(':checked') }]);
        });

        // Expand/collapse all
        $('#btn-expand-all').on('click', function (e) {
          var levels = $('#select-expand-all-levels').val();
          $expandibleTree.treeview('expandAll', { levels: levels, silent: $('#chk-expand-silent').is(':checked') });
        });

        $('#btn-collapse-all').on('click', function (e) {
          $expandibleTree.treeview('collapseAll', { silent: $('#chk-expand-silent').is(':checked') });
        });



        var $checkableTree = $('#treeview-checkable').treeview({
          data: defaultData,
          levels: 1,
          showIcon: false,
          showCheckbox: true,
          onNodeChecked: function(event, node) {
            
            var mid = node.text;
            mid = mid.substring(0, mid.indexOf(' '));
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=checkmodule&user_id=<%= v.getStaffId() %>&modulechecked="+mid,
                success: function (result) {
                    $('#checkable-output').prepend('<p>' + node.text + ' was checked</p>');
                }
            });
            
            //$.ajax({
             //       data: a,
             //       type: 'POST',
             //       url: "PathController?moduleid="+path+"&process="+process,
            //        success: function (result) {
             //           $('#maincontainer').empty().html(result).hide().fadeIn(300);
             //       }
            //    });
            
          },
          onNodeUnchecked: function (event, node) {
            
            var mid = node.text;
            mid = mid.substring(0, mid.indexOf(' '));
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=uncheckmodule&user_id=<%= v.getStaffId() %>&modulechecked="+mid,
                success: function (result) {
                    $('#checkable-output').prepend('<p>' + node.text + ' was unchecked</p>');
                }
            });
          }
        });

        var findCheckableNodess = function() {
          return $checkableTree.treeview('search', [ $('#input-check-node').val(), { ignoreCase: false, exactMatch: false } ]);
        };
        var checkableNodes = findCheckableNodess();

        // Check/uncheck/toggle nodes
        $('#input-check-node').on('keyup', function (e) {
          checkableNodes = findCheckableNodess();
          $('.check-node').prop('disabled', !(checkableNodes.length >= 1));
        });

        $('#btn-check-node.check-node').on('click', function (e) {
          $checkableTree.treeview('checkNode', [ checkableNodes, { silent: $('#chk-check-silent').is(':checked') }]);
        });

        $('#btn-uncheck-node.check-node').on('click', function (e) {
          $checkableTree.treeview('uncheckNode', [ checkableNodes, { silent: $('#chk-check-silent').is(':checked') }]);
        });

        $('#btn-toggle-checked.check-node').on('click', function (e) {
          $checkableTree.treeview('toggleNodeChecked', [ checkableNodes, { silent: $('#chk-check-silent').is(':checked') }]);
        });

        // Check/uncheck all
        $('#btn-check-all').on('click', function (e) {
          $checkableTree.treeview('checkAll', { silent: $('#chk-check-silent').is(':checked') });
        });

        $('#btn-uncheck-all').on('click', function (e) {
          $checkableTree.treeview('uncheckAll', { silent: $('#chk-check-silent').is(':checked') });
        });



        var $disabledTree = $('#treeview-disabled').treeview({
          data: defaultData,
          onNodeDisabled: function(event, node) {
            $('#disabled-output').prepend('<p>' + node.text + ' was disabled</p>');
          },
          onNodeEnabled: function (event, node) {
            $('#disabled-output').prepend('<p>' + node.text + ' was enabled</p>');
          },
          onNodeCollapsed: function(event, node) {
            $('#disabled-output').prepend('<p>' + node.text + ' was collapsed</p>');
          },
          onNodeUnchecked: function (event, node) {
            $('#disabled-output').prepend('<p>' + node.text + ' was unchecked</p>');
          },
          onNodeUnselected: function (event, node) {
            $('#disabled-output').prepend('<p>' + node.text + ' was unselected</p>');
          }
        });

        var findDisabledNodes = function() {
          return $disabledTree.treeview('search', [ $('#input-disable-node').val(), { ignoreCase: false, exactMatch: false } ]);
        };
        var disabledNodes = findDisabledNodes();

        // Expand/collapse/toggle nodes
        $('#input-disable-node').on('keyup', function (e) {
          disabledNodes = findDisabledNodes();
          $('.disable-node').prop('disabled', !(disabledNodes.length >= 1));
        });

        $('#btn-disable-node.disable-node').on('click', function (e) {
          $disabledTree.treeview('disableNode', [ disabledNodes, { silent: $('#chk-disable-silent').is(':checked') }]);
        });

        $('#btn-enable-node.disable-node').on('click', function (e) {
          $disabledTree.treeview('enableNode', [ disabledNodes, { silent: $('#chk-disable-silent').is(':checked') }]);
        });

        $('#btn-toggle-disabled.disable-node').on('click', function (e) {
          $disabledTree.treeview('toggleNodeDisabled', [ disabledNodes, { silent: $('#chk-disable-silent').is(':checked') }]);
        });

        // Expand/collapse all
        $('#btn-disable-all').on('click', function (e) {
          $disabledTree.treeview('disableAll', { silent: $('#chk-disable-silent').is(':checked') });
        });

        $('#btn-enable-all').on('click', function (e) {
          $disabledTree.treeview('enableAll', { silent: $('#chk-disable-silent').is(':checked') });
        });



        var $tree = $('#treeview12').treeview({
          data: json
        });


        


        



        });
       </script>
    </head>
    <body>
        <div id ="maincontainer">
                
   <div class="partition"> 
       <div class="headofpartition"></div>
       <div class="bodyofpartition">
           
           <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
                    <td valign="middle" align="left">
			<div class="btn-group btn-group-sm" role="group" aria-label="...">
                                
                                
                                <div class="btn-group" role="group" aria-label="...">
                                    <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                                    

                                    
                                    
                                  </div>
                              </div>
                        
		</td>
		</tr>
  </table>
  <br>
       
            <div class="row">
                <div class="col-md-12">
                        <div class="panel-heading">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab1default" data-toggle="tab">Functions</a></li>
                                    <li><a href="#tab2default" data-toggle="tab">Module Accessibility</a></li>
                                    <!--<li class="dropdown">
                                        <a href="#" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#tab4default" data-toggle="tab">Default 4</a></li>
                                            <li><a href="#tab5default" data-toggle="tab">Default 5</a></li>
                                        </ul>
                                    </li>-->
                                </ul>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab1default">
                                
                                    
                                <!--begin tab1default - Bill of Quantities-->    
                                <div class="row">
                                        <div class="col-md-12">
                                                <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <label>Name : <%= v.getStaffName() %> | </label>
                                                            <label>ID : <%= v.getStaffId()%> | <%= v.getLevel() %></label>
                                                        </div>
                                                        <div class="panel-body">
                                                                <div class="table-responsive">
                                                                    <form data-toggle="validator" role="form" id="saveform">
                                                                        <input type="hidden" name="userID" value="<%= v.getStaffId() %>">
                                                                        
                                                                        <table class="table table-condensed">
                                                                                <thead>
                                                                <tr>
                                                                                    <td><strong>No</strong></td>
                                                                                    <td><strong>Function</strong></td>
                                                                                    <td class="text-center"><strong>Access Right</strong></td>
                                                                                    
                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <%
                                                                                        List<UserFunction> listAll = (List<UserFunction>) UserDAO.getFunction(log);
                                                                                          for (UserFunction j : listAll) {
                                                                                            
                                                                                            String disable = "disabled";

                                                                                            if(log.getAccessLevel()==0 || log.getAccessLevel()==1){
                                                                                                disable = "";
                                                                                            }
                                                                                            
                                                                                        %>
                                                                                       
                                                                                    <tr>
                                                                                        <td><%= j.getFunctionId()%></td>
                                                                                        <td><%= j.getFunction()%></td>
                                                                                        <td class="text-center">
                                                                                            <input type="checkbox" name="function" value="<%= j.getFunctionId()%>" <%= UserDAO.getAccess(log,v.getStaffId(), j.getFunctionId()) %> <%= disable %>>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%}%>
                                                                                    <tr>
                                                                                        <td class="no-line"></td>
                                                                                        <td class="no-line"></td>
                                                                                        <td class="no-line text-center">&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="no-line">
                                                                                            <%
                                                                                            if(log.getAccessLevel()==0 || log.getAccessLevel()==1){
                                                                                            
                                                                                            %>
                                                                                            <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="accessright"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                                                                                            <%
                                                                                                }
                                                                                                %>
                                                                                        </td>
                                                                                        <td class="no-line"></td>
                                                                                        <td class="no-line text-center">&nbsp;</td>
                                                                                    </tr>
                                                                                </tbody>
                                                                        </table>
                                                                    </form>
                                                                </div>
                                                        </div>
                                                </div>
                                        </div>
                                    </div>
                                <!--end tab2default - Bill of Quantities--> 
                                
                                    
                                </div>
                                <div class="tab-pane fade" id="tab2default">
                                    
                                    
                                    <div class="row">
                                        <div class="col-sm-6">
                                          <div class="form-group">
                                            <label for="input-check-node" class="sr-only">Search Tree:</label>
                                            <input type="input" class="form-control input-sm" id="input-check-node" placeholder="Search Module...">
                                          </div>
                                          
                                        </div>
                                        
                                        <div class="col-sm-6 bottom">
                                              <button type="button" class="btn btn-default btn-sm" id="btn-check-all">Check All</button>
                                              <button type="button" class="btn btn-default btn-sm" id="btn-uncheck-all">Uncheck All</button>
                                             
                                        </div>
                                        
                                        
                                    </div>
                                    
                                  <div class="row">
                                       
                                        <div class="col-sm-9">
                                          <div id="treeview-checkable" class=""></div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div id="checkable-output">
                                                
                                            </div>
                                        </div>
                                      </div>
                                
                                </div>
                                
                                <div class="tab-pane fade" id="tab4default">Default 4</div>
                                <div class="tab-pane fade" id="tab5default">Default 5</div>
                            </div>
                        </div>
                </div>
                </div>
        <br/>
        </div>
                    </div>
                </div>
    </body>
</html>