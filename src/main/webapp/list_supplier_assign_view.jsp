<%-- 
    Document   : list_supplier_view
    Created on : May 13, 2016, 1:02:13 PM
    Author     : user
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.model.financial.ap.VendorAssign"%>
<%@page import="com.lcsb.fms.util.dao.SupplierDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<div class="list-group"><%
            
            String keyword = request.getParameter("keyword");
            List<VendorAssign> slist = (List<VendorAssign>) SupplierDAO.getSupplierAssign(log, keyword);
            for (VendorAssign c : slist) { 
               %>
       
            <a href="<%= c.getSuppname()%>" id="<%= c.getSuppcode()%>" id1="<%= c.getSuppaddress()%>" id2="<%= c.getAccode()%>" id3="<%= c.getAcdesc()%>" id4="<%= c.getCompanycode()%>" id5="<%= c.getCompanyname()%>" id6="<%= c.getGstid()%>"  title="" class="list-group-item thisresult">
                <div id="left_div"><%= c.getSuppcode()%></div>
                <div id="right_div"><%= c.getSuppname()%></div>
            </a>

               <%    
            } 
        %>
</div>