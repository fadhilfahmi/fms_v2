<%-- 
    Document   : cb_bankreport_preparevoucher
    Created on : Mar 20, 2017, 10:00:24 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.setup.company.CeManageBank"%>
<%@page import="com.lcsb.fms.util.dao.BankDAO"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.BankReportDAO"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    CeManageBank sp = (CeManageBank) BankDAO.getInfo(log,request.getParameter("bankcode"));
    Module mod = (Module) BankReportDAO.getModule();

//String remark = VendorPaymentDAO.getRemark(request.getParameter("sessionid"));
//double total = VendorPaymentDAO.getSumInvoice(request.getParameter("sessionid"));

%>

<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });
        //$('#JVdate').datepicker({ dateFormat: 'yyyy-mm-dd' });

        $('#tarikh').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        });


        $("#total").keyup(function () {
            var val = $(this).val();
            var to = 'amount';
            //alert(val);
            //alert(checkRegexp(val,to));
            if (checkRegexp(val, to)) {
                $('#res_digit').empty();
                $('#div_digit').removeClass('has-error');
                $('#div_digit').addClass('has-success');
                $('#res_digit').append("<i class=\"fa fa-check-circle right-green\"></i>&nbsp;&nbsp;");
                $('#savebutton').prop('disabled', false);
                convertRM(val, to);
            } else {
                $('#res_digit').empty();
                $('#div_digit').removeClass('has-success');
                $('#div_digit').addClass('has-error');
                $('#res_digit').append("<i class=\"fa fa-exclamation-circle fail-red\"></i>&nbsp;&nbsp;Numeric Formatted Only");
                $('#savebutton').prop('disabled', true);
            }
            //
        });

        $('#getbuyer').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Buyer Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_buyer.jsp?name=bname&code=bcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });
        $('#getbank').click(function (e) {

            var a = 'bankcodex';
            var b = 'bankname';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Bank',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_bank.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getcheque').click(function (e) {

            var a = 'cekno';
            var bankcode = $('#bankcode').val();
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Cheque No',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_cheque.jsp?bankcode=' + bankcode + '&cekcolumn=' + a);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#gettype').click(function (e) {
            //e.preventDefault();

            var a = 'paidcode';
            var b = 'paidname';
            var c = 'paidaddress';
            var d = 'paidpostcode';
            var e = 'paidcity';
            var f = 'paidstate';
            var g = 'gstid';

            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Paid Type',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_type.jsp?code=' + a + '&name=' + b + '&address=' + c + '&postcode=' + d + '&city=' + e + '&state=' + f + '&gstid=' + g);
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $("#paymentmode").change(function () {

            var thisval = $(this).val();

            if (thisval == 'Cheque') {
                $('#getcheque').prop('disabled', false);
            } else {
                $('#getcheque').prop('disabled', true);
            }

        });



    });

    var amountto = $('#amount').val();
    $('#total').val(amountto);
    convertRM(amountto, 'amountrm');
    
    $('#year').val($('#curyear').val());
    $('#period').val($('#curperiod').val());

</script>




<form data-toggle="validator" role="form" id="savemaster">
    <input type="hidden" name="prepareid" id="pid" value="<%= log.getUserID()%>">
    <input type="hidden" name="preparename" id="pname" value="<%= log.getFullname()%>">
    <input type="hidden" name="preparedate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
    <input type="hidden" name="coacode" id="coacode" value="">
    <input type="hidden" name="coadesc" id="coadesc" value="">
    <div class="well">
        <div class="row">

            <div class="col-sm-4">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Date &nbsp&nbsp<span class="res_code"></span></label>
                    <input type="text" class="form-control input-sm" id="tarikh" name="tarikh" placeholder="Date of Payment Voucher" autocomplete="off" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">   
                </div>
            </div>
        </div>

    </div><div class="well"><div class="row">
            <div class="col-sm-3">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Paid Type&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                    <div class="form-group-sm input-group">
                        <input type="text" class="form-control input-sm" id="paidtype" name="paidtype" value="Bank" readonly>
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-sm" type="button" id="gettype"><i class="fa fa-cog"></i> Paid Type</button>
                        </span>
                    </div>    
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Paid Code &nbsp&nbsp<span class="res_code"></span></label>
                    <input type="text" class="form-control input-sm" id="paidcode" name="paidcode" value="<%= sp.getCeManageBankPK().getCode()%>" autocomplete="off" >   
                </div>
            </div>
            <div class="col-sm-5">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Paid Name&nbsp&nbsp<span class="res_code"></span></label>
                    <input type="text" class="form-control input-sm" id="paidname" name="paidname" value="<%= sp.getName()%>" autocomplete="off" > 
                </div>
            </div></div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Address &nbsp&nbsp<span class="res_code"></span></label>
                    <textarea class="form-control" rows="4"  id="paidaddress" name="paidaddress">-</textarea>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Postcode &nbsp&nbsp</label>
                    <input type="text" class="form-control input-sm" id="paidpostcode" name="paidpostcode" value="-" autocomplete="off" > 
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" id="">
                    <label for="inputName" class="control-label">City &nbsp&nbsp</label>
                    <input type="text" class="form-control input-sm" id="paidcity" name="paidcity" value="-" autocomplete="off" > 
                </div>
            </div><div class="col-sm-4">
                <div class="form-group" id="">
                    <label for="inputName" class="control-label">State &nbsp&nbsp</label>
                    <input type="text" class="form-control input-sm" id="paidstate" name="paidstate" value="-" autocomplete="off" > 
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">GST ID&nbsp&nbsp<span class="res_code"></span></label>
                    <input type="text" class="form-control input-sm" id="gstid" name="gstid" value="-" autocomplete="off" > 
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Remarks &nbsp&nbsp<span class="res_code"></span></label>
                    <textarea class="form-control" rows="3"  id="remarks" name="remarks">Bank Charges</textarea>
                </div>
            </div>
        </div>

    </div>

    <div class="well"><div class="row">

        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group" id="div_digit">
                    <label for="inputName" class="control-label">RM (Digit) &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                    <input type="text" class="form-control input-sm" id="total" name="amount" value="<%//= total %>" autocomplete="off" > 
                </div>
            </div>
            <div class="col-sm-5">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">RM (In Words) &nbsp&nbsp<span class="res_code"></span></label>
                    <textarea class="form-control" rows="3"  id="amountrm" name="rm"></textarea>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Payment Method &nbsp&nbsp<span class="res_code"></span></label>
                    <select class="form-control input-sm" id="paymentmode" name="paymentmode">
                        <%= ParameterDAO.parameterList(log,"Payment Method", "Cheque")%>
                    </select>
                </div></div></div><div class="row">
            <div class="col-sm-3">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Bank Code &nbsp&nbsp<span class="res_code"></span></label>
                    <div class="form-group-sm input-group">
                        <input type="text" class="form-control input-sm" id="bankcodex" name="bankcode" value="<%= sp.getCeManageBankPK().getCode()%>" autocomplete="off" >
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-sm" id="getbank" title=""><i class="fa fa-cog"></i>&nbsp;&nbsp;Get Bank</button></span>
                    </div></div>
            </div>
            <div class="col-sm-5">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Bank Name&nbsp&nbsp<span class="res_code"></span></label>

                    <input type="text" class="form-control input-sm" id="bankname" name="bankname" value="<%= sp.getName() %>" autocomplete="off" >

                </div>  
            </div>
            <div class="col-sm-4">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Cheque Number&nbsp&nbsp<span class="res_code"></span></label>
                    <div class="form-group-sm input-group">
                        <input type="text" class="form-control input-sm" id="cekno" name="cekno" value="None" autocomplete="off" readonly > 
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-sm" id="getcheque" title=""><i class="fa fa-cog"></i>&nbsp;&nbsp;Get Cheque No.</button></span></div>
                </div>
            </div>
        </div>
    </div>
           
                    <input type="hidden" class="form-control input-sm" id="year" name="year" placeholder="Code" autocomplete="off" value="<%= AccountingPeriod.getCurYear(log)%>"> 
                    <input type="hidden" class="form-control input-sm" id="period" name="period"  autocomplete="off" value="<%= AccountingPeriod.getCurPeriod(log)%>">   
           
                    <input type="hidden" class="form-control input-sm" id="estatecode" name="estatecode" placeholder="0.00" autocomplete="off" value="<%= log.getEstateCode()%>" readonly > 
                    <input type="hidden" class="form-control input-sm" id="estatename" name="estatename" placeholder="0.00" autocomplete="off" value="<%= log.getEstateDescp()%>" readonly> 
           



    <!--<div class="form-group-sm input-group">
    <input type="text" class="form-control input-sm">
    <span class="input-group-btn">
    <button class="btn btn-default btn-sm" type="button"><i class="fa fa-cog"></i> Get Account Code</button>
    </span>
    </div>-->

</form>




