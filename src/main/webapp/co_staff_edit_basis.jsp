<%-- 
    Document   : co_staff_edit_basis
    Created on : Apr 4, 2017, 1:54:34 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.setup.company.staff.Staff"%>
<%@page import="com.lcsb.fms.dao.setup.company.StaffDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Staff v = (Staff) StaffDAO.getInfo(log, request.getParameter("refer"));
    Module mod = (Module) StaffDAO.getModule();

%>

<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#tab_content .datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });


        $('#backtolist').click(function (e) {
            var path = $(this).attr('title');
            var process = $(this).attr('name');
            var refer = $(this).attr('type');
            $.ajax({
                url: "PathController?moduleid=" + path + "&process=" + process + "&refer=" + refer,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#tab_content').empty().html(result).hide().fadeIn(100);
                }
            });
            return false;
        });

        $('.getbank').click(function (e) {

            var a = $(this).attr('id');
            var b = $(this).attr('id1');
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Taxation',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_branch.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });


    });

</script>
<div class="col-sm-6">
    <p></p>
    <div class="row">
        <div class="col-sm-12">
            <button id="backtolist" class="btn btn-default btn-xs" title="<%= mod.getModuleID()%>" name="viewbasic" type="<%= v.getStaffid()%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
            <button id="savebutton"  class="btn btn-default btn-xs" title="<%= mod.getModuleID()%>" name="editbasisprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
            </td>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <div class="well">

                <form data-toggle="validator" role="form" id="saveform">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Staff Info &nbsp&nbsp<span id="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="staffid" name="staffid" value="<%= v.getStaffid()%>" placeholder="Staff ID No" autocomplete="off" required>
                                <p></p>
                                <input type="text" class="form-control input-sm" id="name" name="name" value="<%= v.getName()%>" placeholder="Full Name" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Title<span id="res_code"></span></label>
                                <select class="form-control input-sm" id="title" name="title">
                                    <%= ParameterDAO.parameterList(log, "Title", v.getTitle())%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">New IC No<span id="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="ic" name="ic" placeholder="" value="<%= v.getIc()%>" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Old IC No<span id="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="oldic" name="oldic" placeholder="" value="<%= v.getOldic()%>" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Gender &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="sex" name="sex">
                                    <%= ParameterDAO.parameterList(log, "Gender", v.getSex())%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Date of Birth</label>
                                <div class="form-group-sm input-group">
                                    <input type="text" id="birth" name="birth" class="form-control datepicker" value="<%= v.getBirth()%>" >
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Place of Birth<span id="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="pobirth" name="pobirth" value="<%= v.getPobirth()%>" placeholder="" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Marital Status &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="marital" name="marital">
                                    <%= ParameterDAO.parameterList(log, "Marital Status", v.getMarital())%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Religion &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="religion" name="religion">
                                    <%= ParameterDAO.parameterList(log, "Religion", v.getReligion())%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Race &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="race" name="race">
                                    <%= ParameterDAO.parameterList(log, "Race", v.getRace())%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Citizen &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="citizen" name="citizen">
                                    <%= ParameterDAO.parameterList(log, "Citizen", v.getCitizen())%>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Address</label>
                                <textarea class="form-control" rows="4"  id="address" name="address"><%= v.getAddress()%></textarea>
                                <p></p>
                                <input type="text" class="form-control input-sm" id="postcode" name="postcode" value="<%= v.getPostcode()%>" placeholder="Postcode">
                                <p></p>
                                <input type="text" class="form-control input-sm" id="city" name="city" placeholder="City" value="<%= v.getCity()%>">
                                <p></p>
                                <input type="text" class="form-control input-sm" id="state" name="state" placeholder="State" value="<%= v.getState()%>">
                                <p></p>
                                <input type="text" class="form-control input-sm" id="country" name="country" placeholder="Country">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-sm">
                                <label for="inputName" class="control-label">Contact No.</label>
                                <div class="form-group" id="div_code">
                                    <input type="text" class="form-control input-sm" id="phone" name="phone" placeholder="Phone Number" value="<%= v.getPhone()%>" autocomplete="off" >
                                    <p></p>
                                    <input type="text" class="form-control input-sm" id="hp" name="hp" placeholder="H/P Number" value="<%= v.getHp()%>">
                                    <p></p>
                                    <input type="text" class="form-control input-sm" id="email" name="email" placeholder="Email" value="<%= v.getEmail()%>">
                                    <p></p>
                                    <input type="text" class="form-control input-sm" id="fax" name="fax" placeholder="Fax Number" value="<%= v.getFax()%>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Date Joined</label>
                                <div class="form-group-sm input-group">
                                    <input type="text" id="datejoin" name="datejoin" class="form-control datepicker" value="<%= v.getDatejoin()%>" >
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-sm">
                                <label for="inputName" class="control-label">Active?</label>
                                <select class="form-control input-sm" id="status" name="status">
                                    <%= ParameterDAO.parameterList(log, "Service Status", v.getStatus())%>
                                </select>
                            </div>
                        </div>
                    </div>            
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Remark</label>
                                <textarea class="form-control" rows="4"  id="remarks" name="remarks"><%= v.getRemarks()%></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

