<%-- 
    Document   : tx_input_view
    Created on : Nov 21, 2016, 10:56:05 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.financial.tx.TxInputDetail"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxInput"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxReconcileMaster"%>
<%@page import="com.lcsb.fms.dao.financial.tx.TaxReconcileDAO"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxOutputDetail"%>
<%@page import="com.lcsb.fms.dao.financial.tx.TaxOutputDAO"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxOutput"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxVoucherItem"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxVoucher"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    TxReconcileMaster v = (TxReconcileMaster) TaxReconcileDAO.getRecon(log,request.getParameter("refer"));
    Module mod = (Module) TaxReconcileDAO.getModule();

    String status = TaxReconcileDAO.getStatus(log,v.getRefno());

    String distribute = request.getParameter("distribute");

    String reconview = "";
    if (!TaxReconcileDAO.checkExistDetail(log,v.getRefno())) {
        reconview = "New";
    } else {
        reconview = v.getRefno();
    }
%>
<link href="css/table_style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        window.scrollTo(0, 0);

        callPnotify('<%= request.getParameter("status")%>', '<%= request.getParameter("text")%>');

        $('#print').click(function (e) {
            printElement($("#viewjv").html());
        });

        $('.viewvoucher').click(function (e) {
            var a = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewor&refer=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".editmain").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editmain&referno=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".edititem").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('name');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + b + "&referno=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".deleteitem").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('name');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are you sure to delete?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + b + "&referno=" + a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });

            return false;
        });

        $(".deletemain").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('href');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'All datas for this Official Receipt will be permanently deleted. Are you sure to proceed?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&referno=" + a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            return false;
        });

        $("#addcredit").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addcredit&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });


        $(".addcheque").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addcheque&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $(".adddebitnote").click(function (e) {
            //e.preventDefault();
            var referno = $(this).attr('id');
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Debit Note',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {


                    var $content = $('<body></body>').load('list_ardebitnote.jsp?referno=' + referno);
                    $('body').on('click', '.thisresult-debitnote', function (event) {
                        event.preventDefault();

                        dialog.close();

                    });

                    return $content;
                }
            });

            return false;
        });

        $(".addcredit").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addcredit&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $(".check").click(function (e) {
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=check&referno=" + id,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $('.activerowm').click(function (e) {
            e.preventDefault();
            $(".togglerow").toggle();
            //$(this).toggleClass('info');
            //$("#togglerow_nd"+b).toggleClass('warning');
        });

        $('.activerowx').click(function (e) {
            e.preventDefault();
            var b = $(this).attr('id');
            $(".togglerow_nd" + b).toggle();
            $(this).toggleClass('info');
            $(".togglerow_nd" + b).toggleClass('warning');
        });

        $('.activerowy').click(function (e) {
            e.preventDefault();
            var b = $(this).attr('id');
            $("#togglerow_st" + b).toggle();
            $(this).toggleClass('info');
            $("#togglerow_st" + b).toggleClass('warning');
        });

        $(".approve").click(function () {
            //var a = $("form").serialize();
            var a = $(this).attr('id');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are you sure to Approve this?',
                type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Approve', // <-- Default value is 'OK',
                btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=dist&referno=" + a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            return false;
        });



        $('.chcbox-no').click(function (e) {

            var b = $(this).attr('id');
            //$(this).toggleClass('info');
            // $('#chkbox'+b).prop('checked', true);
            var checkBox = $('#chkbox' + b);
            checkBox.prop("checked", !checkBox.prop("checked"));
            var disno = [];
            var x = 0;
            var j = 0;

            var m = 0;
            var k = 0;

            $.each($("input[name='check-output']:checked"), function () {
                disno.push($(this).val());
            });

            console.log(disno);

            for (i = 0; i < disno.length; i++) {
                j = $('#total_tx' + disno[i]).val();
                //x = j + x;
                x = (parseFloat(j) + parseFloat(x));

                k = $('#total_am' + disno[i]).val();
                //x = j + x;
                m = (parseFloat(k) + parseFloat(m));
                console.log(i + '---' + j);
                //console.log($('#mt_refinery'+i).val());
            }



            $('#total-amt').html(m.toFixed(2));
            $('#total-taxamt').html(x.toFixed(2));
            $('#total_am').val(m.toFixed(2));
            $('#total_tx').val(x.toFixed(2));
            $('#total_txoutput_tab').html(x.toFixed(2));

            $('#net-return').html((parseFloat($('#total_tx').val().replace(/,/g, '')) - parseFloat($('#total_txi').val().replace(/,/g, ''))).toFixed(2));
        });

        $('.chcbox-no-input').click(function (e) {

            var b = $(this).attr('id');
            //$(this).toggleClass('info');
            // $('#chkbox'+b).prop('checked', true);
            var checkBox = $('#chkboxi' + b);
            checkBox.prop("checked", !checkBox.prop("checked"));
            var disno = [];
            var x = 0;
            var j = 0;

            var m = 0;
            var k = 0;

            $.each($("input[name='check-input']:checked"), function () {
                disno.push($(this).val());
            });

            console.log(disno);

            for (i = 0; i < disno.length; i++) {
                j = $('#total_txi' + disno[i]).val();
                //x = j + x;
                x = (parseFloat(j) + parseFloat(x));

                k = $('#total_ami' + disno[i]).val();
                //x = j + x;
                m = (parseFloat(k) + parseFloat(m));
                console.log(i + '---' + j);
                //console.log($('#mt_refinery'+i).val());
            }



            $('#total-amti').html(m.toFixed(2));
            $('#total-taxamti').html(x.toFixed(2));
            $('#total_ami').val(m.toFixed(2));
            $('#total_txi').val(x.toFixed(2));
            $('#total_txinput_tab').html(x.toFixed(2));

            $('#net-return').html((parseFloat($('#total_tx').val().replace(/,/g, '')) - parseFloat($('#total_txi').val().replace(/,/g, ''))).toFixed(2));

        });

        var totalOutput = $('#total_tx').val();
        $('#total_txoutput_tab').html(totalOutput);

        var totalInput = $('#total_txi').val();
        $('#total_txinput_tab').html(totalInput);

        $('#net-return').html((parseFloat(totalOutput.replace(/,/g, '')) - parseFloat(totalInput.replace(/,/g, ''))).toFixed(2));

        $(".deleteall").click(function () {
            //var a = $("form").serialize();
            var a = $(this).attr('id');

            BootstrapDialog.confirm({
                title: '<span style="color:#d9534f;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp; <strong>Confirmation</strong></span>',
                message: 'Are you sure to delete?',
                type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=deletedetail&refer=" + a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            return false;
        });

    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
                </tr>
            </table>
            <br>
            <table class="table table-bordered table-striped  table-hover">
                <thead>
                    <tr>
                        <th colspan="4">Input Tax Information
                            <!--<small><span class="label label-primary">Preparing</span></small>-->
                            <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">

                                <%if (!status.equals("Approved") && TaxReconcileDAO.checkExistDetail(log,v.getRefno())) {
                                        if ((TaxReconcileDAO.isCheck(log,v.getRefno()) && log.getListUserAccess().get(2).getAccess())) {%>
                                <button  class="btn btn-default btn-xs approve" title="<%= mod.getModuleID()%>" id=""><i class="fa fa-check-square-o" aria-hidden="true"></i> Approve</button>
                                <%} else if (!TaxReconcileDAO.isApprove(log,v.getRefno()) && log.getListUserAccess().get(1).getAccess()) {%>

                                <button  class="btn btn-default btn-xs check" title="<%= mod.getModuleID()%>" id="" name="addrefer"><i class="fa fa-check-square-o" aria-hidden="true"></i> Check</button>

                                <%}%>

                                <button id="" class="btn btn-default btn-xs viewvoucher" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i></button>
                                <button id="" class="btn btn-default btn-xs editmain" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                <button id=""  class="btn btn-danger btn-xs deletemain" title="<%= mod.getModuleID()%>" id="" name="delete_st"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                <%} else {%>
                                <button id="" class="btn btn-default btn-xs viewvoucher" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i></button>
                                    <%}%>
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody class="activerowm tdrow">
                    <tr>
                        <th width="15%">Status</th>
                        <td width="35%"></td>
                        <th width="15%">Reference No</th>
                        <td width="35%"><%= v.getRefno()%></td>
                    </tr>
                    <tr>
                        <th>Tax Return Due date</th>
                        <td><%= v.getTaxreturndate()%></td>
                        <th>Name</th>
                        <td><%= v.getName()%></td>
                    </tr>
                    <tr>
                        <th>Taxable Period From</th>
                        <td><%= v.getTaxperiodfrom()%></td>
                        <th>Taxable Period To</th>
                        <td><%= v.getTaxperiodto()%></td>
                    </tr>
                </tbody>
            </table>
                    
                      <%if (TaxReconcileDAO.checkExistDetail(log, v.getRefno())) {%>
            <button id="<%= v.getRefno()%>" class="btn btn-default btn-xs deleteall pull-right" title="<%= mod.getModuleID()%>" name="deleteall">Delete</button>
            <%} else {%>
            <button id="savebutton"  class="btn btn-default btn-xs pull-right" title="<%= mod.getModuleID()%>" name="savereconcile"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
            <%}%>
            <br>
            <form data-toggle="validator" role="form" id="saveform">   
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel-heading">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab1default" data-toggle="tab">Output |  <strong><span id="total_txoutput_tab"></span></strong></a></li>
                                <li><a href="#tab2default" data-toggle="tab">Input |  <strong><span id="total_txinput_tab"></span></strong></a></li>
                                <li class="pull-right"><a style="color:#555;">Net Tax Return |  <strong><span id="net-return"></span></strong></a></li>
                                <!--<li class="dropdown">
                                    <a href="#" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#tab4default" data-toggle="tab">Default 4</a></li>
                                        <li><a href="#tab5default" data-toggle="tab">Default 5</a></li>
                                    </ul>
                                </li>-->
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab1default">


                                    <!--begin tab1default - Bill of Quantities-->      
                                    <table class="table-bordered table-striped table-hover" width="100%">
                                        <thead>

                                            <tr>
                                                <th style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;" width="2%"></th>

                                                <th style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;" width="7%">Trx Date</th>
                                                <th style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;">Refno</th>
                                                <th style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;">Name</th>
                                                <th style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;">Amount</th>
                                                <th style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;">Tax Amount</th>
                                        </thead>
                                        <tbody>
                                        <input type="hidden" class="form-control input-sm" name="txr_refno" value="<%= v.getRefno()%>">

                                        <%
                                            double totalamt = 0.00;
                                            double totaltax = 0.00;

                                            List<TxOutput> listAll = (List<TxOutput>) TaxReconcileDAO.getReconOutput(log, reconview);
                                            for (TxOutput j : listAll) {
                                                List<TxOutputDetail> listItem = (List<TxOutputDetail>) TaxReconcileDAO.getReconOutputAll(log,j.getRefno());
                                                if (!listItem.isEmpty()) {%>
                                        <input type="hidden" class="form-control input-sm" id="total_am<%= j.getRefno()%>" name="total_am<%= j.getRefno()%>" value="<%= TaxReconcileDAO.getSum(log,"amount", "tx_output_detail", j.getRefno())%>">
                                        <input type="hidden" class="form-control input-sm" id="total_tx<%= j.getRefno()%>" name="total_tx<%= j.getRefno()%>" value="<%= TaxReconcileDAO.getSum(log,"taxamt", "tx_output_detail", j.getRefno())%>">

                                        <tr>
                                            <td style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;">
                                                <%
                                                    if (reconview.equals("New")) {
                                                %>   
                                                <input type="checkbox" id="chkbox<%= j.getRefno()%>" name="check-output" class="chcbox-no" value="<%= j.getRefno()%>" checked></td>
                                                <%
                                                    }
                                                %>
                                            <td class="warning activerowx" id="<%= j.getRefno()%>" style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;" colspan="3"><%= j.getRefno()%> - <%= j.getName()%>

                                            </td>
                                            <td class="warning activerowx" id="<%= j.getRefno()%>" style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;" width="15%">
                                                <span class="pull-right"><%= GeneralTerm.currencyFormat(TaxReconcileDAO.getSum(log,"amount", "tx_output_detail", j.getRefno()))%></span>
                                            </td>
                                            <td class="warning activerowx" id="<%= j.getRefno()%>" style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;" width="15%">
                                                <span class="pull-right"><%= GeneralTerm.currencyFormat(TaxReconcileDAO.getSum(log,"taxamt", "tx_output_detail", j.getRefno()))%></span>

                                            </td>

                                        </tr>


                                        <%}
                                            int o = 0;
                                            for (TxOutputDetail i : listItem) {
                                                o++;
                                               
                                                    totalamt = totalamt + i.getAmount();
                                                    totaltax = totaltax + i.getTaxamt();

                                               
                                        %>
                                        <tr class="togglerow_nd<%= j.getRefno()%>" style="display: none">
                                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><%=o%></td>
                                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getDate()%></span></td>
                                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getTxOutputDetailPK().getVoucherno()%></span></td>
                                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getTaxdescp()%></span></td>
                                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;" width="15%"><span class="pull-right"><%= GeneralTerm.normalDebit(i.getAmount())%></span></td>
                                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;" width="15%"><span class="pull-right"><%= GeneralTerm.normalDebit(i.getTaxamt())%> 
                                                    &nbsp;
                                            </td>
                                        </tr>


                                        <%}
                                            }%>

                                        <tr>
                                            <td style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;"></td>
                                            <td style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;"><strong>TOTAL</strong></td>
                                            <td style="font-size: 12px !important; padding: 6px;"></td>
                                            <td style="font-size: 12px !important; padding: 6px;"></td>
                                            <td style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;"><strong><span id="total-amt" class="pull-right"><%= GeneralTerm.normalDebit(totalamt)%></span></strong></td>
                                            <td style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;">
                                                <strong><span id="total-taxamt" class="pull-right"><%= GeneralTerm.normalDebit(totaltax)%></span></strong>
                                                <input type="hidden" id="total_am" name="total_am" value="<%= totalamt%>">
                                                <input type="hidden" id="total_tx" name="total_tx" value="<%= GeneralTerm.normalDebit(totaltax)%>">
                                            </td>
                                        </tr>


                                        </tbody>
                                    </table>

                                    <!--end tab2default - Bill of Quantities--> 


                                </div>
                                <div class="tab-pane fade" id="tab2default">  
                                    <table class="table-bordered table-striped table-hover" width="100%">
                                        <thead>

                                            <tr>
                                                <th style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;" width="2%"></th>

                                                <th style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;" width="7%">Trx Date</th>
                                                <th style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;">Refno</th>
                                                <th style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;">Name</th>
                                                <th style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;">Amount</th>
                                                <th style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;">Tax Amount</th>
                                        </thead>
                                        <tbody>
                                        <input type="hidden" class="form-control input-sm" name="refnoi" value="<%= v.getRefno()%>">

                                        <%
                                            double totalamt1 = 0.00;
                                            double totaltax1 = 0.00;

                                            List<TxInput> listAll1 = (List<TxInput>) TaxReconcileDAO.getReconInput(log,reconview);
                                            for (TxInput j : listAll1) {
                                                List<TxInputDetail> listItem = (List<TxInputDetail>) TaxReconcileDAO.getReconInputAll(log,j.getRefno());
                                                if (!listItem.isEmpty()) {%>
                                        <input type="hidden" class="form-control input-sm" id="total_ami<%= j.getRefno()%>" name="total_ami<%= j.getRefno()%>" value="<%= TaxReconcileDAO.getSum(log,"amount", "tx_input_detail", j.getRefno())%>">
                                        <input type="hidden" class="form-control input-sm" id="total_txi<%= j.getRefno()%>" name="total_txi<%= j.getRefno()%>" value="<%= TaxReconcileDAO.getSum(log,"taxamt", "tx_input_detail", j.getRefno())%>">

                                        <tr>
                                            <td style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;">
                                                <%
                                                    if (reconview.equals("New")) {
                                                %>   
                                                <input type="checkbox" id="chkboxi<%= j.getRefno()%>" name="check-input" class="chcbox-no-input" value="<%= j.getRefno()%>" checked></td>
                                                <%
                                                    }
                                                %>

                                            <td class="warning activerowx" id="<%= j.getRefno()%>" style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;" colspan="3"><%= j.getRefno()%> - <%= j.getName()%>

                                            </td>
                                            <td class="warning activerowx" id="<%= j.getRefno()%>" style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;" width="15%">
                                                <span class="pull-right"><%= GeneralTerm.currencyFormat(TaxReconcileDAO.getSum(log,"amount", "tx_input_detail", j.getRefno()))%></span>
                                            </td>
                                            <td class="warning activerowx" id="<%= j.getRefno()%>" style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;" width="15%">
                                                <span class="pull-right"><%= GeneralTerm.currencyFormat(TaxReconcileDAO.getSum(log,"taxamt", "tx_input_detail", j.getRefno()))%></span>

                                            </td>

                                        </tr>


                                        <%}
                                            int o = 0;
                                            for (TxInputDetail i : listItem) {
                                                o++;
                                                totalamt1 = totalamt1 + i.getAmount();
                                                totaltax1 = totaltax1 + i.getTaxamt();
                                        %>
                                        <tr class="togglerow_nd<%= j.getRefno()%>" style="display: none">
                                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><%=o%></td>
                                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getDate()%></span></td>
                                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getTxInputDetailPK().getVoucherno()%></span></td>
                                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getTaxdescp()%></span></td>
                                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;" width="15%"><span class="pull-right"><%= GeneralTerm.normalCredit(i.getAmount())%></span></td>
                                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;" width="15%"><span class="pull-right"><%= GeneralTerm.normalCredit(i.getTaxamt())%> 
                                                    &nbsp;
                                            </td>
                                        </tr>


                                        <%}
                                            }%>

                                        <tr>
                                            <td style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;"></td>
                                            <td style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;"><strong>TOTAL</strong></td>
                                            <td style="font-size: 12px !important; padding: 6px;"></td>
                                            <td style="font-size: 12px !important; padding: 6px;"></td>
                                            <td style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;"><strong><span id="total-amti" class="pull-right"><%= GeneralTerm.normalCredit(totalamt1)%></span></strong></td>
                                            <td style="font-size: 12px !important; padding: 6px; padding-left: 6px; padding-right: 6px;">
                                                <strong><span id="total-taxamti" class="pull-right"><%= GeneralTerm.normalCredit(totaltax1)%></span></strong>
                                                <input type="hidden" id="total_ami" name="total_ami" value="<%= totalamt1%>">
                                                <input type="hidden" id="total_txi" name="total_txi" value="<%= GeneralTerm.normalCredit(totaltax1)%>">
                                            </td>
                                        </tr>


                                        </tbody>
                                    </table>

                                </div>

                                <div class="tab-pane fade" id="tab4default">Default 4</div>
                                <div class="tab-pane fade" id="tab5default">Default 5</div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>    
