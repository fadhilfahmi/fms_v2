<%-- 
    Document   : ar_print_report_aging
    Created on : Mar 30, 2017, 12:26:09 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArReportAging"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArReportAgingParam"%>
<%@page import="com.lcsb.fms.dao.financial.ar.ArReportDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) ArReportDAO.getModule();
    ArReportAgingParam prm = (ArReportAgingParam) session.getAttribute("ar_aging_param");
    List<ArReportAging> listx = (List<ArReportAging>) ArReportDAO.getAllAging(log,prm, 0, 0, false);


%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#print').click(function (e) {
            printReport($("#viewprint").html());
        });

        $(".goto").click(function () {
            var link = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + link,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


    });
</script>
<link href="css/table_style.css" rel="stylesheet" type="text/css" />
<link href="css/pvoucher.css" rel="stylesheet" type="text/css" />
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="aging"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="print"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addinv" ><i class="fa fa-floppy-o"></i>&nbsp;Print</button>
                    </td>
                </tr>
            </table>
            <br>
            <div id="viewprint">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead
                    <tr>
                        <td colspan="7">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="50px"><img src="image/logo_lkpp.jpg" width="40px" style="vertical-align:middle" ></td>
                                    <td><span class="bigtitlebold"><%= log.getEstateDescp()%></span><span class="pull-right"></span>
                                        <p class="font_inheader_normal">BUYER AGING ANALYSIS</p>
                                        <p class="font_inheader_normal">AGING DATE <%= prm.getDate() %></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" class="border_bottom"></td>
                    </tr>
                    <tr>
                        <td width="15%"><p class="column_head_style">Buyer Code</p></td>
                        <td width="35%"><p class="column_head_style">Buyer Name</p></td>
                        <td width="10%" align="right"><p class="column_head_style"> <=30 Days</p></td>
                        <td width="10%" align="right"><p class="column_head_style">31-60 Days</p></td>
                        <td width="10%" align="right"><p class="column_head_style">61-90 Days</p></td>
                        <td width="10%" align="right"><p class="column_head_style">91-120 Days</p></td>
                        <td width="10%" align="right"><p class="column_head_style"> >120 Days</p></td>
                    </tr>
                    </thead>
                    <tbody>
                    <%

                        for (ArReportAging j : listx) {
                    %>
                    <tr>
                        <td><span class="title_row_font_sub"><%= j.getBuyerCode()%></span></td>
                        <td><span class="title_row_font_sub"><%= j.getBuyerName()%></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getDayCount1()) %></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getDayCount2()) %></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getDayCount3()) %></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getDayCount4()) %></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getDayCount5()) %></span></td>
                    </tr>
                  
                        <%} %>
                    <tr>
                        <td>&nbsp;<span class="title_row_font_main"></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"></span></td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td>&nbsp;<span class="title_row_font_main"></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"></span></td>
                        <td>&nbsp;</td>
                    </tr>
                    <!--<tr>
                        <td>&nbsp;<span class="total_row_font_bold">NET CURRENT ASSET</span></td>
                        <td align="right" >&nbsp;<span class="total_row_font_bold"></span></td>
                        <td align="right" >&nbsp;<span class="total_row_font_bold"></td>
                    </tr>-->

                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
</div>