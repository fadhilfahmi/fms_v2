<%-- 
    Document   : cf_contractor_add
    Created on : Apr 21, 2016, 8:10:10 AM
    Author     : user
--%>

<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ContractorDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) ContractorDAO.getModule();

%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/formValidation.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#getbank').click(function (e) {

            var a = 'bankcode';
            var b = 'bankname';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Taxation',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_bank.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getvendor').click(function (e) {

            var a = 'code';
            var b = 'vendor_name';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Vendor',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {

                    var $content = $('<body></body>').load('list_vendor.jsp?code=vendor&name=companyname&registerno=register&bumiputra=bumiputra&ownername=ownername&address=address&city=city&postcode=postcode&state=state&title=title&position=position&hp=hp&phone=phone&fax=fax&email=email&bankname=bankname&bankaccount=bankaccount&payment=payment&remarks=remarks');
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        /*$('#saveform').formValidation({
         message: 'This value is not valid',
         icon: {
         valid: 'glyphicon glyphicon-ok',
         invalid: 'glyphicon glyphicon-remove',
         validating: 'fa fa-refresh'
         },
         fields: {
         
         code: {
         required:true,
         validators: {
         notEmpty: {
         message: 'The Account Code is required'
         },
         digits: {
         message: 'The value can contain only digits'
         }
         }
         }
         }
         });*/
        

    });

</script>
<div id ="maincontainer">
    <div class="partition">
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                        <button id="getvendor"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="getvendor"><i class="fa fa-search-plus" aria-hidden="true"></i>&nbsp;Get from Vendor Information</button>
                    </td>
                </tr>
            </table>
            <br>

            <form data-toggle="validator" role="form" id="saveform">
                <input type="hidden" name="prepareid" id="pid" value="<%= log.getUserID()%>">
                <input type="hidden" name="preparename" id="pname" value="<%= log.getFullname()%>">
                <input type="hidden" name="preparedate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Code &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="code" name="code" value="<%= AutoGenerate.getNewCode("contractor_info", "suppliercode")%>" autocomplete="off" required readonly>  
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Company Name &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="companyname" name="companyname"  autocomplete="off">  
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Vendor &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="vendor" name="vendor" autocomplete="off" >   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Register &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="register" name="register"  autocomplete="off">  
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Bumiputra &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="bumiputra" name="bumiputra">
                                    <%= ParameterDAO.parameterList(log,"YesNo Type", "")%>
                                </select> 
                            </div>
                        </div>

                    </div>
                </div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Owner Name &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="ownername" name="ownername" autocomplete="off" >   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Owner NRIC No. &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="owneric" name="owneric" autocomplete="off" value="">   
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Nationality &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="nationality" name="nationality" autocomplete="off" >   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Class &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="kelas" name="kelas" autocomplete="off" value="">   
                            </div>
                        </div></div>
                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Account Code&nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="coa" name="coa">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="coa" id1="coadescp"><i class="fa fa-cog"></i> Account</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Account Description&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="coadescp" name="coadescp" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                    </div></div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Title &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="contacttitle" name="contacttitle">
                                    <%= ParameterDAO.parameterList(log,"Title", "")%>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Contact Name &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="contactperson" name="contactperson" autocomplete="off" >   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Position &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="contactposition" name="contactposition">
                                    <%= ParameterDAO.parameterList(log,"Position", "")%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Handphone No. &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="hp" name="hp" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Phone No. &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="phone" name="phone" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Fax No. &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="fax" name="fax" placeholder="" autocomplete="off" > 
                            </div></div></div><div class="row">
                        <div class="col-sm-6">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Email &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="email" name="email" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">URL Address &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="url" name="url" placeholder="" autocomplete="off" > 
                            </div>
                        </div></div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Address &nbsp&nbsp<span class="res_code"></span></label>
                                <textarea class="form-control" rows="4"  id="address" name="address"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">City &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="city" name="city" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">State &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="state" name="state" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Postcode &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="postcode" name="postcode" placeholder="" autocomplete="off" > 
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Country &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="country" name="country" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                    </div>
                </div>



                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Bank&nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="bankcode" name="bank" placeholder="" autocomplete="off" >
                                    <span class="input-group-btn">


                                        <button class="btn btn-default btn-sm" id="getbank" title=""><i class="fa fa-cog"></i>Bank List</button></span>
                                </div>  
                            </div>
                        </div><div class="col-sm-9">
                            <label for="inputName" class="control-label">Bank Description &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                            <input type="text" class="form-control input-sm" id="bankname" name="bankdesc" placeholder="" autocomplete="off" > 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Bank Account&nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="bankaccount" name="bankaccount" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Payment Method &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="payment" name="payment">
                                    <%= ParameterDAO.parameterList(log,"Payment Method", "")%>
                                </select> 
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Remarks &nbsp&nbsp<span class="res_code"></span></label>
                                <textarea class="form-control" rows="3"  id="remarks" name="remarks"></textarea>
                            </div>
                        </div></div></div>

                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Deposit Code&nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="depositcoa" name="depositcoa">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="depositcoa" id1="depositcoadescp"><i class="fa fa-cog"></i> Deposit</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Deposit Description&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="depositcoadescp" name="depositcoadescp" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Retention &nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="retention" name="retention">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="retention" id1="retentiondescp"><i class="fa fa-cog"></i> Retention</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label"> Retention Description &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="retentiondescp" name="retentiondescp" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Permit Code &nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="permitcode" name="permitcode">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="permitcode" id1="permitdesc"><i class="fa fa-cog"></i>Permit Code</button>
                                    </span>
                                </div>  
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label"> Permit Description &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="permitdesc" name="permitdesc" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

