<%-- 
    Document   : gl_accountperiod_set
    Created on : Mar 15, 2016, 8:51:26 AM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.model.financial.gl.AccountPeriod"%>
<%@page import="com.lcsb.fms.dao.financial.gl.AccountPeriodDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
AccountPeriod view = (AccountPeriod) AccountPeriodDAO.getCurPeriod(log);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <title>JSP Page</title>
    </head>
    <body>
        <div class="well">
  
<form data-toggle="validator" role="form" id="saveform">
    <div class="row self-row">
        <label for="inputName" class="control-label">Current Period &nbsp&nbsp<span id="res_code"></span></label>
    </div>
    
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= view.getStartperiod() %>" readonly>
            </div>   
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= view.getEndperiod() %>" readonly>
            </div>   
        </div>
        
    </div>
            
    <div class="row self-row">
        <label for="inputName" class="control-label">Set New &nbsp&nbsp<span id="res_code"></span></label>
    </div>
    
    <div class="row">
        <div class="col-sm-2">
            <div class="form-group">
                <select class="form-control input-sm" id="newyear" name="newyear">
                    <%= ParameterDAO.parameterList(log,"Year", AccountingPeriod.getCurYear(log)) %>
                </select>
            </div>   
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <select class="form-control input-sm" id="newperiod" name="newperiod">
                    <%= ParameterDAO.parameterList(log,"Period", AccountingPeriod.getCurPeriod(log)) %>
                </select>
            </div>   
        </div>        
        <div class="col-sm-4">
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= view.getStartperiod() %>">
            </div>   
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= view.getEndperiod() %>">
            </div>   
        </div>
        
    </div>        
    <!--<div class="form-group-sm input-group">
    <input type="text" class="form-control input-sm">
    <span class="input-group-btn">
    <button class="btn btn-default btn-sm" type="button"><i class="fa fa-cog"></i> Get Account Code</button>
    </span>
    </div>-->
    
</form>
</div>
    </body>
</html>
