<%-- 
    Document   : ap_pv__list
    Created on : May 10, 2016, 12:38:55 PM
    Author     : user
--%>

<%@page import="com.lcsb.fms.dao.financial.ap.VendorDebitNoteDAO"%>
<%@page import="com.lcsb.fms.util.model.ListTable"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.ModuleItem"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.ConnectionModel"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,java.util.*" errorPage="" isThreadSafe="false" %>
<%@ page import="java.util.Date"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
String year = AccountingPeriod.getCurYear(log);
String period = AccountingPeriod.getCurPeriod(log);
Module mod = (Module) VendorDebitNoteDAO.getModule();

response.setHeader("Cache-Control", "no-cache");

SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
DecimalFormat dcf=new DecimalFormat("###,###,###,###,###,###,##0.00");
String [] dat =new String [17];
String date = formatter.format(new Date());
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>IPAMIS</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/tether.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>

<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
 <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
    
				
var updateTable = '<%= mod.getMainTable() %>';

<%
List<ListTable> mlist = (List<ListTable>) VendorDebitNoteDAO.tableList();
for (ListTable m : mlist) {
    //if(m.getList_View().equals("1")){
        %>
        columntoView.push('<%= m.getColumnName()%>');
        columnName.push('<%= m.getTitleName()%>');
        <%    
    //}
    
}
%>
		
var colsTosend = 'cols=';
for (m = 0; m < columntoView.length; ++m) {//put value from database to variable jget
if(m == columntoView.length - 1){
colsTosend += columntoView[m];
}else{
colsTosend += columntoView[m]+'&cols=';
}
}


	  $( "#add-new" ).click(function() {
                                        $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=addnew",
                                            success: function (result) {
                                            // $("#haha").html(result);
                                            $('#herey').empty().html(result).hide().fadeIn(300);
                                        }});
                                        return false;
	  });

	  
          $( "#check-button" ).click(function() {
                $.ajax({
                    url: "PathController?moduleid=<%= mod.getModuleID() %>&process=check",
                    success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
                return false;
	  });

	
	  var orderby = 'order by voucherno desc';
	  var oTable = $('#example').dataTable( {
              "responsive": true,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "retrievetable.jsp?table="+updateTable+"&code=<%= mod.getReferID_Master() %>&"+colsTosend+"&order="+orderby,
		"aoColumns": [
		  {"sTitle": columnName[0], "mData": columntoView[0], "sWidth": "17%", "bSearchable": true},
		  {"sTitle": columnName[1], "mData": columntoView[1], "sWidth": "9%"},
		  {"sTitle": columnName[2], "mData": columntoView[2], "sWidth": "15%"},
                  {"sTitle": columnName[3], "mData": columntoView[3], "sWidth": "15%"},
                  //{"sTitle": columnName[4], "mData": columntoView[4], "sWidth": "15%"},
		  {"sTitle": columnName[4], "mData": columntoView[4], "sWidth": "15%", "bVisible": false},
		  {"sTitle": columnName[5], "mData": columntoView[5], "sWidth": "15%", "bVisible": false},
		  {"sTitle": columnName[6], "mData": columntoView[6], "sWidth": "15%", "bVisible": false},
		  {"sTitle": "Status", "mData": null, "sWidth": "10%"},
		  {"sTitle": "Action", "mData": null, "bSortable": false, "bSearchable": false, "sWidth": "20%"}
			
			],
		  "aoColumnDefs": [ {
			"aTargets": [8],
			  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
			  	
				//var a = $(icon('addacc',oData.checkbyid,oData.approvebyid,oData.postflag));
				//var b = $('<img src="image/icon/view.png" title="View" width="22" style="vertical-align:middle;cursor:pointer">');
				//var c = $('<img src="image/icon/edit_2.png" title="Edit" width="22" style="vertical-align:middle;cursor:pointer">');
				//var c = $(icon('edit',oData.checkbyid,oData.approvebyid,oData.postflag));
				//var e = $(icon('delete',oData.checkbyid,oData.approvebyid,oData.postflag));
				//var f = $(icon('post',oData.checkbyid,oData.approvebyid,oData.postflag));
				//var g = $(icon('cancel',oData.checkbyid,oData.approvebyid,oData.postflag));
                                
                                var c = $(icon('edit',oData.checkid,oData.appid,oData.post));
				var e = $(icon('delete',oData.checkid,oData.appid,oData.post));
				var f = $(icon('post',oData.checkid,oData.appid,oData.post));
                                var a = $(icon('adddetail',oData.checkid,oData.appid,oData.post));
                                var b = $('<button type="button" class="btn btn-default btn-xs"><i class="fa fa-folder-open"></i></button>');
                                var k = $(icon('check',oData.checkid,oData.appid,oData.post));
                                var g = $(icon('cancel',oData.checkid,oData.appid,oData.post));
				
				a.on('click', function() {
				  //console.log(oData);
				  if(iconClick('edit',oData.checkid,oData.appid,oData.post)==0){
				  	//addSubform(oData.JVrefno,subTable,'<%= mod.getModuleID() %>',module_abb,oTable);
					//addDebitCredit(oData.JVrefno);
                                        
                                        $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=addnewitem&refer="+oData.refer,
                                            success: function (result) {
                                            // $("#haha").html(result);
                                            $('#herey').empty().html(result).hide().fadeIn(300);
                                        }});
                                        return false;
				  }
				  
				  return false;
				});
				b.on('click', function() {
				  //console.log(oData);
				  $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=viewpv&refer="+oData.refer,
                                            success: function (result) {
                                            // $("#haha").html(result);
                                            $('#herey').empty().html(result).hide().fadeIn(300);
                                        }});
                                        return false;
				});
				c.on('click', function() {
				  //console.log(oData);
				  if(iconClick('edit',oData.checkid,oData.appid,oData.post)==0){
				  	//editRowx(oData.JVrefno,updateTable,subTable,<%= mod.getModuleID() %>,module_abb,oTable);
                                        
                                        $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=editpv&refer="+oData.refer,
                                            success: function (result) {
                                            // $("#haha").html(result);
                                            $('#herey').empty().html(result).hide().fadeIn(300);
                                        }});
                                        return false;
				  }
				  
				  //$("#dialog-form-update").dialog("open");
				  //$( "#dialog-form-update" ).dialog( "open" );
				  return false;
				});
				e.on('click', function() {
				  //console.log(oData);
				  if(iconClick('delete',oData.checkid,oData.appid,oData.post)==0){
				  	//deleteRow(oData.JVrefno);
                                        
                                        BootstrapDialog.confirm({
                                            title: 'Confirmation',
                                            message: 'Are you sure to delete?',
                                            type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                            closable: true, // <-- Default value is false
                                            draggable: true, // <-- Default value is false
                                            btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                            btnOKLabel: 'Delete', // <-- Default value is 'OK',
                                            btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                                            callback: function(result) {
                                                // result will be true if button was click, while it will be false if users close the dialog directly.
                                                if(result) {
                                                    $.ajax({
                                                        url: "PathController?moduleid=<%= mod.getModuleID() %>&process=delete&refer="+oData.refer,
                                                        success: function (result) {
                                                        // $("#haha").html(result);
                                                        setTimeout(function(){
                                                        oTable.fnClearTable();}, 500);
                                                    }});
                                                }
                                            }
                                        });
                                        
                                        
                                        return false;
				  }
				  
				  return false;
				});
				f.on('click', function() {
				  //console.log(oData);
				  if(iconClick('post',oData.checkid,oData.appid,oData.post)==0){
				  	//postThis(oData.JVrefno);
                                        $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=post&referno="+oData.invref,
                                            success: function (result) {
                                            // $("#haha").html(result);
                                            $('#herey').empty().html(result).hide().fadeIn(300);
                                        }});
				  }
				 
				  return false;
				});
				g.on('click', function() {
				  //console.log(oData);
				  if(iconClick('cancel',oData.checkid,oData.appid,oData.post)==0){
				  	cancel(oData.JVrefno);
				  }
				  
				  return false;
				});
                                
                                k.on('click', function() {
				  //console.log(oData);
				  if(iconClick('delete',oData.checkid,oData.appid,oData.post)==0){
				  	//deleteRow(oData.JVrefno);
                                        
                                        BootstrapDialog.confirm({
                                            title: 'Confirmation',
                                            message: 'Are you sure to delete?',
                                            type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                            closable: true, // <-- Default value is false
                                            draggable: true, // <-- Default value is false
                                            btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                            btnOKLabel: 'Delete', // <-- Default value is 'OK',
                                            btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                                            callback: function(result) {
                                                // result will be true if button was click, while it will be false if users close the dialog directly.
                                                if(result) {
                                                    $.ajax({
                                                        url: "PathController?moduleid=<%= mod.getModuleID() %>&process=delete&referno="+oData.invref,
                                                        success: function (result) {
                                                        // $("#haha").html(result);
                                                        setTimeout(function(){
                                                        oTable.fnClearTable();}, 500);
                                                    }});
                                                }
                                            }
                                        });
                                        
                                        
                                        return false;
				  }
				  
				  return false;
				});
					  
				                
                                $(nTd).empty();
				$(nTd).attr("class",'btn-group');
                                $(nTd).attr("align",'right');
                                $(nTd).prepend(a,f,b,c,k,e,g);
			  }
			 
		  },

		  {
			"aTargets": [7],//special rules for determine a status
			  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
				var stat = 'Preparing';

				if(oData.checkid.length > 0){
				  stat = 'Checked';
				}
				if(oData.appid.length > 0){
				  stat = 'Approved';
				}
				if(oData.post == 'posted'){
				  stat = 'Posted';
				}
				if(oData.post == 'Cancel'){
				  stat = 'Cancelled';
				}

				$(nTd).empty();
				//$(nTd).attr("id",'btntest');
					$(nTd).prepend(stat);
			  }
			 
		  } ]

		  
		  
		} );


} );

</script>

<body>
    <div id ="maincontainer">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="74%" class="borderbot" align="left"><span class="bigfonttitle"><%= mod.getModuleDesc() %></span>&nbsp;&nbsp;<span class="midfonttitle"> List of <%= mod.getModuleDesc() %></span></td>
                <td width="26%" class="borderbot" align="right">&nbsp;</td>
            </tr>
        </table>
        <br>
        <div class="partition"> 
            <div class="headofpartition"></div>
            <div class="bodyofpartition">
                <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                    <tr>
                        <td valign="middle" align="left">
                            <button id="add-new" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> Add <%= mod.getModuleDesc() %></button>
                             </td>
                    </tr>
                </table>
                        <hr>       
                <div class="dataTable_wrapper"> 
                    <table class="table table-striped table-bordered table-hover" id="example">
                    </table>
                </div>
                       
            </div>
        </div>
    </div>
</body>
</html>
  
