<%-- 
    Document   : gl_debitnotereceive_view
    Created on : Nov 17, 2016, 4:19:53 PM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.fms.model.financial.gl.GLAccCredit"%>
<%@page import="com.lcsb.fms.model.financial.gl.GLCreditNote"%>
<%@page import="com.lcsb.fms.dao.financial.gl.CreditNoteDAO"%>
<%@page import="com.lcsb.fms.model.financial.gl.GLItemCreditNote"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
GLItemCreditNote vi = (GLItemCreditNote) CreditNoteDAO.getCNEitemEst(log,request.getParameter("refer"));
GLCreditNote v = (GLCreditNote) CreditNoteDAO.getCNEest(log,request.getParameter("refer"));
GLAccCredit vx = (GLAccCredit) CreditNoteDAO.getAccCreditEst(log,request.getParameter("refer"));
Module mod = (Module) CreditNoteDAO.getModule("mill");

String status = CreditNoteDAO.getStatusEst(log,v.getNoteno());
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        window.scrollTo(0,0);
        
        callPnotify('<%= request.getParameter("status") %>','<%= request.getParameter("text") %>');
        
        
        $('#print').click(function(e){
            printElement($("#viewjv").html());
        });
        
        $('.viewvoucher').click(function(e){
            var a = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=viewformatted&refer="+a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
	});
        
        $('.gotoJV').click(function(e){
            var a = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=gotoJV&referno="+a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
	});
            
        
              
        $( ".deletemain" ).click(function() {
            var a = $(this).attr('id');
            var b = $(this).attr('href');
                 
            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'All datas for this Official Receipt will be permanently deleted. Are you sure to proceed?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function(result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if(result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=delete&referno="+a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            return false;
        });
        
        $( ".send" ).click(function() {
            var a = $(this).attr('id');
            var b = $(this).attr('href');
                 
            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'This Debit Note will be send and generate Journal Voucher. Are you sure to proceed?',
                type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Proceed', // <-- Default value is 'OK',
                btnOKClass: 'btn-primary', // <-- If you didn't specify it, dialog type will be used,
                callback: function(result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if(result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=generateJV&redirect=detail&referno="+a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            return false;
        });
              
           
        $('.activerowm').click(function(e){
            e.preventDefault();
            $(".togglerow").toggle();
            //$(this).toggleClass('info');
            //$("#togglerow_nd"+b).toggleClass('warning');
        });

        $('.activerowx').click(function(e){
            e.preventDefault();
            var b = $(this).attr('id');
            $("#togglerow_nd"+b).toggle();
            $(this).toggleClass('info');
            $("#togglerow_nd"+b).toggleClass('warning');
        });
                
        $('.activerowy').click(function(e){
            e.preventDefault();
            var b = $(this).attr('id');
            $("#togglerow_st"+b).toggle();
            $(this).toggleClass('info');
            $("#togglerow_st"+b).toggleClass('warning');
        });
        
        $( ".approve" ).click(function() {
            //var a = $("form").serialize();
            var a = $(this).attr('id');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are you sure to Approve this?',
                type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Approve', // <-- Default value is 'OK',
                btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
                callback: function(result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if(result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=dist&referno="+a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            return false;
        });
              
    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
		</tr>
            </table>
            <br>
            <table class="table table-bordered table-striped  table-hover">
                <thead>
                    <tr>
                        <th colspan="4">Voucher Information
                        <!--<small><span class="label label-primary">Preparing</span></small>-->
                            <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
                            
                            <%if(!status.equals("JV Created")){%>
                                
                                <button  class="btn btn-default btn-xs send" title="<%= mod.getModuleID() %>" id="<%= v.getNoteno()%>"><i class="fa fa-paper-plane" aria-hidden="true"></i> Create JV</button>
                                <button id="<%= v.getNoteno() %>"  class="btn btn-danger btn-xs deletemain" title="<%= mod.getModuleID() %>" id="<%//= pvi.getRefer() %>" name="delete_st"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                
                            <%}else{%>
                                <button  class="btn btn-default btn-xs gotoJV" title="<%= mod.getModuleID() %>" id="<%= v.getJVno()%>"><%= v.getJVno() %></button>
                            <%}%>
                                
                                <button id="<%= v.getNoteno() %>" class="btn btn-default btn-xs viewvoucher" title="<%= mod.getModuleID() %>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i></button>
                              
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody class="activerowm tdrow">
                    <tr>
                        <th width="15%">Status</th>
                        <td width="35%"><%= GeneralTerm.getStatusLabel(CreditNoteDAO.getStatusEst(log,v.getNoteno())) %></td>
                        <th width="15%">Note No</th>
                        <td width="35%"><%= v.getNoteno()%></td>
                    </tr>
                    <tr>
                        <th>Sender</th>
                        <td><%= v.getEstcode()%> - <%= v.getEstname()%></td>
                        <th>Receiver</th>
                        <td><%= v.getReceivercode()%> - <%= v.getReceivername()%></td>
                    </tr>
                    <tr>
                        <th>Sender Account</th>
                        <td><%= v.getAcccode()%> - <%= v.getAccdesc() %></td>
                        <th>Receiver Account</th>
                        <td><%= vx.getAcccode()%> - <%= vx.getAccdesc()%></td>
                    </tr>
                    <tr>
                        <th>Date</th>
                        <td><%= v.getNotedate()%></td>
                        <th>Period</th>
                        <td><%= v.getYear() %>, <%= v.getPeriod() %></td>
                    </tr>
                    <tr>
                        <th>Total Amount</th>
                        <td>RM<%= GeneralTerm.currencyFormat(v.getTotal()) %></td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Remark</th>
                        <td colspan="3"><%= v.getRemark()%></td>
                    </tr>
                    
                </tbody>
            </table>
                    
            
                
            <table class="table table-bordered table-striped table-hover" id="dataTables">
                <thead>
                    <tr>
                        <th colspan="5">Detail<!--<small>asdada</small>-->
                            
                        </th>
                    </tr>
                    <tr>
                        <th>Reference No</th>
                        <th>Description</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <%List<GLItemCreditNote> listAll = (List<GLItemCreditNote>) CreditNoteDAO.getAllCNEItemEst(log,v.getNoteno());
                    if(listAll.isEmpty()){%>
                <tbody>
                    <tr>
                        <td colspan="3">
                            <span class="font-small-red">No data available.</span>
                        </td>
                    </tr>
                    <%}
                    for (GLItemCreditNote j : listAll) {%>
                    <tr class="activerowy" id="<%= j.getRefer()%>">
                        <td class="tdrow">&nbsp;<%= j.getRefer()%></td>
                        <td class="tdrow">&nbsp;<%= j.getDescp()%></td>
                        <td class="tdrow" align="right">&nbsp;<%= GeneralTerm.currencyFormat(j.getAmount()) %></td>
                        
                    </tr>
                    
                </tbody>
                    <%}%>
            </table>
        </div>
    </div>
</div>





      

      
