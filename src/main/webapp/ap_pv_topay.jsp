<%-- 
    Document   : ap_inv_topay
    Created on : Oct 19, 2016, 9:55:45 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.ap.VendorInvoice"%>
<%@page import="com.lcsb.fms.dao.financial.ap.VendorInvoiceDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    String[] values = request.getParameterValues("invno");

%>
<form data-toggle="validator" role="form" id="saveamount">
    <table class="table table-bordered table-striped table-hover" id="list1">
        <thead>
            <tr>
                <th></th>
                <th>Invoice No</th>
                <th>Date</th>
                <th>Payee</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            <%
            int r = 0;
            for (int i = 0; i < values.length - 1; i++) {
                r++;
                VendorInvoice vi = (VendorInvoice) VendorInvoiceDAO.getPNV(log, values[i]);%>
            <tr id="<%//= j.getInvrefno()%>" class="activerowy">
                <td width="3%"><%=i + 1%></td>
                <td class="tdrow"><%= vi.getInvrefno()%></td>
                <td class="tdrow"><%= vi.getDate()%></td>
                <td class="tdrow"><%= vi.getSuppcode()%> - <%= vi.getSuppname()%></td>
                <td class="tdrow">
                    <input type="hidden" name="invrefno<%=r%>" value="<%= vi.getInvrefno()%>">
                    <input type="text" class="form-control input-sm" id="amount<%=r%>" name="amount<%=r%>" value="<%= GeneralTerm.currencyFormat((vi.getTotalamount()) - vi.getPaid()) %>" required></td>
            </tr>
            <%}%>
        <input type="hidden" name="totalrow" value="<%=r%>">
        </tbody>
    </table>
</form>