<%-- 
    Document   : list_bank
    Created on : Mar 4, 2016, 4:06:05 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
            
             $.ajax({
                        url: "list_vendor_view.jsp",
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
            }});
            
           $('#keyword').keyup(function(){
                //var l = $('input[name=searchby]:checked').val();
                var keyword = $(this).val();
                
                 $.ajax({
                        url: "list_vendor_view.jsp?keyword="+keyword,
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
                }});
            });
            
            
            
            $('#result').on('click', '.thisresult', function(e) { 
                
                $('#<%= request.getParameter("name") %>').val($(this).attr('href'));
                $('#<%= request.getParameter("code") %>').val($(this).attr('id'));
                $('#<%= request.getParameter("registerno") %>').val($(this).attr('registerno'));
                $('#<%= request.getParameter("bumiputra") %>').val($(this).attr('bumiputra'));
                $('#<%= request.getParameter("ownername") %>').val($(this).attr('ownername'));
                $('#<%= request.getParameter("address") %>').val($(this).attr('address'));
                $('#<%= request.getParameter("city") %>').val($(this).attr('city'));
                $('#<%= request.getParameter("postcode") %>').val($(this).attr('postcode'));
                $('#<%= request.getParameter("state") %>').val($(this).attr('state'));
                $('#<%= request.getParameter("title") %>').val($(this).attr('title'));
                $('#<%= request.getParameter("position") %>').val($(this).attr('position'));
                $('#<%= request.getParameter("hp") %>').val($(this).attr('hp'));
                $('#<%= request.getParameter("phone") %>').val($(this).attr('phone'));
                $('#<%= request.getParameter("fax") %>').val($(this).attr('fax'));
                $('#<%= request.getParameter("email") %>').val($(this).attr('email'));
                $('#<%= request.getParameter("bankname") %>').val($(this).attr('bankname'));
                $('#<%= request.getParameter("bankaccount") %>').val($(this).attr('bankaccount'));
                $('#<%= request.getParameter("payment") %>').val($(this).attr('payment'));
                $('#<%= request.getParameter("remarks") %>').val($(this).attr('remarks'));
                
                e.preventDefault();
            });
            
           
            
        });
        </script>
    </head>
    <body>
         
        
        <div class="form-group">
            <div class="form-group input-group">
                <input type="text" class="form-control" id="keyword">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
        
        <div id="result">
       
        </div>
        
            
    </body>
</html>