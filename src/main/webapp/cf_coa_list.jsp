<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ChartofAccountDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.ModuleItem"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,java.util.*" errorPage="" isThreadSafe="false" %>
<%@ page import="java.util.Date"%>
<%
    response.setHeader("Cache-Control", "no-cache");
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) ChartofAccountDAO.getModule();
    String tab = request.getParameter("tab");
%>


<!-- END MESSAGE BOX-->

<!-- START PRELOADS -->
<audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
<audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
<!-- END PRELOADS -->                       

<!-- START SCRIPTS -->
<!-- START PLUGINS -->
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>        
<!-- END PLUGINS -->                

<!-- THIS PAGE PLUGINS -->
<script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
<script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>

<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>    
<!-- END PAGE PLUGINS -->

<!-- START TEMPLATE -->
<script type="text/javascript" src="js/settings.js"></script>

<script type="text/javascript" src="js/plugins.js"></script>        
<script type="text/javascript" src="js/actions.js"></script>     
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<!-- END TEMPLATE -->
<!-- END SCRIPTS --> 
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        var tab = sessionStorage.getItem(1);
        //$('#add-new').attr('id', 'add-new' + tab);
        //$('#refresh').attr('id', 'refresh' + tab);
        //console.log(tab);

        var updateTable = '<%= mod.getMainTable()%>';

    <%
        List<ModuleItem> mlist = (List<ModuleItem>) ModuleDAO.getAllModuleItem(log, ModuleDAO.getModule(log, mod.getModuleID()).getModuleID(), "cf_module_param");
        for (ModuleItem m : mlist) {
            if (m.getList_View().equals("1")) {
    %>
        columntoView.push('<%= m.getColumn_Name()%>');
        columnName.push('<%= m.getTitle()%>');
    <%
            }

        }
    %>

        var colsTosend = 'cols=';
        for (m = 0; m < columntoView.length; ++m) {//put value from database to variable jget
            if (m == columntoView.length - 1) {
                colsTosend += columntoView[m];
            } else {
                colsTosend += columntoView[m] + '&cols=';
            }
        }

        $('body').one('click', '#add-new', function (e) {
            //$("#add-new").click(function () {
            console.log('lalala');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addnew",
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });

        var orderby = ' order by voucherid desc';
        var oTable = $('#coa').DataTable({
            "responsive": true,
            "bServerSide": true,
            "sAjaxSource": "retrievetable.jsp?table=" + updateTable + "&code=<%= mod.getReferID_Master()%>&" + colsTosend + "&order=" + orderby,
            "order": [[0, "asc"]],
            "aoColumns": [
                {"sTitle": columnName[0], "mData": columntoView[0], "sWidth": "15%", "bSearchable": true},
                {"sTitle": columnName[1], "mData": columntoView[1], "sWidth": "27%"},
                {"sTitle": columnName[2], "mData": columntoView[2], "sWidth": "15%"},
                {"sTitle": columnName[3], "mData": columntoView[3], "sWidth": "15%"},
                {"sTitle": columnName[4], "mData": columntoView[4], "sWidth": "15%"}
            ]




        });

        $('body').on('click', '#refresh', function (e) {
            oTable.draw();
            return false;
        });

        $('#coa tbody').on('click', 'tr', function () {
            var data = oTable.row(this).data();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewdetail&referno=" + data.code,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });





    });
</script>
<div class="row">
    <div class="form-group">
        <div class="col-md-12">                                                                                                                        
            <div class="btn-group">
                <button class="btn btn-default"><i class="fa fa-plus-circle"></i>  Add <%= mod.getModuleDesc()%></button>  
                
            </div> 
                <ul class="panel-controls pull-right">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>  
        </div>
    </div> 
</div>
                <br>
<div class="row">
    <div class="col-md-12">

        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            
            <div class="panel-body">


                <table class="table table-striped table-bordered table-hover" id="coa">

                </table>

            </div></div></div></div>
