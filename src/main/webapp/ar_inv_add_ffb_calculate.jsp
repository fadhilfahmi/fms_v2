<%-- 
    Document   : ar_inv_add_ffb_calculate
    Created on : Nov 12, 2016, 9:12:05 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.TaxDAO"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArFFB"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    ArFFB ar = (ArFFB) SalesInvoiceDAO.getFFB(log,request.getParameter("sessionid"));
%>
<!DOCTYPE html>
<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered table-striped table-hover" id="list">
            <thead>

                <tr>
                    <th>TOTAL FFB SUPPLIED</th>
                    <th class="pull-right"><%= ar.getFfb()%> MT</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <p>Average Shipment Month Price of Palm Oil</p>
                        <p>Transport of Palm Oil</p>
                        <p>CESS MPOB</p>
                        <p>AVR Discounted Price for High FFA CPO</p>
                        <p>Nett Price of Crude Palm Oil</p>
                        <p>&nbsp;</p>
                        <p>Average Shipment Month Price of Palm Kernel</p>
                        <p>Extraction Rate of Palm Oil</p>
                        <p>Extraction Rate of Palm Kernel</p>
                        <p>&nbsp;</p>
                        <p>Cost of Palm Oil Per M/Ton FFB</p>
                        <p>Cost of Palm Kernel Per M/Ton FFB</p>
                        <p>Total Cost Per M/Ton FFB</p>
                        <p>&nbsp;</p>
                        <p>Less : a) Processing Cost Per M/Ton FFB</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b) External FFB Transport</p>
                        <p>&nbsp;</p>
                        <p>Price Per M/Ton FFB</p>
                        <p>&nbsp;</p>
                    </td>
                    <td class="pull-right">
                        <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getMpobpricecpo())%></span></p>
                        <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getTransport())%></span></p>
                        <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getCess())%></span></p>
                        <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getDiscount())%></span></p>
                        <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getNettcpo())%></span></p>
                        <p>&nbsp;</p>
                        <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getMpobpricepk())%></span></p>
                        <p><span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getOer())%></span></p><p>&nbsp;</p>
                        <p><span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getKer())%></span></p>
                        <p>&nbsp;</p><p>&nbsp;</p>
                        <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getCostcpo())%></span></p>
                        <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getCostpk())%></span></p>
                        <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getTotalcost())%></span></p>
                        <p>&nbsp;</p>
                        <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getMillcost())%></span></p>
                        <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getExternalffb())%></span></p>
                        <p>&nbsp;</p>
                        <p>RM&nbsp;&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(ar.getPriceperton())%></span></p>
                        <p>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <th>TOTAL</th>
                    <th class="pull-right">RM&nbsp;&nbsp;<%= GeneralTerm.currencyFormat(ar.getTotal())%></th>
                </tr>
                <tr>
                    <th>GST (<%= TaxDAO.getTax(log,"SR").getTaxrate()%>%)</th>
                    <th class="pull-right">RM&nbsp;&nbsp;<%= GeneralTerm.currencyFormat(ar.getGst())%></th>
                </tr>
                <tr>
                    <th>NETT AMOUNT</th>
                    <th class="pull-right">RM&nbsp;&nbsp;<%= GeneralTerm.currencyFormat(ar.getNettamount())%>
                        <input type="hidden" id="nettamount" value="<%= ar.getNettamount()%>">
                    </th>
                </tr>
            </tbody>

        </table>
    </div>
</div>