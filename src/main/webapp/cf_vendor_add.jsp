<%-- 
    Document   : cf_coa_add
    Created on : Mar 11, 2016, 8:58:06 AM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.VendorDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) VendorDAO.getModule();

%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/formValidation.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.js"></script>

<script type="text/javascript" charset="utf-8">

    $(document).ready(function () {

        /*$('#saveform').formValidation({
         message: 'This value is not valid',
         icon: {
         valid: 'glyphicon glyphicon-ok',
         invalid: 'glyphicon glyphicon-remove',
         validating: 'fa fa-refresh'
         },
         fields: {
         
         code: {
         required:true,
         validators: {
         notEmpty: {
         message: 'The Account Code is required'
         },
         digits: {
         message: 'The value can contain only digits'
         }
         }
         }
         }
         });*/


    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>



            <form data-toggle="validator" role="form" id="saveform">
                <input type="hidden" name="prepareid" id="pid" value="<%= log.getUserID()%>">
                <input type="hidden" name="preparename" id="pname" value="<%= log.getFullname()%>">
                <input type="hidden" name="preparedate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Code&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="code" name="code" value="<%= AutoGenerate.get4digitNo(log,"vendor_info", "code")%>" autocomplete="off" required readonly >  
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Company Name &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="vendor_name" name="vendor_name" autocomplete="off" >   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Registration No &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="register_no" name="register_no" autocomplete="off" value="">   
                            </div>
                        </div>
                    </div>

                </div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Address &nbsp&nbsp<span class="res_code"></span></label>
                                <textarea class="form-control" rows="3"  id="vendor_address" name="baddress"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Postcode &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="postcode" name="postcode" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">City &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="city" name="city" placeholder="" autocomplete="off" > 
                            </div>
                        </div><div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">State &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="state" name="state" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Active &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="active" name="active">
                                    <%= ParameterDAO.parameterList(log,"YesNo Type", "")%>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Bumiputra &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="bumiputra" name="bumiputra">
                                    <%= ParameterDAO.parameterList(log,"YesNo Type", "")%>
                                </select> 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Position &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="position" name="position">
                                    <%= ParameterDAO.parameterList(log,"Position", "")%>
                                </select> 
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Title &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="title" name="title">
                                    <%= ParameterDAO.parameterList(log,"Title", "")%>
                                </select> 
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Contact Person &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="person" name="person" placeholder="" autocomplete="off" > 
                            </div>
                        </div>

                    </div>
                </div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">HP No &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="hp" name="hp">
                                    <%= ParameterDAO.parameterList(log,"Title", "")%>
                                </select> 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Phone No &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="phone" name="phone" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Fax No &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="fax" name="fax" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Email &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="email" name="email" placeholder="" autocomplete="off" > 
                            </div>
                        </div>

                    </div>
                </div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Payment Method &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="payment" name="payment">
                                    <%= ParameterDAO.parameterList(log,"Payment Method", "")%>
                                </select> 
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Bank &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="bank" name="bank" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Account No &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="bankaccount" name="bankaccount" placeholder="" autocomplete="off" > 
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Remarks &nbsp&nbsp<span class="res_code"></span></label>
                                <textarea class="form-control" rows="3"  id="remarks" name="remarks"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">GST ID &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="gstid" name="gstid" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
