<%-- 
    Document   : cf_buyer_view
    Created on : Apr 18, 2016, 4:53:29 PM
    Author     : HP
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.BuyerDAO"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Buyer"%>
<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
Buyer view = (Buyer) BuyerDAO.getInfo(log,request.getParameter("id"));
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
           
            
              $( ".printbutton" ).click(function() {
            printReport($("#buyerdiv").html());
        });
                
              
         });
        </script>
            <div id ="maincontainer">
                <div class="row">
                    <div class="col-sm-3">
                    <button id="<%//= v.getRefer()%>" class="btn btn-default btn-xs printbutton" title="<%//= mod.getModuleID()%>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i> Print Buyer</button>
                    </div>
                </div>
                    <br>
                    <div id="buyerdiv">
                <table id="table_2" cellspacing="0">
                 <tr>
                     <td width="30%" colspan="2" class="bd_bottom" align="left" valign="top"><strong>Buyer Information</strong></td>
               </tr>  
               <tr>
                   <td width="30%" class="bd_bottom" align="left" valign="top">Code</td>
                   <td width="70%" class="bd_bottom" align="left"><%= view.getCode() %></td>
               </tr>
               <tr>
                   <td class="bd_bottom" align="left" valign="top">Description</td>
                   <td class="bd_bottom" align="left"> 
                       <%= view.getCompanyname()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">CoRegistration No</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getCoregister()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Address</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getAddress()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Postcode</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getPostcode()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">City</td>
                   <td class="bd_bottom" align="left">
                       <%= view.getCity()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">State</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getState()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Country</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getCountry()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Porla</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getPorla()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Bumiputra</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getBumiputra()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Person</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getPerson()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Title</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getTitle()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">HP No</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getHp()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Phone No</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getPhone()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Fax</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getFax()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Email</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getEmail()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Website URL</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getUrl()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Vendor</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getVendor()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Mill</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getMill()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">GST id</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getGstid()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Remarks</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getRemarks()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Position</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getPosition()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Account Code</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getCoa()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Account Description</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getCoadescp()%>
                   </td>
               </tr>
          </table>
                    </div>
            </div>
      

      
      
      
      

