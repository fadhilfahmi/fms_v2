<%-- 
    Document   : co_staff_basis_view
    Created on : Apr 19, 2017, 2:05:06 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.setup.company.staff.Staff"%>
<%@page import="com.lcsb.fms.dao.setup.company.StaffDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) StaffDAO.getModule();
    Staff v = (Staff) StaffDAO.getInfo(log, request.getParameter("refer"));

%>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $(".editbasis").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editbasis&refer=" + refer,
                success: function (result) {
                    $('#tab_content').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });
    });
</script>

<table class="data table table-striped no-margin">
    <thead>
        <tr>
            <th colspan="2">
                <span class="pull-left">
                    <button  class="btn btn-default btn-xs editbasis" title="<%= mod.getModuleID()%>" id="<%= v.getStaffid()%>" name="editbasis"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</button>
                    <button  class="btn btn-default btn-xs adddesignation" title="<%= mod.getModuleID()%>" id="<%= v.getStaffid()%>" name="editlist"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
                </span>
            </th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <th width="20%">ID</th>
            <td><%= v.getStaffid()%></td>
        </tr>
        <tr>
            <th>Name</th>
            <td><%= v.getName()%></td>
        </tr>
        <tr>
            <th>Title</th>
            <td><%= v.getTitle()%></td>
        </tr>
        <tr>
            <th>Status</th>
            <td><%= v.getStatus()%></td>
        </tr>
        <tr>
            <th>IC No.</th>
            <td><%= v.getIc()%></td>
        </tr>
        <tr>
            <th>Old IC No.</th>
            <td><%= v.getOldic()%></td>
        </tr>
        <tr>
            <th>Address</th>
            <td><%= v.getAddress()%>, <%= v.getPostcode()%>, <%= v.getCity()%>, <%= v.getState()%></td>
        </tr>
        <tr>
            <th>D.O.B</th>
            <td><%= v.getBirth()%></td>
        </tr>
        <tr>
            <th>Gender</th>
            <td><%= v.getSex()%></td>
        </tr>
        <tr>
            <th>Marital Status</th>
            <td><%= v.getMarital()%></td>
        </tr>
        <tr>
            <th>Religion</th>
            <td><%= v.getReligion()%></td>
        </tr>
        <tr>
            <th>Citizen</th>
            <td><%= v.getCitizen()%></td>
        </tr>
        <tr>
            <th>Phone</th>
            <td><%= v.getPhone()%></td>
        </tr>
        <tr>
            <th>HP No.</th>
            <td><%= v.getHp()%></td>
        </tr>
        <tr>
            <th>Email</th>
            <td><%= v.getEmail()%></td>
        </tr>
        <tr>
            <th>Date Join</th>
            <td><%= v.getDatejoin()%></td>
        </tr>
    </tbody>
</table>