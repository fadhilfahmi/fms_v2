<%-- 
    Document   : gl_prepay_generatejournal
    Created on : Nov 13, 2015, 4:05:32 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.model.financial.cashbook.CbCekbook"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.ChequeBookDAO"%>
<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page import="com.lcsb.fms.util.dao.DataDAO"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.util.model.ModuleItem"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.dao.financial.gl.JournalVoucherDAO"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.dao.financial.gl.PrepaymentDAO"%>
<%@page import="com.lcsb.fms.model.financial.gl.Prepayment"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
CbCekbook cb = (CbCekbook) ChequeBookDAO.getInfo(log,request.getParameter("no"));
%>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
           
            
              $( ".printbutton" ).click(function() {
                  //var a = $("form").serialize();
                  var a = $("#saveform :input[value!='']").serialize(); 
                  //alert($('input[name="ad_amount"]').val());
                  checkField();
                  var b = checkField();
                  if(b==false){
                    $.ajax({
                        data: a,
                        type: 'POST',
                        url: "DataController?do=add&formlevel=master",
                        success: function (result) {
                          $('#maincontainer').empty().html(result).hide().fadeIn(300);
                        }
                    });
                     
                  }    
                  
               
                    return false;
              });
              
         });
        </script>
            <div id ="maincontainer">
                <table id="table_2" cellspacing="0">
        
               <tr>
                   <td width="30%" class="bd_bottom" align="left" valign="top">Cheque Book No</td>
                   <td width="70%" class="bd_bottom" align="left"><%= cb.getNo() %></td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Bank/Branch Code</td>
                   <td  class="bd_bottom" align="left"> <%= cb.getBankcode()%>
                       
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Bank/Branch Name</td>
                   <td  class="bd_bottom" align="left"><%= cb.getBankname()%>
                       
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Date Received</td>
                   <td  class="bd_bottom" align="left"><%= cb.getTarikh()%>
                       
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Start Cheque No</td>
                   <td  class="bd_bottom" align="left"><%= cb.getStartcek()%></td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">No of Cheque</td>
                   <td  class="bd_bottom" align="left"><%= cb.getNocek()%></td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">End of Cheque</td>
                   <td class="bd_bottom" align="left"><%= cb.getEndcek()%></td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Active</td>
                   <td  class="bd_bottom" align="left"><%= cb.getActive()%></td>
               </tr>
          </table>
            </div>
      

      
      
      
      

