

<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.general.DataTable"%>
<%@page import="com.lcsb.fms.dao.financial.gl.JournalVoucherDAO"%>
<%@page import="com.lcsb.fms.util.model.ListTable"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.ModuleItem"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.ConnectionModel"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,java.util.*" errorPage="" isThreadSafe="false" %>
<%@ page import="java.util.Date"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) JournalVoucherDAO.getModule();

    DataTable prm = (DataTable) session.getAttribute("dtparam");

    String view = "";
    String period = "";
    String year = "";
    String btn_all = "default";
    String btn_today = "default";
    String btn_thismonth = "default";
    String select_year = "btn-default";
    String select_period = "btn-default";

    if (prm != null) {
        view = prm.getViewBy();
        period = prm.getPeriod();
        year = prm.getYear();
        btn_all = "default";

        if (view.equals("all")) {
            btn_all = "primary";
        } else if (view.equals("today")) {
            btn_today = "primary";
        } else if (view.equals("thismonth")) {
            btn_thismonth = "primary";
        } else if (view.equals("byperiod")) {
            select_year = "btn-primary";
            select_period = "btn-primary";
        }
    } else {
        view = "thismonth";
        //period = AccountingPeriod.getCurPeriod(log);
        //year = AccountingPeriod.getCurYear(log);
    }

    response.setHeader("Cache-Control", "no-cache");
%>

<!-- START PLUGINS -->
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>        
<!-- END PLUGINS -->                

<!-- THIS PAGE PLUGINS -->
<script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
<script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>

<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>    
<!-- END PAGE PLUGINS -->

<!-- START TEMPLATE -->

<script type="text/javascript" src="js/plugins.js"></script>        
<script type="text/javascript" src="js/actions.js"></script>     
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script src="jsfunction/timeago.js" type="text/javascript"></script>
<!-- END TEMPLATE -->
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        new timeago().render($('.need_to_be_rendered'));
        //var timeagoInstance = new timeago('<%//= SalesInvoiceDAO.getLastSync() %>');
        //timeagoInstance.render($('.need_to_be_rendered'));


        $.ajax({
            url: "PathController?moduleid=000000&moduletypeID=<%= mod.getModuleID()%>&process=datatable&view=<%= view%>&year=<%= year%>&period=<%= period%>",
            success: function (result) {
                $('#datatable-render').empty().html(result).hide().fadeIn(300);
            }
        });


        $("#add-new").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addnew",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        $("#check-button").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=check",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        $('body').on('click', '#refresh', function (e) {
            console.log('Refreshing');
            oTable.draw();
            return false;
        });

        $("#filter").click(function () {
            $("#filterdiv").toggle();
            //oTable.draw();
            return false;
        });

        $("#refine").click(function () {
            $("#filterdiv").toggle();
            //oTable.draw();
            return false;
        });



        $(".tochange").click(function (e) {
            $('.tochange').addClass('btn-default').removeClass('btn-primary');
            $('.changeperiod').addClass('btn-default').removeClass('btn-primary');
            $(this).addClass('btn-primary').removeClass('btn-default');
            $('#datatable-render').html('<div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>');
            var filterrange = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=000000&moduletypeID=<%= mod.getModuleID()%>&process=datatable&view=" + filterrange,
                success: function (result) {
                    $('#datatable-render').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".changeperiod").change(function (e) {
            $('.tochange').addClass('btn-default').removeClass('btn-primary');
            $('.changeperiod').addClass('btn-primary');
            var newyear = $('#newyear').val();
            var newperiod = $('#newperiod').val();
            $('#datatable-render').html('<div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>');
            e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=000000&moduletypeID=<%= mod.getModuleID()%>&process=datatable&view=byperiod&year=" + newyear + "&period=" + newperiod,
                success: function (result) {
                    $('#datatable-render').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $('#add-invoice').click(function (e) {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addfrominvoice",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });

    });

</script>
<div class="row">
    <div class="form-group">
        <div class="col-md-12">                                                                                                                        
            <div class="btn-group">
                <button id="add-new" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> New <%= mod.getModuleDesc()%></button> 

            </div> 
            <ul class="panel-controls pull-right">
                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
            </ul>  
        </div>
    </div> 
</div>
                <br>               
<div class="row">
    
    
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <button id="all" class="tochange btn btn-<%=btn_all%> btn-sm">All Transactions</button>
                <button id="today" class="tochange btn btn-<%=btn_today%> btn-sm">Today</button>
                <button id="thismonth" class="tochange btn btn-<%=btn_thismonth%> btn-sm">This Month</button>
            </div>
            <div class="col-sm-2">
                <div class="form-group">                                                                               
                    <select class="form-control  changeperiod <%=select_year%>" id="newyear" name="newyear" data-style="btn-success">
                        <%= ParameterDAO.parameterList(log, "Year", year)%>
                    </select>
                </div>  
            </div>
            <div class="col-sm-2">
                <div class="form-group">                                                                               
                    <select class="form-control  changeperiod <%=select_period%>" id="newperiod" name="newperiod" data-style="btn-success">
                        <%= ParameterDAO.parameterList(log, "Period", period)%>
                    </select>
                </div> 
            </div>
        </div>
    </div>

</div></div>

</div>
<div class="row">
    <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="dataTable_wrapper" id="datatable-render"> 
                    <div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>
                </div>
            </div>
        </div>
    </div>
</div>