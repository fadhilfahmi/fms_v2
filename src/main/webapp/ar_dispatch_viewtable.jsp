<%-- 
    Document   : ar_dispatch_viewtable
    Created on : Aug 8, 2017, 10:55:58 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArCPORefinery"%>
<%@page import="com.lcsb.fms.dao.financial.ar.ArDispatchDAO"%>
<%@page import="com.lcsb.fms.general.DataTable"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoice"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) ArDispatchDAO.getModule();
    String type = request.getParameter("type");
    String location = request.getParameter("location");

%>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        
        var editor;
        var oTable = $('#list<%=type + location%>').DataTable({
            responsive: true,
            "pagingType": "full",
            "aaSorting": [[0, "desc"]],
            formOptions: {
                bubble: {
                    title: 'Edit',
                    buttons: false
                }
            }
        });
        
        
        
        $('#list<%=type + location%>').on('click', 'tbody td:not(:first-child)', function (e) {
            var data = oTable.row(this).data();
            //alert(data[0]);
            e.preventDefault();
            
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                //animate: false,
                title: 'View Dispatch Detail',
                message: function (dialog) {
                    var $content = $('<body></body>').load('ar_dispatch_view_each.jsp?type=<%=type%>&location=<%=location%>&disno=' + data[1]);
                    //$('body').on('click', '.thisresult_nd', function(event){
                    //    dialog.close();
                    //    event.preventDefault();
                    //});
                    return $content;
                },
                buttons: [{
                        //icon: 'fa fa-circle-o-notch',
                        label: 'Save & Close',
                        cssClass: 'btn-danger',
                        autospin: true,
                        action: function (dialogRef) {
                            
                            var a = $("#saveform :input").serialize();
                            //$('#maincontainer').empty().html('<div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>').hide().fadeIn(300);
                            //checkField();
                            //var b = checkField();
                            //if(b==false){
                            $.ajax({
                                async: true,
                                data: a,
                                type: 'POST',
                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=updatedispatch",
                                success: function (result) {
                                    dialogRef.close();
                                    if ('<%=type%>' == 'cpo') {
                                        $.ajax({
                                            async: true,
                                            url: "PathController?moduleid=020904&process=cpo_local",
                                            success: function (result) {
                                                $('#dataTable_wrapper_cpo_local').empty().html(result).hide().fadeIn(300);
                                                return false;
                                            }
                                        });
                                    }
                                    if ('<%=type%>' == 'kernel') {
                                        $.ajax({
                                            async: true,
                                            url: "PathController?moduleid=020904&process=pk_local",
                                            success: function (result) {
                                                $('#dataTable_wrapper_pk_local').empty().html(result).hide().fadeIn(300);
                                                return false;
                                            }
                                        });
                                    }
                                    //$('#maincontainer').empty().html(result).hide().fadeIn(100);
                                }
                            });
                            
                        }
                    }, {
                        label: 'Close',
                        action: function (dialogRef) {
                            dialogRef.close();
                        }
                    }]
            });
        });
        
        $('#list<%=type + location%>').on('click', 'tbody td:not(:first-child)', function (e) {
        
        });
        
    });
</script>
<table class="table table-bordered table-hover" id="list<%=type + location%>">
    <thead>
    <th>#</th>
    <th>Dispatch No.</th>
    <th>Contract No.</th>
    <th>Date</th>
    <th>Action</th>
</thead>
<%
    
    List<ArCPORefinery> listAll = (List<ArCPORefinery>) ArDispatchDAO.getAllRefinery(log, type, location);
    if (listAll.isEmpty()) {%>
<tr>
    <td colspan="5">
        <span class="font-small-red">No data available.</span>
    </td>
</tr>

<%}
    int i = 0;
String trcolor = "#ffffff";
    for (ArCPORefinery j : listAll) {
        i++;
        
        if (location.equals("mill")) {

            if(!ArDispatchDAO.checkExistRefinery(log, type, j.getDispatchNo())){
            trcolor = "#ffc1bf";
}
            
            
        }
        
%>
<tr bgcolor="<%=trcolor%>">
    <td><%= i%></td>
    <td><%= j.getDispatchNo()%></td>
    <td><%= j.getContract()%></td>
    <td><%= j.getDate()%></td>
    <td>

        <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
            <%
                if(location.equals("mill")){
                    %><a><i class="fa fa-download" aria-hidden="true"></i></a><%
                }
                %>
            <a><i class="fa fa-eye" aria-hidden="true"></i></a>
            <a><i class="fa fa-pencil" aria-hidden="true"></i></a>

        </div>
    </td>
</tr>
<%}%>

</table>
