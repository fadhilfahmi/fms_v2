<%-- 
    Document   : list_invoice
    Created on : Oct 19, 2016, 3:40:37 PM
    Author     : fadhilfahmi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
            
             $.ajax({
                        url: "list_payee_view.jsp",
                        success: function (result) {
                        $('#result-payee').empty().html(result).hide().fadeIn(300);
                
            }});
            
           $('#keyword-payee').keyup(function(){
                //var l = $('input[name=searchby]:checked').val();
                var keyword = $(this).val();
                
                 $.ajax({
                        url: "list_payee_view.jsp?keyword="+keyword,
                        success: function (result) {
                        $('#result-payee').empty().html(result).hide().fadeIn(300);
                
                }});
            });
            
            
            
            $('#result-payee').on('click', '.thisresult-payee', function(e) { 
                var a = $(this).attr('href');
                var b = $(this).attr('id');
                var c = $(this).attr('title');
                
                $.ajax({
                        url: "ap_pv_select_invoice.jsp?suppcode="+b,
                        success: function (result) {
                        $('#list-invoice').empty().html(result).hide().fadeIn(300);
                
                }});
                
                e.preventDefault();
            });
            
           
            
        });
        </script>
    </head>
         
        
        <div class="form-group">
            <div class="form-group input-group">
                <input type="text" class="form-control" id="keyword-payee">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
        
        <div id="result-payee">
       
        </div>
        
            
