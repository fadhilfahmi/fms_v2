<%-- 
    Document   : tx_closeopenperiod_view
    Created on : Jan 11, 2017, 4:41:04 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxPeriod"%>
<%@page import="com.lcsb.fms.dao.financial.tx.TaxMasterDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.model.financial.gl.AccountPeriod"%>
<%@page import="com.lcsb.fms.dao.financial.gl.AccountPeriodDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    TxPeriod cur = (TxPeriod) TaxMasterDAO.getTaxPeriod(log);
    TxPeriod pre = (TxPeriod) TaxMasterDAO.getPreviousTaxPeriod(log);
    TxPeriod nxt = (TxPeriod) TaxMasterDAO.getNextTaxPeriod(log);
%>
<div class="row">
    <div class="col-sm-4">
        <div class="well">

            <form data-toggle="validator" role="form" id="saveform">
                <div class="row self-row">
                    <label for="inputName" class="control-label">Previous Period &nbsp&nbsp<span id="res_code"></span></label>
                </div>


                <div class="row">
                    <div class="col-sm-6">
                        <label for="inputName" class="control-label">Year</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= pre.getYear()%>" readonly>
                        </div>   
                    </div>
                    <div class="col-sm-6">
                        <label for="inputName" class="control-label">Period</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= pre.getPeriod()%>" readonly>
                        </div>   
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <label for="inputName" class="control-label">Start Date</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= pre.getTaxstart()%>" readonly>
                        </div>   
                    </div>
                    <div class="col-sm-6">
                        <label for="inputName" class="control-label">End Date</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= pre.getTaxend()%>" readonly>
                        </div>   
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <label for="inputName" class="control-label">Date Close Period</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= pre.getClosedate()%>" readonly>
                        </div>   
                    </div>
                </div>




            </form>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="well">

            <form data-toggle="validator" role="form" id="saveform">
                <div class="row self-row">
                    <label for="inputName" class="control-label">Current Period &nbsp&nbsp<span id="res_code"></span></label>
                </div>


                <div class="row">
                    <div class="col-sm-6">
                        <label for="inputName" class="control-label">Year</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= cur.getYear()%>" readonly>
                        </div>   
                    </div>
                    <div class="col-sm-6">
                        <label for="inputName" class="control-label">Period</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= cur.getPeriod()%>" readonly>
                        </div>   
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <label for="inputName" class="control-label">Start Date</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= cur.getTaxstart()%>" readonly>
                        </div>   
                    </div>
                    <div class="col-sm-6">
                        <label for="inputName" class="control-label">End Date</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= cur.getTaxend()%>" readonly>
                        </div>   
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <label for="inputName" class="control-label">Date Close Period</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= AccountingPeriod.getCurrentTimeStamp()%>" readonly>
                        </div>   
                    </div>
                </div>




            </form>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="well">

            <form data-toggle="validator" role="form" id="saveform">
                <div class="row self-row">
                    <label for="inputName" class="control-label">Next Period &nbsp&nbsp<span id="res_code"></span></label>
                </div>


                <div class="row">
                    <div class="col-sm-6">
                        <label for="inputName" class="control-label">Year</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= nxt.getYear()%>" readonly>
                        </div>   
                    </div>
                    <div class="col-sm-6">
                        <label for="inputName" class="control-label">Period</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= nxt.getPeriod()%>" readonly>
                        </div>   
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <label for="inputName" class="control-label">Start Date</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= nxt.getTaxstart()%>" readonly>
                        </div>   
                    </div>
                    <div class="col-sm-6">
                        <label for="inputName" class="control-label">End Date</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= nxt.getTaxend()%>" readonly>
                        </div>   
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <label for="inputName" class="control-label">Date Close Period</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="" readonly>
                        </div>   
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
