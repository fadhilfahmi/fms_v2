<%-- 
    Document   : cf_supplier_add
    Created on : Apr 19, 2016, 8:59:02 AM
    Author     : user
--%>
<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.SupplierDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) SupplierDAO.getModule();

%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/formValidation.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#getbank').click(function (e) {

            var a = 'bankcode';
            var b = 'bankname';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Taxation',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_bank.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });
        /*$('#saveform').formValidation({
         message: 'This value is not valid',
         icon: {
         valid: 'glyphicon glyphicon-ok',
         invalid: 'glyphicon glyphicon-remove',
         validating: 'fa fa-refresh'
         },
         fields: {
         
         code: {
         required:true,
         validators: {
         notEmpty: {
         message: 'The Account Code is required'
         },
         digits: {
         message: 'The value can contain only digits'
         }
         }
         }
         }
         });*/
        

        $("#savebuttonprompt").click(function () {

            var code = $('#code').val();
            var comp = $('#companyname').val();

            var path = $(this).attr('title');
            var process = $(this).attr('name');

            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Add to Vendor and <%= mod.getModuleDesc()%> ?',
                message: function (dialog) {
                    var $content = $('<body></body>').html(code + ' - ' + comp);
                    //$('body').on('click', '.thisresult_nd', function(event){
                    //    dialog.close();
                    //    event.preventDefault();
                    //});
                    return $content;
                },
                buttons: [{
                        //icon: 'fa fa-circle-o-notch',
                        label: 'Add to Vendor Information and <%= mod.getModuleDesc()%>',
                        cssClass: 'btn-primary',
                        autospin: true,
                        action: function (dialogRef) {
                            var $button = this;

                            //dialogRef.enableButtons(false);
                            //dialogRef.setClosable(false);
                            //dialogRef.getModalBody().html('<span style="font-size:14px">Finalizing in progress...</span>');   


                            var a = $("#saveform :input").serialize();
                            //checkField();
                            //var b = checkField();
                            //if(b==false){
                            $.ajax({
                                data: a,
                                type: 'POST',
                                url: "PathController?moduleid=" + path + "&process=" + process + "&transfer=yes",
                                success: function (result) {
                                    dialogRef.close();
                                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                                }
                            });


                            /*$.ajax({
                             async:false,
                             url: "PathController?moduleid=<%//= mod.getModuleID() %>&process=setnewperiod&year="+newy+"&period="+newp,
                             success: function (result) {
                             // $("#haha").html(result);
                             //$('#herey').empty().html(result).hide().fadeIn(300);
                             if(result==1){
                             setTimeout(function(){
                             //dialogRef.close();
                             dialogRef.enableButtons(true);
                             dialogRef.setClosable(true);
                             $button.disable();
                             $button.stopSpin(); // Equals to $button.toggleSpin(false);
                             dialogRef.getModalBody().html('<i class="fa fa-check right-green"></i>&nbsp;&nbsp;<span style="font-size:14px">Success!</span>');
                             }, 3000);
                             }else{
                             setTimeout(function(){
                             //dialogRef.close();
                             dialogRef.enableButtons(true);
                             dialogRef.setClosable(true);
                             //$button.disable();
                             $button.stopSpin(); // Equals to $button.toggleSpin(false);
                             dialogRef.getModalBody().html('<i class="fa fa-times fail-red"></i>&nbsp;&nbsp;<span style="font-size:14px">Failed!</span>');
                             }, 3000);
                             }
                             }
                             });*/

                        }
                    }, {
                        label: 'Close',
                        action: function (dialogRef) {
                            dialogRef.close();
                        }
                    }]
            });


            //var a = $("form").serialize();

            //}   
            return false;
        });

    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebuttonprompt"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>
            <form data-toggle="validator" role="form" id="saveform">
                <input type="hidden" name="prepareid" id="pid" value="<%= log.getUserID()%>">
                <input type="hidden" name="preparename" id="pname" value="<%= log.getFullname()%>">
                <input type="hidden" name="preparedate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Code&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="code" name="code" value="<%= AutoGenerate.getNewCode("supplier_info", "code")%>" autocomplete="off" required readonly >  
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Company Name &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="companyname" name="companyname" autocomplete="off" >   
                            </div>
                        </div>


                    </div>
                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Registration No &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="coregister" name="coregister" autocomplete="off" value="">   
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">GST ID &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="gstid" name="gstid" autocomplete="off" >   
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Bumiputra &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="bumiputra" name="bumiputra">
                                    <%= ParameterDAO.parameterList(log,"YesNo Type", "")%>
                                </select> 
                            </div>
                        </div>

                    </div>
                </div>
                <div class="well">
                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">HQ Account Code&nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="hqactcode" name="hqactcode">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="hqactcode" id1="hqactdesc"><i class="fa fa-cog"></i> Account</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">HQ Account Description&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="hqactdesc" name="hqactdesc" placeholder="0.00" autocomplete="off" > 
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Account Code&nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="coa" name="coa">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="coa" id1="coadescp"><i class="fa fa-cog"></i> Account</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Account Description&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="coadescp" name="coadescp" placeholder="0.00" autocomplete="off" > 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Title &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="title" name="title">
                                    <%= ParameterDAO.parameterList(log,"Title", "")%>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Name &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="person" name="person" placeholder="" autocomplete="off" > 
                            </div>
                        </div>


                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Position &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="position" name="position">
                                    <%= ParameterDAO.parameterList(log,"Position", "")%>
                                </select> 
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Address &nbsp&nbsp<span class="res_code"></span></label>
                                <textarea class="form-control" rows="4"  id="address" name="address"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">City &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="city" name="city" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">State &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="state" name="state" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Postcode &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="postcode" name="postcode" placeholder="" autocomplete="off" > 
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Country &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="country" name="country" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                    </div>    


                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Handphone No. &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="hp" name="hp" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Phone No. &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="phone" name="phone" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Fax No. &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="fax" name="fax" placeholder="" autocomplete="off" > 
                            </div></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Email &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="email" name="email" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">URL Address &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="url" name="url" placeholder="" autocomplete="off" > 
                            </div>
                        </div></div>
                </div>

                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Bank&nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="bankcode" name="bank" placeholder="" autocomplete="off" >
                                    <span class="input-group-btn">


                                        <button class="btn btn-default btn-sm" id="getbank" title=""><i class="fa fa-cog"></i>Get Bank</button></span>
                                </div>  
                            </div>
                        </div><div class="col-sm-9">
                            <label for="inputName" class="control-label">Bank Description &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                            <input type="text" class="form-control input-sm" id="bankname" name="bankdesc" placeholder="" autocomplete="off" > 
                        </div></div><div class="row"></div>
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Bank Account&nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="bankaccount" name="bankaccount" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Payment Method &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="payment" name="payment">
                                    <%= ParameterDAO.parameterList(log,"Payment Method", "")%>
                                </select> 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Remarks &nbsp&nbsp<span class="res_code"></span></label>
                                <textarea class="form-control" rows="3"  id="remarks" name="remarks"></textarea>
                            </div>
                        </div>

                    </div>
                </div>



                <div class="well">

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Deposit Code&nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="depositcode" name="depositcode">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="depositcode" id1="depositdesc"><i class="fa fa-cog"></i> Deposit</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Deposit Description&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="depositdesc" name="depositdesc" placeholder="0.00" autocomplete="off" > 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Suspense Code&nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="suspensecode" name="suspensecode">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="suspensecode" id1="suspensedesc"><i class="fa fa-cog"></i> Suspense</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Suspense Description&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="suspensedesc" name="suspensedesc" placeholder="0.00" autocomplete="off" > 
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
