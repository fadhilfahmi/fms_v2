<%-- 
    Document   : cf_vendor_gethq
    Created on : Sep 27, 2016, 9:17:32 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.setup.configuration.VendorDAO"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) VendorDAO.getModule();
//UserAccount v = (UserAccount) UserDAO.getInfo(request.getParameter("referenceno"));

%>
<style>
    .invoice-title h2, .invoice-title h3 {
        display: inline-block;
    }

    .table > tbody > tr > .no-line {
        border-top: none;
    }

    .table > thead > tr > .no-line {
        border-bottom: none;
    }

    .table > tbody > tr > .thick-line {
        border-top: 2px solid;
    }
</style>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $('#keyword').keyup(function () {
            //var l = $('input[name=searchby]:checked').val();
            var keyword = $(this).val();

            $.ajax({
                url: "cf_vendor_gethq_search.jsp?keyword=" + keyword,
                success: function (result) {
                    $('#result').empty().html(result).hide().fadeIn(300);

                }});
        });

        $('#result').on('click', '.thisresult', function (e) {

            var a = $(this).attr('href');
            var b = $(this).attr('id');

            $('#result').html('');

            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=listsearch&type=" + a + "&code=" + b,
                success: function (result) {
                    $('#result').empty().html(result).hide().fadeIn(300);

                }});

            e.preventDefault();

        });

    });
</script>
<div id ="maincontainer">

    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <div class="btn-group btn-group-sm" role="group" aria-label="...">


                            <div class="btn-group" role="group" aria-label="...">
                                <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>




                            </div>
                        </div>

                    </td>
                </tr>
            </table>
            <br>
            <div class="well">
                <label>Search From HQ Database</label>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="form-group input-group">
                                <input type="text" class="form-control" id="keyword">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>

                        <div id="result">

                        </div>
                    </div>
                </div>

            </div>
            <br/>
        </div>
    </div>
</div>