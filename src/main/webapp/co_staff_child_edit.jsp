<%-- 
    Document   : co_staff_designation_add
    Created on : Apr 6, 2017, 10:32:08 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.setup.company.staff.StaffChild"%>
<%@page import="com.lcsb.fms.model.setup.company.staff.Staff"%>
<%@page import="com.lcsb.fms.dao.setup.company.StaffDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) StaffDAO.getModule();
    StaffChild v = (StaffChild) StaffDAO.getChild(log, request.getParameter("refer"));

%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $('#tab_content .datepicker').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        });


        $('#backtolist').click(function (e) {
            var path = $(this).attr('title');
            var process = $(this).attr('name');
            var refer = $(this).attr('type');
            $.ajax({
                url: "PathController?moduleid=" + path + "&process=" + process + "&refer=" + refer,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#tab_content').empty().html(result).hide().fadeIn(100);
                }
            });
            return false;
        });

    });

</script>

<div class="col-sm-6">
    <p></p>
    <div class="row">
        <div class="col-sm-12">
            <span class="pull-left">
                <button id="backtolist" class="btn btn-default btn-xs" title="<%= mod.getModuleID()%>" name="viewchild" type="<%= v.getStaffid()%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                <button id="savebutton"  class="btn btn-default btn-xs" title="<%= mod.getModuleID()%>" name="editchildprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>

        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <div class="well">
                <form data-toggle="validator" role="form" id="saveform">
                    <input type="hidden" name="staffid" value="<%= v.getStaffid()%>">
                    <input type="hidden" name="id" value="<%= v.getId()%>">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Name&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="name" name="name" value="<%= v.getName()%>" autocomplete="off" > 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Mother's Name<span id="res_code"></span></label>
                                <select class="form-control input-sm" id="motherid" name="motherid">
                                    <%= StaffDAO.getMotherList(log, v.getStaffid(), v.getMotherid())%>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Child No.&nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="no" name="no">
                                    <%= StaffDAO.childListNumber(log, "Child Number", v.getNo(), v.getStaffid())%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Alive ?<span id="res_code"></span></label>
                                <select class="form-control input-sm" id="alive" name="alive">
                                    <%= ParameterDAO.parameterList(log, "YesNo Type", v.getAlive())%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Tax Dependent ?<span id="res_code"></span></label>
                                <select class="form-control input-sm" id="taxdependent" name="taxdependent">
                                    <%= ParameterDAO.parameterList(log, "YesNo Type", ((v.isTaxdependent()) ? "Yes" : "No"))%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">IC No.&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="ic" name="ic" value="<%= v.getIc()%>" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Birth Certificate No.&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="birthno" name="birthno" value="<%= v.getBirthno()%>" autocomplete="off" > 
                            </div>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Date of Birth</label>
                                <div class="form-group-sm input-group">
                                    <input type="text" id="dob" name="dob" class="form-control datepicker" value="<%= v.getDob()%>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Gender &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="gender" name="gender">
                                    <%= ParameterDAO.parameterList(log, "Gender", "")%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Religion &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="religion" name="religion">
                                    <%= ParameterDAO.parameterList(log, "Religion", v.getReligion())%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Race &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="race" name="race">
                                    <%= ParameterDAO.parameterList(log, "Race", v.getRace())%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Citizen &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="citizen" name="citizen">
                                    <%= ParameterDAO.parameterList(log, "Citizen", v.getCitizen())%>
                                </select>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>


