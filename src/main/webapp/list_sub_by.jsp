<%-- 
    Document   : list_sub_by
    Created on : Mar 2, 2016, 12:40:25 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.SubAccount"%>
<%@page import="com.lcsb.fms.util.dao.SubAccountDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<div class="list-group"><%
            
            String by = request.getParameter("by");
            String keyword = request.getParameter("keyword");
            
            SubAccount sub = SubAccountDAO.setInfo(by);
            
            List<SubAccount> slist = (List<SubAccount>) SubAccountDAO.getSubAcc(log,sub,keyword);
            for (SubAccount c : slist) { 
               %>
       
            <a href="<%= c.getTheDesc()%>" id="<%= c.getTheCode()%>" class="list-group-item thisresult_nd">
                <div id="left_div"><%= c.getTheCode()%></div>
                <div id="right_div"><%= c.getTheDesc()%></div>
            </a>

               <%    
            } 
        %>
</div>