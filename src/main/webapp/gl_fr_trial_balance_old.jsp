<%-- 
    Document   : gl_fr_trial_balance_old
    Created on : Feb 25, 2017, 1:11:35 PM
    Author     : fadhilfahmi
--%>
<%@  page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,ipamis.*" errorPage="" %><%@ page import="java.util.Date"%><%Connection con = (Connection) session.getAttribute("con");%>
<%@ page import="ipamis.*"%>
<!DOCTYPE HTML>
<html>
    <head>
        <title>IPAMIS Financial Report - Trial Balance</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <style type="text/css">
            <!--
            table {
                border-collapse: collapse;
                -fs-table-paginate: paginate;
                margin: 0 auto;
                padding: 0px;
                border-spacing: 0px;
                border-width: 0px;
                width: 100%;
            }
            thead {display: table-header-group; }
            tfoot {display: table-footer-group; }
            thead th, thead td {position: static; }

            @media screen{
                #footer{
                    display: none !important;
                }
            }
            div.footer {
                display: block; text-align: center;
                position: running(footer);
            }
            #footer {
                position: running(footer);
                text-align: right;
            }
            @page {
                @bottom-right {
                    content: element(footer);
                }
            }

            #pagenumber:before {
                content: counter(page);
            }

            #pagecount:before {
                content: counter(pages);
            } 
            -->
        </style>
        <link href="css/ipamis_report.css" rel="stylesheet" type="text/css">
        <link href="css/report.css" rel="stylesheet" type="text/css">

    </head>
    <script language="JavaScript" type="text/JavaScript">  
        function periksa()  
        {
        if(document.getElementById("rtype").value==="pdf"){
        document.form1.action="report_pdf_viewer.jsp";
        }
        else if(document.getElementById("rtype").value==="excel"){
        document.form1.action="report_excel_generator.jsp";
        }	
        else{
        document.form1.action="gl_fr_Trial_Balance2.jsp";
        }
        document.form1.view.value="true"
        document.form1.submit()
        }

        function tukar()
        {
        ida=document.form1.vby.value

        if(ida=="1")
        {document.form1.loccode1.disabled=true;
        document.form1.loccode2.disabled=true} 
        else if(ida=="2")
        {document.form1.loccode1.disabled=false;
        document.form1.loccode2.disabled=true}
        else if(ida=="3")
        {document.form1.loccode1.disabled=true;
        document.form1.loccode2.disabled=false}
        }


        function cetak()
        {
        window.print()
        }


    </script>
    <%
        if (request.getParameter("view") == null) {
    %>
    <link href="css/mainForm.css" rel="stylesheet" type="text/css" />
    <%
        }
    %>
    <body class="landscapePage">

        <form name="form1" method="get" action="">
            <%
                /*Class.forName ("org.gjt.mm.mysql.Driver");*/
             /*Connection con = DriverManager.getConnection("jdbc:mysql://localhost/IPAMIS_db","root","");*/
                if (request.getParameter("view") == null) {
            %>
            <fieldset>
                <legend>Trial Balance - Sheet No. 3</legend>
                <ol>
                    <li>
                        <label for="year">Year</label>
                        <input name="year" type="text" id="year" value="<%=(String) session.getAttribute("year")%>">
                    </li>
                    <li>
                        <label for="period">Period</label>
                        <select name="period" id="period">
                            <option value="<%=(String) session.getAttribute("period")%>" selected="selected"><%=(String) session.getAttribute("period")%></option>
                            <option value="1" >1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>			
                    </li>
                    <li>
                        <label>Type</label>
                        <select name="otype" id="select2">
                            <option value="1">Opening Balance</option>
                            <option value="2">This Month</option>
                            <option value="3" selected="selected">To Date</option>
                        </select>			
                    </li>
                    <li>
                        <label for="vby">View By</label>
                        <select name="vby" id="vby" onChange="tukar()">
                            <option value="1">Consolidation</option>
                            <option value="2">Management</option>
                            <option value="3" selected="selected">Estate</option>
                        </select>			
                    </li>
                    <li>
                        <label for="loccode1">&nbsp;&nbsp;Management</label>
                        <select name="loccode1" disabled="true" id="loccode1">
                            <%
                                Statement stmmnt = con.createStatement();
                                ResultSet setmnt = stmmnt.executeQuery("select code,name from ce_manage order by code");
                                while (setmnt.next()) {
                                    out.println("<option value=\"" + setmnt.getString("code") + "\">" + setmnt.getString("code") + " - " + setmnt.getString("name") + "</option>");
                                }
                            %>
                        </select>			
                    </li>
                    <li>
                        <label for="loccode2">&nbsp;&nbsp;Estate</label>
                        <select name="loccode2" id="loccode2">
                            <%
                                Statement stmest = con.createStatement();
                                ResultSet setest = stmest.executeQuery("select code,name from ce_estate order by code");
                                while (setest.next()) {
                                    out.println("<option value=\"" + setest.getString("code") + "\">" + setest.getString("code") + " - " + setest.getString("name") + "</option>");
                                }
                            %>		
                        </select>			
                    </li>		
                    <li>
                        <label for="vall">View All Account</label>
                        <select name="vall" id="vall">
                            <option value="No" selected>No</option>
                            <option value="Yes">Yes</option>
                        </select>			
                    </li>
                    <li>
                        <label for="rtype">Report Type</label>
                        <select name="rtype" id="rtype">
                            <option value="pdf" selected="true">PDF</option>
                            <option value="html">HTML</option>
                            <option value="excel">Excel</option>
                        </select>
                    </li>	         	
                    <li>
                        <input type="button" name="Submit3" onClick="location.href = 'gl_fr_incomeexpenses.jsp'" value="<<">
                        <input type="button" name="Button" value="Generate Report" onClick="periksa()">
                        <input type="button" name="Submit4" onClick="location.href = 'gl_Financial_Report.jsp'" value="Return To Index">
                        <input type="button" name="Submit" onClick="location.href = 'gl_fr_summaryexp.jsp'" value=">>">
                        <input type="hidden" name="view" value="view"><input type="hidden" name="report" id="report" value="gl_fr_Trial_Balance2.jsp">
                    </li>									
                </ol>
            </fieldset>

            <%
            } else if (request.getParameter("view").equalsIgnoreCase("true")) {

                String exportToExcel = request.getParameter("exportToExcel");
                if (exportToExcel != null
                        && exportToExcel.toString().equalsIgnoreCase("YES")) {
                    response.setContentType("application/vnd.ms-excel");
                    response.setHeader("Content-Disposition", "inline; filename="
                            + "excel.xls");

                }

                String sqlS = "";
                String allv = request.getParameter("vall");
                boolean vall = false;
                SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");

          //	disable temporary
          //	if(allv.equalsIgnoreCase("Yes"))vall=true;
                vall = false;
                String year = request.getParameter("year");
                String period = request.getParameter("period");
                String otype = request.getParameter("otype");
                String vby = request.getParameter("vby");
                String loccode = "";
                String locname = "Consolidation";
                //String tbtable="";
                String oblyp = " where concat(year,period)='" + year.concat(period) + "'";

                String url = "MPSB".concat(year).concat(period).concat(".csv");
                String date = "";
                Statement stmp = con.createStatement();
                ResultSet s = stmp.executeQuery("select endperiod from gl_altaccountingperiod where year='" + year + "' and period='" + period + "'");
                if (s.next()) {
                    date = formatter.format(s.getDate("endperiod"));
                }
                String prd = "";
                if (Integer.parseInt(period) < 10) {
                    prd = "0".concat(period);
                } else {
                    prd = period;
                }

                String yr = prd.concat(year);

                String oblby = "";
                String obltype = "";
                String obl = "";
                String tbtable = "";

                if (vby.equalsIgnoreCase("2")) {
                    loccode = request.getParameter("loccode1");
                    oblby = " and loccode like concat('" + loccode + "','%')";
                    tbtable = "ce_manage";
                } else if (vby.equalsIgnoreCase("3")) {
                    loccode = request.getParameter("loccode2");
                    oblby = " and loccode like concat('" + loccode + "','%')";
                    tbtable = "ce_estate";
                }
                Statement stmname = con.createStatement();
                ResultSet setname = null;
                if (!vby.equalsIgnoreCase("1")) {
                    //Statement stmname = con.createStatement ();
                    setname = stmname.executeQuery("select code,name from " + tbtable + " where code ='" + loccode + "'");

                    if (setname.next()) {
                        locname = setname.getString("name");
                    }
                }
                //if(otype.equalsIgnoreCase("1"))	{obltype=" from gl_openingbalance ";tbtable="gl_openingbalance";}
                String obltype2 = "";
                String syaratre = "";
          //String syaratre2="";
                if (otype.equalsIgnoreCase("1")) {
                    obltype = "  gl_openingbalance ";
                    obltype2 = "  gl_closingbalance ";

                } else if (otype.equalsIgnoreCase("2")) {
                    obltype = "  gl_openingbalance_detail ";
                    obltype2 = "  gl_openingbalance_detail ";
                    syaratre = "and t1.coacode not like  '3103%'";
          //		syaratre2=" acccode <> "
                } else if (otype.equalsIgnoreCase("3")) {
                    obltype = "  gl_closingbalance ";
                    obltype2 = "  gl_closingbalance ";
                }

                /*Trialbalance pdfrpt=new Trialbalance(con,year,period,vby,loccode,otype,application.getRealPath(""));
                  try{
                  pdfrpt.startpdf(year,period,vby,loccode,otype);
                  }catch(Exception x)
                  {
                  out.println(x);
                  }*/

            %>
            <div class='footer reportNo' id="footer">
                Page <span id="pagenumber"></span> of <span id="pagecount"></span>
            </div>
            <table width="100%" border="0" align="center">
                <tr>
                    <td class="reportNo">&nbsp;</td>
                    <td class="reportNo">&nbsp;</td>
                    <td class="reportNo">&nbsp;</td>
                    <td class="reportNo">&nbsp;</td>
                    <td class="reportNo">&nbsp;</td>
                    <td class="reportNo">&nbsp;</td>
                    <td class="reportNo">&nbsp;</td> 
                    <td class="reportNo">SHEET NO 3</td>
                </tr>
            </table>


            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr> 
                    <td width="8%" rowspan="3"><img src="<%=CompanyProfile.getCompanyLogo()%>" width="70" height="70"></td>
                    <td colspan="7" class="reportTitle"><%=CompanyProfile.getCompanyName()%></td>
                </tr>
                <tr> 
                    <td colspan="7" class="reportTitle"><%=locname.toUpperCase()%></td>
                </tr>
                <tr> 
                    <td colspan="7" class="reportTitle">TRIAL BALANCE FOR PERIOD <%=period%> YEAR <%=year%></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td width="92%" class="reportTitle">&nbsp;</td>
                    <td width="92%" class="reportTitle">&nbsp;</td>
                    <td width="92%" class="reportTitle">&nbsp;</td>
                    <td width="92%" class="reportTitle">&nbsp;</td>
                    <td width="92%" class="reportTitle">&nbsp;</td>
                    <td width="92%" class="reportTitle">&nbsp;</td>
                    <td width="92%" class="reportTitle">&nbsp;</td>
                </tr>
            </table>

            <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
                <thead>
                    <tr> 
                        <th width="10%" class="border1110 reportSubTitle0 textCenter">&nbsp;ACCOUNT CODE</th>
                        <th width="29%" class="border1110 reportSubTitle0 textCenter">&nbsp;ACCOUNT DESCRIPTION</th>
                        <th width="30%" class="border1110 reportSubTitle0 textCenter">SUB</th>
                        <th width="10%" class="border1110 reportSubTitle0 textCenter">&nbsp;STAGE</th>
                        <th width="10%" class="border1110 reportSubTitle0 textCenter">&nbsp;ENTERPRISE</th>
                        <th width="10%" class="border1110 reportSubTitle0 textCenter">DEBIT&nbsp;</th>
                        <th width="1%" class="border1110 reportSubTitle0 textCenter">&nbsp;</th>
                        <th width="10%" class="border1011 reportSubTitle0 textCenter">CREDIT&nbsp;</th>
                    </tr>
                </thead>
                <%

                    try {
                        DecimalFormat dfx = new DecimalFormat("#0.00");
                        int rowsAffected = 0;
                        Statement updvallv = con.createStatement();
                        sqlS = "delete from gl_rpt_trial_balance ";
                        rowsAffected = updvallv.executeUpdate(sqlS);
                        double debit = 0;
                        double credit = 0;

                        if (syaratre.equalsIgnoreCase("")) {
                            String xxc = "gl_openingbalance";
                            Statement stmre = con.createStatement();
                            ResultSet setre = stmre.executeQuery("select count(*) as cnt   from  gl_openingbalance_detail where coacode like '3103%' and concat(year,period)=concat('" + year + "','" + period + "') " + oblby + "  group by coacode");
                            if (setre.next()) {
                                if (setre.getInt("cnt") > 0) {
                                    xxc = "gl_closingbalance";
                                }
                            }

                            setre = stmre.executeQuery("select coacode,coadesc,ifnull(sum(debit),0) as sdebit, ifnull(sum(credit),0) as scredit   from  " + xxc + " where coacode like '3103%' and concat(year,period)=concat('" + year + "','" + period + "') " + oblby + "  group by coacode");
                            while (setre.next()) {

                                double debitx = setre.getDouble("sdebit");
                                double creditx = setre.getDouble("scredit");

                                double beza = debitx - creditx;
                                if (beza > 0) {
                                    debitx = beza;
                                    creditx = 0;
                                } else if (beza < 0) {
                                    debitx = 0;
                                    creditx = (beza * (-1));
                                } else {
                                    debitx = 0;
                                    creditx = 0;
                                }

                                debit += debitx;
                                credit += creditx;
                                Statement updre = con.createStatement();
                                sqlS = "insert into gl_rpt_trial_balance ";
                                sqlS += " (acccode,accdesc,stgcode,stgdesc,entcode,entdesc,satype,sacode,sadesc,debit,credit) ";
                                sqlS += " values('" + setre.getString("coacode") + "','" + setre.getString("coadesc") + "','--','--','--','--','--','--','--'," + dfx.format(debitx) + "," + dfx.format(creditx) + ") ";
                                obl = sqlS;
                                rowsAffected = updre.executeUpdate(sqlS);
                            }
                        }
                        String extx = "coacode,coadesc,applevel,satype,sacode,sadesc";
                        String extx2 = "coacode,applevel,satype,sacode";
                        String syaratxx = "coacode not like  '3103%' and year='" + year + "' and period='" + period + "'";
                        String tablex = obltype;
                        /*	
                    if(vall)
                    {	
                            syaratxx="code<>'3103' and finallvl='yes'";
                            extx2="code,applevel,cttype,ctcode";
                            tablex="chartofacccount";
                            extx="code as coacode,descp as coadesc,applevel,cttype,ctcode,ctdesc";
                    }
                         */
                        Statement stmcoa = con.createStatement();
                        Statement updvall = con.createStatement();
                        ResultSet setcoa = stmcoa.executeQuery("select " + extx + " from  " + tablex + " where " + syaratxx + "  group by " + extx2);
                        int i = 0;
                        boolean masuk = false;
                        //out.println("select "+ extx +" from  "+ tablex +" where "+syaratxx+"  group by "+extx2+"<br><br>");
                        while (setcoa.next()) {
                                obl = "select ifnull(sum(debit),0) as sdebit,ifnull(sum(credit),0) as scredit,satype,sacode,sadesc from ".concat(obltype).concat(" as t1 ").concat(oblyp).concat(oblby).concat(" and coacode='" + setcoa.getString("coacode") + "' and satype='" + setcoa.getString("satype") + "' and sacode='" + setcoa.getString("sacode") + "' " + syaratre + " and applevel='Estate' group by  coacode,satype,sacode ");
                                masuk = false;
                                //out.println(obl+"<br><br>");
                                Statement stmcobx = con.createStatement();
                                ResultSet setcobx = stmcobx.executeQuery(obl);
                                while (setcobx.next()) {
                                    masuk = true;
                                    if ((setcobx.getDouble("sdebit") != 0) || (setcobx.getDouble("scredit") != 0) || vall) {
                                        double debitx = setcobx.getDouble("sdebit");
                                        double creditx = setcobx.getDouble("scredit");
                                        if ((setcobx.getDouble("sdebit") != 0) && (setcobx.getDouble("scredit") != 0)) {
                                            double beza = debitx - creditx;
                                            if (beza > 0) {
                                                debitx = beza;
                                                creditx = 0;
                                            } else if (beza < 0) {
                                                debitx = 0;
                                                creditx = (beza * (-1));
                                            } else {
                                                debitx = 0;
                                                creditx = 0;
                                            }
                                        }
                                        debit += debitx;
                                        credit += creditx;

                                        String satype = "";
                                        String sacode = "";
                                        String sadesc = "";

                                        
                                            satype = setcobx.getString("satype");
                                            sacode = setcobx.getString("sacode");
                                            sadesc = setcobx.getString("sadesc");
                                       

                                        //i++;
                                        sqlS = "insert into gl_rpt_trial_balance ";
                                        sqlS += " (acccode,accdesc,stgcode,stgdesc,entcode,entdesc,satype,sacode,sadesc,debit,credit) ";
                                        sqlS += " values('" + setcoa.getString("coacode") + "','" + setcoa.getString("coadesc") + "','--','--','--','--','" + satype + "','" + sacode + "','" + sadesc + "'," + dfx.format(debitx) + "," + dfx.format(creditx) + ") ";
                                        obl = sqlS;
                                        rowsAffected = updvall.executeUpdate(sqlS);

                                    }//if((setcobx.getDouble("sdebit")!=0)||(setcobx.getDouble("scredit")!=0)||vall)
                                }//while(setcobx.next())
                                if (!masuk && vall) {
                                    sqlS = "insert into gl_rpt_trial_balance ";
                                    sqlS += " (acccode,accdesc,stgcode,stgdesc,entcode,entdesc,satype,sacode,sadesc,debit,credit) ";
                                    sqlS += " values('" + setcoa.getString("coacode") + "','" + setcoa.getString("coadesc") + "','--','--','--','--','--','--','--',0.00,0.00) ";
                                    obl = sqlS;
                                    rowsAffected = updvall.executeUpdate(sqlS);
                                }
                        }//while(setcoa.next())  
                        DecimalFormat df = new DecimalFormat(" ###,###,###,##0.00");
                        String syaratlagi = "where debit<>0 or credit<>0";
                        if (vall) {
                            syaratlagi = "";
                        }
                        Statement stmtbrpt = con.createStatement();
                        ResultSet settbrpt = stmtbrpt.executeQuery("select * from gl_rpt_trial_balance " + syaratlagi + " group by  acccode,stgcode,entcode,satype,sacode order by acccode,stgcode,entcode,satype,sacode");
                        while (settbrpt.next()) {
                            i++;
                            String dt = "";
                            String ct = "";

                            if (settbrpt.getDouble("debit") == 0) {
                                dt = "";
                            } else {
                                dt = df.format(settbrpt.getDouble("debit"));
                            }
                            if (settbrpt.getDouble("credit") == 0) {
                                ct = "";
                            } else {
                                ct = df.format(settbrpt.getDouble("credit"));
                            }

                            String satext = "";
                            if (settbrpt.getString("satype").equals("None")) {
                                satext = "";
                            } else {
                                satext = settbrpt.getString("satype").concat(" - ").concat(settbrpt.getString("sacode")).concat(" - ").concat(settbrpt.getString("sadesc"));
                            }


                %>
                <tr> 
                    <td class="dataNormalText"><%=settbrpt.getString("acccode")%></td>
                    <td class="dataNormalText"><%=settbrpt.getString("accdesc")%></td>
                    <td class="dataNormalText"><%=satext%></td>
                    <td class="dataNormalText"><%=settbrpt.getString("stgdesc")%></td>
                    <td class="dataNormalText"><%=settbrpt.getString("entdesc")%></td>
                    <td class="dataNormalNumber"><div align="right"><%=dt%></td>
                    <td>&nbsp;</td>
                    <td class="dataNormalNumber"><div align="right"><%=ct%></td>
                </tr>
                <%}%>
                <tr> 
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="dataNormalText">&nbsp;TOTAL</td>
                    <td class="cellTotal"><%=df.format(debit)%></td>
                    <td>&nbsp;</td>
                    <td class="cellTotal"><%=df.format(credit)%></td>
                </tr>
                <tr class="no-print"> 
                    <td colspan="8" class="no-print"><div align="center">
                            <input type="button" name="Button" value="Back" onClick="location.href = 'gl_fr_Trial_Balance2.jsp'">
                            <input type="button" name="Submit2" value="Print" onClick="window.print()">
                            <input type="button" name="Button" value="Excel File" onClick="location.href = 'gl_fr_excel.jsp?date=<%=date%>&loccode=<%=loccode%>&yr=<%=yr%>'"></div></td>
                </tr>
            </table>
            <%

            //out.println(obl+"<br>");
                    } catch (Exception e) {
                        out.println(obl + "<br>" + e.toString());/*con.close();*/
                    }

                }
            %>
        </form>
        <p>&nbsp;</p>
    </body>
</html>