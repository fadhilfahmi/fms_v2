<%-- 
    Document   : cb_pv_add
    Created on : May 3, 2016, 8:40:28 AM
    Author     : user
--%>

<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.PaymentVoucherDAO"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.CashVoucherDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) PaymentVoucherDAO.getModule();
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" charset="utf-8">

    $(document).ready(function () {
        //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });
        //$('#JVdate').datepicker({ dateFormat: 'yyyy-mm-dd' });

        $('#tarikh').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        }).change(dateChanged)
                .on('changeDate', dateChanged);



        $("#total").keyup(function () {
            var val = $(this).val();
            var to = 'amount';
            //alert(val);
            //alert(checkRegexp(val,to));
            if (checkRegexp(val, to)) {
                $('#res_digit').empty();
                $('#div_digit').removeClass('has-error');
                $('#div_digit').addClass('has-success');
                $('#res_digit').append("<i class=\"fa fa-check-circle right-green\"></i>&nbsp;&nbsp;");
                $('#savebutton').prop('disabled', false);
                if (val.length < 13) {
                    convertRM(val, to);
                }

            } else {
                $('#res_digit').empty();
                $('#div_digit').removeClass('has-success');
                $('#div_digit').addClass('has-error');
                $('#res_digit').append("<i class=\"fa fa-exclamation-circle fail-red\"></i>&nbsp;&nbsp;Numeric Formatted Only");
                $('#savebutton').prop('disabled', true);
            }
            //
        });

        $('#getbuyer').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Buyer Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_buyer.jsp?name=bname&code=bcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });
    

        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

     

        $("#paymentmode").change(function () {

            var thisval = $(this).val();

            if (thisval == 'Cheque') {
                $('#getcheque').prop('disabled', false);
            } else {
                $('#getcheque').prop('disabled', true);
            }

        });

        $('#getcheque').click(function (e) {

            var a = 'cekno';
            var bankcode = $('#bankcode').val();
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Cheque No',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_cheque.jsp?bankcode=' + bankcode + '&cekcolumn=' + a);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addpv" ><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>



            <form data-toggle="validator" role="form" id="saveform">
                <input type="hidden" name="prepareid" id="pid" value="<%= log.getUserID()%>">
                <input type="hidden" name="preparename" id="pname" value="<%= log.getFullname()%>">
                <input type="hidden" name="preparedate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
                <input type="hidden" name="coacode" id="coacode" value="">
                <input type="hidden" name="coadesc" id="coadesc" value="">
                <input type="hidden" class="form-control input-sm" id="estatecode" name="estatecode" value="<%= log.getEstateCode()%>" > 
                <input type="hidden" class="form-control input-sm" id="estatename" name="estatename" value="<%= log.getEstateDescp()%>"> 

                <div class="col-sm-6">
                    <label for="inputName" class="control-label">Voucher Setting</label>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Voucher No &nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="voucherno" name="voucherno" placeholder="Auto Generated" autocomplete="off" readonly>   
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-sm-12">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Remarks &nbsp&nbsp<span class="res_code"></span></label>
                                    <textarea class="form-control" rows="3"  id="remarks" name="remarks"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Date &nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="tarikh" name="tarikh" placeholder="Date of Payment Voucher" autocomplete="off" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">   
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Year &nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="year" name="year" placeholder="Code" autocomplete="off" value="<%= AccountingPeriod.getCurYearByCurrentDate() %>">   
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Period &nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="period" name="period"  autocomplete="off" value="<%= AccountingPeriod.getCurPeriodByCurrentDate() %>">   
                                </div>
                            </div>
                        </div>
                    </div>

                    <label for="inputName" class="control-label">Payment Info&nbsp&nbsp<span class="res_code"></span></label>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group" id="div_digit">
                                    <label for="inputName" class="control-label">RM (Digit) &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                    <input type="text" class="form-control input-sm" id="total" name="amount" placeholder="0.00" autocomplete="off" > 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">RM (In Words) &nbsp&nbsp<span class="res_code"></span></label>
                                    <textarea class="form-control" rows="3"  id="amount" name="rm"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Payment Method &nbsp&nbsp<span class="res_code"></span></label>
                                    <select class="form-control input-sm" id="paymentmode" name="paymentmode">
                                        <%= ParameterDAO.parameterList(log, "Payment Method", "")%>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Bank Code &nbsp&nbsp<span class="res_code"></span></label>
                                    <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="bankcode" name="bankcode" placeholder="" autocomplete="off" >
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-sm" id="getbank" title=""><i class="fa fa-cog"></i> Get Bank</button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Bank Name&nbsp&nbsp<span class="res_code"></span></label>

                                    <input type="text" class="form-control input-sm" id="bankname" name="bankname" placeholder="" autocomplete="off" >

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Cheque Number&nbsp&nbsp<span class="res_code"></span></label>
                                    <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="cekno" name="cekno" placeholder="" autocomplete="off" readonly > 
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-sm" id="getcheque" title="" disabled><i class="fa fa-cog"></i></button></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <label for="inputName" class="control-label">Pay To Info</label>
                    <div class="well">
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Paid Type&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                    <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="paidtype" name="paidtype" readonly>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-sm" type="button" id="gettype"><i class="fa fa-cog"></i> Paid Type</button>
                                        </span>
                                    </div>    
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Paid Code &nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="paidcode" name="paidcode" placeholder="" autocomplete="off" >   
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Paid Name&nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="paidname" name="paidname" placeholder="" autocomplete="off" > 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Address &nbsp&nbsp<span class="res_code"></span></label>
                                    <textarea class="form-control" rows="3"  id="paidaddress" name="paidaddress"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group" id="div_digit">
                                    <label for="inputName" class="control-label">Postcode &nbsp&nbsp</label>
                                    <input type="text" class="form-control input-sm" id="paidpostcode" name="paidpostcode" placeholder="" autocomplete="off" > 
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group" id="div_digit">
                                    <label for="inputName" class="control-label">City &nbsp&nbsp</label>
                                    <input type="text" class="form-control input-sm" id="paidcity" name="paidcity" placeholder="" autocomplete="off" > 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group" id="div_digit">
                                    <label for="inputName" class="control-label">State &nbsp&nbsp</label>
                                    <input type="text" class="form-control input-sm" id="paidstate" name="paidstate" placeholder="" autocomplete="off" > 
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">GST ID&nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="gstid" name="gstid" placeholder="" autocomplete="off" > 
                                </div>
                            </div>
                        </div>
                    </div>

                    <label for="inputName" class="control-label">Additional Info</label><small>| Optional, enter when needed.</small>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Rounding Adjustment Account Code&nbsp&nbsp<span class="res_code"></span></label>
                                    <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="racoacode" name="racoacode">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-sm getaccount" type="button" id="racoacode" id1="racoadesc"><i class="fa fa-cog"></i> Account</button>
                                        </span>
                                    </div>  
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Rounding Adjustment Account Description&nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="racoadesc" name="racoadesc" placeholder="" autocomplete="off" > 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>