<%-- 
    Document   : em_roc_view
    Created on : Jun 13, 2017, 2:18:34 PM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.dao.financial.employee.EmROCDAO"%>
<%@page import="com.lcsb.fms.model.financial.employee.EmRoc"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.BuyerDAO"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Buyer"%>
<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
EmRoc view = (EmRoc) EmROCDAO.getROC(log, request.getParameter("refer"));
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
           
            
              $( ".printbutton" ).click(function() {
            printReport($("#buyerdiv").html());
        });
                
              
         });
        </script>
            <div id ="maincontainer">
                <div class="row">
                    <div class="col-sm-3">
                    <button id="<%//= v.getRefer()%>" class="btn btn-default btn-xs printbutton" title="<%//= mod.getModuleID()%>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
                    </div>
                </div>
                    <br>
                    <div id="buyerdiv">
                <table id="table_2" cellspacing="0">
                 <tr>
                     <td width="30%" colspan="2" class="bd_bottom" align="left" valign="top"><strong>Report of Change</strong></td>
               </tr>  
               <tr>
                   <td width="30%" class="bd_bottom" align="left" valign="top">Staff ID</td>
                   <td width="70%" class="bd_bottom" align="left"><%= view.getStaffid()%></td>
               </tr>
               <tr>
                   <td class="bd_bottom" align="left" valign="top">Staff Name</td>
                   <td class="bd_bottom" align="left"> 
                       <%= view.getStaffname()%>
                   </td>
               </tr>
               
               <tr>
                   <td class="bd_bottom" align="left" valign="top">Staff Designation</td>
                   <td class="bd_bottom" align="left"> 
                       <%= view.getStaffdesignation()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">File No.</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getFileno()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">RoC</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getRchange()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Date Effective</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getDateeffective()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Amount</td>
                   <td class="bd_bottom" align="left">
                       <%= GeneralTerm.currencyFormat(view.getAmount()) %>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Remark</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getRemark()%>
                   </td>
               </tr>
               
          </table>
                    </div>
            </div>
      

      
      
      
      

