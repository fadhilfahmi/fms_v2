<%-- 
    Document   : cf_taxtable
    Created on : Jun 21, 2017, 8:52:35 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.TaxTableDAO"%>
<%@page import="com.lcsb.fms.model.setup.configuration.TaxTable"%>
<%@page import="java.util.List"%>
<%
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
%>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            
            <table class="table table-bordered table-striped  table-hover">
                <thead>
                    <tr>
                        <th>P (RM)</th>
                        <th>M (RM)</th>
                        <th>R (%)</th>
                        <th>B (1 & 3) (RM)</th>
                        <th>B (2) (RM)</th>
                    </tr>
                </thead>
                <tbody class="activerowm tdrow">
                    <%List<TaxTable> listAll = (List<TaxTable>) TaxTableDAO.getListTax(log);
                   
                                for (TaxTable j : listAll) {%>
                    <tr>
                        <td width="25"><%= GeneralTerm.currencyFormat(j.getPstart()) %> - <%= GeneralTerm.currencyFormat(j.getPend()) %></td>
                        <td width="20"><span class="pull-right"><%= GeneralTerm.currencyFormat(j.getM()) %></span></td>
                        <td width="15"><span class="pull-right"><%= GeneralTerm.currencyFormat(j.getR()) %></span></td>
                        <td width="20"><span class="pull-right"><%= GeneralTerm.currencyFormat(j.getB13()) %></span></td>
                        <td width="20"><span class="pull-right"><%= GeneralTerm.currencyFormat(j.getB2()) %></span></td>
                    </tr>
                    <%}%>
                </tbody>
            </table>
        </div>
    </div>
</div>    
