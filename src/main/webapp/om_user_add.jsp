<%-- 
    Document   : cp_board_add
    Created on : May 25, 2016, 2:42:34 PM
    Author     : user
--%>
<%@page import="com.lcsb.fms.dao.administration.om.UserDAO"%>
<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
Module mod = (Module) UserDAO.getModule();

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <script src="jsfunction/jquery-1.12.0.min.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/formValidation.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.js"></script>

<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
        
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
            
            $('.getstaff').click(function(e){
              var a = $(this).attr('id');
              var b = $(this).attr('id1');
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Staff Information',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_staff.jsp?code='+a+'&descp='+b);
                        $('body').on('click', '.thisresult', function(event){
                            dialog.close();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
           
            $('#getbank').click(function(e){
               
               var a = 'bankcode';
               var b = 'bankname';
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Taxation',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_bank.jsp?code='+a+'&name='+b);
                        $('body').on('click', '.thisresult', function(event){
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
           $('#confirmpassword').keyup(function(e){
               
               var conpass = $(this).val();
               var realpass = $('#password').val();
               
               if(conpass==realpass){
                   $('#res_pass').html('');
                   $('#res_pass').append("<i class=\"fa fa-check-circle right-green\"></i>&nbsp;&nbsp;");
                   $('#savebutton').prop('disabled', false);
               }else{
                   $('#res_pass').html('');
                   $('#res_pass').append("<i class=\"fa fa-exclamation-circle fail-red\"></i>&nbsp;Password not match!");
                   $('#savebutton').prop('disabled', true);
               }
              
               
               return false;
           });
           
           $('#level').change(function(e){
               
               var lvl = $(this).val();
               var lvlcode = '';
               
               if(lvl=='Administrator'){
                   lvlcode = '1';
               }else if(lvl=='Staff'){
                   lvlcode = '2';
               }
               
               $('#level_id').val(lvlcode);
               return false;
           });
           
             $('#date_inactive').datepicker({
               format:'yyyy-mm-dd',
               defaultDate:'now',
               autoclose:true
           });
            $('#date_active').datepicker({
               format:'yyyy-mm-dd',
               defaultDate:'now',
               autoclose:true
           });
            
          $('#getaccount').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Chart of Account',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_coa.jsp');
                        $('body').on('click', '.thisresult', function(event){
                            dialog.close();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
        });
        
       </script>
    </head>
    <body>
        <div id ="maincontainer">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
              <td width="74%" class="borderbot" align="left"><span class="bigfonttitle"><%= mod.getModuleDesc() %></span>&nbsp;&nbsp;<span class="midfonttitle">Add New <%= mod.getModuleDesc() %></span></td>
		<td width="26%" class="borderbot" align="right">&nbsp;</td>
	  </tr>
  </table>
	<br>
   <div class="partition"> 
       <div class="headofpartition"></div>
       <div class="bodyofpartition">
  <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
			<td valign="middle" align="left">
				<button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                                <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="addprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
			</td>
		</tr>
  </table>
  <br>
  

  
<form data-toggle="validator" role="form" id="saveform">
    <input type="hidden" name="prepareid" id="pid" value="<%= log.getUserID()%>">
    <input type="hidden" name="preparename" id="pname" value="<%= log.getFullname()%>">
    <input type="hidden" name="preparedate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp() %>">
    <div class="well">
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">User Name &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="user" name="user" autocomplete="off" placeholder="Less than 20 characters">   
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Password &nbsp&nbsp<span class="res_code"></span></label>
                <input type="password" class="form-control input-sm" id="password" name="password" autocomplete="off" placeholder="" >   
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Confirm Password &nbsp&nbsp<span class="res_style pull-right" id="res_pass"></span></label>
                <input type="password" class="form-control input-sm" id="confirmpassword" name="confirmpassword" autocomplete="off" placeholder="">   
             </div>
        </div>
        </div>
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Level &nbsp&nbsp<span class="res_code"></span></label>
                <select class="form-control input-sm" id="level" name="level">
                    <%= ParameterDAO.parameterList(log,"User Level","") %>
                </select> 
                <input type="hidden" name="level_id" id="level_id" value="1">
             </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">Staff ID&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span id="res_digit" class="res_style"></span></label>
                <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="staff_id" name="staff_id" readonly>
                                        <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getstaff" type="button" id="staff_id" id1="staff_name"><i class="fa fa-cog"></i> Staff</button>
                                        </span>
                                    </div>  
             </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Staff Name&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="staff_name" name="staff_name" readonly>
             </div>
        </div>
      
   
    </div>
    </div>
        
   
          
    
    
         
        
    
    
    
</form>
   
     </div>
     </div>
      </div>
      

    </body>
</html>