<%-- 
    Document   : list_invoice_view
    Created on : Oct 19, 2016, 3:41:36 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.model.financial.ap.VendorPayee"%>
<%@page import="com.lcsb.fms.dao.financial.ap.VendorPaymentDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>

                                <div class="list-group"><%

                                            String keyword = request.getParameter("keyword");
                                            List<VendorPayee> slist = (List<VendorPayee>) VendorPaymentDAO.getSupplierHasInvoice(log,keyword);
                                            
                                            if(slist.isEmpty()){%>
                                            <a class="list-group-item thisresult-payee">
                                                <div id="leftdiv>"<span class="font-small-red"><strong>No invoice available.</strong></span></div>
                                            </a>
                                            <%}
                                            for (VendorPayee c : slist) { 
                                               %>

                                            <a href="<%= c.getSuppname()%>" id="<%= c.getSuppcode()%>"   title="" class="list-group-item thisresult-payee">
                                                <div id="left_div"><%= c.getSuppcode()%></div>
                                                <div id="right_div"><%= c.getSuppname()%><span class="badge pull-right"><%= c.getTotalInvoice() %></span></div>
                                            </a>

                                               <%    
                                            } 
                                        %>
                                </div>