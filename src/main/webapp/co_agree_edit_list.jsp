<%-- 
    Document   : ap_pv_edit_list
    Created on : May 11, 2016, 9:06:22 AM
    Author     : user
--%>
<%@page import="com.lcsb.fms.dao.financial.ap.VendorPaymentDAO"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucher"%>

<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoice"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoiceItem"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
PaymentVoucherItem vi = (PaymentVoucherItem) VendorPaymentDAO.getPYVitem(log,request.getParameter("refer"));
PaymentVoucher v = (PaymentVoucher) VendorPaymentDAO.getPYV(log,request.getParameter("refer"));
Module mod = (Module) VendorPaymentDAO.getModule();
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
           
            

              $( ".editmain" ).click(function() {
                  //var a = $("form").serialize();
                  var a = $(this).attr('id');
                  var b = $(this).attr('href');
                 
                 $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=editmain&refer="+a,
                                            success: function (result) {
                                            // $("#haha").html(result);
                                            $('#herey').empty().html(result).hide().fadeIn(300);
                                        }});
                                        return false;
               
                   
              });
              
              $( ".edititem" ).click(function() {
                  //var a = $("form").serialize();
                  var a = $(this).attr('id');
                  var b = $(this).attr('href');
                 
                 $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=edititem&referno="+a,
                                            success: function (result) {
                                            // $("#haha").html(result);
                                            $('#herey').empty().html(result).hide().fadeIn(300);
                                        }});
                                        return false;
               
                   
              });
              
              $( ".deletecvitem" ).click(function() {
                  //var a = $("form").serialize();
                  var a = $(this).attr('id');
                  var b = $(this).attr('href');
                 
                 BootstrapDialog.confirm({
                                            title: 'Confirmation',
                                            message: 'Are you sure to delete?',
                                            type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                            closable: true, // <-- Default value is false
                                            draggable: true, // <-- Default value is false
                                            btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                            btnOKLabel: 'Delete', // <-- Default value is 'OK',
                                            btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                                            callback: function(result) {
                                                // result will be true if button was click, while it will be false if users close the dialog directly.
                                                if(result) {
                                                    $.ajax({
                                                        url: "PathController?moduleid=020104&process=deletejvitem&referno="+a,
                                                        success: function (result) {
                                                        $('#herey').empty().html(result).hide().fadeIn(300);
                                                        }
                                                    });
                                                }
                                            }
                                        });
                 
                 
                
                                        return false;
               
                   
              });
              
              $( ".gotodelete" ).button({
                    icons: {
                      primary: "ui-icon-trash"
                    }
              })
              .click(function() {
                  //var a = $("form").serialize();
                  var a = $(this).attr('id');
                  var b = $(this).attr('href');
                  
                  $.ajax({
                        url: "PathController?moduleid=<%//=moduleid%>&process=deletesub&referenceno="+a+"&type="+b,
                        success: function (result) {
                        // $("#haha").html(result);
                        $('#maincontainer').empty().html(result).hide().fadeIn(300);
                    }});
                    return false;
               
                    
              });
              
              
                
              
                 
                 
                 
                  
                 
                 
              
         });
        </script>
            <div id ="maincontainer">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
              <td width="74%" class="borderbot" align="left"><span class="bigfonttitle"><%= mod.getModuleDesc() %></span>&nbsp;&nbsp;<span class="midfonttitle">Editing List</span></td>
		<td width="26%" class="borderbot" align="right">&nbsp;</td>
	  </tr>
  </table>
	<br>
   <div class="partition"> 
       <div class="headofpartition"></div>
       <div class="bodyofpartition">
 <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
			<td valign="middle" align="left">
				<button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                                
			</td>
		</tr>
  </table>
  <br>
  
 <table class="table table-bordered table-striped" id="dataTables">
  <tr>
      <td class="tdrow">&nbsp;<strong><%= v.getRefer()%></strong></td>
    <td class="tdrow">&nbsp;<strong><%=  v.getRemarks()%></strong></td>
    <td class="tdrow">&nbsp;<strong></strong></td>
    <td class="tdrow" align="right"><%= GeneralTerm.currencyFormat(v.getAmount()) %></td>
    <td class="tdrow" align="right">&nbsp;
        <div class="btn-group btn-group-xs" role="group" aria-label="...">
            <button id="<%= v.getRefer()%>" class="btn btn-default btn-xs editmain" title="<%= mod.getModuleID() %>" id="<%//= pvi.getRefer() %>" name="viewlist"><i class="fa fa-pencil"></i></button>
        <button id="savebutton"  class="btn btn-default btn-xs deletecvmain" title="<%= mod.getModuleID() %>" id="<%//= pvi.getRefer() %>" name="addjv"><i class="fa fa-trash-o"></i></button>
        </div></td>
  </tr>
  <%
  List<PaymentVoucherItem> listAll = (List<PaymentVoucherItem>) VendorPaymentDAO.getAllPYVItem(log,v.getRefer());
    for (PaymentVoucherItem j : listAll) {
  %>
  <tr>
    <td class="tdrow">&nbsp;&nbsp;&nbsp;<%= j.getVoucer()%></td>
    <td class="tdrow">&nbsp;<%= j.getCoacode()%> - <%= j.getCoadescp()%></td>
    <td class="tdrow">&nbsp;<%= j.getSacode()%> - <%= j.getSadesc()%></td>
    <td class="tdrow" align="right">&nbsp;<%= GeneralTerm.currencyFormat(j.getAmount()) %></td>
    <td class="tdrow" align="right">&nbsp;
        <!--<a href="sub" id="<%//= pvi.getRefer() %>" class="goto">Edit</a>&nbsp;
        <a href="sub" id="<%//= pvi.getNovoucher() %>" class="gotodelete">Delete</a>-->
        
    <div class="btn-group btn-group-xs" role="group" aria-label="...">
                               
                                <button id="<%= j.getVoucer()%>" class="btn btn-default btn-xs edititem" title="<%= mod.getModuleID() %>" id="<%//= pvi.getRefer() %>" name="viewlist"><i class="fa fa-pencil"></i></button>
                                <button id="<%= j.getVoucer() %>"  class="btn btn-default btn-xs deletecvitem" title="<%= mod.getModuleID() %>" id="<%//= pvi.getRefer() %>" name="addjv"><i class="fa fa-trash-o"></i></button>
                              </div>
    </td>
  </tr>
  <%
    }     
  %>
</table>

     </div>
     </div>
      </div>







      

      
