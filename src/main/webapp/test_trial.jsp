<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,Report.*" errorPage="" %><%Connection con = (Connection) session.getAttribute("con");%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd" >
<html>
    <head>
        <title>Untitled Document</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <style type="text/css">
            <!--
            .style3 {font-size: 9px}
            .style5 {font-size: 9px; font-family: Arial, Helvetica, sans-serif; }
            .style6 {font-family: Arial, Helvetica, sans-serif}
            .style21 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; font-weight: bold; }
            thead {display: table-header-group; }
            tfoot {display: table-footer-group; }
            thead th, thead td {position: static; }
            -->
        </style>
        <link href="css/ipamis_report.css" rel="stylesheet" type="text/css">
    </head>
    <script language="JavaScript" type="text/JavaScript">
        function periksa()
        {
        document.form1.view.value="true"
        document.form1.submit()
        }

        function tukar()
        {
        ida=document.form1.vby.value

        if(ida=="1")
        {document.form1.loccode1.disabled=true;
        document.form1.loccode2.disabled=true} 
        else if(ida=="2")
        {document.form1.loccode1.disabled=false;
        document.form1.loccode2.disabled=true}
        else if(ida=="3")
        {document.form1.loccode1.disabled=true;
        document.form1.loccode2.disabled=false}
        }

        function belakang()
        {
        history.go(-1)
        }
        function cetak()
        {
        window.print()
        }

        function printSetup()


        {
        var a = document.all.item("noprint");


        if (a!=null) {


        if (a.length!=null) {
        //multiple tags found


        for (i=0; i< a.length; i++) {
        a(i).style.display = window.event.type == "beforeprint" ? "none" :"inline";
        }
        } else 
        //only one tag
        a.style.display = window.event.type == "beforeprint" ? "none" :"inline";
        }
        }
    </script>
    <%
        if (request.getParameter("view") == null) {
    %>
    <link href="css/mainForm.css" rel="stylesheet" type="text/css" />
    <%
        }
    %>
    <body onbeforeprint="printSetup()" onafterprint="printSetup()">

        <form name="form1" method="post" action="gl_fr_Trial_Balance2.jsp">
            <%
                /*Class.forName ("org.gjt.mm.mysql.Driver");*/
             /*Connection con = DriverManager.getConnection("jdbc:mysql://localhost/IPAMIS_db","root","");*/
                if (request.getParameter("view") == null) {
            %>
            <fieldset>
                <legend>Trial Balance - Sheet No. 3</legend>
                <ol>
                    <li>
                        <label for="year">Year</label>
                        <input name="year" type="text" id="year" value="<%=(String) session.getAttribute("year")%>">
                    </li>
                    <li>
                        <label for="period">Period</label>
                        <select name="period" id="period">
                            <option value="<%=(String) session.getAttribute("period")%>" selected="selected"><%=(String) session.getAttribute("period")%></option>
                            <option value="1" >1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>			
                    </li>
                    <li>
                        <label>Type</label>
                        <select name="otype" id="select2">
                            <option value="1">Opening Balance</option>
                            <option value="2">This Month</option>
                            <option value="3" selected="selected">To Date</option>
                        </select>			
                    </li>
                    <li>
                        <label for="vby">View By</label>
                        <select name="vby" id="vby" onChange="tukar()">
                            <option value="1">Consolidation</option>
                            <option value="2">Management</option>
                            <option value="3" selected="selected">Estate</option>
                        </select>			
                    </li>
                    <li>
                        <label for="loccode1">&nbsp;&nbsp;Management</label>
                        <select name="loccode1" disabled="true" id="loccode1">
                            <%
                                Statement stmmnt = con.createStatement();
                                ResultSet setmnt = stmmnt.executeQuery("select code,name from ce_manage order by code");
                                while (setmnt.next()) {
                                    out.println("<option value=\"" + setmnt.getString("code") + "\">" + setmnt.getString("code") + " - " + setmnt.getString("name") + "</option>");
                                }
                            %>
                        </select>			
                    </li>
                    <li>
                        <label for="loccode2">&nbsp;&nbsp;Estate</label>
                        <select name="loccode2" id="loccode2">
                            <%
                                Statement stmest = con.createStatement();
                                ResultSet setest = stmest.executeQuery("select code,name from ce_estate order by code");
                                while (setest.next()) {
                                    out.println("<option value=\"" + setest.getString("code") + "\">" + setest.getString("code") + " - " + setest.getString("name") + "</option>");
                                }
                            %>		
                        </select>			
                    </li>		
                    <li>
                        <label for="vall">View All Account</label>
                        <select name="vall" id="vall">
                            <option value="No" selected>No</option>
                            <option value="Yes">Yes</option>
                        </select>			
                    </li>	
                    <li>
                        <input type="button" name="Submit3" onClick="location.href = 'gl_fr_incomeexpenses.jsp'" value="<<">
                        <input type="button" name="Button" value="Generate Report" onClick="periksa()">
                        <input type="button" name="Submit4" onClick="location.href = 'gl_Financial_Report.jsp'" value="Return To Index">
                        <input type="button" name="Submit" onClick="location.href = 'gl_fr_summaryexp.jsp'" value=">>">
                        <input type="hidden" name="view" value="view">
                    </li>									
                </ol>
            </fieldset>

            <%
            } else if (request.getParameter("view").equalsIgnoreCase("true")) {
                String sqlS = "";
                String allv = request.getParameter("vall");
                boolean vall = false;

          //	disable temporary
          //	if(allv.equalsIgnoreCase("Yes"))vall=true;
                vall = false;
                String year = request.getParameter("year");
                String period = request.getParameter("period");
                String otype = request.getParameter("otype");
                String vby = request.getParameter("vby");
                String loccode = "";
                String locname = "Consolidation";
                //String tbtable="";
                String oblyp = " where concat(year,period)='" + year.concat(period) + "'";
                String oblby = "";
                String obltype = "";
                String obl = "";
                String tbtable = "";

                if (vby.equalsIgnoreCase("2")) {
                    loccode = request.getParameter("loccode1");
                    oblby = " and loccode like concat('" + loccode + "','%')";
                    tbtable = "ce_manage";
                } else if (vby.equalsIgnoreCase("3")) {
                    loccode = request.getParameter("loccode2");
                    oblby = " and loccode like concat('" + loccode + "','%')";
                    tbtable = "ce_estate";
                }
                Statement stmname = con.createStatement();
                ResultSet setname = null;
                if (!vby.equalsIgnoreCase("1")) {
                    //Statement stmname = con.createStatement ();
                    setname = stmname.executeQuery("select code,name from " + tbtable + " where code ='" + loccode + "'");

                    if (setname.next()) {
                        locname = setname.getString("name");
                    }
                }
                //if(otype.equalsIgnoreCase("1"))	{obltype=" from gl_openingbalance ";tbtable="gl_openingbalance";}
                String obltype2 = "";
                String syaratre = "";
          //String syaratre2="";
                if (otype.equalsIgnoreCase("1")) {
                    obltype = "  gl_openingbalance ";
                    obltype2 = "  gl_closingbalance ";

                } else if (otype.equalsIgnoreCase("2")) {
                    obltype = "  gl_openingbalance_detail ";
                    obltype2 = "  gl_openingbalance_detail ";
                    syaratre = "and t1.coacode not like  '3103%'";
          //		syaratre2=" acccode <> "
                } else if (otype.equalsIgnoreCase("3")) {
                    obltype = "  gl_closingbalance ";
                    obltype2 = "  gl_closingbalance ";
                }
                /*
                  Trialbalance pdfrpt=new Trialbalance(con,year,period,vby,loccode,otype,application.getRealPath(""));
                  try{
                  pdfrpt.startpdf(year,period,vby,loccode,otype);
                  }catch(Exception x)
                  {
                  out.println(x);
                  }
                 */

            %>
            <table width="100%" border="0" align="center">
                <tr> 
                    <td><div align="right"><strong><font size="1" face="Verdana, Arial, Helvetica, sans-serif">SHEET 
                                    NO 3</font> </strong></div></td>
                </tr>
            </table>


            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr> 
                    <td width="7%" rowspan="3"><img src="<%=(String) session.getAttribute("companyLogo")%>" width="70" height="70"></td>
                    <td width="92%"><strong><font size="3" face="Arial, Helvetica, sans-serif">DOMINION SQUARE SDN BHD </font></strong></td>
                </tr>
                <tr> 
                    <td><font size="3" face="Arial, Helvetica, sans-serif"><strong><%=locname.toUpperCase()%></strong></font></td>
                </tr>
                <tr> 
                    <td><strong><font size="3" face="Arial, Helvetica, sans-serif">TRIAL BALANCE 
                                FOR PERIOD <%=period%> YEAR <%=year%></font></strong></td>
                </tr>
            </table>

            <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
                <thead>
                    <tr bordercolor="#000000"> 
                        <th width="10%" style="border-style: solid; border-width: 1px"><strong><font size="1" face="Arial, Helvetica, sans-serif">&nbsp;ACCOUNT 
                                    CODE</font></strong></th>
                        <th width="29%" style="border-style: solid; border-width: 1px;border-left-style: none"><strong><font size="1" face="Arial, Helvetica, sans-serif">&nbsp;ACCOUNT 
                                    DESCRIPTION</font></strong></th>
                        <th width="30%" style="border-style: solid; border-width: 1px;border-left-style: none"><strong><font size="1" face="Arial, Helvetica, sans-serif">SUB</font></strong></th>
                        <th width="10%" style="border-style: solid; border-width: 1px;border-left-style: none"><strong><font size="1" face="Arial, Helvetica, sans-serif">&nbsp;STAGE</font></strong></th>
                        <th width="10%" style="border-style: solid; border-width: 1px;border-left-style: none"><strong><font size="1" face="Arial, Helvetica, sans-serif">&nbsp;ENTERPRISE</font></strong></th>
                        <th width="10%" style="border-style: solid; border-width: 1px;border-left-style: none"><div align="right"><strong><font size="1" face="Arial, Helvetica, sans-serif">DEBIT&nbsp;</font></strong></div></th>

                        <th width="1%" style="border-style: solid; border-width: 1px;border-right-style: none;border-left-style: none">&nbsp;</th>
                        <th width="10%" style="border-style: solid; border-width: 1px;border-left-style: none"><div align="right"><strong><font size="1" face="Arial, Helvetica, sans-serif">CREDIT&nbsp;</font></strong></div></th>
                    </tr>
                </thead>
                <%

                    try {
                        DecimalFormat dfx = new DecimalFormat("#0.00");
                        int rowsAffected = 0;
                        Statement updvallv = con.createStatement();
                        sqlS = "delete from gl_rpt_trial_balance ";
                        rowsAffected = updvallv.executeUpdate(sqlS);
                        double debit = 0;
                        double credit = 0;
                        if (syaratre.equalsIgnoreCase("")) {
                            String xxc = "gl_openingbalance";
                            Statement stmre = con.createStatement();
                            ResultSet setre = stmre.executeQuery("select count(*) as cnt   from  gl_openingbalance_detail where coacode like '3103%' and concat(year,period)=concat('" + year + "','" + period + "') " + oblby + "  group by coacode");
                            if (setre.next()) {
                                if (setre.getInt("cnt") > 0) {
                                    xxc = "gl_closingbalance";
                                }
                            }

                            setre = stmre.executeQuery("select coacode,coadesc,ifnull(sum(debit),0) as sdebit, ifnull(sum(credit),0) as scredit   from  " + xxc + " where coacode like '3103%' and concat(year,period)=concat('" + year + "','" + period + "') " + oblby + "  group by coacode");
                            while (setre.next()) {
                                double debitx = setre.getDouble("sdebit");
                                double creditx = setre.getDouble("scredit");

                                double beza = debitx - creditx;
                                if (beza > 0) {
                                    debitx = beza;
                                    creditx = 0;
                                } else if (beza < 0) {
                                    debitx = 0;
                                    creditx = (beza * (-1));
                                } else {
                                    debitx = 0;
                                    creditx = 0;
                                }

                                debit += debitx;
                                credit += creditx;
                                Statement updre = con.createStatement();
                                sqlS = "insert into gl_rpt_trial_balance ";
                                sqlS += " (acccode,accdesc,stgcode,stgdesc,entcode,entdesc,satype,sacode,sadesc,debit,credit) ";
                                sqlS += " values('" + setre.getString("coacode") + "','" + setre.getString("coadesc") + "','--','--','--','--','--','--','--'," + dfx.format(debitx) + "," + dfx.format(creditx) + ") ";
                                obl = sqlS;
                                rowsAffected = updre.executeUpdate(sqlS);
                            }
                        }
                        String extx = "coacode,coadesc,applevel,satype,sacode,sadesc";
                        String extx2 = "coacode,applevel,satype,sacode";
                        String syaratxx = " coacode not like  '3103%' and year='" + year + "' and period='" + period + "'";
                        String tablex = obltype;
                        /*	
                    if(vall)
                    {	
                            syaratxx="code<>'3103' and finallvl='yes'";
                            extx2="code,applevel,cttype,ctcode";
                            tablex="chartofacccount";
                            extx="code as coacode,descp as coadesc,applevel,cttype,ctcode,ctdesc";
                    }
                         */
                        Statement stmcoa = con.createStatement();
                        Statement updvall = con.createStatement();
                        ResultSet setcoa = stmcoa.executeQuery("select " + extx + " from  " + tablex + " where " + syaratxx + "  group by " + extx2);
                        int i = 0;
                        boolean masuk = false;
                        //out.println("select "+ extx +" from  "+ tablex +" where "+syaratxx+"  group by "+extx2+"<br><br>");
                        while (setcoa.next()) {
                            if (setcoa.getString("applevel").equalsIgnoreCase("field enterprise")) {
                                obl = "select ifnull(sum(t1.debit),0) as sdebit,ifnull(sum(t1.credit),0) as scredit,t1.stagecode,t1.stagedesc,right(t1.loccode,2) as entcode,t2.descp as entdescp,t1.satype,t1.sacode,t1.sadesc from " + obltype + " as t1 left join enterprise_info as t2 on t2.code=right(t1.loccode,2) where concat(t1.year,t1.period)=concat('" + year + "','" + period + "') and t1.loccode like '" + loccode + "%' and t1.satype='" + setcoa.getString("satype") + "' and t1.sacode='" + setcoa.getString("sacode") + "' and t1.coacode='" + setcoa.getString("coacode") + "' " + syaratre + "  and t1.applevel='Field Enterprise' group by t1.coacode,t1.stagecode,right(t1.loccode,2) order by t1.coacode,t1.stagecode,t1.satype,t1.sacode,right(t1.loccode,2)";
                                //out.println(obl+"<br><br>");
                                masuk = false;
                                Statement stmcob = con.createStatement();
                                ResultSet setcob = stmcob.executeQuery(obl);
                                while (setcob.next()) {
                                    masuk = true;
                                    if ((setcob.getDouble("sdebit") != 0) || (setcob.getDouble("scredit") != 0) || vall) {

                                        double debitx = setcob.getDouble("sdebit");
                                        double creditx = setcob.getDouble("scredit");
                                        double beza = debitx - creditx;
                                        if (beza > 0) {
                                            debitx = beza;
                                            creditx = 0;
                                        } else if (beza < 0) {
                                            debitx = 0;
                                            creditx = (beza * (-1));
                                        } else {
                                            debitx = 0;
                                            creditx = 0;
                                        }
                                        debit += debitx;
                                        credit += creditx;
                                        //i++;

                                        sqlS = "insert into gl_rpt_trial_balance ";
                                        sqlS += " (acccode,accdesc,stgcode,stgdesc,entcode,entdesc,satype,sacode,sadesc,debit,credit) ";
                                        sqlS += " values('" + setcoa.getString("coacode") + "','" + setcoa.getString("coadesc") + "','" + setcob.getString("stagecode") + "','" + setcob.getString("stagedesc") + "','" + setcob.getString("entcode") + "','" + setcob.getString("entdescp") + "','" + setcob.getString("satype") + "','" + setcob.getString("sacode") + "','" + setcob.getString("sadesc") + "'," + dfx.format(debitx) + "," + dfx.format(creditx) + ") ";
                                        obl = sqlS;
                                        rowsAffected = updvall.executeUpdate(sqlS);

                                    }
                                }
                                if (!masuk && vall) {
                                    sqlS = "insert into gl_rpt_trial_balance ";
                                    sqlS += " (acccode,accdesc,stgcode,stgdesc,entcode,entdesc,satype,sacode,sadesc,debit,credit) ";
                                    sqlS += " values('" + setcoa.getString("coacode") + "','" + setcoa.getString("coadesc") + "','--','--','--','--','--','--','--',0.00,0.00) ";
                                    obl = sqlS;
                                    rowsAffected = updvall.executeUpdate(sqlS);
                                }
                            } else {

                                obl = "select ifnull(sum(debit),0) as sdebit,ifnull(sum(credit),0) as scredit,satype,sacode,sadesc from ".concat(obltype).concat(" as t1 ").concat(oblyp).concat(oblby).concat(" and coacode='" + setcoa.getString("coacode") + "' and satype='" + setcoa.getString("satype") + "' and sacode='" + setcoa.getString("sacode") + "' " + syaratre + " and applevel='Estate' group by  coacode,satype,sacode ");
                                masuk = false;
                                //out.println(obl+"<br><br>");
                                Statement stmcobx = con.createStatement();
                                ResultSet setcobx = stmcobx.executeQuery(obl);
                                while (setcobx.next()) {
                                    masuk = true;
                                    if ((setcobx.getDouble("sdebit") != 0) || (setcobx.getDouble("scredit") != 0) || vall) {
                                        double debitx = setcobx.getDouble("sdebit");
                                        double creditx = setcobx.getDouble("scredit");
                                        if ((setcobx.getDouble("sdebit") != 0) && (setcobx.getDouble("scredit") != 0)) {
                                            double beza = debitx - creditx;
                                            if (beza > 0) {
                                                debitx = beza;
                                                creditx = 0;
                                            } else if (beza < 0) {
                                                debitx = 0;
                                                creditx = (beza * (-1));
                                            } else {
                                                debitx = 0;
                                                creditx = 0;
                                            }
                                        }
                                        debit += debitx;
                                        credit += creditx;
                                        //i++;

                                        sqlS = "insert into gl_rpt_trial_balance ";
                                        sqlS += " (acccode,accdesc,stgcode,stgdesc,entcode,entdesc,satype,sacode,sadesc,debit,credit) ";
                                        sqlS += " values('" + setcoa.getString("coacode") + "','" + setcoa.getString("coadesc") + "','--','--','--','--','" + setcobx.getString("satype") + "','" + setcobx.getString("sacode") + "','" + setcobx.getString("sadesc") + "'," + dfx.format(debitx) + "," + dfx.format(creditx) + ") ";
                                        obl = sqlS;
                                        rowsAffected = updvall.executeUpdate(sqlS);

                                    }//if((setcobx.getDouble("sdebit")!=0)||(setcobx.getDouble("scredit")!=0)||vall)
                                }//while(setcobx.next())
                                if (!masuk && vall) {
                                    sqlS = "insert into gl_rpt_trial_balance ";
                                    sqlS += " (acccode,accdesc,stgcode,stgdesc,entcode,entdesc,satype,sacode,sadesc,debit,credit) ";
                                    sqlS += " values('" + setcoa.getString("coacode") + "','" + setcoa.getString("coadesc") + "','--','--','--','--','--','--','--',0.00,0.00) ";
                                    obl = sqlS;
                                    rowsAffected = updvall.executeUpdate(sqlS);
                                }
                            }// if(setcoa.getString("applevel").equalsIgnoreCase("field enterprise"))
                        }//while(setcoa.next())  
                        DecimalFormat df = new DecimalFormat(" ###,###,###,##0.00");
                        String syaratlagi = "where debit<>0 or credit<>0";
                        if (vall) {
                            syaratlagi = "";
                        }
                        Statement stmtbrpt = con.createStatement();
                        ResultSet settbrpt = stmtbrpt.executeQuery("select * from gl_rpt_trial_balance " + syaratlagi + " group by  acccode,stgcode,entcode,satype,sacode order by acccode,stgcode,entcode,satype,sacode");
                        while (settbrpt.next()) {
                            i++;
                            String dt = "";
                            String ct = "";

                            if (settbrpt.getDouble("debit") == 0) {
                                dt = "";
                            } else {
                                dt = df.format(settbrpt.getDouble("debit"));
                            }
                            if (settbrpt.getDouble("credit") == 0) {
                                ct = "";
                            } else {
                                ct = df.format(settbrpt.getDouble("credit"));
                            }

                            String satext = "";
                            if (settbrpt.getString("satype").equals("None")) {
                                satext = "";
                            } else {
                                satext = settbrpt.getString("satype").concat(" - ").concat(settbrpt.getString("sacode")).concat(" - ").concat(settbrpt.getString("sadesc"));
                            }

                %>
                <tr> 
                    <td><font size="1" face="Arial, Helvetica, sans-serif"><%=settbrpt.getString("acccode")%></font></td>
                    <td><font size="1" face="Arial, Helvetica, sans-serif"><%=settbrpt.getString("accdesc")%></font></td>
                    <td class="dataNormalText"><%=satext%></td>
                    <td><font size="1" face="Arial, Helvetica, sans-serif"><%=settbrpt.getString("stgdesc")%></font></td>
                    <td><font size="1" face="Arial, Helvetica, sans-serif"><%=settbrpt.getString("entdesc")%></font></td>
                    <td><div align="right"><font size="1" face="Arial, Helvetica, sans-serif"><%=dt%></font></div></td>
                    <td><font size="1" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
                    <td><div align="right"><font size="1" face="Arial, Helvetica, sans-serif"><%=ct%></font></div></td>
                </tr>
                <%}%>
                <tr> 
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><font size="1" face="Arial, Helvetica, sans-serif">&nbsp;TOTAL</font></td>
                    <td bordercolor="#000000" style="border-top-style: solid; border-top-width: 1px;border-bottom-style: solid; border-bottom-width: medium"><div align="right"><strong><font size="1" face="Arial, Helvetica, sans-serif"><%=df.format(debit)%></font></strong></div></td>
                    <td bordercolor="#000000">&nbsp;</td>
                    <td bordercolor="#000000" style="border-top-style: solid; border-top-width: 1px;border-bottom-style: solid; border-bottom-width: medium"><div align="right"><strong><font size="1" face="Arial, Helvetica, sans-serif"><%=df.format(credit)%></font></strong></div></td>
                </tr>
                <tr> 
                    <td><span class="style3"></span></td>
                    <td><span class="style3"></span></td>
                    <td>&nbsp;</td>
                    <td><span class="style3"></span></td>
                    <td><span class="style3"></span></td>
                    <td><span class="style3"></span></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr> 
                    <td colspan="8"><div align="center"><span class="style3"><SPAN id="noprint"> 
                                    <input type="button" name="Button" value="Back" onClick="document.form1.view = false;document.form1.submit();">
                                    <input type="button" name="Submit2" value="Print" onClick="window.print()">
                                </SPAN> </span></div></td>
                </tr>
            </table>
            <%
            //out.println(obl+"<br>");
                    } catch (Exception e) {
                        out.println(obl + "<br>" + e.toString());/*con.close();*/
                    }

                }
            %>
        </form>
        <p>&nbsp;</p>
    </body>
</html>