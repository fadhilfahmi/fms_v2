<%-- 
    Document   : cf_broker_view
    Created on : Apr 26, 2016, 12:48:31 PM
    Author     : user
--%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.BrokerDAO"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Broker"%>
<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
Broker view = (Broker) BrokerDAO.getInfo(log,request.getParameter("id"));
%>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
           
            
              $( ".printbutton" ).click(function() {
                  //var a = $("form").serialize();
                  var a = $("#saveform :input[value!='']").serialize(); 
                  //alert($('input[name="ad_amount"]').val());
                  checkField();
                  var b = checkField();
                  if(b==false){
                    $.ajax({
                        data: a,
                        type: 'POST',
                        url: "DataController?do=add&formlevel=master",
                        success: function (result) {
                          $('#maincontainer').empty().html(result).hide().fadeIn(300);
                        }
                    });
                     
                  }    
                  
               
                    return false;
              });
              
         });
        </script>
            <div id ="maincontainer">
                <table id="table_2" cellspacing="0">
                    
               <tr>
                   <td width="30%" class="bd_bottom" align="left" valign="top">Code</td>
                   <td width="70%" class="bd_bottom" align="left"><%= view.getCode() %></td>
               </tr>
              
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Code</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getCode()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Company Name</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getCompanyname()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Company Register No.</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getCoregister()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Bumiputra</td>
                   <td class="bd_bottom" align="left">
                       <%= view.getBumiputra()%>
                   </td>
               </tr>
               
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">HQ Account Code</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getHqactcode()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">HQ Account Description</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getHqactdesc()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Account</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getCoa()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Account Description</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getCoadescp()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Person</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getPerson()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Title</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getTitle()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Position</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getPosition()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Handphone No</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getHp()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Address</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getAddress()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">City</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getCity()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">State</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getState()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Postcode</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getPostcode()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Country</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getCountry()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Phone No.</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getPhone()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Fax</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getFax()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Email</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getEmail()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">URL Address</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getUrl()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Bank</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getBank()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Bank Account No.</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getBankaccount()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Payment</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getPayment()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Remark</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getRemarks()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Bank Description</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getBankdesc()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Active Status</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getActive()%>
                   </td>
               </tr>
               
          </table>
            </div>
