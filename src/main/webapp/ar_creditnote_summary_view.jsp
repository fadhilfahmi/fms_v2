<%-- 
    Document   : ar_creditnote_summary_view
    Created on : Sep 8, 2017, 10:13:12 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.financial.gl.PostDAO"%>
<%@page import="com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArCreditNoteItem"%>
<%@page import="com.lcsb.fms.dao.financial.ar.ArCreditNoteDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArCreditNote"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
//PaymentVoucherItem vi = (PaymentVoucherItem) VendorPaymentDAO.getPYVitem(request.getParameter("refer"));
    ArCreditNote v = (ArCreditNote) ArCreditNoteDAO.getMain(log, request.getParameter("refer"));
    Module mod = (Module) ArCreditNoteDAO.getModule();

    String status = ArCreditNoteDAO.getStatus(log, v.getNoteno());

    String topath = request.getParameter("topath");
    String moduleid = mod.getModuleID();
    String token = request.getParameter("status");

    if (topath == null) {
        topath = "viewlist";
    } else {
        //moduleid = request.getParameter("frommodule");
    }

    String classButtonColor = "";
    String classButtonDisable = "";

    if (status.equals("Approved") && token.equals("Checked")) {
        classButtonColor = "btn-warning";
        classButtonDisable = "disabled";
    } else if (status.equals("Checked") && token.equals("Prepared")) {
        classButtonColor = "btn-warning";
        classButtonDisable = "disabled";
    } else if (token.equals("Checked")) {
        classButtonColor = "btn-success";
        classButtonDisable = "";
    } else if (token.equals("Prepared")) {
        classButtonColor = "btn-success";
        classButtonDisable = "";
    }
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        window.scrollTo(0, 0);

        callPnotify('<%= request.getParameter("status")%>', '<%= request.getParameter("text")%>');


        $('#print').click(function (e) {
            printElement($("#viewjv").html());
        });

        $("#addcredit").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addcredit&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $(".additem").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=additem&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".adddebit").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=adddebit&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });



    });
</script>
<table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
    <tr>
        <td valign="middle" align="left">
            <div class="btn-group" role="group" aria-label="...">
                <button id="backtox" class="btn btn-default btn-xs" title="<%= mod.getModuleID()%>"  name="<%= topath%>" type="<%= token%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                <button id="<%= v.getNoteno()%>" class="btn btn-default btn-xs viewprinted" title="<%= mod.getModuleID()%>"  name="<%= topath%>" type="<%= token%>"><i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;&nbsp;View Printed</button>
                <%
                    if (token.equals("Prepared")) {
                %>
                <button id="<%= v.getNoteno()%>" class="btn <%= classButtonColor%> btn-xs checknow <%= classButtonDisable%>" title="<%= mod.getModuleID()%>"  name="<%= topath%>" type="<%= token%>"><i class="fa fa-check-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;Check</button>
                <%
                } else if (token.equals("Checked")) {
                %>
                <button id="<%= v.getNoteno()%>" class="btn <%= classButtonColor%> btn-xs approvenow <%= classButtonDisable%>" title="<%= mod.getModuleID()%>"  name="<%= topath%>" type="<%= token%>"><i class="fa fa-check-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;Approve</button>
                <%
                    }
                %>
            </div>

        </td>
    </tr>
</table>
<br>
<table class="table table-bordered table-striped  table-hover">
    <thead>
        <tr>
            <th colspan="4">Voucher Information
                <!--<small><span class="label label-primary">Preparing</span></small>-->
                
            </th>
        </tr>
    </thead>
    <tbody class="activerowm tdrow">
        <tr>
            <th width="25%">Status</th>
            <td width="25%"><%= GeneralTerm.getStatusLabel(ArCreditNoteDAO.getStatus(log, v.getNoteno()))%></td>
            <th width="25%">Buyer</th>
            <td width="25%"><%= v.getBuyercode()%> - <%= v.getBuyername()%></td>
        </tr>
        <tr>
            <th>Period</th>
            <td><%= v.getYear()%>, <%= v.getPeriod()%></td>
            <th>Bank</th>
            <td></td>
        </tr>
        <tr>
            <th>Note No</th>
            <td><%= v.getNoteno()%></td>
            <th>Amount</th>
            <td>RM<%= GeneralTerm.currencyFormat(v.getTotal())%></td>
        </tr>
        <tr>
            <th>Note Date</th>
            <td><%= v.getNotedate()%></td>
            <th>Remark</th>
            <td><%= v.getRemark()%></td>
        </tr>
        <tr class="togglerow" style="display: none">
            <th>Paid Address</th>
            <td></td>
            <th>Paid Postcode</th>
            <td></td>
        </tr>
    </tbody>
</table>
<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th colspan="5">Detail Information<!--<small>asdada</small>-->
                <span class="pull-right">
                    <%if (!status.equals("Approved")) {%>
                    <button  class="btn btn-default btn-xs additem" title="<%= mod.getModuleID()%>" id="<%= v.getNoteno()%>" name="addrefer"><i class="fa fa-plus-circle" aria-hidden="true"></i> Detail</button>
                    <%}%>
                </span>
            </th>
        </tr>
        <tr>
            <th>Refer. No</th>
            <th>Account</th>
            <th>Remark</th>
            <th>Amount</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <%List<ArCreditNoteItem> listRefer = (List<ArCreditNoteItem>) ArCreditNoteDAO.getAllItem(log, v.getNoteno());
                    if (listRefer.isEmpty()) {%>
        <tr>
            <td colspan="5">
                <span class="font-small-red">No data available.</span>
            </td>
        </tr>
        <%}
                        for (ArCreditNoteItem c : listRefer) {%>
        <tr class="activerowx" id="<%= c.getRefer()%>">
            <td class="tdrow">&nbsp;<%= c.getRefer()%></td>
            <td class="tdrow">&nbsp;<%= c.getCoacode()%> - <%= c.getCoaname()%></td>
            <td class="tdrow">&nbsp;<%= c.getRemarks()%></td>
            <td class="tdrow" align="right">&nbsp;<%= GeneralTerm.currencyFormat(c.getAmount())%></td>
            <td class="tdrow" align="right">&nbsp;
                <%if (!status.equals("Approved")) {%>
                <div class="btn-group btn-group-xs" role="group" aria-label="...">
                    <button id="<%= c.getRefer()%>" class="btn btn-default btn-xs edititem" title="<%= mod.getModuleID()%>" name="edititem"><i class="fa fa-pencil"></i></button>
                    <button id="<%= c.getRefer()%>"  class="btn btn-default btn-xs deleteitem" title="<%= mod.getModuleID()%>" name="deleteitem"><i class="fa fa-trash-o"></i></button>
                </div>
                <%}%>
            </td>
        </tr>
        <tr id="togglerow_nd<%= c.getRefer()%>" style="display: none">
            <td colspan="5">
                <table class="table table-bordered">
                    <thead></thead>
                    <tbody>
                        <tr>
                            <th>To Date Paid</th>
                            <td><%//= c.getPaid() %></td>
                            <th>B/F Amount</th>
                            <td><%//= c.getBf() %></td>
                        </tr>
                        <tr>
                            <th>To Paid</th>
                            <td><%//= c.getPayment() %></td>
                            <th>C/F Amount</th>
                            <td><%//= c.getCf() %></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <%}%>
    </tbody>
</table>







