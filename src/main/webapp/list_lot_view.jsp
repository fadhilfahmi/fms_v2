<%-- 
    Document   : list_bank_view
    Created on : Mar 4, 2016, 4:06:59 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.model.setup.configuration.LotInformation"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.LotInformationDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<div class="list-group"><%
            
            String keyword = request.getParameter("keyword");
            List<LotInformation> slist = (List<LotInformation>) LotInformationDAO.getAllLot(log,keyword);
            for (LotInformation c : slist) { 
               %>
       
            <a href="<%= c.getLotDescp()%>" id="<%= c.getLotCode()%>" title="" class="list-group-item thisresult">
                <div id="left_div"><%= c.getLotCode()%></div>
                <div id="right_div"><%= c.getLotDescp()%></div>
            </a>

               <%    
            } 
        %>
</div>