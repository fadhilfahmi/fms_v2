<%--
    Document   : cp_department_view
    Created on : Apr 18, 2016, 4:53:29 PM
    Author     : HP
--%>


<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.setup.company.DepartmentDAO"%>
<%@page import="com.lcsb.fms.model.setup.company.Department"%>
<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
Department view = (Department) DepartmentDAO.getInfo(log,request.getParameter("id"));
%>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {

              $( ".printbutton" ).click(function() {
                  //var a = $("form").serialize();
                  var a = $("#saveform :input[value!='']").serialize();
                  //alert($('input[name="ad_amount"]').val());
                  checkField();
                  var b = checkField();
                  if(b==false){
                    $.ajax({
                        data: a,
                        type: 'POST',
                        url: "DataController?do=add&formlevel=master",
                        success: function (result) {
                          $('#maincontainer').empty().html(result).hide().fadeIn(300);
                        }
                    });

                  }

                  return false;
              });

         });
        </script>
            <div id ="maincontainer">
                <table id="table_2" cellspacing="0">

               <tr>
                   <td width="30%" class="bd_bottom" align="left" valign="top">Code</td>
                   <td width="70%" class="bd_bottom" align="left"><%= view.getCode() %></td>
               </tr>
               <tr>
                   <td class="bd_bottom" align="left" valign="top">Active</td>
                   <td class="bd_bottom" align="left">
                       <%= view.getActive()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Type</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getType()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Name</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getName()%>
                   </td>
               </tr>
          </table>
				</div>
