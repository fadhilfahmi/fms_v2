<%-- 
    Document   : ap_pv_add_item
    Created on : May 10, 2016, 3:53:33 PM
    Author     : user
--%>
<%@page import="com.lcsb.fms.model.management.contract.Agreement"%>
<%@page import="com.lcsb.fms.model.management.contract.AgreementJob"%>
<%@page import="com.lcsb.fms.dao.management.contract.AgreementDAO"%>
<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
AgreementJob v = (AgreementJob) AgreementDAO.getJob(request.getParameter("refer"));
Agreement vm = (Agreement) AgreementDAO.getCAG(log,request.getParameter("refer"));
Module mod = (Module) AgreementDAO.getModule();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="jsfunction/jquery-1.12.0.min.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
        <link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
           //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });
           
           $('.getjob').click(function(e){
              var a = $(this).attr('id');
              var b = $(this).attr('id1');
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Chart of Job',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_job.jsp?code='+a+'&descp='+b);
                        $('body').on('click', '.thisresult', function(event){
                            dialog.close();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
        
           
           $('#getlocation').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Location',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_location.jsp');
                        $('body').on('click', '.thisresult_nd', function(event){
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
           $('#getoutput').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Output',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_output.jsp');
                        $('body').on('click', '.thisresult_nd', function(event){
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
           $('#getsub').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Sub Account Detail',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_sub.jsp');
                        $('body').on('click', '.thisresult_nd', function(event){
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
           $('#gettax').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Taxation',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_tax.jsp');
                        $('body').on('click', '.thisresult', function(event){
                            dialog.close();
                            var a = $('#pricework').val();
                            a = a.replace(/\,/g,'');
                            var b = $('#taxrate').val();
                            var c = parseFloat(b) * parseFloat(a) / 100;
                            //$('#taxamt').val(parseFloat(c).toFixed(2));
                            $('#taxamt').val(c.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
                            
                            
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
                
                
               
               return false;
           });
           
           $('.calculate').keyup(function(e){
               var todeduct = $(this).val();
               var theval = $(this).attr('id');
               
               
               if(todeduct!=''){
                   var tot = 0;
                   if(theval=='debit'){
                       $('#credit').val(0.0);
                   }
                   if(theval=='credit'){
                       $('#debit').val(0.0);
                   }
                   
               }
               
           });
           
           $('.totalup').keyup(function(e){
               
               var qty = $('#qty').val();
               var unitp = $('#unitp').val();
               var taxrate = $('#taxrate').val();
               
               var total = qty * unitp;
               var taxamt = taxrate * total / 100;
               $('#taxamt').val(taxamt);
               $('#amount').val(total);
               
           });
           
           $('.calculate_gst').change(function(e){
               alert(9);
               var amt = $('#amount').val();
               var taxrate = $('#taxrate').val();
               
               var total = taxrate * 100 / amt;
               
               $('#taxamt').val(total);
               
           });
           
           $('.form-control').focusout(function(e){
               if(($('#debit').val()==0.00) || ($('#loccode').val()=='') || ($('#actdesc').val()=='') || ($('#remarks').val()=='')){
                   $('#savebutton').prop('disabled', true);
               }else{
                   $('#savebutton').prop('disabled', false);
               }
           });
           
            $('#getproduct').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Product Info',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_product.jsp?name=prodname&code=prodcode');
                        $('body').on('click', '.thisresult', function(event){
                            console.log('tutup');
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
           $('.calc_amount').keyup(function(e){
               var qty = $('#unit').val();
               var unitp = $('#priceunit').val();
               
               var total = qty * unitp;
               
                $('#pricework').val(total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
                  
               
           });
           
           
           
           
        });
        
       </script>
    </head>
    <body>
        <div id ="maincontainer">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
              <td width="74%" class="borderbot" align="left"><span class="bigfonttitle"><%= mod.getModuleDesc() %></span>&nbsp;&nbsp;<span class="midfonttitle">Add New <%= mod.getModuleDesc() %></span></td>
		<td width="26%" class="borderbot" align="right">&nbsp;</td>
	  </tr>
  </table>
	<br>
   <div class="partition"> 
       <div class="headofpartition"></div>
       <div class="bodyofpartition">
  <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
			<td valign="middle" align="left">
				<button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                                <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="addjob" disabled><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
			</td>
		</tr>
  </table>
  <br>
  

  
<form data-toggle="validator" role="form" id="saveform">
    <input type="hidden" name="prepareid" id="prepareid" value="<%= log.getUserID()%>">
    <input type="hidden" name="preparename" id="preparename" value="<%= log.getFullname()%>">
    <input type="hidden" name="preparedate" id="preparedate" value="<%= AccountingPeriod.getCurrentTimeStamp() %>">
    <div class="well">
     <div class="row">
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">No &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="refer" name="refer" placeholder="Auto Generated" autocomplete="off" required readonly>   
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Reference No &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="no" name="no" placeholder="Auto Generated" autocomplete="off" value="<%= v.getNo()%>" required readonly>   
             </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">Job Code&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span id="res_digit" class="res_style"></span></label>
                <div class="form-group-sm input-group">
                    <input type="text" class="form-control input-sm" id="jobcode" name="jobcode" readonly>
                    <span class="input-group-btn">
                    <button class="btn btn-default btn-sm getjob" type="button" id="jobcode" id1="jobdescp"><i class="fa fa-cog"></i> Job</button>
                    </span>
                </div>  
             </div>
        </div>
        <div class="col-sm-8">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Job Description&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="jobdescp" name="jobdescp" readonly>
             </div>
        </div>
        
    </div> 
    
   
        
    </div>
    
 <div class="well">
     <div class="row">
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Location Level&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="loclevel" name="loclevel" readonly>
                                        <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="getlocation"><i class="fa fa-cog"></i> Location</button>
                                        </span>
                                    </div>    
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Location Code&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="loccode" name="loccode"  autocomplete="off" readonly>   
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Location Name&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="locdesc" name="locname" placeholder="" autocomplete="off" readonly >   
             </div>
        </div>
    </div>    
        <div class="row">
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Output Type&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="type" name="type" readonly>
                                        <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="getoutput"><i class="fa fa-cog"></i> Output</button>
                                        </span>
                                    </div>    
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Output Code&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="outputcode" name="outputcode"  autocomplete="off" readonly>   
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Output Description&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="outputdesc" name="outputdesc" placeholder="" autocomplete="off" readonly >   
             </div>
        </div>
    </div>      
         <div class="row">
        
        <div class="col-sm-12">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Remarks&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                <textarea class="form-control" rows="3"  id="remark" name="remark"></textarea>
             </div>
        </div>
    </div>
        <div class="row">
        <div class="col-sm-3">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Unit Measure &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="unitmeasure" name="unitmeasure" placeholder="From Output" autocomplete="off" value="" readonly> 
             </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Unit Of Job &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm calc_amount" id="unit" name="unit" placeholder="0.00" autocomplete="off" value=""> 
             </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Unit Price Of Job &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm calc_amount" id="priceunit" name="priceunit" placeholder="0.00" autocomplete="off" value=""> 
             </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Total Price of Job &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="pricework" name="pricework" placeholder="0.00" autocomplete="off" value=""> 
             </div>
        </div>
       
        </div></div>
    <div class="well"> 
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Tax Code &nbsp&nbsp<span class="res_code"></span></label>
                <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm calculate_gst" id="taxcode" name="taxcode" readonly>
                                        <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="gettax"><i class="fa fa-cog"></i> Tax Type</button>
                                        </span>
                                    </div>  
             </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Tax Description &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="taxdescp" name="taxdescp" autocomplete="off" readonly> 
             </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Tax Rate &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="taxrate" name="taxrate" value="0.00" autocomplete="off" readonly> 
             </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Tax Amount &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="taxamt" name="taxamt" placeholder="0.00" autocomplete="off" value="0.00" readonly > 
             </div>
        </div>
    </div>  </div>     
   
    
    
    
    <!--<div class="form-group-sm input-group">
    <input type="text" class="form-control input-sm">
    <span class="input-group-btn">
    <button class="btn btn-default btn-sm" type="button"><i class="fa fa-cog"></i> Get Account Code</button>
    </span>
    </div>-->
    
</form>
</div>
  
 
     </div>
     </div>
      </div>
      

    </body>
</html>