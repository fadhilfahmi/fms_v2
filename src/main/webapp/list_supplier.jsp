<%-- 
    Document   : list_supplier
    Created on : May 13, 2016, 12:59:06 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
            
             $.ajax({
                        url: "list_supplier_view.jsp",
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
            }});
            
           $('#keyword').keyup(function(){
                //var l = $('input[name=searchby]:checked').val();
                var keyword = $(this).val();
                
                 $.ajax({
                        url: "list_supplier_view.jsp?keyword="+keyword,
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
                }});
            });
            
            
            
            $('#result').on('click', '.thisresult', function(e) { 
                var a = $(this).attr('href');
                var b = $(this).attr('id');
                var c = $(this).attr('title');
                var d = $(this).attr('id1');
                var e = $(this).attr('id2');
                var f = $(this).attr('id3');
                var g = $(this).attr('id4');
                var h = $(this).attr('id5');
                var i = $(this).attr('id6');
                
                var x = '<%= request.getParameter("city") %>';
                var address = '';
                if(x=='null'){
                    address = d + ', ' + f + ', ' + e + ', ' + g;
                }else{
                    address = d;
                }
                
                $('#<%= request.getParameter("name") %>').val(a);
                $('#<%= request.getParameter("code") %>').val(b);
                $('#<%= request.getParameter("address") %>').val(address);
                $('#city').val(e);
                $('#postcode').val(f);
                $('#state').val(g);
                $('#gstid').val(i);
                
                e.preventDefault();
            });
            
           
            
        });
        </script>
    </head>
    <body>
         
        
        <div class="form-group">
            <div class="form-group input-group">
                <input type="text" class="form-control" id="keyword">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
        
        <div id="result">
       
        </div>
        
            
    </body>
</html>
