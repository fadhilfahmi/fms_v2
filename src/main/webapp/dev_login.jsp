<%-- 
    Document   : dev_login
    Created on : Apr 6, 2017, 11:46:32 AM
    Author     : fadhilfahmi
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        var track_page = 1; //track user scroll as page number, right now page number is 1
        var loading = false; //prevents multiple loads

        load_contents(track_page); //initial content load

        $(window).scroll(function () { //detect page scroll
            if ($(window).scrollTop() + $(window).height() >= $(document).height()) { //if user scrolled to bottom of the page
                track_page++; //page number increment
                console.log('----' + track_page);
                load_contents(track_page); //load content   
            }
        });

//Ajax load function
        function load_contents(track_page) {
            if (loading == false) {
                loading = true;  //set loading flag on
                $('.loading-info').show(); //show loading animation 
                /*$.post('fetch_pages.jsp', {'page': track_page}, function (data) {
                 loading = false; //set loading flag off once the content is loaded
                 if (data.trim().length == 0) {
                 //notify user if nothing to load
                 $('.loading-info').html("No more records!");
                 return;
                 }
                 
                 setTimeout(function () {
                 $('.loading-info').hide(); //hide loading animation once data is received
                 $("#results").append(data); //append data into #results element
                 }, 1000);
                 
                 
                 }).fail(function (xhr, ajaxOptions, thrownError) { //any errors?
                 alert(thrownError); //alert with HTTP error
                 })*/

                $.ajax({
                    async: false,
                    url: "ProcessController?moduleid=080302&process=loadloginlog&page=" + track_page,
                    success: function (data) {

                        loading = false; //set loading flag off once the content is loaded
                        if (data.trim().length == 0) {
                            //notify user if nothing to load
                            $('.loading-info').html("No more records!");
                            return;
                        }

                        setTimeout(function () {
                        $('.loading-info').hide(); //hide loading animation once data is received
                        $("#results").append(data); //append data into #results element
                        }, 300);

                    },
                    error: function (xhr, ajaxOptions, thrownError) { //any errors?
                        alert(thrownError); //alert with HTTP error
                    }
                });
            }
        }

    });
</script>
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-th-list" aria-hidden="true"></i> Login Log
    </div>
    <!-- /.panel-heading -->
    <div class="row">
        <div class="col-sm-6"><br>&nbsp;&nbsp;&nbsp;&nbsp;Legend : <span class="label label-primary" style="background-color:#4cad73">Today's Login</span></div></div>
    <div class="panel-body">
        
        <div class="row">
        <div class="col-sm-6">
        <div class="list-group" id="results">
            
            
        </div>
        </div>
        </div>
        <!-- /.list-group -->
        <div class="row">
            <div class="col-sm-6">
        <a href="#" class="btn btn-default btn-block loading-info"><i class="fa fa-spinner fa-spin fa-2x fa-fw"></i></a>
            </div>
        </div>
    </div>
    <!-- /.panel-body -->
</div>
<!--<div class="wrapper">
    <ul id="results"></ul>
    <div class="loading-info"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
</div>-->

