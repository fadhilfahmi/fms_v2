<%-- 
    Document   : cb_bankreport
    Created on : Mar 1, 2017, 4:05:54 PM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.fms.dao.financial.cashbook.BankReportDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.dao.financial.gl.FinancialReportDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) BankReportDAO.getModule();
%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $("#viewreport").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewreport&vby=" + $('#vby').val() + "&year=" + $('#year').val() + "&period=" + $('#period').val() + "&otype=" + $('#otype').val(),
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });

        

        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });


    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
                </tr>
            </table>
            <br>
            <form data-toggle="validator" role="form" id="saveform">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="well">

                            <div class="row self-row">
                                <label for="inputName" class="control-label">Bank Report Parameter</label>
                            </div>


                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group" id="div_digit">
                                        <label for="inputName" class="control-label">Bank Code &nbsp&nbsp<span class="res_code"></span></label>
                                        <div class="form-group-sm input-group">
                                            <input type="text" class="form-control input-sm" id="bankcode" name="bankcode">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-sm" type="button" id="getbank"><i class="fa fa-cog"></i> Bank</button>
                                            </span>
                                        </div>  
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group" id="div_digit">
                                        <label for="inputName" class="control-label">Bank Name &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                        <input class="form-control input-sm" style="font-size:12px" name="bankname" id="bankname" type="text" value="" size="50"  />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Year</label>
                                        <select class="form-control input-sm" id="year" name="year">
                                            <%= ParameterDAO.parameterList(log,"Year", AccountingPeriod.getCurYear(log))%>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Period</label>
                                        <select class="form-control input-sm" id="period" name="period">
                                            <%= ParameterDAO.parameterList(log,"Period", AccountingPeriod.getCurPeriod(log))%>
                                        </select> 
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Month</label>
                                        <select class="form-control input-sm" id="period" name="period">
                                            <%= ParameterDAO.parameterList(log,"Month", "")%>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                            <label>Carry Forward</label>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="" name="carry">All non-clear status from this period to next period.
                                                </label>
                                            </div>
                                        </div>
                                </div>



                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <button id="savebutton"  class="btn btn-default btn-sm pull-right" title="<%= mod.getModuleID()%>" name="generateBankReport">Generate Report</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                </div>
            </form>                               

        </div>
    </div>
</div>

