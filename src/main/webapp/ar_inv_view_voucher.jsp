<%-- 
    Document   : cb_cv_view_voucher
    Created on : Mar 21, 2016, 11:22:17 AM
    Author     : Dell
--%>


<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.ext.Restriction"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoice"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoiceItem"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.CashVoucher"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.CashVoucherDAO"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.CashVoucherAccount"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.dao.LocationDAO"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucher"%>
<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucherItem"%>
<%@page import="com.lcsb.fms.dao.financial.gl.JournalVoucherDAO"%>
<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    SalesInvoiceItem vi = (SalesInvoiceItem) SalesInvoiceDAO.getINVitem(log,request.getParameter("referno"));
    SalesInvoice v = (SalesInvoice) SalesInvoiceDAO.getINV(log,request.getParameter("referno"));
    Module mod = (Module) SalesInvoiceDAO.getModule();
    
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/pvoucher.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
        <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            
            
            
            $(".viewx").click(function () {
                    var a = $(this).attr('id');
                    if(a=='summary'){
                        $('.rowdetailed').hide();
                        $('.rowsummary').show();
                    }
                    
                    if(a=='detail'){
                        $('.rowdetailed').show();
                        $('.rowsummary').hide();
                        
                    }
            });
            
            $('.action').click(function(e){
                var action = $(this).attr('id');
                
                $.ajax({
                    url: "PathController?moduleid=<%= mod.getModuleID() %>&process="+action+"&referno=<%= v.getInvref()%>",
                    success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            
                return false;
				 
            });
            
            $('.action_2').click(function(e){
                var action = $(this).attr('id');
                
                BootstrapDialog.confirm({
                    title: 'Confirmation',
                    message: 'Are you sure to '+action+' this?',
                    type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                    btnOKLabel: 'Delete', // <-- Default value is 'OK',
                    btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                    callback: function(result) {
                                                // result will be true if button was click, while it will be false if users close the dialog directly.
                        if(result) {
                            $.ajax({
                                url: "PathController?moduleid=<%= mod.getModuleID() %>&process="+action+"&referno=<%= v.getInvref() %>",
                                    success: function (result) {
                                        $('#herey').empty().html(result).hide().fadeIn(300);
                                    }
                            });
                        }
                    }
                });
				 
            });
        });
</script>
    </head>
    <body>
	
        
        
            <div id ="maincontainer">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
              <td width="74%" class="borderbot" align="left"><span class="bigfonttitle">View <%= mod.getModuleDesc()%></span>&nbsp;&nbsp;<span class="midfonttitle"></span></td>
		<td width="26%" class="borderbot" align="right">&nbsp;</td>
	  </tr>
  </table>
	<br>
   <div class="partition"> 
       <div class="headofpartition"></div>
       <div class="bodyofpartition">
  <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
			<td valign="middle" align="left">
                                
                             
			</td>
		</tr>
  </table>

  <table id="table_left" width="100%" cellspacing="0">
                        <tr>
                            <td valign="middle" width="33%" align="left">
                                <div class="btn-group btn-group-sm" role="group" aria-label="...">
                                
                                
                                <div class="btn-group" role="group" aria-label="...">
                                    <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="editlist" type="<%= v.getInvref() %>"><i class="fa fa-arrow-circle-o-left"></i></button>
                                    <button id="print" class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-print" aria-hidden="true"></i></button>
                                    <button id="edit" class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="viewlist" <%= Restriction.getEdit(v.getCid(), v.getAid()) %>><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                    
                                    <%
                                    if(log.getListUserAccess().get(1).getAccess()){
                                    %>
                                    <button id="check" class="btn btn-default btn-sm action_2" title="<%= mod.getModuleID() %>" name="viewlist" <%= Restriction.getCheck(v.getCid()) %>><i class="fa fa-check-square-o" aria-hidden="true"></i> Check</button>
                                    <%
                                    }
                                    if(log.getListUserAccess().get(2).getAccess()){
                                    %>
                                    <button id="approve" class="btn btn-default btn-sm action_2" title="<%= mod.getModuleID() %>" name="viewlist" <%= Restriction.getApprove(v.getCid(), v.getAid()) %>><i class="fa fa-check-square-o" aria-hidden="true"></i> Approve</button>
                                    <%
                                    }
                                    %>

                          
                                    
                                  </div>
                              </div>
                                    
                              <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default btn-sm active viewx" id="detail">
                                  <input type="radio" class="viewt" name="viewtype" id="detail" value="detail" autocomplete="off" checked> Detailed
                                </label>
                                <label class="btn btn-default btn-sm viewx" id="summary">
                                  <input type="radio" class="viewt"  name="viewtype" id="summary" value="summary" autocomplete="off"> Summary
                                </label>
                              </div>
                                
                            </td>
                            <td width="33%" align="right">
                                <div id="totalpage">
                                    <select id="page">
                                        <option value="'+i+'"></option>
                                    </select> 
                                </div>
                                </td>
                        </tr>
                    </table>
    <table id="maintbl" width="100%" cellspacing="0">
            <tr>
                <td valign="top">
                    
                </td>
            </tr>
            <tr>
                <td>
                    <table id="table_left" width="100%" cellspacing="0"><tr><td width="100%">
                        <div id="viewjv">
                            
                            <table width="100%" border="0" cellspacing="0" cellpadding="10"><tr><td><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="50%"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="4%"><img src="image/logo_lkpp.jpg" width="25" height="25"></td><td width="96%">&nbsp;<span class="comnamevc"><strong>LKPP CORPORATION SDN BHD</strong></span></td></tr></table></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                            <td align="right"><strong>TAX INVOICE</strong></td></tr><tr><td><p class="medfont2">&nbsp;</p><p class="medfont3">No 45/4, Jalan Teluk Sisek</p><p class="medfont3">Peti surat 144</p><p class="medfont3">25710 Kuantan</p><p class="medfont3">Pahang Darul Makmur</p><p class="medfont3">&nbsp;</p><p class="medfont3">Tel : 09-5165180</p></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr>
                        <td valign="top">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td rowspan="5" align="left" class="bordertopgrey" width="40%">&nbsp;<span class="boldtitle">To :</span></td>
                                    <td class="bordertoprightgrey" width="60%">&nbsp;<span class="normtitle">1</span> 
                                    
                                    </td>
                                </tr>
                                <tr>
                                    
                                    <td class="borderleftbottomrightgrey">&nbsp;<span class="normtitle"><%= v.getBname()%></span></td>
                                </tr>
                                <tr>
                                    
                                    <td class="borderleftbottomrightgrey">&nbsp;<span class="normtitle"><%= v.getBaddress()%></span></td>
                                </tr>
                                <tr>
                                    
                                    <td class="borderleftbottomrightgrey">&nbsp;<span class="normtitle"><%= v.getBpostcode()%>, <%= v.getBstate()%></span></td>
                                </tr>
                                <tr>
                                    
                                    <td class="borderleftbottomrightgrey">&nbsp;<span class="normtitle"><%= v.getGstid()%></span></td>
                                </tr>
                                
                               
                            </table>
			</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left" class="bordertopgrey" width="40%">&nbsp;<span class="boldtitle">PAGE</span></td>
                                    <td class="bordertoprightgrey" width="60%">&nbsp;<span class="normtitle">1</span> 
                                    
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="borderleftbottomgrey">&nbsp;<span class="boldtitle">REFERENCE NO</span></td>
                                    <td class="borderleftbottomrightgrey">&nbsp;<span class="normtitle"><%= v.getInvref()%></span></td>
                                </tr>
                                <tr>
                                    <td align="left" class="borderleftbottomgrey">&nbsp;<span class="boldtitle">DATE</span></td>
                                    <td class="borderleftbottomrightgrey">&nbsp;<span class="normtitle"><%= v.getInvdate()%></span></td>
                                </tr>
                                <tr>
                                    <td align="left" class="borderleftbottomgrey">&nbsp;<span class="boldtitle">Contract No</span></td>
                                    <td class="borderleftbottomrightgrey">&nbsp;<span class="normtitle"><%= v.getContractno()%></span></td>
                                </tr>
                                
                            </table>
                        </td>
                     </tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td align="right">&nbsp;<span class="boldtitle" id="noprint"></span></td></tr></table></td></tr><tr><td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td colspan="9" class="forlisttitle"><span class="boldtitle">&nbsp;List of Account</span></td>
                                            </tr>
                                            <tr>
                                                <td width="2%" class="bordertop">&nbsp;<span class="intableheader_jv">No</span></td>
                                                <td width="37%" class="bordertop">&nbsp;<span class="intableheader_jv">Description</span></td>
                                                <td width="10%" class="bordertoprightgrey">&nbsp;<span class="intableheader_jv">Quantity</span></td>
                                                <td width="10%" class="bordertoprightgrey">&nbsp;<span class="intableheader_jv">Unit Measure</span></td>
                                                <td width="10%" class="bordertoprightgrey">&nbsp;<span class="intableheader_jv">Unit Price</span></td>
                                                <td width="10%" class="bordertoprightgrey">&nbsp;<span class="intableheader_jv">GST Amount</span></td>
                                                <td width="10%" class="bordertoprightgrey">&nbsp;<span class="intableheader_jv">Total</span></td></tr>

                        
			
			<!--<tr height="20px"><td class="borderleft">&nbsp;<span class="fontjv_item"></span></td>
			<td class="borderleft">&nbsp;<span class="fontjv_item"></span></td>
			<td class="borderrightend" align="right">&nbsp;<span class="fontjv_item"></span>&nbsp;</td>
			<td class="borderrightend" align="right">&nbsp;<span class="fontjv_item"></span>&nbsp;</td>
			<td class="borderrightend" align="right">&nbsp;<span class="fontjv_item">b/f</span>&nbsp;</td>
			<td class="borderrightend" align="right">&nbsp;<span class="fontjv_item">'+round_decimals(parseFloat(tot_debt),2)+'</span>&nbsp;</td> 
			<td class="borderrightend" align="right">&nbsp;<span class="fontjv_item">'+round_decimals(parseFloat(tot_cred),2)+'</span>&nbsp;</td></tr>-->
			
                        <%
                        int i = 0;
                        double sumQty = 0.0;
                        String unitPrice = "";
                        String unitM = "";
                        double taxamt = 0.0;
                        double totalAmt = 0.0;
                        
                        List<SalesInvoiceItem> slist = (List<SalesInvoiceItem>) SalesInvoiceDAO.getAllINVItem(log,request.getParameter("referno"));
                        for (SalesInvoiceItem c : slist) {
                            i++;
                            sumQty+=c.getQty();
                            unitPrice=c.getUnitp();
                            unitM=c.getUnitm();
                            taxamt+=c.getTaxamt();
                            totalAmt+=c.getAmount();
                            
                        %>
			<tr height="20px" class="rowdetailed"><td class="borderleft">&nbsp;<span class="fontjv_item"><%= i %></span></td>
                            <td class="borderleft">&nbsp;<span class="fontjv_item"><%= c.getRemarks()%></span></td>
                            <td class="borderrightend" align="right"><span class="fontjv_item"><%= c.getQty()%></span></td>
                            <td class="borderrightend" align="right"><span class="fontjv_item"><%= c.getUnitm()%></span></td>
                            <td class="borderrightend" align="left"><span class="fontjv_item"><%= c.getUnitp()%></span></td>
                            <td class="borderrightend" align="right"><span class="fontjv_item"><%= GeneralTerm.currencyFormat(c.getTaxamt()) %></span></td> 
                            <td class="borderrightend" align="right"><span class="fontjv_item"><%= GeneralTerm.currencyFormat(c.getAmount()) %></span></td>
                        </tr>
                        
                        
                        <%}%>
                        
                        <tr height="20px" class="rowsummary" style="display: none"><td class="borderleft">&nbsp;<span class="fontjv_item"><%= i %></span></td>
                            <td class="borderleft">&nbsp;<span class="fontjv_item"><%= v.getRemarks()%>asdas</span></td>
                            <td class="borderrightend" align="right"><span class="fontjv_item"><%= GeneralTerm.currencyFormat(sumQty)%></span></td>
                            <td class="borderrightend" align="right"><span class="fontjv_item"><%= unitM%></span></td>
                            <td class="borderrightend" align="left"><span class="fontjv_item"><%= GeneralTerm.currencyFormat(Double.parseDouble(unitPrice))%></span></td>
                            <td class="borderrightend" align="right"><span class="fontjv_item"><%= GeneralTerm.currencyFormat(taxamt) %></span></td> 
                            <td class="borderrightend" align="right"><span class="fontjv_item"><%= GeneralTerm.currencyFormat(totalAmt) %></span></td>
                        </tr>
                        
			<tr><td colspan="9"></td></tr>
			<tr><td colspan="9"></td></tr>
		
		<tr>
                    <td class="borderleft">&nbsp;</td>
                    <td class="borderleft"  valign="top">&nbsp;</td>
                    <td class="borderrightend" align="right">&nbsp;</td>
                    <td class="borderrightend" align="right">&nbsp;</td>
                    <td class="borderrightend" align="right">&nbsp;</td>
                    <td class="borderrightend" align="right">&nbsp;</td>
                    <td class="borderrightend" align="right">&nbsp;</td>
                </tr>     
	  

		<tr>
                    <td class="borderleft">&nbsp;</td>
                    <td class="borderleft">&nbsp;</td>
                    <td class="borderrightend">&nbsp;</td>
                    <td class="borderrightend">&nbsp;</td>
                    <td class="borderrightend">&nbsp;</td>
                    <td class="borderrightend">&nbsp;</td>
                    <td class="borderrightend">&nbsp;</td>
                </tr>
                <tr>
                    
                    <td colspan="6" align="right" class="borderbottomboldright" valign="top">&nbsp;
                    
                        <dl class="dl-horizontal pull-right">
                                <dt>TOTAL&nbsp;</dt>
                                <dt>GST 6%&nbsp;</dt>
                                <dt>ROUNDING ADJUSTMENT&nbsp;</dt>
                                <dt>TOTAL (Inclusive GST)&nbsp;</dt>
                            </dl>
                    </td>
                    
                    <td align="right" class="borderbottomboldright">
                        <dl class="dl-horizontal pull-right">
                            <dd><%= GeneralTerm.currencyFormat(SalesInvoiceDAO.getTotalExGST(log,v.getInvref())) %>&nbsp;</dd>
                            <dd><%= GeneralTerm.currencyFormat(SalesInvoiceDAO.getTotalGST(log,v.getInvref())) %>&nbsp;</dd>
                                <dd>0.00&nbsp;</dd>
                                <dd><%= GeneralTerm.currencyFormat(SalesInvoiceDAO.getTotalInGST(log,v.getInvref())) %>&nbsp;</dd>
                            </dl>
                    </td>
                </tr>
                
                                        </table></td></tr><tr><td></td></tr><tr>
                                    <td height="30px">&nbsp;<span class="boldtitle">Year</span> - <%= v.getYear()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="boldtitle">Period</span> - <%= v.getPeriod()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="boldtitle">No</span> - <%//= v.getVoucherid()%></td></tr><tr><td><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="25%" class="bordertopgreyauth"><p class="paytofonttop">&nbsp;&nbsp;</p><p class="paytofonttop">&nbsp;&nbsp;_________________________</p>
	     <p class="paytofontmiddle">&nbsp;&nbsp;<strong>RECEIVED BY</strong></p></td><td width="25%" class="bordertopgreyauthright"><p class="paytofonttop">&nbsp;&nbsp;</p><p class="paytofonttop">&nbsp;&nbsp;_________________________</p><p class="paytofontmiddle">&nbsp;&nbsp;<strong>APPROVED BY</strong></p></td></tr>

	  <tr><td class="borderonesidegreyauth" valign="top"><p class="paytofonttop">&nbsp;&nbsp;Name</p><p class="paytofontmiddle">&nbsp;&nbsp;<strong><%//= v.getReceivebyname()%> - <%//= v.getReceiveid() %></strong></p>
	    <p class="paytofonttop">&nbsp;&nbsp;Type</p><p class="paytofontmiddle">&nbsp;&nbsp;<strong><%//= v.getReceivetype()%></strong></p></td>
	    <td class="borderonesidegreyauthright" valign="top"><p class="paytofontmiddle">&nbsp;&nbsp;</p><p class="paytofonttop">&nbsp;&nbsp;Name</p><p class="paytofontmiddle">&nbsp;&nbsp;<strong><%//= v.getApprobyname()%> - <%//= v.getAppid() %></strong></p>
	      <p class="paytofonttop">&nbsp;&nbsp;Designation</p><p class="paytofontmiddle">&nbsp;&nbsp;<strong><%//= v.getAppdesign()%></strong></p>
	      <p class="paytofontmiddle">&nbsp;&nbsp;</p><p class="paytofonttop">&nbsp;&nbsp;Date</p><p class="paytofontmiddle">&nbsp;&nbsp;<strong><%//ß= v.getApprovedate()%></strong></p></td></tr>
	  <tr><td class="borderleftbottomgrey">&nbsp;</td><td class="borderleftbottomgreyright">&nbsp;</td></tr></table></td></tr></table>
                            
			</div>
                    </table>
                </td>
            </tr>
            <input type="hidden" id="checkbyid" value="">
            <input type="hidden" id="checkbyname" value="">
            <input type="hidden" id="checkbydesign" value="">
            <input type="hidden" id="checkdate" value="">
	</table>

   
        </div>
    </div>
</div>
    </body>
</html>

