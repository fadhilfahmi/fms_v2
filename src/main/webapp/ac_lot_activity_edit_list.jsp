<%-- 
    Document   : ac_lot_activity_edit_list
    Created on : Dec 6, 2016, 8:55:27 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.management.activity.LotActivity"%>
<%@page import="com.lcsb.fms.dao.management.activity.LotActivityDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
//PaymentVoucherItem vi = (PaymentVoucherItem) VendorPaymentDAO.getPYVitem(request.getParameter("refer"));
    LotActivity v = (LotActivity) LotActivityDAO.getInfo(request.getParameter("referno"));
    Module mod = (Module) LotActivityDAO.getModule();

//String status = PaymentVoucherDAO.getStatus(v.getRefer());
%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/tether.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>

<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        window.scrollTo(0, 0);

        callPnotify('<%= request.getParameter("status")%>', '<%= request.getParameter("text")%>');


        $('#print').click(function (e) {
            printElement($("#viewjv").html());
        });
        var oTable = $('#list-bank').DataTable({
            responsive: true,
            "aaSorting": [[0, "desc"]]
        });

        $('.viewvoucher').click(function (e) {
            var a = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewpv&refer=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });
        
        $('.printenvelop').click(function (e) {
            var a = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=printenvelop&refer=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });
        
        $('.printcheque').click(function (e) {
            var a = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=printcheque&refer=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".editmain").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editmain&referno=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".edititem").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('name');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + b + "&referno=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".deleteitem").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('name');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are you sure to delete?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=" + b + "&referno=" + a,
                            success: function (result) {
                                oTable.row('#' + a).remove().draw(false);
                            }
                        });
                    }
                }
            });

            return false;
        });

        $(".deletemain").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('href');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'All datas for this Official Receipt will be permanently deleted. Are you sure to proceed?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&referno=" + a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            return false;
        });

        $("#addcredit").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addcredit&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $(".add-st").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addbank&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".addbank").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=addbank&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".check").click(function (e) {
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=check&referno=" + id,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $('.activerowm').click(function (e) {
            e.preventDefault();
            $(".togglerow").toggle();
            //$(this).toggleClass('info');
            //$("#togglerow_nd"+b).toggleClass('warning');
        });

        $('.activerowx').click(function (e) {
            e.preventDefault();
            var b = $(this).attr('id');
            $("#togglerow_nd" + b).toggle();
            $(this).toggleClass('info');
            $("#togglerow_nd" + b).toggleClass('warning');
        });

        $('.activerowy').click(function (e) {
            e.preventDefault();
            var b = $(this).attr('id');
            $("#togglerow_st" + b).toggle();
            $(this).toggleClass('info');
            $("#togglerow_st" + b).toggleClass('warning');
        });



    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
                </tr>
            </table>
            <br>
            <table class="table table-bordered table-striped  table-hover">
                <thead>
                    <tr>
                        <th colspan="4">Lot Activity 
                            <!--<small><span class="label label-primary">Preparing</span></small>-->
                            <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
                                <button id="<%= v.getRefer()%>" class="btn btn-default btn-xs viewvoucher" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i> Generate PV</button>
                                <button id="<%= v.getRefer()%>" class="btn btn-default btn-xs printenvelop" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i> Print Envelope</button>
                                <button id="<%= v.getRefer()%>" class="btn btn-default btn-xs printcheque" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i> Print Cheque</button>
                                <button id="<%= v.getRefer()%>" class="btn btn-default btn-xs viewvoucher" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i></button>
                                <button id="<%= v.getRefer()%>" class="btn btn-default btn-xs editmain" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                <button id="<%= v.getRefer()%>"  class="btn btn-danger btn-xs deletemain" title="<%= mod.getModuleID()%>" id="<%//= pvi.getRefer() %>" name="delete_st"><i class="fa fa-trash-o" aria-hidden="true"></i></button>

                                <button id="<%= v.getRefer()%>" class="btn btn-default btn-xs viewvoucher" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i></button>

                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody class="activerowm tdrow">
                    <tr>
                        <th width="15%">Status</th>
                        <td width="35%"><%= v.getRefer()%></td>
                        <th width="15%">Reference No</th>
                        <td width="35%"><%= v.getRefer()%></td>
                    </tr>
                    <tr>
                        <th>Lot </th>
                        <td><%= v.getLotcode()%> - <%= v.getLotdesc() %></td>
                        <th>Quarter</th>
                        <td><%= v.getQuarter()%></td>
                    </tr>
                    <tr>
                        <th>Year</th>
                        <td><%= v.getYear()%></td>
                        <th>Max Float Amount</th>
                        <td><%= v.getPeriod()%></td>
                    </tr>
                    <tr>
                        <th>Remarks</th>
                        <td colspan="3"><%= v.getRemark()%></td>
                    </tr>

                </tbody>
            </table>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab">Bank</a></li>
                            <li><a href="#tab2default" data-toggle="tab">Petty Cash</a></li>
                            <!--<li class="dropdown">
                                <a href="#" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#tab4default" data-toggle="tab">Default 4</a></li>
                                    <li><a href="#tab5default" data-toggle="tab">Default 5</a></li>
                                </ul>
                            </li>-->
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab1default">


                                <!--begin tab1default - Bill of Quantities-->    


                                <table class="table table-bordered table-striped table-hover" id="list-bank">
                                    <thead>
                                        <tr>
                                            <th colspan="6"><!--<small>asdada</small>-->
                                                <span class="pull-right">
                                                    <button  class="btn btn-default btn-xs add-st" title="<%= mod.getModuleID()%>" id="<%= v.getRefer()%>" name="addbank"><i class="fa fa-plus-circle" aria-hidden="true"></i> Bank</button>
                                                </span>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th width="10%">Code</th>
                                            <th width="25%">Name</th>
                                            <th width="15%">Account Code</th>
                                            <th width="25%">Account Desc.</th>
                                            <th width="15%">Fund Type</th>
                                            <th width="10%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%//List<CeManageBank> listBank = (List<CeManageBank>) ManagementCpDAO.getAllBank(v.getCode());

                                            //for (CeManageBank c : listBank) {
                                            //    CeManageBankPK l = c.getCeManageBankPK();

                                        %>
                                        <tr class="activerowx" id="<%//= l.getCode()%>">
                                            <td class="tdrow">&nbsp;<%//= l.getCode()%></td>
                                            <td class="tdrow">&nbsp;<%//= c.getName()%></td>
                                            <td class="tdrow">&nbsp;<%//= c.getCoacode()%></td>
                                            <td class="tdrow">&nbsp;<%//= c.getCoadesc()%></td>
                                            <td class="tdrow">&nbsp;<%//= c.getFtype()%></td>
                                            <td class="tdrow" align="right">&nbsp;
                                                <div class="btn-group btn-group-xs" role="group" aria-label="...">
                                                    <button id="<%//= l.getCode()%>" class="btn btn-default btn-xs edititem" title="<%= mod.getModuleID()%>" name="edit_bank"><i class="fa fa-pencil"></i></button>
                                                    <button id="<%//= l.getCode()%>"  class="btn btn-default btn-xs deleteitem" title="<%= mod.getModuleID()%>" name="delete_bank"><i class="fa fa-trash-o"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        <%//}%>
                                    </tbody>
                                </table>
                                <!--end tab2default - Bill of Quantities--> 


                            </div>
                            <div class="tab-pane fade" id="tab2default">


                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="input-check-node" class="sr-only">Search Tree:</label>
                                            <input type="input" class="form-control input-sm" id="input-check-node" placeholder="Search Module...">
                                        </div>

                                    </div>

                                    <div class="col-sm-6 bottom">
                                        <button type="button" class="btn btn-default btn-sm" id="btn-check-all">Check All</button>
                                        <button type="button" class="btn btn-default btn-sm" id="btn-uncheck-all">Uncheck All</button>

                                    </div>


                                </div>

                                <div class="row">

                                    <div class="col-sm-9">
                                        <div id="treeview-checkable" class=""></div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div id="checkable-output">

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="tab-pane fade" id="tab4default">Default 4</div>
                            <div class="tab-pane fade" id="tab5default">Default 5</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>








