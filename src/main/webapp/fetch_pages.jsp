<%-- 
    Document   : fetch_pages
    Created on : Feb 22, 2017, 4:42:12 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    int page_number = Integer.parseInt(request.getParameter("page"));
    int item_per_page = 25;
    int position = ((page_number-1) * item_per_page);
    
    ResultSet rs = null;
    out.println("<strong>SELECT COACode, COADesc, Remark FROM gl_posting_distribute ORDER BY id DESC LIMIT "+position+", "+item_per_page+"</strong>");
    PreparedStatement stmt = log.getCon().prepareStatement("SELECT COACode, COADesc, Remark FROM gl_posting_distribute ORDER BY id DESC LIMIT ?, ?");
    stmt.setInt(1, position);
    stmt.setInt(2, item_per_page);
    rs = stmt.executeQuery();
    while (rs.next()) {
        
        out.println("<p>"+rs.getString(1) +"-"+rs.getString(2) +"-"+rs.getString(3) +"</p>");
    }
%>
