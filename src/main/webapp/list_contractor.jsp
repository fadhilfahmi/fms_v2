<%-- 
    Document   : list_supplier
    Created on : May 13, 2016, 12:59:06 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
            
             $.ajax({
                        url: "list_contractor_view.jsp",
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
            }});
            
           $('#keyword').keyup(function(){
                //var l = $('input[name=searchby]:checked').val();
                var keyword = $(this).val();
                
                 $.ajax({
                        url: "list_contractor_view.jsp?keyword="+keyword,
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
                }});
            });
           
            
            $('#result').on('click', '.thisresult', function(e) { 
                var a = $(this).attr('href');
                var b = $(this).attr('id');
                var address = $(this).attr('address');
                var city = $(this).attr('city');
                var postcode = $(this).attr('postcode');
                var state = $(this).attr('state');
                var gstid = $(this).attr('gstid');
                var ownername = $(this).attr('ownername');
                var owneric = $(this).attr('owneric');
                
                
                $('#<%= request.getParameter("name") %>').val(a);
                $('#<%= request.getParameter("code") %>').val(b);
                $('#<%= request.getParameter("address") %>').val(address);
                $('#<%= request.getParameter("city") %>').val(city);
                $('#<%= request.getParameter("postcode") %>').val(postcode);
                $('#<%= request.getParameter("state") %>').val(state);
                $('#<%= request.getParameter("gstid") %>').val(gstid);
                $('#<%= request.getParameter("ownername") %>').val(ownername);
                $('#<%= request.getParameter("owneric") %>').val(owneric);
                
                e.preventDefault();
            });
            
           
            
        });
        </script>
    </head>
    <body>
         
        
        <div class="form-group">
            <div class="form-group input-group">
                <input type="text" class="form-control" id="keyword">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
        
        <div id="result">
       
        </div>
        
            
    </body>
</html>
