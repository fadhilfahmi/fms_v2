<%-- 
    Document   : list_search
    Created on : Mar 1, 2016, 10:16:44 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.ServerList"%>
<%@page import="com.lcsb.fms.util.dao.EstateDAO"%>
<%@page import="com.lcsb.fms.util.model.Supplier"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.SupplierDAO"%>
<%@page import="com.lcsb.fms.util.dao.SearchDAO"%>
<%@page import="com.lcsb.fms.util.model.Search"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
 <%
     
     String table = request.getParameter("table");
     String column = request.getParameter("column");
     String keyword = request.getParameter("keyword");
     String method = request.getParameter("method");
     String code = request.getParameter("code");
     String descp = request.getParameter("descp");
     %><div class="list-group"><%
            List<ServerList> slist = (List<ServerList>) EstateDAO.getAllServer(log,keyword);
            if(slist.isEmpty()){
               %>
       
                <div class="alert alert-danger" role="alert">
                    No matching search criteria were found for <strong><%= keyword %></strong>! 
                    
                </div>
               <%   
            }
            for (ServerList c : slist) { 
               %>
       
            <a class="list-group-item thisresult">
                <div id="left_div"><%= c.getEstatecode()%></div>
                <div id="right_div"><%= c.getDescp()%><button id="<%= c.getEstatecode()%>" class="btn btn-default btn-xs pull-right getcode"><i class="fa fa-cloud-download" aria-hidden="true"></i></button></div>
            </a>

               <%    
            } 
        %>
</div>