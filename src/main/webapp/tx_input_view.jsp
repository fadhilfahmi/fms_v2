<%-- 
    Document   : tx_input_view
    Created on : Nov 21, 2016, 10:56:05 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.financial.tx.TxInput"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxInputDetail"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxVoucherItem"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxVoucher"%>
<%@page import="com.lcsb.fms.dao.financial.tx.TaxInputDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    TxInput v = (TxInput) TaxInputDAO.getInput(log, request.getParameter("refer"));
    Module mod = (Module) TaxInputDAO.getModule();

    String status = TaxInputDAO.getStatus(log, v.getRefno());

    String distribute = request.getParameter("distribute");
%>
<link href="css/table_style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        window.scrollTo(0, 0);

        callPnotify('<%= request.getParameter("status")%>', '<%= request.getParameter("text")%>');

        $('#print').click(function (e) {
            printElement($("#viewjv").html());
        });

        $('.viewvoucher').click(function (e) {
            var a = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewor&refer=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".editmain").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editmain&referno=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".edititem").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('name');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + b + "&referno=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".deleteitem").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('name');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are you sure to delete?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + b + "&referno=" + a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });

            return false;
        });

        $(".deletemain").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('href');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'All datas for this Official Receipt will be permanently deleted. Are you sure to proceed?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&referno=" + a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            return false;
        });

        $("#addcredit").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addcredit&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });


        $(".addcheque").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addcheque&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $(".adddebitnote").click(function (e) {
            //e.preventDefault();
            var referno = $(this).attr('id');
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Debit Note',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {


                    var $content = $('<body></body>').load('list_ardebitnote.jsp?referno=' + referno);
                    $('body').on('click', '.thisresult-debitnote', function (event) {
                        event.preventDefault();

                        dialog.close();

                    });

                    return $content;
                }
            });

            return false;
        });

        $(".addcredit").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addcredit&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $(".check").click(function (e) {
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=check&referno=" + id,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $('.activerowm').click(function (e) {
            e.preventDefault();
            $(".togglerow").toggle();
            //$(this).toggleClass('info');
            //$("#togglerow_nd"+b).toggleClass('warning');
        });

        $('.activerowx').click(function (e) {
            e.preventDefault();
            var b = $(this).attr('id');
            $("#togglerow_nd" + b).toggle();
            $(this).toggleClass('info');
            $("#togglerow_nd" + b).toggleClass('warning');
        });

        $('.activerowy').click(function (e) {
            e.preventDefault();
            var b = $(this).attr('id');
            $("#togglerow_st" + b).toggle();
            $(this).toggleClass('info');
            $("#togglerow_st" + b).toggleClass('warning');
        });

        $(".approve").click(function () {
            //var a = $("form").serialize();
            var a = $(this).attr('id');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are you sure to Approve this?',
                type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Approve', // <-- Default value is 'OK',
                btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=dist&referno=" + a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            return false;
        });

        $(".deleteall").click(function () {
            //var a = $("form").serialize();
            var a = $(this).attr('id');

            BootstrapDialog.confirm({
                title: '<span style="color:#d9534f;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp; <strong>Confirmation</strong></span>',
                message: 'Are you sure to delete?',
                type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=deletedetail&refer=" + a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            return false;
        });



        $('.td-click').click(function (e) {

            var b = $(this).attr('id');
            //$(this).toggleClass('info');
            // $('#chkbox'+b).prop('checked', true);
            var checkBox = $('#chkbox' + b);
            checkBox.prop("checked", !checkBox.prop("checked"));
            var disno = [];
            var x = 0;
            var j = 0;

            var m = 0;
            var k = 0;

            $.each($("input[name='checkboxdisno']:checked"), function () {
                disno.push($(this).val());
            });

            console.log(disno);

            for (i = 0; i < disno.length; i++) {
                j = $('#total_tx' + disno[i]).val();
                //x = j + x;
                x = (parseFloat(j) + parseFloat(x));

                k = $('#total_am' + disno[i]).val();
                //x = j + x;
                m = (parseFloat(k) + parseFloat(m));
                console.log(i + '---' + j);
                //console.log($('#mt_refinery'+i).val());
            }



            $('#total-amt').html(m.toFixed(2));
            $('#total-taxamt').html(x.toFixed(2));
            $('#total_am').val(m.toFixed(2));
            $('#total_tx').val(x.toFixed(2));
        });



    });
</script>

<div id ="maincontainer">
    <div class="partition">
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
                </tr>
            </table>
            <br>
            <table class="table table-bordered table-striped  table-hover">
                <thead>
                    <tr>
                        <th colspan="4">Input Tax Information
                            <!--<small><span class="label label-primary">Preparing</span></small>-->
                            <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">

                                <%if (!status.equals("Approved") && TaxInputDAO.checkExistDetail(log, v.getRefno())) {
                                        if ((TaxInputDAO.isCheck(log, v.getRefno()) && log.getListUserAccess().get(2).getAccess())) {%>
                                <button  class="btn btn-default btn-xs approve" title="<%= mod.getModuleID()%>" id=""><i class="fa fa-check-square-o" aria-hidden="true"></i> Approve</button>
                                <%} else if (!TaxInputDAO.isApprove(log, v.getRefno()) && log.getListUserAccess().get(1).getAccess()) {%>

                                <button  class="btn btn-default btn-xs check" title="<%= mod.getModuleID()%>" id="" name="addrefer"><i class="fa fa-check-square-o" aria-hidden="true"></i> Check</button>

                                <%}%>

                                <button id="" class="btn btn-default btn-xs editmain" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                <button id=""  class="btn btn-danger btn-xs deletemain" title="<%= mod.getModuleID()%>" id="" name="delete_st"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                    <%} else {%>
                                <button id="" class="btn btn-default btn-xs editmain" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                <button id=""  class="btn btn-danger btn-xs deletemain" title="<%= mod.getModuleID()%>" id="" name="delete_st"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                    <%}%>
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody class="activerowm tdrow">
                    <tr>
                        <th width="15%">Status</th>
                        <td width="35%"></td>
                        <th width="15%">Reference No</th>
                        <td width="35%"><%= v.getRefno()%></td>
                    </tr>
                    <tr>
                        <th>Tax Return Due date</th>
                        <td><%= v.getTaxreturndate()%></td>
                        <th>Name</th>
                        <td><%= v.getName()%></td>
                    </tr>
                    <tr>
                        <th>Taxable Period From</th>
                        <td><%= v.getTaxperiodfrom()%></td>
                        <th>Taxable Period To</th>
                        <td><%= v.getTaxperiodto()%></td>
                    </tr>


                </tbody>
            </table>
            <%if (TaxInputDAO.checkExistDetail(log, v.getRefno())) {%>
            <button id="<%= v.getRefno()%>" class="btn btn-default btn-xs deleteall pull-right" title="<%= mod.getModuleID()%>" name="deleteall">Delete</button>
            <%} else {%>
            <button id="savebutton"  class="btn btn-default btn-xs pull-right" title="<%= mod.getModuleID()%>" name="addinputdetail"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
            <%}%>
            <br>
            <br>
            <form data-toggle="validator" role="form" id="saveform">    
                <table class="table-bordered table-striped table-hover" width="100%">
                    <thead>

                        <tr>
                            <th class="smalltd" width="7%">Trx Date</th>
                            <th class="smalltd">Refno</th>
                            <th class="smalltd">Tax Code</th>
                            <th class="smalltd">Tax Descp</th>
                            <th class="smalltd">Comp Code</th>
                            <th class="smalltd">Comp Name</th>
                            <th class="smalltd">GST ID</th>
                            <th class="smalltd" width="8%">Amount</th>
                            <th class="smalltd" width="5%">Tax</th>
                            <th class="smalltd">Remark</th>
                            <th class="smalltd" width="8%">Tax Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                    <input type="hidden" class="form-control input-sm" name="refno" value="<%= v.getRefno()%>">

                    <%

                        double totalamt = 0.00;
                        double totaltax = 0.00;
                        int o = 0;

                        if (!TaxInputDAO.checkExistDetail(log, v.getRefno())) {

                            List<TxVoucher> listAll = (List<TxVoucher>) TaxInputDAO.getAllVoucher();
                            for (TxVoucher j : listAll) {
                                List<TxInputDetail> listItem = (List<TxInputDetail>) TaxInputDAO.getAllTaxDetail(log, j);
                                if (!listItem.isEmpty()) {%>

                    <tr class="warning">
                        <th class="smalltd" colspan="11"><%= j.getType()%></th>

                    </tr>


                    <%}
                        for (TxInputDetail i : listItem) {
                            o++;
                            totalamt = totalamt + i.getAmount();
                            totaltax = totaltax + i.getTaxamt();
                    %>
                    <input type="hidden" class="form-control input-sm" name="trxtype<%= i.getVoucherrefer()%>" value="<%= i.getTrxtype()%>">
                    <input type="hidden" class="form-control input-sm" id="total_am<%= i.getVoucherrefer()%>" name="total_am<%= i.getVoucherrefer()%>" value="<%= i.getAmount()%>">
                    <input type="hidden" class="form-control input-sm" id="total_tx<%= i.getVoucherrefer()%>" name="total_tx<%= i.getVoucherrefer()%>" value="<%= i.getTaxamt()%>">


                    <tr class="td-click" id="<%= i.getVoucherrefer()%>">
                        <td class="smalltd"><span class="pull-left"><%= i.getDate()%></span></td>
                        <td class="smalltd"><span class="pull-left"><%= i.getTxInputDetailPK().getVoucherno()%></span></td>
                        <td class="smalltd"><span class="pull-left"><%= i.getTxInputDetailPK().getTaxcode()%></span></td>
                        <td class="smalltd"><span class="pull-left"><%= i.getTaxdescp()%></span></td>
                        <td class="smalltd"><span class="pull-left"><%= i.getCompcode()%></span></td>
                        <td class="smalltd"><span class="pull-left"><%= i.getCompname()%></span></td>
                        <td class="smalltd"><span class="pull-left"><%= i.getGstid()%></span></td>
                        <td class="smalltd"><span class="pull-right"><%= GeneralTerm.currencyFormat(i.getAmount())%></span></td>
                        <td class="smalltd"><span class="pull-left"><%= i.getTaxrate()%></span></td>
                        <td class="smalltd"><span class="pull-left"><%= i.getRemarks()%></span></td>
                        <td class="smalltd"><span class="pull-right"><%= GeneralTerm.currencyFormat(i.getTaxamt())%> 
                                &nbsp;
                                <input type="checkbox" id="chkbox<%= i.getVoucherrefer()%>" name="checkboxdisno" class="chcbox-no" value="<%= i.getVoucherrefer()%>" checked></td>
                                </tr>


                                <%}
                                    }
                                } else {
                                    List<TxVoucher> listAll = (List<TxVoucher>) TaxInputDAO.getAllVoucher();
                                    for (TxVoucher j : listAll) {
                                        List<TxInputDetail> listItem = (List<TxInputDetail>) TaxInputDAO.getAllTaxDetailSaved(log, v.getRefno(), j.getAbb());
                                        if (!listItem.isEmpty()) {%>

                                <tr class="warning">
                                    <th class="smalltd" colspan="11"><%= j.getType()%></th>

                                </tr>


                                <%}
                                    for (TxInputDetail i : listItem) {
                                        o++;
                                        totalamt = totalamt + i.getAmount();
                                        totaltax = totaltax + i.getTaxamt();
                                %>
                                <input type="hidden" class="form-control input-sm" name="trxtype<%= i.getVoucherrefer()%>" value="<%= i.getTrxtype()%>">
                                <input type="hidden" class="form-control input-sm" id="total_am<%= i.getVoucherrefer()%>" name="total_am<%= i.getVoucherrefer()%>" value="<%= i.getAmount()%>">
                                <input type="hidden" class="form-control input-sm" id="total_tx<%= i.getVoucherrefer()%>" name="total_tx<%= i.getVoucherrefer()%>" value="<%= i.getTaxamt()%>">


                                <tr class="td-click" id="<%= i.getVoucherrefer()%>">
                                    <td class="smalltd"><span class="pull-left"><%= i.getDate()%></span></td>
                                    <td class="smalltd"><span class="pull-left"><%= i.getTxInputDetailPK().getVoucherno()%></span></td>
                                    <td class="smalltd"><span class="pull-left"><%= i.getTxInputDetailPK().getTaxcode()%></span></td>
                                    <td class="smalltd"><span class="pull-left"><%= i.getTaxdescp()%></span></td>
                                    <td class="smalltd"><span class="pull-left"><%= i.getCompcode()%></span></td>
                                    <td class="smalltd"><span class="pull-left"><%= i.getCompname()%></span></td>
                                    <td class="smalltd"><span class="pull-left"><%= i.getGstid()%></span></td>
                                    <td class="smalltd"><span class="pull-right"><%= GeneralTerm.currencyFormat(i.getAmount())%></span></td>
                                    <td class="smalltd"><span class="pull-left"><%= i.getTaxrate()%></span></td>
                                    <td class="smalltd"><span class="pull-left"><%= i.getRemarks()%></span></td>
                                    <td class="smalltd"><span class="pull-right"><%= GeneralTerm.currencyFormat(i.getTaxamt())%> 
                                            &nbsp;
                                    </td>
                                </tr>


                                <%}
                                        }
                                    }%>

                                <tr>
                                    <td class="smalltd"><strong>TOTAL</strong></td>
                                    <td class="smalltd"></td>
                                    <td class="smalltd"></td>
                                    <td class="smalltd"></td>
                                    <td class="smalltd"></td>
                                    <td class="smalltd"></td>
                                    <td class="smalltd"></td>
                                    <td class="smalltd pull-right"><strong><span id="total-amt"><%= GeneralTerm.currencyFormat(totalamt)%></span></strong></td>
                                    <td class="smalltd"></td>
                                    <td class="smalltd"></td>
                                    <td class="smalltd pull-right">
                                        <strong><span id="total-taxamt"><%= GeneralTerm.currencyFormat(totaltax)%></span></strong>
                                        <input type="hidden" id="total_am" name="total_am" value="<%= totalamt%>">
                                        <input type="hidden" id="total_tx" name="total_tx" value="<%= totaltax%>">
                                    </td>
                                </tr>


                                </tbody>
                </table>
            </form>
        </div>
    </div>
</div>    
