<%-- 
    Document   : ap_pv_select_invoice
    Created on : Oct 19, 2016, 2:43:33 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoice"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.dao.financial.ap.VendorInvoiceDAO"%>
<%@page import="com.lcsb.fms.model.financial.ap.VendorInvoice"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('.activerowy').click(function (e) {
            e.preventDefault();
            var b = $(this).attr('id');
            $("#"+b).toggleClass('success');
            //$(this).toggleClass('info');
            // $('#chkbox'+b).prop('checked', true);
            var checkBox = $('#chkbox' + b);
            checkBox.prop("checked", !checkBox.prop("checked"));
            $("#icon-select-"+b).toggle();
            
            var i = 0;
            $.each($("input[name='invoice-chkbox']:checked"), function () {
                i++;
                $('#invoice-btn').prop('disabled', false);
                //$("#icon-select-"+b).html("<i class=\"fa fa-check\" aria-hidden=\"true\"></i>");
                //$("#icon-select-"+b).html("");
            });
            
            $("#invoice-select").html(i);
            console.log(i);





        });




    });
</script>
<label>&nbsp;<i class="fa fa-info-circle" aria-hidden="true"></i> Please select invoice(s) in order to proceed. </label>
<label class="pull-right">Invoice Selected : <span id="invoice-select">0</span> </label>
<table class="table table-bordered table-striped table-hover" id="list1">
    <thead>

        <tr>
            <th></th>
            <th width="300px">Invoice No</th>
            <th>Date</th>
            <th>Payer</th>
            <th>Amount</th>
        </tr>
    </thead>
    <tbody>
        <%List<SalesInvoice> listx = (List<SalesInvoice>) SalesInvoiceDAO.getNoPaidINV(log,request.getParameter("payercode"));
            int i = 0;
            for (SalesInvoice j : listx) {

                i++;
        %>
        <tr id="<%= j.getInvref()%>" class="activerowy">
            <td width="3%"><label>
                    <input type="checkbox" id="chkbox<%= j.getInvref()%>" class="chkbox" name="invoice-chkbox" value="<%= j.getInvref()%>" style="opacity:0;" required>
                </label>
                    
            </td>
            <td class="tdrow"><%= j.getInvref()%> <span id="icon-select-<%= j.getInvref()%>" style="color: green;display: none"><i class="fa fa-check" aria-hidden="true"></i></span></td>
            <td class="tdrow"><input type="hidden" id="dispatchno<%=i%>" name="dispatchno<%=i%>" value="<%= j.getInvdate()%>"><%= j.getInvdate()%></td>
            <td class="tdrow"><input type="hidden" id="trailer<%=i%>" name="trailer<%=i%>" value="<%= j.getBcode()%>"><%= j.getBcode()%> - <%= j.getBname()%></td>
            <td class="tdrow"><span class="pull-right"><%= GeneralTerm.currencyFormat(j.getAmountno())%></span></td>

        </tr>
        <%}%>

    </tbody>

</table>
