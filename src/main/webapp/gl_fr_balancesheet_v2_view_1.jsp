<%-- 
    Document   : gl_fr_balancesheet_v2_view
    Created on : Oct 5, 2015, 12:02:22 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.report.financial.balancesheet.BsItem"%>
<%@page import="com.lcsb.fms.report.financial.balancesheet.BsMaster"%>
<%@page import="com.lcsb.fms.report.financial.balancesheet.BalanceSheetDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.model.report.BalancesheetModel"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) BalanceSheetDAO.getModule();
    Connection con = ConnectionUtil.getConnection();
    BalancesheetModel blc = new BalancesheetModel();
    AccountingPeriod Acp = new AccountingPeriod();
    GeneralTerm Get = new GeneralTerm();
    String[] maincode = {"12", "13", "1102"};

    String otype = request.getParameter("otype");
    String vby = request.getParameter("vby");
    String year = request.getParameter("year");
    String period = request.getParameter("period");
    String moduleid = request.getParameter("moduleid");


%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#print').click(function (e) {
            printReport($("#viewprint").html());
        });

        $(".goto").click(function () {
            var link = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%=moduleid%>&process=" + link,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


    });
</script>
<link href="css/table_style.css" rel="stylesheet" type="text/css" />
<link href="css/pvoucher.css" rel="stylesheet" type="text/css" />
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="balancesheet_option"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="print"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addinv" ><i class="fa fa-floppy-o"></i>&nbsp;Print</button>
                    </td>
                </tr>
            </table>
            <br>
            <div id="viewprint">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td colspan="4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="50px"><img src="image/logo_lkpp.jpg" width="40px" style="vertical-align:middle" ></td>
                                    <td><span class="bigtitlebold"><%= log.getEstateDescp()%></span><span class="pull-right">SHEET NO. 1A</span><p class="font_inheader_normal">BALANCE SHEET AS AT <%= AccountingPeriod.getEndofPeriod(year, period)%></p></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="border_bottom"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td width="15%" align="right"><p class="column_head_style">THIS YEAR</p></td>
                        <td width="15%" align="right"><p class="column_head_style">PREVIOUS YEAR</p></td>
                    </tr>
                    <tr>
                        <td>&nbsp;<span class="title_row_font_main"><%= BalanceSheetDAO.getBs().get(0).getHeader()%></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"></span></td>
                        <td>&nbsp;</td>
                    </tr>
                    <%
                        BsMaster mt = BalanceSheetDAO.getMaster(year, period, otype, vby, "12", log);
                        List<BsItem> listAll = (List<BsItem>) mt.getListItem();

                        for (BsItem j : listAll) {

                            if (j.isHeader()) {%>


                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;<span class="title_row_font_sub"><%= j.getCoacode()%> <%= j.getCoadescp()%></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"></span></td>
                    </tr>

                    <%
                        }
                        if (j.getCurrentAmount() != 0.0) {
                            if (j.isChild()) {
                    %>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="title_row_font_sub"><%= j.getCoacode()%> <%= j.getCoadescp()%></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getCurrentAmount())%></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getPrevAmount())%></span></td>
                    </tr>
                    <%
                    } else {
                    %>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;<span class="title_row_font_sub"><%= j.getCoacode()%> <%= j.getCoadescp()%></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getCurrentAmount())%></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getPrevAmount())%></span></td>
                    </tr>
                    <%

                                }
                            }

                        }

                    %>
                    <tr>
                        <td>&nbsp;<span class="total_row_font">TOTAL <%= BalanceSheetDAO.getBs().get(0).getHeader()%></span></td>
                        <td align="right" class="border_total_up">&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt.getClusterTotalNow())%></span></td>
                        <td align="right" class="border_total_up">&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt.getClusterTotalPrev())%></span></td>
                    </tr>
                    <tr>
                        <td>&nbsp;<span class="title_row_font_main"></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"></span></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;<span class="title_row_font_main"><%= BalanceSheetDAO.getBs().get(1).getHeader()%></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"></span></td>
                        <td>&nbsp;</td>
                    </tr>
                    <%
                        BsMaster mt2 = BalanceSheetDAO.getMaster(year, period, otype, vby, "13", log);
                        List<BsItem> list2 = (List<BsItem>) mt2.getListItem();
                        for (BsItem j : list2) {

                            if (j.isHeader()) {%>


                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;<span class="title_row_font_sub"><%= j.getCoacode()%> <%= j.getCoadescp()%></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"></span></td>
                    </tr>

                    <%
                        }
                        if (j.getCurrentAmount() != 0.0) {
                            if (j.isChild()) {
                    %>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="title_row_font_sub"><%= j.getCoacode()%> <%= j.getCoadescp()%></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getCurrentAmount())%></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getPrevAmount())%></span></td>
                    </tr>
                    <%
                    } else {
                    %>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;<span class="title_row_font_sub"><%= j.getCoacode()%> <%= j.getCoadescp()%></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getCurrentAmount())%></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getPrevAmount())%></span></td>
                    </tr>
                    <%

                                }
                            }
                        }
                    %>
                    <tr>
                        <td>&nbsp;<span class="total_row_font">TOTAL <%= BalanceSheetDAO.getBs().get(1).getHeader()%></span></td>
                        <td align="right" class="border_total_up">&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt2.getClusterTotalNow())%></span></td>
                        <td align="right" class="border_total_up">&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt2.getClusterTotalPrev())%></td>
                    </tr>
                    <tr>
                        <td>&nbsp;<span class="title_row_font_main"></span></td>
                        <td align="right">&nbsp;<span class="total_row_font"></span></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;<span class="total_row_font_bold">NET CURRENT ASSET</span></td>
                        <td align="right" >&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt.getClusterTotalNow() - mt2.getClusterTotalNow())%></span></td>
                        <td align="right" >&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt.getClusterTotalPrev() - mt2.getClusterTotalPrev())%></td>
                    </tr>


                </table>
                <br>
                <div style="page-break-before:always">

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td colspan="4">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="50px"><img src="image/logo_lkpp.jpg" width="40px" style="vertical-align:middle" ></td>
                                        <td><span class="bigtitlebold"><%= log.getEstateDescp()%></span><span class="pull-right">SHEET NO. 1B</span><p class="font_inheader_normal">BALANCE SHEET AS AT <%= AccountingPeriod.getEndofPeriod(year, period)%></p></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="border_bottom"></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td width="15%" align="right"><p class="column_head_style">THIS YEAR</p></td>
                            <td width="15%" align="right"><p class="column_head_style">PREVIOUS YEAR</p></td>
                        </tr>
                        <tr>
                            <td>&nbsp;<span class="title_row_font_main"><%= BalanceSheetDAO.getBs().get(2).getHeader()%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"></span></td>
                            <td>&nbsp;</td>
                        </tr>
                        <%
                            BsMaster mt3 = BalanceSheetDAO.getMaster(year, period, otype, vby, "11", log);
                            List<BsItem> list3 = (List<BsItem>) mt3.getListItem();
                            for (BsItem j : list3) {

                                if (j.isHeader()) {%>


                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;<span class="title_row_font_sub"><%= j.getCoacode()%> <%= j.getCoadescp()%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"></span></td>
                        </tr>

                        <%
                            }
                            //if (j.getCurrentAmount() != 0.0) {
                            if (j.isChild()) {
                        %>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="title_row_font_sub"><%= j.getCoacode()%> <%= j.getCoadescp()%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getCurrentAmount())%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getPrevAmount())%></span></td>
                        </tr>
                        <%
                        } else {
                        %>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;<span class="title_row_font_sub"><%= j.getCoacode()%> <%= j.getCoadescp()%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getCurrentAmount())%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getPrevAmount())%></span></td>
                        </tr>
                        <%

                                }
                                // }
                            }
                        %>
                        <tr>
                            <td>&nbsp;<span class="total_row_font">TOTAL <%= BalanceSheetDAO.getBs().get(2).getHeader()%></span></td>
                            <td align="right" class="border_total_up">&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt3.getClusterTotalNow())%></span></td>
                            <td align="right" class="border_total_up">&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt3.getClusterTotalPrev())%></span></td>
                        </tr>

                        <tr>
                            <td>&nbsp;<span class="title_row_font_main">LESS :</span></td>
                            <td align="right">&nbsp;<span class="total_row_font"></span></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;<span class="title_row_font_main"><%= BalanceSheetDAO.getBs().get(3).getHeader()%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"></span></td>
                            <td>&nbsp;</td>
                        </tr>
                        <%
                            BsMaster mt4 = BalanceSheetDAO.getMaster(year, period, otype, vby, "110211", log);
                            List<BsItem> list4 = (List<BsItem>) mt4.getListItem();
                            for (BsItem j : list4) {

                                if (j.isHeader()) {%>


                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;<span class="title_row_font_sub"><%= j.getCoacode()%> <%= j.getCoadescp()%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"></span></td>
                        </tr>

                        <%
                            }
                            //if (j.getCurrentAmount() != 0.0) {
                            if (j.isChild()) {
                        %>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="title_row_font_sub"><%= j.getCoacode()%> <%= j.getCoadescp()%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getCurrentAmount())%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getPrevAmount())%></span></td>
                        </tr>
                        <%
                        } else {
                        %>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;<span class="title_row_font_sub"><%= j.getCoacode()%> <%= j.getCoadescp()%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getCurrentAmount())%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getPrevAmount())%></span></td>
                        </tr>
                        <%

                                }
                                //    }
                            }
                        %>

                        <tr>
                            <td>&nbsp;<span class="total_row_font"></span></td>
                            <td align="right" class="border_total_up">&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt4.getClusterTotalNow())%></span></td>
                            <td align="right" class="border_total_up">&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt4.getClusterTotalPrev())%></td>
                        </tr>

                        <tr>
                            <td>&nbsp;<span class="total_row_font">NET FIXED ASSET</span></td>
                            <td align="right" class="border_total_up">&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt3.getClusterTotalNow() - mt4.getClusterTotalNow())%></span></td>
                            <td align="right" class="border_total_up">&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt3.getClusterTotalPrev() - mt4.getClusterTotalPrev())%></td>
                        </tr>
                        <tr>
                            <td>&nbsp;<span class="total_row_font">NET ASSET</span></td>
                            <td align="right" class="border_total_up">&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit((mt.getClusterTotalNow() - mt2.getClusterTotalNow()) - (mt3.getClusterTotalPrev() - mt4.getClusterTotalPrev()))%></span></td>
                            <td align="right" class="border_total_up">&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt2.getClusterTotalPrev())%></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td align="right">&nbsp;<span class="total_row_font"></span></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;<span class="title_row_font_main">FINANCE BY :</span></td>
                            <td align="right">&nbsp;<span class="total_row_font"></span></td>
                            <td>&nbsp;</td>
                        </tr>
                        <%
                            BsMaster mt5 = BalanceSheetDAO.getMaster(year, period, otype, vby, "137101", log);
                            List<BsItem> list5 = (List<BsItem>) mt5.getListItem();
                            for (BsItem j : list5) {
                                if (j.getCurrentAmount() != 0.0) {%>



                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;<span class="title_row_font_sub"><%= j.getCoacode()%> <%= j.getCoadescp()%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getCurrentAmount())%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getPrevAmount())%></span></td>
                        </tr>

                        <%
                                }
                            }

                            BsMaster mt6 = BalanceSheetDAO.getMaster(year, period, otype, vby, "137102", log);
                            List<BsItem> list6 = (List<BsItem>) mt6.getListItem();
                            for (BsItem j : list6) {
                                if (j.getCurrentAmount() != 0.0) {%>



                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;<span class="title_row_font_sub"><%= j.getCoacode()%> <%= j.getCoadescp()%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getCurrentAmount())%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getPrevAmount())%></span></td>
                        </tr>

                        <%
                                }
                            }
                        %>
                        <tr>
                            <td>&nbsp;<span class="total_row_font"></span></td>
                            <td align="right" class="border_total_up">&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt5.getClusterTotalNow() + mt6.getClusterTotalNow())%></span></td>
                            <td align="right" class="border_total_up">&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt5.getClusterTotalPrev() + mt6.getClusterTotalPrev())%></span></td>
                        </tr>
                        <tr>
                            <td>&nbsp;<span class="title_row_font_main"></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"></span></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;<span class="title_row_font_main"><%= BalanceSheetDAO.getBs().get(6).getHeader()%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"></span></td>
                            <td>&nbsp;</td>
                        </tr>
                        <%
                            BsMaster mt7 = BalanceSheetDAO.getMaster(year, period, otype, vby, "3103", log);
                            List<BsItem> list7 = (List<BsItem>) mt7.getListItem();
                            for (BsItem j : list7) {
                                if (j.getCurrentAmount() != 0.0) {%>



                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;<span class="title_row_font_sub"><%= j.getCoacode()%> <%= j.getCoadescp()%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getCurrentAmount())%></span></td>
                            <td align="right">&nbsp;<span class="total_row_font"><%= GeneralTerm.normalCredit(j.getPrevAmount())%></span></td>
                        </tr>

                        <%
                                }
                            }
                        %>
                        <tr>
                            <td>&nbsp;<span class="total_row_font"></span></td>
                            <td align="right" class="border_total_up">&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt7.getClusterTotalNow())%></span></td>
                            <td align="right" class="border_total_up">&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt7.getClusterTotalPrev())%></span></td>
                        </tr>
                        <tr>
                            <td>&nbsp;<span class="total_row_font_bold">TOTAL</span></td>
                            <td align="right" class="border_total_up">&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt5.getClusterTotalNow() + mt6.getClusterTotalNow() - mt7.getClusterTotalNow())%></span></td>
                            <td align="right" class="border_total_up">&nbsp;<span class="total_row_font_bold"><%= GeneralTerm.normalCredit(mt5.getClusterTotalPrev() + mt6.getClusterTotalPrev() - mt7.getClusterTotalPrev())%></span></td>
                        </tr>


                    </table>

                </div>
            </div>
        </div>
    </div>
</div>