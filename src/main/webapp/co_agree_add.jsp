<%@page import="com.lcsb.fms.dao.management.contract.AgreementDAO"%>
<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<% 
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
Module mod = (Module) AgreementDAO.getModule();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        
<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="bootstrap-daterangepicker-master/daterangepicker.css" />
      <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="bootstrap-daterangepicker-master/moment.js"></script>
      <script type="text/javascript" src="bootstrap-daterangepicker-master/daterangepicker.js"></script>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
           
            $('#tarikh').datepicker({
               format:'yyyy-mm-dd',
               defaultDate:'now',
               autoclose:true
            });
           
            $('.range').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
              }
            }, function(start, end, label) {
                $('#date').val(start.format('YYYY-MM-DD'));
                $('#dateend').val(end.format('YYYY-MM-DD'));
            });
           
           
           $("#total").keyup(function(){
                var val = $(this).val();
                var to = 'amount';
                //alert(val);
                //alert(checkRegexp(val,to));
                if(checkRegexp(val,to)){
                    $('#res_digit').empty();
                   $('#div_digit').removeClass('has-error');
                   $('#div_digit').addClass('has-success');
                   $('#res_digit').append("<i class=\"fa fa-check-circle right-green\"></i>&nbsp;&nbsp;");
                    $('#savebutton').prop('disabled', false);
                    convertRM(val,to);
                }else{
                    $('#res_digit').empty();
                   $('#div_digit').removeClass('has-success');
                   $('#div_digit').addClass('has-error');
                   $('#res_digit').append("<i class=\"fa fa-exclamation-circle fail-red\"></i>&nbsp;&nbsp;Numeric Formatted Only");
                    $('#savebutton').prop('disabled', true);
                }
                //
            });
            
             $('#getbuyer').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Buyer Info',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_buyer.jsp?name=bname&code=bcode');
                        $('body').on('click', '.thisresult', function(event){
                            console.log('tutup');
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           $('#getbank').click(function(e){
               
               var a = 'bankcode';
               var b = 'bankname';
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Bank',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_bank.jsp?code='+a+'&name='+b);
                        $('body').on('click', '.thisresult', function(event){
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
           $('#getcheque').click(function(e){
               
               var a = 'cekno';
               var bankcode = $('#bankcode').val();
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Cheque No',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_cheque.jsp?bankcode='+bankcode+'&cekcolumn='+a);
                        $('body').on('click', '.thisresult', function(event){
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
           $('#getlocation').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Location',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_location.jsp');
                        $('body').on('click', '.thisresult_nd', function(event){
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
           $('#gettype').click(function(e){
               //e.preventDefault();
               
               var a = 'paidcode';
               var b = 'paidname';
               var c = 'paidaddress';
               var d = 'paidpostcode';
               var e = 'paidcity';
               var f = 'paidstate';
               var g = 'gstid';
               
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Paid Type',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_type.jsp?code='+a+'&name='+b+'&address='+c+'&postcode='+d+'&city='+e+'&state='+f+'&gstid='+g);
                        $('body').on('click', '.thisresult_nd', function(event){
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
           $("#paymentmode").change(function(){
                
                var thisval = $(this).val();
                
                if(thisval=='Cheque'){
                    $('#getcheque').prop('disabled', false);
                }else{
                    $('#getcheque').prop('disabled', true);
                }
                
           });
           
           $('#getcontractor').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Contractor Info',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        
                        var $content = $('<body></body>').load('list_contractor.jsp?name=name&code=code&address=address&city=city&postcode=postcode&state=state&gstid=gstid&ownername=owner&owneric=ic');
                        $('body').on('click', '.thisresult', function(event){
                            console.log('tutup');
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
        });
        
       </script>
    </head>
    <body>
        <div id ="maincontainer">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
              <td width="74%" class="borderbot" align="left"><span class="bigfonttitle"><%= mod.getModuleDesc() %></span>&nbsp;&nbsp;<span class="midfonttitle">Add New <%= mod.getModuleDesc() %></span></td>
		<td width="26%" class="borderbot" align="right">&nbsp;</td>
	  </tr>
  </table>
	<br>
   <div class="partition"> 
       <div class="headofpartition"></div>
       <div class="bodyofpartition">
           
           <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
                    <td valign="middle" align="left">
			<button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="addmain" ><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
		</td>
		</tr>
  </table>
  <br>
  

  
<form data-toggle="validator" role="form" id="saveform">
    <input type="hidden" name="preid" id="pid" value="<%= log.getUserID()%>">
    <input type="hidden" name="prename" id="pname" value="<%= log.getFullname()%>">
    <input type="hidden" name="predate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp() %>">
    <input type="hidden" name="estatecode" id="coacode" value="<%= log.getEstateCode()%>">
    <input type="hidden" name="estatename" id="coadesc" value="<%= log.getEstateDescp()%>">
    <div class="well">
    <div class="row">
        
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Contract No &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="refer" name="refer" placeholder="Auto Generated" autocomplete="off" readonly>   
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Year &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="year" name="year" placeholder="Auto Generated" autocomplete="off" value="<%= AccountingPeriod.getCurYear(log)%>" readonly>   
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Period &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="period" name="period" placeholder="Date of Payment Voucher" autocomplete="off" value="<%= AccountingPeriod.getCurPeriod(log)%>" readonly>   
             </div>
        </div>
    </div>
    
    </div><div class="well">
    
   <div class="row">
        
       <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Contract Type&nbsp&nbsp<span class="res_code"></span></label>
                <select class="form-control input-sm" id="typeagree" name="typeagree">
                    <%= ParameterDAO.parameterList(log,"Agreement Type","") %>
                </select>
                                    </div>  
             </div>
        
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Company Code &nbsp&nbsp<span class="res_code"></span></label>
                <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="code" name="code" readonly>
                                        <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="getcontractor"><i class="fa fa-cog"></i> Get Contractor</button>
                                        </span>
                                    </div></div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Company Name &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="name" name="name" placeholder="" autocomplete="off" readonly > 
             </div>
        </div>
   </div>
        <div class="row">
            <div class="col-sm-4">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">GST ID &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <input type="text" class="form-control input-sm" id="gstid" name="gstid" placeholder="" autocomplete="off" > 
             </div>
            </div>
            <div class="col-sm-8">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Address &nbsp&nbsp<span class="res_code"></span></label>
                <textarea class="form-control" rows="4"  id="address" name="address"></textarea>
             </div>
        </div>
        </div>
    <div class="row">
    
        <div class="col-sm-8">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Owner Name &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="owner" name="owner" placeholder="" autocomplete="off" > 
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Owner IC &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="ic" name="ic" placeholder="" autocomplete="off" > 
             </div>
        </div>
       
    </div>
    </div>
    
    <div class="well">
        
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Contract Period &nbsp&nbsp<span class="res_code"></span></label>
                    <div class="form-group-sm input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-sm range" type="button"><i class="fa fa-calendar"></i></button>
                        </span>
                        <input type="text" class="form-control input-sm" id="date" name="date" placeholder="Start Date" readonly>
                        <input type="hidden" id="rangeperiod">

                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">&nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="dateend" name="dateend" placeholder="End Date" autocomplete="off" value="" readonly>   
             </div>
            </div>
        </div>
    </div>
                
    
    
          
   
    
    
    
    <!--<div class="form-group-sm input-group">
    <input type="text" class="form-control input-sm">
    <span class="input-group-btn">
    <button class="btn btn-default btn-sm" type="button"><i class="fa fa-cog"></i> Get Account Code</button>
    </span>
    </div>-->
    
</form>

     </div>
     </div>
      </div>
      

    </body>
</html>


