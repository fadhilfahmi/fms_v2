<%-- 
    Document   : gl_fr_gllisting
    Created on : Nov 30, 2016, 3:50:20 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.report.financial.gl.GListDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) GListDAO.getModule();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="jsfunction/jquery-1.12.0.min.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/formValidation.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/bootstrap.js"></script>

        <link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {

                $('.dateformat').datepicker({
                    format: 'yyyy-mm-dd',
                    defaultDate: 'now',
                    autoclose: true
                });


                


                $('#getestate').click(function (e) {

                    var a = 'loccode';
                    var b = 'locname';
                    //e.preventDefault();
                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_DEFAULT,
                        title: 'Get Estate',
                        //message: $('<body></body>').load('list_coa.jsp')
                        message: function (dialog) {
                            var $content = $('<body></body>').load('list_estate.jsp?code=' + a + '&name=' + b);
                            $('body').on('click', '.thisresult', function (event) {
                                dialog.close();
                                event.preventDefault();
                            });

                            return $content;
                        }
                    });

                    return false;
                });



                $('#getlocation').click(function (e) {
                    //e.preventDefault();
                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_DEFAULT,
                        title: 'Get Location',
                        //message: $('<body></body>').load('list_coa.jsp')
                        message: function (dialog) {
                            var $content = $('<body></body>').load('list_location.jsp');
                            $('body').on('click', '.thisresult_nd', function (event) {
                                dialog.close();
                                event.preventDefault();
                            });

                            return $content;
                        }
                    });

                    return false;
                });

                $('#getsub').click(function (e) {
                    //e.preventDefault();
                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_DEFAULT,
                        title: 'Get Sub Account Detail',
                        //message: $('<body></body>').load('list_coa.jsp')
                        message: function (dialog) {
                            var $content = $('<body></body>').load('list_sub.jsp');
                            $('body').on('click', '.thisresult_nd', function (event) {
                                dialog.close();
                                event.preventDefault();
                            });

                            return $content;
                        }
                    });

                    return false;
                });

                $('#generate').click(function (e) {

                    e.preventDefault();
                    var refer = $(this).attr('href');
                    $.ajax({
                        url: "PathController?moduleid=<%= mod.getModuleID()%>&process=generateGL",
                        success: function (result) {
                            $('#herey').empty().html(result).hide().fadeIn(300);
                        }
                    });

                    return false;
                });



            });

        </script>
    </head>
    <body>

        <div id ="maincontainer">
            <div class="partition"> 
                <div class="bodyofpartition">

                    <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                        <tr>
                            <td valign="middle" align="left">
                                <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                            </td>
                        </tr>
                    </table>
                    <br>

                    <form data-toggle="validator" role="form" id="saveform">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="well">

                                    <div class="row self-row">
                                        <label for="inputName" class="control-label">Trial Balance Parameter</label>
                                    </div>


                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">Year</label>
                                                <select class="form-control input-sm" id="year" name="year">
                                                    <%= ParameterDAO.parameterList(log,"Year", AccountingPeriod.getCurYear(log))%>
                                                </select> 
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">Period</label>
                                                <select class="form-control input-sm" id="period" name="period">
                                                    <%= ParameterDAO.parameterList(log,"Period", AccountingPeriod.getCurPeriod(log))%>
                                                </select> 
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group" id="div_code">
                                                <label for="inputName" class="control-label">Type</label>
                                                <select class="form-control input-sm" id="otype" name="otype">
                                                    <%= ParameterDAO.parameterList(log,"Balance Sheet Type", "")%>
                                                </select> 

                                                            
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!--<div class="form-group" id="div_code">
                                                <label for="inputName" class="control-label">To Account&nbsp&nbsp<span class="res_code"></span></label>
                                                <div class="form-group-sm input-group">
                                                    <input type="text" class="form-control input-sm" id="tcode" name="tcode" value="31">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default btn-sm getaccount" type="button" id="tcode" id1="actdesc"><i class="fa fa-cog"></i></button>
                                                    </span>
                                                </div>  
                                            </div>-->
                                        </div>
                                    </div>





                                </div></div>
                            <div class="col-sm-6">
                                <div class="well">

                                    <div class="row self-row">
                                        <label for="inputName" class="control-label">Additional Parameter &nbsp&nbsp<span id="res_code"></span></label>
                                    </div>


                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">Location</label>
                                                <div class="form-group-sm input-group">
                                                    <input type="text" class="form-control input-sm" id="loclevel" name="loclevel" value="Company" readonly>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default btn-sm" type="button" id="getlocation"><i class="fa fa-cog"></i> Location</button>
                                                    </span>
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group" id="div_code">
                                                <label for="inputName" class="control-label">&nbsp;&nbsp;<span class="res_code"></span></label>
                                                <input type="text" class="form-control input-sm" id="loccode" name="loccode" value="<%= log.getEstateCode()%>"  autocomplete="off" readonly>   
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-sm">
                                                <div class="form-group" id="div_code">
                                                    <input type="text" class="form-control input-sm" id="locdesc" name="locdesc" value="<%= log.getEstateDescp()%>" autocomplete="off" value="" readonly >

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    





                                </div>
                            </div>
                        </div>
                    </form>                               
                    <div class="row pull-right">
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="generateTB">Generate Report</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                </div>
            </div>
        </div>


    </body>
</html>
