<%-- 
    Document   : cb_official_prepare_voucher
    Created on : Oct 25, 2016, 9:17:27 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.setup.configuration.BuyerDAO"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Buyer"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) OfficialReceiptDAO.getModule();

    Buyer sp = (Buyer) BuyerDAO.getInfo(log,request.getParameter("buyercode"));

    String remark = OfficialReceiptDAO.getRemark(log,request.getParameter("sessionid"));
    double total = OfficialReceiptDAO.getSumInvoice(log,request.getParameter("sessionid"));

%>
<!DOCTYPE html>
<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });
        //$('#JVdate').datepicker({ dateFormat: 'yyyy-mm-dd' });

        $('#date').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        }).change(dateChanged)
                .on('changeDate', dateChanged);

      

        $("#amount").keyup(function () {
            var val = $(this).val();
            var to = 'rm';
            //alert(val);
            //alert(checkRegexp(val,to));
            if (checkRegexp(val, to)) {
                $('#res_digit').empty();
                $('#div_digit').removeClass('has-error');
                $('#div_digit').addClass('has-success');
                $('#res_digit').append("<i class=\"fa fa-check-circle right-green\"></i>&nbsp;&nbsp;");
                $('#savebutton').prop('disabled', false);
                convertRM(val, to);
            } else {
                $('#res_digit').empty();
                $('#div_digit').removeClass('has-success');
                $('#div_digit').addClass('has-error');
                $('#res_digit').append("<i class=\"fa fa-exclamation-circle fail-red\"></i>&nbsp;&nbsp;Numeric Formatted Only");
                $('#savebutton').prop('disabled', true);
            }
            //
        });

        $('#getbuyer').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Buyer Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_buyer.jsp?name=paidname&code=paidcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });
        $('#getbank').click(function (e) {

            var a = 'bankcode';
            var b = 'bankname';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Bank',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_bank.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        $(".required-item").trigger("keyup");
                        event.preventDefault();
                    });

                    return $content;
                }
            });



            return false;
        });

        $('#gettype').click(function (e) {
            //e.preventDefault();

            var a = 'paidcode';
            var b = 'paidname';
            var c = 'paidaddress';
            var d = 'paidpostcode';
            var e = 'paidcity';
            var f = 'paidstate';
            var g = 'gstid';

            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Paid Type',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_type.jsp?code=' + a + '&name=' + b + '&address=' + c + '&postcode=' + d + '&city=' + e + '&state=' + f + '&gstid=' + g);
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        convertRM('<%= total%>', 'amount');

        $('.required-item').keyup(function (e) {
            if (($('#bankcode').val() == '') || ($('#bankname').val() == '')) {
                $('#prepare-btn').prop('disabled', true);
            } else {
                console.log('ok');
                $('#prepare-btn').prop('disabled', false);
            }


        });
    });

</script>
</head>




<form data-toggle="validator" role="form" id="savemaster">
    <input type="hidden" name="prepareid" id="pid" value="<%= log.getUserID()%>">
    <input type="hidden" name="preparename" id="pname" value="<%= log.getFullname()%>">
    <input type="hidden" name="preparedate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
    <input type="hidden" name="coacode" id="coacode" value="">
    <input type="hidden" name="coadesc" id="coadesc" value="">
    <input type="hidden" class="form-control input-sm" id="estatecode" name="estatecode" placeholder="0.00" autocomplete="off" value="<%= log.getEstateCode()%>" readonly > 
    <input type="hidden" class="form-control input-sm" id="estatename" name="estatename" placeholder="0.00" autocomplete="off" value="<%= log.getEstateDescp()%>" readonly> 

    <label for="inputName" class="control-label">High Priority Information&nbsp&nbsp</label><small>| Fill this field in order to proceed</small>
    <div class="well">
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group" id="div_code">
                    <i class="fa fa-star" style="font-size:10px; color:#f9546a"></i>&nbsp;<label for="inputName" class="control-label">Date &nbsp&nbsp<span class="res_code"></span></label>
                    <input type="text" class="form-control input-sm" id="date" name="date" placeholder="Date of Journal Voucher" autocomplete="off" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">   
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group" id="div_digit">
                    <i class="fa fa-star" style="font-size:10px; color:#f9546a"></i>&nbsp;<label for="inputName" class="control-label">Bank Code &nbsp&nbsp<span class="res_code"></span></label>
                    <div class="form-group-sm input-group">
                        <input type="text" class="form-control input-smd required-item" id="bankcode" name="bankcode">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-sm" type="button" id="getbank"><i class="fa fa-cog"></i> Bank</button>
                        </span>
                    </div>  
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group" id="div_digit">
                    <label for="inputName" class="control-label">Bank Name</label> <small>Bank where the transaction goes</small>
                    <input class="form-control input-sm required-item" style="font-size:12px" name="bankname" id="bankname" type="text" value="" size="50"  />
                </div>
            </div>
        </div>
    </div>
    <div class="well">

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Year &nbsp&nbsp<span class="res_code"></span></label>
                    <input type="text" class="form-control input-sm" id="year" name="year" placeholder="Year" autocomplete="off" value="<%= AccountingPeriod.getCurYear(log)%>">   
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Period &nbsp&nbsp<span class="res_code"></span></label>
                    <input type="text" class="form-control input-sm" id="period" name="period"  autocomplete="off" value="<%= AccountingPeriod.getCurPeriod(log)%>">   
                </div>
            </div>
        </div>
    </div>
    <label for="inputName" class="control-label">Medium Priority Information&nbsp&nbsp</label><small>| Payer detail is from Buyer Information, please ensure the detail provided.</small>
    <div class="well">
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Paid Type&nbsp&nbsp<span class="res_code"></span></label>
                    <div class="form-group-sm input-group">
                        <input type="text" class="form-control input-sm" id="paidtype" name="paidtype"  value="Buyer" readonly>
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-sm" type="button" id="gettype"><i class="fa fa-cog"></i> Paid Type</button>
                        </span>
                    </div>    
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Paid Code &nbsp&nbsp<span class="res_code"></span></label>
                    <input type="text" class="form-control input-sm" id="paidcode" name="paidcode" value="<%= sp.getCode()%>" autocomplete="off" >   
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Paid Name&nbsp&nbsp<span class="res_code"></span></label>
                    <input type="text" class="form-control input-sm" id="paidname" name="paidname" value="<%= sp.getCompanyname()%>" autocomplete="off" > 
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">GST ID&nbsp&nbsp<span class="res_code"></span></label>
                    <input type="text" class="form-control input-sm" id="gstid" name="gstid" value="<%= sp.getGstid()%>" autocomplete="off" > 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Address &nbsp&nbsp<span class="res_code"></span></label>
                    <textarea class="form-control" rows="3"  id="paidaddress" name="paidaddress"><%= sp.getAddress()%></textarea>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group" id="div_digit">
                    <label for="inputName" class="control-label">Postcode &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                    <input type="text" class="form-control input-sm" id="paidpostcode" name="paidpostcode" value="<%= sp.getPostcode()%>" autocomplete="off" > 
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group" id="div_digit">
                    <label for="inputName" class="control-label">City &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                    <input type="text" class="form-control input-sm" id="paidcity" name="paidcity" value="<%= sp.getCity()%>" autocomplete="off" > 
                </div>
            </div><div class="col-sm-3">
                <div class="form-group" id="div_digit">
                    <label for="inputName" class="control-label">State &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                    <input type="text" class="form-control input-sm" id="paidstate" name="paidstate" value="<%= sp.getState()%>" autocomplete="off" > 
                </div>
            </div>
        </div>
    </div>
    <div class="well">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group" id="div_digit">
                    <label for="inputName" class="control-label">Amount (RM) &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                    <input type="text" class="form-control input-sm" id="amountno" name="amount" value="<%= total%>" autocomplete="off" > 
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">RM (In Words) &nbsp&nbsp<span class="res_code"></span></label>
                    <textarea class="form-control" rows="3"  id="amount" name="rm"></textarea>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Remarks &nbsp&nbsp<span class="res_code"></span></label>
                    <textarea class="form-control" rows="3"  id="remarks" name="remarks"><%= remark%></textarea>
                </div>
            </div>
        </div>
    </div>
    <label for="inputName" class="control-label">Low Priority Information&nbsp&nbsp</label><small>| Optional, enter when needed.</small>
    <div class="well">         
        <div class="row">
            <div class="col-sm-5">
                <div class="form-group" id="div_code">
                    <label for="inputName" class="control-label">Rounding Adjustment Code &nbsp&nbsp<span class="res_code"></span></label>
                    <div class="form-group-sm input-group">
                        <input type="text" class="form-control input-sm" id="racoacode" name="racoacode">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-sm getaccount" type="button" id="racoacode" id1="racoadesc"><i class="fa fa-cog"></i> Account</button>
                        </span>
                    </div>  
                </div>
            </div>
            <div class="col-sm-7">
                <div class="form-group" id="div_digit">
                    <label for="inputName" class="control-label">Rounding Adjustment Description &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                    <input type="text" class="form-control input-sm" id="racoadesc" name="racoadesc" placeholder="" autocomplete="off" > 
                </div>
            </div>

        </div>        




        <!--<div class="form-group-sm input-group">
        <input type="text" class="form-control input-sm">
        <span class="input-group-btn">
        <button class="btn btn-default btn-sm" type="button"><i class="fa fa-cog"></i> Get Account Code</button>
        </span>
        </div>-->

</form>