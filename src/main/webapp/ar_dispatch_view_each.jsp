<%-- 
    Document   : ar_dispatch_view_each
    Created on : Aug 8, 2017, 4:29:35 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.financial.ar.ArCPORefinery"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.financial.ar.ArDispatchDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
ArCPORefinery v = ArDispatchDAO.getRefinery(log, request.getParameter("type"), request.getParameter("location"), request.getParameter("disno"));
%>
<div class="well">

                <form data-toggle="validator" role="form" id="saveform">
                    <input type="hidden" class="form-control input-sm" id="old_dispatch_no" name="old_dispatch_no" value="<%= v.getDispatchNo()%>">
                    <input type="hidden" class="form-control input-sm" id="type" name="type" value="<%= request.getParameter("type") %>">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-sm">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Dispatch No.&nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="dispatch_no" name="dispatch_no" value="<%= v.getDispatchNo()%>">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Contract No</label>
                                <input type="text" class="form-control input-sm" id="contract" name="contract" value="<%= v.getContract()%>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Date</label>
                                <input type="text" class="form-control input-sm dateformat" id="date" name="date" value="<%= v.getDate() %>">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Trailer</label>
                                <input type="text" class="form-control input-sm dateformat" id="trailer" name="trailer" value="<%= v.getTrailer()%>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-sm">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Driver&nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="driver" name="driver" value="<%= v.getDriver()%>">

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Mt</label>
                                <input type="text" class="form-control input-sm dateformat" id="mt" name="mt" value="<%= v.getMt()%>">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Mt Refinery</label>
                                <input type="text" class="form-control input-sm dateformat" id="mt_refinery" name="mt_refinery" value="<%= v.getMtRefinery()%>">
                            </div>
                        </div>
                    </div>
                              


                </form>
            </div>
