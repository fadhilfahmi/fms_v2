<%-- 
    Document   : ar_rpt_aging
    Created on : Mar 28, 2017, 6:25:01 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.financial.ar.ArReportDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) ArReportDAO.getModule();
%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" type="text/css" media="all" href="bootstrap-daterangepicker-master/daterangepicker.css" />
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="bootstrap-daterangepicker-master/moment.js"></script>
<script type="text/javascript" src="bootstrap-daterangepicker-master/daterangepicker.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


//function(start, end, label) { console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')'); });
        $('.datepicker').datepicker({
         format: 'yyyy-mm-dd'
         });

        $('#getbuyer').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Buyer Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_buyer.jsp?name=bname&code=bcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        ///$('.btnoption').click(function (e) {
        ////    var l = $('input[name=viewby]:checked').val();
        //   console.log(l);
        //   return false;
        //});
        $('.btnoption').on('click', function () {
            var v = ($(this).find('input').val());
            
            if(v=='all'){
                $(".buyer").prop('disabled', true);
                $('#getbuyer').prop('disabled', true);
                $('.buyer').val('')
                
            }else if(v=='bybuyer'){
                $(".buyer").prop('disabled', false);
                $('#getbuyer').prop('disabled', false);
            }
            
        });


    });
</script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
                </tr>
            </table>
            <br>

            <form data-toggle="validator" role="form" id="saveform">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="well">

                            <div class="row self-row">
                                <label for="inputName" class="control-label">Aging Analysis Parameter</label>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Until Date</label>
                                        <div class="form-group-sm input-group">
                                            <input type="text" id="todate" name="todate" class="form-control datepicker" >
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-sm" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="inputName" class="control-label">Choose Buyer</label><br>
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default btn-sm btnoption active">
                                            <input type="radio" name="viewby" id="viewby" value="All" autocomplete="off" checked> All
                                        </label>
                                        <label class="btn btn-default btn-sm btnoption">
                                            <input type="radio" name="viewby" id="viewby" value="Buyer"  autocomplete="off"> Select Buyer
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Buyer Code</label>
                                        <div class="form-group-sm input-group">
                                            <input type="text" class="form-control input-sm buyer" id="bcode" name="buyercode" disabled>
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-sm" type="button" id="getbuyer" disabled><i class="fa fa-cog"></i> Buyer</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Buyer Description</label>
                                        <input type="text" class="form-control input-sm buyer" id="bname" name="buyername" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">                       
                        <div class="row pull-right">
                            <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewaging">Generate Report</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                </div>
            </form>                               

        </div>
    </div>
</div>