<%-- 
    Document   : cf_coa_edit
    Created on : Mar 11, 2016, 8:58:23 AM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Vendor"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.VendorDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) VendorDAO.getModule();
    Vendor edit = (Vendor) VendorDAO.getInfo(log,request.getParameter("referenceno"));

%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $('#getbank').click(function (e) {

            var a = 'bankcode';
            var b = 'bankname';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Taxation',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_bank.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });
    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="editprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>
                <form data-toggle="validator" role="form" id="saveform">
                    <input type="hidden" name="prepareid" id="pid" value="<%= log.getUserID()%>">
                    <input type="hidden" name="preparename" id="pname" value="<%= log.getFullname()%>">
                    <input type="hidden" name="preparedate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Code&nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="code" name="code" value="<%= edit.getCode()%>" autocomplete="off" required readonly >  
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Company Name &nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="vendor_name" name="vendor_name" autocomplete="off" value="<%= edit.getVendorName()%>">   
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Registration No &nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="register_no" name="register_no" placeholder="Date of Journal Voucher" autocomplete="off" value="<%= edit.getRegisterNo()%>">   
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Address &nbsp&nbsp<span class="res_code"></span></label>
                                    <textarea class="form-control" rows="3"  id="vendor_address" name="vendor_address"><%= edit.getVendorAddress()%></textarea>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group" id="div_digit">
                                    <label for="inputName" class="control-label">Postcode &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                    <input type="text" class="form-control input-sm" id="postcode" name="postcode" placeholder="" autocomplete="off" value="<%= edit.getPostcode()%>"> 
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group" id="div_digit">
                                    <label for="inputName" class="control-label">City &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                    <input type="text" class="form-control input-sm" id="city" name="city" placeholder="" autocomplete="off" value="<%= edit.getCity()%>"> 
                                </div>
                            </div><div class="col-sm-3">
                                <div class="form-group" id="div_digit">
                                    <label for="inputName" class="control-label">State &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                    <input type="text" class="form-control input-sm" id="state" name="state" placeholder="" autocomplete="off" value="<%= edit.getState()%>"> 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Active &nbsp&nbsp<span class="res_code"></span></label>
                                    <select class="form-control input-sm" id="active" name="active">
                                        <%= ParameterDAO.parameterList(log,"YesNo Type", edit.getActive())%>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group" id="div_digit">
                                    <label for="inputName" class="control-label">Bumiputra &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                    <select class="form-control input-sm" id="bumiputra" name="bumiputra">
                                        <%= ParameterDAO.parameterList(log,"YesNo Type", edit.getBumiputra())%>
                                    </select> 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group" id="div_digit">
                                    <label for="inputName" class="control-label">Position &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                    <select class="form-control input-sm" id="position" name="position">
                                        <%= ParameterDAO.parameterList(log,"Position", edit.getPosition())%>
                                    </select> 
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group" id="div_digit">
                                    <label for="inputName" class="control-label">Title &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                    <select class="form-control input-sm" id="title" name="title">
                                        <%= ParameterDAO.parameterList(log,"Title", edit.getTitle())%>
                                    </select> 
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group" id="div_digit">
                                    <label for="inputName" class="control-label">Contact Person &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                    <input type="text" class="form-control input-sm" id="person" name="person" placeholder="" autocomplete="off" value="<%= edit.getPerson()%>"> 
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group" id="div_digit">
                                    <label for="inputName" class="control-label">HP No &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                    <input type="text" class="form-control input-sm" id="hp" name="hp" placeholder="" autocomplete="off" value="<%= edit.getHp()%>">  
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group" id="div_digit">
                                    <label for="inputName" class="control-label">Phone No &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                    <input type="text" class="form-control input-sm" id="phone" name="phone" placeholder="" autocomplete="off" value="<%= edit.getPhone()%>" > 
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group" id="div_digit">
                                    <label for="inputName" class="control-label">Fax No &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                    <input type="text" class="form-control input-sm" id="fax" name="fax" placeholder="" autocomplete="off" value="<%= edit.getFax()%>" > 
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group" id="div_digit">
                                    <label for="inputName" class="control-label">Email &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                    <input type="text" class="form-control input-sm" id="email" name="email" placeholder="" autocomplete="off" value="<%= edit.getEmail()%>" > 
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group" id="div_digit">
                                    <label for="inputName" class="control-label">Payment Method &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                    <select class="form-control input-sm" id="payment" name="payment">
                                        <%= ParameterDAO.parameterList(log,"Payment Method", edit.getPayment())%>
                                    </select> 
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group" id="div_digit">
                                    <label for="inputName" class="control-label">Bank &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                    <input type="text" class="form-control input-sm" id="bank" name="bank" placeholder="" autocomplete="off" value="<%= edit.getBank()%>" > 
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group" id="div_digit">
                                    <label for="inputName" class="control-label">Account No &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                    <input type="text" class="form-control input-sm" id="bankaccount" name="bankaccount" placeholder="" autocomplete="off" value="<%= edit.getBankaccount()%>" > 
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Remarks &nbsp&nbsp<span class="res_code"></span></label>
                                    <textarea class="form-control" rows="3"  id="remarks" name="remarks"><%= edit.getRemarks()%></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
        </div>
    </div>
</div>

