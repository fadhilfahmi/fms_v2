<%-- 
    Document   : list_module
    Created on : Oct 23, 2016, 11:58:35 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.administration.om.UserDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    
                         List<Module> clist = (List<Module>) UserDAO.getModuleAccess(log,2,null,log.getUserID());
                            if(clist.isEmpty()){%>
                               <li>
                                    <a href="#"><i class="fa fa-dashboard fa-fw"></i> You dont have access to any module.</a>
                                </li> <%
                            }
                            for (Module c : clist) {
                                if(c.isAccess()){
                        %>    
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> <%= c.getModuleDesc() %><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                               <%
                                List<Module> dlist = (List<Module>) UserDAO.getModuleAccess(log,4,c.getModuleID(),log.getUserID());
                                        for (Module d : dlist) {
                                            
                                        if(d.isAccess()){
                                %> 
                                <li>
                                    <a href="#"><%= d.getModuleDesc() %> <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <%
                                        List<Module> elist = (List<Module>) UserDAO.getModuleAccess(log,6,d.getModuleID(),log.getUserID());
                                        for (Module e : elist) {
                                            if(e.isAccess()){
                                        
                                        %>
                                        <li class="moduleto">
                                            <a class="tomodule" href="../fms_v1/ModuleController?moduleid=<%= e.getModuleID() %>"><%= e.getModuleDesc() %></a>
                                        </li>
                                        <%}}%>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                                <%}}%>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <%}}%>
