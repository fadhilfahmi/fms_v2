<%-- 
    Document   : cf_parameter_view
    Created on : Apr 25, 2016, 10:47:24 AM
    Author     : user
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Param"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ParamDAO"%>

<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
Param view = (Param) ParamDAO.getInfo(log,request.getParameter("id"));
%>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
           
            
              $( ".printbutton" ).click(function() {
                  //var a = $("form").serialize();
                  var a = $("#saveform :input[value!='']").serialize(); 
                  //alert($('input[name="ad_amount"]').val());
                  checkField();
                  var b = checkField();
                  if(b==false){
                    $.ajax({
                        data: a,
                        type: 'POST',
                        url: "DataController?do=add&formlevel=master",
                        success: function (result) {
                          $('#maincontainer').empty().html(result).hide().fadeIn(300);
                        }
                    });
                     
                  }    
                  
               
                    return false;
              });
              
         });
        </script>
            <div id ="maincontainer">
                <table id="table_2" cellspacing="0">
                    
               <tr>
                   <td width="30%" class="bd_bottom" align="left" valign="top">Parameter</td>
                   <td width="70%" class="bd_bottom" align="left"><%= view.getParameter() %></td>
               </tr>
               <tr>
                   <td class="bd_bottom" align="left" valign="top">Value</td>
                   <td class="bd_bottom" align="left"> 
                       <%= view.getValue()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Order</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getDorder()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">ID Number</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getId()%>
                   </td>
               </tr>
              
          </table>
            </div>
