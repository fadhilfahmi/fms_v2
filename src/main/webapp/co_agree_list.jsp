<%-- 
    Document   : ap_pv__list
    Created on : May 10, 2016, 12:38:55 PM
    Author     : user
--%>

<%@page import="com.lcsb.fms.dao.management.contract.AgreementDAO"%>
<%@page import="com.lcsb.fms.util.model.ListTable"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.ModuleItem"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.ConnectionModel"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,java.util.*" errorPage="" isThreadSafe="false" %>
<%@ page import="java.util.Date"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
Module mod = (Module) AgreementDAO.getModule();

response.setHeader("Cache-Control", "no-cache");

SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
DecimalFormat dcf=new DecimalFormat("###,###,###,###,###,###,##0.00");
String [] dat =new String [17];
String date = formatter.format(new Date());
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>IPAMIS</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/tether.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>

<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
 <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
    
				
var updateTable = '<%= mod.getMainTable() %>';

<%
List<ListTable> mlist = (List<ListTable>) AgreementDAO.tableList();
for (ListTable m : mlist) {
    //if(m.getList_View().equals("1")){
        %>
        columntoView.push('<%= m.getColumnName()%>');
        columnName.push('<%= m.getTitleName()%>');
        <%    
    //}
    
}
%>
		
var colsTosend = 'cols=';
for (m = 0; m < columntoView.length; ++m) {//put value from database to variable jget
if(m == columntoView.length - 1){
colsTosend += columntoView[m];
}else{
colsTosend += columntoView[m]+'&cols=';
}
}


	  $( "#add-new" ).click(function() {
                                        $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=addnew",
                                            success: function (result) {
                                            // $("#haha").html(result);
                                            $('#herey').empty().html(result).hide().fadeIn(300);
                                        }});
                                        return false;
	  });

	  
          $( "#check-button" ).click(function() {
                $.ajax({
                    url: "PathController?moduleid=<%= mod.getModuleID() %>&process=check",
                    success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
                return false;
	  });
          
            
	
	  var orderby = 'order by voucherno desc';
	  var oTable = $('#example').dataTable( {
              "responsive": true,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "retrievetable.jsp?table="+updateTable+"&code=<%= mod.getReferID_Master() %>&"+colsTosend+"&order="+orderby,
		"aoColumns": [
		  {"sTitle": columnName[0], "mData": columntoView[0], "sWidth": "15%", "bSearchable": true},
		  {"sTitle": columnName[1], "mData": columntoView[1], "sWidth": "35%"},
		  {"sTitle": columnName[2], "mData": columntoView[2], "sWidth": "15%", "bVisible": false},
		  {"sTitle": columnName[3], "mData": columntoView[3], "sWidth": "15%", "bVisible": false},
		  {"sTitle": "Status", "mData": null, "sWidth": "20%"},
		  {"sTitle": "Action", "mData": null, "bSortable": false, "bSearchable": false, "sWidth": "30%"}
			
			],
		  "aoColumnDefs": [ {
			"aTargets": [5],
			  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
			  	
				var action = '<div class="btn-group">';
                                    //action += '<button type="button" class="btn btn-default btn-xs">Action</button>';
                                    action += '<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                                    action += '<span><i class="fa fa-plus-circle"></i></span>';
                                    action += '<span class="sr-only">Toggle Dropdown</span>';
                                    action += '</button>';
                                    action += '<ul class="dropdown-menu btn-xs">';
                                    action += '<li class="btn-xs"><a href="'+oData.refer+'" id="addaccount">Job</a></li>';
                                    action += '<li class="btn-xs"><a href="'+oData.refer+'" id="addreference">Variance Order</a></li>';
                                    //action += '<li role="separator" class="divider"></li>';
                                    //action += '<li class="btn-xs"><a href="#"><i class="fa fa-paper-plane" aria-hidden="true"></i> Post</a></li>';
                                    action += '</ul>';
                                    action += '</div>';
                                    
                                    var check = '<div class="btn-group">';
                                    //action += '<button type="button" class="btn btn-default btn-xs">Action</button>';
                                    check += '<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                                    check += '<span><i class="fa fa-check-square"></i></span>';
                                    check += '<span class="sr-only">Toggle Dropdown</span>';
                                    check += '</button>';
                                    check += '<ul class="dropdown-menu btn-xs">';
                                    check += '<li class="btn-xs"><a href="'+oData.refer+'" id="check">Check</a></li>';
                                    check += '<li class="btn-xs"><a href="'+oData.refer+'" id="approve">Approve</a></li>';
                                    //action += '<li role="separator" class="divider"></li>';
                                    //action += '<li class="btn-xs"><a href="#"><i class="fa fa-paper-plane" aria-hidden="true"></i> Post</a></li>';
                                    check += '</ul>';
                                    check += '</div>';
                                
                                var c = $(icon('edit',oData.saksiid,oData.pengurusid,''));
				var e = $(icon('delete',oData.saksiid,oData.pengurusid,''));
				var f = $(icon('post',oData.saksiid,oData.pengurusid,''));
                                var a = $(action);
                                var b = $('<button type="button" class="btn btn-default btn-xs"><i class="fa fa-folder-open"></i></button>');
                                var k = $(check);
                                var g = $(icon('cancel',oData.saksiid,oData.pengurusid,''));
				
				
				b.on('click', function() {
				  //console.log(oData);
				  $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=view&refer="+oData.refer,
                                            success: function (result) {
                                            // $("#haha").html(result);
                                            $('#herey').empty().html(result).hide().fadeIn(300);
                                        }});
                                        return false;
				});
				c.on('click', function() {
				  //console.log(oData);
				  if(iconClick('edit',oData.saksiid,oData.pengurusid,'')==0){
				  	//editRowx(oData.JVrefno,updateTable,subTable,<%= mod.getModuleID() %>,module_abb,oTable);
                                        
                                        $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=editpv&refer="+oData.refer,
                                            success: function (result) {
                                            // $("#haha").html(result);
                                            $('#herey').empty().html(result).hide().fadeIn(300);
                                        }});
                                        return false;
				  }
				  
				  //$("#dialog-form-update").dialog("open");
				  //$( "#dialog-form-update" ).dialog( "open" );
				  return false;
				});
				e.on('click', function() {
				  //console.log(oData);
				  if(iconClick('delete',oData.saksiid,oData.pengurusid,'')==0){
				  	//deleteRow(oData.JVrefno);
                                        
                                        BootstrapDialog.confirm({
                                            title: 'Confirmation',
                                            message: 'Are you sure to delete?',
                                            type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                            closable: true, // <-- Default value is false
                                            draggable: true, // <-- Default value is false
                                            btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                            btnOKLabel: 'Delete', // <-- Default value is 'OK',
                                            btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                                            callback: function(result) {
                                                // result will be true if button was click, while it will be false if users close the dialog directly.
                                                if(result) {
                                                    $.ajax({
                                                        url: "PathController?moduleid=<%= mod.getModuleID() %>&process=delete&refer="+oData.refer,
                                                        success: function (result) {
                                                        // $("#haha").html(result);
                                                        setTimeout(function(){
                                                        oTable.fnClearTable();}, 500);
                                                    }});
                                                }
                                            }
                                        });
                                        
                                        
                                        return false;
				  }
				  
				  return false;
				});
				f.on('click', function() {
				  //console.log(oData);
				  if(iconClick('post',oData.saksiid,oData.pengurusid,'')==0){
				  	//postThis(oData.JVrefno);
                                        $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=post&referno="+oData.invref,
                                            success: function (result) {
                                            // $("#haha").html(result);
                                            $('#herey').empty().html(result).hide().fadeIn(300);
                                        }});
				  }
				 
				  return false;
				});
				g.on('click', function() {
				  //console.log(oData);
				  if(iconClick('cancel',oData.saksiid,oData.pengurusid,'')==0){
				  	cancel(oData.JVrefno);
				  }
				  
				  return false;
				});
					  
				                
                                $(nTd).empty();
				$(nTd).attr("class",'btn-group');
                                $(nTd).attr("align",'right');
                                $(nTd).prepend(a,b,c,k,e,g);
                                
                                
			  }
			 
		  },

		  {
			"aTargets": [4],//special rules for determine a status
			  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
				var stat = '<span class="label label-primary">Preparing</span>';

				if(oData.saksiid.length > 0){
				  stat = '<span class="label label-default">Checked</span>';
				}
				if(oData.pengurusid.length > 0){
				  stat = '<span class="label label-info">Approved</span>';
				}

				$(nTd).empty();
				//$(nTd).attr("id",'btntest');
					$(nTd).prepend(stat);
			  }
			 
		  } ]

		  
		  
		} );
                
                $('#example tbody').on( 'click', '#addaccount', function (e) {
                    e.preventDefault();
                    var refer = $(this).attr('href');
                    $.ajax({
                        url: "PathController?moduleid=<%= mod.getModuleID() %>&process=addnewitem&refer="+refer,
                        success: function (result) {
                            $('#herey').empty().html(result).hide().fadeIn(300);
                        }
                    });
                    
                    return false;
                });
                
                $('#example tbody').on( 'click', '#addreference', function (e) {
                    e.preventDefault();
                    var refer = $(this).attr('href');
                    $.ajax({
                        url: "PathController?moduleid=<%= mod.getModuleID() %>&process=addreference&refer="+refer,
                        success: function (result) {
                            $('#herey').empty().html(result).hide().fadeIn(300);
                        }
                    });
                    
                    return false;
                });
                
                $('#example tbody').on( 'click', '#check', function (e) {
                    e.preventDefault();
                    var refer = $(this).attr('href');
                    BootstrapDialog.confirm({
                                            title: 'Confirmation',
                                            message: 'Are you sure to check this agreement ('+refer+')?',
                                            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                            closable: true, // <-- Default value is false
                                            draggable: true, // <-- Default value is false
                                            btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                            btnOKLabel: 'Check This', // <-- Default value is 'OK',
                                            btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
                                            callback: function(result) {
                                                // result will be true if button was click, while it will be false if users close the dialog directly.
                                                if(result) {
                                                    $.ajax({
                                                        url: "PathController?moduleid=<%= mod.getModuleID() %>&process=delete&refer="+refer,
                                                        success: function (result) {
                                                        // $("#haha").html(result);
                                                        setTimeout(function(){
                                                        oTable.fnClearTable();}, 500);
                                                    }});
                                                }
                                            }
                                        });
                    
                    return false;
                });
                
                $('#example tbody').on( 'click', '#approve', function (e) {
                    e.preventDefault();
                    var refer = $(this).attr('href');
                    BootstrapDialog.confirm({
                                            title: 'Confirmation',
                                            message: 'Are you sure to approve this agreement ('+refer+')?',
                                            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                            closable: true, // <-- Default value is false
                                            draggable: true, // <-- Default value is false
                                            btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                            btnOKLabel: 'Approve This', // <-- Default value is 'OK',
                                            btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
                                            callback: function(result) {
                                                // result will be true if button was click, while it will be false if users close the dialog directly.
                                                if(result) {
                                                    $.ajax({
                                                        url: "PathController?moduleid=<%= mod.getModuleID() %>&process=delete&refer="+refer,
                                                        success: function (result) {
                                                        // $("#haha").html(result);
                                                        setTimeout(function(){
                                                        oTable.fnClearTable();}, 500);
                                                    }});
                                                }
                                            }
                                        });
                    
                    return false;
                });


} );

</script>

<body>
    <div id ="maincontainer">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="74%" class="borderbot" align="left"><span class="bigfonttitle"><%= mod.getModuleDesc() %></span>&nbsp;&nbsp;<span class="midfonttitle"> List of <%= mod.getModuleDesc() %></span></td>
                <td width="26%" class="borderbot" align="right">&nbsp;</td>
            </tr>
        </table>
        <br>
        <div class="partition"> 
            <div class="headofpartition"></div>
            <div class="bodyofpartition">
                <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                    <tr>
                        <td valign="middle" align="left">
                            <button id="add-new" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> Add <%= mod.getModuleDesc() %></button>
                        </td>
                    </tr>
                </table>
                        <hr>       
                <div class="dataTable_wrapper"> 
                    <table class="table table-striped table-bordered table-hover" id="example">
                    </table>
                </div>
                       
            </div>
        </div>
    </div>
</body>
</html>
  
