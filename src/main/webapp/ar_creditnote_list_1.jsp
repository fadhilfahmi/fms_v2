<%-- 
    Document   : ar_debitnote_list
    Created on : Nov 7, 2016, 9:19:54 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.financial.ar.ArCreditNoteDAO"%>
<%@page import="com.lcsb.fms.util.model.ListTable"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.ModuleItem"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.ConnectionModel"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,java.util.*" errorPage="" isThreadSafe="false" %>
<%@ page import="java.util.Date"%>
<%

    Module mod = (Module) ArCreditNoteDAO.getModule();

    response.setHeader("Cache-Control", "no-cache");

%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/tether.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>

<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        var updateTable = '<%= mod.getMainTable()%>';

    <%
        List<ListTable> mlist = (List<ListTable>) ArCreditNoteDAO.tableList();
        for (ListTable m : mlist) {
                        //if(m.getList_View().equals        ("1")){
%>
        columntoView.push('<%= m.getColumnName()%>');
        columnName.push('<%= m.getTitleName()%>');
    <%
            //}

        }
    %>

        var colsTosend = 'cols=';
        for (m = 0; m < columntoView.length; ++m) {//put value from database to variable jget
            if (m == columntoView.length - 1) {
                colsTosend += columntoView[m];
            } else {
                colsTosend += columntoView[m] + '&cols=';
            }
        }


        $("#add-new").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addnew",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        $("#check-button").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=check",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });



        var orderby = 'order by voucherno desc';
        var oTable = $('#example').DataTable({
            destroy: true,
            "aaSorting": [[0, "desc"]],
            "responsive": true,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "retrievetable.jsp?table=" + updateTable + "&code=<%= mod.getReferID_Master()%>&" + colsTosend + "&order=" + orderby,
            "aoColumns": [
                {"sTitle": columnName[0], "mData": columntoView[0], "sWidth": "9%", "bSearchable": true},
                {"sTitle": columnName[1], "mData": columntoView[1], "sWidth": "9%"},
                {"sTitle": columnName[2], "mData": columntoView[2], "sWidth": "25%"},
                {"sTitle": columnName[3], "mData": columntoView[3], "sWidth": "15%"},
                //{"sTitle": columnName[4], "mData": columntoView[4], "sWidth": "15%"},
                {"sTitle": columnName[4], "mData": columntoView[4], "sWidth": "15%", "bVisible": false},
                {"sTitle": columnName[5], "mData": columntoView[5], "sWidth": "15%", "bVisible": false},
                {"sTitle": columnName[6], "mData": columntoView[6], "sWidth": "15%", "bVisible": false},
                {"sTitle": "Status", "mData": null, "sWidth": "15%"},
                {"sTitle": "Action", "mData": null, "bSortable": false, "bSearchable": false, "sWidth": "13%"}

            ],
            "aoColumnDefs": [{
                    "aTargets": [8],
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {

                        var action = '<div class="btn-group">';
                        //action += '<button type="button" class="btn btn-default btn-xs">Action</button>';
                        action += icon('multi', oData.checkid, oData.appid, oData.post);
                        action += '<span><i class="fa fa-plus-circle"></i></span>';
                        action += '<span class="sr-only">Toggle Dropdown</span>';
                        action += '</button>';
                        action += '<ul class="dropdown-menu btn-xs">';
                        action += '<li class="btn-xs"><a href="' + oData.refer + '" id="additem">Detail</a></li>';
                        //action += '<li role="separator" class="divider"></li>';
                        //action += '<li class="btn-xs"><a href="#"><i class="fa fa-paper-plane" aria-hidden="true"></i> Post</a></li>';
                        action += '</ul>';
                        action += '</div>';

                        var c = $(icon('edit', oData.checkid, oData.appid, oData.post));
                        var e = $(icon('delete', oData.checkid, oData.appid, oData.post));
                        var a = $(action);
                        var b = $('<button type="button" class="btn btn-default btn-xs"><i class="fa fa-folder-open"></i></button>');


                        b.on('click', function () {
                            //console.log(oData);
                            $.ajax({
                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewpv&refer=" + oData.refer,
                                success: function (result) {
                                    // $("#haha").html(result);
                                    $('#herey').empty().html(result).hide().fadeIn(300);
                                }});
                            return false;
                        });
                        c.on('click', function () {
                            //console.log(oData);
                            if (iconClick('edit', oData.checkid, oData.appid, oData.post) == 0) {
                                //editRowx(oData.JVrefno,updateTable,subTable,<%= mod.getModuleID()%>,module_abb,oTable);

                                $.ajax({
                                    url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editlist&referno=" + oData.refer,
                                    success: function (result) {
                                        // $("#haha").html(result);
                                        $('#herey').empty().html(result).hide().fadeIn(300);
                                    }});
                                return false;
                            }

                            //$("#dialog-form-update").dialog("open");
                            //$( "#dialog-form-update" ).dialog( "open" );
                            return false;
                        });
                        e.on('click', function () {
                            //console.log(oData);
                            if (iconClick('delete', oData.checkid, oData.appid, oData.post) == 0) {
                                //deleteRow(oData.JVrefno);

                                BootstrapDialog.confirm({
                                    title: 'Confirmation',
                                    message: 'Are you sure to delete?',
                                    type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                    closable: true, // <-- Default value is false
                                    draggable: true, // <-- Default value is false
                                    btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                    btnOKLabel: 'Delete', // <-- Default value is 'OK',
                                    btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                                    callback: function (result) {
                                        // result will be true if button was click, while it will be false if users close the dialog directly.
                                        if (result) {
                                            $.ajax({
                                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&referno=" + oData.refer,
                                                success: function (result) {
                                                    // $("#haha").html(result);
                                                    setTimeout(function () {
                                                        oTable.draw();
                                                    }, 500);
                                                }});
                                        }
                                    }
                                });


                                return false;
                            }

                            return false;
                        });

                        $(nTd).empty();
                        $(nTd).attr("class", 'btn-group');
                        $(nTd).attr("align", 'right');
                        $(nTd).prepend(a, b, c, e);


                    }

                },
                {
                    "aTargets": [7], //special rules for determine a status
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        var stat = '<span class="label label-primary">Preparing</span>';

                        if (oData.checkid.length > 0) {
                            stat = '<span class="label label-default">Checked</span>';
                        }
                        if (oData.appid.length > 0) {
                            stat = '<span class="label label-success">Approved</span>';
                        }
                        if (oData.postflag == 'Cancel') {
                            stat = 'Cancelled';
                        }
                        
                        var dc = '';
                        $.ajax({
                            async: false,
                            url: "ProcessController?moduleid=000000&process=checkDCnote&referno=" + oData.noteno,
                            success: function (result) {
                                // $("#haha").html(result);
                                dc = result;

                            }
                        });

                        $(nTd).empty();
                        //$(nTd).attr("id",'btntest');
                        $(nTd).prepend(stat, dc);
                    }

                },
                {
                    "aTargets": [3], //special rules for determine a status
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        var s = parseFloat(oData.total);
                        var amt = formatCurrency(s);

                        $(nTd).empty();
                        $(nTd).attr("class", 'align-right');
                        $(nTd).prepend(amt);
                    }

                }]



        });

        $('#example tbody').on('click', 'tr', function () {
            var data = oTable.row(this).data();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editlist&referno=" + data.noteno,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });





        $('#example tbody').on('click', '#additem', function (e) {
            e.preventDefault();
            var refer = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addnewitem&refer=" + noteno,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $('#example tbody').on('click', '#addrefer', function (e) {
            e.preventDefault();
            var refer = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addrefer&refer=" + noteno,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });


    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="add-new" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> Add <%= mod.getModuleDesc()%></button>

                    </td>
                </tr>
            </table>
            <br>       
            <div class="dataTable_wrapper"> 
                <table class="table table-striped table-bordered table-hover" id="example">
                </table>
            </div>

        </div>
    </div>
</div>

