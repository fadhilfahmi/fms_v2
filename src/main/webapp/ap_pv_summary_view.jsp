<%-- 
    Document   : ap_pv_summary_view
    Created on : Sep 11, 2017, 9:33:16 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.ui.UIConfig"%>
<%@page import="com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO"%>
<%@page import="com.lcsb.fms.dao.financial.gl.PostDAO"%>
<%@page import="com.lcsb.fms.general.EnglishNumberToWords"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.PaymentVoucherDAO"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucherRefer"%>
<%@page import="com.lcsb.fms.dao.financial.ap.VendorPaymentDAO"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucher"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoice"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoiceItem"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    PaymentVoucherItem vi = (PaymentVoucherItem) VendorPaymentDAO.getPYVitem(log, request.getParameter("refer"));
    PaymentVoucher v = (PaymentVoucher) VendorPaymentDAO.getPYV(log, request.getParameter("refer"));
    Module mod = (Module) ModuleDAO.getModule(log, request.getParameter("moduleid"));

    String status = PaymentVoucherDAO.getStatus(log, v.getRefer());
    String topath = request.getParameter("topath");
    String moduleid = mod.getModuleID();
    String token = request.getParameter("status");

    if (topath == null) {
        topath = "viewlist";
    } else {
        //moduleid = request.getParameter("frommodule");
    }

    String classButtonColor = "";
    String classButtonDisable = "";

    if (status.equals("Approved") && token.equals("Checked")) {
        classButtonColor = "btn-warning";
        classButtonDisable = "disabled";
    } else if (status.equals("Checked") && token.equals("Prepared")) {
        classButtonColor = "btn-warning";
        classButtonDisable = "disabled";
    } else if (token.equals("Checked")) {
        classButtonColor = "btn-success";
        classButtonDisable = "";
    } else if (token.equals("Prepared")) {
        classButtonColor = "btn-success";
        classButtonDisable = "";
    }
%>
<style>

    a.thelink {
        cursor: pointer;
        color: #ffffff;
        text-decoration: underline;
    }
    a.thelink:link {
        color: #ffffff;
    }

    /* visited link */
    a.thelink:visited {
        color: #ffffff;
    }

    /* mouse over link */
    a.thelink:hover {
        color: #097a62;
    }

    /* selected link */
    a.thelink:active {
        color: #ffffff;
    }
</style>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        window.scrollTo(0, 0);

        callPnotify('<%= request.getParameter("status")%>', '<%= request.getParameter("text")%>');


        $('#print').click(function (e) {
            printElement($("#viewjv").html());
        });
        $('.printcheque').click(function (e) {

            var paidname = $('#paidname').val();
            var datecheque = $('#datecheque').val();
            var amountcheque = $('#amountcheque').val();
            var amountrm = $('#amountrm').val();


            //var href = '/fms_v1/pdfjs/web/viewer.html';
            /*$.ajax({
             url: 'cheque.jsp?paidname=' + paidname + '&datecheque=' + datecheque + '&amountcheque=' + amountcheque + '&amountrm=' + amountrm,
             success: function (result) {
             //$('body').empty().html(result).hide().fadeIn(300);
             $('#herey').empty().html(result).hide().fadeIn(300);
             
             }
             });*/
            window.open('cheque.jsp?paidname=' + paidname + '&datecheque=' + datecheque + '&amountcheque=' + amountcheque + '&amountrm=' + amountrm, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
            window.close()
            return false;
        });



        $("#addcredit").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addcredit&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $(".addrefer").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addrefer&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".adddebit").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=adddebit&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });









    });
</script>
<table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
    <tr>
        <td valign="middle" align="left">
            <div class="btn-group" role="group" aria-label="...">
                <button id="backtox" class="btn btn-default btn-xs" title="<%= mod.getModuleID()%>"  name="<%= topath%>" type="<%= token%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                <button id="<%= v.getRefer()%>" class="btn btn-default btn-xs viewprinted" title="<%= mod.getModuleID()%>"  name="<%= topath%>" type="<%= token%>"><i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;&nbsp;View Printed</button>
                <%
                    if (token.equals("Prepared")) {
                %>
                <button id="<%= v.getRefer()%>" class="btn <%= classButtonColor%> btn-xs checknow <%= classButtonDisable%>" title="<%= mod.getModuleID()%>"  name="<%= topath%>" type="<%= token%>"><i class="fa fa-check-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;Check</button>
                <%
                } else if (token.equals("Checked")) {
                %>
                <button id="<%= v.getRefer()%>" class="btn <%= classButtonColor%> btn-xs approvenow <%= classButtonDisable%>" title="<%= mod.getModuleID()%>"  name="<%= topath%>" type="<%= token%>"><i class="fa fa-check-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;Approve</button>
                <%
                    }
                %>
            </div>

        </td>
    </tr>
</table>
<br>
<table class="table table-bordered table-striped  table-hover">
    <thead>
        <tr>
            <th colspan="4">Voucher Information
                <!--<small><span class="label label-primary">Preparing</span></small>-->

                
            </th>
        </tr>
    </thead>
    <tbody class="activerowm tdrow">

        <tr>
            <th width="25%">Status</th>
            <td width="25%"><%= GeneralTerm.getStatusLabel(PaymentVoucherDAO.getStatus(log, v.getRefer()))%>

            </td>
            <th width="25%">Paid Name</th>
            <td width="25%">(<%= v.getPaidcode()%>) <%= v.getPaidname()%> - <%= v.getPaidtype()%></td>
        </tr>
        <tr>
            <th>Period</th>
            <td><%= v.getYear()%>, <%= v.getPeriod()%></td>
            <th>Bank</th>
            <td><%= v.getBankcode()%> - <%= v.getBankname()%></td>
        </tr>
        <tr>
            <th>Voucher No</th>
            <td><%= v.getRefer()%></td>
            <th>Amount</th>
            <td>RM<%= GeneralTerm.currencyFormat(v.getAmount())%></td>
        </tr>
        <tr>
            <th>Voucher Date</th>
            <td><%= v.getTarikh()%></td>
            <th>Remark</th>
            <td><%= v.getRemarks()%></td>
        </tr>
        <tr class="togglerow" style="display: none">
            <th>Paid Address</th>
            <td><%= v.getPaidaddress()%></td>
            <th>Paid Postcode</th>
            <td><%= v.getPaidpostcode()%></td>
        </tr>
        <tr class="togglerow" style="display: none">
            <th>Paid City</th>
            <td><%= v.getPaidcity()%></td>
            <th>Paid State</th>
            <td><%= v.getPaidstate()%></td>
        </tr>
        <tr class="togglerow" style="display: none">
            <th>Rounding</th>
            <td><%= v.getRacoacode()%> - <%= v.getRacoadesc()%></td>
            <th>Paid Type</th>
            <td><%= v.getPaidtype()%></td>
        </tr>
        <tr class="togglerow" style="display: none">
            <th>Bank</th>
            <td><%= v.getBankcode()%> - <%= v.getBankname()%></td>
            <th>Cheque No</th>
            <td><%= v.getCekno()%></td>
        </tr>
    </tbody>
</table>
<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th colspan="5">Reference Information<!--<small>asdada</small>-->
                <span class="pull-right">
                    <%if (!status.equals("Approved")) {%>
                    <button  class="btn btn-default btn-xs addrefer" title="<%= mod.getModuleID()%>" id="<%= v.getRefer()%>" name="addrefer"><i class="fa fa-plus-circle" aria-hidden="true"></i> Reference</button>
                    <%}%>
                </span>
            </th>
        </tr>
        <tr>
            <th>Type</th>
            <th>Date</th>
            <th>Remark</th>
            <th>Amount</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <%List<PaymentVoucherRefer> listRefer = (List<PaymentVoucherRefer>) VendorPaymentDAO.getAllRefer(log, v.getRefer());
                        if (listRefer.isEmpty()) {%>
        <tr>
            <td colspan="5">
                <span class="font-small-red">No data available.</span>
            </td>
        </tr>
        <%}
                        for (PaymentVoucherRefer c : listRefer) {%>
        <tr class="activerowx" id="<%= c.getRefer()%>">
            <td class="tdrow">&nbsp;<i class="fa fa-paperclip" aria-hidden="true"></i>&nbsp;&nbsp;<%= c.getType()%> </td>
            <td class="tdrow">&nbsp;<%= c.getTarikh()%></td>
            <td class="tdrow">&nbsp;<%= c.getRemarks()%></td>
            <td class="tdrow" align="right">&nbsp;<%= GeneralTerm.currencyFormat(c.getAmount())%></td>
            <td class="tdrow" align="right">&nbsp;
                <%if (!status.equals("Approved")) {%>
                <div class="btn-group btn-group-xs" role="group" aria-label="...">
                    <button id="<%= c.getRefer()%>" class="btn btn-default btn-xs edititem" title="<%= mod.getModuleID()%>" name="edit_nd"><i class="fa fa-pencil"></i></button>
                    <button id="<%= c.getRefer()%>"  class="btn btn-default btn-xs deleteitem" title="<%= mod.getModuleID()%>" name="delete_nd"><i class="fa fa-trash-o"></i></button>
                </div>
                <%}%>
            </td>
        </tr>
        <tr id="togglerow_nd<%= c.getRefer()%>" style="display: none">
            <td colspan="5">
                <table class="table table-bordered">
                    <thead></thead>
                    <tbody>
                        <tr>
                            <th>To Date Paid</th>
                            <td><%= c.getPaid()%></td>
                            <th>B/F Amount</th>
                            <td><%= c.getBf()%></td>
                        </tr>
                        <tr>
                            <th>To Paid</th>
                            <td><%= c.getPayment()%></td>
                            <th>C/F Amount</th>
                            <td><%= c.getCf()%></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <%}%>
    </tbody>
</table>

<table class="table table-bordered table-striped table-hover" id="dataTables">
    <thead>
        <tr>
            <th colspan="5">Detail<!--<small>asdada</small>-->
                <span class="pull-right">
                    <%if (!status.equals("Approved")) {%>
                    <button  class="btn btn-default btn-xs adddebit" title="<%= mod.getModuleID()%>" id="<%= v.getRefer()%>" name="adddebit"><i class="fa fa-plus-circle" aria-hidden="true"></i> Debit Account</button>
                    <%}%>
                </span>
            </th>
        </tr>
        <tr>
            <th>Reference No</th>
            <th>Account</th>
            <th>Sub Account</th>
            <th>Amount</th>
            <th>Action</th>
        </tr>
    </thead>
    <%List<PaymentVoucherItem> listAll = (List<PaymentVoucherItem>) VendorPaymentDAO.getAllPYVItem(log, v.getRefer());
                    if (listAll.isEmpty()) {%>
    <tbody>
        <tr>
            <td colspan="5">
                <span class="font-small-red">No data available.</span>
            </td>
        </tr>
        <%}
                        for (PaymentVoucherItem j : listAll) {%>
        <tr class="activerowy" id="<%= j.getVoucer()%>">
            <td class="tdrow">&nbsp;<%= j.getVoucer()%></td>
            <td class="tdrow">&nbsp;<%= j.getCoacode()%> - <%= j.getCoadescp()%></td>
            <td class="tdrow">&nbsp;<%= j.getSatype()%> / <%= j.getSacode()%> / <%= j.getSadesc()%> </td>
            <td class="tdrow" align="right">&nbsp;<%= GeneralTerm.currencyFormat(j.getAmount())%></td>
            <td class="tdrow" align="right">&nbsp;
                <%if (!status.equals("Approved")) {%>
                <div class="btn-group btn-group-xs" role="group" aria-label="...">
                    <button id="<%= j.getVoucer()%>" class="btn btn-default btn-xs edititem" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-pencil"></i></button>
                    <button id="<%= j.getVoucer()%>" class="btn btn-default btn-xs deleteitem" title="<%= mod.getModuleID()%>"  name="delete_st"><i class="fa fa-trash-o"></i></button>
                </div>
                <%}%>
            </td>
        </tr>
        <tr id="togglerow_st<%= j.getVoucer()%>" style="display: none">
            <td colspan="5">
                <table class="table table-bordered">
                    <thead></thead>
                    <tbody>
                        <tr>
                            <th width="15%">Location</th>
                            <td  width="35%"><%= j.getLoclevel()%> / <%= j.getLoccode()%> / <%= j.getLocname()%></td>
                            <th width="15%">Taxation</th>
                            <td width="35%"><%= j.getTaxcode()%> - <%= j.getTaxdescp()%> / <%= j.getSadesc()%></td>
                        </tr>
                        <tr>
                            <th>Tax Amount</th>
                            <td><%= j.getTaxamt()%></td>
                            <th>Tax Rate</th>
                            <td><%= j.getTaxrate()%></td>
                        </tr>
                        <tr>
                            <th>Remarks</th>
                            <td colspan="3"><%= j.getRemarks()%></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
    <%}%>
</table>








