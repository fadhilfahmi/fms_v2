<%-- 
    Document   : cf_contractor_edit
    Created on : Apr 21, 2016, 12:57:13 PM
    Author     : qayyum
--%>
<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Contractor"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ContractorDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) ContractorDAO.getModule();
    Contractor edit = (Contractor) ContractorDAO.getInfo(log,request.getParameter("referenceno"));

%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        

        $('#getbank').click(function (e) {

            var a = 'bankcode';
            var b = 'bankname';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Taxation',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_bank.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });
    });

</script>
</head>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="editprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>



            <form data-toggle="validator" role="form" id="saveform">
                <input type="hidden" name="prepareid" id="pid" value="<%= log.getUserID()%>">
                <input type="hidden" name="preparename" id="pname" value="<%= log.getFullname()%>">
                <input type="hidden" name="preparedate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Code &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="code" name="code" value="<%=edit.getCode()%>" autocomplete="off" required readonly>  
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Company Name &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="companyname" name="companyname"  autocomplete="off" value="<%=edit.getCompanyname()%>">  
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Vendor &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="vendor" name="vendor" autocomplete="off" value="<%=edit.getVendor()%>">   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Register &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="register" name="register"  autocomplete="off" value="<%=edit.getRegister()%>">  
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Bumiputra &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="bumiputra" name="bumiputra" value="<%=edit.getBumiputra()%>">
                                    <%= ParameterDAO.parameterList(log,"YesNo Type", "") %>
                                </select> 
                            </div>
                        </div>

                    </div>
                </div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Owner Name &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="ownername" name="ownername" autocomplete="off" value="<%=edit.getOwnername()%>">   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Owner NRIC No. &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="owneric" name="owneric" autocomplete="off" value="<%=edit.getOwneric()%>">   
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Nationality &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="nationality" name="nationality" autocomplete="off" value="<%=edit.getNationality()%>">   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Class &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="kelas" name="kelas" autocomplete="off" value="<%=edit.getKelas()%>">   
                            </div>
                        </div></div>
                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Account Code&nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="coa" name="coa" value="<%=edit.getCoa()%>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="coa" id1="coadescp"><i class="fa fa-cog"></i> Account</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Account Description&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="coadescp" name="coadescp" placeholder="" autocomplete="off" value="<%=edit.getCoadescp()%>"> 
                            </div>
                        </div>
                    </div></div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Title &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="contacttitle" name="contacttitle" value="<%=edit.getContacttitle()%>">
                                    <%= ParameterDAO.parameterList(log,"Title", "")%>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Contact Name &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="contactperson" name="contactperson" autocomplete="off" value="<%=edit.getContactperson()%>">   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Position &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="contactposition" name="contactposition" value="<%=edit.getContactposition()%>">
                                    <%= ParameterDAO.parameterList(log,"Position", "")%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Handphone No. &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="hp" name="hp" placeholder="" autocomplete="off" value="<%=edit.getHp()%>"> 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Phone No. &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="phone" name="phone" placeholder="" autocomplete="off" value="<%=edit.getPhone()%>"> 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Fax No. &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="fax" name="fax" placeholder="" autocomplete="off" value="<%=edit.getFax()%>"> 
                            </div></div></div><div class="row">
                        <div class="col-sm-6">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Email &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="email" name="email" placeholder="" autocomplete="off" value="<%=edit.getEmail()%>"> 
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">URL Address &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="url" name="url" placeholder="" autocomplete="off" value="<%=edit.getUrl()%>"> 
                            </div>
                        </div></div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Address &nbsp&nbsp<span class="res_code"></span></label>
                                <textarea class="form-control" rows="4"  id="address" name="address"><%=edit.getAddress()%></textarea>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">City &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="city" name="city" placeholder="" autocomplete="off" value="<%=edit.getCity()%>"> 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">State &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="state" name="state" placeholder="" autocomplete="off" value="<%=edit.getState()%>"> 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Postcode &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="postcode" name="postcode" placeholder="" autocomplete="off" value="<%=edit.getPostcode()%>"> 
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Country &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="country" name="country" placeholder="" autocomplete="off" value="<%=edit.getCountry()%>"> 
                            </div>
                        </div>
                    </div>
                </div>



                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Bank&nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="bankcode" name="bank" placeholder="" autocomplete="off" value="<%=edit.getBank()%>">
                                    <span class="input-group-btn">


                                        &nbsp;<button class="btn btn-default btn-sm" id="getbank" title=""><i class="fa fa-cog"></i>Get Bank</button></span>
                                </div>  
                            </div>
                        </div><div class="col-sm-9">
                            <label for="inputName" class="control-label">Bank Description &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                            <input type="text" class="form-control input-sm" id="bankname" name="bankdesc" placeholder="" autocomplete="off" value="<%=edit.getBankdesc()%>"> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Bank Account&nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="bankaccount" name="bankaccount" placeholder="" autocomplete="off" value="<%=edit.getBankaccount()%>"> 
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Payment Method &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="payment" name="payment" value="<%=edit.getPayment()%>">
                                    <%= ParameterDAO.parameterList(log,"Payment Method", "")%>
                                </select> 
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Remarks &nbsp&nbsp<span class="res_code"></span></label>
                                <textarea class="form-control" rows="3"  id="remarks" name="remarks"><%=edit.getRemarks()%></textarea>
                            </div>
                        </div></div></div>

                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Deposit Code&nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="depositcoa" name="depositcoa" value="<%=edit.getDepositcoa()%>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="depositcoa" id1="depositcoadescp"><i class="fa fa-cog"></i> Deposit</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Deposit Description&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="depositcoadescp" name="depositcoadescp" placeholder="" autocomplete="off" value="<%=edit.getDepositcoadescp()%>"> 
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Retention &nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="actcode" name="retention" value="<%=edit.getRetention()%>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="retention" id1="retentiondescp"><i class="fa fa-cog"></i> Retention</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label"> Retention Description &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" value="<%=edit.getRetentiondescp()%>" class="form-control input-sm" id="retentiondescp" name="retentiondescp" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                    </div><div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Permit Code &nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" value="<%=edit.getPermitcode()%>" class="form-control input-sm" id="permitcode" name="permitcode">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="permitcode" id1="permitdesc"><i class="fa fa-cog"></i>Permit Code</button>
                                    </span>
                                </div>  
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label"> Permit Description &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="permitdesc" name="permitdesc" placeholder="" autocomplete="off" value="<%=edit.getPermitdesc()%>"> 
                            </div>
                        </div>
                    </div>
            </form>
        </div>

    </div>
</div>
</div>
