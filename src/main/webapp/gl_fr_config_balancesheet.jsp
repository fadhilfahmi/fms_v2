<%-- 
    Document   : gl_fr_config_balancesheet
    Created on : Mar 2, 2017, 10:38:13 AM
    Author     : fadhilfahmi
--%>
<%@page import="com.lcsb.fms.dao.financial.gl.FinancialReportDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ChartofAccountDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage=""%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) FinancialReportDAO.getModule();
%>

<style>
    li {list-style-type: none;}
</style>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        
        $('#layer1').on('click', '.addlayer', function () {
            var layerid = $(this).attr('id');
            console.log('addlayer'+layerid);
            var col1 = '';
            var col2 = '';

            if (layerid == 1) {
                col1 = '0';
                col2 = '7';
            } else if (layerid == 2) {
                col1 = '1';
                col2 = '7';
            }

            var layer = '<li>' +
                    '<div class="form-group col-lg-' + col1 + '" id="div_code">&nbsp;' +
                    '</div>' +
                    '<div class="form-group col-lg-' + col2 + '" id="div_code">' +
                    '<div class="pull-left">' +
                    '<button type="button" class="btn btn-default btn-sm addlayer" id="' + layerid + '"><i class="fa fa-plus" aria-hidden="true"></i></button>&nbsp;' +
                    '</div>' +
                    '<div class="pull-right">' +
                    '&nbsp;<button type="button" class="btn btn-default btn-sm"><i class="fa fa-folder-open-o" aria-hidden="true"></i></button>' +
                    '</div>' +
                    '<div>' +
                    '<div class="input-group">' +
                    '<input type="text" class="form-control input-sm" value="1" />' +
                    '<span class="input-group-btn">' +
                    '<button class="btn btn-default btn-sm" type="button"><i class="fa fa-cog" aria-hidden="true"></i></button>' +
                    '</span>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</li>';
            $('#layer'+layerid).append(layer);
        });
        
        $('#layer1').on('click', '.addsublayer', function () {
            
            var layerid2 = $(this).attr('id');
            console.log('sublayer-'+layerid2);
            layerid2 = parseInt(layerid2);

            var layer2 = '<li>' +
                    '<div class="form-group col-lg-1" id="div_code">&nbsp;' +
                    '</div>' +
                    '<div class="form-group col-lg-7" id="div_code">' +
                    '<div class="pull-left">' +
                    '<button type="button" class="btn btn-default btn-sm addlayer" id="' + layerid2 + '"><i class="fa fa-plus" aria-hidden="true"></i></button>&nbsp;' +
                    '</div>' +
                    '<div class="pull-right">' +
                    '&nbsp;<button type="button" class="btn btn-default btn-sm addsublayer id='+ layerid2+1 +'"><i class="fa fa-folder-open-o" aria-hidden="true"></i></button>' +
                    '</div>' +
                    '<div>' +
                    '<div class="input-group">' +
                    '<input type="text" class="form-control input-sm" value="2" />' +
                    '<span class="input-group-btn">' +
                    '<button class="btn btn-default btn-sm" type="button"><i class="fa fa-cog" aria-hidden="true"></i></button>' +
                    '</span>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</li>';
            $('#layer2').append(layer2);
        });

        $(".setting-button").click(function () {

            var link = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + link,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <div class="row">
                <div class="col-lg-6">
                    <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="reportconfig"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group" id="div_code">
                        <label for="inputName" class="control-label">Report Title &nbsp&nbsp<span id="res_code"></span></label>
                        <input type="text" class="form-control input-sm" id="code" name="code" autocomplete="off" required>
                    </div>
                </div> 
            </div>

            <div class="row">
                <div class="col-sm-8">
                    <ul class="nav" id="layer1">
                        
                        <li>
                            <div class="form-group col-sm-0" id="div_code">&nbsp;
                            </div>     
                            <div class="form-group col-sm-7" id="div_code">
                                <div class="pull-left">
                                    <button type="button" class="btn btn-default btn-sm addlayer" id="1"><i class="fa fa-plus" aria-hidden="true"></i></button>&nbsp;
                                </div>
                                <div class="pull-right">
                                    &nbsp;<button type="button" class="btn btn-default btn-sm addsublayer" id="2"><i class="fa fa-folder-open-o" aria-hidden="true"></i></button>
                                </div>
                                <div>
                                    <div class="input-group">
                                        <input type="text" class="form-control input-sm" value="1" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-sm" type="button"><i class="fa fa-cog" aria-hidden="true"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <ul id="layer2">
                                

                            </ul>
                        </li>
                        <!--<li>
                            <ul class="nav">
                                <li>
                                    <div class="row">
                                        <div class="form-group col-lg-1" id="div_code">&nbsp;
                                        </div>
                                        <div class="form-group  col-lg-7" id="div_code">
                                            <div class="pull-left">
                                                <button type="button" class="btn btn-default btn-sm addlayer" id="2"><i class="fa fa-plus" aria-hidden="true"></i></button>&nbsp;
                                            </div>
                                            <div class="pull-right">
                                                &nbsp;<button type="button" class="btn btn-default btn-sm"><i class="fa fa-folder-open-o" aria-hidden="true"></i></button>
                                            </div>
                                            <div>
                                                <div class="input-group">
                                                    <input type="text" class="form-control input-sm" value="1" />
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default btn-sm" type="button"><i class="fa fa-cog" aria-hidden="true"></i></button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="nav">
                                        <li>
                                            <div class="row">
                                                <div class="form-group col-lg-2" id="div_code">&nbsp;
                                                </div>
                                                <div class="form-group col-lg-7" id="div_code">
                                                    <div class="pull-left">
                                                        <button type="button" class="btn btn-default btn-sm addlayer" id="3"><i class="fa fa-plus" aria-hidden="true"></i></button>&nbsp;
                                                    </div>
                                                    <div class="pull-right">
                                                        &nbsp;<button type="button" class="btn btn-default btn-sm"><i class="fa fa-folder-open-o" aria-hidden="true"></i></button>
                                                    </div>
                                                    <div>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-sm" value="1" />
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default btn-sm" type="button"><i class="fa fa-cog" aria-hidden="true"></i></button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>-->
                    </ul>
                </div> 
            </div>
        </div>
    </div>
</div>

