<%-- 
    Document   : testpage
    Created on : Dec 2, 2016, 10:09:31 AM
    Author     : fadhilfahmi
--%>

<%@page import="java.sql.Connection"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.logging.Level"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFCell"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFRow"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFSheet"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@page import="org.apache.poi.poifs.filesystem.POIFSFileSystem"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.BufferedInputStream"%>
<%@page import="com.lcsb.fms.util.ext.ReadExcelFile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/pvoucher.css?1" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div>
            <%
                Connection con = ConnectionUtil.getConnection();
                InputStream input = new BufferedInputStream(
                        new FileInputStream("/Users/fadhilfahmi/Google Drive/fms_v1/target/fms_v1-1.0/uploadedFiles/WriteSheet.xls"));
                POIFSFileSystem fs = new POIFSFileSystem(input);
                HSSFWorkbook wb = new HSSFWorkbook(fs);
                HSSFSheet sheet = wb.getSheetAt(0);
            %><table border="1" cellpadding="0" cellspacing="0"><%
                Iterator rows = sheet.rowIterator();

                String str = "";
                String col = "";
                int f = 0;
                while (rows.hasNext()) {
                    HSSFRow row = (HSSFRow) rows.next();
                    out.println("<tr>");
                    Iterator cells = row.cellIterator();
                    int c = 0;
                    int e = 0;
                    while (cells.hasNext()) {
                        c++;
                        //out.println(c+"<br>");
                        HSSFCell cell = (HSSFCell) cells.next();
                        //if (c < 2) {

                %><td>
                    <%                        String val = "";

                        if (HSSFCell.CELL_TYPE_NUMERIC == cell.getCellType()) {
                            val = String.valueOf(cell.getNumericCellValue());
                        } else if (HSSFCell.CELL_TYPE_STRING == cell.getCellType()) {
                            val = String.valueOf(cell.getStringCellValue());
                        } else if (HSSFCell.CELL_TYPE_BOOLEAN == cell.getCellType()) {
                            val = String.valueOf(cell.getBooleanCellValue());
                        } else if (HSSFCell.CELL_TYPE_BLANK == cell.getCellType()) {

                            if (e < 4) {
                                e++;
                                val = "aa";

                            } else {
                                val = "";
                                // e = 0;
                            }

                        } else {
                            out.print("N/A");
                        }
//out.println(val);
                        
                        

                        if (c == 4) {
                           str = str + "\"" + val + "\"";
                        } else if (c < 4) {
                            str = str + "\"" + val + "\",";
                        }

                    %></td><%  //}
                        }

                    %></tr>
                <%String query = "INSERT INTO chartofacccount_side(code, descp,prevcode,prevdesc) values (" + str + ")";
                        out.println(query+"<br>");
                        PreparedStatement ps = con.prepareStatement(query);

                        //ps.executeUpdate();
                        //ps.close();  
                        
                        str="";
                    }
                %>
            </table>
        </div>
    </body>
</html>
