<%-- 
    Document   : em_payroll_payslip_view
    Created on : Jun 11, 2017, 1:50:11 AM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.model.financial.employee.EmPayrollMaster"%>
<%@page import="com.lcsb.fms.util.dao.EstateDAO"%>
<%@page import="com.lcsb.fms.util.model.Estate"%>
<%@page import="com.lcsb.fms.model.financial.employee.EmPayrollEarning"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.financial.employee.EmPayrollDAO"%>
<%@page import="com.lcsb.fms.model.financial.employee.EmPayrollDeduction"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    response.setHeader("Cache-Control", "no-cache");

    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) EmPayrollDAO.getModule();
    Estate est = EstateDAO.getEstateInfo(log, log.getEstateCode(), "estatecode");
    String refer = request.getParameter("refer");
    String staffid = request.getParameter("staffid");

    EmPayrollMaster em = (EmPayrollMaster) EmPayrollDAO.getPayrollMaster(log, refer, staffid);
%>
<link href="css/pvoucher.css?1" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $('#print').click(function (e) {

            printElement($("#viewprint").html());

        });
        
        $('#next').click(function (e) {
            
            var r = $('#thisrefer').val();
            var c = $('#thisstaffid').val();

            $('.modal-body').empty().load('em_payroll_payslip_view.jsp?refer=' + r + '&staffid=' + c);

        });

        $('#confirm').click(function (e) {
            var cont = '<span>Confirming...</span>';
            $(this).empty().html(cont).hide().fadeIn(300);
            e.preventDefault();
            if (e.handled !== true) { //Checking for the event whether it has occurred or not.
                e.handled = true; // Basically setting value that the current event has occurred.

                $.ajax({
                    url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=confirmslip&refer=<%= refer%>&staffid=<%=staffid%>",
                    success: function (result) {

                        if (result == 1) {
                            setTimeout(function () {
                                $('#confirm').prop('disabled', true);
                                $('#print').prop('disabled', false);
                                $('#confirm-<%=staffid%>').prop('disabled', true);
                                $('#confirm-<%=staffid%>').addClass('btn-success').removeClass('btn-default');
                                $('#confirm-<%=staffid%>').html('Confirmed');
                                $('#confirm').html('<i class="fa fa-check-circle" aria-hidden="true" style="color:#1d9d73"></i>&nbsp;Confirmed');

                            }, 1000);

                        } else {

                        }
                    }
                });

            }

        });

    });
</script>
<button id="confirm"  class="btn btn-default btn-xs" title="<%//= mod.getModuleID()%>" name="addinv" <%= ((!em.isConfirmSlip()) ? "" : "disabled")%>><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;<%= ((!em.isConfirmSlip()) ? "Confirm" : "Confirmed")%></button>
<button id="print"  class="btn btn-default btn-xs" title="<%//= mod.getModuleID()%>" name="addinv" <%= ((em.isConfirmSlip()) ? "" : "disabled")%>><i class="fa fa-floppy-o"></i>&nbsp;Print</button>
<span class="pull-right">
    <button id="previous"  class="btn btn-default btn-xs" title="<%//= mod.getModuleID()%>" name="addinv" ><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Previous</button>
    <button id="next"  class="btn btn-default btn-xs" title="<%//= mod.getModuleID()%>" name="addinv" >Next&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i></button>
</span>
<hr>
<div id ="maincontainer">
    <div id="viewprint">
        <form data-toggle="validator" role="form" id="saveform">
            <input type="hidden" id="thisstaffid" value="<%= staffid %>">
            <input type="hidden" id="thisrefer" value="<%= refer%>">

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="4">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="50px"><img src="<%= est.getLogopath()%>" width="40px" style="vertical-align:middle" ></td>
                                <td><strong><%= log.getEstateDescp()%></strong><span class="pull-right"></span>
                                    <p class="font_inheader_normal"><%= est.getAddress()%></p>
                                    <p class="font_inheader_normal"><%= est.getPostcode()%>, <%= est.getCity()%>, <%= est.getState()%></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="border_bottom"></td>
                </tr>
            </table>
                                    <div class="text-center"><h1>PAY SLIP <%//= EmPayrollDAO.calculateTax(log, refer, staffid) %></h1></div>
            <div class="bold-bottom"><%= AccountingPeriod.getMonthofPeriod(em.getEmPayroll().getPeriod()).toUpperCase()%> <%= em.getEmPayroll().getYear()%></div>
            <table  class="" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr> 
                    <th class="smalltd" width="15%">STAFF ID</th>
                    <td class="smalltd" width="35%"><%= em.getGetStaff().getStaffid()%></td>
                    <th class="smalltd" width="15%">NAME</th>
                    <td class="smalltd" width="35%"><%= em.getGetStaff().getName()%></td>
                </tr>
                <tr> 
                    <th class="smalltd" width="15%">IC NO.</th>
                    <td class="smalltd" width="35%"><%= em.getGetStaff().getIc()%></td>
                    <th class="smalltd" width="15%">DEPARTMENT</th>
                    <td class="smalltd" width="35%"><%= em.getGetStaff().getDepartment().toUpperCase()%></td>
                </tr>
                <tr> 
                    <th class="smalltd" width="15%">EPF NO</th>
                    <td class="smalltd" width="35%"><%= em.getGetStaff().getDepartment()%></td>
                    <th class="smalltd" width="15%">DESIGNATION</th>
                    <td class="smalltd" width="35%"><%= em.getGetStaff().getPposition().toUpperCase()%></td>
                </tr>
                <tr> 
                    <th class="smalltd" width="15%">BANK</th>
                    <td class="smalltd" width="35%"><%= em.getGetStaff().getBankdesc()%></td>
                    <th class="smalltd" width="15%">BANK ACCOUNT NO.</th>
                    <td class="smalltd" width="35%"><%= em.getGetStaff().getAccno()%></td>
                </tr>
            </table>
            <div class="text-center bold-bottom-t"></div>
            <table  class="table-striped table-hover" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">

                <tr> 
                    <th width="33%" height="25px" class="smalltd grey-bg"><div class="text-center">EARNING (RM)</div></th>
                    <th width="33%" class="smalltd grey-bg"><div class="text-center">DEDUCTION (RM)</div></th>
                </tr>

                <tr> 
                    <td style="vertical-align:top">
                        <table  class="" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <%
                                double totalEarn = 0.0;
                                List<EmPayrollEarning> listE = (List<EmPayrollEarning>) em.getListEarn();
                                for (EmPayrollEarning j : listE) {

                                    totalEarn += j.getAmount();
                            %>
                            <tr> 
                                <td class="smalltd"><%= j.getEarndesc()%></td>
                                <td class="smalltd"><span class="pull-right"><%=  GeneralTerm.currencyFormat(j.getAmount())%></span></td>
                                    <%}%>
                            </tr>
                            <tr> 
                                <td class="smalltd"></td>
                                <td class="smalltd"></td>

                            </tr>
                        </table>
                    </td>
                    <td style="vertical-align:top">
                        <table  class="" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <%
                                double totalDeduct = 0.0;
                                List<EmPayrollDeduction> listAllx = (List<EmPayrollDeduction>) em.getListDeduct();
                                for (EmPayrollDeduction j : listAllx) {

                                    totalDeduct += j.getAmount();
                            %>
                            <tr> 
                                <td class="smalltd"><%= j.getDeductdesc()%></td>
                                <td class="smalltd"><span class="pull-right"><%=  GeneralTerm.currencyFormat(j.getAmount())%></span></td>
                                    <%}%>
                            </tr>
                            <tr> 
                                <td class="smalltd"></td>
                                <td class="smalltd"></td>

                            </tr>
                        </table>
                    </td>

                </tr>
                <tr> 
                    <td class="smalltd grey-bg"><span class="pull-left"><strong>TOTAL EARNING</strong></span><span class="pull-right"><strong><%= GeneralTerm.currencyFormat(totalEarn)%></strong></span></td>
                    <td class="smalltd grey-bg"><span class="pull-left"><strong>TOTAL DEDUCTION</strong></span><span class="pull-right"><strong><%= GeneralTerm.currencyFormat(totalDeduct)%></strong></span></td>

                </tr>
                <tr> 
                    <td class="smalltd grey-bg"><span class="pull-left"><strong>NET PAY</strong></span><span class="pull-right"><strong><%= GeneralTerm.currencyFormat(totalEarn - totalDeduct)%></strong></span></td>
                    <td class="smalltd grey-bg"><span class="pull-left"><strong>EMPLOYER EPF</strong></span><span class="pull-right"><strong><%= GeneralTerm.currencyFormat(totalDeduct)%></strong></span></td>

                </tr>
                <tr> 
                    <td class="smalltd grey-bg"><span class="pull-left"><strong></strong></span><span class="pull-right"><strong></strong></span></td>
                    <td class="smalltd grey-bg"><span class="pull-left"><strong>EMPLOYER SOCSO</strong></span><span class="pull-right"><strong><%= GeneralTerm.currencyFormat(totalDeduct)%></strong></span></td>

                </tr>
            </table>
        </form>
    </div>
</div>