<%-- 
    Document   : gl_jv_edit_list
    Created on : Mar 3, 2016, 3:42:15 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.model.financial.cashbook.CbCashVoucherReceiptPK"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.CbCashVoucherReceipt"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.ui.UIConfig"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.CashVoucher"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.CashVoucherAccount"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.CashVoucherDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucher"%>
<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucherItem"%>
<%@page import="com.lcsb.fms.dao.financial.gl.JournalVoucherDAO"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    CashVoucherAccount vi = (CashVoucherAccount) CashVoucherDAO.getCVitem(log, request.getParameter("referno"));
    CashVoucher v = (CashVoucher) CashVoucherDAO.getCV(log, request.getParameter("referno"));
    Module mod = (Module) CashVoucherDAO.getModule();

    //List<CashVoucherAccount> listAll = (List<CashVoucherAccount>) CashVoucherDAO.getAllCVItem(log, v.getRefer());
    //for (CashVoucherAccount j : listAll) {
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $(".editcvmain").click(function () {
            //var a = $("form").serialize();
            var a = $(this).attr('id');
            var b = $(this).attr('href');

            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editcvmain&referno=" + a,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;


        });

        $(".editcvitem").click(function () {
            //var a = $("form").serialize();
            var a = $(this).attr('id');
            var b = $(this).attr('href');

            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editcvitem&referno=" + a,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;


        });

        $(".deletecv").click(function () {
            //var a = $("form").serialize();
            var a = $(this).attr('id');
            var b = $(this).attr('href');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are you sure to delete?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "PathController?moduleid=020202&process=deletecv&referno=" + a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });



            return false;


        });

        $(".deletecvitem").click(function () {
            //var a = $("form").serialize();
            var a = $(this).attr('id');
            var b = $(this).attr('href');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are you sure to delete?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "PathController?moduleid=020202&process=deletecvitem&referno=" + a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });



            return false;


        });

        $(".gotodelete").button({
            icons: {
                primary: "ui-icon-trash"
            }
        })
                .click(function () {
                    //var a = $("form").serialize();
                    var a = $(this).attr('id');
                    var b = $(this).attr('href');

                    $.ajax({
                        url: "PathController?moduleid=<%//=moduleid%>&process=deletesub&referenceno=" + a + "&type=" + b,
                        success: function (result) {
                            // $("#haha").html(result);
                            $('#maincontainer').empty().html(result).hide().fadeIn(300);
                        }});
                    return false;


                });
    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
                </tr>
            </table>
            <br>
            <%= UIConfig.renderBanner(log, v.getRefer(), mod.getModuleID())%>
            <table class="table table-bordered table-striped  table-hover">
                <thead>
                    <tr>
                        <th colspan="4">Voucher Information
                            <!--<small><span class="label label-primary">Preparing</span></small>-->
                            <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
                                <%= UIConfig.renderMainButton(log, v.getRefer(), mod.getModuleID())%> 
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody class="activerowm tdrow">
                    <tr>
                        <th width="15%">Status</th>
                        <td width="35%"><%//= GeneralTerm.getStatusLabel(OfficialReceiptDAO.getStatus(log, v.getRefer()))%><%//= DebitCreditNoteDAO.checkDCNoteExist(log, v.getRefer())%></td>
                        <th width="15%">Paid Name</th>
                        <td width="35%">(<%= v.getPayto()%>) <%//= v.getPaidname()%> - <%//= v.getPaidtype()%></td>
                    </tr>
                    <tr>
                        <th>Period</th>
                        <td><%= v.getYear()%>, <%= v.getPeriod()%></td>
                        <th>Bank</th>
                        <td><%//= v.getBankcode()%> - <%//= v.getBankname()%></td>
                    </tr>
                    <tr>
                        <th>Voucher No</th>
                        <td><%= v.getRefer()%></td>
                        <th>Amount</th>
                        <td>RM<%//= GeneralTerm.currencyFormat(v.getAmount())%></td>
                    </tr>
                    <tr>
                        <th>Voucher Date</th>
                        <td><%//= AccountingPeriod.fullDateMonth(v.getDate())%></td>
                        <th>Amount (String)</th>
                        <td><%//= v.getRm()%></td>
                    </tr>

                    <tr>
                        <th>Remarks</th>
                        <td colspan="3"><%//= v.getRemarks()%></td>
                    </tr>
                    <tr class="togglerow" style="display: none">
                        <th>Paid Address</th>
                        <td><%//= v.getPaidaddress()%></td>
                        <th>Paid Postcode</th>
                        <td><%//= v.getPaidpostcode()%></td>
                    </tr>
                    <tr class="togglerow" style="display: none">
                        <th>Paid City</th>
                        <td><%//= v.getPaidcity()%></td>
                        <th>Paid State</th>
                        <td><%//= v.getPaidstate()%></td>
                    </tr>
                    <tr class="togglerow" style="display: none">
                        <th>Rounding</th>
                        <td><%= v.getRacoacode()%> - <%= v.getRacoadesc()%></td>
                        <th>Paid Type</th>
                        <td><%//= v.getPaidtype()%></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th colspan="5">Receipt Information<!--<small>asdada</small>-->
                            <span class="pull-right">
                                <%= UIConfig.renderSecondSubButton(log, v.getRefer(), mod.getModuleID())%>
                            </span>
                        </th>
                    </tr>
                    <tr>
                        <th>Receipt No</th>
                        <th>Date</th>
                        <th>Issued By</th>
                        <th>Amount</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <%List<CbCashVoucherReceipt> listRefer = (List<CbCashVoucherReceipt>) CashVoucherDAO.getAllReceipt(log, v.getRefer());
                        if (listRefer.isEmpty()) {%>
                    <tr>
                        <td colspan="5">
                            <span class="font-small-red">No data available.</span>
                        </td>
                    </tr>
                    <%}
                        for (CbCashVoucherReceipt c : listRefer) {
                            CbCashVoucherReceiptPK cPK = c.getCbCashVoucherReceiptPK();
                    %>
                    <tr class="activerowx" id="<%= cPK.getRefer() %>">
                        <td class="tdrow">&nbsp;<i class="fa fa-paperclip" aria-hidden="true"></i>&nbsp;&nbsp;<%= cPK.getNoreceipt() %> </td>
                        <td class="tdrow">&nbsp;<%= c.getTarikh() %></td>
                        <td class="tdrow">&nbsp;<%= c.getTrade()%></td>
                        <td class="tdrow" align="right">&nbsp;<%= GeneralTerm.currencyFormat(c.getTotal())%></td>
                        <td class="tdrow" align="right">&nbsp;
                            <%
                                boolean[] typeArrayOn = {false, true, true};
                                String[] classArray = {"", "edititem", "deleteitem"};
                                String[] nameArray = {"", "edit_nd", "delete_nd"};
                                out.println(UIConfig.renderActionGroupButton(log, cPK.getRefer(), mod.getModuleID(), typeArrayOn, classArray, nameArray));
                            %>
                        </td>
                    </tr>
                    <tr id="togglerow_nd<%= cPK.getRefer()%>" style="display: none">
                        <td colspan="5">
                            <table class="table table-bordered">
                                <thead></thead>
                                <tbody>
                                    <tr>
                                        <th>Reference No</th>
                                        <td><%= cPK.getRefer()%></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <%}%>
                </tbody>
            </table>

            <table class="table table-bordered table-striped table-hover" id="dataTables">
                <thead>
                    <tr>
                        <th colspan="5">Detail<!--<small>asdada</small>-->
                            <span class="pull-right">
                                <%= UIConfig.renderFirstSubButton(log, v.getRefer(), mod.getModuleID())%>
                            </span>
                        </th>
                    </tr>
                    <tr>
                        <th>Reference No</th>
                        <th>Account</th>
                        <th>Sub Account</th>
                        <th>Amount</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <%List<CashVoucherAccount> listAll = (List<CashVoucherAccount>) CashVoucherDAO.getAllCVItem(log, v.getRefer());
                    if (listAll.isEmpty()) {%>
                <tbody>
                    <tr>
                        <td colspan="5">
                            <span class="font-small-red">No data available.</span>
                        </td>
                    </tr>
                    <%}
                        for (CashVoucherAccount j : listAll) {%>
                    <tr class="activerowy" id="<%= j.getRefer()%>">
                        <td class="tdrow">&nbsp;<%= j.getRefer()%></td>
                        <td class="tdrow">&nbsp;<%= j.getCoacode()%> - <%= j.getCoadescp()%></td>
                        <td class="tdrow">&nbsp;<%= j.getSatype()%> / <%= j.getSacode()%> / <%= j.getSadesc()%> </td>
                        <td class="tdrow" align="right">&nbsp;<%= GeneralTerm.currencyFormat(Double.parseDouble(j.getDebit()))%></td>
                        <td class="tdrow" align="right">&nbsp;
                            <%
                                boolean[] typeArrayOn = {false, true, true};
                                String[] classArray = {"", "edititem", "deleteitem"};
                                String[] nameArray = {"", "edit_st", "delete_st"};
                                out.println(UIConfig.renderActionGroupButton(log, j.getRefer(), mod.getModuleID(), typeArrayOn, classArray, nameArray));
                            %>
                        </td>
                    </tr>
                    <tr id="togglerow_st<%= j.getRefer()%>" style="display: none">
                        <td colspan="5">
                            <table class="table table-bordered">
                                <thead></thead>
                                <tbody>
                                    <tr>
                                        <th width="15%">Location</th>
                                        <td  width="35%"><%= j.getLoclevel()%> / <%= j.getLoccode()%> / <%= j.getLocname()%></td>
                                        <th width="15%">Taxation</th>
                                        <td width="35%"><%= j.getTaxcode()%> - <%= j.getTaxdescp()%> / <%= j.getSadesc()%></td>
                                    </tr>
                                    <tr>
                                        <th>Tax Amount</th>
                                        <td><%= j.getTaxamt()%></td>
                                        <th>Tax Rate</th>
                                        <td><%= j.getTaxrate()%></td>
                                    </tr>
                                    <tr>
                                        <th>Remarks</th>
                                        <td colspan="3"><%= j.getRemarks()%></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
                <%}%>
            </table>
        </div>
    </div>
</div> 
