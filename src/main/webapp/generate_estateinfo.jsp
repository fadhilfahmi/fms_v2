<%-- 
    Document   : test_connect
    Created on : Jan 19, 2017, 9:34:31 AM
    Author     : fadhilfahmi
--%>

<%@page import="java.sql.Statement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.lcsb.fms.util.model.ServerList"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            Connection conhq = ConnectionUtil.getHQConnection();
            Connection con = ConnectionUtil.getConnection();

            String query1 = "CREATE TABLE `estateinfo_period` ("
                    + "`managecode` varchar(100) NOT NULL DEFAULT '',"
                    + "`managedescp` varchar(100) NOT NULL DEFAULT '',"
                    + "`estatecode` varchar(100) NOT NULL DEFAULT '',"
                    + "`estatedescp` varchar(100) NOT NULL DEFAULT '',"
                    + "`datestart` date NOT NULL DEFAULT '0000-00-00',"
                    + "`hqsuspence` varchar(100) NOT NULL DEFAULT '',"
                    + "`hqcurrent` varchar(100) NOT NULL DEFAULT '',"
                    + "`hqsuspencedescp` varchar(100) NOT NULL DEFAULT '',"
                    + "`hqcurrentdescp` varchar(100) NOT NULL DEFAULT '',"
                    + "`address` text ,"
                    + "`postcode` varchar(100) DEFAULT '',"
                    + "`city` varchar(100) DEFAULT '',"
                    + "`district` varchar(100) DEFAULT '',"
                    + "`state` varchar(100) DEFAULT '',"
                    + "`phone` varchar(100)  DEFAULT '',"
                    + "`fax` varchar(100) DEFAULT '',"
                    + "`email` varchar(100) DEFAULT '',"
                    + "`mapref` varchar(100) DEFAULT '',"
                    + "`hectar` varchar(100) DEFAULT '',"
                    + "`major` varchar(100) DEFAULT '',"
                    + "`region` varchar(100),"
                    + "`active` varchar(3) DEFAULT 'Yes',"
                    + "`gstid` varchar(100) DEFAULT '',"
                    + "`taxreturnperiod` varchar(100)  DEFAULT '',"
                    + "`year` varchar(4) NOT NULL DEFAULT '',"
                    + "`period` varchar(2) NOT NULL DEFAULT ''"
                    + ") ENGINE=MyISAM DEFAULT CHARSET=latin1";

            String query2 = "CREATE TABLE `estateinfo_oldcode` ("
                    + "`managecode` varchar(100) NOT NULL DEFAULT '',"
                    + "`managedescp` varchar(100) NOT NULL DEFAULT '',"
                    + "`estatecode` varchar(100) NOT NULL DEFAULT '',"
                    + "`estatedescp` varchar(100) NOT NULL DEFAULT '',"
                    + "`datestart` date NOT NULL DEFAULT '0000-00-00',"
                    + "`hqsuspence` varchar(100) NOT NULL DEFAULT '',"
                    + "`hqcurrent` varchar(100) NOT NULL DEFAULT '',"
                    + "`hqsuspencedescp` varchar(100) NOT NULL DEFAULT '',"
                    + "`hqcurrentdescp` varchar(100) NOT NULL DEFAULT '',"
                    + "`address` text ,"
                    + "`postcode` varchar(100) DEFAULT '',"
                    + "`city` varchar(100) DEFAULT '',"
                    + "`district` varchar(100) DEFAULT '',"
                    + "`state` varchar(100) DEFAULT '',"
                    + "`phone` varchar(100)  DEFAULT '',"
                    + "`fax` varchar(100) DEFAULT '',"
                    + "`email` varchar(100) DEFAULT '',"
                    + "`mapref` varchar(100) DEFAULT '',"
                    + "`hectar` varchar(100) DEFAULT '',"
                    + "`major` varchar(100) DEFAULT '',"
                    + "`region` varchar(100),"
                    + "`active` varchar(3) DEFAULT 'Yes',"
                    + "`gstid` varchar(100) DEFAULT '',"
                    + "`taxreturnperiod` varchar(100)  DEFAULT ''"
                    + ") ENGINE=MyISAM DEFAULT CHARSET=latin1";

            //int j = 0;
            //List<ServerList> listItem = (List<ServerList>) ConnectionUtil.getAllServerlist();
            //for (ServerList i : listItem) {
            //   j++;
            //  out.println(j + ". " + i.getDescp() + " - " + i.getSvrip() + " - ");
            try {
                //Connection newcon = ConnectionUtil.createExternalConnection(i);
                Connection newcon = ConnectionUtil.createMillConnection(request.getParameter("estatecode"));
                //Connection newcon = ConnectionUtil.getHQConnection();

                //PreparedStatement ps = newcon.prepareStatement(query1);
                //ps.executeUpdate();
                // ps.close();
                Statement stmtDrop = newcon.createStatement();
                stmtDrop.executeUpdate("drop table if exists estateinfo_period");
                stmtDrop.executeUpdate("drop table if exists estateinfo_oldcode");

                PreparedStatement ps1 = newcon.prepareStatement(query1);
                //PreparedStatement ps1 = con.prepareStatement(query1);
                ps1.executeUpdate();

                ps1.close();

                PreparedStatement ps2 = newcon.prepareStatement(query2);
                //PreparedStatement ps2 = con.prepareStatement(query2);
                ps2.executeUpdate();

                ps2.close();

                Statement stmtclear = newcon.createStatement();
                stmtclear.executeUpdate("insert into estateinfo_oldcode (`managecode`,`managedescp`,`estatecode`,`estatedescp`,`datestart`,`hqsuspence`,`hqcurrent`,`hqsuspencedescp`,`hqcurrentdescp`,`address`,`postcode`,`city`,`district`, `state`,`phone`,`fax`,`email`,`mapref`,`hectar`,`major`,`region`,`active`,`gstid`,`taxreturnperiod`) select `managecode`,`managedescp`,`estatecode`,`estatedescp`,`datestart`,`hqsuspence`,`hqcurrent`,`hqsuspencedescp`,`hqcurrentdescp`,`address`,`postcode`,`city`,`district`, `state`,`phone`,`fax`,`email`,`mapref`,`hectar`,`major`,`region`,`active`,`gstid`,`taxreturnperiod` from estateinfo");
                stmtclear.close();

                String currentcode = "";

                ResultSet rs = null;
                PreparedStatement stmt = con.prepareStatement("select * from chartofacccount_side");
                rs = stmt.executeQuery();
                while (rs.next()) {
                    currentcode = rs.getString("currentcode");

                    String q = ("UPDATE estateinfo_oldcode set hqsuspence = ?,hqcurrent = ? WHERE hqcurrent = '" + currentcode + "'");
                    PreparedStatement ps3 = newcon.prepareStatement(q);
                    //PreparedStatement ps3 = con.prepareStatement(q);

                    ps3.setString(1, rs.getString("prevsuspencecode"));
                    ps3.setString(2, rs.getString("prevcurrentcode"));

                    ps3.executeUpdate();
                    ps3.close();
                }

                for (int i = 2009; i < 2017; i++) {

                    Statement stmtgen = newcon.createStatement();
                    stmtgen.executeUpdate("delete from estateinfo_period where year=" + i);	

                    for (int x = 1; x < 13; x++) {

                        stmtgen.executeUpdate("insert into estateinfo_period (`managecode`,`managedescp`,`estatecode`,`estatedescp`,`datestart`,`hqsuspence`,`hqcurrent`,`hqsuspencedescp`,`hqcurrentdescp`,`address`,`postcode`,`city`,`district`, `state`,`phone`,`fax`,`email`,`mapref`,`hectar`,`major`,`region`,`active`,`gstid`,`taxreturnperiod`,`year`,`period`) select `managecode`,`managedescp`,`estatecode`,`estatedescp`,`datestart`,`hqsuspence`,`hqcurrent`,`hqsuspencedescp`,`hqcurrentdescp`,`address`,`postcode`,`city`,`district`, `state`,`phone`,`fax`,`email`,`mapref`,`hectar`,`major`,`region`,`active`,`gstid`,`taxreturnperiod`,'" + i + "','" + String.valueOf(x) + "' from estateinfo_oldcode");	
                    }
                    stmtgen.close();
                }
                
                Statement stmtgen2017 = newcon.createStatement();
                stmtgen2017.executeUpdate("delete from estateinfo_period where year='2017'");	

                    for (int j = 1; j < 13; j++) {
                        stmtgen2017.executeUpdate("insert into estateinfo_period (`managecode`,`managedescp`,`estatecode`,`estatedescp`,`datestart`,`hqsuspence`,`hqcurrent`,`hqsuspencedescp`,`hqcurrentdescp`,`address`,`postcode`,`city`,`district`, `state`,`phone`,`fax`,`email`,`mapref`,`hectar`,`major`,`region`,`active`,`gstid`,`taxreturnperiod`,`year`,`period`) select `managecode`,`managedescp`,`estatecode`,`estatedescp`,`datestart`,`hqsuspence`,`hqcurrent`,`hqsuspencedescp`,`hqcurrentdescp`,`address`,`postcode`,`city`,`district`, `state`,`phone`,`fax`,`email`,`mapref`,`hectar`,`major`,`region`,`active`,`gstid`,`taxreturnperiod`,'2017','" + String.valueOf(j) + "' from estateinfo");		
                    }
                    stmtgen2017.close();
                out.println("Success<br>");

                newcon.close();

            } catch (Exception e) {
                e.printStackTrace();
                out.println("Failed - ");
                out.println(e + "<br>");
            }
            //}

            //conhq.close();

        %>
    </body>
</html>
