<%-- 
    Document   : cf_bank_edit
    Created on : Apr 20, 2016, 2:07:41 PM
    Author     : qoyum
--%>

<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.model.setup.company.Bank"%>
<%@page import="com.lcsb.fms.dao.setup.company.BankDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) BankDAO.getModule();
    Bank edit = (Bank) BankDAO.getInfo(log,request.getParameter("referenceno"));

%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $('#getbank').click(function (e) {

            var a = 'bankcode';
            var b = 'bankname';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Taxation',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_bank.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });
        
    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="editprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>



            <form data-toggle="validator" role="form" id="saveform">
                <input type="hidden" name="prepareid" id="pid" value="<%= log.getUserID()%>">
                <input type="hidden" name="preparename" id="pname" value="<%= log.getFullname()%>">
                <input type="hidden" name="preparedate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Code &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="code" name="code" value="<%=edit.getCode()%>" autocomplete="off" required readonly >   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Bank Name &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="name" name="name"  autocomplete="off" value="<%=edit.getName()%>">  
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Abbreviation &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="abbreviation" name="abbreviation" autocomplete="off" value="<%=edit.getAbbreviation()%>"> 
                            </div>
                        </div>

                    </div></div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Account Code&nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="coa" name="coa" value="<%=edit.getCoa()%>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="coa" id1="coadescp"><i class="fa fa-cog"></i> Account</button>
                                    </span>
                                </div></div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Account Description&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="coadescp" name="coadescp" placeholder="" autocomplete="off" value="<%=edit.getCoadescp()%>"> 
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Overdue &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="overdue" name="overdue" placeholder="" autocomplete="off" value="<%=edit.getOverdue()%>"> 
                            </div>
                        </div></div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Remarks &nbsp&nbsp<span class="res_code"></span></label>
                                <textarea class="form-control" rows="3"  id="remarks" name="remarks" ><%=edit.getRemarks()%></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
