<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>

<%
Connection con = ConnectionUtil.getConnection();
response.setHeader("Cache-Control", "no-cache");



  //Page configuration
  String title = "Supplier";  
  String url = "cf_supplier";
  String pagex = "cf_supplier_table";
  String add_button = "Add Supplier";
  String[] columntoView = {"code", "name", "abbreviation", "coa", "coadescp", "branch", "action" };
  String[] columntoViewwidth = {"20", "25", "25", "15", "15", "15", "15" };
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>IPAMIS</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<!-- print style sheet -->
<style type="text/css" media="print">
div.tableContainer {overflow: visible;  }
table>tbody {overflow: visible; }
td {height: 14pt;} /*adds control for test purposes*/
thead td  {font-size: 11pt; }
tfoot td  {
  text-align: center;
  font-size: 9pt;
  border-bottom: solid 1px slategray;
  }
  
thead {display: table-header-group; }
tfoot {display: table-footer-group; }
thead th, thead td  {position: static; } 

thead tr  {position: static; } /*prevent problem if print after scrolling table*/ 
table tfoot tr {     position: static;    }

body { font-size: 62.5%; }
    label, input { display:block; }
    input.text { margin-bottom:12px; width:95%; padding: .4em; }
    fieldset { padding:0; border:0; margin-top:25px; }
    h1 { font-size: 1.2em; margin: .6em 0; }
    div#users-contain { width: 350px; margin: 20px 0; }
    div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
    div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
    .ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
</style>
<style type="text/css" title="currentStyle">
 
</style>
<script type="text/javascript" language="javascript" src="dtables/media/js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="dtables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>



<!--<script type="text/javascript" language="javascript" src="jquery-ui-1.10.3.custom/js/jquery-1.9.1.js"></script>
<script type="text/javascript" language="javascript" src="jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript" language="javascript" src="jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>-->



    <script type="text/javascript" charset="utf-8">
      $(document).ready(function() {

      //line to configure***********************************************************************************************************************************  
        var updateTable = "supplier_info";
         var updateTitles = [ "Supplier Code","Supplier Name","Co. Registration No","Bumiputra","Account Code","Account Description","Contact Person","Contact Title","Contact Position","Contact HP No","Address","Postcode","City","State","Country","Telephone","Fax","Email","URL","Bank Code","Bank Name","Bank Account No","Payment Term (Days)","Remarks","Deposit Account Code","Deposit Account Description" ];
        var updateFields = [ "code","companyname","coregister","bumiputra","coa","coadescp","person","title","position","hp","address","postcode","city","state","country","phone","fax","email","url","bank","bankdesc","bankaccount","payment","remarks","depositcode","depositdesc" ];
        var columntoView = [ "code","companyname","coregister","coa","coadescp" ];

       // alert(updateFields.length);
       //validation
        var updateReq = [ "1", "1", "0", "0", "1", "1", "0", "0" ];//compulsory field
        var regexValid = [ "0", "0", "0", "0", "0", "0", "0" ];//check for numeric only
        //alert(updateFields.length);
        //end of validation
        var updateID = "code";
        var updateDesc = "companyname";
        var yesno = [ "Yes", "No" ];    
      var updateFieldtype = [ "text","text","text","text","text - button","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text" ];
      
      var updateFieldbtparam = [ "readonly","","","","","","" ];
      var updateFieldvalue=[ "AUTO","","","","","","" ];
      var updateFieldsql = [ "","","","","","","" ];
      var updateFieldparam = ["size=\"25\"","size=\"50\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"50\"","size=\"25\"","size=\"50\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"50\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"50\"" ];

//alert(updateFieldparam.length);
        var viewBoxTitle = "View <%=title%>";
        var addBoxTitle = "Add New <%=title%>";
        var updateBoxTitle = "Edit <%=title%>";
        var deleteBoxTitle = "Delete <%=title%>";
        var generalUsage = "<%=title%>";

        var deletePage = "cf_supplier_delete";
        var addPage = "cf_supplier_addprocess";

        //--------------additional functions goes here---------------------------------------------------------------------------------------------------------
        //----for branch b=branch-----------
        var bTable = "supplier_branch";
        var bTitles = [ "Supplier Code","Supplier Name","Branch Code","Branch Name","Contact Person","Contact Title","Contact Position","Contact HP No","Address","Postcode","City","State","Telephone","Fax","Email","URL","Remarks" ];
        var bFields = [ "suppcode","suppname","bcode","bname","person","title","position","hp","address","city","state","postcode","phone","fax","email","url","remarks" ];
        var bTitlestoview = [ "Branch Code","Branch Name","Contact Person","Contact Title", "Contact Position", "Contact HP No", "Address", "Postcode", "City", "State", "Telephone", "Fax", "Email", "URL", "Remarks" ];
        var bFieldstoview = [ "bcode","bname","person","title","position","hp","address","postcode","city","state","phone","fax","email","url","remarks" ];
        var bReq = [ "1", "1", "1", "0", "0", "0", "0", "0", "0", "0" ];
        var bID = "bcode";
        var bDesc = "bname"; 
      var bFieldtype = [ "text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text","text" ];
      var bFieldbtparam = [ "readonly","","","","","","","","","","","","","","","","" ];
      var bFieldvalue=[ "AUTO","","","","","","","","","","","","","","","","" ];
      var bFieldsql = [ "","","","","","","","","","","","","","","","","" ];
      var bFieldparam = ["size=\"25\"","size=\"50\"","size=\"25\"","size=\"50\"","size=\"50\"","size=\"50\"","size=\"50\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"","size=\"25\"" ];

      var updateReq_branch = [ "1", "1", "1", "1", "0", "0", "0", "0" ];//compulsory field

      var bBoxTitle = "Add New Branch";
        var editPage = "cf_supplier_editprocess";
        var bPage = "cf_supplier_branch_addprocess";
        var beditPage = "cf_supplier_branch_editprocess";
        var bdeletePage = "cf_supplier_branch_delete";

        var dicode = "";
        //line to configure***********************************************************************************************************************************

        var colsTosend = 'cols=';
      
      for (m = 0; m < columntoView.length; ++m) {//put value from database to variable jget
          
          if(m == columntoView.length - 1){
            colsTosend += columntoView[m];
          }else{
            colsTosend += columntoView[m]+'&cols=';
          }
      }

      //alert(colsTosend);

      

        $( "#add-new" ).button({
        icons: {
          primary: "ui-icon-circle-plus"
        }
      })
      .click(function() {
          addNew(addBoxTitle, updateFields, updateFieldtype, updateTitles, updateFieldparam, updateFieldsql, updateReq, addPage, updateID, null, updateReq);
      });



      $( "#button_view" ).button({
        icons: {
          primary: "ui-icon-circle-plus",
          text: false
        }
      });

      $( "#button_edit" ).button({
        icons: {
          primary: "ui-icon-bookmark",
          text: false
        }
      });

      $( "#get-acct" ).button({
        icons: {
          primary: "ui-icon-circle-plus"
        }
      })
      .click(function() {
          $( "#account-form" ).dialog( "open" );
      });
      
      
      var orderby = '';
      var oTable = $('#example').dataTable( {
        "bJQueryUI": true,
        "bProcessing": true,
        "bServerSide": true,
        "bAutoWidth": true,
        "sPaginationType": "full_numbers",
        "sScrollY": "320px",
        "sAjaxSource": "retrievetable.jsp?table="+updateTable+"&code="+updateID+"&"+colsTosend+"&order="+orderby,
        "aoColumns": [
          {"sTitle": "Supplier Code", "mData": "code", "sWidth": "8%", "bSearchable": true},
          {"sTitle": "Supplier Name", "mData": "companyname", "sWidth": "24%"},
          {"sTitle": "Register No", "mData": "coregister", "sWidth": "12%"},
          {"sTitle": "Account Code", "mData": "coa", "sWidth": "8%"},
          {"sTitle": "Account Description", "mData": "coadescp", "sWidth": "24%"},
          {"sTitle": "Branch", "mData": null, "bSortable": false, "bSearchable": false, "sWidth": "12%"},
          {"sTitle": "Action", "mData": null, "bSortable": false, "bSearchable": false, "sWidth": "12%"}
        ],
          "aoColumnDefs": [ {
            "aTargets": [6],
              "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
              
                var b = $('<img src="image/icon/view.png" title="View" width="22" style="vertical-align:middle;cursor:pointer">');
                var c = $('<img src="image/icon/edit_2.png" title="Edit" width="22" style="vertical-align:middle;cursor:pointer">');
                var e = $('<img src="image/icon/delete.png" title="Delete" width="22" style="vertical-align:middle;cursor:pointer">');
                b.addClass('buttontbl');
                c.addClass('buttontbl');
                e.addClass('buttontbl');
                
                 b.on('click', function() {
                  //console.log(oData);
                  viewRow(oData.code);
                  return false;
                });
                 c.on('click', function() {
                  //console.log(oData);
                  editRow(oData.code);
                  //$("#dialog-form-update").dialog("open");
                  //$( "#dialog-form-update" ).dialog( "open" );
                  return false;
                });
                e.on('click', function() {
                  //console.log(oData);
                  deleteRow(oData.code);
                  return false;
                });
                      
                $(nTd).empty();
                $(nTd).attr("id",'btntest');
                          $(nTd).prepend(b,c,e);
              }
             
          },
          {
            "aTargets": [5],
              "fnCreatedCell": function(nTd, sData, oData, iRow, iCol)              {
                var b = $('<img src="image/icon/add_2.png" title="Add Detail" width="22" style="vertical-align:middle;cursor:pointer">');
                var c = $('<img src="image/icon/view.png" title="View" width="22" style="vertical-align:middle;cursor:pointer">');
                b.addClass('buttontbl');
                c.addClass('buttontbl');
                
                b.on('click', function() {
                  //console.log(oData);
                  var sendInfo = [ oData.code, oData.companyname];
                  addNew(bBoxTitle, bFields, bFieldtype, bTitles, bFieldparam, bFieldsql, bReq, bPage, bID, sendInfo, updateReq_branch);
                  return false;
                });
                c.on('click', function() {
                  //console.log(oData);
                  var sendInfo = [ oData.code, oData.companyname];
                  viewBranch(oData.code, oData.companyname);
                  return false;
                });
                      
                $(nTd).empty();
                $(nTd).attr("id",'btntest');
                          $(nTd).prepend(b,c);
              }
          } ]

          
          
        } );



      $( "#btntest" ).button({
        icons: {
          primary: "ui-icon-circle-plus"
        }
      });

      
        
      function addNew(boxTitle, upFields, upType, upTitles, upParam, upSQL, upReq, upPage, upID, addInfo, validate) {
        //var boxTitle = addBoxTitle;
        var newItem = "";
        var str = '<div id="dialog-form-update" title="'+boxTitle+'"><p class="validateTips">Some form fields are required.</p><form><table id="table_2" cellspacing="0">';
        var i;

        for (i = 0; i < upFields.length; ++i) {//put value from database to variable jget
          //----------addon------------------
          var valueAdd = '';
          if(addInfo != null){
          
            if(i == 0){
              valueAdd = addInfo[i];
            }
            if(i == 1){
              valueAdd = addInfo[i];
            }
          }
          //----------addon------------------
          str += '<tr><td width="30%" class="bd_bottom" align="left">'+upTitles[i]+'</td>';
          str += '<td width="63%" class="bd_bottom" align="left">';

          if(upType[i] == "text"){

            if(upFields[i] == 'code'){
             
              str += '<input style="font-size:11px" id="ad_'+upFields[i]+'" type="text" value="'+valueAdd+'" '+upParam[i]+' readonly  />&nbsp;<input type="button" id="getvendor" value="Get Vendor Info">' ;
            }else if(upFields[i] == 'bank'){
             
              str += '<input style="font-size:11px" id="ad_'+upFields[i]+'" type="text" value="'+valueAdd+'" '+upParam[i]+' readonly  />&nbsp;<input type="button" id="getbank" value="Get Bank">' ;
            }else if(upFields[i] == 'depositcode'){
              dicode = "code like '1331%'";
              str += '<input style="font-size:11px" id="ad_'+upFields[i]+'" type="text" value="'+valueAdd+'" '+upParam[i]+' readonly  />&nbsp;<input type="button" class="getacc" id="yes" value="Get Code">' ;
            }else if(upFields[i] == 'bcode'){
              var no = '';//column to retrieve the value
              var table = 'supplier_branch';
              var colx = ['bcode'];
              var defineColumn = "ifnull(concat(lpad(max(bcode)+1,6,'0')), '"+addInfo[0]+"01') as";
              var strx = 'cols=';
              for (j = 0; j < colx.length; ++j) {
                  strx+= colx[j]+'&cols=';
              }

              var where = 'where suppcode='+addInfo[0];
              var exString1 = 'table='+table+'&'+strx+'&sWhere='+where;
              $.ajax({
                url: "get_dataquery.jsp?definecolumn="+encodeURIComponent(defineColumn),
                type:'POST',
                async: false,
                data: exString1,
                success: function(output_string){
                  var obj = $.parseJSON(output_string);
                  $.each(obj, function() {
                      no = this['bcode'];

                  });
                }
              });

              valueAdd=no;
              str += '<input style="font-size:11px" id="ad_'+upFields[i]+'" type="text" value="'+valueAdd+'" '+upParam[i]+' readonly  />';
            }else if(upFields[i] == 'bankcode' || upFields[i] == 'bankname'){
              str += '<input style="font-size:11px" id="ad_'+upFields[i]+'" type="text" value="'+valueAdd+'" '+upParam[i]+' readonly  />';
            }else if(upFields[i] == 'branchname'){
              str += '<input style="font-size:11px" id="ad_'+upFields[i]+'" type="text" value="'+addInfo[2]+' - " '+upParam[i]+'   /><input type="hidden" id="defaultabb" value="'+addInfo[2]+' - ">';
            }else{
              str += '<input style="font-size:11px" id="ad_'+upFields[i]+'" type="text" '+upParam[i]+' value="'+valueAdd+'"  />';
            }

            

          }else if(upType[i] == "text - button"){

            str += '<input id="ad_'+upFields[i]+'"  type="text" '+upParam[i]+' />&nbsp;<input type="button" class="getacc" id="no" value="Get Account Code"></td>';

          }else if(upType[i] == "combo - listdb"){

            str += '<select id="ad_'+upFields[i]+'"  '+upParam[i]+'>';
            var listDB = "parameter";
            var listFields = [ "parameter", "value" ];
            var listID = "aparameter";
            var listItem = 'cols=';
            
            for (m = 0; m < listFields.length; ++m) {//put value from database to variable jget
              listItem += listFields[m]+'&cols=';
            }
            
            var listString = 'identifier='+listID+'&'+listID+'='+upSQL[i]+'&table='+listDB+'&'+listItem;

            $.ajax({
              url: 'get_data.jsp',
              type:'POST',
              async: false,
              data: listString,
              success: function(output){
                var obja = $.parseJSON(output);
                        $.each(obja, function() {
                    var paramt = this['parameter'];
                    var valt = this['value'];
                    str += '<option value"'+valt+'">'+valt+'</option>';
                  });
              }
            });
            
            str += '</select>';
                          
          }else if(upType[i] == "combo - yesno"){

            str += '<select id="ad_'+upFields[i]+'"  '+upParam[i]+'>';
            var selOpt = "";

            for (u = 0; u < yesno.length; ++u) {
              str += '<option value"'+yesno[u]+'">'+yesno[u]+'</option>';
            }

            str += '</select>';

            
          }
                        
          str += '</td><td class="bd_bottom">&nbsp;</td></td><td class="bd_bottom">&nbsp; </td></tr>';

        }

        str += '</table></form></div>';

        var addTable = $(str);

        $(addTable).dialog({
            autoOpen: false,
          height: 550,
          width: 700,
          modal: true,
          buttons: {
              "Save": function() {
              var eachI = [];
              var bValid = true;
              var c = 0;
              newItem = $('#ad_'+upFields[1]).val(); 
              //$(this).addClass('ui-state-error');

              var eachV = [];
                var idV = '';
                for (j = 0; j < upFields.length; ++j) {
                if(validate[j] == 1){
                      //alert(updateFields[j]);

                      eachV[j]= $('#ad_'+upFields[j]).val();

                      bValid = checkLength( eachV[j], 'ad_'+upFields[j]);

                      if(bValid==false){
                        c++;
                      }
                  

                    if(regexValid[j] == 1){
                      
                      eachV[j]= $('#ad_'+upFields[j]).val();
                      bValid = checkRegexp( eachV[j], 'ad_'+upFields[j]);
                      if(bValid==false){
                        c++;
                      }
                      
                    

                    }
                    
                  }

                }

                //special valid for branchname
                if(upFields[2]=='branchcode'){
                  var n = $('#ad_branchname').val();
                var t = $('#defaultabb').val();

                if(n.length == t.length){
                  $('#ad_branchname').addClass('ui-state-error');
                  $(this).addClass('ui-state-error');

                  c++;
                }
                }
                
                //----------------------------

              if(c>0){
                $(this).addClass('ui-state-error');
              }

              if (c==0) {

                var s = 0;
                var upTitle = [];
                var dataStringUp = "";
                for (j = 0; j < upFields.length; ++j) {//put value from database to variable jget
                  upTitle[j] = $('#ad_'+upFields[j]).val();

                  if(j == upFields.length-1){
                    dataStringUp += upFields[j]+'='+upTitle[j];
                  }else{
                    dataStringUp += upFields[j]+'='+upTitle[j]+'&';
                  }
                                          
                }
                                 //alert(dataStringUp);
                $.ajax({
                  type:'POST',
                  async: false,
                  data:dataStringUp,
                  url:upPage+'.jsp',
                  success:function(response) {
                    s = 1;
                  }
                });
                              
                if(s==1){
                  $(this).dialog("destroy");

                  successNoty( generalUsage , newItem, generalUsage+' Added', 'Successfull added ');
                  setTimeout(function(){
                      location.reload();}, 1000);
                }
                
              }
            },
            Cancel: function() {
              $(this).dialog("destroy");
            }
          }
        });

          $(addTable).dialog("open");

          ///---------important to destroy box get
                  $('.ui-dialog-titlebar-close').on('click',function(){
                    $(addTable).dialog("destroy");
                    $('#get_supplier_box').remove();
                  }); 
                  ///---------important to destroy box get

          /*$( ".getsomething" ).button({
                  icons: {
                    primary: "ui-icon-circle-plus"
                  }
                });
  */

          //---------validation----------
          /*var eachV = '';
          var idV = '';
          for (j = 0; j < updateFields.length; ++j) {
          if(updateReq[j] == 1){
              $('#ad_'+updateFields[j]).on('focusout',function(){
                //alert(updateFields[j]);
                eachV= $(this).val();
                idV= $(this).attr('id');

                checkLength( eachV, idV);
                
              });

              
              
            }

            if(regexValid[j] == 1){

                $('#ad_'+updateFields[j]).on('focus',function(){
                //alert(updateFields[j]);
                $(this).removeClass('ui-state-error');
                $("#spanerr").remove();
                
              });

              $('#ad_'+updateFields[j]).on('focusout',function(){
                //alert(updateFields[j]);
                eachV= $(this).val();
                idV= $(this).attr('id');
                
                checkRegexp( eachV, idV);
                
              });

              }
          }
          */
          //special valid
            $('#ad_branchname').on('focusout',function(){
              var n = $(this).val();
              var t = $('#defaultabb').val();

              if(n.length == t.length){
                $(this).addClass('ui-state-error');
              }
                //alert(n);
                //eachV= $(this).val();
                //idV= $(this).attr('id');
                
                //checkRegexp( eachV, idV);
                
              });
          //------end of validation \

        //-----------get bank-----------------------

        
        $('#getbank').on('click',function(){
          var table = 'info_bank';
          var colx = [ "code","name" ];

          var strx = 'cols=';
          for (j = 0; j < colx.length; ++j) {
            strx+= colx[j]+'&cols=';
          }

          var where = "";
          var exString1 = 'table='+table+'&'+strx;

          var strGetBox = '<div id="get_supplier_box" title="Get Bank">';

            strGetBox += '<div class="ui-widget" align="left"><label for="keyword-update">Type the Vendor Code or Vendor Name: </label><input id="keyword-insert" align="left align="left""  size="55"></div>';
            strGetBox += '<div><table id="supplier_output">';
            
            var y = 0;
            $.ajax({
              url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
              type:'POST',
              async: false,
              data: exString1,
              success: function(output){
                var jGet = [];
                var i = 0;
                var obja = $.parseJSON(output);
                        $.each(obja, function() {
                          y++
                          
                          for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
                      jGet[i] = this[colx[i]];
                    }
                    strGetBox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
                     for (i = 0; i < colx.length; ++i) {
                        strGetBox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
                    }
                    strGetBox += '</td></tr>';
                  });
                } 
            }); 
            strGetBox += '';
            strGetBox += '</table></div>';
            strGetBox += '</div>';
          
          var updateTableMini = $(strGetBox);

          $( updateTableMini ).dialog({
            autoOpen: false,
            height: 300,
            width: 400,
            modal: true,
            buttons: {

            }
          });

          $( updateTableMini ).dialog( "open" );

          var fieldform = [ "bank","bankdesc" ];

          

          for (j = 1; j < y+1; ++j) {  
                $('.selected_td'+j).on('click',function(){
                  var m = $(this).html();
                var v = $(this).attr('id');
                 var nV = "";
                for (i = 0; i < colx.length; ++i) {
                  //alert(colx[i]+'--'+updateFields[i]);
                  nv = $('#'+colx[i]+v).val();
                  
                  $("#ad_"+fieldform[i]).val(nv);
                }
                
                $( "#get_supplier_box" ).remove();

                });
              }
          
          $( "#keyword-insert" ).keyup(function() {

            var newacc = $('#keyword-insert').val();
            var where2 = " where code like '%"+newacc+"%' or name like '%"+newacc+"%' order by code";
            var exString2 = 'table='+table+'&'+strx;


            var returnsuppbox = '<table id="supplier_output">';

            var y = 0;
            $.ajax({
              url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2),
              type:'POST',
              async: false,
              data: exString2,
              success: function(output){
                var jGet = [];
                var i = 0;
                var obja = $.parseJSON(output);
                        $.each(obja, function() {
                         y++
                          
                          for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
                      jGet[i] = this[colx[i]];
                    }
                    returnsuppbox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
                     for (i = 0; i < colx.length; ++i) {
                        returnsuppbox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
                    }
                  });
                } 
            }); 
            returnsuppbox += '';
            returnsuppbox += '</table>';

            $('#supplier_output').html(returnsuppbox);

            for (j = 1; j < y+1; ++j) {  
                $('.selected_td'+j).on('click',function(){
                  var m = $(this).html();
                var v = $(this).attr('id');
                 var nV = "";
                for (i = 0; i < colx.length; ++i) {
                  //alert(colx[i]+'--'+updateFields[i]);
                  nv = $('#'+colx[i]+v).val();
                  
                  $("#ad_"+fieldform[i]).val(nv);
                }
                
                $( "#get_supplier_box" ).remove();

                });
              }
          });
        });
        //---------get bank---------------

        //--------------------------get vendor---------------------------
        $('#getvendor').on('click',function(){
            var table = 'vendor_info';
          var colx = [ "code","vendor_name","register_no","vendor_address","postcode","city","state","country","active","bumiputra","person","title","hp","phone","fax","email","url","position" ];

          var strx = 'cols=';
          for (j = 0; j < colx.length; ++j) {
            strx+= colx[j]+'&cols=';
          }

          var where = "";
          var exString1 = 'table='+table+'&'+strx;

          var strGetBox = '<div id="get_supplier_box" title="Get Vendor">';

            strGetBox += '<div class="ui-widget" align="left"><label for="keyword-update">Type the Vendor Code or Vendor Name: </label><input id="keyword-insert" align="left align="left""  size="55"></div>';
            strGetBox += '<div><table id="supplier_output">';
            //strGetBox += '<div class="ui-widget" style="margin-top:2em; font-family:Arial" align="left">Buyer Code:<br><input type="text" id="gcode-update" size="55"><br><textarea id="gdescp-update" cols="55" rows="5"></textarea></div>';
            var y = 0;
            $.ajax({
              url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
              type:'POST',
              async: false,
              data: exString1,
              success: function(output){
                var jGet = [];
                var i = 0;
                var obja = $.parseJSON(output);
                        $.each(obja, function() {
                          y++
                          
                          for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
                      jGet[i] = this[colx[i]];
                    }
                    strGetBox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
                     for (i = 0; i < colx.length; ++i) {
                        strGetBox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
                    }
                    strGetBox += '</td></tr>';
                  });
                } 
            }); 
            strGetBox += '';
            strGetBox += '</table></div>';
            strGetBox += '</div>';


          var updateTableMini = $(strGetBox);

          $( updateTableMini ).dialog({
            autoOpen: false,
            height: 300,
            width: 400,
            modal: true,
            buttons: {
            
              /*"Get Code": function() {
                var gcodex = $("#gcode-update").val();
                var gdescpx = $("#gdescp-update").val();
                $("#ad_companycode").val(gcodex);
                $("#ad_companyname").val(gdescpx);
                $(this).dialog("destroy");
              },
              Cancel: function() {
                $(this).dialog("destroy");
                $( this ).dialog( "close" );
              }*/
            }
          });

          $( updateTableMini ).dialog( "open" );

          var fieldform = [ "code","companyname","coregister","bumiputra","person","title","position","hp","address","city","state","postcode","country","phone","fax","email","url" ];

          var fromvendor = [ "code","vendor_name","register_no","bumiputra","person","title","position","hp","vendor_address","city","state","postcode","country","phone","fax","email","url" ];

          for (j = 1; j < y+1; ++j) {  
                $('.selected_td'+j).on('click',function(){
                  var m = $(this).html();
                var v = $(this).attr('id');
                 var nV = "";
                for (i = 0; i < fromvendor.length; ++i) {
                  //alert(colx[i]+'--'+updateFields[i]);
                  nv = $('#'+fromvendor[i]+v).val();
                  
                  $("#ad_"+fieldform[i]).val(nv);
                }
                
                $( "#get_supplier_box" ).remove();

                });
              }
          
          /*$( "#keyword-update" ).autocomplete({
            source: "get_buyer.jsp",
            minLength: 2,
            select: function( event, ui ) {
              loga( ui.item ?
                ui.item.id :
                "Nothing selected, input was " + this.value );
              logi( ui.item ?
                ui.item.value :
                "Nothing selected, input was " + this.value );
            }
          });

  */      
          $( "#keyword-insert" ).keyup(function() {

            var newacc = $('#keyword-insert').val();
            var where2 = " where code like '%"+newacc+"%' or vendor_name like '%"+newacc+"%' order by code";
            var exString2 = 'table='+table+'&'+strx;


            var returnsuppbox = '<table id="supplier_output">';
            //strGetBox += '<div class="ui-widget" style="margin-top:2em; font-family:Arial" align="left">Buyer Code:<br><input type="text" id="gcode-update" size="55"><br><textarea id="gdescp-update" cols="55" rows="5"></textarea></div>';
            var y = 0;
            $.ajax({
              url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2),
              type:'POST',
              async: false,
              data: exString2,
              success: function(output){
                var jGet = [];
                var i = 0;
                var obja = $.parseJSON(output);
                        $.each(obja, function() {
                         y++
                          
                          for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
                      jGet[i] = this[colx[i]];
                    }
                    returnsuppbox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
                     for (i = 0; i < colx.length; ++i) {
                        returnsuppbox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
                    }
                  });
                } 
            }); 
            returnsuppbox += '';
            returnsuppbox += '</table>';

            $('#supplier_output').html(returnsuppbox);

            for (j = 1; j < y+1; ++j) {  
                $('.selected_td'+j).on('click',function(){
                  var m = $(this).html();
                var v = $(this).attr('id');
                 var nV = "";
                for (i = 0; i < fromvendor.length; ++i) {
                  //alert(colx[i]+'--'+updateFields[i]);
                  nv = $('#'+fromvendor[i]+v).val();
                  
                  $("#ad_"+fieldform[i]).val(nv);
                }
                
                $( "#get_supplier_box" ).remove();

                });
              }
          });
        });
        

        //-----------------------get account code-----------------------------------  
        $('.getacc').on('click',function(){

          var where = '';
          var where2 = '';
          var tocd = '';
          var tods = '';
          var u = $(this).attr('id');

          
          if(u == 'yes'){
            where = " where active = 'Yes' and "+dicode+" order by code";
            tocd = 'depositcode';
            tods = 'depositdesc';
          }else if(u == 'no'){
            where = " where active = 'Yes' order by code";
            tocd = 'coa';
            tods = 'coadescp';

          }
          var table = 'chartofacccount';
          var colx = ['code', 'descp' ];
          var strx = 'cols=';
          for (j = 0; j < colx.length; ++j) {
            strx+= colx[j]+'&cols=';
          }

          
          var exString1 = 'table='+table+'&'+strx;

          var strGetBox = '<div id="get_supplier_box" title="Get Account">';

            strGetBox += '<div class="ui-widget" align="left"><label for="keyword-update">Type the Account Code or Account Name: </label><input id="keyword-insert" align="left align="left""  size="55"></div>';
            strGetBox += '<div><table id="supplier_output">';
            //strGetBox += '<div class="ui-widget" style="margin-top:2em; font-family:Arial" align="left">Buyer Code:<br><input type="text" id="gcode-update" size="55"><br><textarea id="gdescp-update" cols="55" rows="5"></textarea></div>';
            var y = 0;
            $.ajax({
              url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
              type:'POST',
              async: false,
              data: exString1,
              success: function(output){
                var jGet = [];
                var i = 0;
                var obja = $.parseJSON(output);
                        $.each(obja, function() {
                          y++
                          
                          for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
                      jGet[i] = this[colx[i]];
                    }
                    strGetBox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
                    strGetBox += '<input type="hidden" value="'+jGet[0]+'" id="acccode'+jGet[0]+'">';
                    strGetBox += '<input type="hidden" value="'+jGet[1]+'" id="accdesc'+jGet[0]+'">';
                    strGetBox += '</td></tr>';
                  });
                } 
            }); 
            strGetBox += '';
            strGetBox += '</table></div>';
            strGetBox += '</div>';


          var updateTableMini = $(strGetBox);

          $( updateTableMini ).dialog({
            autoOpen: false,
            height: 300,
            width: 400,
            modal: true,
            buttons: {
            
              /*"Get Code": function() {
                var gcodex = $("#gcode-update").val();
                var gdescpx = $("#gdescp-update").val();
                $("#ad_companycode").val(gcodex);
                $("#ad_companyname").val(gdescpx);
                $(this).dialog("destroy");
              },
              Cancel: function() {
                $(this).dialog("destroy");
                $( this ).dialog( "close" );
              }*/
            }
          });

          $( updateTableMini ).dialog( "open" );

          for (j = 1; j < y+1; ++j) {  
                $('.selected_td'+j).on('click',function(){
                  var m = $(this).html();
                var v = $(this).attr('id');
                var aVal = $('#acccode'+v).val();
                var bVal = $('#accdesc'+v).val();
                $("#ad_"+tocd).val(aVal);
                $("#ad_"+tods).val(bVal);
                $( "#get_supplier_box" ).remove();

                });
              }
          
          /*$( "#keyword-update" ).autocomplete({
            source: "get_buyer.jsp",
            minLength: 2,
            select: function( event, ui ) {
              loga( ui.item ?
                ui.item.id :
                "Nothing selected, input was " + this.value );
              logi( ui.item ?
                ui.item.value :
                "Nothing selected, input was " + this.value );
            }
          });

  */      
          $( "#keyword-insert" ).keyup(function() {

            var newacc = $('#keyword-insert').val();

            
            if(u == 'yes'){
              where2 = " where "+dicode+" and (code like '%"+newacc+"%' or descp like '%"+newacc+"%')   order by code";
            }else if(u == 'no'){
              where2 = " where code like '%"+newacc+"%' or descp like '%"+newacc+"%' order by code";

            }
            var exString2 = 'table='+table+'&'+strx;


            var returnsuppbox = '<table id="supplier_output">';
            //strGetBox += '<div class="ui-widget" style="margin-top:2em; font-family:Arial" align="left">Buyer Code:<br><input type="text" id="gcode-update" size="55"><br><textarea id="gdescp-update" cols="55" rows="5"></textarea></div>';
            var y = 0;
            $.ajax({
              url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2),
              type:'POST',
              async: false,
              data: exString2,
              success: function(output){
                var jGet = [];
                var i = 0;
                var obja = $.parseJSON(output);
                        $.each(obja, function() {
                          y++;
                          
                          for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
                      jGet[i] = this[colx[i]];
                    }
                    returnsuppbox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
                    returnsuppbox += '<input type="hidden" value="'+jGet[0]+'" id="acccode'+jGet[0]+'">';
                    returnsuppbox += '<input type="hidden" value="'+jGet[1]+'" id="accdesc'+jGet[0]+'">';
                    returnsuppbox += '</td></tr>';
                  });
                } 
            }); 
            returnsuppbox += '';
            returnsuppbox += '</table>';

            $('#supplier_output').html(returnsuppbox);

            for (j = 1; j < y+1; ++j) {  
                $('.selected_td'+j).on('click',function(){
                  var m = $(this).html();
                var v = $(this).attr('id');
                var aVal = $('#acccode'+v).val();
                var bVal = $('#accdesc'+v).val();

                $("#ad_"+tocd).val(aVal);
                $("#ad_"+tods).val(bVal);
                $( "#get_supplier_box" ).remove();

                });
              }
          });

        });
  
        

        }
        //$("body").on("click", "#example tbody tr", function (e) {
        function editRow(tdCode) {
          var nDesc = "";
                var strItem = 'cols=';

                for (j = 0; j < updateFields.length; ++j) {//put value from database to variable jget
                    strItem+= updateFields[j]+'&cols=';
          }

          var sCode = tdCode;
                var dialogText="Value = "+sCode;
                var targetUrl = $(this).attr("href");
          var exString = 'identifier='+updateID+'&'+updateID+'='+sCode+'&table='+updateTable+'&'+strItem;
                
                $.ajax({
                url: 'get_data.jsp',
                type:'POST',
                data: exString,
                success: function(output_string){
                    var obj = $.parseJSON(output_string);
                      $.each(obj, function() {
                        //var pd = $("<div/>").attr("id", "pageDialog");
                  //$(pd).text(jsType ).dialog("open");
                  var str = '<div id="dialog-form-update" title="'+updateBoxTitle+'"><p class="validateTips">All form fields are required.</p><form><table id="table_2" cellspacing="0">';
                  var i;
                  var jGet = [];

                  for (i = 0; i < updateFields.length; ++i) {//put value from database to variable jget
                    jGet[i] = this[updateFields[i]];
                    str += '<tr><td width="30%" class="bd_bottom" align="left">'+updateTitles[i]+'</td>';
                    str += '<td width="63%" class="bd_bottom" align="left">';

                    if(updateFieldtype[i] == "text"){
                      if(updateFields[i]==updateID){

                        str += '<input style="font-size:11px" id="up_'+updateFields[i]+'" type="text" value="'+jGet[i]+'" '+updateFieldparam[i]+' readonly  />';
                      }else if(updateFields[i] == 'bank'){
             
                        str += '<input style="font-size:11px" id="up_'+updateFields[i]+'" type="text" value="'+jGet[i]+'" '+updateFieldparam[i]+' readonly  />&nbsp;<input type="button" id="getbank" value="Get Bank">' ;
                      }else if(updateFields[i] == 'depositcode'){
                        dicode = "code like '1331%'";
                        str += '<input style="font-size:11px" id="up_'+updateFields[i]+'" type="text" value="'+jGet[i]+'" '+updateFieldparam[i]+' readonly  />&nbsp;<input type="button" class="getacc" id="yes" value="Get Code">' ;
                      }else{
                        str += '<input style="font-size:11px" id="up_'+updateFields[i]+'" type="text" value="'+jGet[i]+'" '+updateFieldparam[i]+'  />';
                      }
                      
                    }else if(updateFieldtype[i] == "text - button"){
                      str += '<input id="up_'+updateFields[i]+'"  type="text" value="'+jGet[i]+'" '+updateFieldparam[i]+' />&nbsp;<input type="button" class="getacc" id="no"  value="Get Account Code"></td>';
                    }else if(updateFieldtype[i] == "combo - listdb"){
                      str += '<select id="up_'+updateFields[i]+'"  '+updateFieldparam[i]+'>';
                      var selParam = "";
                      var listDB = "parameter";
                        var listFields = [ "parameter", "value" ];
                        var listID = "aparameter";
                        var listItem = 'cols=';
                        
                        for (m = 0; m < listFields.length; ++m) {//put value from database to variable jget
                            listItem += listFields[m]+'&cols=';
                      }
                        
                        var listString = 'identifier='+listID+'&'+listID+'='+updateFieldsql[i]+'&table='+listDB+'&'+listItem;

                      $.ajax({
                        url: 'get_data.jsp',
                          type:'POST',
                          async: false,
                          data: listString,
                          success: function(output){
                              var obja = $.parseJSON(output);
                                $.each(obja, function() {
                            var paramt = this['parameter'];
                            var valt = this['value'];

                            if(valt == jGet[i]){
                              selParam="selected";
                            }else{
                              selParam="";
                            }
                              str += '<option value"'+valt+'" '+selParam+'>'+valt+'</option>';
                                
                          });
                          }
                      });
                      
                      str += '</select>';
                          
                    }else if(updateFieldtype[i] == "combo - yesno"){
                      str += '<select id="up_'+updateFields[i]+'"  '+updateFieldparam[i]+'>';
                      var selOpt = "";

                      for (u = 0; u < yesno.length; ++u) {
                        if(yesno[u] == jGet[i]){
                          selOpt="selected";
                        }else{
                          selOpt="";
                        }
                        str += '<option value"'+yesno[u]+'" '+selOpt+'>'+yesno[u]+'</option>';
                      }
                      str += '</select>';
                    }
                        
                    str += '</td><td class="bd_bottom">&nbsp;</td></td><td class="bd_bottom">&nbsp; </td></tr>';

                      }

                        str += '</table></form></div>';

                        var updateTable = $(str);

                        $(updateTable).dialog({
                          autoOpen: false,
                    height: 550,
                    width: 700,
                    modal: true,
                    buttons: {
                        "Update Account": function() {
                          var upTitle = [];
                        var dataStringUp = "";

                        for (j = 0; j < updateFields.length; ++j) {//put value from database to variable jget
                                upTitle[j] = $('#up_'+updateFields[j]).val();

                          if(updateDesc == updateFields[j]){
                                    nDesc = $('#up_'+updateFields[j]).val();
                                }
                                      
                                if(j == updateFields.length-1){
                                    dataStringUp += updateFields[j]+'='+upTitle[j];
                                }else{
                                    dataStringUp += updateFields[j]+'='+upTitle[j]+'&';
                                }
                                      
                        }
                             //alert(dataStringUp);
                        $.ajax({
                            type:'POST',
                            data:dataStringUp,
                            url:editPage+'.jsp',
                            success:function(response) {
                                $(this).dialog("destroy");
                            }
                        });
                        
                        $(this).dialog("destroy");
                        successNoty( generalUsage , nDesc, generalUsage+' Updated', 'Successfull for ');
                        setTimeout(function(){
                      location.reload();}, 1000);
                           
                      },
                      Cancel: function() {
                        $(this).dialog("destroy");
                        //$( this ).dialog( "close" );
                      }
                    }
                        });
                    
                    $(updateTable).dialog("open");

                    ///---------important to destroy box get
                  $('.ui-dialog-titlebar-close').on('click',function(){
                    $(updateTable).dialog("destroy");
                    $('#get_supplier_box').remove();
                  }); 
                  ///---------important to destroy box get

                });

//-----------get bank-----------------------

        
        $('#getbank').on('click',function(){
          var table = 'info_bank';
          var colx = [ "code","name" ];

          var strx = 'cols=';
          for (j = 0; j < colx.length; ++j) {
            strx+= colx[j]+'&cols=';
          }

          var where = "";
          var exString1 = 'table='+table+'&'+strx;

          var strGetBox = '<div id="get_supplier_box" title="Get Bank">';

            strGetBox += '<div class="ui-widget" align="left"><label for="keyword-update">Type the Vendor Code or Vendor Name: </label><input id="keyword-insert" align="left align="left""  size="55"></div>';
            strGetBox += '<div><table id="supplier_output">';
            
            var y = 0;
            $.ajax({
              url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
              type:'POST',
              async: false,
              data: exString1,
              success: function(output){
                var jGet = [];
                var i = 0;
                var obja = $.parseJSON(output);
                        $.each(obja, function() {
                          y++
                          
                          for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
                      jGet[i] = this[colx[i]];
                    }
                    strGetBox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
                     for (i = 0; i < colx.length; ++i) {
                        strGetBox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
                    }
                    strGetBox += '</td></tr>';
                  });
                } 
            }); 
            strGetBox += '';
            strGetBox += '</table></div>';
            strGetBox += '</div>';
          
          var updateTableMini = $(strGetBox);

          $( updateTableMini ).dialog({
            autoOpen: false,
            height: 300,
            width: 400,
            modal: true,
            buttons: {

            }
          });

          $( updateTableMini ).dialog( "open" );

          var fieldform = [ "bank","bankdesc" ];

          

          for (j = 1; j < y+1; ++j) {  
                $('.selected_td'+j).on('click',function(){
                  var m = $(this).html();
                var v = $(this).attr('id');
                 var nV = "";
                for (i = 0; i < colx.length; ++i) {
                  //alert(colx[i]+'--'+updateFields[i]);
                  nv = $('#'+colx[i]+v).val();
                  
                  $("#up_"+fieldform[i]).val(nv);
                }
                
                $( "#get_supplier_box" ).remove();

                });
              }
          
          $( "#keyword-insert" ).keyup(function() {

            var newacc = $('#keyword-insert').val();
            var where2 = " where code like '%"+newacc+"%' or name like '%"+newacc+"%' order by code";
            var exString2 = 'table='+table+'&'+strx;


            var returnsuppbox = '<table id="supplier_output">';
            
            var y = 0;
            $.ajax({
              url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2),
              type:'POST',
              async: false,
              data: exString2,
              success: function(output){
                var jGet = [];
                var i = 0;
                var obja = $.parseJSON(output);
                        $.each(obja, function() {
                         y++
                          
                          for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
                      jGet[i] = this[colx[i]];
                    }
                    returnsuppbox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
                     for (i = 0; i < colx.length; ++i) {
                        returnsuppbox += '<input type="hidden" value="'+jGet[i]+'" id="'+colx[i]+jGet[0]+'">';
                    }
                  });
                } 
            }); 
            returnsuppbox += '';
            returnsuppbox += '</table>';

            $('#supplier_output').html(returnsuppbox);

            for (j = 1; j < y+1; ++j) {  
                $('.selected_td'+j).on('click',function(){
                  var m = $(this).html();
                var v = $(this).attr('id');
                 var nV = "";
                for (i = 0; i < colx.length; ++i) {
                  //alert(colx[i]+'--'+updateFields[i]);
                  nv = $('#'+colx[i]+v).val();
                  
                  $("#up_"+fieldform[i]).val(nv);
                }
                
                $( "#get_supplier_box" ).remove();

                });
              }
          });
        });
        //---------get bank---------------
            
                
        $('.getacc').on('click',function(){

          var where = '';
          var where2 = '';
          var tocd = '';
          var tods = '';
          var u = $(this).attr('id');
          
          if(u == 'yes'){
            where = " where active = 'Yes' and "+dicode+" order by code";
            tocd = 'depositcode';
            tods = 'depositdesc';
          }else if(u == 'no'){
            where = " where active = 'Yes' order by code";
            tocd = 'coa';
            tods = 'coadescp';

          }
          
          var table = 'chartofacccount';
          var colx = ['code', 'descp' ];
          var strx = 'cols=';
          for (j = 0; j < colx.length; ++j) {
            strx+= colx[j]+'&cols=';
          }

          var exString1 = 'table='+table+'&'+strx;

          var strGetBox = '<div id="get_supplier_box" title="Get Account">';

            strGetBox += '<div class="ui-widget" align="left"><label for="keyword-update">Type the Account Code or Account Name: </label><input id="keyword-insert" align="left align="left""  size="55"></div>';
            strGetBox += '<div><table id="supplier_output">';
            //strGetBox += '<div class="ui-widget" style="margin-top:2em; font-family:Arial" align="left">Buyer Code:<br><input type="text" id="gcode-update" size="55"><br><textarea id="gdescp-update" cols="55" rows="5"></textarea></div>';
            var y = 0;
            $.ajax({
              url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where),
              type:'POST',
              async: false,
              data: exString1,
              success: function(output){
                var jGet = [];
                var i = 0;
                var obja = $.parseJSON(output);
                        $.each(obja, function() {

                          y++;
                          for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
                      jGet[i] = this[colx[i]];
                    }
                    strGetBox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
                    strGetBox += '<input type="hidden" value="'+jGet[0]+'" id="acccode'+jGet[0]+'">';
                    strGetBox += '<input type="hidden" value="'+jGet[1]+'" id="accdesc'+jGet[0]+'">';
                    strGetBox += '</td></tr>';
                  });
                } 
            }); 
            strGetBox += '';
            strGetBox += '</table></div>';
            strGetBox += '</div>';


          var updateTableMini = $(strGetBox);

          $( updateTableMini ).dialog({
            autoOpen: false,
            height: 300,
            width: 400,
            modal: true,
            buttons: {
            
              /*"Get Code": function() {
                var gcodex = $("#gcode-update").val();
                var gdescpx = $("#gdescp-update").val();
                $("#ad_companycode").val(gcodex);
                $("#ad_companyname").val(gdescpx);
                $(this).dialog("destroy");
              },
              Cancel: function() {
                $(this).dialog("destroy");
                $( this ).dialog( "close" );
              }*/
            }
          });

          $( updateTableMini ).dialog( "open" );


            for (j = 1; j < y+1; ++j) {  
                $('.selected_td'+j).on('click',function(){
                  var m = $(this).html();
                var v = $(this).attr('id');
                var aVal = $('#acccode'+v).val();
                var bVal = $('#accdesc'+v).val();
                $("#up_"+tocd).val(aVal);
                $("#up_"+tods).val(bVal);
                $( "#get_supplier_box" ).remove();

                });
              }
             
          $( "#keyword-insert" ).keyup(function() {

            var newacc = $('#keyword-insert').val();
             if(u == 'yes'){
              where2 = " where "+dicode+" and (code like '%"+newacc+"%' or descp like '%"+newacc+"%')   order by code";
            }else if(u == 'no'){
              where2 = " where code like '%"+newacc+"%' or descp like '%"+newacc+"%' order by code";

            }
            var exString2 = 'table='+table+'&'+strx;


            var returnsuppbox = '<table id="supplier_output">';
            //strGetBox += '<div class="ui-widget" style="margin-top:2em; font-family:Arial" align="left">Buyer Code:<br><input type="text" id="gcode-update" size="55"><br><textarea id="gdescp-update" cols="55" rows="5"></textarea></div>';
            var y = 0;
            $.ajax({
              url: "get_dataquery.jsp?sWhere="+encodeURIComponent(where2),
              type:'POST',
              async: false,
              data: exString2,
              success: function(output){
                var jGet = [];
                var i = 0;
                var obja = $.parseJSON(output);
                        $.each(obja, function() {
                          y++;
                          
                          for (i = 0; i < colx.length; ++i) {//put value from database to variable jget
                      jGet[i] = this[colx[i]];
                    }
                    returnsuppbox += '<tr><td class="selected_td'+y+'" style="cursor:hand" id="'+jGet[0]+'">'+jGet[0]+' - '+jGet[1];
                    returnsuppbox += '<input type="hidden" value="'+jGet[0]+'" id="acccode'+jGet[0]+'">';
                    returnsuppbox += '<input type="hidden" value="'+jGet[1]+'" id="accdesc'+jGet[0]+'">';
                    returnsuppbox += '</td></tr>';
                  });
                } 
            }); 
            returnsuppbox += '';
            returnsuppbox += '</table>';

            $('#supplier_output').html(returnsuppbox);

            for (j = 1; j < y+1; ++j) {  
                $('.selected_td'+j).on('click',function(){
                  var m = $(this).html();
                var v = $(this).attr('id');
                var aVal = $('#acccode'+v).val();
                var bVal = $('#accdesc'+v).val();

                $("#up_"+tocd).val(aVal);
                $("#up_"+tods).val(bVal);
                $( "#get_supplier_box" ).remove();

                });
              }
          });

        });
  
        


                


                        


                        
                  }
                 });

                              
                }

                function viewRow(tdCode) {
                     var strItem = 'cols=';
                    for (j = 0; j < updateFields.length; ++j) {//put value from database to variable jget
                      strItem+= updateFields[j]+'&cols=';
            }

             
                    var sCode = tdCode;
                    var dialogText="Value = "+sCode;

                    var exString = 'identifier='+updateID+'&'+updateID+'='+sCode+'&table='+updateTable+'&'+strItem;
                    $.ajax({
                    url: 'get_data.jsp',
                    type:'POST',
                    data: exString,
                    success: function(output_string){
                        var obj = $.parseJSON(output_string);
                        $.each(obj, function() {
                          

                              //var pd = $("<div/>").attr("id", "pageDialog");
                        //$(pd).text(jsType ).dialog("open");
                      var str = '<div id="dialog-form-print" title="'+viewBoxTitle+'"><table id="table_2" cellspacing="0">';
                      var i;
                        var jGet = [];

                      for (i = 0; i < updateFields.length; ++i) {//put value from database to variable jget
                        jGet[i] = this[updateFields[i]];

                        str += '<tr><td width="30%" class="bd_bottom" align="left">'+updateTitles[i]+'</td>';
                        str += '<td width="63%" class="bd_bottom" align="left">';
                        str += '<strong>'+jGet[i]+'</strong>';
                        str += '</td><td class="bd_bottom">&nbsp;</td></td><td class="bd_bottom">&nbsp; </td></tr>';

                            }

                            str += '</table></div>';

                            var viewTable = $(str);

                             $(viewTable).dialog({
                              autoOpen: false,
                            height: 550,
                            width: 700,
                            modal: true,

                            buttons: [{
                                            id:"btn-accept",
                                            text: "Print",
                                            click: function(e) {
                                                    //$('#dialog-form-print').jqprint();
                                                    //$("#dialog-form-print").printElement({printMode:'popup'});
                                                   // $("#dialog-form-print").printElement();
                                                   //$('#printOut').click(function(e){
                                                      var ptitle = 'Supplier Information';
                                        e.preventDefault();
                                        var w = window.open();
                                        var printOne = $('#dialog-form-print').html();
                                        w.document.write('<html><head><link href="css/table_style.css" rel="stylesheet" type="text/css" /><title>Copy Printed</title></head><body><h1>'+ptitle+'</h1><hr />' + printOne + '<hr /></body></html>');
                                        w.window.print();
                                        w.document.close();
                                        w.window.close();
                                        return false;
                                    

                                
                                            }
                                    },{
                                            id:"btn-cancel",
                                            text: "Cancel",
                                            click: function() {
                                                    $(this).dialog("destroy");
                                            }
                                    }]
                              });
    

                             
                                $(viewTable).dialog("open"); 
                          });
                  }
                 });

                              
                }

                

                function deleteRow(tdCode){
            var str = '<div id="dialog-form-update" title="'+deleteBoxTitle+'">';
                    str += 'Are you sure to delete this (';
                    str += tdCode;
                    str += ') ?</div>';
                  var deleteBox = $(str);
                  var k = 0;
                $(deleteBox).dialog({
                  autoOpen: false,
              modal: true,

              buttons: {
        
                "I'm Sure": function() {

                  $.ajax({
                    type:'POST',
                    async: false,
                    data:'id='+tdCode,
                    url:deletePage+'.jsp',
                    success:function(response) {
                      k=1;
                    }
                  });

                  if(k==1){
                    $(this).dialog("destroy");
                      successNoty( generalUsage , tdCode, generalUsage+' Deleted', 'Successful delete '+generalUsage+' ');
                      setTimeout(function(){
                      location.reload();}, 1000);
                  }
                  
                                   
                  //$(this).dialog("destroy");
                  //successNoty( generalUsage , tdCode, generalUsage+' Deleted', 'Successful delete '+generalUsage+' ');

                               
                },
                Cancel: function() {
                  $(this).dialog("destroy");
                }
              }
            });

              $(deleteBox).dialog("open"); 
          
              }



              function successNoty( nType, nIdentify, nTitle, nText){
                $.pnotify({
              title: nTitle,
              text: nText+nIdentify,
              type: 'success',
              styling: 'jqueryui',
              shadow: false,
              addClass:'customsuccess'
            });
              }

              function infoNoty( nText){
                $.pnotify({
                  text:nText,
                  styling: 'jqueryui'
                });
              }

              function errorNoty( nType, nIdentify ){
                $.pnotify({
                title: 'No Shadow Error',
                text: 'I don\'t have a shadow. (It\'s cause I\'m a vampire or something. Or is that reflections...)',
                type: 'error',
                shadow: false
            });
              }

              //--------------------------this page functions go here--------------------------------------------------------
              
              function viewBranch(tdCode, tdDesc) {
                    var strItem = 'cols=';
                    var bIDmain = 'suppcode';
                    for (j = 0; j < bFieldstoview.length; ++j) {//put value from database to variable jget
                      strItem+= bFieldstoview[j]+'&cols=';
            }
            
            var sCode = tdCode;
                    var dialogText="Value = "+sCode;

                    var exString = 'identifier='+bIDmain+'&'+bIDmain+'='+sCode+'&table='+bTable+'&'+strItem;
                    $.ajax({
                    url: 'get_data.jsp',
                    type:'POST',
                    data: exString,
                    success: function(output_string){
                        var obj = $.parseJSON(output_string);
                        var str = '<div id="dialog-view-detail" title="View Branches for '+tdDesc+' ('+tdCode+')"><table id="tablelistbranch" width="100%" cellspacing="0">';
                          str += '<tr><td class="bd_head" align="left"><strong>#</strong></td>';
                     for (j = 0; j < 2; ++j) {
                      str += '<td class="bd_head"><strong>'+bTitlestoview[j]+'</strong></td>';
                    }
                    str += '<td class="bd_head" align="left"><strong>Action</strong></td></tr>';
                      var i = 0;
                        var jGet = [];
                        $.each(obj, function() {
                              
                          i++;
                     for (j = 0; j < bFieldstoview.length; ++j) {//put value from database to variable jget
                              jGet[j] = this[bFieldstoview[j]];
                    }

                    str += '<tr><td class="bd_bottom" align="left">'+i+'</td>';
                    for (j = 0; j < 2; ++j) {
str += '<td class="bd_bottom" align="left">';
                    str += jGet[j];
                    
                    str += '</td>';
                    }
                    str += '<td class="bd_bottom">';
                    str += '<img id="vbranch'+i+'" class="'+i+'" src="image/icon/view.png" title="View" width="22" style="vertical-align:middle;cursor:pointer">&nbsp;';
                    str += '<img id="ebranch'+i+'" class="'+i+'" src="image/icon/edit_2.png" title="Edit" width="22" style="vertical-align:middle;cursor:pointer">&nbsp;';
                    str += '<img id="dbranch'+i+'" class="'+i+'" src="image/icon/delete.png" title="Delete" width="22" style="vertical-align:middle;cursor:pointer"></td></tr>';
                    for (j = 0; j < bFieldstoview.length; ++j) {
                      str += '<input type="hidden" id="hid_'+i+bFieldstoview[j]+'" value="'+jGet[j]+'">';
                    }
                  });

                        if(jGet[0] == null){
                          str += '<tr><td class="bd_bottom" align="left" colspan="4"><img src=\"image/icon/1387554186_Sign Warning.png\" width=\"16\" style=\"vertical-align:middle\">&nbsp;&nbsp;No branch registered for this supplier.</td></tr>';
                        }

                        str += '</table></div>';
                        
                        var viewTable = $(str);

                        $(viewTable).dialog({
                          autoOpen: false,
                    height: 550,
                    width: 700,
                    modal: true,
                    buttons: {
                      Close: function() {
                        $(this).dialog("destroy");
                      }
                    }
                        });

                    $(viewTable).dialog("open"); 

                     ///---------important to destroy box get
                  $('.ui-dialog-titlebar-close').on('click',function(){
                    $(viewTable).dialog("destroy");
                    $('#get_supplier_box').remove();
                  }); 

                    for (j = 1; j < i+1; ++j) {
                       
                      $('#vbranch'+j).on('click',function(){
                      var a = $(this).attr('class');
                      var viewD = '<table id="tblbranchdetail" class="table_3" cellspacing="0">';
                      viewD += '<tr><td colspan="2"><button id="backtolist">Back to List</button>&nbsp;<button id="printlist">Print</button></td></tr>';
                      for (j = 0; j < bFieldstoview.length; ++j) {
                        var newV = $('#hid_'+a+bFieldstoview[j]).val();
                        viewD += '<tr><td width="15%" class="bd_bottom" align="left">'+bTitlestoview[j]+'</td>';
                        viewD += '<td width="70%" class="bd_bottom" align="left">'+newV+'</td>';
                        viewD += '</tr>';
                      }
                      viewD += '</table>';
                      $("#tablelistbranch").hide();
                      $("#dialog-view-detail").html(viewD);

                      $( "#backtolist" ).button({
                        icons: {
                          primary: "ui-icon-circle-arrow-w"
                        }
                      })
                      .click(function() {

                        $("#dialog-view-detail").remove();
                         viewBranch(tdCode, tdDesc);
                         //alert('ss');
                      });

                      $( "#printlist" ).button({
                        icons: {
                          primary: "ui-icon-print"
                        }
                      })
                      .click(function(e) {

                        var ptitle = 'Branch Detail';
                                        e.preventDefault();
                                        var w = window.open();
                                        var printOne = $('#dialog-view-detail').html();
                                        w.document.write('<html><head><link href="css/table_style.css" rel="stylesheet" type="text/css" /><title>Copy Printed</title></head><body><h1>'+ptitle+'</h1><hr />' + printOne + '<hr /></body></html>');
                                        w.window.print();
                                        w.document.close();
                                        w.window.close();
                                        return false;
                         //alert('ss');
                      });

                    });

                    $('#ebranch'+j).on('click',function(){
                      var a = $(this).attr('class');
                      var viewD = '<table id="tblbranchdetail" cellspacing="0">';
                      viewD += '<tr><td colspan="2"><button id="backtolist">Back to List</button>&nbsp;<button id="updatebranch">Update</button></td></tr>';
                      for (j = 0; j < bFieldstoview.length; ++j) {
                        var newV = $('#hid_'+a+bFieldstoview[j]).val();
                        viewD += '<tr><td width="15%" class="bd_bottom" align="left">'+bTitlestoview[j]+'</td>';
                        viewD += '<td width="70%" class="bd_bottom" align="left">';

                        if(bFieldtype[j] == "text"){

                        viewD += '<input style="font-size:11px" id="ed_'+bFieldstoview[j]+'" '+bFieldparam[j]+' '+bFieldbtparam[j]+' type="text" value="'+newV+'"  />';
                      
                      
                        }
                        viewD += '</td>';
                        viewD += '</tr>';
                      }
                      viewD += '</table>';
                      $("#tablelistbranch").hide();
                      $("#dialog-view-detail").html(viewD);

                      $( "#backtolist" ).button({
                        icons: {
                          primary: "ui-icon-circle-arrow-w"
                        }
                      })
                      .click(function() {
                        
                        $("#dialog-view-detail").remove();
                         viewBranch(tdCode, tdDesc);
                         //alert('ss');
                      });

                      $( "#updatebranch" ).button({
                        icons: {
                          primary: "ui-icon-disk"
                        }
                      })
                      .click(function() {
                        var upTitle = [];
                        var dataStringUp = "";

                        for (j = 0; j < bFieldstoview.length; ++j) {//put value from database to variable jget
                                upTitle[j] = $('#ed_'+bFieldstoview[j]).val();

                          if(updateDesc == bFieldstoview[j]){
                                    nDesc = $('#up_'+bFieldstoview[j]).val();
                                }
                                      
                                if(j == bFieldstoview.length-1){
                                    dataStringUp += bFieldstoview[j]+'='+upTitle[j];
                                }else{
                                    dataStringUp += bFieldstoview[j]+'='+upTitle[j]+'&';
                                }
                                      
                        }
                             //alert(dataStringUp);
                        $.ajax({
                            type:'POST',
                            data:dataStringUp,
                            url:beditPage+'.jsp',
                            success:function(response) {
                                $("#dialog-view-detail").remove();
                                viewBranch(tdCode, tdDesc);
                            }
                        });
                        
                        
                      });

                    });

                    $('#dbranch'+j).on('click',function(){
                      var a = $(this).attr('class');
                      var bCode = $('#hid_'+a+bFieldstoview[0]).val();
                      var bDesc = $('#hid_'+a+bFieldstoview[1]).val();
                      var str = '<div id="dialog-form-update" title="Delete Branch Code">';
                              str += 'Are you sure to delete this (';
                              str += bDesc;
                              str += ') ?</div>';
                            var deleteBox = $(str);
                            var k = 0;
                          $(deleteBox).dialog({
                            autoOpen: false,
                        modal: true,

                        buttons: {
                  
                          "I'm Sure": function() {

                            $.ajax({
                              type:'POST',
                              async: false,
                              data:'id='+bCode,
                              url:bdeletePage+'.jsp',
                              success:function(response) {
                                k=1;
                              }
                            });

                            if(k==1){
                              $(this).dialog("destroy");
                                //successNoty( generalUsage , tdCode, generalUsage+' Deleted', 'Successful delete '+generalUsage+' ');
                                $('#dialog-view-detail').remove();
                                viewBranch(tdCode, tdDesc)
                            }
                            
                                             
                            //$(this).dialog("destroy");
                            //successNoty( generalUsage , tdCode, generalUsage+' Deleted', 'Successful delete '+generalUsage+' ');

                                         
                          },
                          Cancel: function() {
                            $(this).dialog("destroy");
                          }
                        }
                      });

                        $(deleteBox).dialog("open"); 

                    });
                  }
              }
                });

            



                              
                }

                




} );

      
    </script>



<link rel="stylesheet" href="jquery-ui-1.10.3.custom/development-bundle/themes/greenbox/jquery.ui.all.css">
<script src="jquery-ui-1.10.3.custom/development-bundle/ui/jquery.ui.core.js"></script>
<script src="jquery-ui-1.10.3.custom/development-bundle/ui/jquery.ui.widget.js"></script>
<script src="jquery-ui-1.10.3.custom/development-bundle/ui/jquery.ui.button.js"></script>
<link rel="stylesheet" href="jquery-ui-1.10.3.custom/development-bundle/demos/demos.css">
<script src="jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.js"></script>
<script src="dtables/myscript.js"></script>

<script src="pnotify-1.2.0/jquery.pnotify.js"></script>
<script src="pnotify-1.2.0/jquery.pnotify.min.js"></script>
<link rel="stylesheet" href="pnotify-1.2.0/jquery.pnotify.default.css">
<link rel="stylesheet" href="pnotify-1.2.0/jquery.pnotify.default.css">
<link rel="stylesheet" href="pnotify-1.2.0/jquery.pnotify.default.icons.css">

<script>
  $(document).ready(function() {
    $( "#button_add" ).button({
      icons: {
        primary: "ui-icon-circle-plus"
        
        
      }
      
    });
    $( "#button_tree" ).button({
      icons: {
        primary: "ui-icon-bookmark"
        
        
      }
      
    });
    $( "#button_all" ).button({
      icons: {
        primary: "ui-icon-print"
        
        
      }
      
    });
    
    $("#button_add").click(function(){
      addAcc();
    });
    
    $("#button_tree").click(function(){
      viewTree();
    });
    /*.next().button({
      icons: {
        primary: "ui-icon-gear",
        secondary: "ui-icon-triangle-1-s"
      }
    }).next().button({
      icons: {
        primary: "ui-icon-gear",
        secondary: "ui-icon-triangle-1-s"
      },
      text: false
    });*/


  });
  </script>




<link href="css/table_style.css" rel="stylesheet" type="text/css" />


<body>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="74%" class="borderbot" align="left"><span class="bigfonttitle"><%=title%> Information</span></td>
        <td width="26%" class="borderbot" align="right">&nbsp;</td>
      </tr>
  </table>
    <br>
  <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
        <tr>
            <td valign="middle" align="left">
                <button id="add-new"><%=add_button%></button>
            </td>
        </tr>
  </table>
  <br>
  <div class="tableContainer"> 
      <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
          <thead>
              <tr>
                <%
                for (int i = 0; i < columntoView.length; i++) {
                %>
                  <th width="<%=columntoViewwidth%>%"></th>
                  <%
                }
                  %>
              </tr>
          </thead>
          <tbody>
              <tr>
                  <td colspan="<%=columntoView.length%>" class="dataTables_empty">Please Wait..</td>
              </tr>
          </tbody>
        </table>
    </div>
</body>
</html>
  