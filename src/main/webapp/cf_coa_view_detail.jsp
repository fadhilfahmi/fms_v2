<%-- 
    Document   : cf_coa_view_list
    Created on : Jan 19, 2017, 4:30:39 PM
    Author     : fadhilfahmi
--%>

<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.model.setup.configuration.ChartofAccount"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ChartofAccountDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    ChartofAccount v = (ChartofAccount) ChartofAccountDAO.getInfo(log, request.getParameter("refer"));
    Module mod = (Module) ChartofAccountDAO.getModule();
%>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('.activerowy').click(function (e) {
            //console.log(oData);

            var code = $(this).attr('id');
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'View <%= mod.getModuleDesc()%>',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('cf_coa_view.jsp?id=' + code);
                    //$('body').on('click', '.thisresult_nd', function(event){
                    //    dialog.close();
                    //    event.preventDefault();
                    //});

                    return $content;
                }
            });
            return false;
        });

        $('.editcoa').click(function (e) {

            var code = $(this).attr('id');
            var parent = $(this).attr('name');

            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=edit&referenceno=" + code + "&from=viewdetail&parent=" + parent,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });
    });

</script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<div class="row">
    <div class="form-group pull-left">
        <div class="col-md-12">                                                                                                                        
            <div class="btn-group">
                <button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>

            </div>      
        </div>
    </div> 
</div>

<br>
<div class="row">
    <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-body">
            <table class="table table-bordered table-striped  table-hover">
                <thead>
                    <tr>
                        <th colspan="4">Account Information
                            <!--<small><span class="label label-primary">Preparing</span></small>-->
                           <div class="btn-group btn-group-sm pull-right">


                                <button id="<%= v.getCode()%>" class="btn btn-default viewvoucher" title="<%= mod.getModuleID()%>" name="viewjv"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
                                <button id="<%= v.getCode()%>" class="btn btn-default editmain" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</button>
                                <button id="<%= v.getCode()%>"  class="btn btn-danger deletemain" title="<%= mod.getModuleID()%>" id="<%//= pvi.getRefer() %>" name="delete_st"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody class="activerowm tdrow">
                    <tr>
                        <th width="15%">Code</th>
                        <td width="35%"><%= v.getCode()%></td>
                        <th width="15%">Description</th>
                        <td width="35%"><%= v.getDescp()%></td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td><%= v.getActive()%></td>
                        <th>Apportion Type</th>
                        <td><%= v.getApptype()%></td>
                    </tr>
                    <tr>
                        <th>Final Level</th>
                        <td><%= v.getFinallvl()%></td>
                        <th>Apportion Level</th>
                        <td><%= v.getApplevel()%></td>
                    </tr>

                </tbody>
            </table>

            <table class="table table-bordered table-striped table-hover" id="dataTables">
                <thead>
                    <tr>
                        <th colspan="6">Detail<!--<small>asdada</small>-->
                            <span class="pull-right">

                            </span>
                        </th>
                    </tr>
                    <tr>
                        <th>Code</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Final Level</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <%List<ChartofAccount> listAll = (List<ChartofAccount>) ChartofAccountDAO.getAllCode(log, v.getCode());
                    if (listAll.isEmpty()) {%>
                <tbody>
                    <tr>
                        <td colspan="5">
                            <span class="font-small-red">No data available.</span>
                        </td>
                    </tr>
                    <%}
                        for (ChartofAccount j : listAll) {%>
                    <tr class="activerowy" id="<%= j.getCode()%>">
                        <td class="tdrow">&nbsp;<%= j.getCode()%></td>
                        <td class="tdrow">&nbsp;<%= j.getDescp()%></td>
                        <td class="tdrow">&nbsp;<%= j.getActive()%></td>
                        <td class="tdrow" align="right">&nbsp;<%= j.getFinallvl()%></td>
                        <td class="tdrow" align="right">&nbsp;
                            
                                <button id="<%= j.getCode()%>" class="btn btn-warning btn-xs editcoa" title="<%= mod.getModuleID()%>" name="<%= v.getCode()%>">Edit</button>
                                <button id="<%= j.getCode()%>" class="btn btn-danger btn-xs deleteitem" title="<%= mod.getModuleID()%>"  name="delete_st">Delete</button>
                        </td>
                    </tr>
                </tbody>
                <%}%>
            </table>
            </div>
        </div>
    </div>
</div>    