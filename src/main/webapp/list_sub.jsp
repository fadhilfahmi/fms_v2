<%-- 
    Document   : list_sub
    Created on : Mar 2, 2016, 12:36:40 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
            
             $.ajax({
                        url: "list_parameter.jsp?type=Sub-Account Type",
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
            }});
            
            $('#keyword').keyup(function(){
                //var l = $('input[name=searchby]:checked').val();
                var keyword = $(this).val();
                console.log(keyword);
                
                var level = $('#searchlevel').val();
                var url = '';
                var by = $('#satype').val();
                if(level=='first'){
                    url = "list_parameter.jsp?type=Sub-Account Type&keyword="+keyword;
                }else if(level=='second'){
                    url = "list_sub_by.jsp?by="+by+"&keyword="+keyword;
                }
                 $.ajax({
                        url: url,
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
                }});
            });
            
            
            
            $('#result').on('click', '.thisresult', function(e) { 
                var a = $(this).attr('href');
                var b = $(this).attr('id');
                
                $('#satype').val(b);
                $('#searchlevel').val('second');
                $.ajax({
                        url: "list_sub_by.jsp?by="+b,
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
                }});
                e.preventDefault();
            });
            
            $('#result').on('click', '.thisresult_nd', function(e) { 
                var a = $(this).attr('href');
                var b = $(this).attr('id');
                
                $('#sacode').val(b);
                $('#sadesc').val(a);
                $.ajax({
                        url: "list_location_by.jsp?by="+b,
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
                }});
                e.preventDefault();
            });
            
            $('#result').on('click', '#keyword', function(e) { 
                //var l = $('input[name=searchby]:checked').val();
                var keyword = $(this).val();
                console.log(keyword);
                 $.ajax({
                        url: "list_location_by.jsp?table="+table+"&keyword="+keyword,
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
                }});
            });
            
        });
        </script>
    </head>
    <body>
         
        
        <div class="form-group">
            <div class="form-group input-group">
                <input type="text" class="form-control" id="keyword">
                <input type="hidden" class="form-control" id="searchlevel" value="first">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
        
        <div id="result">
       
        </div>
        
            
    </body>
</html>
