<%-- 
    Document   : cf_portfolio_add
    Created on : Dec 15, 2016, 10:55:37 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.setup.configuration.PortfolioDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) PortfolioDAO.getModule();

%>
        <script src="jsfunction/jquery-1.12.0.min.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/formValidation.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/bootstrap.js"></script>

        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {

                /*$('#saveform').formValidation({
                 message: 'This value is not valid',
                 icon: {
                 valid: 'glyphicon glyphicon-ok',
                 invalid: 'glyphicon glyphicon-remove',
                 validating: 'fa fa-refresh'
                 },
                 fields: {
                 
                 code: {
                 required:true,
                 validators: {
                 notEmpty: {
                 message: 'The Account Code is required'
                 },
                 digits: {
                 message: 'The value can contain only digits'
                 }
                 }
                 }
                 }
                 });*/


                $('.getbank').click(function (e) {

                    var a = $(this).attr('id');
                    var b = $(this).attr('id1');
                    //e.preventDefault();
                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_DEFAULT,
                        title: 'Get Taxation',
                        //message: $('<body></body>').load('list_coa.jsp')
                        message: function (dialog) {
                            var $content = $('<body></body>').load('list_branch.jsp?code=' + a + '&name=' + b);
                            $('body').on('click', '.thisresult', function (event) {
                                dialog.close();
                                event.preventDefault();
                            });

                            return $content;
                        }
                    });

                    return false;
                });


            });

        </script>
        <div id ="maincontainer">
            <div class="partition"> 
                <div class="bodyofpartition">
                    <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                        <tr>
                            <td valign="middle" align="left">
                                <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist" type="<%//= v.getCode() %>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                                <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                            </td>
                        </tr>
                    </table>
                    <br>

                    <div class="well">

                        <form data-toggle="validator" role="form" id="saveform">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Portfolio Info &nbsp&nbsp<span id="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Auto Generated (Code)" autocomplete="off" readonly required>
                                        <p></p>
                                        <input type="text" class="form-control input-sm" id="descp" name="descp" placeholder="Description" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group" id="div_code">
                                        <label for="inputName" class="control-label">Register No<span id="res_code"></span></label>
                                        <input type="text" class="form-control input-sm" id="coregister" name="coregister" autocomplete="off" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-sm">
                                        <div class="form-group" id="div_code">
                                            <label for="inputName" class="control-label">Account&nbsp&nbsp<span class="res_code"></span></label>
                                            <div class="form-group-sm input-group">
                                                <input type="text" class="form-control input-sm" id="coa" name="coa">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default btn-sm getaccount" type="button" id="coa" id1="coadescp"><i class="fa fa-cog"></i> Account</button>
                                                </span>
                                            </div>  
                                        </div>
                                        <div class="form-group" id="div_code">
                                            <input type="text" class="form-control input-sm" id="coadescp" name="coadescp" placeholder="" autocomplete="off" > 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Address</label>
                                        <textarea class="form-control" rows="4"  id="address" name="address"></textarea>
                                        <p></p>
                                        <input type="text" class="form-control input-sm" id="postcode" name="postcode" placeholder="Postcode">
                                        <p></p>
                                        <input type="text" class="form-control input-sm" id="city" name="city" placeholder="City">
                                        <p></p>
                                        <input type="text" class="form-control input-sm" id="state" name="state" placeholder="State">
                                        <p></p>
                                        <input type="text" class="form-control input-sm" id="country" name="country" placeholder="Country">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-sm">
                                        <label for="inputName" class="control-label">Contact No.</label>
                                        <div class="form-group" id="div_code">
                                            <input type="text" class="form-control input-sm" id="phone" name="phone" placeholder="Phone Number" autocomplete="off" > 
                                            <p></p>
                                            <input type="text" class="form-control input-sm" id="fax" name="fax" placeholder="Fax Number">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-sm">
                                        <label for="inputName" class="control-label">GST ID</label>
                                        <div class="form-group" id="div_code">
                                            <input type="text" class="form-control input-sm" id="gstid" name="gstid" placeholder="" autocomplete="off" > 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-sm">
                                        <label for="inputName" class="control-label">Active?</label>
                                        <select class="form-control input-sm" id="active" name="active">
                                            <%= ParameterDAO.parameterList(log,"YesNo Type", "")%>
                                        </select>
                                    </div>
                                </div>
                            </div>            
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Remark</label>
                                        <textarea class="form-control" rows="4"  id="remarks" name="remarks"></textarea>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
