<%-- 
    Document   : cb_pv_view_voucher
    Created on : May 9, 2016, 10:01:22 AM
    Author     : user
--%>

<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.dao.financial.ap.VendorPaymentDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.ext.Restriction"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucher"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.PaymentVoucherDAO"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.CashVoucher"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.CashVoucherDAO"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.CashVoucherAccount"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.dao.LocationDAO"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucher"%>
<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucherItem"%>
<%@page import="com.lcsb.fms.dao.financial.gl.JournalVoucherDAO"%>
<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    PaymentVoucherItem vi = (PaymentVoucherItem) PaymentVoucherDAO.getPYVitem(log,request.getParameter("voucer"));
    PaymentVoucher v = (PaymentVoucher) PaymentVoucherDAO.getPYV(log,request.getParameter("refer"));
    Module mod = (Module) PaymentVoucherDAO.getModule();


%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/pvoucher.css?1" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {

                $('#print').click(function (e) {
                    //var printContents = document.getElementById("viewjv").innerHTML;
                    //var originalContents = document.body.innerHTML;

                    //document.body.innerHTML = printContents;

                    //window.print();

                    //document.body.innerHTML = originalContents;

                    printElement($("#viewjv").html());

                });

                $('.action').click(function (e) {
                    var action = $(this).attr('id');

                    $.ajax({
                        url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + action + "&refer=<%= v.getRefer()%>",
                        success: function (result) {
                            // $("#haha").html(result);
                            $('#herey').empty().html(result).hide().fadeIn(300);
                        }});

                    return false;

                });

                $('.action_2').click(function (e) {
                    var action = $(this).attr('id');

                    BootstrapDialog.confirm({
                        title: 'Confirmation',
                        message: 'Are you sure to ' + action + ' this?',
                        type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                        closable: true, // <-- Default value is false
                        draggable: true, // <-- Default value is false
                        btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                        btnOKLabel: 'Delete', // <-- Default value is 'OK',
                        btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                        callback: function (result) {
                            // result will be true if button was click, while it will be false if users close the dialog directly.
                            if (result) {
                                $.ajax({
                                    url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + action + "&refer=<%= v.getRefer()%>",
                                    success: function (result) {
                                        $('#herey').empty().html(result).hide().fadeIn(300);
                                    }
                                });
                            }
                        }
                    });

                });
            });
        </script>


    </head>
    <body>



        <div id ="maincontainer">
            
            <div class="partition"> 
                <div class="headofpartition"></div>
                <div class="bodyofpartition">
                    <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                        <tr>
                            <td valign="middle" align="left">


                            </td>
                        </tr>
                    </table>

                    <table id="table_left" width="100%" cellspacing="0" border="0">
                        <tr>
                            <td valign="middle" width="33%" align="left">
                                <div class="btn-group btn-group-sm" role="group" aria-label="...">


                                    <div class="btn-group" role="group" aria-label="...">
                                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i></button>
                                        <button id="print" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-print" aria-hidden="true"></i> Print</button>




                                    </div>
                                </div>
                                <span>&nbsp;&nbsp;<strong><%= v.getRefer()%></strong> | Printable Page : 3</span>
                            </td>
                            <td width="60%" style="vertical-align: middle;">
                                <!--<div class="form-group" id="div_code">
                                    
                                    <select class="form-control input-xs" id="reflexcb" name="reflexcb">
                                        <%//= ParameterDAO.parameterList("YesNo Type", "No")%>
                                    </select>   
                                </div>-->
                            </td>
                        </tr>
                    </table>
                    <table id="maintbl" width="100%" cellspacing="0" border="0">
                        <tr>
                            <td class="greybg">
                                <table id="table_left" width="100%" cellspacing="0">
                                    <tr>
                                        <td width="100%">
                                            <div id="viewjv" style="overflow-y: scroll; height:600px;">
                                                <%//= PaymentVoucher_Format.printPV(v, vi, log)%>
                                            </div>
                                </table>
                            </td>
                        </tr>
                        <input type="hidden" id="checkbyid" value="">
                        <input type="hidden" id="checkbyname" value="">
                        <input type="hidden" id="checkbydesign" value="">
                        <input type="hidden" id="checkdate" value="">
                    </table>


                </div>
            </div>
        </div>
    </body>
</html>
