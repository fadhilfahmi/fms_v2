<%-- 
    Document   : em_payroll_getearning
    Created on : Jun 7, 2017, 8:52:15 AM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.model.financial.gl.AccountPeriod"%>
<%@page import="com.lcsb.fms.dao.financial.gl.AccountPeriodDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    AccountPeriod cur = (AccountPeriod) AccountPeriodDAO.getCurPeriod(log);
    AccountPeriod pre = (AccountPeriod) AccountPeriodDAO.getPreviousPeriod(log);

    String type = request.getParameter("type");
    String table = "";

    if (type.equals("Earning")) {
        table = "cf_earninginfo";
    } else if (type.equals("Deduction")) {
        table = "cf_deductioninfo";
    }
%>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        var s = $('input[name=searchby]:checked').val();
        var t = $('input[name=method]:checked').val();
        $.ajax({
            url: "list_search.jsp?table=<%=table%>&column=" + s + "&keyword=&method=" + t + "&code=code&descp=descp&special=<%= request.getParameter("special")%>",
            success: function (result) {
                $('#result').empty().html(result).hide().fadeIn(300);

            }});

        $('#keyword').keyup(function () {
            var l = $('input[name=searchby]:checked').val();
            var m = $('input[name=method]:checked').val();

            var keyword = $(this).val();
            console.log(keyword);
            $.ajax({
                url: "list_search.jsp?table=<%=table%>&column=" + l + "&keyword=" + keyword + "&method=" + m + "&code=code&descp=descp&special=<%= request.getParameter("special")%>",
                success: function (result) {
                    $('#result').empty().html(result).hide().fadeIn(300);

                }});
        });

        $('#result').on('click', '.thisresult', function (e) {
            var a = $(this).attr('href');
            var b = $(this).attr('id');

            $('#<%= request.getParameter("code")%>').val(a);
            $('#<%= request.getParameter("descp")%>').val(b);
            e.preventDefault();
        });




    });
</script>
<div class="well">
    <form>
        <input type="hidden" id="code" value="">
        <input type="hidden" id="descp" value="">
        <div class="row">
            <div class="col-sm-12">
                <label for="year" class="control-label input-group">Refine Your Search</label>
                <div class = "btn-toolbar" role = "toolbar">

                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default btn-sm active">
                            <input name="searchby" id="searchby" value="code" type="radio" checked>Code
                        </label>
                        <label class="btn btn-default btn-sm">
                            <input name="searchby" id="searchby" value="descp" type="radio">Description
                        </label>
                    </div>
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default btn-sm active">
                            <input name="method" id="method" value="start" type="radio" checked>Start
                        </label>
                        <label class="btn btn-default btn-sm">
                            <input name="method" id="method" value="contain" type="radio">Containing
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="row">&nbsp;</div>
    <div class="form-group">
        <div class="form-group input-group">
            <input type="text" class="form-control" id="keyword" autofocus>
            <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                </button>
            </span>
        </div>
    </div>

    <div id="result">

    </div>
    <div id="buttonhere">
        <button class="btn btn-default btn-sm gotocoa" type="button"><i class="fa fa-cog"></i>
            <%=type%> Information
        </button>
    </div>

</div>  