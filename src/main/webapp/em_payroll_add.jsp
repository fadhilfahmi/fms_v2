<%-- 
    Document   : em_payroll_add
    Created on : Jun 5, 2017, 3:31:52 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.financial.employee.EmPayrollDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) EmPayrollDAO.getModule();

%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/formValidation.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {




    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist" type="<%//= v.getCode() %>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addpayroll"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>

            <div class="well">

                <form data-toggle="validator" role="form" id="saveform">
                    
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Year<span id="res_code"></span></label>
                                <select class="form-control input-sm" id="year" name="year">
                                    <%= ParameterDAO.parameterList(log, "Year", AccountingPeriod.getCurYear(log))%>
                            </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Period<span id="res_code"></span></label>
                                <select class="form-control input-sm" id="period" name="period">
                                    <%= ParameterDAO.parameterList(log, "Period", AccountingPeriod.getCurPeriod(log))%>
                            </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
