<%-- 
    Document   : list_search
    Created on : Mar 1, 2016, 10:16:44 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.SearchDAO"%>
<%@page import="com.lcsb.fms.util.model.Search"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
 <%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
 
     String table = request.getParameter("table");
     String column = request.getParameter("column");
     String keyword = request.getParameter("keyword");
     String method = request.getParameter("method");
     String code = request.getParameter("code");
     String descp = request.getParameter("descp");
     String special = request.getParameter("special");
     
     %><div class="list-group"><%
            List<Search> slist = (List<Search>) SearchDAO.getAllResult(log, table,column, keyword, method,code, descp, special);
            if(slist.isEmpty()){
               %>
       
                <div class="alert alert-danger" role="alert">
                    No matching search criteria were found for <strong><%= keyword %></strong>! 
                </div>

               <%   
            }
            for (Search c : slist) { 
               %>
       
            <a href="<%= c.getCode() %>" id="<%= c.getDecsp() %>" class="list-group-item thisresult">
                <div id="left_div"><%= c.getCode() %></div>
                <div id="right_div"><%= c.getDecsp() %></div>
            </a>

               <%    
            } 
        %>
</div>