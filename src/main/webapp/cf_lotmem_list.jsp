<%-- 
    Document   : cf_lotmem_list
    Created on : Apr 22, 2016, 11:29:39 AM
    Author     : user
--%>

<%@page import="com.lcsb.fms.dao.setup.configuration.LotMemberDAO"%>
<%@page import="com.lcsb.fms.util.model.ListTable"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.ModuleItem"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.ConnectionModel"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,java.util.*" errorPage="" isThreadSafe="false" %>
<%@ page import="java.util.Date"%>
<%
    Module mod = (Module) LotMemberDAO.getModule();

    response.setHeader("Cache-Control", "no-cache");
%>

<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/tether.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>

<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        var updateTable = '<%= mod.getMainTable()%>';

    <%
        List<ListTable> mlist = (List<ListTable>) LotMemberDAO.tableList();
        for (ListTable m : mlist) {
                        //if(m.getList_View().equals        ("1")){
%>
        columntoView.push('<%= m.getColumnName()%>');
        columnName.push('<%= m.getTitleName()%>');
    <%
            //}

        }
    %>

        var colsTosend = 'cols=';
        for (m = 0; m < columntoView.length; ++m) {//put value from database to variable jget
            if (m == columntoView.length - 1) {
                colsTosend += columntoView[m];
            } else {
                colsTosend += columntoView[m] + '&cols=';
            }
        }


        $("#add-new").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addnew",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });

        $("#viewall").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewall",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        var orderby = 'order by voucherid desc';
        var oTable = $('#example').DataTable({
            "aaSorting": [[1, "asc"]],
            "responsive": true,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "retrievetable.jsp?table=" + updateTable + "&code=<%= mod.getReferID_Master()%>&" + colsTosend + "&order=" + orderby,
            "aoColumns": [
                {"sTitle": columnName[0], "mData": columntoView[0], "sWidth": "15%", "bSearchable": true},
                {"sTitle": columnName[1], "mData": columntoView[1], "sWidth": "27%"},
                {"sTitle": columnName[2], "mData": columntoView[2], "sWidth": "15%"},
                {"sTitle": columnName[3], "mData": columntoView[3], "sWidth": "10%"},
                {"sTitle": "Action", "mData": null, "bSortable": false, "bSearchable": false, "sWidth": "13%"}

            ],
            "aoColumnDefs": [{
                    "aTargets": [4],
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {



                        var c = $(icon('edit', oData.lockedby, oData.recon_refno, ''));
                        var e = $(icon('delete', oData.lockedby, oData.recon_refno, ''));
                        var f = $(icon('post', oData.lockedby, oData.recon_refno, ''));
                        var a = $('<button type="button" class="btn btn-default btn-xs"><i class="fa fa-plus-circle"></i></button>');
                        var b = $('<button type="button" class="btn btn-default btn-xs"><i class="fa fa-folder-open"></i></button>');
                        var k = $(icon('check', oData.lockedby, oData.recon_refno, ''));
                        var g = $(icon('cancel', oData.lockedby, oData.recon_refno, ''));

                        /*a.on('click', function() {
                         //console.log(oData);
                         if(iconClick('edit',oData.checkid,oData.appid,oData.post            )==0){
                         //addSubform(oData.JVrefno,subTable,'<%= mod.getModuleID()%>',module_abb,oTable);
                         //addDebitCredit(oData.JVrefno);
                         
                         $.ajax({
                         url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addnewitem&referno="+oData.refer,
                         success: function (result) {
                         // $("#haha").html(result);
                         $('#herey').empty().html(result).hide().fadeIn(300);
                         }});
                         return false;
                         }
                         
                         return false;
                         });*/
                        b.on('click', function () {
                            //console.log(oData);
                            $.ajax({
                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewor&refer=" + oData.mem_Code,
                                success: function (result) {
                                    // $("#haha").html(result);
                                    $('#herey').empty().html(result).hide().fadeIn(300);
                                }});
                            return false;
                        });
                        c.on('click', function () {
                            //console.log(oData);
                            if (iconClick('edit', oData.lockedby, oData.recon_refno, '') == 0) {
                                //editRowx(oData.JVrefno,updateTable,subTable,<%= mod.getModuleID()%>,module_abb,oTable);

                                $.ajax({
                                    url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editlist&referno=" + oData.mem_Code,
                                    success: function (result) {
                                        // $("#haha").html(result);
                                        $('#herey').empty().html(result).hide().fadeIn(300);
                                    }});
                                return false;
                            }

                            //$("#dialog-form-update").dialog("open");
                            //$( "#dialog-form-update" ).dialog( "open" );
                            return false;
                        });
                        e.on('click', function () {
                            //console.log(oData);
                            if (iconClick('delete', oData.lockedby, oData.recon_refno, '') == 0) {
                                //deleteRow(oData.JVrefno);

                                BootstrapDialog.confirm({
                                    title: 'Confirmation',
                                    message: 'Are you sure to delete?',
                                    type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                    closable: true, // <-- Default value is false
                                    draggable: true, // <-- Default value is false
                                    btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                    btnOKLabel: 'Delete', // <-- Default value is 'OK',
                                    btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                                    callback: function (result) {
                                        // result will be true if button was click, while it will be false if users close the dialog directly.
                                        if (result) {
                                            $.ajax({
                                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&referno=" + oData.refno,
                                                success: function (result) {
                                                    // $("#haha").html(result);
                                                    setTimeout(function () {
                                                        oTable.draw();
                                                    }, 500);
                                                }});
                                        }
                                    }
                                });


                                return false;
                            }

                            return false;
                        });






                        $(nTd).empty();
                        $(nTd).attr("class", 'btn-group');
                        $(nTd).attr("align", 'right');
                        $(nTd).prepend(a, b, c, e);
                    }

                }]



        });

        $('#example tbody').on('click', 'tr', function () {
            var data = oTable.row(this).data();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editlist&referno=" + data.mem_Code,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });



    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="add-new" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> Add <%= mod.getModuleDesc()%></button>
                        <button id="viewall" class="btn btn-default btn-sm refresh"><i class="fa fa-refresh"></i> View All</button>
                        <button id="refresh" class="btn btn-default btn-sm refresh"><i class="fa fa-refresh"></i> Refresh</button>

                        <!--<button id="add-debitnote" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> From Sales Debit Note</button>-->
                    </td>
                </tr>
            </table>
            <hr style="color:#333; border:thick">       
            <div class="dataTable_wrapper"> 
                <table class="table table-striped table-bordered table-hover" id="example">
                </table>
            </div>

        </div>
    </div>
</div>