<%-- 
    Document   : report_pdf_generator
    Created on : Mar 2, 2017, 4:55:18 PM
    Author     : fadhilfahmi
--%>

<%-- 
    Document   : htmltopdf
    Created on : Aug 17, 2016, 5:18:48 PM
    Author     : eksm
--%>
<%@page import="org.jsoup.Jsoup"%>
<%@page import="java.io.UnsupportedEncodingException"%>
<%@page import="javax.xml.transform.OutputKeys"%>
<%@page import="javax.xml.transform.Transformer"%>
<%@page import="javax.xml.transform.TransformerFactory"%>
<%@page import="javax.xml.transform.stream.StreamResult"%>
<%@page import="java.io.StringWriter"%>
<%@page import="javax.xml.transform.dom.DOMSource"%>
<%@page import="javax.xml.transform.TransformerException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.CharArrayWriter"%>
<%@page import="java.io.OutputStream"%>
<%@page import="org.xhtmlrenderer.pdf.ITextRenderer"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="com.lowagie.text.DocumentException"%>
<%@page import="org.xml.sax.SAXException"%>
<%@page import="javax.xml.parsers.ParserConfigurationException"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ page contentType="application/pdf" language="java" errorPage="" %>
<%!
    public String getStringFromDoc(Document doc) throws TransformerException {

        DOMSource domSource = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(domSource, result);
        writer.flush();
        return writer.toString();
    }

    public class CharArrayWriterResponse extends HttpServletResponseWrapper {

        private final CharArrayWriter charArray = new CharArrayWriter();

        public CharArrayWriterResponse(HttpServletResponse response) {
            super(response);
        }

        @Override
        public PrintWriter getWriter() throws IOException {
            return new PrintWriter(charArray);
        }

        public String getOutput() {
            return charArray.toString();
        }
    }

    private byte[] getHTML() throws UnsupportedEncodingException {
        StringBuffer buf = new StringBuffer();
        buf.append("<html>");
        buf.append("<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/><title>JSP Page</title></head>");
        buf.append("<body><h1>Hello World!</h1>");
        buf.append("</body></html>");

        return buf.toString().getBytes("ISO-8859-1");
    }

    private String toXHTML(String html) {
        final org.jsoup.nodes.Document document = Jsoup.parse(html.replace("&nbsp;", " ").replace("\u00a0", " "));
        document.select("script").remove();
        document.outputSettings().syntax(org.jsoup.nodes.Document.OutputSettings.Syntax.xml);
        return document.html();
    }

    protected void streamPDf(HttpServletRequest request, HttpServletResponse response, String url) throws ServletException, IOException,
            SAXException, ParserConfigurationException, TransformerException, DocumentException {

        // custom response write: writes the processed JSP to an HTML String
        // see : http://valotas.com/get-output-of-jsp-or-servlet-response/
        CharArrayWriterResponse customResponse = new CharArrayWriterResponse(response);
        //request.getRequestDispatcher("/pages/output.jsp").forward(request, customResponse);
        request.getRequestDispatcher(url).forward(request, customResponse);
        String html = toXHTML(customResponse.getOutput());
        //System.out.println(html);
        byte[] data = html.getBytes();

        // send the generated HTML to XhtmlRenderer
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = builder.parse(new ByteArrayInputStream(data));

//Document doc = builder.parse(new ByteArrayInputStream(getHTML()));
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocument(doc, request.getRequestURL().toString());
        //System.out.println(getStringFromDoc(renderer.getDocument()));
        //System.out.println(" OK");
        renderer.layout();

        // stream the generated PDF to the client
        OutputStream out = response.getOutputStream();
        response.setContentType("application/pdf");
        //response.setHeader("Content-disposition", "attachment; filename=export.pdf");
        //response.setContentLength(data.length);

        //renderer.createPDF(out);

            renderer.createPDF(out);


        out.flush();
        out.close();
        return;
    }
%>
<%
    String url;

    //url = "/gl_fr_tb.jsp?year=2016&period=2&otype=3&vby=3&loccode2=0901&vall=No&view=true";
    url = request.getParameter("report").concat("?").concat(request.getQueryString());
    streamPDf(request, response, url);

%>