<%-- 
    Document   : ap_pv_add_item
    Created on : May 10, 2016, 3:53:33 PM
    Author     : user
--%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.PaymentVoucherDAO"%>
<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.dao.financial.ap.VendorPaymentDAO"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucher"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem"%>

<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    PaymentVoucherItem v = (PaymentVoucherItem) VendorPaymentDAO.getItem(request.getParameter("refer"));
    PaymentVoucher vm = (PaymentVoucher) VendorPaymentDAO.getPYV(log, request.getParameter("refer"));
    Module mod = (Module) PaymentVoucherDAO.getModule();
%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        

        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getsub').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Sub Account Detail',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_sub.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#gettax').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Taxation',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_tax.jsp');
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        var a = $('#amount').val();
                        a = a.replace(/\,/g, '');
                        var b = $('#taxrate').val();
                        var c = parseFloat(b) * parseFloat(a) / 100;
                        $('#taxamt').val(parseFloat(c).toFixed(2));


                        event.preventDefault();
                    });

                    return $content;
                }
            });



            return false;
        });

        $('.calculate').keyup(function (e) {
            var todeduct = $(this).val();
            var theval = $(this).attr('id');


            if (todeduct != '') {
                var tot = 0;
                if (theval == 'debit') {
                    $('#credit').val(0.0);
                }
                if (theval == 'credit') {
                    $('#debit').val(0.0);
                }

            }

        });

        $('.totalup').keyup(function (e) {

            var qty = $('#qty').val();
            var unitp = $('#unitp').val();
            var taxrate = $('#taxrate').val();

            var total = qty * unitp;
            var taxamt = taxrate * total / 100;
            $('#taxamt').val(taxamt);
            $('#amount').val(total);

        });

        $('.calculate_gst').change(function (e) {
            alert(9);
            var amt = $('#amount').val();
            var taxrate = $('#taxrate').val();

            var total = taxrate * 100 / amt;

            $('#taxamt').val(total);

        });

        $('.form-control').focusout(function (e) {
            if (($('#debit').val() == 0.00) || ($('#loccode').val() == '') || ($('#actdesc').val() == '') || ($('#remarks').val() == '')) {
                $('#savebutton').prop('disabled', true);
            } else {
                $('#savebutton').prop('disabled', false);
            }
        });

        $('#getproduct').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Product Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_product.jsp?name=prodname&code=prodcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#amount').keyup(function (e) {
            //e.preventDefault();
            var oriamount = $('#oriamount').val();
            var newamount = $(this).val();
            var currency = "$1,100.00";
            var number1 = Number(oriamount.replace(/[^0-9\.]+/g, ""));
            var number2 = Number(newamount.replace(/[^0-9\.]+/g, ""));
            var bal = parseFloat(number1) - parseFloat(number2);

            if (bal < 0) {
                $("#bal").css({"color": "red"});
            } else {
                $("#bal").css({"color": "black"});
            }
            $('#bal').html(formatCurrency(bal));


            return false;
        });

        $('#amount').change(function (e) {
            var oriamount = $('#oriamount').val();
            var newamount = $(this).val();
            var currency = "$1,100.00";
            var number1 = Number(oriamount.replace(/[^0-9\.]+/g, ""));
            var number2 = Number(newamount.replace(/[^0-9\.]+/g, ""));
            var bal = parseFloat(number1) - parseFloat(number2);

            if (bal < 0) {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: 'Error',
                    message: 'Amount exceed current balance!',
                    animate: false,
                    buttons: [{
                            label: 'Close',
                            action: function (dialogRef) {
                                dialogRef.close();
                            }
                        }]
                });

                $('#bal').html("0.00");
                $(this).val(oriamount);
                $("#bal").css({"color": "black"});

            }



            return false;
        });


    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="editlist" type="<%= vm.getRefer()%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addpvitem" disabled><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>



            <form data-toggle="validator" role="form" id="saveform">
                <input type="hidden" name="prepareid" id="prepareid" value="<%= log.getUserID()%>">
                <input type="hidden" name="preparename" id="preparename" value="<%= log.getFullname()%>">
                <input type="hidden" name="preparedate" id="preparedate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Reference No &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="refer" name="refer" placeholder="Auto Generated" autocomplete="off" value="<%= vm.getRefer()%>" required readonly>   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Voucher No &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="voucer" name="voucer" placeholder="Auto Generated" autocomplete="off" required readonly>   
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Location Level&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="loclevel" name="loclevel" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="getlocation"><i class="fa fa-cog"></i> Location</button>
                                    </span>
                                </div>    
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Location Code&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="loccode" name="loccode"  autocomplete="off" readonly>   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Location Name&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="locdesc" name="locname" placeholder="" autocomplete="off" readonly >   
                            </div>
                        </div>
                    </div>


                </div>

                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Account Code&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span id="res_digit" class="res_style"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="coacode" name="coacode" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="coacode" id1="coadescp"><i class="fa fa-cog"></i> Account</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Account Description&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="coadescp" name="coadescp" readonly>
                            </div>
                        </div>

                    </div> 
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Sub Account Type&nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="satype" name="satype" value="None" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="getsub"><i class="fa fa-cog"></i> Sub Account</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Sub Account Code &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="sacode" name="sacode" placeholder="" autocomplete="off" value="00" readonly> 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Sub Account Description &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="sadesc" name="sadesc" placeholder="" autocomplete="off" value="Not Applicable" readonly > 
                            </div>
                        </div></div>
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Remarks&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <textarea class="form-control" rows="3"  id="remarks" name="remarks"><%= vm.getRemarks()%></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Amount &nbsp&nbsp<span class="res_code"></span></label><span class="pull-right">Balance | <strong id="bal">0.00</strong></span>
                                <input type="hidden" id="oriamount" value="<%= GeneralTerm.currencyFormat(PaymentVoucherDAO.getBalance(log, "amount", vm.getRefer()))%>">
                                <input type="text" class="form-control input-sm" id="amount" name="amount" value="<%= PaymentVoucherDAO.getBalance(log, "amount", vm.getRefer())%>" autocomplete="off" value=""> 
                            </div>
                        </div>

                    </div></div>
                <div class="well"> 
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Tax Code &nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="hidden" name="taxcoacode" id="taxcoacode">
                                    <input type="hidden" name="taxcoadescp" id="taxcoadescp">
                                    <input type="text" class="form-control input-sm calculate_gst" id="taxcode" name="taxcode" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="gettax"><i class="fa fa-cog"></i> Tax Type</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Tax Description &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="taxdescp" name="taxdescp" autocomplete="off" readonly> 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Tax Rate &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="taxrate" name="taxrate" value="0.00" autocomplete="off" readonly> 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Tax Amount &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="taxamt" name="taxamt" placeholder="0.00" autocomplete="off" value="0.00" readonly > 
                            </div>
                        </div>
                    </div>
                </div> 
            </form>
        </div>
    </div>
</div>