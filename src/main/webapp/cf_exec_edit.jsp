<%-- 
    Document   : cf_exec_edit
    Created on : Apr 18, 2016, 4:52:40 PM
    Author     : HP
--%>

<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Executive"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ExecDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) ExecDAO.getModule();
    Executive edit = (Executive) ExecDAO.getInfo(log,request.getParameter("referenceno"));

%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#getaccount').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Chart of Account',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_coa.jsp');
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                    });

                    return $content;
                }
            });

            return false;
        });
        $(document).ready(function () {
            //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });
            //$('#JVdate').datepicker({ dateFormat: 'yyyy-mm-dd' });

            $('#birth').datepicker({
                format: 'yyyy-mm-dd',
                defaultDate: 'now',
                autoclose: true
            });
        });
    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="editprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>
            <form data-toggle="validator" role="form" id="saveform">
                <input type="hidden" name="prepareid" id="pid" value="<%= log.getUserID()%>">
                <input type="hidden" name="preparename" id="pname" value="<%= log.getFullname()%>">
                <input type="hidden" name="preparedate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Id &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="id" name="id" autocomplete="off" value="<%= edit.getId()%>">   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Name &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="name" name="name" autocomplete="off" value="<%= edit.getName()%>">   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Service Status &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="status" name="status">
                                    <%= ParameterDAO.parameterList(log,"Service Status", edit.getStatus())%>
                                </select> 
                            </div>
                        </div>
                    </div>
                    <div class="row">       
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">New I/C No &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="newic" name="newic" autocomplete="off" value="<%= edit.getNewic()%>">   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Old I/C No &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="oldic" name="oldic" autocomplete="off" value="<%= edit.getOldic()%>">   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Date of Birth &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="birth" name="birth" autocomplete="off" value="<%= edit.getBirth()%>">   
                            </div>
                        </div>     
                    </div>
                </div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Address &nbsp&nbsp<span class="res_code"></span></label>
                                <textarea class="form-control" rows="3"  id="address" name="address"><%= edit.getAddress()%></textarea>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Postcode &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="postcode" name="postcode" placeholder="" autocomplete="off" value="<%= edit.getPostcode()%>"> 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">City &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="city" name="city" placeholder="" autocomplete="off" value="<%= edit.getCity()%>"> 
                            </div>
                        </div><div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">State &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="state" name="state" placeholder="" autocomplete="off" value="<%= edit.getState()%>"> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">HP No &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="hp" name="hp" placeholder="" autocomplete="off" value="<%= edit.getHp()%>">  
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Phone No &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="phone" name="phone" placeholder="" autocomplete="off" value="<%= edit.getPhone()%>" > 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Fax No &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="fax" name="fax" placeholder="" autocomplete="off" value="<%= edit.getFax()%>" > 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Email &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="email" name="email" placeholder="" autocomplete="off" value="<%= edit.getEmail()%>" > 
                            </div>
                        </div>

                    </div>
                </div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Religion &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="religion" name="religion">
                                    <%= ParameterDAO.parameterList(log,"Religion", edit.getReligion())%>
                                </select> 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Race &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="race" name="race">
                                    <%= ParameterDAO.parameterList(log,"Race", edit.getRace())%>
                                </select> 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Gender &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="sex" name="sex">
                                    <%= ParameterDAO.parameterList(log,"Gender", edit.getSex())%>
                                </select> 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Citizen &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="citizen" name="citizen">
                                    <%= ParameterDAO.parameterList(log,"Citizen", edit.getCitizen())%>
                                </select> 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Passport &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="passport" name="passport" autocomplete="off" value="<%= edit.getPassport()%>">   
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Blood Type &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="blood" name="blood">
                                    <%= ParameterDAO.parameterList(log,"Blood Type", edit.getBlood())%>
                                </select> 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Marital Status &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="marital" name="marital">
                                    <%= ParameterDAO.parameterList(log,"Marital Status", edit.getMarital())%>
                                </select> 
                            </div>
                        </div>       
                    </div>
                </div>    
            </form>
        </div>
    </div>
</div>

