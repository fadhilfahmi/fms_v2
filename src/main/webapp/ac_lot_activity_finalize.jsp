<%-- 
    Document   : ac_lot_activity_finalize
    Created on : Dec 22, 2016, 2:30:01 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.LotInformationDAO"%>
<%@page import="com.lcsb.fms.dao.management.activity.LotActivityDAO"%>
<%@page import="com.lcsb.fms.model.management.activity.LotProfitCalc"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LotProfitCalc lot = (LotProfitCalc) LotActivityDAO.getFinalize(request.getParameter("sessionid"));
    int getTotalMember = LotActivityDAO.getTotalMember(request.getParameter("sessionid"));
%>
<table class="table table-bordered table-striped  table-hover">
    <thead>
        <tr>
            <th colspan="4">Lot Information

            </th>
        </tr>
    </thead>
    <tbody class="activerowm tdrow">
        <tr>
            <th width="15%">Lot</th>
            <td width="35%"><%= lot.getLotcode()%> - <%= lot.getLotdesc()%></td>
            <th width="15%">Hectarage</th>
            <td width="35%">Acre : <%= LotInformationDAO.getInfo(lot.getLotcode()).getLotAcre()%>, Hectar : <%= LotInformationDAO.getInfo(lot.getLotcode()).getLotHect()%></td>
        </tr>
        <tr>
            <th>Period</th>
            <td><%= lot.getYear()%> - <%= lot.getPeriod()%></td>
            <th>Quarter</th>
            <td><%= lot.getQuarter()%></td>
        </tr>
        <tr>
            <th>Total Distribution</th>
            <td><%= GeneralTerm.currencyFormat(lot.getProfitperhect() * Double.parseDouble(LotInformationDAO.getInfo(lot.getLotcode()).getLotHect()))%></td>
            <th>Total Members</th>
            <td><%= getTotalMember%></td>
        </tr>
        <tr>
            <th>Profit Perhectar</th>
            <td><%= GeneralTerm.currencyFormat(lot.getProfitperhect())%></td>
            <th>Suggested Distribute</th>
            <td><%= GeneralTerm.currencyFormat(lot.getDistsuggest())%></td>
        </tr>
        <tr>
            <th>Reason</th>
            <td colspan="3"></td>
        </tr>

        <tr>
            <th>Prepare By</th>
            <td colspan="3"></td>
        </tr>

    </tbody>
</table>
