<%-- 
    Document   : logout
    Created on : Apr 10, 2017, 4:05:36 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.ErrorIO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" sizes="57x57" href="assets/fav/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="assets/fav/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="assets/fav/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="assets/fav/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="assets/fav/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="assets/fav/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="assets/fav/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="assets/fav/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="assets/fav/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="assets/fav/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="assets/fav/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="assets/fav/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="assets/fav/favicon-16x16.png">
        <link rel="manifest" href="assets/fav/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="assets/fav/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <title>Logging out...</title>

        <!-- Bootstrap -->
        <link href="bower_components/bootstrap.min.css" rel="stylesheet">

        <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <script src="jsfunction/jquery-1.12.0.min.js"></script>
        <!-- Custom Theme Style -->
        <link href="css/custom/custom.css" rel="stylesheet">
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {

                $("#backto").click(function () {
                    $.ajax({
                        url: "PathController?moduleid=<%= request.getParameter("moduleid")%>&process=viewlist",
                        success: function (result) {
                            $('#herey').empty().html(result).hide().fadeIn(300);
                        }
                    });
                    return false;
                });

                $("#backtohome").click(function () {
                    $.ajax({
                        url: "start_2.jsp",
                        success: function (result) {
                            $('#herey').empty().html(result).hide().fadeIn(300);
                        }
                    });
                    return false;
                });

                $("#login").click(function () {
                    var href = $(this).attr('href');
                    $.ajax({
                        url: "Logout",
                        success: function (result) {
                            //alert(result);
                            //window.location.replace(result);
                            $('body').empty().load(href).hide().fadeIn(300);
                        }
                    });
                    return false;
                });
            });
        </script>
    </head>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <!-- page content -->
                <div class="col-md-12">
                    <div class="col-middle">
                        <div class="text-center">
                            <div class="error-template">
                                <!--<h1>
                                    Oops!</h1>-->
                                <h2>
                                    Please wait, system is logging out...</h2>
                                <div class="error-details">
                                    <span style="color:#C00"><strong><%//= ErrorIO.getError()%></strong></span>
                                </div>
                                <div class="error-actions">
                                    <i class="fa fa-spinner fa-spin fa-4x fa-fw"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->
            </div>
        </div>
    </body>

