<%-- 
    Document   : ar_agreement_view_list
    Created on : Oct 12, 2016, 2:40:34 PM
    Author     : fadhilfahmi
--%>

<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArContractInfo"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArContractAgree"%>
<%@page import="com.lcsb.fms.dao.financial.ar.ArContractDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
//ArContractAgree vi = (ArContractAgree) ArContractDAO.getPYVitem(request.getParameter("refer"));
ArContractInfo v = (ArContractInfo) ArContractDAO.getInfo(log,request.getParameter("refer"));
Module mod = (Module) ArContractDAO.getModule();


%>

<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/tether.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>

<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
 <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $.ajax({
            async:true,
            url: "ProcessController?moduleid=<%= mod.getModuleID() %>&process=getDiffMill&millcode=2411&buyercode=<%= v.getCode() %>",
            success: function (result) {

                if(result==0){
                    var cont = 'No new Contract';
                    $('.retrievemill').empty().html(cont).hide().fadeIn(300);
                    $(".retrievemill").prop("disabled",true);

                }else{

                    var cont = 'New Contracts from Mill <span class="badge">'+result+'</span>';
                    $('.retrievemill').empty().html(cont).hide().fadeIn(300);

                }

            },
                    error: function(){
                       var cont = 'Failed to retrieve';
                    $('.retrievemill').empty().html(cont).hide().fadeIn(300);
                    }
        });
        
        $( ".retrievemill" ).click(function() {
                var cont = '<i class="fa fa-cog fa-spin fa-fw"></i><span>Retrieving...</span>';
                $('.retrievemill').empty().html(cont).hide().fadeIn(300);
                $.ajax({
                    async:true,
                    url: "PathController?moduleid=<%= mod.getModuleID() %>&process=retrievemillagreement&buyercode=<%= v.getCode() %>",
                    success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                    var conts = '<i class="fa fa-check-circle success" aria-hidden="true"></i> Successful Retrieved';
                    $('.retrievemill').empty().html(conts).hide().fadeIn(300);
                },
                    error: function(){
                        var conts = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Failed';
                    $('.retrievemill').empty().html(conts).hide().fadeIn(300);
                    }
                });
                return false;
	  });
        
        var oTable = $('#list').DataTable( {
            responsive: true,
            "aaSorting": [[ 0, "desc" ]],
	} );
        
        $('#list tbody').on('click', 'tr', function (e) {
            var data = oTable.row( this ).data();
            //alert(data[0]);
            e.preventDefault();
               
               BootstrapDialog.show({
                    type:BootstrapDialog.TYPE_DEFAULT,
                    size:BootstrapDialog.SIZE_WIDE,
                            //animate: false,
                    title: 'View Contract Detail',
                        message: function(dialog) {
                        var $content = $('<body></body>').load('ar_agreement_view_each.jsp?refer='+data[0]+'&buyer=<%= v.getCode() %>');
                        //$('body').on('click', '.thisresult_nd', function(event){
                        //    dialog.close();
                        //    event.preventDefault();
                        //});
                        return $content;
                    },
                        buttons: [{
                            label: 'Close',
                            action: function(dialogRef){
                                dialogRef.close();
                            }
                        }]
                    });
               
        });
        
        $('#print').click(function(e){
            printElement($("#viewjv").html());
        });
        
        $('.viewvoucher').click(function(e){
            var a = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=viewpv&refer="+a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
	});
            
        $( ".editmain" ).click(function() {
            var a = $(this).attr('id');
            var b = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=editmain&referno="+a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });
              
        $( ".edititem" ).click(function() {
            var a = $(this).attr('id');
            var b = $(this).attr('name');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process="+b+"&refer="+a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });
              
        $( ".deleteitem" ).click(function() {
            var a = $(this).attr('id');
            var b = $(this).attr('name');
            
            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are you sure to delete?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function(result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if(result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process="+b+"&referno="+a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            
            return false;
        });
              
        $( ".deletemain" ).click(function() {
            var a = $(this).attr('id');
            var b = $(this).attr('href');
                 
            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'All datas for this Official Receipt will be permanently deleted. Are you sure to proceed?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function(result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if(result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=delete&referno="+a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            return false;
        });
              
        $( "#addcredit" ).click(function(e) {
            e.preventDefault();
            var refer = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=addcredit&refer="+refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
                    
            return false;
        });
                
        $( ".addagree" ).click(function(e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=addagree&refer="+refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });
                
        $( ".adddebit" ).click(function(e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=adddebit&refer="+refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });
                
        $( ".check" ).click(function(e) {
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID() %>&process=check&referno="+id,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });
            
       
        $( ".approve" ).click(function() {
            //var a = $("form").serialize();
            var a = $(this).attr('id');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are you sure to Approve this?',
                type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Approve', // <-- Default value is 'OK',
                btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
                callback: function(result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if(result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=dist&referno="+a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            return false;
        });
              
    });
</script>

<div id ="maincontainer">
    <div class="partition">
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
		</tr>
            </table>
            <br>
            <table class="table table-bordered table-striped  table-hover">
                <thead>
                    <tr>
                        <th colspan="4">Voucher Information
                        <!--<small><span class="label label-primary">Preparing</span></small>-->
                            <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
                            
                                <button id="<%//= v.getRefer() %>" class="btn btn-default btn-xs viewvoucher" title="<%= mod.getModuleID() %>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i></button>
                                <button id="<%//= v.getRefer() %>" class="btn btn-default btn-xs editmain" title="<%= mod.getModuleID() %>" name="edit_st"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                <button id="<%//= v.getRefer() %>"  class="btn btn-danger btn-xs deletemain" title="<%= mod.getModuleID() %>" id="<%//= pvi.getRefer() %>" name="delete_st"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                               
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody class="activerowm tdrow">
                    <tr>
                        <th width="25%">Buyer</th>
                        <td width="75%"><%= v.getCode() %> - <%= v.getDescp() %></td>
                    </tr>
                    <tr>
                        <th>Address</th>
                        <td><%= v.getAddress() %></td>
                    </tr>
                    <tr>
                        <th>Mill</th>
                        <td><%= v.getLoccode()%> - <%= v.getLocname() %></td>
                    </tr>
                    <tr>
                        <th>Account</th>
                        <td><%= v.getActcode()%> - <%= v.getActdescp() %></td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td><%= v.getActive()%></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-bordered table-striped table-hover" id="list">
                <thead>
                    <tr>
                        <th colspan="8">Contract<!--<small>asdada</small>-->
                            <span class="pull-right">
                                <button  class="btn btn-default btn-xs addagree" title="<%= mod.getModuleID() %>" id="<%= v.getCode()%>" name="addagree"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Contract</button>
                                <button  class="btn btn-default btn-xs retrievemill" title="<%= mod.getModuleID() %>" id="<%= v.getCode()%>" name="addagree"><i class="fa fa-cog fa-spin fa-fw"></i>
<span>Loading</span></button>
                            </span>
                        </th>
                    </tr>
                    <tr>
                        <th>#</th>
                        <th>Commodity</th>
                        <th>Contract No</th>
                        <th>Date</th>
                        <th>Delivery Month</th>
                        <th>Price</th>
                        <th>Active</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <%List<ArContractAgree> listx = (List<ArContractAgree>) ArContractDAO.getAllAgreement(log,v.getCode());
                 
                    for (ArContractAgree j : listx) {%>
                    <tr class="activerowy" id="<%= j.getId()%>">
                        <td class="tdrow"><%= j.getId()%></td>
                        <td class="tdrow"><%= j.getComcd()%> - <%= j.getComde()%></td>
                        <td class="tdrow">&nbsp;<%= j.getNo()%></td>
                        <td class="tdrow">&nbsp;<%= j.getDate()%></td>
                        <td class="tdrow">&nbsp;<%= j.getDmonth()%> - <%= j.getDyear()%></td>
                        <td class="tdrow">&nbsp;<%= j.getPrice() %></td>
                        <td class="tdrow" align="right">&nbsp;<%= j.getActive() %></td>
                        <td class="tdrow" align="right">&nbsp;
                            <div class="btn-group btn-group-xs" role="group" aria-label="...">
                                <button id="<%= j.getId()%>" class="btn btn-default btn-xs edititem" title="<%= mod.getModuleID() %>" name="edit_st"><i class="fa fa-pencil"></i></button>
                                <button id="<%= j.getId()%>" class="btn btn-default btn-xs deleteitem" title="<%= mod.getModuleID() %>"  name="delete_st"><i class="fa fa-trash-o"></i></button>
                            </div>
                        </td>
                    </tr>
                    <%}%>
                </tbody>
                    
            </table>
        </div>
    </div>
</div>