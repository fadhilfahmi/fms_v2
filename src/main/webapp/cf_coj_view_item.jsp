<%-- 
    Document   : cf_coj_view_item
    Created on : Jun 2, 2016, 8:59:08 AM
    Author     : user
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ChartofJobDAO"%>
<%@page import="com.lcsb.fms.model.setup.configuration.ChartofJob"%>
<%@page import="com.lcsb.fms.model.setup.configuration.ChartofJobItem"%>
<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
ChartofJobItem view = (ChartofJobItem) ChartofJobDAO.getInfoItem(log,request.getParameter("id"));
%>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
           
            
              $( ".printbutton" ).click(function() {
                  //var a = $("form").serialize();
                  var a = $("#saveform :input[value!='']").serialize(); 
                  //alert($('input[name="ad_amount"]').val());
                  checkField();
                  var b = checkField();
                  if(b==false){
                    $.ajax({
                        data: a,
                        type: 'POST',
                        url: "DataController?do=add&formlevel=master",
                        success: function (result) {
                          $('#maincontainer').empty().html(result).hide().fadeIn(300);
                        }
                    });
                     
                  }    
                  
               
                    return false;
              });
              
         });
        </script>
            <div id ="maincontainer">
                <table id="table_2" cellspacing="0">
                    
               <tr>
                   <td width="30%" class="bd_bottom" align="left" valign="top">Job Code</td>
                   <td width="70%" class="bd_bottom" align="left"><%= view.getJobcode() %></td>
               </tr>
               <tr>
                   <td class="bd_bottom" align="left" valign="top">Account Code</td>
                   <td class="bd_bottom" align="left"> 
                       <%= view.getAccountcode()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Account Description</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getAccountdescp()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Remarks</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getRemarks()%>
                   </td>
               </tr>
              
               
              
              
          </table>
            </div>