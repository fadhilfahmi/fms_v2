<%-- 
    Document   : gl_fr_gllisting
    Created on : Nov 16, 2016, 3:13:13 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.financial.gl.LiveGLDAO"%>
<%@page import="com.lcsb.fms.util.ext.ParseSafely"%>
<%@page import="com.lcsb.fms.report.financial.gl.GListMaster"%>
<%@page import="com.lcsb.fms.report.financial.gl.GListDetail"%>
<%@page import="com.lcsb.fms.report.financial.gl.GListParam"%>
<%@page import="com.lcsb.fms.report.financial.gl.GListDAO"%>
<%@page import="com.lcsb.fms.report.financial.gl.GListHeader"%>
<%@page import="com.lcsb.fms.report.financial.balancesheet.BsItem"%>
<%@page import="com.lcsb.fms.report.financial.balancesheet.BsMaster"%>
<%@page import="com.lcsb.fms.report.financial.balancesheet.BalanceSheetDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.model.report.BalancesheetModel"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    GListMaster gl = (GListMaster) LiveGLDAO.generateGListing(log,request.getParameter("refer"));
    Module mod = (Module) GListDAO.getModule();


%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#print').click(function (e) {
            printReport($("#viewprint").html());
        });

        $(".goto").click(function () {
            var link = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%//=moduleid%>&process=" + link,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


    });
</script>
<style>
    .header1{
        border-color:#000;
        border-style: solid; 
        border-width: 1px;
        font-family:Arial, Helvetica, sans-serif;
        font-size:10px;
        font-weight:300;
    }
    .data1{

        font-family:Arial, Helvetica, sans-serif;

        font-size:12px
    }	
    .data2{

        font-family:Arial, Helvetica, sans-serif;

        font-size:9px
    }
    .aright{
        text-align:right}
    thead {display: table-header-group; }
    tfoot {display: table-footer-group; }
    thead th, thead td {position: static; }

    @media print
    {    
        .no-print, .no-print *
        {
            display: none !important;
        }
    }			
</style>
<link href="css/table_style.css" rel="stylesheet" type="text/css" />
<link href="css/pvoucher.css" rel="stylesheet" type="text/css" />
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= request.getParameter("modulefrom") %>" name="editlist" type="<%= request.getParameter("refer") %>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="print"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addinv" ><i class="fa fa-floppy-o"></i>&nbsp;Print</button>
                    </td>
                </tr>
            </table>
            <br>
            <div id="viewprint">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td colspan="4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="50px"><img src="image/logo_lkpp.jpg" width="40px" style="vertical-align:middle" ></td>
                                    <td><span class="bigtitlebold"><%= log.getEstateDescp()%></span><span class="pull-right"></span>
                                        <p class="font_inheader_normal"><%= gl.getGltitle() %></p>
                                        <p class="font_inheader_normal"><%= gl.getGlcodetocode() %></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="border_bottom"></td>
                    </tr>
                </table>
                <table class="table-striped table-hover" width="100%">
                <thead>
                    
                    <tr>
                        <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;" width="7%">Acc.</th>
                        <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">Period</th>
                        <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">Year</th>
                        <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">Date</th>
                        <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">Source</th>
                        <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">Desc</th>
                        <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">Reference</th>
                        <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;" width="8%">Debit</th>
                        <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;" width="8%">Credit</th>
                        <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">Net Change</th>
                        <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;" width="8%">Balance</th>
                        <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;" width="2%"></th>
                    </tr>
                </thead> 
                    <tbody> 
                        <%

                            List<GListHeader> list = (List<GListHeader>) gl.getListCore();
                            for (GListHeader j : list) {
                        %>
                        <tr> 
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><strong><span class="pull-left"><%= j.getCoacode()%></span></strong></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left">&nbsp;</span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left">&nbsp;</span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left">&nbsp;</span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left">&nbsp;</span></td>&nbsp;</span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><strong><span class="pull-left"><%= j.getCoadescp()%></span></strong></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><strong><span class="pull-left"><%= j.getSatype()%> - <%= j.getSacode()%> <%= j.getSadesc()%></span></strong></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left">&nbsp;</span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left">&nbsp;</span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left">&nbsp;</span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-right"><strong><%= GeneralTerm.normalDebit(j.getBalance())%></strong></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left">&nbsp;</span></td>
                        </tr>
<%
                            double totDebit = 0.0;
                            double totCredit = 0.0;
                            List<GListDetail> listi = (List<GListDetail>) j.getListDetail();
                            for (GListDetail i : listi) {
                                totDebit+=ParseSafely.parseDoubleSafely(i.getDebit());
                                totCredit+=ParseSafely.parseDoubleSafely(i.getCredit());
                        %>
                        <tr> 
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left">&nbsp;</span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getPeriod() %></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getYear() %></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getTarikh() %></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getSource() %></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getRemark() %></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getNovoucher() %></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-right"><%= i.getDebit() %></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-right"><%= i.getCredit() %></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-right"><%= i.getNetchange() %></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-right"><%= i.getLastbalance() %></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getStar() %></span></td>
                        </tr>

                        <%
                           }%>
                           <tr> 
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left">&nbsp;</span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-right"><%= GeneralTerm.normalCredit(totDebit) %></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-right"><%= GeneralTerm.normalCredit(totCredit) %></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"></td>
                        </tr>
<%}
                        %>
                        
                        <tr> 
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left">&nbsp;</span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px; border-top:2px solid #CCC;"><span class="pull-right"><strong><%= GeneralTerm.normalCredit(gl.getTotalDebit()) %></strong></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px; border-top:2px solid #CCC;"><span class="pull-right"><strong><%= GeneralTerm.normalCredit(gl.getTotalCredit()) %></strong></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-right"></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-right"></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"></span></td>
                        </tr>

                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
