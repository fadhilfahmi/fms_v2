<%-- 
    Document   : ar_rpt_aging_view
    Created on : Mar 28, 2017, 6:25:20 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArReportAging"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArReportAgingParam"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Buyer"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.BuyerDAO"%>
<%@page import="com.lcsb.fms.dao.financial.ar.ArReportDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) ArReportDAO.getModule();
    ArReportAgingParam prm = (ArReportAgingParam) session.getAttribute("ar_aging_param");
    //Buyer v = (Buyer) BuyerDAO.getInfo(prm.getBuyerCode());
%>    
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('.printreport').click(function (e) {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=printreport",
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });
    });
</script>
<div id ="maincontainer">
    <div class="partition">
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID()%>" name="aging"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
                </tr>
            </table>
            <br>
            <table class="table table-bordered table-striped  table-hover">
                <thead>
                    <tr>
                        <th colspan="4">Voucher Information
                            <!--<small><span class="label label-primary">Preparing</span></small>-->
                            <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">

                                <button id="<%//= v.getRefer() %>" class="btn btn-default btn-xs printreport" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i></button>

                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody class="activerowm tdrow">
                    <tr>
                        <th width="25%">View</th>
                        <td width="75%"><%//= v.getCode()%> - <%//= v.getCompanyname()%></td>
                    </tr>
                    <tr>
                        <th>Until Date</th>
                        <td><%= prm.getDate()%></td>
                    </tr>
                    <tr>
                        <th>Buyer</th>
                        <td><%= prm.getType()%></td>
                    </tr>

                </tbody>
            </table>
            <table class="table table-bordered table-striped table-hover" id="list">
                <thead>
                    <tr>
                        <th colspan="8">Contract<!--<small>asdada</small>-->

                        </th>
                    </tr>
                    <tr>
                        <th>Buyer Code</th>
                        <th>Buyer Name</th>
                        <th><= 30 Days</th>
                        <th>31-60 Days</th>
                        <th>61-90 Days</th>
                        <th>91-120 Days</th>
                        <th>> 120 Days</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        double totalx = 0.0;
                        List<ArReportAging> listx = (List<ArReportAging>) ArReportDAO.getAllAging(log,prm, 0, 0, false);

                        for (ArReportAging j : listx) {
                            totalx += j.getTotal();

                    %>
                    <tr class="activerowy" id="<%//= j.getId()%>">
                        <td class="tdrow"><%= j.getBuyerCode()%></td>
                        <td class="tdrow">&nbsp;<%= j.getBuyerName()%></td>
                        <td class="tdrow">&nbsp;<span class="pull-right"><%= GeneralTerm.normalCredit(j.getDayCount1()) %></span></td>
                        <td class="tdrow">&nbsp;<span class="pull-right"><%= GeneralTerm.normalCredit(j.getDayCount2()) %></span></td>
                        <td class="tdrow">&nbsp;<span class="pull-right"><%= GeneralTerm.normalCredit(j.getDayCount3()) %></span></td>
                        <td class="tdrow" align="right"><span class="pull-right"><%= GeneralTerm.normalCredit(j.getDayCount4()) %></span></td>
                        <td class="tdrow" align="right"><span class="pull-right"><%= GeneralTerm.normalCredit(j.getDayCount5()) %></span></td>
                        <td class="tdrow" align="right"><span class="pull-right"><%= GeneralTerm.normalCredit(j.getTotal()) %></span></td>
                     </tr>
                    <%}%>
                </tbody>

            </table>
        </div>
    </div>
</div>
