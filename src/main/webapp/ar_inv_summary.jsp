<%-- 
    Document   : ar_inv_summary
    Created on : Apr 7, 2017, 12:51:42 PM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.ListTable"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.ModuleItem"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.ConnectionModel"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.CashVoucherDAO"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,java.util.*" errorPage="" isThreadSafe="false" %>
<%@ page import="java.util.Date"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) SalesInvoiceDAO.getModule();

    response.setHeader("Cache-Control", "no-cache");
%>

<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/tether.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>

<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script src="jsfunction/timeago.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        new timeago().render($('.need_to_be_rendered'));
        //var timeagoInstance = new timeago('<%//= SalesInvoiceDAO.getLastSync() %>');
        //timeagoInstance.render($('.need_to_be_rendered'));


        $.ajax({
            url: "datatable.jsp?moduleid=<%= mod.getModuleID()%>&view=approval",
            success: function (result) {
                $('#datatable-render').empty().html(result).hide().fadeIn(300);
            }
        });


        $("#add-new").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addnew",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        $("#check-button").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=check",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        $('body').on('click', '#refresh', function (e) {
            console.log('Refreshing');
            oTable.draw();
            return false;
        });

        $("#filter").click(function () {
            $("#filterdiv").toggle();
            //oTable.draw();
            return false;
        });

        $("#refine").click(function () {
            $("#filterdiv").toggle();
            //oTable.draw();
            return false;
        });

        $("#syncmill").click(function () {
            var cont = '<i class="fa fa-cog fa-spin fa-fw"></i><span> Connecting...</span>';
            $('#syncmill').empty().html(cont).hide().fadeIn(300);
            $.ajax({
                async: true,
                url: "ProcessController?moduleid=000000&process=connecttomill",
                success: function (result) {
                    // $("#haha").html(result);
                    console.log(result);
                    if (result == 1) {
                        //$('#herey').empty().html(result).hide().fadeIn(300);
                        var conts = '<i class="fa fa-check-circle success" aria-hidden="true"></i> Connection Successful';
                        $('#syncmill').empty().html(conts).hide().fadeIn(300);

                        conts = '<i class="fa fa-cog fa-spin fa-fw"></i><span> Syncing...';
                        $('#syncmill').empty().html(conts).hide().fadeIn(300);

                        $.ajax({
                            async: true,
                            url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=syncrefinery",
                            success: function (result) {
                                //var timeagoInstance = new timeago();
                                //timeagoInstance.render($('.need_to_be_rendered'));
                                $('.need_to_be_rendered').html('Just Now');
                                //$('#herey').empty().html(result).hide().fadeIn(300);
                                var conts = '<i class="fa fa-check-circle success" aria-hidden="true"></i> Synced';
                                $('#syncmill').empty().html(conts).hide().fadeIn(300);
                            },
                            error: function () {
                                var conts = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Failed';
                                $('#syncmill').empty().html(conts).hide().fadeIn(300);
                            }
                        });
                    } else {
                        var conts = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Connection Failed';
                        $('#syncmill').empty().html(conts).hide().fadeIn(300);
                    }

                },
                error: function () {
                    var conts = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Connection Failed';
                    $('#syncmill').empty().html(conts).hide().fadeIn(300);
                }
            });
            return false;
        });


    });

</script>
    <div class="panel-heading">
        <i class="fa fa-th-list" aria-hidden="true"></i> Summary of Invoices
    </div>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <div class="dataTable_wrapper" id="datatable-render"> 
                <div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>
            </div>
        </div>
    </div>
</div>
