<%-- 
    Document   : chequex
    Created on : Mar 30, 2017, 8:13:26 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lowagie.text.Rectangle"%>
<%@
page import="java.io.*,
			 com.lowagie.text.*,
			 com.lowagie.text.PageSize.*,
			 com.lowagie.text.Image.*,
			 com.lowagie.text.pdf.*,
			 com.lowagie.text.Paragraph.*,
			 com.lowagie.text.Phrase.*,
			 com.lowagie.text.pdf.PdfPTable.*,
			 com.lowagie.text.pdf.PdfPCell.*"
%><%
response.setContentType( "application/pdf" );
%><%



Rectangle pageSize = new Rectangle(505, 255);
//pageSize.setBackgroundColor
//(new java.awt.Color(959595));
// step 1: creation of a document-object
Document document = new Document(pageSize);

/*String bank = request.getParameter("bank");
String name = request.getParameter("name");
String y1 = request.getParameter("y1");
String y2 = request.getParameter("y2");
String m1 = request.getParameter("m1");
String m2 = request.getParameter("m2");
String d1 = request.getParameter("d1");
String d2 = request.getParameter("d2");*/

String bank = "maybank";
String name = "fadhil";
String y1 = "0";
String y2 = "0";
String m1 = "0";
String m2 = "0";
String d1 = "0";
String d2 = "0";
String date = "31/7/2012";
String amount = request.getParameter("amount");
//String amount = "NINE MILLION NINE HUNDRED NINETY NINE THOUSAND NINE NINE HUNDRED NINETY NINE AND NINETY CENT";
String amount_num = request.getParameter("amount_num");
ByteArrayOutputStream buffer = new ByteArrayOutputStream();
PdfWriter.getInstance( document, buffer );



if(bank.equalsIgnoreCase("maybank")){
document.open();


PdfPTable table = new PdfPTable(2); // Code 1
table.setWidthPercentage(117);
float[] columnWidths = {2f, 1f};
Font font3 = new Font(Font.ITALIC, 9);
Font font4 = new Font(Font.ITALIC, 12);
table.setWidths(columnWidths);
//table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

//		// Code 2
		
		
		
		Paragraph paragraph = new Paragraph(20);
		
			
			Chunk chunk = new Chunk("                           " + amount,font3);
		
		//paragraph.setSpacingBefore(1);
		paragraph.add(chunk);
		//paragraph.setSpacingAfter(10);
		
		
		
		PdfPCell cell1 = new PdfPCell(new Paragraph(" "));
		PdfPCell cell2 = new PdfPCell(new Paragraph(""));
		
		// Code 3
		PdfPCell cell3 = new PdfPCell(new Paragraph(""));
		PdfPCell cell4 = new PdfPCell(new Paragraph(date, font4));
		PdfPCell cell5 = new PdfPCell(new Paragraph(""));
		PdfPCell cell6 = new PdfPCell(new Paragraph(" "));
		PdfPCell cell7 = new PdfPCell(new Paragraph("                                 " + name, font3));
		PdfPCell cell8 = new PdfPCell(new Paragraph(""));
		
		PdfPCell cell9 = new PdfPCell(new Paragraph(""));
		PdfPCell cell10 = new PdfPCell(new Paragraph(""));
		
		// Code 3
		PdfPCell cell11 = new PdfPCell(new Paragraph(" "));
		PdfPCell cell12 = new PdfPCell(new Paragraph(" "));
		PdfPCell cell13 = new PdfPCell(new Paragraph(""));
		PdfPCell cell14 = new PdfPCell(new Paragraph(amount_num, font4));
		PdfPCell cell15 = new PdfPCell(new Paragraph(" "));
		PdfPCell cell16 = new PdfPCell(new Paragraph(" "));
		PdfPCell cell17 = new PdfPCell(new Paragraph(""));
		PdfPCell cell18 = new PdfPCell(new Paragraph(""));
		
		
		cell1.setBorder(Rectangle.NO_BORDER);
		cell2.setBorder(Rectangle.NO_BORDER);
		cell3.setBorder(Rectangle.NO_BORDER);
		cell4.setBorder(Rectangle.NO_BORDER);
		cell5.setBorder(Rectangle.NO_BORDER);
		cell6.setBorder(Rectangle.NO_BORDER);
		cell7.setBorder(Rectangle.NO_BORDER);
		cell8.setBorder(Rectangle.NO_BORDER);
		cell9.setBorder(Rectangle.NO_BORDER);
		cell10.setBorder(Rectangle.NO_BORDER);
		cell11.setBorder(Rectangle.NO_BORDER);
		cell12.setBorder(Rectangle.NO_BORDER);
		cell13.setBorder(Rectangle.NO_BORDER);
		cell14.setBorder(Rectangle.NO_BORDER);
		cell15.setBorder(Rectangle.NO_BORDER);
		cell16.setBorder(Rectangle.NO_BORDER);
		cell17.setBorder(Rectangle.NO_BORDER);
		cell18.setBorder(Rectangle.NO_BORDER);
		
		PdfPCell cell19 = new PdfPCell(new Paragraph(" "));
		PdfPCell cell20 = new PdfPCell(new Paragraph(" "));
		
		// Code 3
		PdfPCell cell31 = new PdfPCell(new Paragraph(" "));
		PdfPCell cell27 = new PdfPCell(new Paragraph(" "));
		PdfPCell cell21 = new PdfPCell(new Paragraph(d1));
		PdfPCell cell22 = new PdfPCell(new Paragraph(d2));
		PdfPCell cell23 = new PdfPCell(new Paragraph(m1));
		PdfPCell cell24 = new PdfPCell(new Paragraph(m2));
		PdfPCell cell25 = new PdfPCell(new Paragraph(y1));
		PdfPCell cell26 = new PdfPCell(new Paragraph(y2));
		
		cell27.setPaddingRight(5);
		cell21.setPaddingRight(5);
		cell22.setPaddingRight(5);
		cell23.setPaddingRight(5);
		cell24.setPaddingRight(5);
		cell25.setPaddingRight(5);
		cell26.setPaddingRight(5);
		
		//setting align untuk nama
		cell7.setPaddingTop(7);
		
		
		cell21.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell22.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell23.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell24.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell25.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell26.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell21.setBorder(Rectangle.NO_BORDER);
		cell22.setBorder(Rectangle.NO_BORDER);
		cell23.setBorder(Rectangle.NO_BORDER);
		cell24.setBorder(Rectangle.NO_BORDER);
		cell25.setBorder(Rectangle.NO_BORDER);
		cell26.setBorder(Rectangle.NO_BORDER);
		cell27.setBorder(Rectangle.NO_BORDER);
		cell31.setBorder(Rectangle.NO_BORDER);
		
		PdfPTable nestedTable = new PdfPTable(8);
		nestedTable.setWidthPercentage(100);
		nestedTable.addCell(cell31);
		nestedTable.addCell(cell27);
		nestedTable.addCell(cell21);
		nestedTable.addCell(cell22);
		nestedTable.addCell(cell23);
		nestedTable.addCell(cell24);
		nestedTable.addCell(cell25);
		nestedTable.addCell(cell26);
		
		cell13.setPaddingLeft(30);
		cell13.setPaddingTop(-1);
		cell14.setPaddingTop(-18);
		cell13.setPaddingRight(15);
		cell13.addElement(paragraph);
		//amount setting align to right
		cell13.setPaddingLeft(43);
		
		//cell14.setVerticalAlignment(Element.ALIGN_TOP);
		//cell7.setHorizontalAlignment(Element.ALIGN_CENTER);
		//cell13.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell14.setFixedHeight(43f);
		//cell14.setPaddingTop(1);
		//cell14.setPaddingBottom(12);
		cell14.setPaddingLeft(39);
		cell14.setUseDescender(true);
		//cell14.setHorizontalAlignment(Element.ALIGN_CENTER);
		 cell14.setVerticalAlignment(Element.ALIGN_MIDDLE);
	
		cell4.setPaddingTop(8);
		//cell4.setRowspan(2);
		
		cell4.setFixedHeight(32f);
		cell4.addElement(nestedTable);
		
		table.addCell(cell17);
		table.addCell(cell18);
		table.addCell(cell1);
		
		//cell4.setFixedHeight(34f);
		
		
		table.addCell(cell4);
		
		// Code 3
		table.addCell(cell3);
		//bawah tarikh
		table.addCell(cell2);
		table.addCell(cell7);
		table.addCell(cell6);
		table.addCell(cell5);
		table.addCell(cell8);
		
		table.addCell(cell9);
		table.addCell(cell10);
		
		// Code 3
		table.addCell(cell13);
		table.addCell(cell14);
		table.addCell(cell11);
		table.addCell(cell12);
		table.addCell(cell15);
		table.addCell(cell16);
	
	
	
		// Code 5
		document.add(table);
		


document.close();
}else{
document.open();


PdfPTable table = new PdfPTable(2); // Code 1
table.setWidthPercentage(117);
float[] columnWidths = {2f, 1f};
Font font3 = new Font(Font.ITALIC, 9);
Font font4 = new Font(Font.ITALIC, 12);
table.setWidths(columnWidths);
//table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

//		// Code 2
		
		
		
		Paragraph paragraph = new Paragraph(20);
		
			
			Chunk chunk = new Chunk("                    " + amount,font3);
		
		//paragraph.setSpacingBefore(1);
		paragraph.add(chunk);
		//paragraph.setSpacingAfter(10);
		
		
		
		PdfPCell cell1 = new PdfPCell(new Paragraph(" "));
		PdfPCell cell2 = new PdfPCell(new Paragraph(""));
		
		// Code 3
		PdfPCell cell3 = new PdfPCell(new Paragraph(""));
		PdfPCell cell4 = new PdfPCell(new Paragraph(date, font4));
		PdfPCell cell5 = new PdfPCell(new Paragraph(""));
		PdfPCell cell6 = new PdfPCell(new Paragraph(" "));
		PdfPCell cell7 = new PdfPCell(new Paragraph("                                   " + name, font3));
		PdfPCell cell8 = new PdfPCell(new Paragraph(""));
		
		PdfPCell cell9 = new PdfPCell(new Paragraph(""));
		PdfPCell cell10 = new PdfPCell(new Paragraph(""));
		
		// Code 3
		PdfPCell cell11 = new PdfPCell(new Paragraph(" "));
		PdfPCell cell12 = new PdfPCell(new Paragraph(" "));
		PdfPCell cell13 = new PdfPCell(new Paragraph(""));
		PdfPCell cell14 = new PdfPCell(new Paragraph(amount_num, font4));
		PdfPCell cell15 = new PdfPCell(new Paragraph(" "));
		PdfPCell cell16 = new PdfPCell(new Paragraph(" "));
		PdfPCell cell17 = new PdfPCell(new Paragraph(""));
		PdfPCell cell18 = new PdfPCell(new Paragraph(""));
		
		
		cell1.setBorder(Rectangle.NO_BORDER);
		cell2.setBorder(Rectangle.NO_BORDER);
		cell3.setBorder(Rectangle.NO_BORDER);
		cell4.setBorder(Rectangle.NO_BORDER);
		cell5.setBorder(Rectangle.NO_BORDER);
		cell6.setBorder(Rectangle.NO_BORDER);
		cell7.setBorder(Rectangle.NO_BORDER);
		cell8.setBorder(Rectangle.NO_BORDER);
		cell9.setBorder(Rectangle.NO_BORDER);
		cell10.setBorder(Rectangle.NO_BORDER);
		cell11.setBorder(Rectangle.NO_BORDER);
		cell12.setBorder(Rectangle.NO_BORDER);
		cell13.setBorder(Rectangle.NO_BORDER);
		cell14.setBorder(Rectangle.NO_BORDER);
		cell15.setBorder(Rectangle.NO_BORDER);
		cell16.setBorder(Rectangle.NO_BORDER);
		cell17.setBorder(Rectangle.NO_BORDER);
		cell18.setBorder(Rectangle.NO_BORDER);
		
		PdfPCell cell19 = new PdfPCell(new Paragraph(" "));
		PdfPCell cell20 = new PdfPCell(new Paragraph(" "));
		
		// Code 3
		PdfPCell cell31 = new PdfPCell(new Paragraph(" "));
		PdfPCell cell27 = new PdfPCell(new Paragraph(" "));
		PdfPCell cell21 = new PdfPCell(new Paragraph(d1));
		PdfPCell cell22 = new PdfPCell(new Paragraph(d2));
		PdfPCell cell23 = new PdfPCell(new Paragraph(m1));
		PdfPCell cell24 = new PdfPCell(new Paragraph(m2));
		PdfPCell cell25 = new PdfPCell(new Paragraph(y1));
		PdfPCell cell26 = new PdfPCell(new Paragraph(y2));
		cell21.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell22.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell23.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell24.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell25.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell26.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell21.setBorder(Rectangle.NO_BORDER);
		cell22.setBorder(Rectangle.NO_BORDER);
		cell23.setBorder(Rectangle.NO_BORDER);
		cell24.setBorder(Rectangle.NO_BORDER);
		cell25.setBorder(Rectangle.NO_BORDER);
		cell26.setBorder(Rectangle.NO_BORDER);
		cell27.setBorder(Rectangle.NO_BORDER);
		cell31.setBorder(Rectangle.NO_BORDER);
		
		PdfPTable nestedTable = new PdfPTable(8);
		nestedTable.setWidthPercentage(100);
		nestedTable.addCell(cell31);
		nestedTable.addCell(cell27);
		nestedTable.addCell(cell21);
		nestedTable.addCell(cell22);
		nestedTable.addCell(cell23);
		nestedTable.addCell(cell24);
		nestedTable.addCell(cell25);
		nestedTable.addCell(cell26);
		
		cell13.setPaddingLeft(37);
		cell13.setPaddingRight(15);
		cell13.addElement(paragraph);
		//cell14.setVerticalAlignment(Element.ALIGN_TOP);
		//cell7.setHorizontalAlignment(Element.ALIGN_CENTER);
		//cell13.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell14.setFixedHeight(34f);
		cell14.setPaddingBottom(9);
		cell14.setPaddingLeft(35);
		cell14.setUseDescender(true);
		//cell14.setHorizontalAlignment(Element.ALIGN_CENTER);
		 cell14.setVerticalAlignment(Element.ALIGN_MIDDLE);
	
		cell4.setPaddingTop(8);
		//cell4.setRowspan(2);
		
		cell4.setFixedHeight(32f);
		cell4.addElement(nestedTable);
		
		table.addCell(cell17);
		table.addCell(cell18);
		table.addCell(cell1);
		
		//cell4.setFixedHeight(34f);
		
		
		table.addCell(cell4);
		
		// Code 3
		table.addCell(cell3);
		//bawah tarikh
		table.addCell(cell2);
		table.addCell(cell7);
		table.addCell(cell6);
		table.addCell(cell5);
		table.addCell(cell8);
		
		table.addCell(cell9);
		table.addCell(cell10);
		
		// Code 3
		table.addCell(cell13);
		table.addCell(cell14);
		table.addCell(cell11);
		table.addCell(cell12);
		table.addCell(cell15);
		table.addCell(cell16);
	
	
	
		// Code 5
		document.add(table);
		


document.close();	
	
}
// step 6: we output the writer as bytes to the response output
DataOutput output = new DataOutputStream( response.getOutputStream() );
byte[] bytes = buffer.toByteArray();
response.setContentLength(bytes.length);
for( int i = 0; i < bytes.length; i++ ) { output.writeByte( bytes[i] ); }
%>
