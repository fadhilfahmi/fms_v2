<%@page import="com.lcsb.fms.dao.financial.gl.FinancialReportDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ChartofAccountDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage=""%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) FinancialReportDAO.getModule();

%>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $(".goto").click(function () {
            var link = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + link,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });
        
        $(".setting-button").click(function () {
        
            var link = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + link,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            
            return false;
        });

    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <div class="row">
                <div class="col-lg-6">
                    <%
                    String settingbutton = "disabled";
                    if(log.getAccessLevel() < 2){// for user's access level superuser = 0 and administrator = 1
                        settingbutton = "";
                    }  
                    %>
                    <button id="reportconfig" class="btn btn-default btn-sm setting-button" <%=settingbutton%>><i class="fa fa-cogs" aria-hidden="true"></i> Report Settings</button>
                </div>
            </div>
                <br>
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Financial Report
                        </div>
                        <div class="panel-body">
                            <div class="list-group">
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp;Cover Page
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp;Manager's Report
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp;Manager's Report Appendix
                                </a>
                                <a href="balancesheet" class="list-group-item goto">
                                    <i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp;Balance Sheet - Sheet No 1
                                </a>
                                <a href="gllisting" class="list-group-item goto">
                                    <i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp;General Ledger Listing
                                </a>
                                <a href="trialbalance" class="list-group-item goto">
                                    <i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp;Trial Balance
                                </a>
                            </div>
                        </div>
                        <div class="panel-footer">
                        </div>
                    </div>
                </div>

            </div>        


            <br>


        </div>
    </div>
</div>