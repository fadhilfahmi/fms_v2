<%-- 
    Document   : cb_bankbook_view
    Created on : May 25, 2017, 12:32:19 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.financial.cashbook.BankBookMaster"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.BankBook"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.BankBookParam"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.BankBookDAO"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.ItemDetail"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.ItemMain"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.BankReconcile"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.BankReconcileReportMaster"%>
<%@page import="com.lcsb.fms.util.ext.ParseSafely"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.BankReportParam"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.BankReportDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.EstateDAO"%>
<%@page import="com.lcsb.fms.util.model.Estate"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) BankBookDAO.getModule();
    Estate est = EstateDAO.getEstateInfo(log, log.getEstateCode(), "estatecode");
    BankBookParam prm = (BankBookParam) session.getAttribute("bb_param");
    BankBookMaster master = (BankBookMaster) BankBookDAO.getMaster(log, prm);
    //BankReconcileReportMaster master = (BankReconcileReportMaster) BankReportDAO.getReconcileMaster(log, prm);
    //BankReconcile br = (BankReconcile) master.getBankReconcile();

%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $('#print').click(function (e) {
            printReport($("#viewprint").html());
        });

        $("#statementbal").keyup(function () {
            var tot = 0;
            var a = parseFloat($(this).val());
            var b = $('#total-outstanding-issued-cheque').val();
            var c = $('#total-outstanding-received-cheque').val();
            var d = $('#total-bankcharges-expense').val();
            var e = $('#total-bankcharges-receive').val();
            var f = $('#total-hibah').val();

            tot = parseFloat(a) - parseFloat(b) + parseFloat(c) + parseFloat(d) - parseFloat(e) - parseFloat(f);

            $('#totalg-outstanding-issued-cheque').val(parseFloat(a) - parseFloat(b));
            $('#totalg-outstanding-received-cheque').val(parseFloat(a) - parseFloat(b) + parseFloat(c));
            $('#totalg-bankcharges-expense').val(parseFloat(a) - parseFloat(b) + parseFloat(c) + parseFloat(d));
            $('#totalg-bankcharges-receive').val(parseFloat(a) - parseFloat(b) + parseFloat(c) + parseFloat(d) - parseFloat(e));
            $('#totalg-hibah').val(parseFloat(a) - parseFloat(b) + parseFloat(c) + parseFloat(d) - parseFloat(e) - parseFloat(f));
            $('#bankbookbal').val(tot);
            return false;
        });

        $("#statementbal").change(function () {
            var a = parseFloat($(this).val());
            $(this).val(a.toFixed(2));

            var b = parseFloat($('#bankbookbal').val());
            $('#bankbookbal').val(b.toFixed(2));

            var c = parseFloat($('#totalg-outstanding-issued-cheque').val());
            $('#totalg-outstanding-issued-cheque').val(c.toFixed(2));

            var d = parseFloat($('#totalg-outstanding-received-cheque').val());
            $('#totalg-outstanding-received-cheque').val(d.toFixed(2));

            var e = parseFloat($('#totalg-bankcharges-expense').val());
            $('#totalg-bankcharges-expense').val(e.toFixed(2));

            var f = parseFloat($('#totalg-bankcharges-receive').val());
            $('#totalg-bankcharges-receive').val(f.toFixed(2));

            var g = parseFloat($('#totalg-hibah').val());
            $('#totalg-hibah').val(g.toFixed(2));

            return false;
        });



    });
</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewbankbookparam" type="<%= request.getParameter("refer")%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>

                        <button id="print"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addinv" ><i class="fa fa-floppy-o"></i>&nbsp;Print</button>


                    </td>
                </tr>
            </table>
            <br>
            <div id="viewprint">
                <form data-toggle="validator" role="form" id="saveform">
                    <input type="hidden" name="estatecode" value="<%= log.getEstateCode()%>">
                    <input type="hidden" name="estatename" value="<%= log.getEstateDescp()%>">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td colspan="4">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="50px"><img src="<%= est.getLogopath()%>" width="40px" style="vertical-align:middle" ></td>
                                        <td><span class="bigtitlebold"><%= log.getEstateDescp()%></span><span class="pull-right"></span>
                                            <p class="font_inheader_normal">BANK BOOK<%//= tb.getTbtitle()%></p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="border_bottom"></td>
                        </tr>
                    </table>
                    <br>
                    <table  class="table-striped table-hover" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr> 
                            <td colspan="3">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr> 
                                        <td width="18%"><strong><font size="1" face="Arial, Helvetica, sans-serif">Bank 
                                                Name:</font></strong></td>
                                        <td width="49%"><strong><font size="1" face="Arial, Helvetica, sans-serif"><%= prm.getBankname()%> 
                                                <input type="hidden" name="codebank" value="<%= prm.getBankcode()%>">
                                                <input type="hidden" name="bname" value="<%= prm.getBankname()%>">
                                                <input type="hidden" name="depan" value="<%//=depan%>">
                                                </font></strong></td>
                                        <td width="7%"><strong><font size="1" face="Arial, Helvetica, sans-serif">Year:</font></strong></td>
                                        <td width="10%"><strong><font size="1" face="Arial, Helvetica, sans-serif"><%= prm.getYear()%> 
                                                <input type="hidden" name="year" value="<%= prm.getYear()%>">
                                                </font></strong></td>
                                        <td width="11%"><strong><font size="1" face="Arial, Helvetica, sans-serif">Period:</font></strong></td>
                                        <td width="5%"><strong><font size="1" face="Arial, Helvetica, sans-serif"><%= prm.getPeriod()%> 
                                                <input type="hidden" name="period" value="<%= prm.getPeriod()%>">
                                                </font></strong></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr> 
                            <td><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
                            <td colspan="2"><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
                        </tr>

                    </table>


                    <%
                        //List<ItemMain> listAll = master.getListItemMain();
                        //for (ItemMain j : listAll) {
                    %>
                    <table  class="table-striped table-hover" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">

                        <tr> 
                            <th width="4%" class="smalltd">No.</th>
                            <th width="10%" class="smalltd">Date</th>
                            <th width="10%" class="smalltd">Reference No</th>
                            <th width="20%" class="smalltd">Description</th>
                            <th width="14%" class="smalltd">Acc. Code</th>
                            <th width="14%" class="smalltd">Sub Code</th>
                            <th width="10%" class="smalltd">Cheque No</th>
                            <th width="6%" class="smalltd">Received</th>
                            <th width="6%" class="smalltd">Paid</th>
                            <th width="6%" class="smalltd">Balance</th>
                        </tr>
                        <tr> 
                            <td><div align="center"><font size="1" face="Arial, Helvetica, sans-serif"></font></div></td>
                            <td class="smalltd"></td>
                            <td class="smalltd"></td>
                            <td class="smalltd"></td>
                            <td class="smalltd"></td>
                            <td class="smalltd"></td>
                            <td class="smalltd"></td>
                            <td class="smalltd" colspan="2"><span class="pull-right">B/F</span></td>

                            <td class="smalltd"><span class="pull-right"><%=  GeneralTerm.normalCredit(master.getBfAmount())%></span></td>
                        </tr>
                        <%
                            List<BankBook> listDetail = master.getListBankBook();
                            if (listDetail.isEmpty()) {%>
                        <tr>
                            <td colspan="10">
                                <span class="font-small-red"><font size="1" face="Arial, Helvetica, sans-serif">No data available.</font></span>
                            </td>
                        </tr>

                        <%}
                            int bil = 1;
                            double totalE = 0.0;
                            double totalR = 0.0;
                            for (BankBook i : listDetail) {

                                double amountR = 0.0;
                                double amountE = 0.0;

                                if (i.getStatus().equals("E")) {
                                    amountE = i.getPvamount();
                                    totalE += amountE;
                                } else if (i.getStatus().equals("R")) {
                                    amountR = i.getPvamount();
                                    totalR += amountR;
                                }

                        %>

                        <tr> 
                            <td><div align="center"><font size="1" face="Arial, Helvetica, sans-serif"><%=bil++%></font></div></td>
                            <td class="smalltd"><%= AccountingPeriod.fullDateMonth(i.getPvdate())%></td>
                            <td class="smalltd"><%= i.getPvid()%></td>
                            <td class="smalltd"><%= i.getPvremarks()%></td>
                            <td class="smalltd"><%=  i.getListcoa()%></td>
                            <td class="smalltd"><%=  i.getSacode()%></td>
                            <td class="smalltd"><%=  i.getListcek()%></td>
                            <td class="smalltd"><span class="pull-right"><%=  GeneralTerm.currencyFormat(amountR)%></span></td>
                            <td class="smalltd"><span class="pull-right"><%=  GeneralTerm.currencyFormat(amountE)%></span></td>

                            <td class="smalltd"><%//=  GeneralTerm.currencyFormat(amountR) %></td>
                        </tr>
                        <%}
                            String fontcolor = "";
                            String alert = "";
                            double gTotal = 0.0;
                            double totalAll = master.getBfAmount() + totalR - totalE;
                            double amountCF = master.getCfAmount();

                            if (GeneralTerm.currencyFormat(totalAll).equals(GeneralTerm.currencyFormat(amountCF))) {
                                gTotal = totalAll;
                            } else {
                                gTotal = totalAll;
                                fontcolor = "font-small-red";
                                alert = "* Total Amount is different with Closing Balance Amount ("+ GeneralTerm.currencyFormat(amountCF) +")";
                            }


                        %>
                        <tr> 
                            <td><div align="center"><font size="1" face="Arial, Helvetica, sans-serif"></font></div></td>
                            <td class="smalltd"></td>
                            <td class="smalltd"></td>
                            <td class="smalltd"></td>
                            <td class="smalltd"></td>
                            <td class="smalltd"></td>
                            <td class="smalltd"></td>
                            <td class="smalltd"><span class="pull-right"><strong><%= GeneralTerm.normalCredit(totalR)%></strong></span></td>
                            <td class="smalltd"><span class="pull-right"><strong><%= GeneralTerm.normalCredit(totalE)%></strong></span></td>
                            <td class="smalltd"><span class="pull-right"><strong><%//= GeneralTerm.normalCredit(master.getBfAmount() + totalR - totalE)%></strong></span></td>
                        </tr>
                        <tr> 
                            <td><div align="center"><font size="1" face="Arial, Helvetica, sans-serif"></font></div></td>
                            <td class="smalltd" colspan="6"><span class="pull-right <%= fontcolor%>"><font size="1" face="Arial, Helvetica, sans-serif"><%= alert %></font></span></td>
                            <td class="smalltd" colspan="2"><span class="pull-right">B/F</span></td>

                            <td class="smalltd"><span class="pull-right <%= fontcolor%>"><font size="1" face="Arial, Helvetica, sans-serif"><strong><%=  GeneralTerm.normalCredit(gTotal)%></strong></font></span></td>
                        </tr>
                    </table>
                    <%//}%>

                </form>
            </div>
        </div>
    </div>
</div>