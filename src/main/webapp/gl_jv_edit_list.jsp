<%-- 
    Document   : gl_jv_edit_list
    Created on : Nov 18, 2016, 10:45:06 AM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucherMaster"%>
<%@page import="com.lcsb.fms.ui.UIConfig"%>
<%@page import="com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO"%>
<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucherItem"%>
<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucher"%>
<%@page import="com.lcsb.fms.dao.financial.gl.JournalVoucherDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
//OfficialCreditItem vi = (OfficialCreditItem) OfficialReceiptDAO.getINVitem(request.getParameter("refer"));
    JournalVoucher v = (JournalVoucher) JournalVoucherDAO.getJV(log, request.getParameter("refer"));
//JournalVoucherItem cek = (JournalVoucherItem) OfficialReceiptDAO.getCheque(request.getParameter("refer"));
    Module mod = (Module) JournalVoucherDAO.getModule();

    String status = JournalVoucherDAO.getStatus(log, v.getJVrefno());

    String distribute = request.getParameter("distribute");
%>

<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        window.scrollTo(0, 0);

        callPnotify('<%= request.getParameter("status")%>', '<%= request.getParameter("text")%>');

        $('#print').click(function (e) {
            printElement($("#viewjv").html());
        });
        

        var oTable = $('#list').DataTable({
            responsive: true,
            mark: true,
            "aaSorting": [[1, "asc"]],
            "dom": '<"toolbar">frtip',
        });
        
    
        $("div#list_filter").append('<button  class="btn btn-default btn-xs addaccount pull-left" title="<%= mod.getModuleID()%>" id="<%= v.getJVrefno()%>" name="addaccount"><i class="fa fa-plus-circle" aria-hidden="true"></i> Account</button>');

     

        $(".adddebitnote").click(function (e) {
            //e.preventDefault();
            var referno = $(this).attr('id');
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Debit Note',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {


                    var $content = $('<body></body>').load('list_ardebitnote.jsp?referno=' + referno);
                    $('body').on('click', '.thisresult-debitnote', function (event) {
                        event.preventDefault();

                        dialog.close();

                    });

                    return $content;
                }
            });

            return false;
        });

        $(".addaccount").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addaccount&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });


    });
</script>


            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
                </tr>
            </table>
            <br>
            
            <div class="row">
    
    
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
            <%= UIConfig.renderBanner(log, v.getJVrefno(), mod.getModuleID())%>
            <table class="table table-bordered table-striped  table-hover">
                <thead>
                    <tr>
                        <th colspan="4">Voucher Information
                            <!--<small><span class="label label-primary">Preparing</span></small>-->
                            <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">

                                <%if (!status.equals("Approved")) {
                                    if ((JournalVoucherDAO.isCheck(log, v.getJVrefno()) && log.getListUserAccess().get(2).getAccess())) {%>

                                <button  class="btn btn-default btn-xs approve" title="<%= mod.getModuleID()%>" id="<%= v.getJVrefno()%>"><i class="fa fa-check-square-o" aria-hidden="true"></i> Approve</button>

                                <%} else if (!JournalVoucherDAO.isApprove(log, v.getJVrefno()) && log.getListUserAccess().get(1).getAccess()) {%> 

                                <button  class="btn btn-default btn-xs check" title="<%= mod.getModuleID()%>" id="<%= v.getJVrefno()%>" name="addrefer"><i class="fa fa-check-square-o" aria-hidden="true"></i> Check</button>

                                <%}%>
                                <button id="<%= v.getJVrefno()%>" class="btn btn-default btn-xs viewgl" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-list-alt" aria-hidden="true"></i> View in GL</button>
                                <button id="<%= v.getJVrefno()%>" class="btn btn-default btn-xs viewvoucher" title="<%= mod.getModuleID()%>" name="viewjv"><i class="fa fa-print" aria-hidden="true"></i></button>
                                <button id="<%= v.getJVrefno()%>" class="btn btn-default btn-xs editmain" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                <button id="<%= v.getJVrefno()%>"  class="btn btn-danger btn-xs deletemain" title="<%= mod.getModuleID()%>" id="<%//= pvi.getRefer() %>" name="delete_st"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                    <%}else{%>
                                <button id="<%= v.getJVrefno()%>" class="btn btn-default btn-xs viewgl" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-list-alt" aria-hidden="true"></i> View in GL</button>
                                <button id="<%= v.getJVrefno()%>" class="btn btn-default btn-xs viewvoucher" title="<%= mod.getModuleID()%>" name="viewjv"><i class="fa fa-print" aria-hidden="true"></i></button>
                                    <%}%>    
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody class="activerowm tdrow">
                    <tr>
                        <th width="15%">Status</th>
                        <td width="35%"><%= GeneralTerm.getStatusLabel(JournalVoucherDAO.getStatus(log, v.getJVrefno()))%>
                            <%= DebitCreditNoteDAO.checkDCNoteExist(log, v.getJVrefno())%></td>
                        <th width="15%">Date</th>
                        <td width="35%"><%= v.getJVdate()%></td>
                    </tr>
                    <tr>
                        <th>Reference No</th>
                        <td><%= v.getJVrefno()%></td>
                        <th>Period</th>
                        <td><%= v.getYear()%>, <%= v.getCurperiod()%></td>
                    </tr>
                    <tr>
                        <th>Debit</th>
                        <td><% if (GeneralTerm.currencyFormat(Double.parseDouble(v.getTodebit())).equals(GeneralTerm.currencyFormat(Double.parseDouble(v.getTocredit())))) {%><i class="fa fa-check right-green"></i><%}%>&nbsp;<strong><%= GeneralTerm.currencyFormat(Double.parseDouble(v.getTodebit()))%></strong></td>
                        <th>Credit</th>
                        <td><% if (GeneralTerm.currencyFormat(Double.parseDouble(v.getTodebit())).equals(GeneralTerm.currencyFormat(Double.parseDouble(v.getTocredit())))) {%><i class="fa fa-check right-green"></i><%}%>&nbsp;<strong><%= GeneralTerm.currencyFormat(Double.parseDouble(v.getTocredit()))%></strong></td>
                    </tr>
                    <tr>
                        <th>Reason</th>
                        <td colspan="3"><%= v.getReason()%></td>
                    </tr>

                    <tr>
                        <th>Prepare By</th>
                        <td colspan="3"><%= v.getPreparedbyid()%> - <%= v.getPreparedbyname()%></td>
                    </tr>

                </tbody>
            </table>



            <div class="invoice-content">
                <div class="table-responsive">
                    <table class="table table-invoice" id="list">
                        <thead>
                            
                            <tr>
                                <th>JOURNAL VOUCHER DETAIL</th>
                                <th><span class="pull-left">ACCOUNT CODE</span></th>
                                <th><span class="pull-left">DEBIT</span></th>
                                <th><span class="pull-left">CREDIT</span></th>
                                <th><span class="pull-left">ACTION</span></th>
                            </tr>
                        </thead>
                        <tbody>

                            <%List<JournalVoucherItem> listAll = (List<JournalVoucherItem>) JournalVoucherDAO.getAllJVItem(log, v.getJVrefno());
                   if (listAll.isEmpty()) {%>
                            <tr>
                                <td colspan="6">
                                    <span class="font-small-red">No data available.</span>
                                </td>
                            </tr>

                            <%}

                                double totalDebit = 0.0;
                                double totalCredit = 0.0;
                                for (JournalVoucherItem j : listAll) {

                                    totalDebit += j.getDebit();
                                    totalCredit += j.getCredit();
                            %>
                            <tr>
                                <td>
                                    <%= j.getJvid()%><br />
                                    <small><%= j.getRemark()%></small>
                                </td>
                                <td><span class="pull-left"><%= j.getActcode()%> - <%= j.getActdesc()%></span><br />
                                    <small><%= j.getSatype()%> / <%= j.getSacode()%> / <%= j.getSadesc()%></small></td>
                                <td><span class="pull-right"><%= GeneralTerm.currencyFormat(j.getDebit())%></span></td>
                                <td><span class="pull-right"><%= GeneralTerm.currencyFormat(j.getCredit())%></span></td>
                                <td>
                                    <%if (!status.equals("Approved")) {%>
                                    <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
                                        <button id="<%= j.getJvid()%>" class="btn btn-default btn-xs edititem" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-pencil"></i></button>
                                        <button id="<%= j.getJvid()%>" class="btn btn-default btn-xs deleteitem" title="<%= mod.getModuleID()%>"  name="delete_st"><i class="fa fa-trash-o"></i></button>
                                    </div>
                                    <%}%>
                                </td>
                            </tr>
                            <%}%>

                        </tbody>
                        <thead>
                            <tr>
                                <th>TOTAL</th>
                                <th><span class="pull-right"><%//= GeneralTerm.currencyFormat( totQty )%></span></th>
                                <th><span class="pull-right"><%= GeneralTerm.currencyFormat(totalDebit)%></span></th>
                                <th><span class="pull-right"><%= GeneralTerm.currencyFormat(totalCredit)%></span></th>
                                <th><span class="pull-right"><%//= GeneralTerm.currencyFormat( totGrand )%></span></th>
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>    
