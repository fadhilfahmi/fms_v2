<%-- 
    Document   : newdt
    Created on : May 5, 2017, 3:18:40 PM
    Author     : fadhilfahmi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">

        <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {
                var oTable = $('#list').DataTable({
                    responsive: true,
                    "aaSorting": [[0, "desc"]],
                });
            });
        </script>
    </head>
    <body>
        <table class="table table-bordered table-striped table-hover" id="list">
            <thead>
                <tr>

                    <th>name</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
                <tr>
                    <td>fadhil</td>
                    <td>fadhil</td>
                    <td>fadhil</td>
                </tr>
            </tbody>

        </table>
    </body>
</html>
