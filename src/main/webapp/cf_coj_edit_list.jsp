<%-- 
    Document   : cf_coj_edit_list
    Created on : Jun 1, 2016, 3:53:13 PM
    Author     : user
--%>

<%@page import="com.lcsb.fms.dao.setup.configuration.ChartofJobDAO"%>
<%@page import="com.lcsb.fms.model.setup.configuration.ChartofJob"%>
<%@page import="com.lcsb.fms.model.setup.configuration.ChartofJobItem"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
ChartofJobItem vi = (ChartofJobItem) ChartofJobDAO.getCJIitem(log,request.getParameter("referenceno"));
ChartofJob v = (ChartofJob) ChartofJobDAO.getCJ(log,request.getParameter("referenceno"));
Module mod = (Module) ChartofJobDAO.getModule();
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
           
            

              $( ".editmain" ).click(function() {
                  //var a = $("form").serialize();
                  var a = $(this).attr('id');
                  var b = $(this).attr('href');
                 
                 $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=editmain&referenceno="+a,
                                            success: function (result) {
                                            // $("#haha").html(result);
                                            $('#herey').empty().html(result).hide().fadeIn(300);
                                        }});
                                        return false;
               
                   
              });
              
              $( ".edititem" ).click(function() {
                  //var a = $("form").serialize();
                  var a = $(this).attr('id');
                  var b = $(this).attr('href');
                 
                 $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=edititem&referenceno="+a,
                                            success: function (result) {
                                            // $("#haha").html(result);
                                            $('#herey').empty().html(result).hide().fadeIn(300);
                                        }});
                                        return false;
               
                   
              });
              $( ".viewitem" ).click(function() {
                  //var a = $("form").serialize();
                  var a = $(this).attr('id');
                  var b = $(this).attr('href');
                 BootstrapDialog.show({
                                    type:BootstrapDialog.TYPE_DEFAULT,
                                     title: 'View <%= mod.getModuleDesc() %>',
                                     //message: $('<body></body>').load('list_coa.jsp')
                                     message: function(dialog) {
                                         var $content = $('<body></body>').load('cf_coj_view_item.jsp?id='+a);
                                         //$('body').on('click', '.thisresult_nd', function(event){
                                         //    dialog.close();
                                         //    event.preventDefault();
                                         //});

                                         return $content;
                                     }
                                 });
                                        return false;
               
                   
              });
             
              
              $( ".deletecvitem" ).click(function() {
                  //var a = $("form").serialize();
                  var a = $(this).attr('id');
                  var b = $(this).attr('href');
                 
                 BootstrapDialog.confirm({
                                            title: 'Confirmation',
                                            message: 'Are you sure to delete?',
                                            type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                            closable: true, // <-- Default value is false
                                            draggable: true, // <-- Default value is false
                                            btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                            btnOKLabel: 'Delete', // <-- Default value is 'OK',
                                            btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                                            callback: function(result) {
                                                // result will be true if button was click, while it will be false if users close the dialog directly.
                                                if(result) {
                                                    $.ajax({
                                                        url: "PathController?moduleid=010111&process=deleterqitem&referenceno="+a,
                                                        success: function (result) {
                                                        $('#herey').empty().html(result).hide().fadeIn(300);
                                                        }
                                                    });
                                                }
                                            }
                                        });
                 
                 
                
                                        return false;
               
                   
              });
              
              $( ".gotodelete" ).button({
                    icons: {
                      primary: "ui-icon-trash"
                    }
              })
              .click(function() {
                  //var a = $("form").serialize();
                  var a = $(this).attr('id');
                  var b = $(this).attr('href');
                  
                  $.ajax({
                        url: "PathController?moduleid=<%//=moduleid%>&process=deletesub&referenceno="+a+"&type="+b,
                        success: function (result) {
                        // $("#haha").html(result);
                        $('#maincontainer').empty().html(result).hide().fadeIn(300);
                    }});
                    return false;
               
                    
              });
              
              
                
              
                 
                 
                 
                  
                 
                 
              
         });
        </script>
            <div id ="maincontainer">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
              <td width="74%" class="borderbot" align="left"><span class="bigfonttitle"><%= mod.getModuleDesc() %></span>&nbsp;&nbsp;<span class="midfonttitle">Editing List</span></td>
		<td width="26%" class="borderbot" align="right">&nbsp;</td>
	  </tr>
  </table>
	<br>
   <div class="partition"> 
       <div class="headofpartition"></div>
       <div class="bodyofpartition">
 <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
			<td valign="middle" align="left">
				<button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                                
			</td>
		</tr>
  </table>
  <br>
  
 <table class="table table-bordered table-striped" id="dataTables">
  
  <%
  List<ChartofJobItem> listAll = (List<ChartofJobItem>) ChartofJobDAO.getAllCJIItem(log,v.getCode());
    for (ChartofJobItem j : listAll) {
  %>
  <tr>
    <td class="tdrow">&nbsp;&nbsp;&nbsp;<%= j.getAccountcode()%></td>
    <td class="tdrow">&nbsp;<%= j.getRemarks()%> <%//= j.getCoadescp()%></td>
    <td class="tdrow">&nbsp;<%= j.getJobcode()%> <td>
    <td class="tdrow" align="right">&nbsp;
        <!--<a href="sub" id="<%//= pvi.getRefer() %>" class="goto">Edit</a>&nbsp;
        <a href="sub" id="<%//= pvi.getNovoucher() %>" class="gotodelete">Delete</a>-->
        
    <div class="btn-group btn-group-xs" role="group" aria-label="...">
                               
                                <button id="<%= j.getId()%>" class="btn btn-default btn-xs viewitem" title="<%= mod.getModuleID() %>" id="<%//= pvi.getRefer() %>" name="et"><i class="fa fa-folder-open"></i></button>
                                <button id="<%= j.getId()%>" class="btn btn-default btn-xs edititem" title="<%= mod.getModuleID() %>" id="<%//= pvi.getRefer() %>" name="viewlist"><i class="fa fa-pencil"></i></button>
                                <button id="<%= j.getId() %>"  class="btn btn-default btn-xs deletecvitem" title="<%= mod.getModuleID() %>" id="<%//= pvi.getRefer() %>" name="addjv"><i class="fa fa-trash-o"></i></button>
                              </div>
    </td>
  </tr>
  <%
    }     
  %>
</table>

     </div>
     </div>
      </div>