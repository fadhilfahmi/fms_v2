<%-- 
    Document   : cb_cv_view_voucher
    Created on : Mar 21, 2016, 11:22:17 AM
    Author     : Dell
--%>


<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.CashVoucher"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.CashVoucherDAO"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.CashVoucherAccount"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.dao.LocationDAO"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucher"%>
<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucherItem"%>
<%@page import="com.lcsb.fms.dao.financial.gl.JournalVoucherDAO"%>
<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    CashVoucherAccount vi = (CashVoucherAccount) CashVoucherDAO.getCVitem(log,request.getParameter("referno"));
    CashVoucher v = (CashVoucher) CashVoucherDAO.getCV(log,request.getParameter("referno"));
    Module mod = (Module) CashVoucherDAO.getModule();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/pvoucher.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
    </head>
    <body>
	
        
        
            <div id ="maincontainer">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
              <td width="74%" class="borderbot" align="left"><span class="bigfonttitle">View Cash Voucher</span>&nbsp;&nbsp;<span class="midfonttitle"></span></td>
		<td width="26%" class="borderbot" align="right">&nbsp;</td>
	  </tr>
  </table>
	<br>
   <div class="partition"> 
       <div class="headofpartition"></div>
       <div class="bodyofpartition">
  <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
			<td valign="middle" align="left">
                                
                             
			</td>
		</tr>
  </table>

  
    <table id="maintbl" width="100%" cellspacing="0">
            <tr>
                <td valign="top">
                    <table id="table_left" width="100%" cellspacing="0">
                        <tr>
                            <td valign="middle" width="33%" align="left">
                                <div class="btn-group btn-group-sm" role="group" aria-label="...">
                                <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                                <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit</button>
                                <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="addjv"><i class="fa fa-print"></i>&nbsp;Print</button>
                              </div>
                                
                            </td>
                            <td width="33%" align="right">
                                <div id="totalpage">
                                    <select id="page">
                                        <option value="'+i+'"></option>
                                    </select> 
                                </div>
                                </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="table_left" width="100%" cellspacing="0"><tr><td width="100%">
                        <div id="viewjv">
                            
                            <table width="100%" border="0" cellspacing="0" cellpadding="10"><tr><td><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="50%"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="4%"><img src="image/logo_lkpp.jpg" width="25" height="25"></td><td width="96%">&nbsp;<span class="comnamevc"><strong>LKPP CORPORATION SDN BHD</strong></span></td></tr></table></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                            <td align="right"><strong>CASH VOUCHER</strong></td></tr><tr><td><p class="medfont2">&nbsp;</p><p class="medfont3">No 45/4, Jalan Teluk Sisek</p><p class="medfont3">Peti surat 144</p><p class="medfont3">25710 Kuantan</p><p class="medfont3">Pahang Darul Makmur</p><p class="medfont3">&nbsp;</p><p class="medfont3">Tel : 09-5165180</p></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td valign="top">
			
			</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left" class="bordertopgrey" width="40%">&nbsp;<span class="boldtitle">PAGE</span></td>
                                    <td class="bordertoprightgrey" width="60%">&nbsp;<span class="normtitle">1</span> 
                                    
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="borderleftbottomgrey">&nbsp;<span class="boldtitle">REFERENCE NO</span></td>
                                    <td class="borderleftbottomrightgrey">&nbsp;<span class="normtitle"><%= v.getRefer()%></span></td>
                                </tr>
                                <tr>
                                    <td align="left" class="borderleftbottomgrey">&nbsp;<span class="boldtitle">DATE</span></td>
                                    <td class="borderleftbottomrightgrey">&nbsp;<span class="normtitle"><%= v.getVoucherdate()%></span></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left" class="borderleftbottomgrey">&nbsp;<div class="amount">RM<%= v.getTotal() %></div></td>
                                  </tr>
                            </table>
                        </td>
                     </tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td align="right">&nbsp;<span class="boldtitle" id="noprint"></span></td></tr></table></td></tr><tr><td><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
			  <td colspan="9" class="forlisttitle"><span class="boldtitle">&nbsp;List of Account</span></td></tr><tr><td width="2%" class="bordertop">&nbsp;<span class="intableheader_jv">No</span></td><td width="15%" class="bordertop">&nbsp;<span class="intableheader_jv">Account Code</span></td><td width="11%" class="bordertoprightgrey">&nbsp;<span class="intableheader_jv">Loc Code</span></td><td width="11%" class="bordertoprightgrey">&nbsp;<span class="intableheader_jv">SA Code</span></td><td width="32%" class="bordertoprightgrey">&nbsp;<span class="intableheader_jv">Remarks</span></td>
			    <td width="9%" class="bordertoprightgrey">&nbsp;<span class="intableheader_jv">Amount</span></td>
			    <td width="9%" class="bordertoprightgrey">&nbsp;<span class="intableheader_jv">Tax</span></td>
			    <td width="9%" class="bordertoprightgrey">&nbsp;<span class="intableheader_jv">Total</span></td></tr>

                        
			
			<!--<tr height="20px"><td class="borderleft">&nbsp;<span class="fontjv_item"></span></td>
			<td class="borderleft">&nbsp;<span class="fontjv_item"></span></td>
			<td class="borderrightend" align="right">&nbsp;<span class="fontjv_item"></span>&nbsp;</td>
			<td class="borderrightend" align="right">&nbsp;<span class="fontjv_item"></span>&nbsp;</td>
			<td class="borderrightend" align="right">&nbsp;<span class="fontjv_item">b/f</span>&nbsp;</td>
			<td class="borderrightend" align="right">&nbsp;<span class="fontjv_item">'+round_decimals(parseFloat(tot_debt),2)+'</span>&nbsp;</td> 
			<td class="borderrightend" align="right">&nbsp;<span class="fontjv_item">'+round_decimals(parseFloat(tot_cred),2)+'</span>&nbsp;</td></tr>-->
			
                        <%
                        int i = 0;
                        List<CashVoucherAccount> slist = (List<CashVoucherAccount>) CashVoucherDAO.getAllCVItem(log,request.getParameter("referno"));
                        for (CashVoucherAccount c : slist) {
                            i++;
                        %>
			<tr height="20px"><td class="borderleft">&nbsp;<span class="fontjv_item"><%= i %></span></td>
                            <td class="borderleft">&nbsp;<span class="fontjv_item"><%= c.getCoacode()%></span></td>
                            <td class="borderrightend" align="right"><span class="fontjv_item"><%= c.getLoccode() %></span></td>
                            <td class="borderrightend" align="right"><span class="fontjv_item"><%= c.getSacode() %></span></td>
                            <td class="borderrightend" align="left"><span class="fontjv_item"><%= c.getRemarks()%></span></td>
                            <td class="borderrightend" align="right"><span class="fontjv_item"><%= GeneralTerm.currencyFormat(Double.parseDouble(c.getDebit())) %></span></td>
                            <td class="borderrightend" align="right"><span class="fontjv_item"><%= GeneralTerm.currencyFormat(c.getTaxamt()) %></span></td> 
                            <td class="borderrightend" align="right"><span class="fontjv_item"><%= GeneralTerm.currencyFormat(Double.parseDouble(c.getDebit())) %></span></td>
                        </tr>
                        <%}%>
                        
			<tr><td colspan="9"></td></tr>
			<tr><td colspan="9"></td></tr>
		
		<tr><td class="borderleft">&nbsp;</td><td class="borderleft"  valign="top">&nbsp;</td><td class="borderrightend" align="right">&nbsp;</td>
		<td class="borderrightend" align="right">&nbsp;</td><td class="borderrightend" align="right">&nbsp;</td>
		<td class="borderrightend" align="right">&nbsp;</td>
		<td class="borderrightend" align="right">&nbsp;</td>  <td class="borderrightend" align="right">&nbsp;</td></tr>     
	  

		<tr><td class="borderleft">&nbsp;</td><td class="borderleft">&nbsp;</td><td class="borderrightend">&nbsp;</td>
		<td class="borderrightend">&nbsp;</td><td class="borderrightend">&nbsp;</td>
		<td class="borderrightend">&nbsp;</td>
		<td class="borderrightend">&nbsp;</td><td class="borderrightend">&nbsp;</td></tr><tr><td class="borderbottombold">&nbsp;</td><td class="borderbottombold" valign="top">&nbsp;</td><td align="right" class="borderbottomboldright">&nbsp;</td>
		<td align="right" class="borderbottomboldright">&nbsp;</td><td align="right" class="borderbottomboldright" valign="top">&nbsp;<span class="boldtitle">TOTAL</span>&nbsp;</td>
		<td align="right" class="borderbottomboldright"><span class="totalamount_jv">&nbsp;&nbsp;<strong><%= GeneralTerm.currencyFormat(CashVoucherDAO.getSum(log,"debit", request.getParameter("referno"))) %>
		
		</strong></span></td>
                <td align="right" class="borderbottomboldright"><span class="totalamount_jv">&nbsp;&nbsp;<strong><%= GeneralTerm.currencyFormat(CashVoucherDAO.getSum(log,"taxamt", request.getParameter("referno"))) %>
		</strong></span>&nbsp;</td>


                <td align="right" class="borderbottomboldright"><span class="totalamount_jv">&nbsp;&nbsp;<strong><%= GeneralTerm.currencyFormat(CashVoucherDAO.getSum(log,"debit", request.getParameter("referno"))) %>
		
		</strong></span>&nbsp;</td>
	  
	 </tr></table></td></tr><tr><td></td></tr><tr>
                                    <td height="30px">&nbsp;<span class="boldtitle">Year</span> - <%= v.getYear()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="boldtitle">Period</span> - <%= v.getPeriod()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="boldtitle">No</span> - <%= v.getVoucherid()%></td></tr><tr><td><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="25%" class="bordertopgreyauth"><p class="paytofonttop">&nbsp;&nbsp;</p><p class="paytofonttop">&nbsp;&nbsp;_________________________</p>
	     <p class="paytofontmiddle">&nbsp;&nbsp;<strong>RECEIVED BY</strong></p></td><td width="25%" class="bordertopgreyauthright"><p class="paytofonttop">&nbsp;&nbsp;</p><p class="paytofonttop">&nbsp;&nbsp;_________________________</p><p class="paytofontmiddle">&nbsp;&nbsp;<strong>APPROVED BY</strong></p></td></tr>

	  <tr><td class="borderonesidegreyauth" valign="top"><p class="paytofonttop">&nbsp;&nbsp;Name</p><p class="paytofontmiddle">&nbsp;&nbsp;<strong><%= v.getReceivebyname()%> - <%= v.getReceiveid() %></strong></p>
	    <p class="paytofonttop">&nbsp;&nbsp;Type</p><p class="paytofontmiddle">&nbsp;&nbsp;<strong><%= v.getReceivetype()%></strong></p></td>
	    <td class="borderonesidegreyauthright" valign="top"><p class="paytofontmiddle">&nbsp;&nbsp;</p><p class="paytofonttop">&nbsp;&nbsp;Name</p><p class="paytofontmiddle">&nbsp;&nbsp;<strong><%= v.getApprobyname()%> - <%= v.getAppid() %></strong></p>
	      <p class="paytofonttop">&nbsp;&nbsp;Designation</p><p class="paytofontmiddle">&nbsp;&nbsp;<strong><%= v.getAppdesign()%></strong></p>
	      <p class="paytofontmiddle">&nbsp;&nbsp;</p><p class="paytofonttop">&nbsp;&nbsp;Date</p><p class="paytofontmiddle">&nbsp;&nbsp;<strong><%= v.getApprovedate()%></strong></p></td></tr>
	  <tr><td class="borderleftbottomgrey">&nbsp;</td><td class="borderleftbottomgreyright">&nbsp;</td></tr></table></td></tr></table>
                            
			</div>
                    </table>
                </td>
            </tr>
            <input type="hidden" id="checkbyid" value="">
            <input type="hidden" id="checkbyname" value="">
            <input type="hidden" id="checkbydesign" value="">
            <input type="hidden" id="checkdate" value="">
	</table>

   
        </div>
    </div>
</div>
    </body>
</html>

