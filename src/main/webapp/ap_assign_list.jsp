<%-- 
    Document   : ap_vendor_list
    Created on : Oct 11, 2016, 3:09:03 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page import="com.lcsb.fms.dao.financial.ap.VendorAssignDAO"%>
<%@page import="com.lcsb.fms.util.model.ListTable"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.util.model.ModuleItem"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.ConnectionModel"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,java.util.*" errorPage="" isThreadSafe="false" %>
<%@ page import="java.util.Date"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
Module mod = (Module) VendorAssignDAO.getModule();

response.setHeader("Cache-Control", "no-cache");

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>IPAMIS</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/tether.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>
<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">
<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
    
				
var updateTable = '<%= mod.getMainTable() %>';

<%
List<ListTable> mlist = (List<ListTable>) VendorAssignDAO.tableList();
for (ListTable m : mlist) {
    //if(m.getList_View().equals("1")){
        %>
        columntoView.push('<%= m.getColumnName()%>');
        columnName.push('<%= m.getTitleName()%>');
        <%    
    //}
    
}
%>
		
var colsTosend = 'cols=';
for (m = 0; m < columntoView.length; ++m) {//put value from database to variable jget
if(m == columntoView.length - 1){
colsTosend += columntoView[m];
}else{
colsTosend += columntoView[m]+'&cols=';
}
}


	  $( "#add-new" ).click(function() {
                                        $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=addnew",
                                            success: function (result) {
                                            // $("#haha").html(result);
                                            $('#herey').empty().html(result).hide().fadeIn(300);
                                        }});
                                        return false;
	  });

	  
          $( "#check-button" ).click(function() {
                $.ajax({
                    url: "PathController?moduleid=<%= mod.getModuleID() %>&process=check",
                    success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
                return false;
	  });
          
            
	
	  var orderby = 'order by voucherno desc';
	  var oTable = $('#example').DataTable( {
                "aaSorting": [[ 0, "desc" ]],
                "responsive": true,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "retrievetable.jsp?table="+updateTable+"&code=<%= mod.getReferID_Master() %>&"+colsTosend+"&order="+orderby,
		"aoColumns": [
		  {"sTitle": columnName[0], "mData": columntoView[0], "sWidth": "9%", "bSearchable": true},
		  {"sTitle": columnName[1], "mData": columntoView[1], "sWidth": "25%"},
		  {"sTitle": columnName[2], "mData": columntoView[2], "sWidth": "9%"},
                  {"sTitle": columnName[3], "mData": columntoView[3], "sWidth": "15%"},
		  {"sTitle": "Action", "mData": null, "bSortable": false, "bSearchable": false, "sWidth": "13%"}
			
			],
		  "aoColumnDefs": [ {
			"aTargets": [4],
			  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
			  	
				var a = $('<%= Button.viewlistButton() %>');
                                var b = $('<%= Button.editlistButton() %>');
                                var c = $('<%= Button.deletelistButton() %>');
				
				a.on('click', function() {
				  //console.log(oData);
				  $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=view&referno="+oData.suppcode,
                                            success: function (result) {
                                            $('#herey').empty().html(result).hide().fadeIn(300);
                                        }});
                                        return false;
				});
				b.on('click', function() {
                                        
                                        $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID() %>&process=edit&referno="+oData.suppcode,
                                            success: function (result) {
                                            $('#herey').empty().html(result).hide().fadeIn(300);
                                        }});
				  
				  
				  return false;
				});
				c.on('click', function() {
                                        BootstrapDialog.confirm({
                                            title: 'Confirmation',
                                            message: 'Are you sure to delete?',
                                            type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                            closable: true, // <-- Default value is false
                                            draggable: true, // <-- Default value is false
                                            btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                            btnOKLabel: 'Delete', // <-- Default value is 'OK',
                                            btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                                            callback: function(result) {
                                                // result will be true if button was click, while it will be false if users close the dialog directly.
                                                if(result) {
                                                    $.ajax({
                                                        url: "PathController?moduleid=<%= mod.getModuleID() %>&process=delete&referno="+oData.suppcode,
                                                        success: function (result) {
                                                        $('#herey').empty().html(result).hide().fadeIn(300);
                                                        //setTimeout(function(){
                                                        //oTable.fnClearTable();}, 500);
                                                    }});
                                                }
                                            }
                                        });
                                  
				  
				  return false;
				});
				
                                              
                                $(nTd).empty();
				$(nTd).attr("class",'btn-group');
                                $(nTd).attr("align",'right');
                                $(nTd).prepend(a,b,c);
                                
                                
			  }
			 
		  } ]

		  
		  
		} );
                
                $('#example tbody').on('click', 'tr', function () {
                    var data = oTable.row( this ).data();
                    $.ajax({
                        url: "PathController?moduleid=<%= mod.getModuleID() %>&process=editlist&referno="+data.refer,
                        success: function (result) {
                            $('#herey').empty().html(result).hide().fadeIn(300);
                        }
                    });
                    return false;
                } );
                
                
                
                
                
                $('#example tbody').on( 'click', '#addaccount', function (e) {
                    e.preventDefault();
                    var refer = $(this).attr('href');
                    $.ajax({
                        url: "PathController?moduleid=<%= mod.getModuleID() %>&process=addnewitem&refer="+refer,
                        success: function (result) {
                            $('#herey').empty().html(result).hide().fadeIn(300);
                        }
                    });
                    
                    return false;
                });
                
                $('#example tbody').on( 'click', '#addrefer', function (e) {
                    e.preventDefault();
                    var refer = $(this).attr('href');
                    $.ajax({
                        url: "PathController?moduleid=<%= mod.getModuleID() %>&process=addrefer&refer="+refer,
                        success: function (result) {
                            $('#herey').empty().html(result).hide().fadeIn(300);
                        }
                    });
                    
                    return false;
                });


} );

</script>

<body>
    <div id ="maincontainer">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="74%" class="borderbot" align="left"><span class="bigfonttitle"><%= mod.getModuleDesc() %></span>&nbsp;&nbsp;<span class="midfonttitle"> List of <%= mod.getModuleDesc() %></span></td>
                <td width="26%" class="borderbot" align="right">&nbsp;</td>
            </tr>
        </table>
        <br>
        <div class="partition"> 
            <div class="headofpartition"></div>
            <div class="bodyofpartition">
                <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                    <tr>
                        <td valign="middle" align="left">
                            <button id="add-new" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> Add <%= mod.getModuleDesc() %></button>
                            
                        </td>
                    </tr>
                </table>
                        <hr>       
                <div class="dataTable_wrapper"> 
                    <table class="table table-striped table-bordered table-hover" id="example">
                    </table>
                </div>
                       
            </div>
        </div>
    </div>
</body>
</html>
  
