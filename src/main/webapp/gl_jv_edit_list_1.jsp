<%-- 
    Document   : gl_jv_edit_list
    Created on : Mar 3, 2016, 3:42:15 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucher"%>
<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucherItem"%>
<%@page import="com.lcsb.fms.dao.financial.gl.JournalVoucherDAO"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
JournalVoucherItem jvi = (JournalVoucherItem) JournalVoucherDAO.getJVItem(request.getParameter("referno"));
JournalVoucher jv = (JournalVoucher) JournalVoucherDAO.getJV(request.getParameter("referno"));
Module mod = (Module) JournalVoucherDAO.getModule();
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
           
            

              $( ".editjv" ).click(function() {
                  //var a = $("form").serialize();
                  var a = $(this).attr('id');
                  var b = $(this).attr('href');
                 
                 $.ajax({
                                            url: "PathController?moduleid=020104&process=editjvitem&referno="+a,
                                            success: function (result) {
                                            // $("#haha").html(result);
                                            $('#herey').empty().html(result).hide().fadeIn(300);
                                        }});
                                        return false;
               
                   
              });
              
              $( ".deletejvitem" ).click(function() {
                  //var a = $("form").serialize();
                  var a = $(this).attr('id');
                  var b = $(this).attr('href');
                 
                 BootstrapDialog.confirm({
                                            title: 'Confirmation',
                                            message: 'Are you sure to delete?',
                                            type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                            closable: true, // <-- Default value is false
                                            draggable: true, // <-- Default value is false
                                            btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                            btnOKLabel: 'Delete', // <-- Default value is 'OK',
                                            btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                                            callback: function(result) {
                                                // result will be true if button was click, while it will be false if users close the dialog directly.
                                                if(result) {
                                                    $.ajax({
                                                        url: "PathController?moduleid=020104&process=deletejvitem&referno="+a,
                                                        success: function (result) {
                                                        $('#herey').empty().html(result).hide().fadeIn(300);
                                                        }
                                                    });
                                                }
                                            }
                                        });
                 
                 
                
                                        return false;
               
                   
              });
              
              $( ".gotodelete" ).button({
                    icons: {
                      primary: "ui-icon-trash"
                    }
              })
              .click(function() {
                  //var a = $("form").serialize();
                  var a = $(this).attr('id');
                  var b = $(this).attr('href');
                  
                  $.ajax({
                        url: "PathController?moduleid=<%//=moduleid%>&process=deletesub&referenceno="+a+"&type="+b,
                        success: function (result) {
                        // $("#haha").html(result);
                        $('#maincontainer').empty().html(result).hide().fadeIn(300);
                    }});
                    return false;
               
                    
              });
              
              
                
              
                 
                 
                 
                  
                 
                 
              
         });
        </script>
            <div id ="maincontainer">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
              <td width="74%" class="borderbot" align="left"><span class="bigfonttitle">Journal Voucher</span>&nbsp;&nbsp;<span class="midfonttitle">Editing List</span></td>
		<td width="26%" class="borderbot" align="right">&nbsp;</td>
	  </tr>
  </table>
	<br>
   <div class="partition"> 
       <div class="headofpartition">Edit Detail</div>
       <div class="bodyofpartition">
 <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
			<td valign="middle" align="left">
				<button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                                
			</td>
		</tr>
  </table>
  <br>
  
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
      <td class="tdrow">&nbsp;<strong><%= jv.getJVrefno() %></strong></td>
    <td class="tdrow">&nbsp;<strong><%=  jv.getReason() %></strong></td>
    <td class="tdrow">&nbsp;<strong></strong></td>
    <td class="tdrow" align="right"><% if(GeneralTerm.currencyFormat(Double.parseDouble(jv.getTodebit())).equals(GeneralTerm.currencyFormat(Double.parseDouble(jv.getTocredit())))){%><i class="fa fa-check right-green"></i><%} %>&nbsp;<strong><%= GeneralTerm.currencyFormat(Double.parseDouble(jv.getTodebit())) %></strong></td>
    <td class="tdrow" align="right"><% if(GeneralTerm.currencyFormat(Double.parseDouble(jv.getTodebit())).equals(GeneralTerm.currencyFormat(Double.parseDouble(jv.getTocredit())))){%><i class="fa fa-check right-green"></i><%} %>&nbsp;<strong><%= GeneralTerm.currencyFormat(Double.parseDouble(jv.getTocredit())) %></strong></td>
    <td class="tdrow" align="right">&nbsp;
        <div class="btn-group btn-group-xs" role="group" aria-label="...">
        <button id="backto" class="btn btn-default btn-xs" title="<%= mod.getModuleID() %>" id="<%//= pvi.getRefer() %>" name="viewlist"><i class="fa fa-pencil"></i></button>
        <button id="savebutton"  class="btn btn-default btn-xs" title="<%= mod.getModuleID() %>" id="<%//= pvi.getRefer() %>" name="addjv"><i class="fa fa-trash-o"></i></button>
        </div></td>
  </tr>
  <%
  List<JournalVoucherItem> listAll = (List<JournalVoucherItem>) JournalVoucherDAO.getAllJVItem(jv.getJVrefno());
    for (JournalVoucherItem j : listAll) {
  %>
  <tr>
    <td class="tdrow">&nbsp;&nbsp;&nbsp;<%= j.getJvid() %></td>
    <td class="tdrow">&nbsp;<%= j.getActcode() %> - <%= j.getActdesc() %></td>
    <td class="tdrow">&nbsp;<%= j.getSacode()%> - <%= j.getSadesc()%></td>
    <td class="tdrow" align="right">&nbsp;<%= GeneralTerm.currencyFormat(j.getDebit()) %></td>
    <td class="tdrow" align="right">&nbsp;<%= GeneralTerm.currencyFormat(j.getCredit()) %></td>
    <td class="tdrow" align="right">&nbsp;
        <!--<a href="sub" id="<%//= pvi.getRefer() %>" class="goto">Edit</a>&nbsp;
        <a href="sub" id="<%//= pvi.getNovoucher() %>" class="gotodelete">Delete</a>-->
        
    <div class="btn-group btn-group-xs" role="group" aria-label="...">
                               
                                <button id="<%= j.getJvid() %>" class="btn btn-default btn-xs editjv" title="<%= mod.getModuleID() %>" id="<%//= pvi.getRefer() %>" name="viewlist"><i class="fa fa-pencil"></i></button>
                                <button id="<%= j.getJvid() %>"  class="btn btn-default btn-xs deletejvitem" title="<%= mod.getModuleID() %>" id="<%//= pvi.getRefer() %>" name="addjv"><i class="fa fa-trash-o"></i></button>
                              </div>
    </td>
  </tr>
  <%
    }     
  %>
</table>

     </div>
     </div>
      </div>
      

      