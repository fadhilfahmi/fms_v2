<%-- 
    Document   : ap_pv_edit_main
    Created on : May 11, 2016, 9:13:00 AM
    Author     : user
--%>

<%@page import="com.lcsb.fms.dao.financial.ap.VendorPaymentDAO"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucher"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
PaymentVoucher v = (PaymentVoucher) VendorPaymentDAO.getPYV(log,request.getParameter("refer"));
Module mod = (Module) VendorPaymentDAO.getModule();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        
<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
           //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });
           //$('#JVdate').datepicker({ dateFormat: 'yyyy-mm-dd' });
           
           $('#tarikh').datepicker({
               format:'yyyy-mm-dd',
               defaultDate:'now',
               autoclose:true
           });
           
           
           $('#JVdate').datepicker({
               format:'yyyy-mm-dd',
               defaultDate:'now',
               autoclose:true
           });
           
           $("#total").keyup(function(){
                var val = $(this).val();
                var to = 'amount';
                //alert(val);
                //alert(checkRegexp(val,to));
                if(checkRegexp(val,to)){
                    $('#res_digit').empty();
                   $('#div_digit').removeClass('has-error');
                   $('#div_digit').addClass('has-success');
                   $('#res_digit').append("<i class=\"fa fa-check-circle right-green\"></i>&nbsp;&nbsp;");
                    $('#savebutton').prop('disabled', false);
                    convertRM(val,to);
                }else{
                    $('#res_digit').empty();
                   $('#div_digit').removeClass('has-success');
                   $('#div_digit').addClass('has-error');
                   $('#res_digit').append("<i class=\"fa fa-exclamation-circle fail-red\"></i>&nbsp;&nbsp;Numeric Formatted Only");
                    $('#savebutton').prop('disabled', true);
                }
                //
            });
            $('#getbank').click(function(e){
               
               var a = 'bankcode';
               var b = 'bankname';
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Taxation',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_bank.jsp?code='+a+'&name='+b);
                        $('body').on('click', '.thisresult', function(event){
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
            
             $('#gettype').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Receiver',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_receiver.jsp');
                        $('body').on('click', '.thisresult_nd', function(event){
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
        });
        
       </script>
    </head>
    <body>
        <div id ="maincontainer">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
              <td width="74%" class="borderbot" align="left"><span class="bigfonttitle"><%= mod.getModuleDesc() %></span>&nbsp;&nbsp;<span class="midfonttitle">Add New <%= mod.getModuleDesc() %></span></td>
		<td width="26%" class="borderbot" align="right">&nbsp;</td>
	  </tr>
  </table>
	<br>
   <div class="partition"> 
       <div class="headofpartition"></div>
       <div class="bodyofpartition">
           
           <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
                    <td valign="middle" align="left">
			<button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="editmainprocess" ><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
		</td>
		</tr>
  </table>
  <br>
  <form data-toggle="validator" role="form" id="saveform">
    <input type="hidden" name="prepareid" id="pid" value="<%= log.getUserID()%>">
    <input type="hidden" name="preparename" id="pname" value="<%= log.getFullname()%>">
    <input type="hidden" name="preparedate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp() %>">
    <input type="hidden" name="coacode" id="coacode" value="">
    <input type="hidden" name="coadesc" id="coadesc" value="">
    <div class="well">
    <div class="row">
        
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Reference No &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="refer" name="refer" placeholder="Auto Generated" autocomplete="off" value="<%= v.getRefer() %>">   
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Voucher No &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="voucherno" name="voucherno" placeholder="Auto Generated" autocomplete="off" value="<%= v.getVoucherno() %>">   
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Date &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="tarikh" name="tarikh" placeholder="Date of Payment Voucher" autocomplete="off" value="<%= v.getTarikh() %>">   
             </div>
        </div>
    </div>
    
    </div><div class="well"><div class="row">
        <div class="col-sm-3">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Paid Type &nbsp&nbsp<span class="res_code"></span></label>
                <select class="form-control input-sm" id="paidtype" name="paidtype" >
                    <%= ParameterDAO.parameterList(log,"Paid Type",v.getPaidtype()) %>
                </select>
            </div></div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Paid Code &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="paidcode" name="paidcode" placeholder="" autocomplete="off" value="<%= v.getPaidcode() %>">   
             </div>
        </div>
        <div class="col-sm-5">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Paid Name&nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="paidname" name="paidname" placeholder="" autocomplete="off" value="<%= v.getPaidname() %>"> 
             </div>
        </div></div>
        <div class="row">
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Address &nbsp&nbsp<span class="res_code"></span></label>
                <textarea class="form-control" rows="4"  id="paidaddress" name="paidaddress"><%= v.getPaidaddress() %></textarea>
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">Postcode &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <input type="text" class="form-control input-sm" id="paidpostcode" name="paidpostcode" placeholder="" autocomplete="off" value="<%= v.getPaidpostcode() %>"> 
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">City &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <input type="text" class="form-control input-sm" id="paidcity" name="paidcity" placeholder="" autocomplete="off" value="<%= v.getPaidcity() %>"> 
             </div>
        </div><div class="col-sm-4">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">State &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <input type="text" class="form-control input-sm" id="paidstate" name="paidstate" placeholder="" autocomplete="off" value="<%= v.getPaidstate() %>"> 
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">GST ID&nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="gstid" name="gstid" placeholder="" autocomplete="off" value="<%= v.getGstid() %>"> 
             </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Remarks &nbsp&nbsp<span class="res_code"></span></label>
                <textarea class="form-control" rows="3"  id="remarks" name="remarks"><%= v.getRemarks() %></textarea>
             </div>
        </div>
    </div>
                
    </div>
    
    <div class="well"><div class="row">
    <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Rounding Adjustment Account Code&nbsp&nbsp<span class="res_code"></span></label>
                <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="racoacode" name="racoacode" value="<%= v.getRacoacode() %>">
                                        <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="racoacode" id1="racoadesc"><i class="fa fa-cog"></i> Account</button>
                                        </span>
                                    </div>  
             </div>
        </div>
        <div class="col-sm-8">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Rounding Adjustment Account Description&nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="racoadesc" name="racoadesc" placeholder="" autocomplete="off" value="<%= v.getRacoadesc() %>"> 
             </div>
        </div>
       
    </div>
        <div class="row">
        <div class="col-sm-3">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">RM (Digit) &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <input type="text" class="form-control input-sm" id="total" name="amount" placeholder="0.00" autocomplete="off" value="<%= v.getAmount() %>"> 
             </div>
        </div>
        <div class="col-sm-5">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">RM (In Words) &nbsp&nbsp<span class="res_code"></span></label>
                <textarea class="form-control" rows="3"  id="amount" name="rm"><%= v.getRm() %></textarea>
             </div>
        </div>
        
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Paid Type &nbsp&nbsp<span class="res_code"></span></label>
                <select class="form-control input-sm" id="paymentmode" name="paymentmode" value="<%= v.getPaymentmode() %>">
                    <%= ParameterDAO.parameterList(log,"Payment Method","") %>
                </select>
            </div></div></div><div class="row">
                <div class="col-sm-3">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Bank Code &nbsp&nbsp<span class="res_code"></span></label>
                <div class="form-group-sm input-group">
                <input type="text" class="form-control input-sm" id="bankcode" name="bankcode" placeholder="" autocomplete="off" value="<%= v.getBankcode() %>">
                <span class="input-group-btn">
                <button class="btn btn-default btn-sm" id="getbank" title=""><i class="fa fa-cog"></i>Get Bank</button></span>
                </div></div>
                     </div>
            <div class="col-sm-5">
             <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Bank Name&nbsp&nbsp<span class="res_code"></span></label>
                
                                        <input type="text" class="form-control input-sm" id="bankname" name="bankname" placeholder="" autocomplete="off" value="<%= v.getBankname() %>">
                                        
                                    </div>  
             </div>
            <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Cheque Number&nbsp&nbsp<span class="res_code"></span></label>
                 <div class="form-group-sm input-group">
                <input type="text" class="form-control input-sm" id="cekno" name="cekno" placeholder="" autocomplete="off" value="<%= v.getCekno() %>"> 
                 <span class="input-group-btn">
                     <button class="btn btn-default btn-sm" id="getbank" title=""><i class="fa fa-cog"></i>Get Cheque No.</button></span></div>
             </div>
        </div>
             </div>
        </div>
                <div class="well">
    <div class="row">
        <div class="col-sm-2">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Year &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="year" name="year" placeholder="Code" autocomplete="off" value="<%= v.getYear() %>">   
             </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Period &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="period" name="period"  autocomplete="off" value="<%= v.getPeriod() %>">   
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Estate Code&nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="estatecode" name="estatecode" placeholder="0.00" autocomplete="off" value="<%= v.getEstatecode() %>" readonly > 
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Estate Name&nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="estatename" name="estatename" placeholder="0.00" autocomplete="off" value="<%= v.getEstatename() %>" readonly> 
             </div>
        </div>
        
    </div></div>
    
    
          
   
    
    
    
    <!--<div class="form-group-sm input-group">
    <input type="text" class="form-control input-sm">
    <span class="input-group-btn">
    <button class="btn btn-default btn-sm" type="button"><i class="fa fa-cog"></i> Get Account Code</button>
    </span>
    </div>-->
    
</form>

  


     </div>
     </div>
      </div>
      

    </body>
</html>