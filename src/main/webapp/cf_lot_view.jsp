<%-- 
    Document   : cf_lot_view
    Created on : Apr 18, 2016, 4:53:29 PM
    Author     : HP
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.LotInformationDAO"%>
<%@page import="com.lcsb.fms.model.setup.configuration.LotInformation"%>
<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
LotInformation view = (LotInformation) LotInformationDAO.getInfo(log,request.getParameter("id"));
%>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
           
            
              $( ".printbutton" ).click(function() {
                  //var a = $("form").serialize();
                  var a = $("#saveform :input[value!='']").serialize(); 
                  //alert($('input[name="ad_amount"]').val());
                  checkField();
                  var b = checkField();
                  if(b==false){
                    $.ajax({
                        data: a,
                        type: 'POST',
                        url: "DataController?do=add&formlevel=master",
                        success: function (result) {
                          $('#maincontainer').empty().html(result).hide().fadeIn(300);
                        }
                    });
                     
                  }    
                  
               
                    return false;
              });
              
         });
        </script>
            <div id ="maincontainer">
                <table id="table_2" cellspacing="0">
                    
               <tr>
                   <td width="30%" class="bd_bottom" align="left" valign="top">Location Code</td>
                   <td width="70%" class="bd_bottom" align="left"><%= view.getLotCode() %></td>
               </tr>
               <tr>
                   <td class="bd_bottom" align="left" valign="top">Active</td>
                   <td class="bd_bottom" align="left"> 
                       <%= view.getActive()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Location Code</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getLoccode()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Lot Acre</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getLotAcre()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Lot Description</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getLotDescp()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Lot Hectare</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getLotHect()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Location Description</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getLocDescp()%>
                   </td>
               </tr>
          </table>
            </div>
      

      
      
      
      

