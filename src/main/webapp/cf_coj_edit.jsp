<%-- 
    Document   : cf_coj_edit
    Created on : Jun 1, 2016, 12:30:10 PM
    Author     : user
--%>

<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.model.setup.configuration.ChartofJob"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ChartofJobDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
Module mod = (Module) ChartofJobDAO.getModule();
ChartofJob edit = (ChartofJob) ChartofJobDAO.getInfo(log,request.getParameter("referenceno"));

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <script src="jsfunction/jquery-1.12.0.min.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
            
           
           $('#getbank').click(function(e){
               
               var a = 'bankcode';
               var b = 'bankname';
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Taxation',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_bank.jsp?code='+a+'&name='+b);
                        $('body').on('click', '.thisresult', function(event){
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
        });
        
       </script>
    </head>
    <body>
        <div id ="maincontainer">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
              <td width="74%" class="borderbot" align="left"><span class="bigfonttitle"><%= mod.getModuleDesc() %></span>&nbsp;&nbsp;<span class="midfonttitle">Edit <%= mod.getModuleDesc() %></span></td>
		<td width="26%" class="borderbot" align="right">&nbsp;</td>
	  </tr>
  </table>
	<br>
   <div class="partition"> 
       <div class="headofpartition"></div>
       <div class="bodyofpartition">
  <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
			<td valign="middle" align="left">
				<button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                                <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="editprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
			</td>
		</tr>
  </table>
  <br>
  
  
  
<form data-toggle="validator" role="form" id="saveform">
    <input type="hidden" name="prepareid" id="pid" value="<%= log.getUserID()%>">
    <input type="hidden" name="preparename" id="pname" value="<%= log.getFullname()%>">
    <input type="hidden" name="preparedate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp() %>">
    <div class="well">
    <div class="row">
      
        <div class="col-sm-3">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Job Code&nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="code" name="code"  autocomplete="off" required value="<%=edit.getCode()%>" readonly>  
             </div>
        </div>
             <div class="col-sm-9">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Job Description &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="descp" name="descp" autocomplete="off" value="<%=edit.getDescp()%>">   
             </div>
        </div>
             
       
    </div></div><div class="well">
    <div class="row">
         
        <div class="col-sm-3">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Final Level&nbsp&nbsp<span class="res_code"></span></label>
                <select class="form-control input-sm" id="finallvl" name="finallvl">
                    <%= ParameterDAO.parameterList(log,"YesNo Type",edit.getFinallvl()) %>
                </select>    
             </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Active&nbsp&nbsp<span class="res_code"></span></label>
                <select class="form-control input-sm" id="active" name="active">
                    <%= ParameterDAO.parameterList(log,"YesNo Type",edit.getActive()) %>
                </select>    
             </div>
        </div>
        
        <div class="col-sm-3">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Job Type&nbsp&nbsp<span class="res_code"></span></label>
                <select class="form-control input-sm" id="type1" name="type1">
                    <%= ParameterDAO.parameterList(log,"Job Type",edit.getType1()) %>
                </select>    
             </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">Workshop Activity   &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <select class="form-control input-sm" id="type2" name="type2">
                    <%= ParameterDAO.parameterList(log,"YesNo Type",edit.getType2()) %>
                </select> 
             </div>
        </div>
   
    </div>
    </div>

        
    
    
    
        
    
    
    
</form>
</div>
   
     </div>
     </div>
      </div>
      

    </body>
</html>