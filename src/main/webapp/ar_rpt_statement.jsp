<%-- 
    Document   : ar_rpt_statement
    Created on : Feb 17, 2017, 12:41:01 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.financial.ar.ArReportDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) ArReportDAO.getModule();
%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" type="text/css" media="all" href="bootstrap-daterangepicker-master/daterangepicker.css" />
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="bootstrap-daterangepicker-master/moment.js"></script>
<script type="text/javascript" src="bootstrap-daterangepicker-master/daterangepicker.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });
        //$('#JVdate').datepicker({ dateFormat: 'yyyy-mm-dd' });

        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        }).change(dateChanged)
                .on('changeDate', dateChanged);

        $('.range').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            }
        });

        $('#getbuyer').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Buyer Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_buyer.jsp?name=bname&code=bcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });
    });
</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
                </tr>
            </table>
            <br>

            <form data-toggle="validator" role="form" id="saveform">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="well">

                            <div class="row self-row">
                                <label for="inputName" class="control-label">Statement Parameter</label>
                            </div>
                            <!--<div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Period Range</label>
                                        <div class="form-group-sm input-group">
                                            <input type="text" id="range" name="range" class="form-control range" value="2016-09-01 - <%//= AccountingPeriod.getCurrentTimeStamp() %>">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-sm" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                            </div>-->
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Start Date</label>
                                        <input type="text" class="form-control input-sm datepicker" id="startdate" name="startdate" placeholder="Date of Journal Voucher" autocomplete="off" value="2016-09-01">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">End Date</label>
                                        <input type="text" class="form-control input-sm datepicker" id="enddate" name="enddate" placeholder="Date of Journal Voucher" autocomplete="off" value="<%= AccountingPeriod.getCurrentTimeStamp() %>">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Buyer Code</label>
                                        <div class="form-group-sm input-group">
                                            <input type="text" class="form-control input-sm" id="bcode" name="buyercode" value="0311" required>
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-sm" type="button" id="getbuyer"><i class="fa fa-cog"></i> Buyer</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Buyer Description</label>
                                        <input type="text" class="form-control input-sm" id="bname" name="buyername" value="HUP LEE OIL MILL SDN BHD" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">                       
                        <div class="row pull-right">
                            <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewstatement">Generate Report</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                </div>
            </form>                               

        </div>
    </div>
</div>
