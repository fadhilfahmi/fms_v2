<%-- 
    Document   : datatable
    Created on : Feb 1, 2017, 10:20:47 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.DataTableDAO"%>
<%@page import="com.lcsb.fms.general.DataTable"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoice"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.util.model.ListTable"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%

    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    DataTable dtparam = (DataTable) session.getAttribute("dtparam");
    DataTable dt = (DataTable) DataTableDAO.getData(log, dtparam);

    Module mod = (Module) ModuleDAO.getModule(log, request.getParameter("moduleid"));

%>
<!-- START PLUGINS -->
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="js/plugins/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap.min.js"></script>        
<!-- END PLUGINS -->                

<!-- THIS PAGE PLUGINS -->
<script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
<script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>

<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>    
<!-- END PAGE PLUGINS -->

<!-- START TEMPLATE -->

<script type="text/javascript" src="js/plugins.js"></script>        
<script type="text/javascript" src="js/actions.js"></script>     
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<!-- END TEMPLATE -->
<script type="text/javascript" charset="utf-8">

    $(document).ready(function () {

    <%        List<ListTable> mlist = (List<ListTable>) ModuleDAO.getTableList(log, request.getParameter("moduleid"));
        for (ListTable m : mlist) {

            //if(m.getList_View().equals("1")){
    %>
    columntoView.push('<%= m.getColumnName()%>');
    columnName.push('<%= m.getTitleName()%>');
    <%
            //}

        }
    %>

    var colsTosend = 'cols=';
    for (m = 0; m < columntoView.length; ++m) {//put value from database to variable jget
    if (m === columntoView.length - 1) {
    colsTosend += columntoView[m];
    } else {
    colsTosend += columntoView[m] + '&cols=';
    }
    }

    var oTable = $('#list').DataTable({
    responsive: true,
            "aaSorting": [[0, "desc"]],
            "bServerSide": true,
            "stateSave": true,
            "sAjaxSource": "retrievetable.jsp?table=<%= mod.getMainTable()%>&code=<%= mod.getReferID_Master()%>&" + colsTosend + "&swhere=",
            
            "aoColumns": [
    <%
        int i = 0;
        for (ListTable m : mlist) {
            //if(m.getList_View().equals("1")){
            if (m.isBoolView()) {
    %>
            {"sTitle": "<%= m.getTitleName()%>", "mData": "<%= m.getColumnName()%>", "bSearchable": true},
    <%
    } else {
    %>
            {"sTitle": "<%= m.getTitleName()%>", "mData": "<%= m.getColumnName()%>", "bVisible": false},
    <%
            }

            i++;

        }
    %>
            {"sTitle": "Status", "mData": null, "sWidth": "10%"}//,
            //{"sTitle": "Action", "mData": null, "bSortable": false, "bSearchable": false, "sWidth": "13%"}
            ],"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var sDirectionClass;
            //console.log(aData.<%= mod.getPost()%>);
            if (aData.<%= mod.getPost()%>.toLowerCase() == "cancel".toLowerCase())
                    sDirectionClass = "cancelrow";
            
            $(nRow).addClass(sDirectionClass);
            return nRow;
            },
            "aoColumnDefs": [{
            "aTargets": [<%=i%>], //special rules for determine a status
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {


                    var stat = '<span class="label label-primary">Preparing</span>';
                    if (oData.<%= mod.getCheck()%>.length > 0) {
                    stat = '<span class="label label-warning">Checked</span>';
                    }
                    if (oData.<%= mod.getApprove()%>.length > 0) {
                    stat = '<span class="label label-success">Approved</span>';
                    }
                    if (oData.<%= mod.getPost()%>.toLowerCase() == 'Cancel'.toLowerCase()) {
                    stat = '<span class="label label-default">Canceled</span>';
                    }

                    var dc = '';
                    $.ajax({
                    async: false,
                            url: "ProcessController?moduleid=000000&process=checkDCnote&referno=" + oData.<%= mod.getReferID_Master()%>,
                            success: function (result) {
                            // $("#haha").html(result);
                            dc = result;
                            }
                    });
                    $(nTd).empty();
                    //$(nTd).attr("id",'btntest');
                    $(nTd).prepend(stat, dc);
                    }

            },
    <%
                if (mod.getModuleID().equals("020104")) {
    %>
            {
            "aTargets": [<%=i - 5%>], //special rules for determine a status
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {

                    var s = parseFloat(sData);
                    var amt = formatCurrency(s);
                    $(nTd).empty();
                    $(nTd).attr("class", 'align-right');
                    $(nTd).prepend(amt);
                    }

            },
    <%
                }
    %>
            {
            "aTargets": [<%=i - 4%>], //special rules for determine a status
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {

                    var s = parseFloat(sData);
                    var amt = formatCurrency(s);
                    $(nTd).empty();
                    $(nTd).attr("class", 'align-right');
                    $(nTd).prepend(amt);
                    }

            }]
    });
    $('#list').on('click', 'tr', function () {
    var data = oTable.row(this).data();
    $.ajax({
    url: "PathController?moduleid=<%= dt.getModule().getModuleID()%>&process=editlist&referno=" + data.<%= mod.getReferID_Master()%>,
            success: function (result) {
            $('#herey').empty().html(result).hide().fadeIn(300);
            }
    });
    return false;
    });
    }
    );
</script>
<table class="table table-bordered table-striped table-hover" id="list">


</table>



