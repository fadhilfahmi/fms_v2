<%-- 
    Document   : gl_jv_add_item
    Created on : Mar 1, 2016, 3:36:17 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucherItem"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.financial.gl.JournalVoucherDAO"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
JournalVoucherItem jv = (JournalVoucherItem) JournalVoucherDAO.getJVItem(request.getParameter("referno"));
Module mod = (Module) JournalVoucherDAO.getModule();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="jsfunction/jquery-1.12.0.min.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
        <link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
           //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });
           
            $('.getaccount').click(function(e){
              var a = $(this).attr('id');
              var b = $(this).attr('id1');
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Chart of Account',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_coa.jsp?code='+a+'&descp='+b);
                        $('body').on('click', '.thisresult', function(event){
                            dialog.close();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
           $('#getlocation').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Location',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_location.jsp');
                        $('body').on('click', '.thisresult_nd', function(event){
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
           $('#getsub').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Sub Account Detail',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_sub.jsp');
                        $('body').on('click', '.thisresult_nd', function(event){
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
           $('#gettax').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Taxation',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_tax.jsp');
                        $('body').on('click', '.thisresult', function(event){
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
           
           $('.calculate').keyup(function(e){
               var todeduct = $(this).val();
               var theval = $(this).attr('id');
               
               
               if(todeduct!=''){
                   var tot = 0;
                   if(theval=='debit'){
                       $('#credit').val(0.0);
                   }
                   if(theval=='credit'){
                       $('#debit').val(0.0);
                   }
                   
               }
               
           });
        });
        
       </script>
    </head>
    <body>
        <div id ="maincontainer">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
              <td width="74%" class="borderbot" align="left"><span class="bigfonttitle">Journal Voucher</span>&nbsp;&nbsp;<span class="midfonttitle">Add New Journal Voucher</span></td>
		<td width="26%" class="borderbot" align="right">&nbsp;</td>
	  </tr>
  </table>
	<br>
   <div class="partition"> 
       <div class="headofpartition"></div>
       <div class="bodyofpartition">
  <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
			<td valign="middle" align="left">
				<button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                                <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="addjvitem"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
			</td>
		</tr>
  </table>
  <br>
  
  <div id="dialog-form-update" title="Add New Information"><p class="validateTips">All form fields are required.</p>
      <form id="saveform"><table id="table_2" cellspacing="0">
       <table border="0" cellspacing="0" cellpadding="6" width="100%">
               <tr>
                   <td width="24%" class="bd_bottom" align="left" valign="top">Reference ID</td>
                   <td width="74%" class="bd_bottom" align="left"> <input style="font-size:11px" name="JVid" id="JVid" type="text" value="AUTO"  size="25"/></td>
               </tr>
               <tr>
                   <td width="24%" class="bd_bottom" align="left" valign="top">JV Reference No</td>
                   <td width="74%" class="bd_bottom" align="left"> <input style="font-size:12px" name="JVrefno" id="JVrefno" type="text" value="<%= jv.getJVrefno() %>" size="25" /></td>
               </tr>
               <tr>
                   <td width="24%" class="bd_bottom" align="left" valign="top">Account Code</td>
                   <td width="74%" class="bd_bottom" align="left">
                       <input style="font-size:12px" name="actcode" id="actcode" type="text"  size="25" value="" />
                       <button class="btn btn-default btn-sm getaccount" type="button" id="actcode" id1="actdesc"><i class="fa fa-cog"></i> Account</button>
                   </td>
               </tr>
               <tr>
                   <td width="24%" class="bd_bottom" align="left" valign="top">Account Description</td>
                   <td width="74%" class="bd_bottom" align="left">
                       <input style="font-size:12px" name="actdesc" id="actdesc" type="text"  size="50" value=""  />
                   </td>
               </tr>
               <tr>
                   <td width="24%" class="bd_bottom" align="left" valign="top">Location Level</td>
                   <td width="74%" class="bd_bottom" align="left">
                      <input style="font-size:12px" name="loclevel" id="loclevel" type="text"  size="25"  value="" />
                      <button class="btn btn-default btn-sm" id="getlocation" title="">Get Location</button>
                   </td>
               </tr>
               <tr>
                   <td width="24%" class="bd_bottom" align="left" valign="top">Location Code</td>
                   <td width="74%" class="bd_bottom" align="left"> 
                       <input style="font-size:12px" name="loccode" id="loccode" type="text"  size="25"  value="" />
                   </td>
                   <td width="2%"></td>
               </tr>
               
               <div class="row">
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Location Level&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="loclevel" name="loclevel" readonly>
                                        <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="getlocation"><i class="fa fa-cog"></i> Location</button>
                                        </span>
                                    </div>    
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Location Code&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="loccode" name="loccode"  autocomplete="off" readonly>   
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Location Name &nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="locdesc" name="locname" placeholder="" autocomplete="off" readonly >   
             </div>
        </div>
    </div>
               <tr>
                   <td width="24%" class="bd_bottom" align="left" valign="top">Location Description</td>
                   <td width="74%" class="bd_bottom" align="left">
                       <input style="font-size:12px" name="locdesc" id="locdesc" type="text"  size="50"  value="" />
                   </td>
               </tr>
               <tr>
                   <td width="24%" class="bd_bottom" align="left" valign="top">Sub-Account Type</td>
                   <td width="74%" class="bd_bottom" align="left">
                      <input style="font-size:12px" name="satype" id="satype" type="text"  size="25" value="None"  />
                      <button class="btn btn-default btn-sm" id="getsub" title="">Get Sub Account</button>
                   </td>
               </tr>
               <tr>
                   <td width="24%" class="bd_bottom" align="left" valign="top">Sub-Account Code</td>
                   <td width="74%" class="bd_bottom" align="left">
                       <input style="font-size:12px" name="sacode" id="sacode" type="text"  size="25" value="00"  />
                   </td>
               </tr>
               <tr>
                   <td width="24%" class="bd_bottom" align="left" valign="top">Sub-Account Desc</td>
                   <td width="74%" class="bd_bottom" align="left">
                       <input style="font-size:12px" name="sadesc" id="sadesc" type="text"  size="50" value ="Not Applicable"  />
                   </td>
               </tr>
               
               <div class="row">
        <div class="col-sm-7">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Sub Account Type&nbsp&nbsp<span class="res_code"></span></label>
                <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="satype" name="satype" value="None" readonly>
                                        <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="getsub"><i class="fa fa-cog"></i> Sub Account</button>
                                        </span>
                                    </div>  
             </div>
        </div>
        <div class="col-sm-5">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Sub Account Code &nbsp&nbsp<span class="res_code"></span></label>
                <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="sacode" name="sacode" value="00">
                                        <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="sacode" id1="sadesc"><i class="fa fa-cog"></i> Account</button>
                                        </span>
                                    </div>  
             </div>
        </div>
        </div>
        <div class="row">
        <div class="col-sm-7">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">Sub Account Description &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <input type="text" class="form-control input-sm" id="sadesc" name="sadesc" placeholder="" autocomplete="off" value="Not Applicable"> 
             </div>
        </div>
               <tr>
                   <td width="24%" class="bd_bottom" align="left" valign="top">GST ID</td>
                   <td width="74%" class="bd_bottom" align="left">
                       <input style="font-size:12px" name="gstid" id="gstid" type="text"  size="25" value=""  />
                   </td>
               </tr>
               <tr>
                   <td width="24%" class="bd_bottom" align="left" valign="top">Tax Code</td>
                   <td width="74%" class="bd_bottom" align="left">
                       <input style="font-size:12px" name="taxcode" id="taxcode" type="text"  size="25" value=""  />
                       <button class="btn btn-default btn-sm" id="gettax" title="">Get Tax</button>
                   </td>
               </tr>
               <tr>
                   <td width="24%" class="bd_bottom" align="left" valign="top">Tax Description</td>
                   <td width="74%" class="bd_bottom" align="left">
                       <textarea name="taxdescp" id="taxdescp"   rows="4" cols="50"></textarea>
                   </td>
               </tr>
               <tr>
                   <td width="24%" class="bd_bottom" align="left" valign="top">Tax Rate</td>
                   <td width="74%" class="bd_bottom" align="left">
                       <input style="font-size:12px" name="taxrate" id="taxrate" type="text"  size="25" value="0.0"  />
                   </td>
               </tr>
               <tr>
                   <td width="24%" class="bd_bottom" align="left" valign="top">Remarks</td>
                   <td width="74%" class="bd_bottom" align="left">
                       <textarea name="remark" id="remark"   rows="4" cols="50"></textarea>
                   </td>
               </tr>
               <tr>
                 <td colspan="2" align="left" valign="top" class="bd_bottom"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr>
                     <td><strong>Debit</strong></td>
                     <td><strong>Credit</strong></td>
                   </tr>
                   <tr>
                       <td><input style="font-size:24px" name="debit" id="debit"  class="calculate" type="text" value="<%= GeneralTerm.currencyFormat(jv.getDebit()) %>" size="25"  autocomplete="off"  /></td>
                     <td><input style="font-size:24px" name="credit" id="credit" class="calculate" type="text" value="<%= GeneralTerm.currencyFormat(jv.getCredit()) %>" size="25"  autocomplete="off" /></td>
                   </tr>
                 </table></td>
               </tr>
               
               <!--<tr>
                   <td width="24%" class="bd_bottom" align="left" valign="top">Balance</td>
                   <td width="74%" class="bd_bottom" align="left">
                      
                   </td>-->
               </tr>
                <input style="font-size:12px" name="balance" id="balance" type="hidden" value="<%= jv.getBalance() %>" size="25"  />
                       <input style="font-size:11px" name="balance_perm" id="balance_perm" type="hidden" value="<%= jv.getBalance() %>" size="25"  />
                       <input style="font-size:11px" name="sum_debit" id="sum_debit" type="hidden" value="<%= jv.getDebit()%>" size="25"  />
                       <input style="font-size:11px" name="sum_credit" id="sum_credit" type="hidden" value="<%= jv.getCredit()%>" size="25"  />
                       <input style="font-size:12px" name="amtbeforetax" id="amtbeforetax" type="hidden" value="0.00" size="25"  />
              <!-- <tr>
                   <td width="24%" class="bd_bottom" align="left" valign="top">Amount Before Tax</td>
                   <td width="74%" class="bd_bottom" align="left">
                       
                   </td>
               </tr>-->
               </table>
      </form>
  </div>
   
     </div>
     </div>
      </div>
      

    </body>
</html>

