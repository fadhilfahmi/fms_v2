<%-- 
    Document   : ap_pv_select_invoice
    Created on : Oct 19, 2016, 2:43:33 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.dao.financial.ap.VendorInvoiceDAO"%>
<%@page import="com.lcsb.fms.model.financial.ap.VendorInvoice"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
%>
<!DOCTYPE html>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('.activerowy').click(function(e){
            e.preventDefault();
            var b = $(this).attr('id');
            //$(this).toggleClass('info');
           // $('#chkbox'+b).prop('checked', true);
            var checkBox = $('#chkbox'+b);
            checkBox.prop("checked", !checkBox.prop("checked"));
           
        });
    });
</script>

<table class="table table-bordered table-striped table-hover" id="list1">
                                <thead>

                                    <tr>
                                        <th></th>
                                        <th>Invoice No</th>
                                        <th>Date</th>
                                        <th>Payee</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%List<VendorInvoice> listx = (List<VendorInvoice>) VendorInvoiceDAO.getNoPaidPNV(log, request.getParameter("suppcode"));
                                    int i = 0;
                                    for (VendorInvoice j : listx) {

                                        i++;
                                    %>
                                    <tr id="<%= j.getInvrefno()%>" class="activerowy">
                                        <td width="3%"><label>
                                                    <input type="checkbox" id="chkbox<%= j.getInvrefno()%>" class="chkbox" value="<%= j.getInvrefno()%>" required>
                                            </label>
                                        </td>
                                        <td class="tdrow"><%= j.getInvrefno()%></td>
                                        <td class="tdrow"><input type="hidden" id="dispatchno<%=i%>" name="dispatchno<%=i%>" value="<%= j.getDate()%>"><%= j.getDate()%></td>
                                        <td class="tdrow"><input type="hidden" id="trailer<%=i%>" name="trailer<%=i%>" value="<%= j.getSuppcode()%>"><%= j.getSuppcode()%> - <%= j.getSuppname()%></td>
                                        <td class="tdrow"><span class="pull-right"><%= GeneralTerm.currencyFormat(j.getTotalamount()) %></span></td>
                                       
                                    </tr>
                                    <%}%>
                                    
                                </tbody>

                            </table>
