<%-- 
    Document   : cp_company_list.jsp
    Created on : Apr 27, 2016, 1:01:17 PM
    Author     : user
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.ListTable"%>
<%@page import="com.lcsb.fms.dao.setup.company.CompanyInfoDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.ModuleItem"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,java.util.*" errorPage="" isThreadSafe="false" %>
<%@ page import="java.util.Date"%>
<%
    response.setHeader("Cache-Control", "no-cache");
    
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) CompanyInfoDAO.getModule();
%>

<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>

<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        var updateTable = '<%= mod.getMainTable()%>';

    <%
    List<ListTable> mlist = (List<ListTable>) CompanyInfoDAO.tableList();
    for (ListTable m : mlist) {
        //if(m.getList_View().equals("1")){
%>
        columntoView.push('<%= m.getColumnName()%>');
        columnName.push('<%= m.getTitleName()%>');
    <%
                //}

            }
    %>

        var colsTosend = 'cols=';
        for (m = 0; m < columntoView.length; ++m) {//put value from database to variable jget
            if (m == columntoView.length - 1) {
                colsTosend += columntoView[m];
            } else {
                colsTosend += columntoView[m] + '&cols=';
            }
        }


        $("#add-new").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addnew",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        $("#check-button").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=check",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        console.log(updateTable);
        var orderby = ' order by voucherid desc';
        var oTable = $('#example').dataTable({
            destroy: true,
            "responsive": true,
            "bServerSide": true,
            "sAjaxSource": "retrievetable.jsp?table=" + updateTable + "&code=<%= mod.getReferID_Master()%>&" + colsTosend + "&order=" + orderby,
            "order": [[0, "asc"]],
            "aoColumns": [
                {"sTitle": columnName[0], "mData": columntoView[0], "sWidth": "15%", "bSearchable": true},
                {"sTitle": columnName[1], "mData": columntoView[1], "sWidth": "27%"},
                {"sTitle": "Action", "mData": null, "bSortable": false, "bSearchable": false, "sWidth": "13%"}
            ],
            "aoColumnDefs": [{
                    "aTargets": [2],
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {


                        var a = $('<%= Button.viewlistButton()%>');
                        var b = $('<%= Button.editlistButton()%>');
                        var c = $('<%= Button.deletelistButton()%>');


                        a.on('click', function () {
                            //console.log(oData);
                            BootstrapDialog.show({
                                type: BootstrapDialog.TYPE_DEFAULT,
                                title: 'View <%= mod.getModuleDesc()%>',
                                //message: $('<body></body>').load('list_coa.jsp')
                                message: function (dialog) {
                                    var $content = $('<body></body>').load('cp_company_view.jsp?id=' + oData.code);
                                    //$('body').on('click', '.thisresult_nd', function(event){
                                    //    dialog.close();
                                    //    event.preventDefault();
                                    //});

                                    return $content;
                                }
                            });
                            return false;
                        });
                        b.on('click', function () {
                            //console.log(oData);

                            $.ajax({
                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=edit&referenceno=" + oData.code,
                                success: function (result) {
                                    // $("#haha").html(result);
                                    $('#herey').empty().html(result).hide().fadeIn(300);
                                }});
                            return false;

                        });
                        c.on('click', function () {

                            BootstrapDialog.confirm({
                                title: 'Confirmation',
                                message: 'Are you sure to delete?',
                                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                closable: true, // <-- Default value is false
                                draggable: true, // <-- Default value is false
                                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                                callback: function (result) {
                                    // result will be true if button was click, while it will be false if users close the dialog directly.
                                    if (result) {
                                        $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&referenceno=" + oData.code,
                                            success: function (result) {
                                                // $("#haha").html(result);
                                                setTimeout(function () {
                                                    oTable.fnClearTable();
                                                }, 500);
                                            }});
                                    }
                                }
                            });

                            return false;

                        });

                        $(nTd).empty();
                        $(nTd).attr("class", 'btn-group');
                        $(nTd).prepend(a, b, c);
                        //$(nTd).prepend(t);
                    }

                }]




        });

        $('#godeletex').on('click', function () {
            var v = $('#placeno').html();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&referenceno=" + v,
                success: function (result) {
                    oTable.fnClearTable();
                    $('#deleteModalx').modal('hide');
                }
            });
        });




    });
</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="add-new" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> Add <%= mod.getModuleDesc()%></button>
                    </td>
                    <td valign="middle" align="right">
                        <button id="refresh" class="btn btn-default btn-sm pull-right refresh"><i class="fa fa-refresh"></i> Refresh</button>
                    </td>
                </tr>
            </table>
            <hr style="color:#333; border:thick">
            <div class="dataTable_wrapper"> 
                <table class="table table-striped table-bordered table-hover" id="example">
                </table>
            </div>
        </div>
    </div>
</div>