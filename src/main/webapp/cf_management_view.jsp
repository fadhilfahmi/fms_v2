<%-- 
    Document   : cf_management_view
    Created on : Apr 18, 2016, 4:53:29 PM
    Author     : HP
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ManagementDAO"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Management"%>
<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
Management view = (Management) ManagementDAO.getInfo(log,request.getParameter("id"));
%>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
           
            
              $( ".printbutton" ).click(function() {
                  //var a = $("form").serialize();
                  var a = $("#saveform :input[value!='']").serialize(); 
                  //alert($('input[name="ad_amount"]').val());
                  checkField();
                  var b = checkField();
                  if(b==false){
                    $.ajax({
                        data: a,
                        type: 'POST',
                        url: "DataController?do=add&formlevel=master",
                        success: function (result) {
                          $('#maincontainer').empty().html(result).hide().fadeIn(300);
                        }
                    });
                     
                  }    
                  
               
                    return false;
              });
              
         });
        </script>
            <div id ="maincontainer">
                <table id="table_2" cellspacing="0">
                    
               <tr>
                   <td width="30%" class="bd_bottom" align="left" valign="top">Code</td>
                   <td width="70%" class="bd_bottom" align="left"><%= view.getCode() %></td>
               </tr>
               <tr>
                   <td class="bd_bottom" align="left" valign="top">Name</td>
                   <td class="bd_bottom" align="left"> 
                       <%= view.getDescp()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">CoRegistration No</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getCoregister()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Address</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getAddress()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Postcode</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getPostcode()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">City</td>
                   <td class="bd_bottom" align="left">
                       <%= view.getCity()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">State</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getState()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Employee Tax</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getEmployeetax()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Integration</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getFlag1()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Intercrop</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getFlag2()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Integration Management Code</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getMgtcode1()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Intercrop Management Code</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getMgtcode2()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Integration Management Description</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getMgtdesc1()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Intercrop Management Description</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getMgtdesc2()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Fax</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getFax()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Email</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getEmail()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Website</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getWebsite()%>
                   </td>
               </tr>
               
          </table>
            </div>
      

      
      
      
      

