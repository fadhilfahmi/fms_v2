<%-- 
    Document   : gl_debitnote_list
    Created on : Oct 26, 2016, 10:17:43 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.financial.gl.DebitNoteDAO"%>
<%@page import="com.lcsb.fms.util.model.ListTable"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.ModuleItem"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.ConnectionModel"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,java.util.*" errorPage="" isThreadSafe="false" %>
<%@ page import="java.util.Date"%>
<%

    Module mod = (Module) DebitNoteDAO.getModule("mill");

    response.setHeader("Cache-Control", "no-cache");

    SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
    DecimalFormat dcf = new DecimalFormat("###,###,###,###,###,###,##0.00");
    String[] dat = new String[17];
    String date = formatter.format(new Date());
%>

<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/tether.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>

<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        var updateTable = '<%= mod.getMainTable()%>';

    <%
    List<ListTable> mlist = (List<ListTable>) DebitNoteDAO.tableList("mill");
    for (ListTable m : mlist) {
        //if(m.getList_View().equals("1")){
%>
        columntoView.push('<%= m.getColumnName()%>');
        columnName.push('<%= m.getTitleName()%>');
    <%
                //}

            }
    %>

        var colsTosend = 'cols=';
        for (m = 0; m < columntoView.length; ++m) {//put value from database to variable jget
            if (m == columntoView.length - 1) {
                colsTosend += columntoView[m];
            } else {
                colsTosend += columntoView[m] + '&cols=';
            }
        }


        $("#add-new").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addnew",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        $("#check-button").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=check",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });



        var orderby = 'order by voucherno desc';
        var oTable = $('#example').DataTable({
            destroy: true,
            "aaSorting": [[1, "desc"]],
            "responsive": true,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "retrievetable.jsp?table=" + updateTable + "&code=<%= mod.getReferID_Master()%>&" + colsTosend + "&order=" + orderby,
            "aoColumns": [
                {"sTitle": columnName[0], "mData": columntoView[0], "sWidth": "9%", "bSearchable": true},
                {"sTitle": columnName[1], "mData": columntoView[1], "sWidth": "9%"},
                {"sTitle": columnName[2], "mData": columntoView[2], "sWidth": "15%"},
                {"sTitle": columnName[3], "mData": columntoView[3], "sWidth": "25%"},
                {"sTitle": columnName[4], "mData": columntoView[4], "sWidth": "15%"},
                {"sTitle": columnName[5], "mData": columntoView[5], "sWidth": "15%", "bVisible": false},
                {"sTitle": columnName[6], "mData": columntoView[6], "sWidth": "15%", "bVisible": false},
                {"sTitle": "Status", "mData": null, "sWidth": "15%"},
                {"sTitle": "Action", "mData": null, "bSortable": false, "bSearchable": false, "sWidth": "13%"}

            ],
            "aoColumnDefs": [{
                    "aTargets": [8],
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {



                        var e = $(icon('delete', oData.checkid, oData.JVno, oData.postflag));
                        var b = $(buttonJVforDC(oData.JVno));


                        b.on('click', function () {
                            //console.log(oData);
                            BootstrapDialog.confirm({
                                title: 'Confirmation',
                                message: 'This Debit Note will be send and generate Journal Voucher. Are you sure to proceed?',
                                type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                closable: true, // <-- Default value is false
                                draggable: true, // <-- Default value is false
                                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                btnOKLabel: 'Proceed', // <-- Default value is 'OK',
                                btnOKClass: 'btn-primary', // <-- If you didn't specify it, dialog type will be used,
                                callback: function (result) {
                                    // result will be true if button was click, while it will be false if users close the dialog directly.
                                    if (result) {
                                        $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=generateJV&redirect=list&referno=" + oData.noteno,
                                            success: function (result) {
                                                setTimeout(function () {
                                                    oTable.draw();
                                                }, 500);
                                            }
                                        });
                                    }
                                }
                            });
                            return false;
                        });

                        e.on('click', function () {
                            //console.log(oData);
                            if (iconClick('delete', '', oData.JVno, oData.postflag) == 0) {

                                BootstrapDialog.confirm({
                                    title: 'Confirmation',
                                    message: 'Are you sure to delete?',
                                    type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                    closable: true, // <-- Default value is false
                                    draggable: true, // <-- Default value is false
                                    btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                    btnOKLabel: 'Delete', // <-- Default value is 'OK',
                                    btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                                    callback: function (result) {
                                        // result will be true if button was click, while it will be false if users close the dialog directly.
                                        if (result) {
                                            $.ajax({
                                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&referno=" + oData.noteno,
                                                success: function (result) {
                                                    // $("#haha").html(result);
                                                    setTimeout(function () {
                                                        oTable.draw();
                                                    }, 500);
                                                }});
                                        }
                                    }
                                });


                                return false;
                            }

                            return false;
                        });

                        $(nTd).empty();
                        $(nTd).attr("class", 'btn-group');
                        $(nTd).attr("align", 'right');
                        $(nTd).prepend(b, e);


                    }

                },
                {
                    "aTargets": [7], //special rules for determine a status
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        var stat = '<span class="label label-primary">Preparing</span>';


                        if (oData.JVno.length > 0) {
                            stat = '<span class="label label-success">JV Created</span>';
                        }
                        if (oData.postflag == 'Cancel') {
                            stat = 'Cancelled';
                        }

                        $(nTd).empty();
                        //$(nTd).attr("id",'btntest');
                        $(nTd).prepend(stat);
                    }

                },
                {
                    "aTargets": [4], //special rules for determine a status
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        var s = parseFloat(oData.total);
                        var amt = formatCurrency(s);

                        $(nTd).empty();
                        $(nTd).attr("class", 'align-right');
                        $(nTd).prepend(amt);
                    }

                }]



        });

        $('#example tbody').on('click', 'tr', function () {
            var data = oTable.row(this).data();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=view&referno=" + data.noteno,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });


        $('body').on('click', '#refresh', function (e) {
            oTable.draw();
            return false;
        });

        $('#extract').click(function (e) {
            e.preventDefault();

            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                size: BootstrapDialog.SIZE_WIDE,
                //animate: false,
                title: 'Extract Data From Mill/Estate',
                message: function (dialog) {
                    var $content = $('<body></body>').load('gl_debitnote_extract.jsp');
                    //$('body').on('click', '.thisresult_nd', function(event){
                    //    dialog.close();
                    //    event.preventDefault();
                    //});
                    $('body').on('click', '.getcode', function (event) {
                        var cont = '<i class="fa fa-cog fa-spin fa-fw"></i><span>Downloading...</span>';
                        var icon2 = '<i class="fa fa-cog fa-spin fa-fw"></i><span>Downloading...</span>';
                        var t = $(this);
                        $(t).empty().html(cont).hide().fadeIn(300);
                        //dialog.close();
                        var c = $(t).attr('id');
                        var y = $('#year').val();
                        var p = $('#period').val();
                        $.ajax({
                            async: true,
                            url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=retrieve&refer=" + c + "&year=" + y + "&period=" + p,
                            success: function (result) {
                                console.log(result);
                                if (result == 0) {
                                    setTimeout(function () {
                                        $(t).empty().html('<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Failed').hide().fadeIn(300);
                                    }, 1000);

                                } else {
                                    setTimeout(function () {
                                        $(t).empty().html('<i class="fa fa-check-circle" aria-hidden="true"></i> Downloaded').hide().fadeIn(300);}, 1000);
                                    oTable.draw();
                                }
                            }
                        });
                    });
                    return $content;
                },
                buttons: [{
                        label: 'Close',
                        action: function (dialogRef) {
                            dialogRef.close();
                        }
                    }]
            });

            /* BootstrapDialog.show({
             type:BootstrapDialog.TYPE_DEFAULT,
             title: 'Finalizing Account',
             message: function(dialog) {
             var $content = $('<body></body>').load('gl_accountperiod_set.jsp?');
             //$('body').on('click', '.thisresult_nd', function(event){
             //    dialog.close();
             //    event.preventDefault();
             //});
             return $content;
             }
             });*/
        });


    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>

                    <td valign="middle" align="left">
                        <button id="extract" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> Extract From Mill/Estate</button>&nbsp;
                        <button id="refresh" class="btn btn-default btn-sm pull-right refresh"><i class="fa fa-refresh"></i> Refresh</button>
                        <!--<button id="add-debitnote" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> From Sales Debit Note</button>-->
                    </td>
                </tr>
            </table>
            <br>       
            <div class="dataTable_wrapper"> 
                <table class="table table-striped table-bordered table-hover" id="example">
                </table>
            </div>

        </div>
    </div>
</div>

