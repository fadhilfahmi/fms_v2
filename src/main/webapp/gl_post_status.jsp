<%-- 
    Document   : gl_post_status
    Created on : Mar 9, 2016, 3:39:15 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.dao.financial.gl.JournalVoucherDAO"%>
<%@page import="com.lcsb.fms.financial.gl.post.Master"%>
<%@page import="com.lcsb.fms.financial.gl.post.Distribute"%>
<%@page import="com.lcsb.fms.financial.gl.post.ErrorPost"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
Distribute dist = (Distribute) session.getAttribute("post_detail");
Master ms = (Master) dist.getMaster();
String module_id = ModuleDAO.moduleABB(ms.getVtype());
String status = "";
String statusStr = "";
String panelClass = "";
String statusIcon = "";
if(dist.isSuccess()){
    status = "Failed";
    panelClass = "panel-red";
    statusIcon = "<i class=\"fa fa-exclamation-circle fail-red\"></i>";
    statusStr = "The voucher "+ms.getNovoucher()+" failed to approve.";
}else{
    status = "Successful";
    panelClass = "panel-green";
    statusIcon = "<i class=\"fa fa-check-circle right-green\"></i>";
    statusStr = "The voucher <strong>"+ms.getNovoucher()+"</strong> was successfully approved and posted.";
    if(!dist.getNoteNo().equals("none")){
        String typeNote = DebitCreditNoteDAO.getTypeNote(dist.getNoteNo());
        statusStr += "<br>"+typeNote+" has been created for this transaction.";
    }
    
}


%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
    $( ".editjv" ).click(function() {
    //var a = $("form").serialize();
        var a = $(this).attr('id');
        var b = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=020104&process=editjvitem&referno="+a,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
        return false;
    });
              
$( "#post" ).click(function() {
    //var a = $("form").serialize();
    var a = $(this).attr('title');

    BootstrapDialog.confirm({
        title: 'Confirmation',
        message: 'Are you sure to post this?',
        type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
        btnOKLabel: 'Post', // <-- Default value is 'OK',
        btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
        callback: function(result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if(result) {
                $.ajax({
                    url: "PathController?moduleid=020104&process=distjv&referno="+a,
                    success: function (result) {
                        $('#herey').empty().html(result).hide().fadeIn(300);
                    }
                });
            }
        }
    });
    return false;
});

});
</script>
<div id ="maincontainer">
<div class="partition">
       <div class="bodyofpartition">
 			<table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate" width="100%">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= module_id %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                                        
                    </td>
                </tr>
          </table>
                        <p></p>
  
 
                    <div class="panel <%= panelClass %>">
                        <div class="panel-heading">
                            Post Status
                        </div>
                        <div class="panel-body">
                        <div class="jumbotron">
                            <h1><%= statusIcon %> <%= status %></h1>
                            <p class="font-post-md"><%= statusStr %></p>
                        </div>
                        </div>
                        <div class="panel-footer">
                            <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Bil</th>
                                            <th>
                                                Status<small></small>
                                            </th>
                                            <th>
                                                Status ID<small></small>
                                            </th>
                                            <th>
                                                Status Description<small></small>
                                            </th>
                                            <th>
                                                Remark<small></small>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
<%
    int i = 0;

List<ErrorPost> listAll = (List<ErrorPost>) dist.getListError();
    for (ErrorPost j : listAll) {
        i++;
        String icon = "";
        if(j.getError()){
            icon = "<i class=\"fa fa-exclamation-circle fail-red\"></i>";
        }else{
            icon = "<i class=\"fa fa-check-circle right-green\"></i>";
        }
%>
                                        <tr>
                                            <th><%= i %></th>
                                            <td><%= icon %></td>
                                            <td><%= j.getErrorID() %></td>
                                            <td><%= j.getErrorDesc() %></td>
                                            <td><%= j.getErrorItem() %></td>
                                        </tr>
                                        <%
                                            }
  %>
                                       
                                        
                                    </tbody>
                                </table>
                        </div>
                    </div>
               
                                
                                
		</div>
	</div>
</div>
      

      