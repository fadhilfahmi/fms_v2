<%-- 
    Document   : list_bank_view
    Created on : Mar 4, 2016, 4:06:59 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoice"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<div class="list-group"><%
            
            String keyword = request.getParameter("keyword");
            List<SalesInvoice> slist = (List<SalesInvoice>) OfficialReceiptDAO.getSalesInvoice(log,keyword);
            for (SalesInvoice c : slist) { 
               %>
       
            <a href="<%= c.getBname()%>" id="<%= c.getInvref()%>" id1="<%= c.getAmountno()%>"   title="" class="list-group-item thisresult">
                <div id="left_div"><%= c.getInvref()%></div>
                <div id="right_div"><%= c.getBname()%> <span class="pull-right"><%= GeneralTerm.currencyFormat(c.getAmountno()) %></span></div>
            </a>

               <%    
            } 
        %>
</div>