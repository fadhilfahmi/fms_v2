<%-- 
    Document   : tx_gst03_add
    Created on : Nov 24, 2016, 12:45:20 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.dao.EstateDAO"%>
<%@page import="com.lcsb.fms.model.financial.tx.TxReconcileMaster"%>
<%@page import="com.lcsb.fms.dao.financial.tx.TaxGst03DAO"%>
<%@page import="com.lcsb.fms.dao.financial.tx.TaxMasterDAO"%>
<%@page import="com.lcsb.fms.dao.financial.tx.TaxInputDAO"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) TaxGst03DAO.getModule();

    String year = String.valueOf(TaxMasterDAO.getTaxPeriod(log).getYear());
    String period = String.valueOf(TaxMasterDAO.getTaxPeriod(log).getPeriod());
    double c1 = TaxGst03DAO.getAmount(log,year, period, "'SR','DS'", "amount");
    double c2 = TaxGst03DAO.getAmount(log,year, period, "'SR','DS','AJS'", "outamt");
    double c3 = TaxGst03DAO.getAmount(log,year, period, "'TX','IM','TX-E43','TX-RE'", "amount");
    double c4 = TaxGst03DAO.getAmount(log,year, period, "'TX','IM','TX-RE','AJP','TX-E43'", "inamt");
    double c6 = TaxGst03DAO.getAmount(log,year, period, "'ZRL'", "amount");
    double c7 = TaxGst03DAO.getAmount(log,year, period, "'ZRE'", "amount");
    double c8 = TaxGst03DAO.getAmount(log,year, period, "'ES','ES43'", "amount");
    double c9 = TaxGst03DAO.getAmount(log,year, period, "'RS'", "amount");
    double c10 = TaxGst03DAO.getAmount(log,year, period, "'IS'", "amount");
    double c12 = TaxGst03DAO.getAmount2(log,year, period, "'AJP'", "amount");
    double c13 = TaxGst03DAO.getAmount2(log,year, period, "'AJS'", "amount");
    double suspend = TaxGst03DAO.getAmount(log,year, period, "'IS'", "amount") * 0.06;

    double pay = 0.00;
    double claim = 0.00;

    double cal = c2 - c4;

    if (cal < 0) {
        claim = cal * -1;
    } else {
        pay = cal;
    }

    pay = GeneralTerm.twoDecimalDouble(pay);
    claim = GeneralTerm.twoDecimalDouble(claim);

%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/formValidation.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.js"></script>

<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $('.dateformat').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        });



        $('.getbank').click(function (e) {

            var a = $(this).attr('id');
            var b = $(this).attr('id1');
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Taxation',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_branch.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getbuyer').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Buyer Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_buyer.jsp?name=bname&code=bcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getestate').click(function (e) {

            var a = 'loccode';
            var b = 'locname';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Estate',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_estate.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getbroker').click(function (e) {

            var a = 'brokercd';
            var b = 'brokerde';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Broker',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_broker.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getproduct').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Product Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_product.jsp?name=prodname&code=prodcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getsub').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Sub Account Detail',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_sub.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('.calculate').keyup(function (e) {
            var todeduct = $(this).val();
            var theval = $(this).attr('id');


            if (todeduct != '') {
                var tot = 0;
                if (theval == 'debit') {
                    $('#credit').val(0.0);
                }
                if (theval == 'credit') {
                    $('#debit').val(0.0);
                }

            }
            return false;
        });


    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="savegst" ><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>
            <form data-toggle="validator" role="form" id="saveform">
                <input type="hidden" name="year" value="<%=year%>">
                <input type="hidden" name="period" value="<%=period%>">
                <div class="well">


                    <%       //period="9";
                        int i = 0;
                        List<TxReconcileMaster> listAll = (List<TxReconcileMaster>) TaxGst03DAO.getAllReconcile(log);
                        for (TxReconcileMaster j : listAll) {
                            i++;
                    %>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-sm">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Reconcile No&nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="reconrefer" name="reconrefer" value="<%= j.getRefno()%>">

                                </div>
                            </div>
                        </div>           
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Tax Debit</label>
                                <input type="text" class="form-control input-sm calculate" id="taxdebit" name="taxdebit" value="<%= GeneralTerm.currencyFormat(j.getTaxdebit())%>" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Tax Credit</label>
                                <input type="text" class="form-control input-sm calculate" id="taxcredit" name="taxcredit" value="<%= GeneralTerm.currencyFormat(j.getTaxcredit())%>" required>
                            </div>
                        </div>
                    </div>
                    <%}%>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group form-group-sm">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Account Code&nbsp&nbsp<span class="res_code"></span></label>
                                    <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="actcode" name="coacode" value="134166">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-sm getaccount" type="button" id="actcode" id1="actdesc"><i class="fa fa-cog"></i> Account</button>
                                        </span>
                                    </div>  
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Account Description&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="actdesc" name="coadescp" value="KASTAM TO PAY" autocomplete="off" > 
                            </div>
                        </div>

                    </div>

                </div>

                <div class="well">                      
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">


                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="3" ><input type="checkbox" name="amend" id="amend">
                                Pindaan/Amendment <input id="bil" name="bil" type="hidden" value="<%=i%>"/></td>
                            <td >&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="3" bgcolor="#99CCFF"><strong> BAHAGIAN A: BUTIRAN ORANG BERDAFTAR</strong></td>
                            <td >&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="3" bgcolor="#99CCFF"><strong><em>PART A : REGISTERED PERSON DETAILS</em></strong></td>
                            <td >&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="5%">&nbsp;</td>
                            <td width="44%">&nbsp;</td>
                            <td width="3%">&nbsp;</td>
                            <td width="47%">&nbsp;</td>
                            <td width="1%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>1) No. CBP*</td>
                            <td>&nbsp;</td>
                            <td rowspan="2"><input id="gstid" name="gstid" type="text" value="<%= EstateDAO.getEstateInfo(log,log.getEstateCode(), "estatecode").getGstid()%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;<em>&nbsp;GST No*</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>2) Nama Perniagaan</td>
                            <td>&nbsp;</td>
                            <td rowspan="2"><input id="name" name="name" type="text" size="50" value="<%= log.getEstateDescp()%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;<em>Name Of Business*</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="3" bgcolor="#99CCFF"><strong>BAHAGIAN B: BUTIRAN PENYATA</strong></td>
                            <td >&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="3" bgcolor="#99CCFF"><em><strong>PART B : RETURN DETAILS</strong></em></td>
                            <td >&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>3) Tempoh Bercukai*</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;<em>Taxable Period*</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;Tarikh Mula  </td>
                            <td>&nbsp;</td>
                            <td><input id="taxstart1" name="taxstart1" type="text" value="<%= AccountingPeriod.gst03Formatter(TaxMasterDAO.getTaxPeriod(log).getTaxstart())%>"/><input id="taxstart" name="taxstart" type="hidden" value="<%= TaxMasterDAO.getTaxPeriod(log).getTaxstart()%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;<em>&nbsp;Start Date</em></td>
                            <td>&nbsp;</td>
                            <td>DD - MM - (YYYY)</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;Tarikh Akhir  </td>
                            <td>&nbsp;</td>
                            <td><input id="taxend1" name="taxend1" type="text" value="<%= AccountingPeriod.gst03Formatter(TaxMasterDAO.getTaxPeriod(log).getTaxend())%>"/><input id="taxend" name="taxend" type="hidden" value="<%= TaxMasterDAO.getTaxPeriod(log).getTaxend()%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;<em>&nbsp;&nbsp;&nbsp;End Date</em></td>
                            <td>&nbsp;</td>
                            <td>DD - MM - (YYYY)</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>4) Tarikh Akhir Serahan Penyata dan Bayaran*</td>
                            <td>&nbsp;</td>
                            <td rowspan="2"><input id="taxdue1" name="taxdue1" type="text" value="<%= AccountingPeriod.gst03Formatter(TaxMasterDAO.getTaxPeriod(log).getTaxdue())%>"/><input id="taxdue" name="taxdue" type="text" value="<%= AccountingPeriod.gst03FormatterNormal(TaxMasterDAO.getTaxPeriod(log).getTaxdue())%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;<em>Return Payment Due Date*</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>5) Cukai Output</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;<em>Output Tax</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;a) Jumlah Nilai Pembekalan Berkadar Standard *</td>
                            <td>RM</td>
                            <td rowspan="2"><input id="c1" name="c1" type="text" value="<%= c1%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em>Total Value of Standard Rated Supply *</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;b) Jumlah Cukai Output (Termasuk Nilai Cukai keatas Hutang Lapuk Dibayar Balik dan Pelarasan lain) *</td>
                            <td>RM</td>
                            <td rowspan="2"><input id="c2" name="c2" type="text" value="<%=c2%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;<em>&nbsp;Total Output Tax (Inclusive of Tax Value on Bad Debt Recovered &amp; other Adjustments) *</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>6) Cukai Input/Input Tax</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;a) Jumlah Nilai Perolehan Berkadar Standard dan Berkadar Rata *</td>
                            <td>RM</td>
                            <td rowspan="2"><input id="c3" name="c3" type="text" value="<%=c3%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em>Total Value of Standard Rated and Flat Rate Acquisitions *</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;b) Jumlah Cukai Input (Termasuk Nilai Cukai keatas Pelepasan Hutang Lapuk dan Pelarasan lain) *</td>
                            <td>RM</td>
                            <td rowspan="2"><input id="c4" name="c4" type="text" value="<%=c4%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em>Total Input Tax (Inclusive of Tax Value on Bad Debt Relief &amp; Other Adjustments) *</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>7) Amaun CBP Kene Dibayar(Butiran 5B - Butiran 6b) *</td>
                            <td>RM</td>
                            <td rowspan="2"><input id="pay" name="pay" type="text" value="<%=pay%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;<em>&nbsp;GST Amount Payable (Item 5b-Item 6b) *</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>8) Amaun CBP Boleh Dituntut (Butiran 6b-Butiran 5b) *</td>
                            <td>RM</td>
                            <td rowspan="2"><input id="claim" name="claim" type="text" value="<%=claim%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;<em>&nbsp;GST Amount Claimable (Item 6b-Item 5b)</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>9) Adakah anda memilih untuk membawa ke hadapan pembayaran balik CBP?</td>
                            <td>&nbsp;</td>
                            <td><input type="checkbox" name="b5" id="b5">
                                (Please Tick if Yes)</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;<em>Do You choose to carry forward refund?</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="3" bgcolor="#99CCFF"><strong>BAHAGIAN C : MAKLUMAT TAMBAHAN</strong></td>
                            <td >&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="3" bgcolor="#99CCFF"><strong><em>PART C : ADDITIONAL INFORMATION</em></strong></td>
                            <td >&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>10) Jumlah Nilai Pembekalan Tempatan Berkadar Sifar *</td>
                            <td>RM</td>
                            <td rowspan="2"><input id="c6" name="c6" type="text" value="<%=c6%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;<em>Total Value of Local Zero-Rated Supplies*</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>11) Jumlah Nilai Pembekalan Eksport*</td>
                            <td>RM</td>
                            <td rowspan="2"><input id="c7" name="c7" type="text" value="<%=c7%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;<em>Total Value of Export Supplies</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>12) Jumlah Nilai Pembekalan Dikecualikan *</td>
                            <td>RM</td>
                            <td rowspan="2"><input id="c8" name="c8" type="text" value="<%=c8%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;<em>&nbsp;Total Value of Exempt Supplies*</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>13) Jumlah Nilai Pembekalan Diberi Pelepasan CBP *</td>
                            <td>RM</td>
                            <td rowspan="2"><input id="c9" name="c9" type="text" value="<%=c9%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;<em>Total Value of Supplies Granted GST Relief</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>14) Jumlah Nilai Pengimportan Barang Dibawah Skim Pedagang Diluluskan *</td>
                            <td>RM</td>
                            <td rowspan="2"><input id="c10" name="c10" type="text" value="<%=c10%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;<em>Total Value of Goods Imported Under Approved Trader Scheme*</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>15) Jumlah Nilai CBP Import Digantung dibawah butiran 14* 6% drp item 14</td>
                            <td>RM</td>
                            <td rowspan="2"><input id="suspend" name="suspend" type="text" value="<%=suspend%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;<em>Total Value of GST Suspended Under item 14*</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>16) Jumlah Nilai Perolehan Harta Modal *</td>
                            <td>RM</td>
                            <td rowspan="2"><input id="c11" name="c11" type="text" value="0.00"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;<em>&nbsp;Total Value of Capital Goods Acquired *</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>17) Jumlah Nilai Pelepasan Hutang Lapuk Termasuk Cukai*</td>
                            <td>RM</td>
                            <td rowspan="2"><input id="c12" name="c12" type="text" value="<%=c12%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;<em>Total Value of Bad Debt Relief inclusive Tax*</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>18) Jumlah Nilai Hutang Lapuk Dibayar Balik Termasuk Cukai *</td>
                            <td>RM</td>
                            <td rowspan="2"><input id="c13" name="c13" type="text" value="<%=c13%>"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;<em>Total Value Of Bad Debt Recovered Inclusive Tax*</em></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="4">19) Pecahan Nilai Cukai Output mengikut Kod Industri Utama</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="4"><em>Breakdown Value Of Tax In Accordance with Major Industries Code</em></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="5%">&nbsp;</td>
                            <td width="12%"><strong>Kod MSIC (<em>MSIC Code</em>)</strong></td>
                            <td width="3%">&nbsp;</td>
                            <td width="28%"><strong>Nilai Cukai Output (<em>Value Of Output</em> )</strong></td>
                            <td width="47%"><strong>Peratusan (<em>Percentage</em>)</strong></td>
                            <td width="5%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <%
                            /*int pos=13;
                        int n=14;
                        Statement stmtcon=con.createStatement();
                        ResultSet rs=stmtcon.executeQuery("select a.compcode,b.msic from tx_output_detail a left join buyer_info b on a.compcode=b.code where year='"+year+"' and period='"+period+"' group by a.compcode,b.msic limit 5 ");
                        while(rs.next())
                        {
                                pos+=pos+2;
                                n+=n+2;*/

                        %>
                        <tr>
                            <td>&nbsp;</td>
                            <td><input id="i14" name="i14" type="text" size="10" value=""/></td>
                            <td><div align="right">RM</div></td>
                            <td><input id="c15" name="c15" type="text" value="0.00" onChange="calPercent()" on/></td>
                            <td><input id="p15" name="p15" type="text" size="15" value="0.00"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><input id="i16" name="i16" type="text" size="10" value=""/></td>
                            <td><div align="right">RM</div></td>
                            <td><input id="c17" name="c17" type="text" value="0.00" onChange="calPercent()"/></td>
                            <td><input id="p17" name="p17" type="text" size="15" value="0.00"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><input id="i18" name="i18" type="text" size="10" value=""/></td>
                            <td><div align="right">RM</div></td>
                            <td><input id="c19" name="c19" type="text" value="0.00" onChange="calPercent()"/></td>
                            <td><input id="p19" name="p19" type="text" size="15" value="0.00"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><input id="i20" name="i20" type="text" size="10" value=""/></td>
                            <td><div align="right">RM</div></td>
                            <td><input id="c21" name="c21" type="text" value="0.00" onChange="calPercent()"/></td>
                            <td><input id="p21" name="p21" type="text" size="15" value="0.00"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><input id="i22" name="i22" type="text" size="10" value=""/></td>
                            <td><div align="right">RM</div></td>
                            <td><input id="c23" name="c23" type="text" value="0.00" onChange="calPercent()"/></td>
                            <td><input id="p23" name="p23" type="text" size="15" value="0.00"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <%//}%>

                        <tr>
                            <td>&nbsp;</td>
                            <td>Others</td>
                            <td><div align="right">RM</div></td>
                            <td><input id="c24" name="c24" type="text" value="0.00" onChange="calPercent()"/></td>
                            <td><input id="p24" name="p24" type="text" size="15" value="0.00"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>TOTAL</td>
                            <td><div align="right">RM</div></td>
                            <td><input id="ctotal" name="ctotal" type="text" value="0.00"/></td>
                            <td><input id="ptotal" name="ptotal" type="text" size="15" value="0.00"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><input id="pos" name="pos" type="hidden" value="<%//=pos%>"/>
                                <input id="n" name="n" type="hidden" value="<%//=n%>"/></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>


