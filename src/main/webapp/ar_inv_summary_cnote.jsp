<%-- 
    Document   : ar_inv_summary_invoice
    Created on : Apr 7, 2017, 2:35:38 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.financial.ar.ArCreditNoteDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArCreditNote"%>
<%@page import="com.lcsb.fms.dao.financial.ar.ArDebitNoteDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArDebitNote"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoice"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) ArCreditNoteDAO.getModule();
%>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $(".auth_cnr").click(function (e) {
            e.preventDefault();
            var a = $(this).attr('id');
            var b = $(this).attr('name');


            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=printpreview&refer=" + a + "&auth="+b,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });



    });
</script>
<div class="invoice-content">
    <div class="table-responsive">
        <table class="table table-invoice">
            <thead>
                <tr>
                    <th>DETAIL</th>
                    <th><span class="pull-left">DATE</span></th>
                    <th><span class="pull-right">AMOUNT</span></th>
                    <th><span class="pull-right">ACTION</span></th>
                </tr>
            </thead>
            <tbody>

                <%
                    String typeAuth = "";
                    String eventAuth = "";
                
                    if(log.getListUserAccess().get(1).getAccess()){
                        typeAuth = "check";
                        eventAuth = "Check";
                    
                    }if(log.getListUserAccess().get(2).getAccess()){
                        typeAuth = "approval";
                        eventAuth = "Approve";
                    }
                    
                    List<ArCreditNote> listAll = (List<ArCreditNote>) ArCreditNoteDAO.getAll(log, typeAuth);
                    if (listAll.isEmpty()) {%>
                <tr>
                    <td colspan="6">
                        <span class="font-small-red">No data available.</span>
                    </td>
                </tr>

                <%}
                    for (ArCreditNote j : listAll) {
                %>
                <tr>
                    <td>
                        <%= j.getBuyername()%><br />
                        <small><%= j.getNoteno()%></small>
                    </td>
                    <td><span class="pull-left"><%= j.getNotedate()%></span><br />
                        <small></small></td>
                    <td><span class="pull-right"><%= GeneralTerm.currencyFormat(j.getTotal())%></span></td>
                    <td>

                        <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
                            <button id="<%= j.getNoteno()%>" class="btn btn-success btn-xs auth_cnr" title="<%= mod.getModuleID()%>" name="<%=eventAuth%>"><%=eventAuth%></button>

                        </div>
                    </td>
                </tr>
                <%}%>

            </tbody>
        </table>
    </div>

</div>
