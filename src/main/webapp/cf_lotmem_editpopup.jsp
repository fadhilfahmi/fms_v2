<%-- 
    Document   : cf_lotmem_editpopup
    Created on : Dec 8, 2016, 12:19:12 AM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.model.setup.configuration.LotMember"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.LotMemberDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.model.financial.gl.AccountPeriod"%>
<%@page import="com.lcsb.fms.dao.financial.gl.AccountPeriodDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    LotMember lt = (LotMember) LotMemberDAO.getInfo(log,request.getParameter("memcode"));
%>
<form data-toggle="validator" role="form" id="saveform">
    <div class="row">
        <div class="col-sm-6">
            <div class="well">
                <div class="row self-row">
                    <label for="inputName" class="control-label">Basic Detail &nbsp&nbsp<span id="res_code"></span></label>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <label for="inputName" class="control-label">Name</label>
                        <div class="form-group">
                            <input type="hidden" class="form-control input-sm" id="code" name="code" placeholder="" autocomplete="off" value="<%= lt.getMemCode()%>">
                            <input type="text" class="form-control input-sm" id="name" name="name" placeholder="" autocomplete="off" value="<%= lt.getMemName()%>">
                        </div>   
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <label for="inputName" class="control-label">IC No</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="ic" name="ic" placeholder="" autocomplete="off" value="<%= lt.getIc()%>">
                        </div>   
                    </div>
                    <div class="col-sm-6">
                        <label for="inputName" class="control-label">Status</label>
                        <select class="form-control input-sm" id="paymethod" name="paymethod">
                            <%= ParameterDAO.parameterList(log,"Lot Member Status", lt.getStatus())%>
                        </select>   
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <label for="inputName" class="control-label">Payment Method &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                        <select class="form-control input-sm" id="paymethod" name="paymethod">
                            <%= ParameterDAO.parameterList(log,"Lot Payment Type", lt.getPaymethod())%>
                        </select>  
                    </div>
                    <div class="col-sm-6">
                        <label for="inputName" class="control-label">Bank</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="bank" name="bank" placeholder="" autocomplete="off" value="<%= lt.getBank()%>">
                        </div>   
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <label for="inputName" class="control-label">Account No</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="accno" name="accno" placeholder="" autocomplete="off" value="<%= lt.getAcc()%>">
                        </div>   
                    </div>
                </div>

            </div></div>
        <div class="col-sm-6">
            <div class="well">

                <div class="row self-row">
                    <label for="inputName" class="control-label">Posting Info &nbsp&nbsp<span id="res_code"></span></label>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <label for="inputName" class="control-label">Address 1</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="address1" name="address1" placeholder="eg : Jalan, Lorong, Taman" autocomplete="off" value="<%= lt.getAddress1()%>" >
                        </div>   
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <label for="inputName" class="control-label">Address 2</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="address2" name="address2" placeholder="Postcode, City" autocomplete="off" value="<%= lt.getAddress2()%>" >
                        </div>   
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <label for="inputName" class="control-label">Address 3</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="address3" name="address3" placeholder="State" autocomplete="off" value="<%= lt.getAddress3()%>" >
                        </div>   
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <label for="inputName" class="control-label">Address 4</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="address4" name="address4" placeholder="" autocomplete="off" value="<%= lt.getAddress4()%>" >
                        </div>   
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <label for="inputName" class="control-label">Address 5</label>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="address5" name="address5" placeholder="" autocomplete="off" value="<%= lt.getAddress5()%>" >
                        </div>   
                    </div>
                </div>


            </div>
        </div>
    </div>
</form>
