<%-- 
    Document   : cf_earning_add
    Created on : Apr 13, 2017, 12:13:24 PM
    Author     : user
--%>


<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.EarningDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) EarningDAO.getModule();

%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/formValidation.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        
        $('#getbank').click(function (e) {

            var a = 'code';
            var b = 'descp';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Taxation',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_bank.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });
    });
    /*$('#saveform').formValidation({
     message: 'This value is not valid',
     icon: {
     valid: 'glyphicon glyphicon-ok',
     invalid: 'glyphicon glyphicon-remove',
     validating: 'fa fa-refresh'
     },
     fields: {
     
     code: {
     required:true,
     validators: {
     notEmpty: {
     message: 'The Account Code is required'
     },
     digits: {
     message: 'The value can contain only digits'
     }
     }
     }
     }
     });*/


</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>



            <form data-toggle="validator" role="form" id="saveform">
                <input type="hidden" name="prepareid" id="pid" value="<%= log.getUserID()%>">
                <input type="hidden" name="preparename" id="pname" value="<%= log.getFullname()%>">
                <input type="hidden" name="preparedate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
                <div class="well">
                    <div class="row">

                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Earning Code&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="code" name="code" value="<%= AutoGenerate.get4digitNo(log,"cf_earninginfo", "code")%>" autocomplete="off" required readonly >  
                            </div>
                        </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Earning Description &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="descp" name="descp" autocomplete="off" value="">   
                            </div>
                        </div>
                    </div>
                
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Active Status &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="status" name="status">
                                    <%= ParameterDAO.parameterList(log,"YesNo Type", "")%>
                                </select> 
                            </div>
                        </div></div>
               
            </form>
        </div>
    </div>
</div>