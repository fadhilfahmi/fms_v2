<%-- 
    Document   : cb_bankreport_view
    Created on : Mar 2, 2017, 8:26:42 AM
    Author     : fadhilfahmi
--%>



<%@page import="com.lcsb.fms.model.financial.cashbook.BankReconcilationPK"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.BankReconcilation"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.BankReportParam"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.BankReportDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.ListTable"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.ModuleItem"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.ConnectionModel"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.CashVoucherDAO"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,java.util.*" errorPage="" isThreadSafe="false" %>
<%@ page import="java.util.Date"%>
<%

    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) BankReportDAO.getModule();
    BankReportParam prm = (BankReportParam) session.getAttribute("br_param");

%>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<script src="bootstrapselect/js/bootstrap-select.js"></script>
<script src="bootstrapselect/js/bootstrap-dropdown.js"></script>


<link rel="stylesheet" href="bootstrapselect/css/bootstrap-select.css">
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        callPnotify('<%= request.getParameter("status")%>', '<%= request.getParameter("text")%>');

        $('body').on('focus', ".datepick", function () {
            $(this).datepicker({
                format: 'yyyy-mm-dd',
                defaultDate: 'now',
                useCurrent: false,
                autoclose: true

            });
        });




        var oTable = $('#list').DataTable({
            responsive: true,
            "bPaginate": false,
            "aaSorting": [[0, "asc"]],
        });


        $("#addtrans").click(function () {

            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addtrans",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });

        $("#report-reconcile").click(function () {
            $('#maincontainer').empty().html('<div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>').hide().fadeIn(100);

            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=reconcilereport",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        $('body').on('click', '#refresh', function (e) {
            console.log('Refreshing');
            oTable.draw();
            return false;
        });

        $("#filter").click(function () {
            $("#filterdiv").toggle();
            //oTable.draw();
            return false;
        });

        $("#refine").click(function () {
            $("#filterdiv").toggle();
            //oTable.draw();
            return false;
        });


        $('body').on('change', ".newstatus", function () {
            var a = $(this).attr('id');
            var b = $(this).val();

            if (b == 'Clear') {

                var c = $('#amountori' + a).val();
                $('#newamtclear' + a).val(c);
            }

            return false;
        });

        $('.td-click').click(function (e) {



            var b = $(this).attr('id');
            console.log(b);
            e.preventDefault();

            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                size: BootstrapDialog.SIZE_WIDE,
                //animate: false,
                title: 'Member Code ' + b,
                message: function (dialog) {
                    var $content = $('<body></body>').load('cb_bankreport_update.jsp?memcode=' + b);
                    //$('body').on('click', '.thisresult_nd', function(event){
                    //    dialog.close();
                    //    event.preventDefault();
                    //});
                    return $content;
                },
                buttons: [{
                        //icon: 'fa fa-circle-o-notch',
                        label: 'Update & Close',
                        cssClass: 'btn-primary',
                        autospin: true,
                        action: function (dialogRef) {
                            var $button = this;
                            dialogRef.enableButtons(false);
                            dialogRef.setClosable(false);
                            //dialogRef.getModalBody().html('<span style="font-size:14px">Finalizing in progress...</span>');
                            var a = $("#saveform :input").serialize();
                            var code = $("#code").val();
                            var name = $("#name").val();
                            var ic = $("#ic").val();
                            var status = $("#status").val();
                            var payment = $("#payment").val();
                            var bank = $("#bank").val();
                            var accno = $("#accno").val();
                            var address1 = $("#address1").val();
                            var address2 = $("#address2").val();
                            var address3 = $("#address3").val();
                            var address4 = $("#address4").val();
                            var address5 = $("#address5").val();
                            $.ajax({
                                async: true,
                                data: a,
                                type: 'POST',
                                url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=updatememberinfo",
                                success: function (result) {
                                    // $("#haha").html(result);
                                    //$('#herey').empty().html(result).hide().fadeIn(300);
                                    if (result == 1) {
                                        setTimeout(function () {
                                            //dialogRef.close();
                                            dialogRef.enableButtons(true);
                                            dialogRef.setClosable(true);
                                            $button.disable();
                                            $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                            dialogRef.getModalBody().html('<i class="fa fa-check right-green"></i>&nbsp;&nbsp;<span style="font-size:14px">Success!</span>');


                                            $('#name' + code).html(name);
                                            $('#ic' + code).html(ic);
                                            $('#status' + code).html(status);
                                            $('#bank' + code).html(bank);
                                            $('#accno' + code).html(accno);
                                            $('#payment' + code).html(payment);
                                            $('#address' + code).html(address1 + ', ' + address2 + ', ' + address3 + ', ' + address4 + ', ' + address5);
                                            console.log();
                                            dialogRef.close();
                                            //$("#"+code).effect("pulsate", { times:3 }, 2000);
                                        }, 1000);
                                    } else {
                                        setTimeout(function () {
                                            //dialogRef.close();
                                            dialogRef.enableButtons(true);
                                            dialogRef.setClosable(true);
                                            //$button.disable();
                                            $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                            dialogRef.getModalBody().html('<i class="fa fa-times fail-red"></i>&nbsp;&nbsp;<span style="font-size:14px">Failed!</span>');
                                        }, 3000);
                                    }
                                }
                            });

                        }
                    }, {
                        label: 'Close',
                        action: function (dialogRef) {
                            dialogRef.close();
                        }
                    }]
            });

        });
        
        $('.selectpicker').selectpicker('refresh');

        $('.activerowy').click(function (e) {
            var b = $(this).attr('id');
            var tr = $(this).closest('tr');
            var row = oTable.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(format(b)).show();
                tr.addClass('shown');
                $('.selectpicker').selectpicker('refresh');
            }
        });


    });

</script>
<div id ="maincontainer">
    <div class="partition"> 

        <div class="bodyofpartition">

            <button id="report-reconcile" class="btn btn-default btn-sm"><i class="fa fa-file-text" aria-hidden="true"></i> Recouncil Report</button>

            <div class="pull-right">
                <button id="filter" class="btn btn-default btn-sm filter"><i class="fa fa-filter" aria-hidden="true"></i> Filter</button>
                <button id="refresh" class="btn btn-default btn-sm refresh"><i class="fa fa-refresh"></i></button>
            </div>
            <br><br> 
            <div id="filterdiv" class="well" style="display: none">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group" id="div_code">
                            <label for="inputName" class="control-label">Year &nbsp;&nbsp;<span id="res_code"></span></label>
                            <select class="form-control input-sm" id="newyear" name="newyear">
                                <%= ParameterDAO.parameterList(log, "Year", AccountingPeriod.getCurYear(log))%>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group" id="div_code">
                            <label for="inputName" class="control-label">Period &nbsp;&nbsp;<span id="res_code"></span></label>
                            <select class="form-control input-sm" id="newyear" name="newyear">
                                <%= ParameterDAO.parameterList(log, "Period", AccountingPeriod.getCurPeriod(log))%>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group" id="div_code">
                            <label for="inputName" class="control-label">&nbsp;</label>
                            <div class="form-group-sm input-group">

                                <button id="refine"  class="btn btn-default btn-sm" title="<%//= mod.getModuleID()%>" name="unpprove">Refine Search</button>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
            <form data-toggle="validator" role="form" id="saveform">
                <div class="dataTable_wrapper" id="datatable-render"> 
                    <table class="table table-bordered table-striped table-hover" id="list">
                        <thead>
                            <tr>
                                <th colspan="9">
                                    <span class="pull-left">
                                        <button  class="btn btn-default btn-xs savetrans" title="<%= mod.getModuleID()%>" id="savebutton" name="savetrans"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
                                        <button id="addtrans"  class="btn btn-default btn-xs addtrans" title="<%= mod.getModuleID()%>" id="<%//= v.getInvref()%>" name="addtrans"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Transaction</button>
                                        <button  class="btn btn-default btn-xs adddetail" title="<%= mod.getModuleID()%>" id="<%//= v.getInvref()%>" name="adddetail"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                    </span>
                                </th>
                            </tr>
                            <tr>
                                <th width="10%"></th>
                                <th width="10%">Date</th>
                                <th width="10%">Voucher No</th>
                                <th width="30%">Paid To/Paid By</th>
                                <th width="30%">Cheque</th>
                                <th width="10%">Amount</th>
                                <th width="30%">Remarks</th>
                                <th width="10%">Date Clear</th>
                                <th width="10%">Amount Clear</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                List<BankReconcilation> listx = (List<BankReconcilation>) BankReportDAO.getAll(log, prm);

                                int inc = 0;
                                String renderRow = "";
                                for (BankReconcilation j : listx) {
                                    inc++;

                                    BankReconcilationPK pk = j.getBankReconcilationPK();
                            %>
                            <tr  id="<%= pk.getRefer()%>">
                                <td width="3%">
                                    <input type="hidden" id="reconid<%= pk.getRefer()%>" name="reconid<%= pk.getRefer()%>" value="<%= pk.getRefer()%>">
                                    
                                    <input type="hidden" id="varians<%= pk.getRefer()%>" name="varians<%= pk.getRefer()%>" value="<%= j.getVarians()%>">
                                    <input type="hidden" id="depan<%= pk.getRefer()%>" name="depan<%= pk.getRefer()%>" value="<%= pk.getDepan()%>">
                                    <input type="hidden" id="remarks<%= pk.getRefer()%>" name="remarks<%= pk.getRefer()%>" value="<%= j.getRemarks()%>">
                                    <input type="hidden" id="amountori<%= pk.getRefer()%>" name="amountori<%= pk.getRefer()%>" value="<%= j.getAmountpv()%>">

                                    <%= inc%>
                                </td>
                                <td width="10%">
                                    <%= j.getVoucherdate()%>
                                </td>
                                <td width="15%">
                                    <%
                                        out.println(pk.getDepan().equals("PVN") ? "<span style=\"color:#ea6254\"><i class=\"fa fa-minus-square\" aria-hidden=\"true\"></i></span>" : "<span style=\"color:#40c2a6\"><i class=\"fa fa-plus-square\" aria-hidden=\"true\"></i></span>");
                                    %>    
                                    &nbsp;&nbsp;<%= pk.getVoucherno()%>
                                </td>
                                <td width="40%">
                                    <span class="label label-<%= j.getLabelPaymode()%>"><%//= j.getPaymode()%></span><%= j.getPaid()%>
                                </td>
                                <td width="10%">
                                    <%= j.getCek()%>
                                </td>
                                <td width="10%">
                                    <span class="pull-right"><%= GeneralTerm.currencyFormat(j.getAmountpv())%></span>
                                </td>
                                <td width="10%">
                                    <select id="<%= pk.getRefer()%>" class="selectpicker show-menu-arrow newstatus" data-style="btn-default btn-sm" data-width="110px" name="status<%= pk.getRefer()%>">
                                        <option data-content="<span class='label label-default'>None</span>" <% out.println(j.getStatus().equals("None") ? "selected" : ""); %>>None</option>
                                        <option data-content="<span class='label label-success'>Clear</span>" <% out.println(j.getStatus().equals("Clear") ? "selected" : ""); %>>Clear</option>
                                        <option data-content="<span class='label label-info'>Outstanding</span>" <% out.println(j.getStatus().equals("Outstanding") ? "selected" : ""); %>>Outstanding</option>
                                    </select>
                                </td>
                               
                                <td width="10%">
                                    <input type="text" name="dateclear<%= pk.getRefer()%>" class="form-control input-sm datepick" id="exampleInputAmount" <% out.println(j.getDateclear()== null ? "placeholder=\"Calendar\"" : "value="+j.getDateclear()+""); %> >
                                </td> 
                                <td width="10%">
                                    <input type="text" id="newamtclear<%= pk.getRefer()%>" name="amtclear<%= pk.getRefer()%>" class="form-control input-sm" placeholder="0.00" value="<%= j.getAmountclear() %>"></td>
                            </tr>

                            <%
                                }
                            %>       
                        </tbody>

                    </table>
                    <input type="hidden" name="totalRow" id="totalRow" value="<%=inc%>">
                </div>
            </form>
        </div>
    </div>
</div>
