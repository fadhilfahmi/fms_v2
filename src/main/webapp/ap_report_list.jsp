<%-- 
    Document   : ar_report_list
    Created on : Feb 17, 2017, 12:12:48 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.financial.ar.ArReportDAO"%>
<%@page import="com.lcsb.fms.dao.financial.gl.FinancialReportDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ChartofAccountDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage=""%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) ArReportDAO.getModule();

%>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $(".goto").click(function (e) {
            e.preventDefault();
            var link = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + link,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(100);
                }
            });
            return false;
        });
    });
</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <br>
            <div class="row">
                <div class="col-lg-6"><label>Vendor Report &nbsp;&nbsp;&nbsp;&nbsp;</label><small class="pull-right"> <i class="fa fa-info-circle" aria-hidden="true"></i> All reports and statements about Vendor.</small></div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="list-group">
                        <a href="statement" class="list-group-item goto">
                            <i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp;Statement Report
                        </a>
                        <a href="aging" class="list-group-item goto">
                            <i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp;Aging Analysis
                        </a>
                        <a href="summaryinvoice" class="list-group-item goto">
                            <i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp;Summary of Invoices
                        </a>
                    </div>
                </div>

            </div>  
        </div>
    </div>
</div>