<%-- 
    Document   : em_payroll_list
    Created on : Jun 5, 2017, 2:49:20 PM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.fms.dao.financial.employee.EmPayrollDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.general.DataTable"%>
<%@page import="com.lcsb.fms.util.model.ListTable"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.ModuleItem"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.ConnectionModel"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,java.util.*" errorPage="" isThreadSafe="false" %>
<%@ page import="java.util.Date"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) EmPayrollDAO.getModule();

    DataTable prm = (DataTable) session.getAttribute("dtparam");

    String view = "";
    String period = "";
    String year = "";
    String btn_all = "primary";
    String btn_today = "default";
    String btn_thismonth = "default";
    String select_year = "btn-default";
    String select_period = "btn-default";

    if (prm != null) {
        view = prm.getViewBy();
        period = prm.getPeriod();
        year = prm.getYear();
        btn_all = "default";

        if (view.equals("all")) {
            btn_all = "primary";
        } else if (view.equals("today")) {
            btn_today = "primary";
        } else if (view.equals("thismonth")) {
            btn_thismonth = "primary";
        } else if (view.equals("byperiod")) {
            select_year = "btn-primary";
            select_period = "btn-primary";
        }
    } else {
        view = "all";
        //period = AccountingPeriod.getCurPeriod(log);
        //year = AccountingPeriod.getCurYear(log);
    }

    response.setHeader("Cache-Control", "no-cache");
%>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        //var timeagoInstance = new timeago('<%//= SalesInvoiceDAO.getLastSync() %>');
        //timeagoInstance.render($('.need_to_be_rendered'));


        $.ajax({
            url: "PathController?moduleid=000000&moduletypeID=<%= mod.getModuleID()%>&process=datatable&view=<%= view%>&year=<%= year%>&period=<%= period%>",
            success: function (result) {
                $('#datatable-render').empty().html(result).hide().fadeIn(300);
            }
        });


        $("#add-new").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addnew",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        $("#check-button").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=check",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        $('body').on('click', '#refresh', function (e) {
            console.log('Refreshing');
            oTable.draw();
            return false;
        });

        $("#filter").click(function () {
            $("#filterdiv").toggle();
            //oTable.draw();
            return false;
        });

        $("#refine").click(function () {
            $("#filterdiv").toggle();
            //oTable.draw();
            return false;
        });

        

        $(".tochange").click(function (e) {
            $('.tochange').addClass('btn-default').removeClass('btn-primary');
            $('.changeperiod').addClass('btn-default').removeClass('btn-primary');
            $(this).addClass('btn-primary').removeClass('btn-default');
            $('#datatable-render').html('<div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>');
            var filterrange = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=000000&moduletypeID=<%= mod.getModuleID()%>&process=datatable&view=" + filterrange,
                success: function (result) {
                    $('#datatable-render').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".changeperiod").change(function (e) {
            $('.tochange').addClass('btn-default').removeClass('btn-primary');
            $('.changeperiod').addClass('btn-primary');
            var newyear = $('#newyear').val();
            var newperiod = $('#newperiod').val();
            $('#datatable-render').html('<div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>');
            e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=000000&moduletypeID=<%= mod.getModuleID()%>&process=datatable&view=byperiod&year=" + newyear + "&period=" + newperiod,
                success: function (result) {
                    $('#datatable-render').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });
        
        $('#add-invoice').click(function (e) {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addfrominvoice",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });

    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <button id="add-new" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> <%= mod.getModuleDesc()%></button>
            <div class="pull-right">
                <button id="filter" class="btn btn-default btn-sm filter"><i class="fa fa-filter" aria-hidden="true"></i> Filter</button>
                <button id="refresh" class="btn btn-default btn-sm refresh"><i class="fa fa-refresh"></i></button>
            </div>
            <br><br> 
            <div id="filterdiv" class="well-filter"> <!--style="display: none"-->
                <div class="row">
                    <div class="col-md-3">
                        <button id="all" class="tochange btn btn-<%=btn_all%> btn-sm">All Payroll</button>
                        <button id="thismonth" class="tochange btn btn-<%=btn_thismonth%> btn-sm">This Month</button>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group" id="div_code">
                            <select class="form-control input-sm changeperiod <%=select_year%>" id="newyear" name="newyear">
                                <%= ParameterDAO.parameterList(log, "Year", year)%>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group" id="div_code">
                            <select class="form-control input-sm changeperiod <%=select_period%>" id="newperiod" name="newperiod">
                                <%= ParameterDAO.parameterList(log, "Period", period)%>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dataTable_wrapper" id="datatable-render"> 
                <div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>
            </div>
        </div>
    </div>
</div>