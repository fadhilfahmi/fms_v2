<%-- 
    Document   : ar_rpt_statement_view
    Created on : Feb 17, 2017, 12:41:17 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArReportStatement"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.gl.GeneralLedger"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Buyer"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.BuyerDAO"%>
<%@page import="com.lcsb.fms.dao.financial.ar.ArReportDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArGLParam"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) ArReportDAO.getModule();
    ArGLParam prm = (ArGLParam) session.getAttribute("gl_posting_param");
    Buyer v = (Buyer) BuyerDAO.getInfo(log,prm.getBuyercode());
%>    
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('.printreport').click(function (e) {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=printreport&origin=ar_rpt_statement_view",
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });
    });
</script>
<div id ="maincontainer">
    <div class="partition">
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID()%>" name="statement"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
                </tr>
            </table>
            <br>
            <table class="table table-bordered table-striped  table-hover">
                <thead>
                    <tr>
                        <th colspan="4">Voucher Information
                            <!--<small><span class="label label-primary">Preparing</span></small>-->
                            <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">

                                <button id="<%//= v.getRefer() %>" class="btn btn-default btn-xs printreport" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i> Print Statement</button>

                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody class="activerowm tdrow">
                    <tr>
                        <th width="25%">Buyer</th>
                        <td width="75%"><%= v.getCode()%> - <%= v.getCompanyname()%></td>
                    </tr>
                    <tr>
                        <th>Range Period</th>
                        <td><%= AccountingPeriod.fullDateMonth(prm.getStartDate()) %> - <%= AccountingPeriod.fullDateMonth(prm.getEndDate()) %></td>
                    </tr>

                </tbody>
            </table>
            <table class="table table-bordered table-striped table-hover" id="list">
                <thead>
                    <tr>
                        <th colspan="8">Contract<!--<small>asdada</small>-->

                        </th>
                    </tr>
                    <tr>
                        <th>Tarikh</th>
                        <th>Source</th>
                        <th>Reference No</th>
                        <th>Debit</th>
                        <th>Credit</th>
                        <th>Balance</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        double totalDebit = 0.0;
                        double totalCredit = 0.0;
                        List<ArReportStatement> listx = (List<ArReportStatement>) ArReportDAO.getAllStatement(log,prm, 0, 0, false);

                        for (ArReportStatement j : listx) {
                            totalDebit += j.getDebit();
                            totalCredit += j.getCredit();

                    %>
                    <tr class="activerowy" id="<%//= j.getId()%>">
                        <td class="tdrow"><%= AccountingPeriod.fullDateMonth(j.getDate()) %></td>
                        <td class="tdrow">&nbsp;<%= j.getType()%></td>
                        <td class="tdrow">&nbsp;<%= j.getRefno()%></td>
                        <td class="tdrow">&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(j.getDebit())%></span></td>
                        <td class="tdrow">&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(j.getCredit())%></span></td>
                        <td class="tdrow" align="right"><%= GeneralTerm.normalCredit(totalDebit - totalCredit)%></td>
                    </tr>
                    <%}%>
                    <tr class="activerowy" id="<%//= j.getId()%>">
                        <th></td>
                        <th></td>
                        <th></td>
                        <th>&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(totalDebit)%></span></td>
                        <th>&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(totalCredit)%></span></td>
                        <th><span class="pull-right"><%= GeneralTerm.normalCredit(totalDebit - totalCredit)%></span></td>
                    </tr>
                </tbody>

            </table>
        </div>
    </div>
</div>