<%-- 
    Document   : gl_post
    Created on : Mar 8, 2016, 10:48:20 AM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.financial.gl.post.ErrorPost"%>
<%@page import="com.lcsb.fms.financial.gl.post.Item"%>
<%@page import="com.lcsb.fms.dao.financial.gl.PostDAO"%>
<%@page import="com.lcsb.fms.financial.gl.post.Master"%>
<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucher"%>
<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucherItem"%>
<%@page import="com.lcsb.fms.dao.financial.gl.JournalVoucherDAO"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
Master post = (Master) PostDAO.getInfo(log,request.getParameter("referno"),request.getParameter("vtype"));

String module_id = ModuleDAO.moduleABB(request.getParameter("vtype"));

boolean doError = false;
List<ErrorPost> checkError = (List<ErrorPost>) post.getListError();
for (ErrorPost f : checkError) {
    if(f.getError()){
        doError = true;
    }
}

String disablePost = "";
if(doError){
    disablePost = "disabled";
}

%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
    $( ".editjv" ).click(function() {
    //var a = $("form").serialize();
        var a = $(this).attr('id');
        var b = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=020104&process=editjvitem&referno="+a,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
        return false;
    });
              
$( "#post" ).click(function() {
    //var a = $("form").serialize();
    var a = $(this).attr('title');

    BootstrapDialog.confirm({
        title: 'Confirmation',
        message: 'Are you sure to post this?',
        type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
        btnOKLabel: 'Post', // <-- Default value is 'OK',
        btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
        callback: function(result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if(result) {
                $.ajax({
                    url: "PathController?moduleid=<%= module_id %>&process=dist&referno="+a,
                    success: function (result) {
                        $('#herey').empty().html(result).hide().fadeIn(300);
                    }
                });
            }
        }
    });
    return false;
});

});
</script>
<div id ="maincontainer">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    	<td width="74%" class="borderbot" align="left"><span class="bigfonttitle">Post Voucher</span>&nbsp;&nbsp;<span class="midfonttitle"></span></td>
		<td width="26%" class="borderbot" align="right">&nbsp;</td>
	  </tr>
</table>
<br>
<div class="partition"> 
	<div class="headofpartition"></div>
       <div class="bodyofpartition">
 			<table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate" width="100%">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= module_id %>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                                        
                    </td>
                    <td valign="middle" align="right">
                        <button id="post" class="btn btn-default btn-sm <%= disablePost %>" title="<%= post.getNovoucher() %>" name="viewlist"><i class="fa fa-check-square-o" aria-hidden="true"></i>&nbsp;&nbsp;Check This Voucher</button>
                                        
                    </td>
                </tr>
          </table>
  <br>
  
  <table class="table table-bordered table-striped">
      <thead>
                                        <tr>
                                            <th colspan="4">
                                                Voucher Information
                                                <!--<small>asdada</small>-->
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>Year</th>
                                            <td><%= post.getYear() %></td>
                                            <th>Company Code</th>
                                            <td><%= post.getEstatecode() %></td>
                                            
                                        </tr>
                                        <tr>
                                            <th>Period</th>
                                            <td><%= post.getPeriod() %></td>
                                            <th>Compane Name</th>
                                            <td><%= post.getEstatename()%></td>
                                        </tr>
                                        <tr>
                                            <th>Voucher No</th>
                                            <td>
                                                <%= post.getNovoucher()%>
                                            </td>
                                           <th>Source</th>
                                            <td>
                                                <%= post.getSource()%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Voucher Date</th>
                                            <td><%= post.getTarikh()%></td>
                                            <th></th>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                      
                        <%
                        String errorClass = "";
                        
                        
                        if(doError){
                            errorClass = "notice-div-red";
                        }else{
                            errorClass = "notice-div-green";
                        }
                        %>
                        
                                    
                            <div class="<%= errorClass %>">
                                <%
                                  
                        String errorIcon = "";
                        List<ErrorPost> listError = (List<ErrorPost>) post.getListError();
                        for (ErrorPost e : listError) {
                            if(e.getError()){
                                errorIcon = "<i class=\"fa fa-exclamation\"></i>";
                            }else{
                                errorIcon = "<i class=\"fa fa-check right-green\"></i>";
                            }
        %>
                                <%= errorIcon %> - <%= e.getErrorID() %> - <%= e.getErrorDesc()%><br>
                                
                                <%}%>
                            </div>
                                
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Bil</th>
                                            <th>
                                                Account<small></small>
                                            </th>
                                            <th>
                                                Sub Account<small></small>
                                            </th>
                                            <th>
                                                Location<small></small>
                                            </th>
                                            <th>
                                                Remark<small></small>
                                            </th>
                                            <th>Debit</th>
                                            <th>Credit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
<%
    int i = 0;

List<Item> listAll = (List<Item>) post.getListItem();
    for (Item j : listAll) {
        i++;
%>
                                        <tr>
                                            <th><%= i %></th>
                                            <td><%= j.getCoacode() %> - <%= j.getCoadesc() %></td>
                                            <td><%= j.getSacode() %> - <%= j.getSadesc() %></td>
                                            <td><%= j.getLoclevel() %> - <%= j.getLoccode() %> - <%= j.getLocdesc() %></td>
                                            <td><%= j.getRemark() %></td>
                                            <td><span class="pull-right"><%= GeneralTerm.currencyFormat(j.getDebit()) %></span></td>
                                            <td><span class="pull-right"><%= GeneralTerm.currencyFormat(j.getCredit()) %></span></td>
                                        </tr>
                                        <%
                                            }
  %>
                                        <tr>
                                            <th colspan="5">Total</th>
                                            <th><span class="pull-right"><%= GeneralTerm.currencyFormat(Double.parseDouble(post.getDebit())) %></span></th>
                                            <th><span class="pull-right"><%= GeneralTerm.currencyFormat(Double.parseDouble(post.getCredit())) %></span></th>
                                        </tr>
                                        
                                    </tbody>
                                </table>
		</div>
	</div>
</div>
      

      