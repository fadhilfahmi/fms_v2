<%-- 
    Document   : ac_lot_activity_step
    Created on : Dec 19, 2016, 10:27:37 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.management.activity.LotActivityDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) LotActivityDAO.getModule();
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>

<link href="bootstrap-form-wizard/form-wizard.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });
        //$('#JVdate').datepicker({ dateFormat: 'yyyy-mm-dd' });

        $('#JVdate').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        });

        $('#saveall').click(function (e) {
            //e.preventDefault();
            var sessionid = $('#sessionid').val();
            $.ajax({
                url: "PathController?moduleid=020903&process=saveinvoice&sessionid=" + sessionid,
                success: function (result) {
                    $('#maincontainer').remove();
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $('#getaccount').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Chart of Account',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_coa.jsp');
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                    });

                    return $content;
                }
            });

            return false;
        });



        $('#getbuyer').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Buyer Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_buyer_mill.jsp?name=bname&code=bcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getproduct').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Product Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_product.jsp?name=prodname&code=prodcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn');

        allWells.hide();
        var ch = 0;
        navListItems.click(function (e) {
            ch++;
            e.preventDefault();
            var $target = $($(this).attr('href')),
                    $item = $(this);
            //console.log($target);
            //console.log($item);
            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url']"),
                    isValid = true;

            if (curStepBtn == 'step-1') {
                //alert(2);

                var lotcode = $('#lotcode').val();
                var lotdesc = $('#lotdesc').val();

                var path = '';


                path = 'ac_lot_activity_members.jsp';
                //$('#step-2-title').html('Choose Contract');
                $('#step-2-string').html('List of  <strong>' + lotdesc + '</strong>\'s Members');
                //$('#step-3-title').html('Complete Refinery');
                //$('#step-3-string').html('Complete Refinery');


                $.ajax({
                    url: path + "?lotcode=" + lotcode,
                    success: function (result) {
                        $('#table-result-step2').empty().html(result).hide().fadeIn(300);
                        window.scrollTo(0, 0);

                    }
                });


            }

            if (curStepBtn == 'step-2') {
                var lotcode = $('#lotcode').val();
                var lotdesc = $('#lotdesc').val();
                var year = $('#year').val();
                var period = $('#period').val();

                var str = '&memcode=';
                var i = 0;
                $.each($("input[name='checkboxmemno']:checked"), function () {
                    i++;
                    str = str + $(this).val() + '&memcode=';

                });

                if (i > 0) {
                    var a = $("#savelot :input").serialize() + str;
                    var b = $("#savelot :input").serialize();
                    $.ajax({
                        data: a,
                        type: 'POST',
                        url: "ProcessController?moduleid=030103&process=tempmember",
                        success: function (result) {
                            if (result == 1) {
                                $.ajax({
                                    async: true,
                                    url: "ac_lot_calculate_profit.jsp?" + b,
                                    success: function (result) {
                                        $('#table-result-calculate').empty().html(result).hide().fadeIn(300);
                                        window.scrollTo(0, 0);

                                    }
                                });

                            }
                            //$('#maincontainer').empty().html(result).hide().fadeIn(300);
                        }
                    });

                    //$('#step-2-title').html('Choose Contract');
                    $('#step-3-string').html('<strong>' + lotdesc + '</strong> | Year : <strong>' + year + '</strong> | Period : <strong>' + period + '</strong>');
                    //$('#step-3-title').html('Complete Refinery');
                    //$('#step-3-string').html('Complete Refinery');

                }


            }

            if (curStepBtn == 'step-3') {

                var sessionid = $('#sessionid').val();

                var a = $("#savecalculation :input").serialize();
                var b = $("#savelot :input").serialize();
                $.ajax({
                    data: a + '&' + b,
                    type: 'POST',
                    url: "ProcessController?moduleid=030103&process=savecalculation&sessionid=" + sessionid,
                    success: function (result) {
                        //$('#maincontainer').empty().html(result).hide().fadeIn(300);
                    }
                });

                $.ajax({
                    data: a,
                    type: 'POST',
                    async: true,
                    url: "ac_lot_activity_distmember.jsp?sessionid=" + sessionid,
                    success: function (result) {
                        console.log('table-load');
                        $('#table-list-memberprofit').empty().html(result).hide().fadeIn(300);
                        window.scrollTo(0, 0);

                    }


                });

            }

            if (curStepBtn == 'step-4') {
                var sessionid = $('#sessionid').val();

                var a = $("#savemaster :input").serialize();
                $.ajax({
                    data: a,
                    async: false,
                    type: 'POST',
                    url: "ProcessController?moduleid=020903&process=tempmaster&sessionid=" + sessionid,
                    success: function (result) {
                        //$('#maincontainer').empty().html(result).hide().fadeIn(300);
                    }
                });

                path = "ac_lot_activity_finalize.jsp?sessionid=" + sessionid;


                $.ajax({
                    async: true,
                    url: path,
                    success: function (result) {
                        console.log('table-load');
                        $('#finalize').empty().html(result).hide().fadeIn(300);

                    }


                });


            }

            if (curStepBtn == 'step-5') {
                alert('Done');
            }


            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.btn-primary').trigger('click');

        $('body').on('click', '.activerowy', function (e) {
            $('.refineryBtn').trigger('click');
            return false;
        });

        $('.getlot').click(function (e) {

            var a = 'lotcode';
            var b = 'lotdesc';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Estate',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_lot.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });


        $('.currency-format').keyup(function (e) {
            console.log(2323);
            var a = $(this).val();
            a = a.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

            $(this).val(a);

            return false;
        });




    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>

                    </td>
                </tr>
            </table>
            <br>

            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                        <p>Lot & Period</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                        <p id="step-2-title">Check Members</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                        <p id="step-3-title">Calculate Profit</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                        <p>Distribution List</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                        <p>Finalizing</p>
                    </div>
                </div>
            </div>




            <form data-toggle="validator" role="form" id="savelot">
                <input type="hidden" id="millcode" value="<%= log.getEstateCode()%>">
                <input type="hidden" id="sessionid" name="sessionid" value="<%= request.getParameter("sessionid")%>">


                <div class="row setup-content" id="step-1">
                    <div class="col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Choose Lot & Period Information
                            </div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group form-group-sm">
                                                    <div class="form-group" id="div_code">
                                                        <label for="inputName" class="control-label">Lot Code & Name&nbsp&nbsp<span class="res_code"></span></label>
                                                        <div class="form-group-sm input-group">
                                                            <input type="text" class="form-control input-sm" id="lotcode" name="lotcode" required>
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default btn-sm getlot" type="button" id="lotcode" id1="lotdesc"><i class="fa fa-cog"></i> Lot</button>
                                                            </span>
                                                        </div>  
                                                    </div>
                                                    <div class="form-group" id="div_code">
                                                        <input type="text" class="form-control input-sm" id="lotdesc" name="lotdesc" placeholder="" autocomplete="off" required> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">            
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="inputName" class="control-label">Year</label>
                                                    <select class="form-control input-sm" id="year" name="year">
                                                        <%= ParameterDAO.parameterList("Year", AccountingPeriod.getCurYear())%>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="inputName" class="control-label">Period</label>
                                                    <select class="form-control input-sm" id="period" name="period">
                                                        <%= ParameterDAO.parameterList("Period", AccountingPeriod.getCurPeriod())%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">            
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="inputName" class="control-label">Quarter</label>
                                                    <select class="form-control input-sm" id="quarter" name="quarter">
                                                        <%= ParameterDAO.parameterList("Quarter", "")%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <button class="btn btn-default nextBtn btn-sm pull-right" type="button" >Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row setup-content" id="step-2">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span id="step-2-string"></span>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div id="table-result-step2">
                                    <div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>
                                </div>
                                <button class="btn btn-default nextBtn step2-Btn btn-sm pull-right" type="button" >Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-3">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span id="step-3-string"></span>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div id="table-result-calculate">
                                    <div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>
                                </div>
                                <button class="btn btn-default nextBtn btn-sm pull-right" type="button" >Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-4">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Profit Distribution
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div id="table-list-memberprofit">
                                    <div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>

                                </div>
                                <button class="btn btn-default nextBtn btn-sm pull-right" type="button" >Next</button>
                            </div>
                        </div></div></div>
            </div>
            <div class="row setup-content" id="step-5">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Finalizing
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div id="finalize">
                                    <div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>
                                </div>
                                <button class="btn btn-success btn-sm pull-right" id="saveall" type="submit">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
