<%-- 
    Document   : ar_inv_add_ffb_param
    Created on : Nov 12, 2016, 11:26:32 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('.refineryBtn').prop('disabled', true);

        $('.form-control').keyup(function (e) {
            if (($('#ffb').val() == 0.00) || ($('#mpobpricecpo').val() == 0.00) || ($('#mpobpricepk').val() == 0.00) || ($('#transport').val() == 0.00) || ($('#millcost').val() == 0.00) || ($('#cess').val() == 0.00) || ($('#oer').val() == 0.00) || ($('#ker').val() == 0.00)) {
                $('.refineryBtn').prop('disabled', true);
            } else {
                console.log('ok');
                $('.refineryBtn').prop('disabled', false);
            }

            if (($('#oer').val() > 100) || ($('#ker').val() > 100)) {

                $(this).removeClass('has-success');
                $(this).addClass('has-error');
                $('#res_code').append("<i class=\"fa fa-exclamation-circle fail-red\"></i>&nbsp;&nbsp;Code not available");
            }
        });

    });
</script>
<form data-toggle="validator" role="form" id="saveffb">
    <div class="well">
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="inputName" class="control-label">Year</label>
                    <input type="text" class="form-control input-sm dateformat" id="year" name="year" value="<%= AccountingPeriod.getCurYear(log)%>">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group form-group-sm">
                    <label for="inputName" class="control-label">Month</label>
                    <select class="form-control input-sm" id="month" name="month">
                        <%= ParameterDAO.parameterList(log,"Month", "")%>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="well">
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="inputName" class="control-label">FFB Supplied (Mt)</label>
                    <input type="text" class="form-control input-sm dateformat" id="ffb" name="ffb" placeholder="">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="inputName" class="control-label">MPOB Price of Palm Oil (RM)</label>
                    <input type="text" class="form-control input-sm dateformat" id="mpobpricecpo" name="mpobpricecpo" placeholder="">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="inputName" class="control-label">MPOB Price of Palm Kernel (RM)</label>
                    <input type="text" class="form-control input-sm dateformat" id="mpobpricepk" name="mpobpricepk" placeholder="">
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <label for="inputName" class="control-label">Transport of Palm Oil (RM)</label>
                    <input type="text" class="form-control input-sm dateformat" id="transport" name="transport" value="52.00" placeholder="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="inputName" class="control-label">Mill Processing Cost (RM)</label>
                    <input type="text" class="form-control input-sm dateformat" id="millcost" name="millcost" value="55.00" placeholder="">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="inputName" class="control-label">OER %</label><span class="oer-res"></span> 
                    <input type="text" class="form-control input-sm dateformat" id="oer" name="oer" placeholder="">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="inputName" class="control-label">KER %</label><span class="ker-res"></span> 
                    <input type="text" class="form-control input-sm dateformat" id="ker" name="ker" placeholder="">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="inputName" class="control-label">CESS MPOB (RM)</label>
                    <input type="text" class="form-control input-sm dateformat" id="cess" name="cess" value="13.00" placeholder="">
                </div>
            </div>
        </div>
    </div>

    <div class="well">
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="inputName" class="control-label">Discounted Price for High FFA</label>
                    <input type="text" class="form-control input-sm dateformat" id="discount" name="discount" value="0.00">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="inputName" class="control-label">Winfall Profit Levy</label>
                    <input type="text" class="form-control input-sm dateformat" id="winfall" name="winfall" value="0.00">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="inputName" class="control-label">External FFB Transport (RM)</label>
                    <input type="text" class="form-control input-sm dateformat" id="externalffb" name="externalffb" placeholder="">
                </div>
            </div>
        </div>
    </div>
</form>