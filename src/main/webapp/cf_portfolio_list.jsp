<%-- 
    Document   : cf_portfolio_list
    Created on : Dec 15, 2016, 10:42:50 AM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.PortfolioDAO"%>
<%@page import="com.lcsb.fms.util.model.ListTable"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.ModuleItem"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.ConnectionModel"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,java.util.*" errorPage="" isThreadSafe="false" %>
<%@ page import="java.util.Date"%>
<%
    Module mod = (Module) PortfolioDAO.getModule();

    response.setHeader("Cache-Control", "no-cache");
%>

<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/tether.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>

<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        var updateTable = '<%= mod.getMainTable()%>';

    <%
        List<ListTable> mlist = (List<ListTable>) PortfolioDAO.tableList();
        for (ListTable m : mlist) {
                        //if(m.getList_View().equals    ("1")){
%>
        columntoView.push('<%= m.getColumnName()%>');
        columnName.push('<%= m.getTitleName()%>');
    <%
            //}

        }
    %>

        var colsTosend = 'cols=';
        for (m = 0; m < columntoView.length; ++m) {//put value from database to variable jget
            if (m == columntoView.length - 1) {
                colsTosend += columntoView[m];
            } else {
                colsTosend += columntoView[m] + '&cols=';
            }
        }


        $("#add-new").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addnew",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });

        $("#viewall").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewall",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        var orderby = 'order by voucherid desc';
        var oTable = $('#example').DataTable({
            destroy:true,
            "aaSorting": [[1, "asc"]],
            "responsive": true,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "retrievetable.jsp?table=" + updateTable + "&code=<%= mod.getReferID_Master()%>&" + colsTosend + "&order=" + orderby,
            "aoColumns": [
                {"sTitle": columnName[0], "mData": columntoView[0], "sWidth": "15%", "bSearchable": true},
                {"sTitle": columnName[1], "mData": columntoView[1], "sWidth": "27%"},
                {"sTitle": columnName[2], "mData": columntoView[2], "sWidth": "15%"},
                {"sTitle": columnName[3], "mData": columntoView[3], "sWidth": "10%"},
                {"sTitle": "Action", "mData": null, "bSortable": false, "bSearchable": false, "sWidth": "13%"}

            ],
            "aoColumnDefs": [{
                    "aTargets": [4],
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {



                        var a = $('<%= Button.viewlistButton()%>');
                        var b = $('<%= Button.editlistButton()%>');
                        var c = $('<%= Button.deletelistButton()%>');


                        a.on('click', function () {
                            BootstrapDialog.show({
                                type: BootstrapDialog.TYPE_DEFAULT,
                                size: BootstrapDialog.SIZE_WIDE,
                                //animate: false,
                                title: 'View Share Portfolio Detail',
                                message: function (dialog) {
                                    var $content = $('<body></body>').load('cf_portfolio_view_each.jsp?refer=' + oData.code);
                                    //$('body').on('click', '.thisresult_nd', function(event){
                                    //    dialog.close();
                                    //    event.preventDefault();
                                    //});
                                    return $content;
                                },
                                buttons: [{
                                        label: 'Close',
                                        action: function (dialogRef) {
                                            dialogRef.close();
                                        }
                                    }]
                            });
                            return false;
                        });
                        b.on('click', function () {

                            $.ajax({
                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=edit&refer=" + oData.code,
                                success: function (result) {
                                    // $("#haha").html(result);
                                    $('#herey').empty().html(result).hide().fadeIn(300);
                                }});

                            return false;
                        });
                        c.on('click', function () {
                            //console.log(oData);
                            //deleteRow(oData.JVrefno);

                            BootstrapDialog.confirm({
                                title: 'Confirmation',
                                message: 'Are you sure to delete?',
                                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                closable: true, // <-- Default value is false
                                draggable: true, // <-- Default value is false
                                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                                callback: function (result) {
                                    // result will be true if button was click, while it will be false if users close the dialog directly.
                                    if (result) {
                                        $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&refer=" + oData.code,
                                            success: function (result) {
                                                // $("#haha").html(result);
                                                setTimeout(function () {
                                                    oTable.draw();
                                                }, 500);
                                            }});
                                    }
                                }
                            });
                            return false;
                        });






                        $(nTd).empty();
                        $(nTd).attr("class", 'btn-group');
                        $(nTd).attr("align", 'right');
                        $(nTd).prepend(a, b, c);
                    }

                }]



        });

        $('#example tbody').on('click', 'tr', function (e) {
            var data = oTable.row(this).data();
            e.preventDefault();

            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                size: BootstrapDialog.SIZE_WIDE,
                //animate: false,
                title: 'View Share Portfolio Detail',
                message: function (dialog) {
                    var $content = $('<body></body>').load('cf_portfolio_view_each.jsp?refer=' + data.code);
                    //$('body').on('click', '.thisresult_nd', function(event){
                    //    dialog.close();
                    //    event.preventDefault();
                    //});
                    return $content;
                },
                buttons: [{
                        label: 'Close',
                        action: function (dialogRef) {
                            dialogRef.close();
                        }
                    }]
            });
        });



    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="add-new" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> Add <%= mod.getModuleDesc()%></button>
                        <button id="viewall" class="btn btn-default btn-sm refresh"><i class="fa fa-refresh"></i> View All</button>
                        <button id="refresh" class="btn btn-default btn-sm refresh"><i class="fa fa-refresh"></i> Refresh</button>

                        <!--<button id="add-debitnote" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> From Sales Debit Note</button>-->
                    </td>
                </tr>
            </table>
            <br>       
            <div class="dataTable_wrapper"> 
                <table class="table table-striped table-bordered table-hover" id="example">
                </table>
            </div>

        </div>
    </div>
</div>

