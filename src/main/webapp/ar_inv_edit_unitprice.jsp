<%-- 
    Document   : ar_inv_edit_unitprice
    Created on : Aug 10, 2017, 10:59:38 AM
    Author     : fadhilfahmi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group" id="div_code">
            <label for="inputName" class="control-label">Current Price&nbsp&nbsp<span class="res_code"></span></label>
            <input type="text" class="form-control input-sm" id="paidname" name="paidname" value="<%= request.getParameter("curprice") %>" autocomplete="off" readonly > 
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group" id="div_code">
            <label for="inputName" class="control-label">New Price&nbsp&nbsp<span class="res_code"></span></label>
            <input type="text" class="form-control input-sm" id="newprice" name="newprice" value="0.00" onClick="this.select();" autocomplete="off" > 
        </div>
    </div>
</div>