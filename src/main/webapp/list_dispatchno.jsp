<%-- 
    Document   : list_dispatchno
    Created on : Nov 10, 2016, 2:31:43 PM
    Author     : fadhilfahmi
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
            
           
            
           $('#searchnow').click(function(){
               $('#searchstatus').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');
                //var l = $('input[name=searchby]:checked').val();
                var keyword = $('#keyword-disno').val();
                
                 $.ajax({
                        url: "list_dispatchno_view.jsp?table=<%= request.getParameter("table") %>&keyword="+keyword,
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                        $('#searchstatus').html('<i class="fa fa-search"></i>');
                
                }});
            });
            
            
            
            $('#result').on('click', '.thisresult', function(e) {
                var b = $(this).attr('id');
                var d = $(this).attr('id1');
                var e = $(this).attr('id2');
                var f = $(this).attr('id3');
                var g = $(this).attr('id4');
                
                var $newrow = '<tr id="'+b+'" class="activerowy1">'
                        +'<td width="3%" class="tdrow-contract" id="'+b+'"><label>'
                        +'<input type="checkbox" id="chkbox'+b+'" name="checkboxdisno" class="chcbox-contract" value="'+b+'" style="opacity:0;" required></label>'
                        +'</td>'
                        +'<td class="tdrow-contract" id="'+b+'"><input type="hidden" id="date'+b+'" name="date'+b+'" value="'+d+'">'+d+'</td>'
                        +'<td class="tdrow-contract" id="'+b+'"><input type="hidden" id="dispatchno'+b+'" name="dispatchno'+b+'" value="'+b+'">'+b+'</td>'
                        +'<td class="tdrow-contract" id="'+b+'"><input type="hidden" id="trailer'+b+'" name="trailer'+b+'" value="'+e+'">'+e+'</td>'
                        +'<td class="tdrow"><input type="text" class="form-control input-sm" id="mt_mill'+b+'" name="mt_mill'+b+'" value="'+f+'" required disabled></td>'
                        +'<td class="tdrow"><input type="text" class="form-control input-sm mt_refinery_gen" id="mt_refinery'+b+'" name="mt_refinery'+b+'" value="'+g+'" required disabled></td>'
                    +'</tr>';
                
                $('#refinery-body').prepend($newrow);
                
                //e.preventDefault();
                return false;
            });
            
           
            
        });
        </script>
    </head>
    <body>
         
        
        <div class="form-group">
            <div class="form-group input-group">
                <input type="text" class="form-control" id="keyword-disno">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" id="searchnow"><span id="searchstatus"><i class="fa fa-search"></i></span>
                    </button>
                </span>
            </div>
        </div>
        
        <div id="result">
       
        </div>
        
            
    </body>
</html>
