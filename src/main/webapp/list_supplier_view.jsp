<%-- 
    Document   : list_supplier_view
    Created on : May 13, 2016, 1:02:13 PM
    Author     : user
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.Supplier"%>
<%@page import="com.lcsb.fms.util.dao.SupplierDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<div class="list-group"><%
            
            String keyword = request.getParameter("keyword");
            List<Supplier> slist = (List<Supplier>) SupplierDAO.getSupplier(log, keyword);
            for (Supplier c : slist) { 
               %>
       
            <a href="<%= c.getSupplierName()%>" id="<%= c.getSupplierCode()%>" id1="<%= c.getSupplierAddress() %>" id2="<%= c.getSupplierCity() %>" id3="<%= c.getSupplierPostcode() %>" id4="<%= c.getSupplierState() %>" id5="<%= c.getSupplierType() %>" id6="<%= c.getGstid()%>"  title="" class="list-group-item thisresult">
                <div id="left_div"><%= c.getSupplierCode()%></div>
                <div id="right_div"><%= c.getSupplierName()%></div>
            </a>

               <%    
            } 
        %>
</div>