<%-- 
    Document   : list_bank_view
    Created on : Mar 4, 2016, 4:06:59 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ProductDAO"%>
<%@page import="com.lcsb.fms.util.model.Product"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<div class="list-group"><%
            
            String keyword = request.getParameter("keyword");
            List<Product> slist = (List<Product>) ProductDAO.getInfo(log, keyword);
            for (Product c : slist) { 
               %>
       
            <a href="<%= c.getDescp()%>" id="<%= c.getCode()%>" id1="<%= c.getCoa() %>" id2="<%= c.getCoadescp() %>" title="" class="list-group-item thisresult">
                <div id="left_div"><%= c.getCode()%></div>
                <div id="right_div"><%= c.getDescp()%></div>
            </a>

               <%    
            } 
        %>
</div>