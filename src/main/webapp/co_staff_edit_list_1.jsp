<%-- 
    Document   : co_staff_edit_list
    Created on : Dec 14, 2016, 10:31:35 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.setup.company.staff.StaffDesignation"%>
<%@page import="com.lcsb.fms.model.setup.company.staff.Staff"%>
<%@page import="com.lcsb.fms.dao.setup.company.StaffDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
//PaymentVoucherItem vi = (PaymentVoucherItem) VendorPaymentDAO.getPYVitem(request.getParameter("refer"));
    Staff v = (Staff) StaffDAO.getInfo(log, request.getParameter("referno"));
    Module mod = (Module) StaffDAO.getModule();

//String status = PaymentVoucherDAO.getStatus(v.getRefer());
%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/tether.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>

<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        window.scrollTo(0, 0);

        callPnotify('<%= request.getParameter("status")%>', '<%= request.getParameter("text")%>');


        $('#print').click(function (e) {
            printElement($("#viewjv").html());
        });
        var oTable = $('#list-bank').DataTable({
            responsive: true,
            "aaSorting": [[0, "desc"]]
        });

        $('.viewvoucher').click(function (e) {
            var a = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewpv&refer=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".editmain").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editmain&referno=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".edititem").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('name');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + b + "&refer=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".deleteitem").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('name');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are you sure to delete?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=" + b + "&referno=" + a,
                            success: function (result) {
                                oTable.row('#' + a).remove().draw(false);
                            }
                        });
                    }
                }
            });

            return false;
        });

        $(".deletemain").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('href');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'All datas for this Official Receipt will be permanently deleted. Are you sure to proceed?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&referno=" + a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            return false;
        });

        $("#addcredit").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addcredit&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $(".add-st").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addlot&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });





        $('.editbasis').click(function (e) {
            var b = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editbasis&referno=" + b,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });

        $('#tab_content1').on('click', '.adddesignation', function (e) {
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=adddesignation&refer=" + id,
                success: function (result) {
                    $('#tab_content1').empty().html(result).hide().fadeIn(100);
                }
            });
            return false;
        });
        
        $.ajax({
        async:true,
            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewdesignation&refer=<%= v.getStaffid()%>",
            success: function (result) {
                $('#tab_content2').empty().html(result).hide().fadeIn(300);
                $('#tab_content1').empty().html(result).hide().fadeIn(300);
            }
        });
        
       

        $(".viewdesignation").click(function (e) {
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewdesignation&refer=" + id,
                success: function (result) {
                    $('#tab_content1').empty().html(result).hide().fadeIn(100);
                }
            });
            return false;
        });

        $('#tab_content1').on('click', '#backtolist', function (e) {
            var path = $(this).attr('title');
            var process = $(this).attr('name');
            var refer = $(this).attr('type');
            $.ajax({
                url: "PathController?moduleid=" + path + "&process=" + process + "&refer=" + refer,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#tab_content1').empty().html(result).hide().fadeIn(100);
                }
            });
            return false;
        });



    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
                </tr>
            </table>
            <br>


            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Staff Information <small>Detail about Staff</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                                <div class="profile_img">
                                    <div id="crop-avatar">
                                        <!-- Current avatar 
                                        <img class="img-responsive avatar-view" src="images/picture.jpg" alt="Avatar" title="Change the avatar">-->
                                    </div>
                                </div>
                                <h3><%= v.getStaffid()%></h3>
                                <div class="row">
                                    <div class="col-md-10">
                                        <h2 style="float:left"><%= v.getName()%></h2>
                                    </div>
                                    <div class="col-md-2">
                                        <a id="<%= v.getStaffid()%>" class="editbasis" title="Edit Basic Information">
                                            <i style="cursor:pointer" class="fa fa-wrench" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="list-group">
                                    <a class="list-group-item" >
                                        <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;IC : <%= v.getIc()%>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;<%= v.getBirth()%>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;<%= v.getSex()%>
                                    </a>
                                    <a href="balancesheet" class="list-group-item goto">
                                        <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;<%= v.getStatus()%>
                                    </a>
                                    <a href="gllisting" class="list-group-item goto">
                                        <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;<%= v.getDatejoin()%>
                                    </a>
                                    <a href="trialbalance" class="list-group-item goto">
                                        <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;<%= v.getAddress()%>
                                    </a>
                                    <a href="gllisting" class="list-group-item goto">
                                        <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;<%= v.getMarital()%>
                                    </a>
                                    <a href="trialbalance" class="list-group-item goto">
                                        <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;<%= v.getHp()%>
                                    </a>
                                </div>

                                <!--<a class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a>-->
                                <br />

                                <!-- start skills -->
                                <h4>Skills</h4>
                                <ul class="list-unstyled user_data">
                                    <li>
                                        <p>Web Applications</p>
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <p>Website Design</p>
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="70"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <p>Automation & Testing</p>
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="30"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <p>UI / UX</p>
                                        <div class="progress progress_sm">
                                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                                        </div>
                                    </li>
                                </ul>
                                <!-- end of skills -->

                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-12">

                                <div class="profile_title">
                                    <div class="col-md-6">
                                        <h2>Additional Information</h2>
                                    </div>
                                </div>
                                <!-- start of user-activity-graph -->
                                <div id="graph_bar" style="width:100%; height:60px;"></div>
                                <!-- end of user-activity-graph -->

                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Designation</a>
                                        </li>
                                        <li role="presentation" class=""><a href="#tab_content2" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Spouse</a>
                                        </li>
                                        <li role="presentation" class=""><a href="#tab_content3" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Children</a>
                                        </li>
                                        <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Bank</a>
                                        </li>
                                        <li role="presentation" class=""><a href="#tab_content5" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Deduction</a>
                                        </li>
                                    </ul>
                                    <div id="myTabContent" class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                                            <!-- start tab content1 -->

                                            <!-- end tab content1 -->

                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                                            <!-- start user projects -->

                                            <!-- end user projects -->

                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                            <p>xxFood truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui
                                                photo booth letterpress, commodo enim craft beer mlkshk </p>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">

                                            <!-- start user projects -->

                                            <!-- end user projects -->

                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="profile-tab">

                                            <!-- start user projects -->

                                            <!-- end user projects -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>









