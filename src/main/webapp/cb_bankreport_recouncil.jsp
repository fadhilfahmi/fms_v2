<%-- 
    Document   : cb_bankreport_recouncil
    Created on : Mar 23, 2017, 4:52:16 PM
    Author     : fadhilfahmi
--%>


<%@page import="com.lcsb.fms.model.financial.cashbook.ItemDetail"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.ItemMain"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.BankReconcile"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.BankReconcileReportMaster"%>
<%@page import="com.lcsb.fms.util.ext.ParseSafely"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.BankReportParam"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.BankReportDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.EstateDAO"%>
<%@page import="com.lcsb.fms.util.model.Estate"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) BankReportDAO.getModule();
    Estate est = EstateDAO.getEstateInfo(log, log.getEstateCode(), "estatecode");
    BankReportParam prm = (BankReportParam) session.getAttribute("br_param");
    BankReconcileReportMaster master = (BankReconcileReportMaster) BankReportDAO.getReconcileMaster(log, prm);
    BankReconcile br = (BankReconcile) master.getBankReconcile();
    
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        
        $('#print').click(function (e) {
            printReport($("#viewprint").html());
        });

        $("#statementbal").keyup(function () {
            var tot = 0;
            var a = parseFloat($(this).val());
            var b = $('#total-outstanding-issued-cheque').val();
            var c = $('#total-outstanding-received-cheque').val();

            tot = parseFloat(a) - parseFloat(b) + parseFloat(c);

            $('#totalg-outstanding-issued-cheque').val(parseFloat(a) - parseFloat(b));
            $('#totalg-outstanding-received-cheque').val(parseFloat(a) - parseFloat(b) + parseFloat(c));
            $('#bankbookbal').val(tot);
            return false;
        });

        $("#statementbal").change(function () {
            var a = parseFloat($(this).val());
            $(this).val(a.toFixed(2));

            var b = parseFloat($('#bankbookbal').val());
            $('#bankbookbal').val(b.toFixed(2));

            var c = parseFloat($('#totalg-outstanding-issued-cheque').val());
            $('#totalg-outstanding-issued-cheque').val(c.toFixed(2));

            var d = parseFloat($('#totalg-outstanding-received-cheque').val());
            $('#totalg-outstanding-received-cheque').val(d.toFixed(2));


            return false;
        });



    });
</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewbankreport" type="<%= request.getParameter("refer")%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        
                        <%if(!master.isDoReconcile()){%>
                        
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="savereconcile"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                        <%}else{%>
                        <button id="print"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addinv" ><i class="fa fa-floppy-o"></i>&nbsp;Print</button>
                        <%}%>
                        
                    </td>
                </tr>
            </table>
            <br>
            <div id="viewprint">
                <form data-toggle="validator" role="form" id="saveform">
                    <input type="hidden" name="estatecode" value="<%= log.getEstateCode() %>">
                    <input type="hidden" name="estatename" value="<%= log.getEstateDescp()%>">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td colspan="4">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="50px"><img src="<%= est.getLogopath()%>" width="40px" style="vertical-align:middle" ></td>
                                        <td><span class="bigtitlebold"><%= log.getEstateDescp()%></span><span class="pull-right"></span>
                                            <p class="font_inheader_normal">BANK RECONCILATION REPORT<%//= tb.getTbtitle()%></p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="border_bottom"></td>
                        </tr>
                    </table>
                    <br>
                    <table  class="table-striped table-hover" width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr> 
                            <td colspan="3">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr> 
                                        <td width="18%"><strong><font size="1" face="Arial, Helvetica, sans-serif">Bank 
                                                Name:</font></strong></td>
                                        <td width="49%"><strong><font size="1" face="Arial, Helvetica, sans-serif"><%= prm.getBankname()%> 
                                                <input type="hidden" name="codebank" value="<%= prm.getBankcode()%>">
                                                <input type="hidden" name="bname" value="<%= prm.getBankname()%>">
                                                <input type="hidden" name="depan" value="<%//=depan%>">
                                                </font></strong></td>
                                        <td width="7%"><strong><font size="1" face="Arial, Helvetica, sans-serif">Year:</font></strong></td>
                                        <td width="10%"><strong><font size="1" face="Arial, Helvetica, sans-serif"><%= prm.getYear()%> 
                                                <input type="hidden" name="year" value="<%= prm.getYear()%>">
                                                </font></strong></td>
                                        <td width="11%"><strong><font size="1" face="Arial, Helvetica, sans-serif">Period:</font></strong></td>
                                        <td width="5%"><strong><font size="1" face="Arial, Helvetica, sans-serif"><%= prm.getPeriod()%> 
                                                <input type="hidden" name="period" value="<%= prm.getPeriod()%>">
                                                </font></strong></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr> 
                            <td width="72%"><font size="1" face="Arial, Helvetica, sans-serif">Balance 
                                in Bank Statement as at <%= AccountingPeriod.fullDateMonth(AccountingPeriod.getEndofPeriod(log, prm.getYear(), prm.getPeriod()))%></font></td>
                            <td width="12%"><div align="right"> 
                                    <p><font size="2" face="Arial, Helvetica, sans-serif"><font face="Arial, Helvetica, sans-serif"><font size="1"></font></font> 
                                        </font></p>
                                </div></td>
                            <td width="16%"><div class="pull-right"><font size="1" face="Arial, Helvetica, sans-serif">RM 
                                    
                                    <%if(!master.isDoReconcile()){%>
                                    <input name="statementbal" id="statementbal" type="text" size="12" value="<%= br.getA() %>">
                                    <%}else{%>
                                    <%= GeneralTerm.currencyFormat(br.getA()) %>
                                    <%}%>
                                    </font></div></td>
                        </tr>
                        <tr> 
                            <td><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
                            <td colspan="2"><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
                        </tr>
                        <tr> 
                            <td>&nbsp;</td>
                            <td colspan="2"><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
                        </tr>

                    </table>


                    <%
                        List<ItemMain> listAll = master.getListItemMain();
                        for (ItemMain j : listAll) {
                    %>
                    <table  class="table-striped table-hover" width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr> 
                            <td colspan="6"><font size="1" face="Arial, Helvetica, sans-serif"><%= j.getTitle()%> :</font></td>
                        </tr>
                        <tr> 
                            <td width="7%"><div><font size="1"><strong><font face="Arial, Helvetica, sans-serif">No.</font></strong></font></div></td>
                            <td width="12%"><div><font size="1"><strong><font face="Arial, Helvetica, sans-serif">Date</font></strong></font></div></td>
                            <td width="39%"><div><font size="1"><strong><font face="Arial, Helvetica, sans-serif">Paid 
                                        by/Paid to</font></strong></font></div></td>
                            <td width="14%"><div><font size="1"><strong><font face="Arial, Helvetica, sans-serif">Cheque 
                                        No.</font></strong></font></div></td>
                            <td width="12%"><div align="right"><font size="1"><strong><font face="Arial, Helvetica, sans-serif">Amount(RM)</font></strong></font></div></td>
                            <td width="16%">&nbsp;</td>
                        </tr>
                        <%
                            List<ItemDetail> listDetail = j.getListItemDetail();
                            if (listDetail.isEmpty()) {%>
                        <tr>
                            <td colspan="6">
                                <span class="font-small-red"><font size="1" face="Arial, Helvetica, sans-serif">No data available.</font></span>
                            </td>
                        </tr>

                        <%}
                            int bil = 1;
                            for (ItemDetail i : listDetail) {

                        %>

                        <tr> 
                            <td><div align="center"><font size="1" face="Arial, Helvetica, sans-serif"><%=bil++%></font></div></td>
                            <td><font size="1" face="Arial, Helvetica, sans-serif"><%= i.getDate()%></font></td>
                            <td><font size="1" face="Arial, Helvetica, sans-serif"><%= i.getPaid()%></font></td>
                            <td><font size="1" face="Arial, Helvetica, sans-serif"><%= ((i.getChequeno()== null) ? "N/A" : i.getChequeno()) %></font></td>
                            <td><div align="right"><font size="1" face="Arial, Helvetica, sans-serif"><%= GeneralTerm.currencyFormat(i.getAmount())%></font></div></td>
                            <td><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
                        </tr>
                        <%}%>
                        <tr> 


                            <td><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
                            <td><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
                            <td><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
                            <td><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
                            <td><div align="right"><font size="1" face="Arial, Helvetica, sans-serif"><input type="hidden" name="total-<%= j.getType()%>" id="total-<%= j.getType()%>" value="<%= j.getTotalItem()%>"><strong><%= GeneralTerm.currencyFormat(j.getTotalItem())%></strong></font></div></td>
                            <td><font size="1" face="Arial, Helvetica, sans-serif">
                                
                                <%if(!master.isDoReconcile()){%>
                                     <input class="pull-right" name="totalg-<%= j.getType()%>" id="totalg-<%= j.getType()%>" type="text" size="12" onChange="tukar()" value="">
                                    <%}else{%>
                                    <span class="pull-right"><%= GeneralTerm.currencyFormat(j.getTotalCurrent()) %></span>
                                    <%}%>
                                
                               
                                
                                </font></td>
                        </tr>
                    </table>
                    <%}%>
                    <table  class="table-striped table-hover" width="90%" border="0" align="center" cellpadding="0" cellspacing="0">

                        <tr> 
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr> 
                            <td colspan="5"><div align="right"><font size="1" face="Arial, Helvetica, sans-serif"> 
                                    <input name="hibah" type="hidden" size="10" value="<%//=totalchrv3%>" readonly="true">
                                    <%//=dcf.format(totalchrv3)%> </font></div></td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr> 
                            <td colspan="5">
                                <div align="left"><font size="1" face="Arial, Helvetica, sans-serif">Balance 
                                    in Bank Book as at <%= AccountingPeriod.fullDateMonth(AccountingPeriod.getEndofPeriod(log, prm.getYear(), prm.getPeriod()))%></font>
                                </div>
                            </td>
                            <td>
                                <div class="pull-right"><font size="1" face="Arial, Helvetica, sans-serif">RM 
                                    
                                    <%if(!master.isDoReconcile()){%>
                                     <input name="bankbookbal" id="bankbookbal" type="text" size="12" onChange="decimal(this, 2)" value="<%= br.getF() %>" >
                                    <%}else{%>
                                    <%= GeneralTerm.currencyFormat(br.getC()) %>
                                    <%}%>
                                    
                                    
                                    </font>
                                </div>
                            </td>
                        </tr>
                        <tr> 
                            <td colspan="6">&nbsp;</td>
                        </tr>

                    </table>
                </form>
            </div>
        </div>
    </div>
</div>