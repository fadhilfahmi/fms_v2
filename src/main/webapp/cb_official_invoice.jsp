<%-- 
    Document   : list_bank
    Created on : Mar 4, 2016, 4:06:05 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) OfficialReceiptDAO.getModule();
%>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $.ajax({
            url: "cb_official_invoice_list.jsp",
            success: function (result) {
                $('#result').empty().html(result).hide().fadeIn(300);

            }});

        $('#keyword').keyup(function () {
            //var l = $('input[name=searchby]:checked').val();
            var keyword = $(this).val();

            $.ajax({
                url: "cb_official_invoice_list.jsp?keyword=" + keyword,
                success: function (result) {
                    $('#result').empty().html(result).hide().fadeIn(300);

                }});
        });



        $('#result').on('click', '.thisresult', function (e) {

            var invref = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addinvoice&refer=" + invref,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });


            /*var a = $(this).attr('href');
             var b = $(this).attr('id');
             var c = $(this).attr('title');
             var d = $(this).attr('id1');
             var e = $(this).attr('id2');
             var f = $(this).attr('id3');
             var g = $(this).attr('id4');
             var h = $(this).attr('id5');
             var i = $(this).attr('id6');
             var j = $(this).attr('id7');
             var k = $(this).attr('id8');
             
             
             $('#<%= request.getParameter("name")%>').val(a);
             $('#<%= request.getParameter("code")%>').val(b);
             $('#baddress').val(d);
             $('#bcity').val(e);
             $('#bpostcode').val(f);
             $('#bstate').val(g);
             $('#gstid').val(i);
             $('#coacode').val(j);
             $('#coadesc').val(k);*/

            e.preventDefault();
        });



    });
</script>

<div class="form-group">
    <div class="form-group input-group">
        <input type="text" class="form-control" id="keyword" placeholder="Search Buyer...">
        <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
            </button>
        </span>
    </div>
</div>

<div id="result">

</div>