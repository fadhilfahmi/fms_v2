<%-- 
    Document   : ar_agreement_view_each
    Created on : Oct 12, 2016, 4:44:24 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArContractInfo"%>
<%@page import="com.lcsb.fms.dao.financial.ar.ArContractDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArContractAgree"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    ArContractAgree vi = (ArContractAgree) ArContractDAO.getEachAgree(log,request.getParameter("refer"));
    ArContractInfo v = (ArContractInfo) ArContractDAO.getInfo(log,request.getParameter("buyer"));
%>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#print').click(function (e) {
            printElement($("#view").html());
        });
    });
</script>


<table class="table table-bordered table-striped  table-hover" id="view">
    <thead>
        <tr>
            <th colspan="4">Contract Detail
                <!--<small><span class="label label-primary">Preparing</span></small>-->
                <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">

                    <button id="print" class="btn btn-default btn-xs viewvoucher" title="<%//= mod.getModuleID() %>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i></button>
                    <button id="<%//= v.getRefer() %>" class="btn btn-default btn-xs editmain" title="<%//= mod.getModuleID() %>" name="edit_st"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button id="<%//= v.getRefer() %>"  class="btn btn-danger btn-xs deletemain" title="<%//= mod.getModuleID() %>" id="<%//= pvi.getRefer() %>" name="delete_st"><i class="fa fa-trash-o" aria-hidden="true"></i></button>

                </div>
            </th>
        </tr>
    </thead>
    <tbody class="activerowm tdrow">
        <tr>
            <th width="20%">Buyer</th>
            <td width="30%"><%= v.getCode()%> - <%= v.getDescp()%></td>
            <th width="20%">Contract Price</th>
            <td width="30%">RM<%= GeneralTerm.currencyFormat(vi.getPrice())%></td>
        </tr>
        <tr>
            <th>Location</th>
            <td><%= vi.getLoccode()%> - <%= vi.getLocname()%></td>
            <th>Month of Delivery</th>
            <td><%= vi.getDmonth()%></td>
        </tr>
        <tr>
            <th>Contract No</th>
            <td><%= vi.getNo()%></td>
            <th>Year of Delivery</th>
            <td><%= vi.getDyear()%></td>
        </tr>
        <tr>
            <th>Contract Date</th>
            <td><%= vi.getDate()%></td>
            <th>Broker</th>
            <td><%= vi.getBrokercd()%> - <%= vi.getBrokerde()%></td>
        </tr>
        <tr>
            <th>Effective Date</th>
            <td><%= vi.getEffectivedate()%></td>
            <th>Broker No</th>
            <td><%= vi.getBrokernum()%></td>
        </tr>
        <tr>
            <th>Commodity</th>
            <td><%= vi.getComcd()%> - <%= vi.getComacc()%></td>
            <th>Destination</th>
            <td><%= vi.getDestination()%></td>
        </tr>
        <tr>
            <th>Quantity</th>
            <td><%= vi.getQty()%></td>
            <th>Active Contract?</th>
            <td><%= vi.getActive()%></td>
        </tr>
        <tr>
            <th>Bases on MPOB ?</th>
            <td><%= vi.getMpob()%></td>
            <th></th>
            <td></td>
        </tr>
    </tbody>
</table>