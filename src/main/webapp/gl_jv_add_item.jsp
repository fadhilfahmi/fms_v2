<%-- 
    Document   : gl_jv_add_item
    Created on : Nov 18, 2016, 11:37:50 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucherItem"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.gl.JournalVoucher"%>
<%@page import="com.lcsb.fms.dao.financial.gl.JournalVoucherDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    JournalVoucher v = (JournalVoucher) JournalVoucherDAO.getJV(log,request.getParameter("referno"));
    JournalVoucherItem bal = (JournalVoucherItem) JournalVoucherDAO.getJVBal(log,request.getParameter("referno").substring(0, 15));

    Module mod = (Module) JournalVoucherDAO.getModule();

%>

<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/formValidation.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.js"></script>

<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        
        $('input').click(function (e) {
            $(this).select();
        });

        $('.dateformat').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        });



        $('.getbank').click(function (e) {

            var a = $(this).attr('id');
            var b = $(this).attr('id1');
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Taxation',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_branch.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getbuyer').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Buyer Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_buyer.jsp?name=bname&code=bcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getestate').click(function (e) {

            var a = 'loccode';
            var b = 'locname';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Estate',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_estate.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getbroker').click(function (e) {

            var a = 'brokercd';
            var b = 'brokerde';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Broker',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_broker.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getproduct').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Product Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_product.jsp?name=prodname&code=prodcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getsub').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Sub Account Detail',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_sub.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('.calculate').keyup(function (e) {
            var todeduct = $(this).val();
            var theval = $(this).attr('id');


            if (todeduct != '') {
                var tot = 0;
                if (theval == 'debit') {
                    $('#credit').val(0.0);
                }
                if (theval == 'credit') {
                    $('#debit').val(0.0);
                }

            }
            return false;
        });


    });

</script>
<div id ="maincontainer">
    <div class="partition">
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="editlist" type="<%= v.getJVrefno()%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addjvitem"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>

            <div class="well">

                <form data-toggle="validator" role="form" id="saveform">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Reference No&nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="JVrefno" name="JVrefno" value="<%= v.getJVrefno()%>" readonly>

                                </div>
                                <div class="form-group" id="div_code">
                                    <input type="text" class="form-control input-sm" id="JVid" name="JVid" placeholder="Auto Generated" readonly> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Account&nbsp&nbsp<span class="res_code"></span></label>
                                    <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="actcode" name="actcode">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-sm getaccount" type="button" id="actcode" id1="actdesc"><i class="fa fa-cog"></i> Account</button>
                                        </span>
                                    </div>  
                                </div>
                                <div class="form-group" id="div_code">
                                    <input type="text" class="form-control input-sm" id="actdesc" name="actdesc" placeholder="" autocomplete="off" > 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Location</label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="loclevel" name="loclevel" value="Company" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="getlocation"><i class="fa fa-cog"></i> Location</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">&nbsp;&nbsp;<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="loccode" name="loccode"  autocomplete="off" value="<%= log.getEstateCode()%>" readonly>   
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <div class="form-group" id="div_code">
                                    <input type="text" class="form-control input-sm" id="locdesc" name="locdesc" placeholder="" autocomplete="off" value="<%= log.getEstateDescp()%>" readonly >

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Sub Account</label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="satype" name="satype" value="None" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="getsub"><i class="fa fa-cog"></i> Sub Acc.</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">&nbsp;&nbsp;<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="sacode" name="sacode"  value="00" autocomplete="off" readonly>   
                            </div>
                        </div>
                    </div>                    

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-sm">
                                <div class="form-group" id="div_code">
                                    <input type="text" class="form-control input-sm" id="sadesc" name="sadesc" placeholder="" autocomplete="off" value="Not Applicable" readonly >

                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Remark</label>
                                <textarea class="form-control" rows="3"  id="remark" name="remark"><%= v.getReason()%></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">            
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Debit</label>
                                <input type="text" class="form-control input-sm calculate" id="debit" name="debit" onClick="this.select();" value="<%= GeneralTerm.currencyFormat(bal.getDebit())%>" required>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Credit</label>
                                <input type="text" class="form-control input-sm calculate" id="credit" name="credit" onClick="this.select();" value="<%= GeneralTerm.currencyFormat(bal.getCredit())%>" required>
                            </div>
                        </div>
                    </div>

                    <input style="font-size:12px" name="balance" id="balance" type="hidden" value="<%= bal.getBalance()%>" size="25"  />
                    <input style="font-size:11px" name="balance_perm" id="balance_perm" type="hidden" value="<%= bal.getBalance()%>" size="25"  />
                    <input style="font-size:11px" name="sum_debit" id="sum_debit" type="hidden" value="<%= bal.getDebit()%>" size="25"  />
                    <input style="font-size:11px" name="sum_credit" id="sum_credit" type="hidden" value="<%= bal.getCredit()%>" size="25"  />
                    <input style="font-size:12px" name="amtbeforetax" id="amtbeforetax" type="hidden" value="0.00" size="25"  />

                </form>
            </div>
        </div>
    </div>
</div>

