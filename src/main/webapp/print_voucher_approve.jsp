<%-- 
    Document   : print_voucher.jsp
    Created on : Dec 3, 2016, 8:16:46 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.template.voucher.FormattedVoucher"%>
<%@page import="com.lcsb.fms.template.voucher.VoucherMaster"%>
<%@page import="com.lcsb.fms.template.voucher.PaymentVoucher_Template"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    FormattedVoucher v = (FormattedVoucher) VoucherMaster.getMaster(request.getParameter("refer"), log);

    Module mod = (Module) ModuleDAO.getModule(log, request.getParameter("moduleid"));
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/pvoucher.css?1" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {

                $('#print').click(function (e) {

                    printElement($("#viewjv").html());

                });

                $("#backtohome").click(function () {
                    $.ajax({
                        url: "user_dashboard.jsp",
                        success: function (result) {
                            // $("#haha").html(result);
                            window.location.reload();
                        }
                    });
                    return false;
                });


                $(".approve").click(function () {
                    var a = $(this).attr('id');
                    var b = $(this).attr('href');
                    var c = $(this).attr('name');

                    var auth = $('#typeauth').val();

                    var sendauth = '';

                    if (auth == 'Check') {
                        sendauth = 'check';
                    } else if (auth == 'Approve') {
                        sendauth = 'distformanager';
                    }

                    if (auth == 'Check') {
                        $.ajax({
                            url: "PathController?moduleid="+c+"&process=" + sendauth + "&referno=" + a,
                            success: function (result) {

                                $('#herey').empty().html(result).hide().fadeIn(100);


                            }
                        });
                    } else if (auth == 'Approve') {
                        BootstrapDialog.confirm({
                            title: 'Confirmation',
                            message: 'Are you sure to ' + auth + '?',
                            type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                            closable: true, // <-- Default value is false
                            draggable: true, // <-- Default value is false
                            btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                            btnOKLabel: auth, // <-- Default value is 'OK',
                            btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
                            callback: function (result) {
                                // result will be true if button was click, while it will be false if users close the dialog directly.
                                if (result) {
                                    $.ajax({
                                        url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + sendauth + "&referno=" + a,
                                        success: function (result) {


                                            window.location.reload();


                                        }
                                    });
                                }
                            }
                        });
                    }


                    return false;
                });



            });
        </script>

    </head>
    <body>
        <div id ="maincontainer">
            <div class="partition"> 
                <div class="headofpartition"></div>
                <div class="bodyofpartition">
                    <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                        <tr>
                            <td valign="middle" align="left">
                                <input type="hidden" id="typeauth" value="<%= request.getParameter("auth")%>">

                            </td>
                        </tr>
                    </table>

                    <table id="table_left" width="100%" cellspacing="0" border="0">
                        <tr>
                            <td valign="middle" width="33%" align="left">
                                <div class="btn-group btn-group-sm" role="group" aria-label="...">


                                    <div class="btn-group" role="group" aria-label="...">
                                        <button id="backtohome" class="btn btn-default btn-sm" title="<%= v.getModuleid()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i></button>

                                        <button id="print" class="btn btn-default btn-sm" title="<%//= mod.getModuleID()%>" name="viewlist"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
                                        <button id="<%= v.getRefer()%>" class="btn btn-success btn-sm approve" title="<%//= mod.getModuleID()%>" name="<%= mod.getModuleID()%>"><%= request.getParameter("auth")%></button>



                                    </div>
                                </div>
                                <span>&nbsp;&nbsp;<strong><%= v.getRefer()%></strong> | Printable Page : <%= v.getPage()%></span>
                            </td>
                            <td width="60%" style="vertical-align: middle;">
                                <!--<div class="form-group" id="div_code">
                                    
                                    <select class="form-control input-xs" id="reflexcb" name="reflexcb">
                                <%//= ParameterDAO.parameterList("YesNo Type", "No")%>
                            </select>   
                        </div>-->
                            </td>
                        </tr>
                    </table>
                    <table id="maintbl" width="100%" cellspacing="0" border="0">
                        <tr>
                            <td class="greybg">
                                <table id="table_left" width="100%" cellspacing="0">
                                    <tr>
                                        <td width="100%">
                                            <div id="viewjv" style="overflow-y: scroll; height:600px;">
                                                <%= v.getOutput()%>
                                            </div>
                                </table>
                            </td>
                        </tr>
                        <input type="hidden" id="checkbyid" value="">
                        <input type="hidden" id="checkbyname" value="">
                        <input type="hidden" id="checkbydesign" value="">
                        <input type="hidden" id="checkdate" value="">
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
