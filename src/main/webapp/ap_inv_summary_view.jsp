<%-- 
    Document   : ap_inv_summary_view
    Created on : Sep 11, 2017, 10:01:53 AM
    Author     : fadhilfahmi
--%>

<%-- 
    Document   : ap_pv_edit_list
    Created on : May 11, 2016, 9:06:22 AM
    Author     : user
--%>

<%@page import="com.lcsb.fms.dao.financial.gl.PostDAO"%>
<%@page import="com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO"%>
<%@page import="com.lcsb.fms.model.financial.ap.VendorInvoiceRefer"%>
<%@page import="com.lcsb.fms.model.financial.ap.VendorInvoice"%>
<%@page import="com.lcsb.fms.dao.financial.ap.VendorInvoiceDAO"%>
<%@page import="com.lcsb.fms.model.financial.ap.VendorInvoiceDetail"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    VendorInvoice v = (VendorInvoice) VendorInvoiceDAO.getPNV(log, request.getParameter("refer"));
    VendorInvoiceDetail vi = (VendorInvoiceDetail) VendorInvoiceDAO.getPNVitem(log, request.getParameter("refer"));
    Module mod = (Module) VendorInvoiceDAO.getModule();
    String status = VendorInvoiceDAO.getStatus(log, v.getInvrefno());
    
String topath = request.getParameter("topath");
    String moduleid = mod.getModuleID();
    String token = request.getParameter("status");

    if (topath == null) {
        topath = "viewlist";
    } else {
        //moduleid = request.getParameter("frommodule");
    }

    String classButtonColor = "";
    String classButtonDisable = "";

    if (status.equals("Approved") && token.equals("Checked")) {
        classButtonColor = "btn-warning";
        classButtonDisable = "disabled";
    } else if (status.equals("Checked") && token.equals("Prepared")) {
        classButtonColor = "btn-warning";
        classButtonDisable = "disabled";
    } else if (token.equals("Checked")) {
        classButtonColor = "btn-success";
        classButtonDisable = "";
    } else if (token.equals("Prepared")) {
        classButtonColor = "btn-success";
        classButtonDisable = "";
    }
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $('#print').click(function (e) {
            printElement($("#viewjv").html());
        });



        $("#addcredit").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addcredit&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $(".addrefer").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addrefer&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".addmaterial").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addmaterial&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });




    });
</script>

<table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
    <tr>
        <td valign="middle" align="left">
            <div class="btn-group" role="group" aria-label="...">
                <button id="backtox" class="btn btn-default btn-xs" title="<%= mod.getModuleID()%>"  name="<%= topath%>" type="<%= token%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                <button id="<%= v.getInvrefno()%>" class="btn btn-default btn-xs viewprinted" title="<%= mod.getModuleID()%>"  name="<%= topath%>" type="<%= token%>"><i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;&nbsp;View Printed</button>
                <%
                    if (token.equals("Prepared")) {
                %>
                <button id="<%= v.getInvrefno()%>" class="btn <%= classButtonColor%> btn-xs checknow <%= classButtonDisable%>" title="<%= mod.getModuleID()%>"  name="<%= topath%>" type="<%= token%>"><i class="fa fa-check-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;Check</button>
                <%
                } else if (token.equals("Checked")) {
                %>
                <button id="<%= v.getInvrefno()%>" class="btn <%= classButtonColor%> btn-xs approvenow <%= classButtonDisable%>" title="<%= mod.getModuleID()%>"  name="<%= topath%>" type="<%= token%>"><i class="fa fa-check-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;Approve</button>
                <%
                    }
                %>
            </div>

        </td>
    </tr>
</table>
<br>
<table class="table table-bordered table-striped  table-hover">
    <thead>
        <tr>
            <th colspan="4">Voucher Information
                <!--<small><span class="label label-primary">Preparing</span></small>-->
                <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">

                    <%if (!status.equals("Approved")) {
                                        if ((VendorInvoiceDAO.isCheck(log, v.getInvrefno()) && log.getListUserAccess().get(2).getAccess())) {%>

                    <button  class="btn btn-default btn-xs approve" title="<%= mod.getModuleID()%>" id="<%= v.getInvrefno()%>"><i class="fa fa-check-square-o" aria-hidden="true"></i> Approve</button>

                    <%} else if (!VendorInvoiceDAO.isApprove(log, v.getInvrefno()) && log.getListUserAccess().get(1).getAccess()) {%> 

                    <button  class="btn btn-default btn-xs check" title="<%= mod.getModuleID()%>" id="<%= v.getInvrefno()%>" name="addrefer"><i class="fa fa-check-square-o" aria-hidden="true"></i> Check</button>

                    <%}%>
                    <button id="<%= v.getInvrefno()%>" class="btn btn-default btn-xs replicate" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-files-o" aria-hidden="true"></i> Replicate</button>
                    <button id="<%= v.getInvrefno()%>" class="btn btn-default btn-xs viewvoucher" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i></button>
                    <button id="<%= v.getInvrefno()%>" class="btn btn-default btn-xs editmain" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button id="<%= v.getInvrefno()%>"  class="btn btn-danger btn-xs deletemain" title="<%= mod.getModuleID()%>" id="<%//= pvi.getRefer() %>" name="delete_st"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                        <%}else{%>
                    <button id="<%= v.getInvrefno()%>" class="btn btn-default btn-xs replicate" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-files-o" aria-hidden="true"></i> Replicate</button>
                    <button id="<%= v.getInvrefno()%>" class="btn btn-default btn-xs viewvoucher" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i></button>
                        <%}%>    
                </div>
            </th>
        </tr>
    </thead>
    <tbody class="activerowm tdrow">
        <tr>
            <th width="25%">Status</th>
            <td width="25%"><%= GeneralTerm.getStatusLabel(VendorInvoiceDAO.getStatus(log, v.getInvrefno()))%></td>
            <th width="25%">Invoice Type</th>
            <td width="25%"><%= v.getInvtype()%></td>
        </tr>
        <tr>
            <th>Period</th>
            <td><%= v.getYear()%>, <%= v.getPeriod()%></td>
            <th>Supplier</th>
            <td><%= v.getSuppcode()%> - <%= v.getSuppname()%></td>
        </tr>
        <tr>
            <th>Voucher No</th>
            <td><%= v.getInvrefno()%></td>
            <th>Amount</th>
            <td>RM<%= GeneralTerm.currencyFormat(v.getTotalamount())%></td>
        </tr>
        <tr>
            <th>Voucher Date</th>
            <td><%= v.getDate()%></td>
            <th>Remark</th>
            <td><%= v.getRemark()%></td>
        </tr>
        <tr class="togglerow" style="display: none">
            <th>Account</th>
            <td><%= v.getAccode()%> - <%= v.getAcdesc()%></td>
            <th>Sub Account</th>
            <td><%= v.getSatype()%> / <%= v.getSacode()%> / <%= v.getSadesc()%></td>
        </tr>
        <tr class="togglerow" style="display: none">
            <th>Paid City</th>
            <td><%//= v.getPaidcity()%></td>
            <th>Paid State</th>
            <td><%//= v.getPaidstate()%></td>
        </tr>
        <tr class="togglerow" style="display: none">
            <th>Rounding</th>
            <td><%= v.getRacoacode()%> - <%= v.getRacoadesc()%></td>
            <th>Paid Type</th>
            <td><%//= v.getPaidtype()%></td>
        </tr>
        <tr class="togglerow" style="display: none">
            <th>Bank</th>
            <td><%= v.getEstatecode()%> - <%= v.getEstatename()%></td>
            <th>Cheque No</th>
            <td></td>
        </tr>
    </tbody>
</table>
<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th colspan="5">Reference Information<!--<small>asdada</small>-->
                <span class="pull-right">
                    <%if (!status.equals("Approved")) {%>
                    <button  class="btn btn-default btn-xs addrefer" title="<%= mod.getModuleID()%>" id="<%= v.getInvrefno()%>" name="addrefer"><i class="fa fa-plus-circle" aria-hidden="true"></i> Reference</button>
                    <%}%>
                </span>
            </th>
        </tr>
        <tr>
            <th>Type</th>
            <th>Date</th>
            <th>Remark</th>
            <th>Amount</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <%List<VendorInvoiceRefer> listRefer = (List<VendorInvoiceRefer>) VendorInvoiceDAO.getAllRefer(log, v.getInvrefno());
                        if (listRefer.isEmpty()) {%>
        <tr>
            <td colspan="5">
                <span class="font-small-red">No data available.</span>
            </td>
        </tr>
        <%}
                        for (VendorInvoiceRefer c : listRefer) {%>
        <tr class="activerowx" id="<%= c.getReferid()%>">
            <td class="tdrow">&nbsp;<i class="fa fa-paperclip" aria-hidden="true"></i>&nbsp;&nbsp;<%= c.getType()%> </td>
            <td class="tdrow">&nbsp;<%= c.getTarikh()%></td>
            <td class="tdrow">&nbsp;<%= c.getRemark()%></td>
            <td class="tdrow" align="right">&nbsp;<%= GeneralTerm.currencyFormat(c.getAmount())%></td>
            <td class="tdrow" align="right">&nbsp;
                <%if (!status.equals("Approved")) {%>
                <div class="btn-group btn-group-xs" role="group" aria-label="...">
                    <button id="<%= c.getReferid()%>" class="btn btn-default btn-xs edititem" title="<%= mod.getModuleID()%>" name="edit_nd"><i class="fa fa-pencil"></i></button>
                    <button id="<%= c.getReferid()%>"  class="btn btn-default btn-xs deleteitem" title="<%= mod.getModuleID()%>" name="delete_nd"><i class="fa fa-trash-o"></i></button>
                </div>
                <%}%>
            </td>
        </tr>
        <tr id="togglerow_nd<%= c.getReferid()%>" style="display: none">
            <td colspan="5">
                <table class="table table-bordered">
                    <thead></thead>
                    <tbody>
                        <tr>
                            <th>Reference No</th>
                            <td><%= c.getReferid()%></td>
                            <th>No</th>
                            <td><%= c.getNo()%></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <%}%>
    </tbody>
</table>

<table class="table table-bordered table-striped table-hover" id="dataTables">
    <thead>
        <tr>
            <th colspan="5">Material<!--<small>asdada</small>-->
                <span class="pull-right">
                    <%if (!status.equals("Approved")) {%>
                    <button  class="btn btn-default btn-xs addmaterial" title="<%= mod.getModuleID()%>" id="<%= v.getInvrefno()%>" name="addmaterial"><i class="fa fa-plus-circle" aria-hidden="true"></i> Material</button>
                    <%}%>
                </span>
            </th>
        </tr>
        <tr>
            <th>Reference No</th>
            <th>Account</th>
            <th>Sub Account</th>
            <th>Amount</th>
            <th>Action</th>
        </tr>
    </thead>
    <%List<VendorInvoiceDetail> listAll = (List<VendorInvoiceDetail>) VendorInvoiceDAO.getAllPNVItem(log, v.getInvrefno());
                    if (listAll.isEmpty()) {%>
    <tbody>
        <tr>
            <td colspan="5">
                <span class="font-small-red">No data available.</span>
            </td>
        </tr>
        <%}
                        for (VendorInvoiceDetail j : listAll) {%>
        <tr class="activerowy" id="<%= j.getInvrefno()%>">
            <td class="tdrow">&nbsp;<%= j.getInvrefno()%></td>
            <td class="tdrow">&nbsp;<%= j.getAccode()%> - <%= j.getAcdesc()%></td>
            <td class="tdrow">&nbsp;<%= j.getSatype()%> / <%= j.getSacode()%> / <%= j.getSadesc()%> </td>
            <td class="tdrow" align="right">&nbsp;<%= GeneralTerm.currencyFormat(j.getAmount())%></td>
            <td class="tdrow" align="right">&nbsp;
                <%if (!status.equals("Approved")) {%>
                <div class="btn-group btn-group-xs" role="group" aria-label="...">
                    <button id="<%= j.getInvrefno()%>" class="btn btn-default btn-xs edititem" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-pencil"></i></button>
                    <button id="<%= j.getInvrefno()%>" class="btn btn-default btn-xs deleteitem" title="<%= mod.getModuleID()%>"  name="delete_st"><i class="fa fa-trash-o"></i></button>
                </div>
                <%}%>
            </td>
        </tr>
        <tr id="togglerow_st<%= j.getInvrefno()%>" style="display: none">
            <td colspan="5">
                <table class="table table-bordered">
                    <thead></thead>
                    <tbody>
                        <tr>
                            <th width="15%">Location</th>
                            <td  width="35%"><%= j.getLoclevel()%> / <%= j.getLoccode()%> / <%= j.getLocdesc()%></td>
                            <th width="15%">Taxation</th>
                            <td width="35%"><%= j.getTaxcode()%> - <%= j.getTaxdescp()%> / <%= j.getSadesc()%></td>
                        </tr>
                        <tr>
                            <th>Tax Amount</th>
                            <td><%= j.getTaxamt()%></td>
                            <th>Tax Rate</th>
                            <td><%= j.getTaxrate()%></td>
                        </tr>
                        <tr>
                            <th>Remarks</th>
                            <td colspan="3"><%= j.getRemark()%></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
    <%}%>
</table>






