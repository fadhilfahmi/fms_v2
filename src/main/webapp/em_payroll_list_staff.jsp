<%-- 
    Document   : em_payroll_list_staff
    Created on : Jun 7, 2017, 11:54:55 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.financial.employee.EmPayrollStaffParam"%>
<%@page import="com.lcsb.fms.model.financial.employee.EmPayrollType"%>
<%@page import="com.lcsb.fms.dao.financial.employee.EmPayrollDAO"%>
<%@page import="com.lcsb.fms.dao.setup.company.StaffDAO"%>
<%@page import="com.lcsb.fms.model.setup.company.staff.Staff"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoiceItem"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Earning"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.EarningDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>

<%
    response.setHeader("Cache-Control", "no-cache");
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    String code = request.getParameter("code");
    String type = request.getParameter("type");
    String view = request.getParameter("view");

    String buttonText = "";
    String buttonClass = "";
    if (view.equals("all")) {
        buttonText = "Applicable Only";
        buttonClass = "view-all";
    } else if (view.equals("applicable")) {
        buttonText = "All";
        buttonClass = "view-applicable";
    }

%>
<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        var oTable = $('#list').DataTable({
            responsive: true,
            "bPaginate": false,
            "aaSorting": [[1, "asc"]],
            "dom": '<"toolbar">frtip',
        });

        $("div.toolbar").html('<button id="view-list-type" class="btn btn-primary btn-xs pull-left <%=  buttonClass %>" type="<%= code %>" title="<%= type %>"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;View <%= buttonText%></button>');



    });
</script>

<div class="invoice-content">
    <div class="table-responsive">
        <table class="table table-invoice" id="list">
            <thead>
                <tr>
                    <th>STAFF ID</th>
                    <th>STAFF NAME</th>
                    <th>POSITION</th>
                    <th>AMOUNT</th>
                    <th>FREQUENCY</th>
                    <th>APPLICABLE</th>
                </tr>
            </thead>
            <%List<Staff> listAllx = (List<Staff>) StaffDAO.getAllHQStaff(log);

                if (listAllx.isEmpty()) {%>
            <tbody>
                <tr>
                    <td colspan="5">
                        <span class="font-small-red">No data available.</span>
                    </td>
                </tr>
                <%}

                    for (Staff j : listAllx) {

                        EmPayrollStaffParam e = (EmPayrollStaffParam) EmPayrollDAO.getParamList(log, j.getStaffid(), code, type);
                        String labelApplicable = "<span class=\"label label-primary\">No</span>";
                        if (e.getApplicable().equals("Yes")) {
                            labelApplicable = "<span class=\"label label-success\">Yes</span>";
                        }

                        boolean viewT = false;

                        if ((view.equals("applicable") && e.getApplicable().equals("Yes")) || (view.equals("all"))) {
                            viewT = true;
                        }

                        if (viewT) {
                %>
                <tr>
                    <td>
                        <%= j.getStaffid()%>
                    </td>
                    <td><span class="pull-left"><%= j.getName()%></span></td>
                    <td><span class="pull-right"><%= j.getPposition()%></span></td>
                    <td>
                        <span class="pull-right">
                            <input type="text" style="text-align:right;" name="amount" value="<%= GeneralTerm.currencyFormat(e.getAmount())%>" class="form-control input-xs changeAmt" id="<%= j.getStaffid()%>" id1="<%=type%>" title="<%= code%>"  onClick="this.select();">
                        </span>
                    </td>
                    <td><span class="pull-right"><%//= GeneralTerm.currencyFormat(j.getAmount())%></span></td>
                    <td><div id="applicable-<%= j.getStaffid()%>"><%= labelApplicable%></div></td>
                </tr>
                <%}
                    }%>
            </tbody>
        </table>
    </div>

</div>

