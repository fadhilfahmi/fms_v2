<%-- 
    Document   : list_search
    Created on : Mar 1, 2016, 10:16:44 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.Supplier"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.SupplierDAO"%>
<%@page import="com.lcsb.fms.util.dao.SearchDAO"%>
<%@page import="com.lcsb.fms.util.model.Search"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
 <%
     
     String table = request.getParameter("table");
     String column = request.getParameter("column");
     String keyword = request.getParameter("keyword");
     String method = request.getParameter("method");
     String code = request.getParameter("code");
     String descp = request.getParameter("descp");
     %><div class="list-group"><%
            List<Supplier> slist = (List<Supplier>) SupplierDAO.getSupplierHQ(keyword);
            if(slist.isEmpty()){
               %>
       
                <div class="alert alert-danger" role="alert">
                    No matching search criteria were found for <strong><%= keyword %></strong>! 
                    
                </div>
                <button class="btn btn-default btn-xs gotoaddsupplier" type="button"><i class="fa fa-plus-circle"></i>
                    Add Supplier
                </button>
               <%   
            }
            for (Supplier c : slist) { 
               %>
       
            <a class="list-group-item thisresult">
                <div id="left_div"><%= c.getSupplierCode()%></div>
                <div id="right_div"><%= c.getSupplierName()%><button id="<%= c.getSupplierCode()%>" class="btn btn-default btn-xs pull-right getcode"><i class="fa fa-cloud-download" aria-hidden="true"></i></button></div>
            </a>

               <%    
            } 
        %>
</div>