<%-- 
    Document   : co_staff_designation_add
    Created on : Apr 6, 2017, 10:32:08 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.setup.company.staff.StaffDesignation"%>
<%@page import="com.lcsb.fms.model.setup.company.staff.Staff"%>
<%@page import="com.lcsb.fms.dao.setup.company.StaffDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) StaffDAO.getModule();
    StaffDesignation v = (StaffDesignation) StaffDAO.getDesignation(log, request.getParameter("refer"));

%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $('#tab_content .datepicker').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        });


        $('#backtolist').click(function (e) {
            var path = $(this).attr('title');
            var process = $(this).attr('name');
            var refer = $(this).attr('type');
            $.ajax({
                url: "PathController?moduleid=" + path + "&process=" + process + "&refer=" + refer,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#tab_content').empty().html(result).hide().fadeIn(100);
                }
            });
            return false;
        });

    });

</script>

<div class="col-sm-6">
    <p></p>
    <div class="row">
        <div class="col-sm-12">
            <span class="pull-left">
                <button id="backtolist" class="btn btn-default btn-xs" title="<%= mod.getModuleID()%>" name="viewdesignation" type="<%= v.getStaffid()%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                <button id="savebutton"  class="btn btn-default btn-xs" title="<%= mod.getModuleID()%>" name="editdesignationprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>

        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <div class="well">
                <form data-toggle="validator" role="form" id="saveform">
                    <input type="hidden" name="staffid" value="<%= v.getStaffid()%>">
                    <input type="hidden" name="id" value="<%= v.getId()%>">
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Place of Work Type&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="worklocation" name="worklocation" value="<%= v.getWorklocation() %>" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="getworktype"><i class="fa fa-cog"></i> Paid Type</button>
                                    </span>
                                </div>    
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Location Code &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="dept_code" name="dept_code" value="<%= v.getDeptCode()%>" autocomplete="off" >   
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Location Name&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="dept_descp" name="dept_descp" value="<%= v.getDeptDescp()%>" autocomplete="off" > 
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Designation<span id="res_code"></span></label>
                                <select class="form-control input-sm" id="designation" name="designation">
                                    <%= ParameterDAO.parameterList(log, "Designation", v.getDesignation())%>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Start Date</label>
                                <div class="form-group-sm input-group">
                                    <input type="text" id="datejoin" name="datejoin" class="form-control datepicker" value="<%= v.getDatejoin()%>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="inputName" class="control-label">End Date</label>
                                <div class="form-group-sm input-group">
                                    <input type="text" id="dateend" name="dateend" class="form-control datepicker" value="<%= v.getDateend()%>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Status</label>
                                <select class="form-control input-sm" id="status" name="status">
                                    <%= ParameterDAO.parameterList(log, "YesNo Type",  v.getStatus())%>
                                </select>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>


