<%-- 
    Document   : list_tax_view
    Created on : Mar 2, 2016, 2:35:15 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.TaxDAO"%>
<%@page import="com.lcsb.fms.util.model.Tax"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<div class="list-group"><%
            
            String keyword = request.getParameter("keyword");
            List<Tax> slist = (List<Tax>) TaxDAO.getAllTax(log,keyword);
            for (Tax c : slist) { 
               %>
       
            <a href="<%= c.getDescp()%>" id="<%= c.getTaxcode()%>" title="<%= c.getTaxrate() %>" id1="<%= c.getTaxcoacode() %>" id2="<%= c.getTaxcoadescp()%>" class="list-group-item thisresult">
                <div id="left_div"><%= c.getTaxcode()%></div>
                <div id="right_div"><%= c.getDescp()%></div>
            </a>

               <%    
            } 
        %>
</div>