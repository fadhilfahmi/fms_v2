<%-- 
    Document   : ap_assign_edit
    Created on : Oct 11, 2016, 11:05:24 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.financial.ap.VendorAssignDAO"%>
<%@page import="com.lcsb.fms.model.financial.ap.VendorAssign"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
VendorAssign v = (VendorAssign) VendorAssignDAO.getInfo(log, request.getParameter("refer"));
Module mod = (Module) VendorAssignDAO.getModule();


%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="jsfunction/jquery-1.12.0.min.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
        <link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
        <script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
           
           
           
           
           
           $('#getsupplier').click(function(e){
               //e.preventDefault();
               BootstrapDialog.show({
                   type:BootstrapDialog.TYPE_DEFAULT,
                    title: 'Get Supplier Info',
                    //message: $('<body></body>').load('list_coa.jsp')
                    message: function(dialog) {
                        var $content = $('<body></body>').load('list_supplier.jsp?name=sname&code=scode&address=saddress');
                        $('body').on('click', '.thisresult', function(event){
                            console.log('tutup');
                            dialog.close();
                            event.preventDefault();
                        });

                        return $content;
                    }
                });
               
               return false;
           });
          
           
           
        });
        
       </script>
    </head>
    <body>
        <div id ="maincontainer">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
              <td width="74%" class="borderbot" align="left"><span class="bigfonttitle"><%= mod.getModuleDesc() %></span>&nbsp;&nbsp;<span class="midfonttitle">Add New Reference for <%= mod.getModuleDesc() %></span></td>
		<td width="26%" class="borderbot" align="right">&nbsp;</td>
	  </tr>
  </table>
	<br>
   <div class="partition"> 
       <div class="headofpartition"></div>
       <div class="bodyofpartition">
  <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
		<tr>
			<td valign="middle" align="left">
				<button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="viewlist" type="<%//= vm.getInvrefno()%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                                <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID() %>" name="update"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
			</td>
		</tr>
  </table>
  <br>
 
  
<form data-toggle="validator" role="form" id="saveform">
    
    <div class="well">
     <div class="row">
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Supplier Code &nbsp&nbsp<span class="res_code"></span></label>
                <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="scode" name="suppcode" value="<%= v.getSuppcode() %>">
                                        <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="getsupplier"><i class="fa fa-cog"></i> Get Supplier</button>
                                        </span>
                                    </div></div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Supplier Name &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="sname" name="suppname" placeholder="" autocomplete="off"  value="<%= v.getSuppname()%>"> 
             </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Supplier Address&nbsp&nbsp<span class="res_code"></span></label>
                <textarea class="form-control" rows="3"  id="saddress" name="suppaddress"><%= v.getSuppaddress() %></textarea>
            </div>  
        </div>
        
   </div>
       
    </div>
      
 <div class="well">         
    <div class="row">
        <div class="col-sm-5">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Account Code &nbsp&nbsp<span class="res_code"></span></label>
                <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="accode" name="accode" value="<%= v.getAccode()%>">
                                        <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="accode" id1="acdesc"><i class="fa fa-cog"></i> Account</button>
                                        </span>
                                    </div>  
             </div>
        </div>
        <div class="col-sm-7">
            <div class="form-group" id="div_digit">
                <label for="inputName" class="control-label">Account Description &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                <input type="text" class="form-control input-sm" id="acdesc" name="acdesc" placeholder="" autocomplete="off" value="<%= v.getAcdesc()%>"> 
             </div>
        </div>
        
    </div>
    <div class="row">
    <div class="col-sm-5">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Company Code &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="companycode" name="companycode" placeholder="0.00" autocomplete="off" value="<%= v.getCompanycode() %>" readonly > 
             </div>
        </div>
        <div class="col-sm-7">
            <div class="form-group" id="div_code">
                <label for="inputName" class="control-label">Company Name &nbsp&nbsp<span class="res_code"></span></label>
                <input type="text" class="form-control input-sm" id="companyname" name="companyname" placeholder="0.00" autocomplete="off" value="<%= v.getCompanyname()%>" readonly> 
             </div>
        </div>
    </div>   
     
 </div> 
    
       
   
    
    
    
    <!--<div class="form-group-sm input-group">
    <input type="text" class="form-control input-sm">
    <span class="input-group-btn">
    <button class="btn btn-default btn-sm" type="button"><i class="fa fa-cog"></i> Get Account Code</button>
    </span>
    </div>-->
    
</form>
</div>
  
 
     </div>
     </div>
      </div>
      

    </body>
</html>
