<%-- 
    Document   : cf_portfolio_view_each
    Created on : Dec 15, 2016, 11:46:01 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.PortfolioDAO"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Portfolio"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Portfolio v = (Portfolio) PortfolioDAO.getInfo(log,request.getParameter("refer"));
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            $('#print').click(function(e){
                printElement($("#view").html());
            });
        });
</script>


            <table class="table table-bordered table-striped  table-hover" id="view">
                <thead>
                    <tr>
                        <th colspan="4">Contract Detail
                        <!--<small><span class="label label-primary">Preparing</span></small>-->
                            <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
                            
                                <button id="print" class="btn btn-default btn-xs viewvoucher" title="<%//= mod.getModuleID() %>" name="edit_st"><i class="fa fa-print" aria-hidden="true"></i></button>
                                <button id="<%//= v.getRefer() %>" class="btn btn-default btn-xs editmain" title="<%//= mod.getModuleID() %>" name="edit_st"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                <button id="<%//= v.getRefer() %>"  class="btn btn-danger btn-xs deletemain" title="<%//= mod.getModuleID() %>" id="<%//= pvi.getRefer() %>" name="delete_st"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                               
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody class="activerowm tdrow">
                    <tr>
                        <th width="20%">Code</th>
                        <td width="30%"><%= v.getCode()%></td>
                        <th width="20%">Description</th>
                        <td width="30%"><%= v.getDescp() %></td>
                    </tr>
                    <tr>
                        <th>Register No</th>
                        <td><%= v.getCoregister()%></td>
                        <th>GST ID</th>
                        <td><%= v.getGstid()%></td>
                    </tr>
                    <tr>
                        <th>Account</th>
                        <td><%= v.getCoa()%> - <%= v.getCoadescp()%></td>
                        <th>Status</th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Address</th>
                        <td><%= v.getAddress()%></td>
                        <th>Postcode</th>
                        <td><%= v.getPostcode()%></td>
                    </tr>
                    <tr>
                        <th>City</th>
                        <td><%= v.getCity()%></td>
                        <th>State</th>
                        <td><%= v.getState()%></td>
                    </tr>
                    <tr>
                        <th>Country</th>
                        <td><%= v.getCountry()%></td>
                        <th>Phone</th>
                        <td><%= v.getPhone()%></td>
                    </tr>
                    <tr>
                        <th>Remarks</th>
                        <td><%= v.getRemarks()%></td>
                        <th></th>
                        <td></td>
                    </tr>
                </tbody>
            </table>