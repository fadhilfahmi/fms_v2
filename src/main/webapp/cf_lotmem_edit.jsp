<%-- 
    Document   : cf_lotmem_edit
    Created on : Apr 26, 2016, 11:05:46 AM
    Author     : qoyum
--%>

<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.model.setup.configuration.LotMember"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.LotMemberDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) LotMemberDAO.getModule();
    LotMember v = (LotMember) LotMemberDAO.getInfo(log,request.getParameter("refer"));

%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.js"></script>

<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#date_inactive').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        });
        $('#date_active').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        });

        $('#getbank').click(function (e) {

            var a = 'bankcode';
            var b = 'bankname';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Taxation',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_bank.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });
    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="editlist" type="<%= v.getMemCode()%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="editprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>

            <form data-toggle="validator" role="form" id="saveform">
                <label for="inputName" class="control-label">Basic Info &nbsp&nbsp</label>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Member Code &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="mem_Code" name="mem_Code" autocomplete="off" value="<%= v.getMemCode()%>" readonly >   
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Member Name &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="mem_Name" name="mem_Name" autocomplete="off" value="<%= v.getMemName()%>">   
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Title &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="title" name="title">
                                    <%= ParameterDAO.parameterList(log,"title", v.getTitle())%>
                                </select>  
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Use of Title   &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="usetitle" name="usetitle">
                                    <%= ParameterDAO.parameterList(log,"YesNo Type", v.getUsetitle())%>
                                </select> 
                            </div>
                        </div>




                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">IC Number &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="ic" name="ic" autocomplete="off" value="<%= v.getIc()%>">   
                            </div>
                        </div>

                    </div>
                </div>

                <label for="inputName" class="control-label">Posting Address &nbsp&nbsp</label>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Address &nbsp&nbsp<span class="res_code"></span></label>
                                <textarea class="form-control" rows="4"  id="address" name="address"><%= v.getAddress()%></textarea>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">City &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="city" name="city"  value="<%= v.getCity()%>" > 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">State &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="state" name="state" value="<%= v.getState()%>" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Postcode &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="postcode" name="postcode" value="<%= v.getPostcode()%>" autocomplete="off" > 
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Telephone Number&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="notel" name="notel" autocomplete="off" value="<%= v.getNotel()%>">   
                            </div></div>
                    </div>
                </div>

                <label for="inputName" class="control-label">Payment Info &nbsp&nbsp</label>
                <div class="well">
                    <div class="row">

                        <div class="col-sm-3">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Payment Method &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <select class="form-control input-sm" id="paymethod" name="paymethod">
                                    <%= ParameterDAO.parameterList(log,"Lot Payment Type", v.getPaymethod())%>
                                </select> 
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Bank&nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="bankname" name="bank" value="<%= v.getBank()%>" autocomplete="off" >
                                    <span class="input-group-btn">


                                        <button class="btn btn-default btn-sm" id="getbank" title=""><i class="fa fa-cog"></i> Get Bank</button></span>
                                </div>  
                            </div>
                        </div> 

                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label"> Bank Account Number &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="acc" name="acc" autocomplete="off" value="<%= v.getAcc()%>">   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Status &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="status" name="status">
                                    <%= ParameterDAO.parameterList(log,"Service Status", v.getStatus())%>
                                </select>   
                            </div>
                        </div>

                    </div>

                </div>

                <label for="inputName" class="control-label">Others Info &nbsp&nbsp</label>                
                <div class="well"><div class="row">
                        <div class="col-sm-2">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Inactive Date &nbsp&nbsp<span id="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="date_inactive" name="date_inactive" placeholder="Inactive Date" autocomplete="off" value="<%= v.getDateInactive()%>">   
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Previous Owner &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="pre_owner" name="pre_owner" autocomplete="off" value="<%= v.getPreOwner()%>">   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Relationship &nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="relation" name="relation">
                                    <%= ParameterDAO.parameterList(log,"Relationship", v.getRelation())%>
                                </select>   
                            </div>
                        </div></div><div class="row">
                        <div class="col-sm-2">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Active Date &nbsp&nbsp<span id="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="date_active" name="date_active" placeholder="Inactive Date" autocomplete="off" value="<%= v.getDateActive()%>">   
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">New Owner &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="new_owner" name="new_owner" autocomplete="off" value="<%= v.getNewOwner()%>">   
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Remarks &nbsp&nbsp<span class="res_code"></span></label>
                                <textarea class="form-control" rows="3"  id="remark" name="remark"><%= v.getRemark()%></textarea>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
