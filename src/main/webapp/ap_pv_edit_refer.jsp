<%-- 
    Document   : ap_pv_edit_refer
    Created on : Oct 6, 2016, 4:15:37 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucherRefer"%>
<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.dao.financial.ap.VendorPaymentDAO"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucher"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.PaymentVoucherItem"%>

<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Parameter"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    PaymentVoucherRefer v = (PaymentVoucherRefer) VendorPaymentDAO.getReferEach(log, request.getParameter("refer"));
    Module mod = (Module) VendorPaymentDAO.getModule();


%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#tarikh').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        });


        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getsub').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Sub Account Detail',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_sub.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#gettax').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Taxation',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_tax.jsp');
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        var a = $('#amount').val();
                        a = a.replace(/\,/g, '');
                        var b = $('#taxrate').val();
                        var c = parseFloat(b) * parseFloat(a) / 100;
                        $('#taxamt').val(parseFloat(c).toFixed(2));


                        event.preventDefault();
                    });

                    return $content;
                }
            });



            return false;
        });

        $('.calculate').keyup(function (e) {
            var todeduct = $(this).val();
            var theval = $(this).attr('id');


            if (todeduct != '') {
                var tot = 0;
                if (theval == 'debit') {
                    $('#credit').val(0.0);
                }
                if (theval == 'credit') {
                    $('#debit').val(0.0);
                }

            }

        });

        $('.totalup').keyup(function (e) {

            var qty = $('#qty').val();
            var unitp = $('#unitp').val();
            var taxrate = $('#taxrate').val();

            var total = qty * unitp;
            var taxamt = taxrate * total / 100;
            $('#taxamt').val(taxamt);
            $('#amount').val(total);

        });

        $('.calculate_gst').change(function (e) {
            alert(9);
            var amt = $('#amount').val();
            var taxrate = $('#taxrate').val();

            var total = taxrate * 100 / amt;

            $('#taxamt').val(total);

        });

        $('.form-control').focusout(function (e) {
            if (($('#debit').val() == 0.00) || ($('#loccode').val() == '') || ($('#actdesc').val() == '') || ($('#remarks').val() == '')) {
                $('#savebutton').prop('disabled', true);
            } else {
                $('#savebutton').prop('disabled', false);
            }
        });

        $('#getproduct').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Product Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_product.jsp?name=prodname&code=prodcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $("#add-new").submit(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addnew",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });

        $("#formUpload").submit(function (event) {

            event.preventDefault();

            //var url=$(this).attr("action");

            $.ajax({
                async: false,
                url: "FileUploadHandler?module=<%= mod.getAbb()%>&refer=<%= v.getVoucer()%>",
                type: "POST",
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (result)
                {

                    //$('#upload-status').empty().html('&nbsp;&nbsp;Upload Succesfull').hide().fadeIn(300);
                    //$('#div_code_upload').removeClass('has-error');
                    //$('#div_code_upload').addClass('has-success');
                    //$('.res_code_upload').append("<i class=\"fa fa-check-circle right-green\"></i>&nbsp;&nbsp;Upload Successfull");
                    $('.res_code_upload').empty().html("<i class=\"fa fa-check-circle right-green\"></i>&nbsp;&nbsp;Upload Successfull").hide().fadeIn(300);
                },
                error: function (xhr, desc, err)
                {
                    $('#upload-status').empty().html(err).hide().fadeIn(300);

                }
            });

        });


    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="editlist" type="<%= v.getVoucer()%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="updaterefer" disabled><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>
            <form action="" method="post" enctype="multipart/form-data" id="formUpload">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code_upload">
                                <label for="inputName" class="control-label">Upload File&nbsp&nbsp<span class="res_code_upload pull-right" id="upload-status"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="file" class="form-control input-sm" id="loclevel" name="file" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="submit" id=""><i class="fa fa-cog"></i> Upload</button>
                                    </span>

                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
            </form> 

            <form data-toggle="validator" role="form" id="saveform">
                <input type="hidden" name="prepareid" id="prepareid" value="<%= log.getUserID()%>">
                <input type="hidden" name="preparename" id="preparename" value="<%= log.getFullname()%>">
                <input type="hidden" name="preparedate" id="preparedate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Voucher No &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="voucer" name="voucer" value="<%= v.getVoucer()%>"  autocomplete="off" required readonly>   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Reference No &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="refer" name="refer" autocomplete="off" placeholder="Auto Generated" value="<%= v.getRefer()%>" required readonly>   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Date &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="tarikh" name="tarikh" placeholder="Date of Payment Voucher" autocomplete="off" value="<%= v.getTarikh()%>">   
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group form-group-sm">
                                <label for="inputName" class="control-label">Reference Type</label>
                                <select class="form-control input-sm" id="type" name="type">
                                    <%= ParameterDAO.parameterList(log, "Reference Type", v.getType())%>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">No &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="no" name="no" placeholder="" autocomplete="off" value="<%= v.getNo()%>" >   
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Remarks&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <textarea class="form-control" rows="3"  id="remarks" name="remarks"><%= v.getRemarks()%></textarea>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Amount &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="amount" name="amount" value="<%= v.getAmount()%>" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">To Date Paid &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="paid" name="paid" placeholder="0.00" autocomplete="off" value="<%= v.getPaid()%>"> 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">B/F Amount &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="bf" name="bf" placeholder="0.00" autocomplete="off" value="<%= v.getBf()%>"> 
                            </div>
                        </div>

                    </div> 

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">To Paid &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="payment" name="payment" placeholder="0.00" autocomplete="off" value="<%= v.getPayment()%>"> 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">PV Balance &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="balance" name="balance" placeholder="0.00" autocomplete="off" value=""> 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">C/F Amount &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="cf" name="cf" placeholder="0.00" autocomplete="off" value="<%= v.getCf()%>"> 
                            </div>
                        </div>

                    </div>        
                </div>
            </form>
        </div>
    </div>
</div>