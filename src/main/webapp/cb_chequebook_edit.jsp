<%-- 
    Document   : cb_chequebook_add
    Created on : Mar 4, 2016, 3:20:14 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.model.financial.cashbook.CbCekbook"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.ChequeBookDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    CbCekbook cb = (CbCekbook) ChequeBookDAO.getInfo(log,request.getParameter("referenceno"));

%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.9/js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $('#getbank').click(function (e) {

            var a = 'bankcode';
            var b = 'bankname';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Taxation',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_bank.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });
    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= ChequeBookDAO.module_id()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= ChequeBookDAO.module_id()%>" name="editprocess"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>

            <div id="dialog-form-update" title="Add New Information"><p class="validateTips">All form fields are required.</p>
                <form id="saveform"><table id="table_2" cellspacing="0">

                        <tr>
                            <td width="10%" class="bd_bottom" align="left" valign="top">Cheque Book No</td>
                            <td width="90%" class="bd_bottom" align="left"><div class="col-sm-2"><input class="form-control input-sm" style="font-size:12px" name="no" id="no" type="text" value="<%= cb.getNo()%>" readonly=""  /></div></td>
                        </tr>
                        <tr>
                            <td width="10%" class="bd_bottom" align="left" valign="top">Bank/Branch Code</td>
                            <td width="90%" class="bd_bottom" align="left"> 
                                <div class="col-sm-2"><input class="form-control input-sm" style="font-size:12px" name="bankcode" id="bankcode" type="text" value="<%= cb.getBankcode()%>"  /></div>
                                <button class="btn btn-default btn-sm" id="getbank" title="">Get Bank</button>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%" class="bd_bottom" align="left" valign="top">Bank/Branch Name</td>
                            <td width="90%" class="bd_bottom" align="left">
                                <div class="col-sm-4"><input class="form-control input-sm" style="font-size:12px" name="bankname" id="bankname" type="text" value="<%= cb.getBankname()%>" size="50"  /></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%" class="bd_bottom" align="left" valign="top">Date Received</td>
                            <td width="90%" class="bd_bottom" align="left">
                                <div class="col-sm-2"><input class="form-control input-sm" style="font-size:12px" name="tarikh" id="tarikh" type="text" value="<%= cb.getTarikh()%>"  /></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%" class="bd_bottom" align="left" valign="top">Start Cheque No</td>
                            <td width="90%" class="bd_bottom" align="left"><div class="col-sm-2"><input class="form-control input-sm" name="startcek" id="startcek" type="text" value="<%= cb.getStartcek()%>"  /></div></td>
                        </tr>
                        <tr>
                            <td width="10%" class="bd_bottom" align="left" valign="top">No of Cheque</td>
                            <td width="90%" class="bd_bottom" align="left"><div class="col-sm-2"><input class="form-control input-sm"  name="nocek" id="nocek" type="text" value="<%= cb.getNocek()%>"  /></div></td>
                        </tr>
                        <tr>
                            <td width="10%" class="bd_bottom" align="left" valign="top">End of Cheque</td>
                            <td width="90%" class="bd_bottom" align="left"><div class="col-sm-2"><input class="form-control input-sm"  name="endcek" id="endcek" type="text" value="<%= cb.getEndcek()%>"  /></div></td>
                        </tr>
                        <tr>
                            <td width="10%" class="bd_bottom" align="left" valign="top">Active</td>
                            <td width="90%" class="bd_bottom" align="left"><div class="col-sm-1"><select class="form-control input-sm" name="active" id="active">
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select></div></td>
                        </tr>
                    </table>
                </form>
            </div>

        </div>
    </div>
</div>

