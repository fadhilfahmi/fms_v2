<%-- 
    Document   : gl_jv_add_new
    Created on : Feb 29, 2016, 2:03:11 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.financial.gl.JournalVoucherDAO"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) JournalVoucherDAO.getModule();
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });
        //$('#JVdate').datepicker({ dateFormat: 'yyyy-mm-dd' });

        $('#JVdate').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        }).change(dateChanged)
                .on('changeDate', dateChanged);
    });

</script>
<div id ="maincontainer">

    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addjv" ><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>
<div class="row">
                        <div class="col-sm-6">
            <div class="well">

                <form data-toggle="validator" role="form" id="saveform">
                    <input type="hidden" name="estatecode" id="estatecode" value="<%= log.getEstateCode()%>">
                    <input type="hidden" name="estatename" id="estatename" value="<%= log.getEstateDescp()%>">
                    <input type="hidden" name="preparedbyid" id="preparedbyid" value="<%= log.getUserID()%>">
                    <input type="hidden" name="preparedbyname" id="preparedbyname" value="<%= log.getFullname()%>">
                    <input type="hidden" name="preparedate" id="preparedate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
                    <input type="hidden" name="todebit" id="todebit" value="0.00">
                    <input type="hidden" name="tocredit" id="tocredit" value="0.00">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Journal Voucher No &nbsp&nbsp<span id="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="JVno" name="JVno" placeholder="Auto Generated" autocomplete="off" required disabled>   
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Date &nbsp&nbsp<span id="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="JVdate" name="JVdate" placeholder="Date of Journal Voucher" autocomplete="off" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">   
                            </div>
                        </div>
                    </div>
                                <div class="row">
                       
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Reflex to CB &nbsp&nbsp<span id="res_code"></span></label>
                                <select class="form-control input-sm" id="reflexcb" name="reflexcb">
                                    <%= ParameterDAO.parameterList(log, "YesNo Type", "No")%>
                                </select>   
                            </div>
                        </div>
                                <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Reference No &nbsp&nbsp<span id="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="JVrefno" name="JVrefno" placeholder="Auto Generated" autocomplete="off" disabled >   
                            </div>
                        </div>
                                
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Year &nbsp&nbsp<span id="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="year" name="year" placeholder="Code" autocomplete="off" value="<%= AccountingPeriod.getCurYearByCurrentDate() %>">   
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Period &nbsp&nbsp<span id="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="period" name="curperiod"  autocomplete="off" value="<%= AccountingPeriod.getCurPeriodByCurrentDate() %>">   
                            </div>
                        </div>
                        
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Remark &nbsp&nbsp<span id="res_code"></span></label>
                                <textarea class="form-control" rows="3"  id="reason" name="reason"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
                            
          </div>
    </div>                  
        </div>
    </div>
</div>

