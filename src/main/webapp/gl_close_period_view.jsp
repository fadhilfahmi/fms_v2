<%-- 
    Document   : gl_close_period_view
    Created on : Mar 15, 2016, 4:29:21 PM
    Author     : Dell
--%>


<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.model.financial.gl.AccountPeriod"%>
<%@page import="com.lcsb.fms.dao.financial.gl.AccountPeriodDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
AccountPeriod cur = (AccountPeriod) AccountPeriodDAO.getCurPeriod(log);
AccountPeriod pre = (AccountPeriod) AccountPeriodDAO.getPreviousPeriod(log);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <title>JSP Page</title>
    </head>
    <body>
        <div class="row">
            <div class="col-sm-6">
                <div class="well">
  
                <form data-toggle="validator" role="form" id="saveform">
                    <div class="row self-row">
                        <label for="inputName" class="control-label">Previous Period &nbsp&nbsp<span id="res_code"></span></label>
                    </div>
                    

                    <div class="row">
                        <div class="col-sm-6">
                            <label for="inputName" class="control-label">Year</label>
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= pre.getYear()%>" readonly>
                            </div>   
                        </div>
                        <div class="col-sm-6">
                            <label for="inputName" class="control-label">Period</label>
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= pre.getPeriod()%>" readonly>
                            </div>   
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="inputName" class="control-label">Start Date</label>
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= pre.getStartperiod() %>" readonly>
                            </div>   
                        </div>
                        <div class="col-sm-6">
                            <label for="inputName" class="control-label">End Date</label>
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= pre.getEndperiod() %>" readonly>
                            </div>   
                        </div>
                    </div>
                            
                    <div class="row">
                        <div class="col-sm-12">
                            <label for="inputName" class="control-label">Date Close Period</label>
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= pre.getClosedate()%>" readonly>
                            </div>   
                        </div>
                    </div>

                   


                </form>
                        </div></div>
            <div class="col-sm-6">
                <div class="well">
  
                <form data-toggle="validator" role="form" id="saveform">
                    <div class="row self-row">
                        <label for="inputName" class="control-label">Current Period &nbsp&nbsp<span id="res_code"></span></label>
                    </div>
                    

                    <div class="row">
                        <div class="col-sm-6">
                            <label for="inputName" class="control-label">Year</label>
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= cur.getYear()%>" readonly>
                            </div>   
                        </div>
                        <div class="col-sm-6">
                            <label for="inputName" class="control-label">Period</label>
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= cur.getPeriod()%>" readonly>
                            </div>   
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="inputName" class="control-label">Start Date</label>
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= cur.getStartperiod() %>" readonly>
                            </div>   
                        </div>
                        <div class="col-sm-6">
                            <label for="inputName" class="control-label">End Date</label>
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= cur.getEndperiod() %>" readonly>
                            </div>   
                        </div>
                    </div>
                            
                    <div class="row">
                        <div class="col-sm-12">
                            <label for="inputName" class="control-label">Date Close Period</label>
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="code" name="code" placeholder="Code" autocomplete="off" value="<%= AccountingPeriod.getCurrentTimeStamp() %>" readonly>
                            </div>   
                        </div>
                    </div>

                   


                </form>
                        </div></div>
        </div>
            
            
    </body>
</html>
