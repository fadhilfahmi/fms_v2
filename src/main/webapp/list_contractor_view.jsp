<%-- 
    Document   : list_supplier_view
    Created on : May 13, 2016, 1:02:13 PM
    Author     : user
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.Contractor"%>
<%@page import="com.lcsb.fms.util.dao.ContractorDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<div class="list-group"><%
            
            String keyword = request.getParameter("keyword");
            List<Contractor> slist = (List<Contractor>) ContractorDAO.getContractor(log,keyword);
            for (Contractor c : slist) { 
               %>
       
            <a href="<%= c.getCompanyname()%>" id="<%= c.getCode()%>" address="<%= c.getAddress()%>" city="<%= c.getCity()%>" postcode="<%= c.getPostcode()%>" state="<%= c.getState()%>" gstid="<%= c.getGstid()%>" ownername="<%= c.getOwnername()%>" owneric="<%= c.getOwneric()%>"  title="" class="list-group-item thisresult">
                <div id="left_div"><%= c.getCode()%></div>
                <div id="right_div"><%= c.getCompanyname()%></div>
            </a>

               <%    
            } 
        %>
</div>