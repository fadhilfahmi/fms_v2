<%-- 
    Document   : list_tax
    Created on : Mar 2, 2016, 2:06:39 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
            
             $.ajax({
                        url: "list_tax_view.jsp",
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
            }});
            
           $('#keyword').keyup(function(){
                //var l = $('input[name=searchby]:checked').val();
                var keyword = $(this).val();
                
                 $.ajax({
                        url: "list_tax_view.jsp?keyword="+keyword,
                        success: function (result) {
                        $('#result').empty().html(result).hide().fadeIn(300);
                
                }});
            });
            
            
            
            $('#result').on('click', '.thisresult', function(e) { 
                var a = $(this).attr('href');
                var b = $(this).attr('id');
                var c = $(this).attr('title');
                var d = $(this).attr('id1');
                var e = $(this).attr('id2');
                
                
                $('#taxdescp').val(a);
                $('#taxcode').val(b);
                $('#taxrate').val(c);
                $('#taxcoacode').val(d);
                $('#taxcoadescp').val(e);
                e.preventDefault();
            });
            
           
            
        });
        </script>
    </head>
    <body>
         
        
        <div class="form-group">
            <div class="form-group input-group">
                <input type="text" class="form-control" id="keyword">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
        
        <div id="result">
       
        </div>
        
            
    </body>
</html>
