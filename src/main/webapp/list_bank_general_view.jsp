<%-- 
    Document   : list_bank_view
    Created on : Mar 4, 2016, 4:06:59 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.model.setup.configuration.BankGeneral"%>
<%@page import="com.lcsb.fms.util.dao.BankDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<div class="list-group"><%
            
            String keyword = request.getParameter("keyword");
            List<BankGeneral> slist = (List<BankGeneral>) BankDAO.getAllBankGeneral(log,keyword);
            for (BankGeneral c : slist) { 
               %>
       
            <a href="<%= c.getName()%>" id="<%= c.getCode()%>" title="" class="list-group-item thisresult">
                <div id="left_div"><%= c.getCode() %></div>
                <div id="right_div"><%= c.getName()%></div>
            </a>

               <%    
            } 
        %>
</div>