<%-- 
    Document   : cb_bankreport_addtrans
    Created on : Mar 17, 2017, 10:02:33 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.financial.cashbook.BankReportDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.dao.financial.gl.FinancialReportDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    Connection con = ConnectionUtil.getConnection();
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) BankReportDAO.getModule();
%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $("#viewreport").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewreport&vby=" + $('#vby').val() + "&year=" + $('#year').val() + "&period=" + $('#period').val() + "&otype=" + $('#otype').val(),
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });

        $('#getbank').click(function (e) {

            var a = 'bankcode';
            var b = 'bankname';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Bank',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_bank.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });


    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewbankreport"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
                </tr>
            </table>
            <br>
            <form data-toggle="validator" role="form" id="saveform">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="well">

                            <div class="row self-row">
                                <label for="inputName" class="control-label">Bank Charges</label>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Year</label>
                                        <select class="form-control input-sm" id="year" name="year">
                                            <%= ParameterDAO.parameterList("Year", AccountingPeriod.getCurYear())%>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Period</label>
                                        <select class="form-control input-sm" id="period" name="period">
                                            <%= ParameterDAO.parameterList("Period", AccountingPeriod.getCurPeriod())%>
                                        </select> 
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Amount</label>
                                        <input type="text" class="form-control input-sm" id="bname" name="descp" placeholder="" autocomplete="off" >
                                    </div>
                                </div>



                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <button id="add-new" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> Create Payment Voucher</button>
                                    <button id="add-new" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> Create Official Receipt</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="well">

                            <div class="row self-row">
                                <label for="inputName" class="control-label">Hibah</label>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Year</label>
                                        <select class="form-control input-sm" id="year" name="year">
                                            <%= ParameterDAO.parameterList("Year", AccountingPeriod.getCurYear())%>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Period</label>
                                        <select class="form-control input-sm" id="period" name="period">
                                            <%= ParameterDAO.parameterList("Period", AccountingPeriod.getCurPeriod())%>
                                        </select> 
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Amount</label>
                                        <input type="text" class="form-control input-sm" id="bname" name="descp" placeholder="" autocomplete="off" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <button id="add-new" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> Create Official Receipt</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>                               

        </div>
    </div>
</div>

