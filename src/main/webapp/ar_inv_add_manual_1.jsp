<%-- 
    Document   : ar_inv_add_manual
    Created on : Sep 29, 2017, 10:51:16 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.ext.ParseSafely"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArPrepareInvoice"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Buyer"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.BuyerDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArContractInfo"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.dao.financial.ar.ArContractDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArContractAgree"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) SalesInvoiceDAO.getModule();


%>
<!DOCTYPE html>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $('#invdate').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        }).change(dateChanged)
                .on('changeDate', dateChanged);



        var currentDate = $('#invdate').val();

        var y1 = currentDate.substring(0, 4);
        var p1 = currentDate.substring(5, 7);

        var pshort = currentDate.substring(5, 6);
        console.log(pshort);
        if (pshort == 0) {
            p1 = currentDate.substring(6, 7)


            $('#year').val(y1);
            $('#period').val(p1);
        }



        $('#getbuyer').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Buyer Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_buyer_mill.jsp?name=bname&code=bcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getproduct').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Product Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_product.jsp?name=prodname&code=prodcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $("#total").change(function () {
            var val = $(this).val();
            var to = 'amount';
            //alert(val);
            //alert(checkRegexp(val,to));
            if (checkRegexp(val, to)) {
                $('#res_digit').empty();
                $('#div_digit').removeClass('has-error');
                $('#div_digit').addClass('has-success');
                $('#res_digit').append("<i class=\"fa fa-check-circle right-green\"></i>&nbsp;&nbsp;");
                $('#savebutton').prop('disabled', false);
                convertRM(val, to);
            } else {
                $('#res_digit').empty();
                $('#div_digit').removeClass('has-success');
                $('#div_digit').addClass('has-error');
                $('#res_digit').append("<i class=\"fa fa-exclamation-circle fail-red\"></i>&nbsp;&nbsp;Numeric Formatted Only");
                $('#savebutton').prop('disabled', true);
            }
            //
        });



        $('#prodcode-prepare').val($('#prodcode').val());
        $('#prodname-prepare').val($('#prodname').val());
        $('#orderno').val($('#broker-number').val());

    });
</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addinv" ><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>


                <form data-toggle="validator" role="form" id="saveform">
                    <input type="hidden" name="pid" id="pid" value="<%= log.getUserID()%>">
                    <input type="hidden" name="pname" id="pname" value="<%= log.getFullname()%>">
                    <input type="hidden" name="pdate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
                    <input type="hidden" name="unitprice" value="<%//= pi.getUnitprice()%>">

                    <input type="hidden" name="prodcode" id="prodcode-prepare" value="">
                    <input type="hidden" name="prodname" id="prodname-prepare" value="">
                    <div class="well">
                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Date &nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="invdate" name="invdate" placeholder="Date of Sales Invoice" autocomplete="off" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">   
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Payment Term (Days)&nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="payment" name="payment" placeholder="Date of Journal Voucher" autocomplete="off" value="<%//= BuyerDAO.getInfo(log, pi.getBuyercode()).getPaymentterm()%>">   
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Order No &nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="orderno" name="orderno" value="" autocomplete="off" >   
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Year &nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="year" name="year" placeholder="Code" autocomplete="off" value="<%= AccountingPeriod.getCurYear(log)%>">   
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Period &nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="period" name="period"  autocomplete="off" value="<%= AccountingPeriod.getCurPeriod(log)%>">   
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Contract No &nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="contractno" name="contractno" value="<%//= pi.getContractNo()%>" autocomplete="off" >   
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="inputName" class="control-label">Buyer Code&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span id="res_digit" class="res_style"></span></label>
                                            <div class="form-group-sm input-group">
                                                <input type="text" class="form-control input-sm" id="bcode" name="bcode" required>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default btn-sm" type="button" id="getbuyer"><i class="fa fa-cog"></i> Buyer</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="inputName" class="control-label">Buyer Description&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                            <input type="text" class="form-control input-sm" id="bname" name="bname" required>
                                        </div>
                                    </div>

                            <div class="col-sm-2">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">GST ID&nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="gstid" name="gstid" value="<%//= pi.getGstid()%>" autocomplete="off" > 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Address &nbsp&nbsp<span class="res_code"></span></label>
                                    <textarea class="form-control" rows="3"  id="baddress" name="address"><%//= pi.getAddress()%></textarea>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Postcode &nbsp&nbsp</label>
                                    <input type="text" class="form-control input-sm" id="bpostcode" name="postcode" value="<%//= pi.getPostcode()%>" autocomplete="off" > 
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">City &nbsp&nbsp</label>
                                    <input type="text" class="form-control input-sm" id="bcity" name="bcity" value="<%//= pi.getCity()%>" autocomplete="off" > 
                                </div>
                            </div><div class="col-sm-3">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">State &nbsp&nbsp</label>
                                    <input type="text" class="form-control input-sm" id="bstate" name="state" value="<%//= pi.getState()%>" autocomplete="off" > 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Remarks &nbsp&nbsp<span class="res_code"></span></label>
                                    <textarea class="form-control" rows="3"  id="remark" name="remark"><%//= pi.getRemark()%></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Account Code&nbsp&nbsp<span class="res_code"></span></label>
                                    <div class="form-group-sm input-group">
                                        <input type="text" class="form-control input-sm" id="coacode" name="coacode" value="<%//= pi.getCoacode()%>">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-sm getaccount" type="button" id="coacode" id1="coadescp"><i class="fa fa-cog"></i> Account</button>
                                        </span>
                                    </div>  
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group" id="div_code">
                                    <label for="inputName" class="control-label">Account Description&nbsp&nbsp<span class="res_code"></span></label>
                                    <input type="text" class="form-control input-sm" id="coadescp" name="coadescp" placeholder="" autocomplete="off" value="<%//= pi.getCoadesc()%>" > 
                                </div>
                            </div>

                        </div>

                    </div>


            </div>
            </form>

    </div>
</div>
</div>
