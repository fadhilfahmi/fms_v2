<%-- 
    Document   : co_staff_designation_list
    Created on : Apr 6, 2017, 10:44:34 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.setup.company.staff.StaffSpouse"%>
<%@page import="com.lcsb.fms.model.setup.company.staff.Staff"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.model.setup.company.staff.StaffDesignation"%>
<%@page import="com.lcsb.fms.dao.setup.company.StaffDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) StaffDAO.getModule();
    Staff v = (Staff) StaffDAO.getInfo(log, request.getParameter("refer"));

%>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $(".editspouse").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editspouse&refer=" + refer,
                success: function (result) {
                    $('#tab_content').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });


        $(".deletespouse").click(function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            var child = $(this).attr('name');
            
        
            BootstrapDialog.confirm({
                title: '<span style="color:#d9534f;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp; <strong>Confirmation</strong></span>',
                message: 'Are you sure to delete?',
                type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {

                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=deletespouse&id=" + id + "&refer=<%=v.getStaffid()%>",
                            success: function (result) {
                                $('#tab_content').empty().html(result).hide().fadeIn(100);
                            }
                        });
                    }
                }
            });

            return false;
        });
    });
</script>
<table class="data table table-striped no-margin">
    <thead>
        <tr>
            <th colspan="6">
                <span class="pull-left">
                    <button  class="btn btn-default btn-xs addspouse" title="<%= mod.getModuleID()%>" id="<%= v.getStaffid()%>" name="editlist"><i class="fa fa-plus-circle" aria-hidden="true"></i> Spouse</button>
                </span>
            </th>
        </tr>
    </thead>
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>IC No.</th>
            <th class="hidden-phone">Working?</th>
            <th>Date of Birth</th>
            <th><span class="pull-right">Action</span></th>
        </tr>
    </thead>
    <tbody>
        <%List<StaffSpouse> listRefer = (List<StaffSpouse>) StaffDAO.getAllSpouse(log, v.getStaffid());
            int i = 0;
            if (listRefer.isEmpty()) {%>
        <tr>
            <td colspan="6">
                <span class="font-small-red">No data available.</span>
            </td>
        </tr>
        <%}
            for (StaffSpouse c : listRefer) {
                i++;
        %>
        <tr>
            <td><%= i%></td>
            <td><%= c.getName()%></td>
            <td><%= c.getIc()%></td>
            <td class="hidden-phone"><%= c.getWorking()%></td>
            <td class="vertical-align-mid">
                <%= c.getDob()%>
            </td>
            <td>
                <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
                    <button id="<%= c.getId()%>" class="btn btn-warning btn-xs editspouse" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-pencil"></i></button>
                    <button id="<%= c.getId()%>" class="btn btn-danger btn-xs deletespouse" title="<%= mod.getModuleID()%>"  name="<%= StaffDAO.getChildBeneathMother(log, c.getId(), v.getStaffid()) %>"><i class="fa fa-trash-o"></i></button>
                </div>
            </td>
        </tr>
        <%}%>
    </tbody>
</table>
