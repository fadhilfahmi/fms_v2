<%-- 
    Document   : ar_dispatch_list
    Created on : Aug 8, 2017, 9:02:21 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.financial.ar.ArDispatchDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) ArDispatchDAO.getModule();
%>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $.ajax({
            async: true,
            url: "PathController?moduleid=020904&process=cpo_local",
            success: function (result) {

                $('#dataTable_wrapper_cpo_local').empty().html(result).hide().fadeIn(300);

                return false;
            },
            error: function (xhr, desc, err)
            {
                console.log(err);
                console.log(xhr);
                console.log(desc);
                alert('error!');

            }
        });

        $.ajax({
            async: true,
            url: "ProcessController?moduleid=000000&process=connecttomill",
            success: function (result) {
                // $("#haha").html(result);
                console.log(result);
                if (result == 1) {

                    $.ajax({
                        async: true,
                        url: "PathController?moduleid=020904&process=cpo_mill",
                        success: function (result) {
                            $('#dataTable_wrapper_cpo_mill').empty().html(result).hide().fadeIn(300);
                            //alert(result);
                            //return false;
                        },
                        error: function (xhr, desc, err)
                        {
                            console.log(err);
                            console.log(xhr);
                            console.log(desc);
                            alert('error!');

                        }
                    });

                    $.ajax({
                        async: true,
                        url: "PathController?moduleid=020904&process=pk_mill",
                        success: function (result) {
                            $('#dataTable_wrapper_pk_mill').empty().html(result).hide().fadeIn(300);
                            return false;
                        }
                    });
                } else {
                    var conts = '<div style="width:200px; margin:0 auto; color:#000 !important;"><span style="color:#d9534f !important;font-size:20px;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Connection Failed</span></div>';
                    $('#dataTable_wrapper_cpo_mill').empty().html(conts).hide().fadeIn(300);
                    $('#dataTable_wrapper_pk_mill').empty().html(conts).hide().fadeIn(300);
                }

            }
        });



        $.ajax({
            async: true,
            url: "PathController?moduleid=020904&process=pk_local",
            success: function (result) {
                $('#dataTable_wrapper_pk_local').empty().html(result).hide().fadeIn(300);
                return false;
            }
        });

        $("#sync").click(function () {
            var cont = '<i class="fa fa-cog fa-spin fa-fw"></i><span> Connecting...</span>';
            $('#sync').empty().html(cont).hide().fadeIn(300);
            $.ajax({
                async: true,
                url: "ProcessController?moduleid=000000&process=connecttomill",
                success: function (result) {
                    // $("#haha").html(result);
                    console.log(result);
                    if (result == 1) {
                        //$('#herey').empty().html(result).hide().fadeIn(300);
                        var conts = '<i class="fa fa-check-circle success" aria-hidden="true"></i> Connection Successful';
                        $('#sync').empty().html(conts).hide().fadeIn(300);

                        conts = '<i class="fa fa-cog fa-spin fa-fw"></i><span> Syncing...';
                        $('#sync').empty().html(conts).hide().fadeIn(300);

                        $.ajax({
                            async: true,
                            url: "ProcessController?moduleid=020903&process=syncrefinery",
                            success: function (result) {
                                //var timeagoInstance = new timeago();
                                //timeagoInstance.render($('.need_to_be_rendered'));
                                //$('.need_to_be_rendered').html('Just Now');
                                //$('#herey').empty().html(result).hide().fadeIn(300);
                                console.log('Synced -- ' + result);
                                var conts = '<i class="fa fa-check-circle success" aria-hidden="true"></i> Synced';
                                $('#sync').empty().html(conts).hide().fadeIn(300);

                                $.ajax({
                                    url: "ar_dispatch_list.jsp",
                                    success: function (result) {
                                        $('#herey').empty().html(result).hide().fadeIn(300);
                                    }
                                });

                            },
                            error: function () {
                                var conts = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Failed';
                                $('#sync').empty().html(conts).hide().fadeIn(300);
                            }
                        });
                    } else {
                        var conts = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Connection Failed';
                        $('#sync').empty().html(conts).hide().fadeIn(300);
                    }

                },
                error: function () {
                    var conts = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Connection Failed';
                    $('#sync').empty().html(conts).hide().fadeIn(300);
                }
            });
            return false;
        });
    });

</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <div class="row">
                <div class="col-sm-12">
                    <span class="pull-left"><label><i>Crude Palm Oil (CPO)</i></label></span><span class="pull-right"><a id="sync" style="cursor: pointer"><i class="fa fa-download" aria-hidden="true"></i> Sync Now</a></span>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>FMS</label>
                    <div id="dataTable_wrapper_cpo_local"> 
                        <div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <label>From Mill</label>
                    <div id="dataTable_wrapper_cpo_mill">
                        <div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>
                    </div>
                </div>
            </div>
            <hr>
            <label><i>Palm Kernel</i></label>
            <div class="row">
                <div class="col-sm-6">
                    <label>FMS</label>
                    <div id="dataTable_wrapper_pk_local">
                        <div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <label>From Mill</label>
                    <div id="dataTable_wrapper_pk_mill">
                        <div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>