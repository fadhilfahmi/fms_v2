<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.financial.gl.AccountPeriodDAO"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.ChartofAccountDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.ModuleItem"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*,java.text.*,java.util.*" errorPage="" isThreadSafe="false" %>
<%@ page import="java.util.Date"%>
<%
    response.setHeader("Cache-Control", "no-cache");
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) AccountPeriodDAO.getModule();
    //session.removeAttribute("dtparam");
%>

<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>

<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        var updateTable = '<%= mod.getMainTable()%>';

    <%
    List<ModuleItem> mlist = (List<ModuleItem>) ModuleDAO.getAllModuleItem(log,ModuleDAO.getModule(log,mod.getModuleID()).getModuleID(), "cf_module_param");
    for (ModuleItem m : mlist) {
        if (m.getList_View().equals("1")) {
    %>
        columntoView.push('<%= m.getColumn_Name()%>');
        columnName.push('<%= m.getTitle()%>');
    <%
                }

            }
    %>

        var colsTosend = 'cols=';
        for (m = 0; m < columntoView.length; ++m) {//put value from database to variable jget
            if (m == columntoView.length - 1) {
                colsTosend += columntoView[m];
            } else {
                colsTosend += columntoView[m] + '&cols=';
            }
        }


        $("#add-new").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addnew",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });

        $("#set-current").click(function () {
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Set Current <%= mod.getModuleDesc()%>',
                message: function (dialog) {
                    var $content = $('<body></body>').load('gl_accountperiod_set.jsp?');
                    //$('body').on('click', '.thisresult_nd', function(event){
                    //    dialog.close();
                    //    event.preventDefault();
                    //});
                    return $content;
                },
                buttons: [{
                        //icon: 'fa fa-circle-o-notch',
                        label: 'Confirm & Close',
                        cssClass: 'btn-primary',
                        autospin: true,
                        action: function (dialogRef) {
                            var $button = this;
                            var newp = $('#newperiod').val();
                            var newy = $('#newyear').val();
                            dialogRef.enableButtons(false);
                            dialogRef.setClosable(false);
                            //dialogRef.getModalBody().html('<span style="font-size:14px">Finalizing in progress...</span>');

                            $.ajax({
                                async: false,
                                url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=setnewperiod&year=" + newy + "&period=" + newp,
                                success: function (result) {
                                    // $("#haha").html(result);
                                    //$('#herey').empty().html(result).hide().fadeIn(300);
                                    if (result == 1) {
                                        setTimeout(function () {
                                            //dialogRef.close();
                                            dialogRef.enableButtons(true);
                                            dialogRef.setClosable(true);
                                            $button.disable();
                                            $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                            dialogRef.getModalBody().html('<i class="fa fa-check right-green"></i>&nbsp;&nbsp;<span style="font-size:14px">Success!</span>');
                                        }, 3000);
                                    } else {
                                        setTimeout(function () {
                                            //dialogRef.close();
                                            dialogRef.enableButtons(true);
                                            dialogRef.setClosable(true);
                                            //$button.disable();
                                            $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                            dialogRef.getModalBody().html('<i class="fa fa-times fail-red"></i>&nbsp;&nbsp;<span style="font-size:14px">Failed!</span>');
                                        }, 3000);
                                    }
                                }
                            });

                        }
                    }, {
                        label: 'Close',
                        action: function (dialogRef) {
                            dialogRef.close();
                        }
                    }]
            });
            return false;
        });



        $("#check-button").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=check",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        console.log(updateTable);
        var orderby = ' order by voucherid desc';
        var oTable = $('#example').dataTable({
            destroy: true,
            "responsive": true,
            "bServerSide": true,
            "sAjaxSource": "retrievetable.jsp?table=" + updateTable + "&code=<%= mod.getReferID_Master()%>&" + colsTosend + "&order=" + orderby,
            "order": [[0, "asc"]],
            "aoColumns": [
                {"sTitle": columnName[0], "mData": columntoView[0], "sWidth": "15%", "bSearchable": true},
                {"sTitle": columnName[1], "mData": columntoView[1], "sWidth": "27%"},
                {"sTitle": columnName[2], "mData": columntoView[2], "sWidth": "15%"},
                {"sTitle": columnName[3], "mData": columntoView[3], "sWidth": "15%"},
                {"sTitle": columnName[4], "mData": columntoView[4], "sWidth": "15%"},
                {"sTitle": "Action", "mData": null, "bSortable": false, "bSearchable": false, "sWidth": "13%"}
            ],
            "aoColumnDefs": [{
                    "aTargets": [5],
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {


                        var a = $('<%= Button.viewlistButton()%>');
                        var b = $('<%= Button.editlistButton()%>');
                        var c = $('<%= Button.deletelistButton()%>');


                        a.on('click', function () {
                            //console.log(oData);
                            BootstrapDialog.show({
                                type: BootstrapDialog.TYPE_DEFAULT,
                                title: 'View <%= mod.getModuleDesc()%>',
                                //message: $('<body></body>').load('list_coa.jsp')
                                message: function (dialog) {
                                    var $content = $('<body></body>').load('cf_coa_view.jsp?id=' + oData.code);
                                    //$('body').on('click', '.thisresult_nd', function(event){
                                    //    dialog.close();
                                    //    event.preventDefault();
                                    //});

                                    return $content;
                                }
                            });
                            return false;
                        });
                        b.on('click', function () {
                            //console.log(oData);

                            $.ajax({
                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=edit&referenceno=" + oData.code,
                                success: function (result) {
                                    // $("#haha").html(result);
                                    $('#herey').empty().html(result).hide().fadeIn(300);
                                }});
                            return false;

                        });
                        c.on('click', function () {

                            BootstrapDialog.confirm({
                                title: 'Confirmation',
                                message: 'Are you sure to delete?',
                                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                closable: true, // <-- Default value is false
                                draggable: true, // <-- Default value is false
                                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                                callback: function (result) {
                                    // result will be true if button was click, while it will be false if users close the dialog directly.
                                    if (result) {
                                        $.ajax({
                                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&referenceno=" + oData.no,
                                            success: function (result) {
                                                // $("#haha").html(result);
                                                setTimeout(function () {
                                                    oTable.fnClearTable();
                                                }, 500);
                                            }});
                                    }
                                }
                            });

                            return false;

                        });

                        $(nTd).empty();
                        $(nTd).attr("class", 'btn-group');
                        $(nTd).prepend(a, b, c);
                        //$(nTd).prepend(t);
                    }

                }]




        });

        $('#godeletex').on('click', function () {
            var v = $('#placeno').html();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&referenceno=" + v,
                success: function (result) {
                    oTable.fnClearTable();
                    $('#deleteModalx').modal('hide');
                }
            });
        });




    });
</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="add-new" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> Add <%= mod.getModuleDesc()%></button>
                        <button id="set-current" class="btn btn-default btn-sm"><i class="fa fa-cog"></i> Set Current <%= mod.getModuleDesc()%></button>
                    </td>
                </tr>
            </table>
            <hr style="color:#333; border:thick">
            <div class="dataTable_wrapper"> 
                <table class="table table-striped table-bordered table-hover" id="example">
                </table>
            </div>
        </div>
    </div>
</div>