<%-- 
    Document   : em_payroll_list_staff
    Created on : Jun 7, 2017, 11:54:55 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.financial.employee.EmPayrollStaffParam"%>
<%@page import="com.lcsb.fms.model.financial.employee.EmPayrollType"%>
<%@page import="com.lcsb.fms.dao.financial.employee.EmPayrollDAO"%>
<%@page import="com.lcsb.fms.dao.setup.company.StaffDAO"%>
<%@page import="com.lcsb.fms.model.setup.company.staff.Staff"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoiceItem"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.model.setup.configuration.Earning"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.EarningDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>

<%
    response.setHeader("Cache-Control", "no-cache");
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    String refer = request.getParameter("refer");


%>
<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        var oTable = $('#list-payslip').DataTable({
            responsive: true,
            "bPaginate": false,
            "aaSorting": [[1, "asc"]],
            "dom": '<"toolbar-1">frtip',
        });

        $("div.toolbar-1").html('<button id="view-list-type-payslip" class="btn btn-primary btn-xs pull-left"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;View </button>');



    });
</script>

<div class="invoice-content">
    <div class="table-responsive">
        <table class="table table-invoice" id="list-payslip">
            <thead>
                <tr>
                    <th>STAFF ID</th>
                    <th>STAFF NAME</th>
                    <th>POSITION</th>
                    <th>EARNING</th>
                    <th>DEDUCTION</th>
                    <th>NET PAY</th>
                    <th>ACTION</th>
                </tr>
            </thead>
            <%List<Staff> listAllx = (List<Staff>) StaffDAO.getAllHQStaff(log);

                if (listAllx.isEmpty()) {%>
            <tbody>
                <tr>
                    <td colspan="5">
                        <span class="font-small-red">No data available.</span>
                    </td>
                </tr>
                <%}

                    for (Staff j : listAllx) {
                        double earn = EmPayrollDAO.getAmountSum(log, refer, j.getStaffid(), "earning", "thismonth");
                        double deduct = EmPayrollDAO.getAmountSum(log, refer, j.getStaffid(), "deduction", "thismonth");
                        boolean b = EmPayrollDAO.getConfirmSlipStatus(log, refer, j.getStaffid());

                %>
                <tr>
                    <td>
                        <%= j.getStaffid()%>
                    </td>
                    <td><span class="pull-left"><%= j.getName()%></span></td>
                    <td><span class="pull-left"><%= j.getPposition()%></span></td>
                    <td><span class="pull-right"><%= GeneralTerm.currencyFormat(earn)%></span></td>
                    <td><span class="pull-right"><%= GeneralTerm.currencyFormat(deduct)%></span></td>
                    <td><span class="pull-right"><%= GeneralTerm.currencyFormat(earn - deduct)%></span></td>

                    <td>


                        <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
                            <button id="confirm-<%= j.getStaffid() %>" class="btn btn-<%= ((b) ? "success" : "default")%> btn-xs pull-left" <%= ((b) ? "disabled" : "")%>><%= ((b) ? "Confirmed" : "Confirm")%></button>
                            <button id="<%= j.getStaffid()%>" class="btn btn-default btn-xs pull-left view-payslip">View Payslip</button>
                        </div>
                    </td>
                </tr>
                <%}%>
            </tbody>
        </table>
    </div>

</div>

