<%-- 
    Document   : ar_inv_finalize
    Created on : Oct 14, 2016, 9:59:54 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.setup.company.MillDAO"%>
<%@page import="com.lcsb.fms.util.dao.EstateDAO"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArTempRefinery"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.model.financial.ar.ArTempMaster"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    ArTempMaster am = (ArTempMaster) SalesInvoiceDAO.getAllInfo(log, request.getParameter("sessionid"));
%>
<!DOCTYPE html>
<table class="table table-bordered table-striped  table-hover">
    <thead>
        <tr>
            <th colspan="4">List of Invoice
                <!--<small><span class="label label-primary">Preparing</span></small>-->

            </th>
        </tr>
    </thead>
    <tbody class="activerowm tdrow">
        <tr>
            <th width="30%">Product</th>
            <td width="70%"><%= request.getParameter("prodcode")%> - <%= request.getParameter("prodname")%></td>
        </tr>
        <tr>
            <th>Account</th>
            <td><%= EstateDAO.getEstateInfo(log, MillDAO.getDefaultMill(log).getCode(), "estatecode").getHqsuspence()%> - <%= EstateDAO.getEstateInfo(log, MillDAO.getDefaultMill(log).getCode(), "estatecode").getHqsuspencedescp()%></td>
        </tr>
        <tr>
            <th>Location</th>
            <td>Company / <%= log.getEstateCode()%> / <%= log.getEstateDescp()%></td>
        </tr>
        <tr>
            <th>Contract No</th>
            <td><%= am.getContractno()%></td>
        </tr>
        <tr>
            <th>Price</th>
            <td><%= GeneralTerm.currencyFormat(am.getUnitprice())%></td>
        </tr>      

    </tbody>
</table>

<div class="invoice-content">
    <div class="table-responsive">
        <table class="table table-invoice">
            <thead>
                <tr>
                    <th>DISPATCH DESCRIPTION</th>
                    <th><span class="pull-right">QUANTITY</span></th>
                    <th><span class="pull-right">TOTAL</span></th>
                    <th><span class="pull-right">TAX</span></th>
                    <th><span class="pull-right">TOTAL WITH TAX</span></th>
                </tr>
            </thead>
            <tbody>

                <%List<ArTempRefinery> listAll = (List<ArTempRefinery>) am.getListDispatch();
                    double totTax = 0.0;
                    double totWithoutTax = 0.0;
                    double totGrand = 0.0;
                    double totQty = 0.0;
                    int i = 0;
                    for (ArTempRefinery j : listAll) {
                        i++;
                        totQty += j.getMtRefinery();
                        totTax += 6 * (j.getMtRefinery() * am.getUnitprice()) / 100;
                        totWithoutTax += j.getMtRefinery() * am.getUnitprice();
                        totGrand += (6 * (j.getMtRefinery() * am.getUnitprice()) / 100) + (j.getMtRefinery() * am.getUnitprice());
                %>
                <tr>
                    <td>
                        Dispatch No : <%= j.getDispatchno()%><br />
                        <small>Date : <%= j.getDate()%>, Trailer : <%= j.getTrailer()%></small>
                    </td>
                    <td><span class="pull-right"><%= j.getMtRefinery()%></span></td>
                    <td><span class="pull-right"><%= GeneralTerm.currencyFormat(j.getMtRefinery() * am.getUnitprice())%></span></td>
                    <td><span class="pull-right"><%= GeneralTerm.currencyFormat(6 * (j.getMtRefinery() * am.getUnitprice()) / 100)%></span></td>
                    <td><span class="pull-right"><%= GeneralTerm.currencyFormat((6 * (j.getMtRefinery() * am.getUnitprice()) / 100) + (j.getMtRefinery() * am.getUnitprice()))%></span></td>
                </tr>
                <%}%>

            </tbody>
            <thead>
                <tr>
                    <th>TOTAL</th>
                    <th><span class="pull-right"><%= GeneralTerm.currencyFormat( totQty )%></span></th>
                    <th><span class="pull-right"><%= GeneralTerm.currencyFormat( totWithoutTax )%></span></th>
                    <th><span class="pull-right"><%= GeneralTerm.currencyFormat( totTax )%></span></th>
                    <th><span class="pull-right"><%= GeneralTerm.currencyFormat( totGrand )%></span></th>
                </tr>
            </thead>
        </table>
    </div>
    
</div>

