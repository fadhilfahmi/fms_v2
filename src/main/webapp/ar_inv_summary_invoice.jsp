<%-- 
    Document   : ar_inv_summary_invoice
    Created on : Apr 7, 2017, 2:35:38 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.general.DataTable"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoice"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) SalesInvoiceDAO.getModule();
%>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $(".auth_inv").click(function (e) {
            e.preventDefault();
            var a = $(this).attr('id');
            var b = $(this).attr('name');


            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=printpreview&refer=" + a + "&auth=" + b,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });



    });
</script>
<div class="invoice-content">
    <div class="table-responsive">
        <table class="table table-invoice">
            <thead>
                <tr>
                    <th>DETAIL</th>
                    <th><span class="pull-left">DATE</span></th>
                    <th><span class="pull-right">AMOUNT</span></th>
                    <th><span class="pull-right">ACTION</span></th>
                </tr>
            </thead>
            <tbody>

                <%  String typeAuth = "";
                    String eventAuth = "";

                    if (log.getListUserAccess().get(1).getAccess()) {
                        typeAuth = "check";
                        eventAuth = "Check";

                    }
                    if (log.getListUserAccess().get(2).getAccess()) {
                        typeAuth = "approval";
                        eventAuth = "Approve";
                    }
                    
                    DataTable prm = new DataTable();
                    prm.setViewBy(typeAuth);
                    List<SalesInvoice> listAll = (List<SalesInvoice>) SalesInvoiceDAO.getAll(log, prm);
                    if (listAll.isEmpty()) {%>
                <tr>
                    <td colspan="6">
                        <span class="font-small-red">No data available.</span>
                    </td>
                </tr>

                <%}
                    for (SalesInvoice j : listAll) {
                %>
                <tr>
                    <td>
                        <%= j.getBname()%><br />
                        <small><%= j.getInvref()%></small>
                    </td>
                    <td><span class="pull-left"><%= j.getInvdate()%></span><br />
                        <small></small></td>
                    <td><span class="pull-right"><%= GeneralTerm.currencyFormat(j.getAmountno())%></span></td>
                    <td>

                        <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
                            <button id="<%= j.getInvref()%>" class="btn btn-success btn-xs auth_inv" title="<%= mod.getModuleID()%>" name="<%=eventAuth%>"><%=eventAuth%></button>

                        </div>
                    </td>
                </tr>
                <%}%>

            </tbody>
        </table>
    </div>

</div>
