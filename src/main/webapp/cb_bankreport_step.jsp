<%-- 
    Document   : ac_lot_activity_step
    Created on : Dec 19, 2016, 10:27:37 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.financial.cashbook.BankReportParam"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.BankReportDAO"%>
<%@page import="com.lcsb.fms.dao.management.activity.LotActivityDAO"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    BankReportParam prm = (BankReportParam) session.getAttribute("br_param");
    Module mod = (Module) BankReportDAO.getModule();
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>

<link href="bootstrap-form-wizard/form-wizard.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });
        //$('#JVdate').datepicker({ dateFormat: 'yyyy-mm-dd' });

        $('#JVdate').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        });

        $('#saveall').click(function (e) {
            //e.preventDefault();
            var vouchertype = $('#vouchervalue').val();
            var sessionid = $('#sessionid').val();
            $.ajax({
                url: "PathController?moduleid=020207&process=savevoucher&sessionid=" + sessionid + "&type=" + vouchertype,
                success: function (result) {
                    $('#maincontainer').remove();
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $('#getaccount').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Chart of Account',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_coa.jsp');
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                    });

                    return $content;
                }
            });

            return false;
        });



        $('#getbuyer').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Buyer Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_buyer_mill.jsp?name=bname&code=bcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getproduct').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Product Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_product.jsp?name=prodname&code=prodcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn');

        allWells.hide();
        var ch = 0;
        navListItems.click(function (e) {
            ch++;
            e.preventDefault();
            var $target = $($(this).attr('href')),
                    $item = $(this);
            //console.log($target);
            //console.log($item);
            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url']"),
                    isValid = true;
                    
                    var ty = $('#typevalue').val();
                    var voucherty = $('#vouchervalue').val();

            if (curStepBtn == 'step-1') {
                
                
                
                if(ty=='hibah'){
                    
                    if($('#pv').hasClass('disabled')){
                        console.log($('#pv').hasClass('disabled'));
                        $("#pv").addClass('thisresult-voucher');
                        $("#pv").removeClass('disabled');
                    }else{
                        console.log($('#pv').hasClass('disabled'));
                        $("#pv").removeClass('thisresult-voucher');
                        $("#pv").addClass('disabled');
                    }
                    
                }else if(ty=='bankcharges'){
                    
                    if($('#pv').hasClass('disabled')){
                        console.log($('#pv').hasClass('disabled'));
                        $("#pv").addClass('thisresult-voucher');
                        $("#pv").removeClass('disabled');
                    }
                    
                }
                

            }

            if (curStepBtn == 'step-2') {

            }

            if (curStepBtn == 'step-3') {

                var bankcode = $('#bankcode').val();
                
                var pathvoucher = '';
                console.log(voucherty);
                if(voucherty=='pv'){
                    pathvoucher = 'cb_bankreport_preparevoucher';
                }else if(voucherty=='or'){
                    pathvoucher = 'cb_bankreport_preparereceipt';
                }

                $.ajax({
                    async: true,
                    url: pathvoucher+".jsp?bankcode=" + bankcode,
                    success: function (result) {
                        console.log('table-load');
                        $('#prepare-voucher').empty().html(result).hide().fadeIn(300);

                    }


                });

            }

            if (curStepBtn == 'step-4') {

                var sessionid = $('#sessionid').val();
                var bankcode = $('#bankcode').val();
                var amount = $('#amount').val();

                var checkSave = false;
                var pathtemp1 = '';
                var pathtemp2 = '';
                
                if(voucherty=='pv'){
                    pathtemp1 = 'tempmaster';
                    pathtemp2 = 'tempamount';
                }else if(voucherty=='or'){
                    pathtemp1 = 'tempmasterOR';
                    pathtemp2 = 'tempamountOR';
                }

                var a = $("#savemaster :input").serialize();
                $.ajax({
                    data: a,
                    async: false,
                    type: 'POST',
                    url: "ProcessController?moduleid=020207&process="+pathtemp1+"&sessionid=" + sessionid,
                    success: function (result) {
                        //$('#maincontainer').empty().html(result).hide().fadeIn(300);
                        if (result == 1) {
                            console.log('Saved temporary master.');
                            checkSave = true;
                        } else {
                            console.log('Failed to save temporary master.');
                        }

                    }
                });


                $.ajax({
                    async: false,
                    type: 'POST',
                    url: "ProcessController?moduleid=020207&process="+pathtemp2+"&sessionid=" + sessionid + "&bankcode=" + bankcode + "&amount=" + amount,
                    success: function (result) {
                        //$('#maincontainer').empty().html(result).hide().fadeIn(300);
                        if (result == 1) {
                            console.log('Saved temporary account.');
                            checkSave = true;
                        } else {
                            console.log('Failed to save temporary account.');
                        }
                    }
                });

                if (checkSave) {

                    $.ajax({
                        async: true,
                        url: "cb_bankreport_finalize.jsp?sessionid=" + sessionid + "&type=" + voucherty,
                        success: function (result) {
                            console.log('table-load');
                            $('#finalize').empty().html(result).hide().fadeIn(300);
                            window.scrollTo(0,0);

                        }
                    });

                }




            }

            if (curStepBtn == 'step-5') {
                alert('Done');
            }


            $(".form-group").removeClass("has-error");

            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].value) {//modification here from original
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.btn-primary').trigger('click');

        $('body').on('click', '.activerowy', function (e) {
            $('.refineryBtn').trigger('click');
            return false;
        });

        $('.getlot').click(function (e) {

            var a = 'lotcode';
            var b = 'lotdesc';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Estate',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_lot.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });


        $('.currency-format').keyup(function (e) {
            console.log(2323);
            var a = $(this).val();
            a = a.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

            $(this).val(a);

            return false;
        });

        $('.thisresult-type').click(function (e) {

            e.preventDefault();
            var typevalue = $(this).attr('id');
            $('#typevalue').val(typevalue);
            $('.typeBtn').trigger('click');
            return false;
        });

        //$('.thisresult-voucher').click(function (e) {
            $('body').on('click', '.thisresult-voucher', function (e) {

            e.preventDefault();
            var typevalue = $(this).attr('id');
            $('#vouchervalue').val(typevalue);
            $('.step2-Btn').trigger('click');
            return false;
        });


    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewbankreport"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>

                    </td>
                </tr>
            </table>
            <br>

            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step" width="20%">
                        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                        <p>Type of Transaction</p>
                    </div>
                    <div class="stepwizard-step" width="20%">
                        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                        <p id="step-2-title">Type of Voucher</p>
                    </div>
                    <div class="stepwizard-step" width="20%">
                        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                        <p id="step-3-title">Amount & Period</p>
                    </div>
                    <div class="stepwizard-step" width="20%">
                        <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                        <p>Prepare Voucher</p>
                    </div>
                    <div class="stepwizard-step" width="20%">
                        <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                        <p>Finalizing</p>
                    </div>
                </div>
            </div>




            <form data-toggle="validator" role="form" id="savelot">
                <input type="hidden" id="typevalue" value="">
                <input type="hidden" id="vouchervalue" value="">
                <input type="hidden" id="sessionid" name="sessionid" value="<%= request.getParameter("sessionid")%>">
                <input type="hidden" id="bankcode" name="bankcode" value="<%= request.getParameter("bankcode")%>">



                <div class="row setup-content" id="step-1">
                    <div class="col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Choose Type of Transaction
                            </div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="list-group">
                                        <a href="" id="hibah" title="" class="list-group-item thisresult-type" >
                                            <div id="left_div"><%//= c.getSuppcode()%></div>
                                            <div id="right_div">Hibah<span class="badge pull-right"><%//= c.getTotalInvoice() %></span></div>
                                        </a>
                                        <a href="" id="bankcharges"   title="" class="list-group-item thisresult-type">
                                            <div id="left_div"><%//= c.getSuppcode()%></div>
                                            <div id="right_div">Bank Charges<span class="badge pull-right"><%//= c.getTotalInvoice() %></span></div>
                                        </a>
                                    </div>
                                    <button class="btn btn-default nextBtn btn-sm pull-right typeBtn" type="button" disabled>Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row setup-content" id="step-2">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Choose Type of Voucher
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="list-group">
                                    <a id="pv"   title="" class="list-group-item thisresult-voucher">
                                        <div id="left_div"><%//= c.getSuppcode()%></div>
                                        <div id="right_div">Payment Voucher<span class="badge pull-right"><%//= c.getTotalInvoice() %></span></div>
                                    </a>
                                    <a id="or"   title="" class="list-group-item thisresult-voucher">
                                        <div id="left_div"><%//= c.getSuppcode()%></div>
                                        <div id="right_div">Official Receipt<span class="badge pull-right"><%//= c.getTotalInvoice() %></span></div>
                                    </a>
                                </div>
                                <button class="btn btn-default nextBtn step2-Btn btn-sm pull-right" type="button" disabled >Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form data-toggle="validator" role="form" id="saveinput">
                <div class="row setup-content" id="step-3">
                    <div class="col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Please enter the following input
                            </div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">Year</label>
                                                <select class="form-control input-sm" id="curyear" name="year">
                                                    <%= ParameterDAO.parameterList(log,"Year", prm.getYear())%>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">Period</label>
                                                <select class="form-control input-sm" id="curperiod" name="period">
                                                    <%= ParameterDAO.parameterList(log,"Period", prm.getPeriod())%>
                                                </select> 
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="inputName" class="control-label">Amount</label>
                                                <input type="text" class="form-control input-sm" id="amount" name="amount" autocomplete="off" >
                                            </div>
                                        </div>



                                    </div>
                                    <button class="btn btn-default nextBtn btn-sm pull-right" type="button" >Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row setup-content" id="step-4">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Preparing Voucher
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div id="prepare-voucher">
                                    <div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>

                                </div>
                                <button class="btn btn-default nextBtn btn-sm pull-right" type="button" >Next</button>
                            </div>
                        </div></div></div>
            </div>
            <div class="row setup-content" id="step-5">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Finalizing
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div id="finalize">
                                    <div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>
                                </div>
                                <button class="btn btn-success btn-sm pull-right" id="saveall" type="submit">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
