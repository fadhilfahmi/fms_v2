<%-- 
    Document   : remote_dateperiod
    Created on : May 18, 2017, 12:05:43 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    //Module mod = (Module) FinancialReportDAO.getModule();
%>
<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<style>
.datepicker{z-index:1151 !important;}
</style>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });
        //$('#JVdate').datepicker({ dateFormat: 'yyyy-mm-dd' });

        $('#date').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        }).change(dateChanged)
                .on('changeDate', dateChanged);

        

    });
</script>



<form data-toggle="validator" role="form" id="saveform">
    <div class="row">
        <div class="col-lg-12">
            <label for="inputName" class="control-label">Please confirm Date and Period for new replicated data.</label>
            <div class="well">
                <div class="row">
                    <div class="col-sm-4">
                        <label for="inputName" class="control-label">Date</label>
                        <input type="text" class="form-control input-sm" id="date" name="replicatedate" autocomplete="off" value="<%//= v.getDate()%>"> 
                    </div>
                    <div class="col-sm-4">
                        <label for="inputName" class="control-label">Year</label>
                        <div class="form-group">
                            <select class="form-control input-sm" id="year" name="replicateyear" readonly>
                                <%= ParameterDAO.parameterList(log, "Year", AccountingPeriod.getCurYear(log))%>
                            </select>
                        </div>   
                    </div>
                    <div class="col-sm-4">
                        <label for="inputName" class="control-label">Period</label>
                        <select class="form-control input-sm" id="period" name="replicateperiod" readonly>
                            <%= ParameterDAO.parameterList(log, "Period", AccountingPeriod.getCurPeriod(log))%>
                        </select>  
                    </div>
                </div>


            </div>
        </div>
    </div>
</form>

