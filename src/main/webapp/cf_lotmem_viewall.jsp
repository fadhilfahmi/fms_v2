<%-- 
    Document   : cf_lotmem_viewall
    Created on : Dec 7, 2016, 11:47:06 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.setup.configuration.LotMember"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.LotMemberDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) LotMemberDAO.getModule();


%>
<link href="css/table_style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        window.scrollTo(0, 0);

        var oTable = $('#list').DataTable( {
            responsive: true,
            "bPaginate": false,
            "aaSorting": [[ 0, "desc" ]],
	} );

        $('#print').click(function (e) {
            printElement($("#viewjv").html());
        });

        

        $('.activerowm').click(function (e) {
            e.preventDefault();
            $(".togglerow").toggle();
            //$(this).toggleClass('info');
            //$("#togglerow_nd"+b).toggleClass('warning');
        });

        $('.activerowx').click(function (e) {
            e.preventDefault();
            var b = $(this).attr('id');
            $("#togglerow_nd" + b).toggle();
            $(this).toggleClass('info');
            $("#togglerow_nd" + b).toggleClass('warning');
        });

        $('.activerowy').click(function (e) {
            e.preventDefault();
            var b = $(this).attr('id');
            $("#togglerow_st" + b).toggle();
            $(this).toggleClass('info');
            $("#togglerow_st" + b).toggleClass('warning');
        });

        $(".approve").click(function () {
            //var a = $("form").serialize();
            var a = $(this).attr('id');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are you sure to Approve this?',
                type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Approve', // <-- Default value is 'OK',
                btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=dist&referno=" + a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            return false;
        });




        $('.td-click').click(function (e) {

            var b = $(this).attr('id');
            e.preventDefault();

            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                size: BootstrapDialog.SIZE_WIDE,
                //animate: false,
                title: 'Member Code ' + b,
                message: function (dialog) {
                    var $content = $('<body></body>').load('cf_lotmem_editpopup.jsp?memcode=' + b);
                    //$('body').on('click', '.thisresult_nd', function(event){
                    //    dialog.close();
                    //    event.preventDefault();
                    //});
                    return $content;
                },
                buttons: [{
                        //icon: 'fa fa-circle-o-notch',
                        label: 'Update & Close',
                        cssClass: 'btn-primary',
                        autospin: true,
                        action: function (dialogRef) {
                            var $button = this;
                            dialogRef.enableButtons(false);
                            dialogRef.setClosable(false);
                            //dialogRef.getModalBody().html('<span style="font-size:14px">Finalizing in progress...</span>');
                            var a = $("#saveform :input").serialize();
                            var code = $("#code").val();
                            var name = $("#name").val();
                            var ic = $("#ic").val();
                            var status = $("#status").val();
                            var payment = $("#payment").val();
                            var bank = $("#bank").val();
                            var accno = $("#accno").val();
                            var address1 = $("#address1").val();
                            var address2 = $("#address2").val();
                            var address3 = $("#address3").val();
                            var address4 = $("#address4").val();
                            var address5 = $("#address5").val();
                            $.ajax({
                                async: true,
                                data: a,
                                type: 'POST',
                                url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=updatememberinfo",
                                success: function (result) {
                                    // $("#haha").html(result);
                                    //$('#herey').empty().html(result).hide().fadeIn(300);
                                    if (result == 1) {
                                        setTimeout(function () {
                                            //dialogRef.close();
                                            dialogRef.enableButtons(true);
                                            dialogRef.setClosable(true);
                                            $button.disable();
                                            $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                            dialogRef.getModalBody().html('<i class="fa fa-check right-green"></i>&nbsp;&nbsp;<span style="font-size:14px">Success!</span>');
                                            
                                            
                                            $('#name'+code).html(name);
                                            $('#ic'+code).html(ic);
                                            $('#status'+code).html(status);
                                            $('#bank'+code).html(bank);
                                            $('#accno'+code).html(accno);
                                            $('#payment'+code).html(payment);
                                            $('#address'+code).html(address1 + ', ' + address2 + ', ' + address3 + ', ' + address4 + ', ' + address5);
                                            console.log();
                                            dialogRef.close();
                                            //$("#"+code).effect("pulsate", { times:3 }, 2000);
                                        }, 1000);
                                    } else {
                                        setTimeout(function () {
                                            //dialogRef.close();
                                            dialogRef.enableButtons(true);
                                            dialogRef.setClosable(true);
                                            //$button.disable();
                                            $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                            dialogRef.getModalBody().html('<i class="fa fa-times fail-red"></i>&nbsp;&nbsp;<span style="font-size:14px">Failed!</span>');
                                        }, 3000);
                                    }
                                }
                            });

                        }
                    }, {
                        label: 'Close',
                        action: function (dialogRef) {
                            dialogRef.close();
                        }
                    }]
            });

            /* BootstrapDialog.show({
             type:BootstrapDialog.TYPE_DEFAULT,
             title: 'Finalizing Account',
             message: function(dialog) {
             var $content = $('<body></body>').load('gl_accountperiod_set.jsp?');
             //$('body').on('click', '.thisresult_nd', function(event){
             //    dialog.close();
             //    event.preventDefault();
             //});
             return $content;
             }
             });*/
        });



    });
</script>

<div id ="maincontainer">
    <div class="partition">
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
                </tr>
            </table>
            <br>

               
                <table class="table-bordered table-striped table-hover" width="100%" id="list">
                    <thead>

                        <tr>
                            <th style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;" width="3%">#</th>
                            <th style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">Code</th>
                            <th style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;" width="18%">Name</th>
                            <th style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">IC</th>
                            <th style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;" width="25%">Address</th>
                            <th style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">Bank</th>
                            <th style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">Account No</th>
                            <th style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">Payment</th>
                            <th style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">Status</th>
                            <th style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;" width="8%">Tel</th>
                        </tr>
                    </thead>
                    <tbody>

                        <%
                            List<LotMember> listItem = (List<LotMember>) LotMemberDAO.getAllMember(log,"", "");
                            int o = 0;
                            for (LotMember i : listItem) {
                                o++;
                        %>

                        <tr class="td-click" id="<%= i.getMemCode()%>">
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= o%></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getMemCode()%></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left" id="name<%=i.getMemCode()%>"><%= i.getMemName()%></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left" id="ic<%=i.getMemCode()%>" ><%= i.getIc()%></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left" id="address<%=i.getMemCode()%>"><%= i.getAddress1()%>, <%= i.getAddress2()%>, <%= i.getAddress3()%>, <%= i.getAddress4()%>, <%= i.getAddress5()%></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left" id="bank<%=i.getMemCode()%>"><%= i.getBank()%></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left" id="accno<%=i.getMemCode()%>"><%= i.getAcc()%></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left" id="payment<%=i.getMemCode()%>"><%= i.getPaymethod()%></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left" id="status<%=i.getMemCode()%>"><%= i.getStatus()%></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-right" id="notel<%=i.getMemCode()%>"><%= i.getNotel()%></td>
                        </tr>


                        <%}%>


                    </tbody>
                </table>
        </div>
    </div>
</div>    
