<%-- 
    Document   : dev_post
    Created on : Jul 5, 2017, 10:22:39 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.lcsb.fms.dao.development.PostTransDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
%>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });

        $('#go').click(function (e) {
            var refer = $('#refer').val();

            $.ajax({
                url: "dev_unapprove_viewgl.jsp?refer=" + refer,
                success: function (result) {
                    $('#rendergl').empty().html(result).hide().fadeIn(300);
                    $('#unapprove').prop('disabled', false);

                }});

            return false;
        });

        $('#postall').click(function (e) {

            $.ajax({
                async: false,
                url: "ProcessController?moduleid=000000&process=postall",
                success: function (result) {
                    if (result == 0) {
                        $('#rendergl').empty().html('<h1>Successful Unapprove & Unpost.</h1>').hide().fadeIn(300);
                    } else {
                        $('#rendergl').empty().html('<h1>Successful Unapprove.</h1>').hide().fadeIn(300);
                    }

                }
            });

            return false;
        });

        $('.viewtrans').click(function (e) {

            var a = 'code';
            var b = 'descp';
            var modid = $(this).attr('id');

            e.preventDefault();
            if (e.handled !== true) { //Checking for the event whether it has occurred or not.
                e.handled = true; // Basically setting value that the current event has occurred.
                
                $.ajax({
                url: "PathController?moduleid=080303&process=viewtrans",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;


            }
        });


    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <div class="well-filter">
                <div class="row">
                    <div class="col-sm-6">

                        <div class="form-group" id="div_code">

                            <button id="unapprove"  class="btn btn-default btn-sm" title="<%//= mod.getModuleID()%>" name="unpprove">Check Unpost Transaction</button>
                            <button id="postall"  class="btn btn-default btn-sm" title="<%//= mod.getModuleID()%>" name="unpprove">Post All</button>

                        </div>
                    </div>
                    <div class="col-sm-6">


                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12" id="rendergl">

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <%
                        ArrayList<String> list = PostTransDAO.listModule(log);

                        for (int i = 0; i < list.size(); i++) {

                    %><p><label for="inputName" class="control-label"><%= ModuleDAO.getModule(log, list.get(i)).getModuleDesc()%><span class="res_code"> - <%= PostTransDAO.totalUnpost(log, list.get(i))%></span></label>&nbsp;&nbsp; <a href="#" id="<%=list.get(i)%>" class="viewtrans icon-link">View</a> <a>Post</a></p><%

                        if (i == list.size() - 1) {

                        } else {

                        }

                    %>

                    <div class="row">
                        <div class="col-sm-12" id="content-<%=list.get(i)%>">

                        </div>
                    </div>

                    <%                                }
                    %>
                </div>
            </div>
        </div>
    </div>
</div>