<%-- 
    Document   : gl_fr_trial_balance_view
    Created on : Feb 25, 2017, 12:10:14 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.report.financial.trialbalance.TBalDetail"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.report.financial.trialbalance.TBalMaster"%>
<%@page import="com.lcsb.fms.report.financial.trialbalance.TBalDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.report.financial.trialbalance.TBParam"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    TBParam prm = (TBParam) session.getAttribute("tb_param");
    TBalMaster tb = (TBalMaster) TBalDAO.getMaster(log,prm);
    Module mod = (Module) TBalDAO.getModule();


%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#print').click(function (e) {
            printReport($("#viewprint").html());
        });

        $(".goto").click(function () {
            var link = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%//=moduleid%>&process=" + link,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#maincontainer').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


    });
</script>
<style>
    .header1{
        border-color:#000;
        border-style: solid; 
        border-width: 1px;
        font-family:Arial, Helvetica, sans-serif;
        font-size:10px;
        font-weight:300;
    }
    .data1{

        font-family:Arial, Helvetica, sans-serif;

        font-size:12px
    }	
    .data2{

        font-family:Arial, Helvetica, sans-serif;

        font-size:9px
    }
    .aright{
        text-align:right}
    thead {display: table-header-group; }
    tfoot {display: table-footer-group; }
    thead th, thead td {position: static; }

    @media print
    {    
        .no-print, .no-print *
        {
            display: none !important;
        }
    }			
</style>
<link href="css/table_style.css" rel="stylesheet" type="text/css" />
<link href="css/pvoucher.css" rel="stylesheet" type="text/css" />
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="trialbalance"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="print"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addinv" ><i class="fa fa-floppy-o"></i>&nbsp;Print</button>
                    </td>
                </tr>
            </table>
            <br>
            <div id="viewprint">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td colspan="4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="50px"><img src="image/logo_lkpp.jpg" width="40px" style="vertical-align:middle" ></td>
                                    <td><span class="bigtitlebold"><%= log.getEstateDescp()%></span><span class="pull-right"></span>
                                        <p class="font_inheader_normal"><%= tb.getTbtitle()%></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="border_bottom"></td>
                    </tr>
                </table>
                <table class="table-striped table-hover" width="100%">
                    <thead>

                        <tr>
                            <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;" width="15%">Account Code</th>
                            <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">Account Description</th>
                            <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">Sub</th>
                            <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">Stage</th>
                            <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">Enterprise</th>
                            <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">Debit</th>
                            <th class="table-bordered" style="font-size: 10px !important; padding-left: 6px; padding-right: 6px;">Credit</th>
                    </thead> 
                    <tbody> 
                        <%
                            List<TBalDetail> listi = (List<TBalDetail>) tb.getListCore();
                            for (TBalDetail i : listi) {
                        %>
                        <tr> 
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getAcccode()%></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getAccdesc()%></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getSatype()%> / <%= i.getSacode()%> / <%= i.getSadesc()%></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getStgcode()%></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"><%= i.getEntcode()%></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-right"><%= GeneralTerm.currencyFormat(i.getDebit())%></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-right"><%= GeneralTerm.normalCredit(i.getCredit())%></span></td>
                        </tr>

                        <%
                            }%>

                        <tr> 
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left">&nbsp;</span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-left"></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-right"><strong><%= GeneralTerm.normalCredit(tb.getTotalDebit()) %></strong></span></td>
                            <td style="font-size: 10px !important; padding: 2px; padding-left: 6px; padding-right: 6px;"><span class="pull-right"><strong><%= GeneralTerm.normalCredit(tb.getTotalCredit()) %></strong></span></td>
                            
                        </tr>

                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>