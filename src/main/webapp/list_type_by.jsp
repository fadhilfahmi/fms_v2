<%-- 
    Document   : list_location_by
    Created on : Mar 2, 2016, 11:32:25 AM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.TypeDAO"%>
<%@page import="com.lcsb.fms.util.model.Type"%>
<%@page import="com.lcsb.fms.util.dao.LocationDAO"%>
<%@page import="com.lcsb.fms.util.model.Location"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    %>
<!DOCTYPE html>
<div class="list-group"><%
            
            String by = request.getParameter("by");
            String keyword = request.getParameter("keyword");
            
            List<Type> slist = (List<Type>) TypeDAO.getType(log, by,keyword);
            for (Type c : slist) { 
               %>
       
            <a href="<%= c.getDescp()%>" id="<%= c.getCode()%>" id1="<%= c.getAddress()%>" id2="<%= c.getCity()%>" id3="<%= c.getPostcode()%>" id4="<%= c.getState()%>" id5="<%= c.getGstid()%>" class="list-group-item thisresult_nd">
                <div id="left_div"><%= c.getCode()%></div>
                <div id="right_div"><%= c.getDescp()%></div>
            </a>

               <%    
            } 
        %>
</div>
