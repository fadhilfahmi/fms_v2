<%-- 
    Document   : co_staff_edit_list
    Created on : Dec 14, 2016, 10:31:35 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.setup.company.staff.StaffDesignation"%>
<%@page import="com.lcsb.fms.model.setup.company.staff.Staff"%>
<%@page import="com.lcsb.fms.dao.setup.company.StaffDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
//PaymentVoucherItem vi = (PaymentVoucherItem) VendorPaymentDAO.getPYVitem(request.getParameter("refer"));
    Staff v = (Staff) StaffDAO.getInfo(log, request.getParameter("referno"));
    Module mod = (Module) StaffDAO.getModule();
    
    
    String tab = request.getParameter("tab");
    if(tab==null){
        tab = "viewbasic";
    }

//String status = PaymentVoucherDAO.getStatus(v.getRefer());
%>

<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        window.scrollTo(0, 0);

        callPnotify('<%= request.getParameter("status")%>', '<%= request.getParameter("text")%>');
        
        


        $('#print').click(function (e) {
            printElement($("#viewjv").html());
        });
        var oTable = $('#list-bank').DataTable({
            responsive: true,
            "aaSorting": [[0, "desc"]]
        });

        $('.viewvoucher').click(function (e) {
            var a = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewpv&refer=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".editmain").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editmain&referno=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".edititem").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('name');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + b + "&refer=" + a,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $(".deleteitem").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('name');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are you sure to delete?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=" + b + "&referno=" + a,
                            success: function (result) {
                                oTable.row('#' + a).remove().draw(false);
                            }
                        });
                    }
                }
            });

            return false;
        });

        $(".deletemain").click(function () {
            var a = $(this).attr('id');
            var b = $(this).attr('href');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'All datas for this Official Receipt will be permanently deleted. Are you sure to proceed?',
                type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Delete', // <-- Default value is 'OK',
                btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&referno=" + a,
                            success: function (result) {
                                $('#herey').empty().html(result).hide().fadeIn(300);
                            }
                        });
                    }
                }
            });
            return false;
        });

        $("#addcredit").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addcredit&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $(".add-st").click(function (e) {
            e.preventDefault();
            var refer = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addlot&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });





        $('.editbasis').click(function (e) {
            var b = $(this).attr('id');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editbasis&referno=" + b,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });

        $('#tab_content').on('click', '.adddesignation', function (e) {
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=adddesignation&refer=" + id,
                success: function (result) {
                    $('#tab_content').empty().html(result).hide().fadeIn(100);
                }
            });
            return false;
        });
        
        $('#tab_content').on('click', '.addbank', function (e) {
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addbank&refer=" + id,
                success: function (result) {
                    $('#tab_content').empty().html(result).hide().fadeIn(100);
                }
            });
            return false;
        });
        
        
        $('#tab_content').on('click', '.addspouse', function (e) {
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addspouse&refer=" + id,
                success: function (result) {
                    $('#tab_content').empty().html(result).hide().fadeIn(100);
                }
            });
            return false;
        });
        
        $('#tab_content').on('click', '.addchild', function (e) {
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addchild&refer=" + id,
                success: function (result) {
                    $('#tab_content').empty().html(result).hide().fadeIn(100);
                }
            });
            return false;
        });
        
        

        $.ajax({
            async: true,
            url: "PathController?moduleid=<%= mod.getModuleID()%>&process=<%=tab%>&refer=<%= v.getStaffid()%>",
            success: function (result) {
                $('#tab_content').empty().html(result).hide().fadeIn(300);
                $('#<%=tab%>').addClass('btn-primary').removeClass('btn-default');
            }
        });



        $(".tochange").click(function (e) {
            $('.tochange').addClass('btn-default').removeClass('btn-primary');
            $(this).addClass('btn-primary').removeClass('btn-default');
            var process = $(this).attr('id');
            var id = $('#staff-id').val();
            console.log(id);
            e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process="+process+"&refer=" + id,
                success: function (result) {
                    $('#tab_content').empty().html(result).hide().fadeIn(100);
                }
            });
            return false;
        });

        /*$('#tab_content').one('click', '#backtolist', function (e) {
            var path = $(this).attr('title');
            var process = $(this).attr('name');
            var refer = $(this).attr('type');
            $.ajax({
                url: "PathController?moduleid=" + path + "&process=" + process + "&refer=" + refer,
                success: function (result) {
                    // $("#haha").html(result);
                    $('#tab_content').empty().html(result).hide().fadeIn(100);
                }
            });
            return false;
        });*/
        
        //$('#getworktype').click(function (e) {
            $('#tab_content').on('click', '#getworktype', function (e) {
            //e.preventDefault();

            var a = 'dept_code';
            var b = 'dept_descp';

            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Work Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_workattype.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        e.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });



    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
                </tr>
            </table>
            <br>


            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
<div id="filterdiv" class="well"> 
                        <div class="row">
                            <div class="col-md-6">
                                <input type="hidden" id="staff-id" value="<%= request.getParameter("referno") %>">
                                <button id="viewbasic" class="tochange btn btn-default btn-sm"><i class="fa fa-user" aria-hidden="true"></i> Basic</button>
                                <button id="viewdesignation" class="tochange btn btn-default btn-sm"><i class="fa fa-briefcase" aria-hidden="true"></i> Designation</button>
                                <button id="viewspouse" class="tochange btn btn-default btn-sm"><i class="fa fa-female" aria-hidden="true"></i> Spouse</button>
                                <button id="viewchild" class="tochange btn btn-default btn-sm"><i class="fa fa-child" aria-hidden="true"></i> Children</button>
                                <button id="viewbank" class="tochange btn btn-default btn-sm"><i class="fa fa-university" aria-hidden="true"></i> Bank</button>
                                <button id="viewdeduct" class="tochange btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> Deduction</button>
                            </div>
                        </div>
</div>
                    <br>
                        <div class="x_panel">
                        
                        <div class="row">
                                <div id="tab_content" style="opacity:100 !important;">

                                    <!-- start tab content1 -->

                                    <!-- end tab content1 -->

                                </div>
                        </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>









