<%-- 
    Document   : cb_official_view
    Created on : Mar 21, 2016, 11:22:17 AM
    Author     : Dell
--%>


<%@page import="com.lcsb.fms.model.financial.cashbook.CbOfficialCek"%>
<%@page import="com.lcsb.fms.util.ext.Restriction"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.OfficialReceipt"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.OfficialCreditItem"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.util.dao.LocationDAO"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    OfficialCreditItem vi = (OfficialCreditItem) OfficialReceiptDAO.getINVitem(log,request.getParameter("referno"));
    OfficialReceipt v = (OfficialReceipt) OfficialReceiptDAO.getINV(log,request.getParameter("referno"));
    CbOfficialCek cek = (CbOfficialCek) OfficialReceiptDAO.getCheque(log,request.getParameter("referno"));
    Module mod = (Module) OfficialReceiptDAO.getModule();


%>
        <link href="css/pvoucher.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {

                $('#print').click(function (e) {
                    printElement($("#viewjv").html());
                });

                $('.action').click(function (e) {
                    var action = $(this).attr('id');

                    $.ajax({
                        url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + action + "&refer=<%= v.getRefer()%>",
                        success: function (result) {
                            // $("#haha").html(result);
                            $('#herey').empty().html(result).hide().fadeIn(300);
                        }});

                    return false;

                });

                $('.action_2').click(function (e) {
                    var action = $(this).attr('id');

                    BootstrapDialog.confirm({
                        title: 'Confirmation',
                        message: 'Are you sure to ' + action + ' this?',
                        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                        closable: true, // <-- Default value is false
                        draggable: true, // <-- Default value is false
                        btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                        btnOKLabel: 'Delete', // <-- Default value is 'OK',
                        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
                        callback: function (result) {
                            // result will be true if button was click, while it will be false if users close the dialog directly.
                            if (result) {
                                $.ajax({
                                    url: "PathController?moduleid=<%= mod.getModuleID()%>&process=" + action + "&referno=<%= v.getRefer()%>",
                                    success: function (result) {
                                        $('#herey').empty().html(result).hide().fadeIn(300);
                                    }
                                });
                            }
                        }
                    });

                });
            });
        </script>
        <div id ="maincontainer">
            <div class="partition"> 
                <div class="bodyofpartition">
                    <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                        <tr>
                            <td valign="middle" align="left">


                            </td>
                        </tr>
                    </table>

                    <table id="table_left" width="100%" cellspacing="0">
                        <tr>
                            <td valign="middle" width="33%" align="left">
                                <div class="btn-group btn-group-sm" role="group" aria-label="...">


                                    <div class="btn-group" role="group" aria-label="...">
                                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i></button>
                                        <button id="print" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-print" aria-hidden="true"></i></button>
                                        <button id="edit" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist" <%= Restriction.getEdit(v.getCheckid(), v.getAppid())%>><i class="fa fa-pencil" aria-hidden="true"></i></button>

                                        <%
                                            if (log.getListUserAccess().get(1).getAccess()) {
                                        %>
                                        <button id="check" class="btn btn-default btn-sm action_2" title="<%= mod.getModuleID()%>" name="viewlist" <%= Restriction.getCheck(v.getCheckid())%>><i class="fa fa-check-square-o" aria-hidden="true"></i> Check</button>
                                        <%
                                            }
                                            if (log.getListUserAccess().get(2).getAccess()) {
                                        %>
                                        <button id="approve" class="btn btn-default btn-sm action_2" title="<%= mod.getModuleID()%>" name="viewlist" <%= Restriction.getApprove(v.getCheckid(), v.getAppid())%>><i class="fa fa-check-square-o" aria-hidden="true"></i> Approve</button>
                                        <%
                                            }
                                        %>



                                    </div>
                                </div>

                            </td>
                            <td width="33%" align="right">
                                <div id="totalpage">
                                    <select id="page">
                                        <option value="'+i+'"></option>
                                    </select> 
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table id="maintbl" width="100%" cellspacing="0">
                        <tr>
                            <td valign="top">

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="table_left" width="100%" cellspacing="0"><tr><td width="100%">
                                            <div id="viewjv">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="10">
                                                    <tr>
                                                        <td>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td width="50%">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td width="4%"><img src="image/logo_lkpp.jpg" width="25" height="25"></td>
                                                                                <td width="96%">&nbsp;<span class="comnamevc"><strong><%=log.getEstateDescp()%></strong></span></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td align="right"><strong>OFFICIAL RECEIPT</strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="medfont2">&nbsp;</p>
                                                                        <p class="medfont3">No 45/4, Jalan Teluk Sisek</p>
                                                                        <p class="medfont3">Peti surat 144</p><p class="medfont3">25710 Kuantan</p><p class="medfont3">Pahang Darul Makmur</p><p class="medfont3">&nbsp;</p><p class="medfont3">Tel : 09-5165180</p></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr>
                                                                    <td valign="top">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td rowspan="4" align="left" class="bordertopgrey" width="40%">&nbsp;<span class="boldtitle">To :</span></td>
                                                                                <td class="bordertoprightgrey" width="60%">&nbsp;<span class="normtitle"><%= v.getPaidname()%></span> 

                                                                                </td>
                                                                            </tr>
                                                                            <tr>

                                                                                <td class="borderleftbottomrightgrey">&nbsp;<span class="normtitle"><%= v.getPaidaddress()%></span></td>
                                                                            </tr>
                                                                            <tr>

                                                                                <td class="borderleftbottomrightgrey">&nbsp;<span class="normtitle"><%= v.getPaidpostcode()%>, <%= v.getPaidstate()%></span></td>
                                                                            </tr>
                                                                            <tr>

                                                                                <td class="borderleftbottomrightgrey">&nbsp;<span class="normtitle"><%= v.getGstid()%></span></td>
                                                                            </tr>


                                                                        </table>
                                                                    </td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td valign="top">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td align="left" class="bordertopgrey" width="40%">&nbsp;<span class="boldtitle">PAGE</span></td>
                                                                                <td class="bordertoprightgrey" width="60%">&nbsp;<span class="normtitle">1</span> 

                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" class="borderleftbottomgrey">&nbsp;<span class="boldtitle">REFERENCE NO</span></td>
                                                                                <td class="borderleftbottomrightgrey">&nbsp;<span class="normtitle"><%= v.getRefer()%></span></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" class="borderleftbottomgrey">&nbsp;<span class="boldtitle">DATE</span></td>
                                                                                <td class="borderleftbottomrightgrey">&nbsp;<span class="normtitle"><%= v.getDate()%></span></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" align="left" class="borderleftbottomgrey">&nbsp;<div class="amount">RM<%= GeneralTerm.currencyFormat(v.getAmount())%></div></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td align="right">&nbsp;<span class="boldtitle" id="noprint"></span></td></tr></table></td></tr><tr><td>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td colspan="10" class="forlisttitle"><span class="boldtitle">&nbsp;List of Account</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="2%" class="bordertop">&nbsp;<span class="intableheader_jv">No</span></td>
                                                                    <td width="15%" class="bordertop">&nbsp;<span class="intableheader_jv">Account Code</span></td>
                                                                    <td width="11%" class="bordertoprightgrey">&nbsp;<span class="intableheader_jv">Sub Code</span></td>
                                                                    <td width="11%" class="bordertoprightgrey">&nbsp;<span class="intableheader_jv">Location Code</span></td>
                                                                    <td width="32%" class="bordertoprightgrey">&nbsp;<span class="intableheader_jv">Remarks</span></td>
                                                                    <td width="9%" class="bordertoprightgrey">&nbsp;<span class="intableheader_jv">Amount</span></td>
                                                                    <td width="9%" class="bordertoprightgrey">&nbsp;<span class="intableheader_jv">Tax</span></td>
                                                                    <td width="9%" class="bordertoprightgrey">&nbsp;<span class="intableheader_jv">Total</span></td>
                                                                </tr>



                                                                <!--<tr height="20px"><td class="borderleft">&nbsp;<span class="fontjv_item"></span></td>
                                                                <td class="borderleft">&nbsp;<span class="fontjv_item"></span></td>
                                                                <td class="borderrightend" align="right">&nbsp;<span class="fontjv_item"></span>&nbsp;</td>
                                                                <td class="borderrightend" align="right">&nbsp;<span class="fontjv_item"></span>&nbsp;</td>
                                                                <td class="borderrightend" align="right">&nbsp;<span class="fontjv_item">b/f</span>&nbsp;</td>
                                                                <td class="borderrightend" align="right">&nbsp;<span class="fontjv_item">'+round_decimals(parseFloat(tot_debt),2)+'</span>&nbsp;</td> 
                                                                <td class="borderrightend" align="right">&nbsp;<span class="fontjv_item">'+round_decimals(parseFloat(tot_cred),2)+'</span>&nbsp;</td></tr>-->

                                                                <%
                                                                    int i = 0;
                                                                    List<OfficialCreditItem> slist = (List<OfficialCreditItem>) OfficialReceiptDAO.getAllINVItem(log,request.getParameter("refer"));
                                                                    for (OfficialCreditItem c : slist) {
                                                                        i++;
                                                                %>
                                                                <tr height="20px"><td class="borderleft">&nbsp;<span class="fontjv_item"><%= i%></span></td>
                                                                    <td class="borderleft">&nbsp;<span class="fontjv_item"><%= c.getCoacode()%></span></td>
                                                                    <td class="borderrightend" align="right"><span class="fontjv_item"><%= c.getSacode()%></span></td>
                                                                    <td class="borderrightend" align="right"><span class="fontjv_item"><%= c.getLoccode()%></span></td>
                                                                    <td class="borderrightend" align="left"><span class="fontjv_item"><%= c.getRemarks()%></span></td>
                                                                    <td class="borderrightend" align="right"><span class="fontjv_item"><%= GeneralTerm.currencyFormat(c.getAmtbeforetax())%></span></td>
                                                                    <td class="borderrightend" align="right"><span class="fontjv_item"><%= GeneralTerm.currencyFormat(c.getTaxamt())%></span></td>
                                                                    <td class="borderrightend" align="right"><span class="fontjv_item"><%= GeneralTerm.currencyFormat(Double.parseDouble(c.getAmount()))%></span></td>
                                                                </tr>
                                                                <%}%>

                                                                <tr>
                                                                    <td colspan="10"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="10"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="borderleft">&nbsp;</td>
                                                                    <td class="borderleft"  valign="top">&nbsp;</td>
                                                                    <td class="borderrightend" align="right">&nbsp;</td>
                                                                    <td class="borderrightend" align="right">&nbsp;</td>
                                                                    <td class="borderrightend" align="right">&nbsp;</td>
                                                                    <td class="borderrightend" align="right">&nbsp;</td>
                                                                    <td class="borderrightend" align="right">&nbsp;</td>
                                                                    <td class="borderrightend" align="right">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="borderleft">&nbsp;</td>
                                                                    <td class="borderleft">&nbsp;</td>
                                                                    <td class="borderrightend">&nbsp;</td>
                                                                    <td class="borderrightend">&nbsp;</td>
                                                                    <td class="borderrightend">&nbsp;</td>
                                                                    <td class="borderrightend">&nbsp;</td>
                                                                    <td class="borderrightend">&nbsp;</td>
                                                                    <td class="borderrightend">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="borderbottombold">&nbsp;</td>
                                                                    <td class="borderbottombold" valign="top">&nbsp;</td>
                                                                    <td align="right" class="borderbottomboldright">&nbsp;</td>
                                                                    <td align="right" class="borderbottomboldright">&nbsp;</td>
                                                                    <td align="right" class="borderbottomboldright" valign="top">&nbsp;<span class="boldtitle">TOTAL</span>&nbsp;</td>
                                                                    <td align="right" class="borderbottomboldright">
                                                                        <span class="totalamount_jv">&nbsp;&nbsp;<strong><%= GeneralTerm.currencyFormat(OfficialReceiptDAO.getSum(log,"amtbeforetax", request.getParameter("referno")))%></strong></span>&nbsp;
                                                                    </td>
                                                                    <td align="right" class="borderbottomboldright"><span class="totalamount_jv">&nbsp;&nbsp;<strong><%= GeneralTerm.currencyFormat(OfficialReceiptDAO.getSum(log,"taxamt", request.getParameter("referno")))%>
                                                                            </strong></span>&nbsp;</td>
                                                                    <td align="right" class="borderbottomboldright"><span class="totalamount_jv">&nbsp;&nbsp;<strong><%= GeneralTerm.currencyFormat(OfficialReceiptDAO.getSum(log,"amount", request.getParameter("referno")))%></strong></span>&nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="30px">&nbsp;
                                                            <span class="boldtitle">Year</span> - <%= v.getYear()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="boldtitle">Period</span> - <%= v.getPeriod()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="boldtitle">Cheque No</span> - <%= cek.getCekno()%></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr><td width="25%" class="bordertopgreyauth"><p class="paytofonttop">&nbsp;&nbsp;</p><p class="paytofonttop">&nbsp;&nbsp;_________________________</p>
                                                                        <p class="paytofontmiddle">&nbsp;&nbsp;<strong>RECEIVED BY</strong></p></td><td width="25%" class="bordertopgreyauthright"><p class="paytofonttop">&nbsp;&nbsp;</p><p class="paytofonttop">&nbsp;&nbsp;_________________________</p><p class="paytofontmiddle">&nbsp;&nbsp;<strong>APPROVED BY</strong></p></td></tr>

                                                                <tr><td class="borderonesidegreyauth" valign="top"><p class="paytofonttop">&nbsp;&nbsp;Name</p><p class="paytofontmiddle">&nbsp;&nbsp;<strong><%//= v.getReceivebyname()%> - <%//= v.getReceiveid() %></strong></p>
                                                                        <p class="paytofonttop">&nbsp;&nbsp;Type</p><p class="paytofontmiddle">&nbsp;&nbsp;<strong><%//= v.getReceivetype()%></strong></p></td>
                                                                    <td class="borderonesidegreyauthright" valign="top"><p class="paytofontmiddle">&nbsp;&nbsp;</p><p class="paytofonttop">&nbsp;&nbsp;Name</p><p class="paytofontmiddle">&nbsp;&nbsp;<strong><%= v.getAppname()%> - <%= v.getAppid()%></strong></p>
                                                                        <p class="paytofonttop">&nbsp;&nbsp;Designation</p><p class="paytofontmiddle">&nbsp;&nbsp;<strong><%= v.getAppposition()%></strong></p>
                                                                        <p class="paytofontmiddle">&nbsp;&nbsp;</p><p class="paytofonttop">&nbsp;&nbsp;Date</p><p class="paytofontmiddle">&nbsp;&nbsp;<strong><%= v.getApprovedate()%></strong></p></td></tr>
                                                                <tr><td class="borderleftbottomgrey">&nbsp;</td><td class="borderleftbottomgreyright">&nbsp;</td></tr></table></td></tr></table>

                                            </div>
                                </table>
                            </td>
                        </tr>
                        <input type="hidden" id="checkbyid" value="">
                        <input type="hidden" id="checkbyname" value="">
                        <input type="hidden" id="checkbydesign" value="">
                        <input type="hidden" id="checkdate" value="">
                    </table>


                </div>
            </div>
        </div>

