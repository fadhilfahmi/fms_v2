<%-- 
    Document   : ap_agreement_list
    Created on : Oct 4, 2016, 3:14:53 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.dao.financial.ar.ArContractDAO"%>
<%@page import="com.lcsb.fms.util.model.ListTable"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) ArContractDAO.getModule();

    String disable_button = "disabled";
    String str_button = "No new Contract";
//if(ArContractDAO.getDiffCount(log.getEstateCode())>0){
    disable_button = "";
    str_button = "New Contracts from Mill";
//}

%>

<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/tether.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/basic_new.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.printElement.js"></script>

<link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $.ajax({
            async: true,
            url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=getDiff&millcode=2411",
            success: function (result) {

                if (result == 0) {
                    var cont = 'No new Contract';
                    $('#add-new-remote').empty().html(cont).hide().fadeIn(300);
                    $("#add-new-remote").prop("disabled", true);

                } else {

                    var cont = 'New Contract from Mill <span class="badge">' + result + '</span>';
                    $('#add-new-remote').empty().html(cont).hide().fadeIn(300);

                }

            }
        });


        var updateTable = '<%= mod.getMainTable()%>';

    <%
    List<ListTable> mlist = (List<ListTable>) ArContractDAO.tableList();
    for (ListTable m : mlist) {
        //if(m.getList_View().equals("1")){
%>
        columntoView.push('<%= m.getColumnName()%>');
        columnName.push('<%= m.getTitleName()%>');
    <%
                //}

            }
    %>

        var colsTosend = 'cols=';
        for (m = 0; m < columntoView.length; ++m) {//put value from database to variable jget
            if (m == columntoView.length - 1) {
                colsTosend += columntoView[m];
            } else {
                colsTosend += columntoView[m] + '&cols=';
            }
        }


        $("#add-new").click(function () {
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addnew",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }});
            return false;
        });


        $("#add-new-remote").click(function () {
            var cont = '<i class="fa fa-cog fa-spin fa-fw"></i><span>Retrieving...</span>';
            $('#add-new-remote').empty().html(cont).hide().fadeIn(300);
            $.ajax({
                async: true,
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=retrievemill",
                success: function (result) {
                    // $("#haha").html(result);
                    $('#herey').empty().html(result).hide().fadeIn(300);
                    var conts = '<i class="fa fa-check-circle success" aria-hidden="true"></i> Successful Retrieved';
                    $('#add-new-remote').empty().html(conts).hide().fadeIn(300);
                },
                error: function () {
                    var conts = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Failed';
                    $('#add-new-remote').empty().html(conts).hide().fadeIn(300);
                }
            });
            return false;
        });


        var orderby = 'order by voucherid desc';
        var oTable = $('#example').DataTable({
            destroy: true,
            "aaSorting": [[0, "desc"]],
            "responsive": true,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "retrievetable.jsp?table=" + updateTable + "&code=<%= mod.getReferID_Master()%>&" + colsTosend + "&order=" + orderby,
            "aoColumns": [
                {"sTitle": columnName[0], "mData": columntoView[0], "sWidth": "10%", "bSearchable": true},
                {"sTitle": columnName[1], "mData": columntoView[1], "sWidth": "70%"},
                {"sTitle": "Action", "mData": null, "bSortable": false, "bSearchable": false, "sWidth": "20%"}

            ],
            "aoColumnDefs": [{
                    "aTargets": [2],
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {


                        var c = $(icon('edit', '', '', ''));
                        var e = $(icon('delete', '', '', ''));
                        var a = $('<button type="button" class="btn btn-default btn-xs" id="addagree"><i class="fa fa-plus-circle"></i></button>');
                        var b = $('<button type="button" class="btn btn-default btn-xs"><i class="fa fa-folder-open"></i></button>');


                        b.on('click', function () {
                            //console.log(oData);
                            $.ajax({
                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=viewor&refer=" + oData.refer,
                                success: function (result) {
                                    // $("#haha").html(result);
                                    $('#herey').empty().html(result).hide().fadeIn(300);
                                }});
                            return false;
                        });
                        c.on('click', function () {
                            //console.log(oData);
                            if (iconClick('edit', oData.checkid, oData.appid, oData.post) == 0) {
                                //editRowx(oData.JVrefno,updateTable,subTable,<%= mod.getModuleID()%>,module_abb,oTable);

                                $.ajax({
                                    url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editinv&refer=" + oData.refer,
                                    success: function (result) {
                                        // $("#haha").html(result);
                                        $('#herey').empty().html(result).hide().fadeIn(300);
                                    }});
                                return false;
                            }

                            //$("#dialog-form-update").dialog("open");
                            //$( "#dialog-form-update" ).dialog( "open" );
                            return false;
                        });
                        e.on('click', function () {
                            //console.log(oData);
                            if (iconClick('delete', oData.checkid, oData.appid, oData.post) == 0) {
                                //deleteRow(oData.JVrefno);

                                BootstrapDialog.confirm({
                                    title: 'Confirmation',
                                    message: 'Are you sure to delete?',
                                    type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                    closable: true, // <-- Default value is false
                                    draggable: true, // <-- Default value is false
                                    btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                                    btnOKLabel: 'Delete', // <-- Default value is 'OK',
                                    btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
                                    callback: function (result) {
                                        // result will be true if button was click, while it will be false if users close the dialog directly.
                                        if (result) {
                                            $.ajax({
                                                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=delete&referno=" + oData.refer,
                                                success: function (result) {
                                                    // $("#haha").html(result);
                                                    setTimeout(function () {
                                                        oTable.fnClearTable();
                                                    }, 500);
                                                }});
                                        }
                                    }
                                });


                                return false;
                            }

                            return false;
                        });


                        $(nTd).empty();
                        $(nTd).attr("class", 'btn-group');
                        $(nTd).attr("align", 'right');
                        $(nTd).prepend(a, b, c, e);
                    }

                }]



        });

        $('#example tbody').on('click', 'tr', function () {
            var data = oTable.row(this).data();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=agreelist&referno=" + data.code,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $('#example tbody').on('click', '#addcredit', function (e) {
            e.preventDefault();
            var refer = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addcredit&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $('#example tbody').on('click', '#addcheque', function (e) {
            e.preventDefault();
            var refer = $(this).attr('href');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=addcheque&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;
        });

        $('#add-invoice').click(function (e) {
            e.preventDefault();

            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                size: BootstrapDialog.SIZE_WIDE,
                //animate: false,
                title: 'Choose Sales Invoice',
                message: function (dialog) {
                    var $content = $('<body></body>').load('cb_official_invoice.jsp');
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });
                    return $content;
                },
                buttons: [{
                        label: 'Close',
                        action: function (dialogRef) {
                            dialogRef.close();
                        }
                    }]
            });

        });


    });

</script>

<div id ="maincontainer">
    <div class="partition">
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="add-new" class="btn btn-default btn-sm"><i class="fa fa-plus-circle success"></i> Add HQ <%= mod.getModuleDesc()%></button>
                        <button id="add-new-remote" class="btn btn-default btn-sm <%=  disable_button%>"><i class="fa fa-cog fa-spin fa-fw"></i>
                            <span>Loading...</span><%//= str_button %> <span class="badge"><%//= ArContractDAO.getDiffCount(log.getEstateCode()) %></span></button>

                    </td>
                </tr>
            </table>
            <br>       
            <div class="dataTable_wrapper"> 
                <table class="table table-striped table-bordered table-hover" id="example">
                </table>
            </div>

        </div>
    </div>
</div>
