<%-- 
    Document   : em_payroll
    Created on : May 30, 2017, 9:19:41 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.financial.gl.CnOPeriodDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.dao.financial.employee.EmPayrollDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) CnOPeriodDAO.getModule();

%>

<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>

<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />

<link href="bootstrap-form-wizard/form-wizard.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
        var navListItems = $('ul.setup-panel li a'),
                allWells = $('.setup-content');

        allWells.hide();

        navListItems.click(function (e)
        {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                    $item = $(this).closest('li');

            if (!$item.hasClass('disabled')) {
                navListItems.closest('li').removeClass('active');
                $item.addClass('active');
                allWells.hide();
                $target.show();
            }

            //var curStep = $(this).closest(".setup-content"),
            //var curStepBtn = $(this).attr("class");

            //console.log(curStepBtn);

        });

        $('ul.setup-panel li.active a').trigger('click');

        // DEMO ONLY //
        $('#activate-step-2').on('click', function (e) {
            $('ul.setup-panel li:eq(1)').removeClass('disabled');
            $('ul.setup-panel li a[href="#step-2"]').trigger('click');
            $('.save-earning').trigger('click');
            //$(this).remove();
        })
        $('#activate-step-3').on('click', function (e) {
            $('ul.setup-panel li:eq(2)').removeClass('disabled');
            $('ul.setup-panel li a[href="#step-3"]').trigger('click');
            //$(this).remove();
        })
        $('#activate-step-4').on('click', function (e) {
            $('ul.setup-panel li:eq(3)').removeClass('disabled');
            $('ul.setup-panel li a[href="#step-4"]').trigger('click');
            //$(this).remove();
        })

        $('#activate-step-5').on('click', function (e) {
            $('ul.setup-panel li:eq(4)').removeClass('disabled');
            $('ul.setup-panel li a[href="#step-5"]').trigger('click');
            //$(this).remove();
        })



        $('#accordion').append('<div id="spinloadingmain" style="width:100px; margin:auto;; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>').hide().fadeIn(100);



        $('.print-paysheet').click(function (ev) {
            ev.stopImmediatePropagation();
            ev.preventDefault();
            if (ev.handled !== true) { //Checking for the event whether it has occurred or not.
                ev.handled = true; // Basically setting value that the current event has occurred.
                var c = $(this).attr('id');//earncode
                var d = $(this).attr('title');//type earning@deduction;
                var r = $('#refer').val();

                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DEFAULT,
                    size: BootstrapDialog.SIZE_WIDE,
                    title: 'View Pay Slip',
                    cssClass: 'print-dialog-landscape',
                    message: function (dialog) {
                        var $content = $('<body></body>').load('em_payroll_paysheet_print.jsp?refer=' + r + '&start=1');
                        //$('body').on('click', '.thisresult_nd', function(event){
                        //    dialog.close();
                        //    event.preventDefault();
                        //});
                        return $content;
                    },
                    buttons: [{
                            label: 'Close',
                            action: function (dialogRef) {
                                dialogRef.close();
                            }
                        }]
                });

            }
            return false;
        });

        $('#scannow').click(function (ev) {

        });


        $('#scannow').click(function (ev) {
            //ev.stopImmediatePropagation();
            ev.preventDefault();

            $('.statusdiv').html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Waiting ... ');
            $('.trclass').removeClass('red-row');
            $('.trclass').removeClass('green-row');

            $.ajax({
                async: true,
                url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=modulearray",
                success: function (result) {
                    var obj = jQuery.parseJSON(result);

                    var i = 0;
                    $.each(obj, function (key, value) {
//console.log(obj.);
                        setTimeout(function () {
                            $('#div' + value).html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Checking ...').hide().fadeIn(100);
                            $.ajax({
                                async: true,
                                url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=scanmodule&modulescan=" + value,
                                success: function (data) {

                                    var m = parseInt(data);


                                    //$('#div' + value).html(data).hide().fadeIn(100);
                                    if (data > 0) {
                                        i = parseFloat(i) + parseFloat(data);
                                        $('#total-unfinish').val(i);
                                        var tot = $('#total-unfinish').val();
                                        console.log('**' + tot);
                                        if (tot > 0) {
                                            console.log('ada lagi');
                                        } else {
                                            console.log('xdok doh');
                                        }

                                        //console.log('#div' + value);
                                        $('#div' + value).html('<i style="color:#f44242" class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<a id="gotomodulex" style="cursor:pointer"><strong>' + data + '</strong> Not Approve yet</a>');
                                    } else {
                                        $('#div' + value).html('<i style="color:#1d9d73" class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;');
                                    }
                                }
                            });
                        }, 1000);



                    });



                }
            });


            return false;
        });

        $.ajax({
            url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=listmodule",
            success: function (result) {
                $('#list-module').append(result).hide().fadeIn(100);
                $('#spinloadingmain').remove();
            }
        });

        $('body').on('click', '#gotomodulex', function (ev) {
            ev.stopImmediatePropagation();
            ev.preventDefault();
            alert(88);
        });
    });
</script>


<div class="well">
    <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
        <tr>
            <td valign="middle" align="left">

                <label>For Accounting Year : <%= AccountingPeriod.getCurYear(log)%> & Period : <%= AccountingPeriod.getCurPeriod(log)%></label>

            </td>
        </tr>
    </table>
    <div class="row form-group">
        <div class="col-xs-12">
            <ul class="nav nav-pills nav-justified thumbnail setup-panel">
                <!--<li class="active"><a href="#step-1">
                        <h2 class="list-group-item-heading">Step 1 </h2>
                        <p class="list-group-item-text">Set Earning</p>
                    </a></li>
                <li class="disabled"><a href="#step-2">
                        <h2 class="list-group-item-heading">Step 2</h2>
                        <p class="list-group-item-text">Set Deduction</p>
                    </a></li>
                <li class="disabled"><a href="#step-3">
                        <h2 class="list-group-item-heading">Step 3</h2>
                        <p class="list-group-item-text">Overtime Claim</p>
                    </a></li>
                <li class="disabled"><a href="#step-4">
                        <h2 class="list-group-item-heading">Step 4</h2>
                        <p class="list-group-item-text">Pay Slip</p>
                    </a></li>
                <li class="disabled"><a href="#step-5">
                        <h2 class="list-group-item-heading">Step 5</h2>
                        <p class="list-group-item-text">Pay Sheet</p>
                    </a></li>
                <li class="disabled"><a href="#step-5">
                        <h2 class="list-group-item-heading">Step 6</h2>
                        <p class="list-group-item-text">Finalize</p>
                    </a></li>-->

                <li class="active"><a href="#step-1" class="closecontent">
                        <h2 class="list-group-item-heading">Step 1 <!--<span style="color: green"><i class="fa fa-check-circle" aria-hidden="true"></i></span>--></h2>
                        <p class="list-group-item-text">Scan Unfinish Transaction</p>
                    </a></li>
                <li class=""><a href="#step-2" class="closecontent">
                        <h2 class="list-group-item-heading">Step 2</h2>
                        <p class="list-group-item-text">Finalize Data</p>
                    </a></li>
                <li class=""><a href="#step-3" id="step-payslip" class="closecontent">
                        <h2 class="list-group-item-heading">Step 3</h2>
                        <p class="list-group-item-text">Close Period</p>
                    </a></li>
            </ul>
        </div>
    </div>
    <div class="row setup-content" id="step-1">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="row">
                            <button id="scannow" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Scan Now</button>
                            <input type="text" id="total-unfinish">
                            <!--<button id="activate-step-2" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Scan Now</button>-->

                            <div class="panel-group accordion-earning" id="list-module" role="tablist" aria-multiselectable="true">
                                <!-- earning content -->
                            </div>
                        </div>

                        <!--<button class="btn btn-default nextBtn btn-sm pull-right" id="activate-step-2" type="button" >Next</button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="row">
                            <a href="#" id="Deduction" class="add icon-link"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add Deduction</a>&nbsp;&nbsp;
                            <a href="#" id="Deduction" class="save icon-link"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save All Deduction</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="result-save-deduction"></span>

                            <p></p>
                            <div class="panel-group accordion-deduction" id="accordion-Deduction" role="tablist" aria-multiselectable="true">
                                <!-- earning content -->
                            </div>
                        </div>

                        <button class="btn btn-default nextBtn btn-sm pull-right" id="activate-step-3" type="button" >Next</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-3">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="row">
                            <a href="#" id="Deduction" class="add icon-link"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add Deduction</a>&nbsp;&nbsp;
                            <a href="#" id="Deduction" class="save icon-link"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save All Deduction</a>&nbsp;&nbsp;&nbsp;&nbsp;<span id="result-save-deduction"></span>

                            <p></p>

                            <div id="content-payslip"></div>

                        </div>

                        <button class="btn btn-default nextBtn btn-sm pull-right" id="activate-step-3" type="button" >Next</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>