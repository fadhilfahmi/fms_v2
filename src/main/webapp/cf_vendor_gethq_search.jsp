<%-- 
    Document   : cf_vendor_gethq_search
    Created on : Sep 27, 2016, 9:31:44 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.model.setup.configuration.Vendor"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.VendorDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="list-group"><%
            
            String type = request.getParameter("type");
            String keyword = request.getParameter("keyword");
            List<Vendor> slist = (List<Vendor>) VendorDAO.searchEntity(keyword);
            if(slist.isEmpty()){
               %>
       
                <div class="alert alert-danger" role="alert">
                    No matching search criteria were found for <strong><%= keyword %></strong>! - <a>Add New Vendor</a>
                </div>

               <%   
            }
            for (Vendor c : slist) { 
               %>
       
               <a href="<%= c.getType()%>" id="<%= c.getCode()%>"  title="" class="list-group-item thisresult">
                <div id="left_div"><%= c.getCode()%></div>
                <div id="left_div"><%= c.getType()%></div>
                <div id="right_div"><%= c.getVendorName()%></div>
            </a>

               <%    
            } 
        %>
</div>
