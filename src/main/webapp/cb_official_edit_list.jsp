<%-- 
    Document   : cb_official_edit_list
    Created on : Mar 3, 2016, 3:42:15 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.ui.UIConfig"%>
<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page import="com.lcsb.fms.dao.financial.gl.PostDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.CbOfficialCek"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.OfficialReceipt"%>
<%@page import="com.lcsb.fms.model.financial.cashbook.OfficialCreditItem"%>
<%@page import="com.lcsb.fms.dao.financial.cashbook.OfficialReceiptDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    OfficialCreditItem vi = (OfficialCreditItem) OfficialReceiptDAO.getINVitem(log, request.getParameter("refer"));
    OfficialReceipt v = (OfficialReceipt) OfficialReceiptDAO.getINV(log, request.getParameter("refer"));
    CbOfficialCek cek = (CbOfficialCek) OfficialReceiptDAO.getCheque(log, request.getParameter("refer"));
    Module mod = (Module) OfficialReceiptDAO.getModule();

    String status = OfficialReceiptDAO.getStatus(log, v.getRefer());

    String distribute = request.getParameter("distribute");
%>


<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        callPnotify('<%= request.getParameter("status")%>', '<%= request.getParameter("text")%>');

    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-primary btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                    </td>
                </tr>
            </table>
            <br>
            <%= UIConfig.renderBanner(log, v.getRefer(), mod.getModuleID())%>
            <table class="table table-bordered table-striped  table-hover">
                <thead>
                    <tr>
                        <th colspan="4">Voucher Information
                            <!--<small><span class="label label-primary">Preparing</span></small>-->
                            <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
                                <%= UIConfig.renderMainButton(log, v.getRefer(), mod.getModuleID())%> 
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody class="activerowm tdrow">
                    <tr>
                        <th width="15%">Status</th>
                        <td width="35%"><%= GeneralTerm.getStatusLabel(OfficialReceiptDAO.getStatus(log, v.getRefer()))%><%= DebitCreditNoteDAO.checkDCNoteExist(log, v.getRefer())%></td>
                        <th width="15%">Paid Name</th>
                        <td width="35%">(<%= v.getPaidcode()%>) <%= v.getPaidname()%> - <%= v.getPaidtype()%></td>
                    </tr>
                    <tr>
                        <th>Period</th>
                        <td><%= v.getYear()%>, <%= v.getPeriod()%></td>
                        <th>Bank</th>
                        <td><%= v.getBankcode()%> - <%= v.getBankname()%></td>
                    </tr>
                    <tr>
                        <th>Voucher No</th>
                        <td><%= v.getRefer()%></td>
                        <th>Amount</th>
                        <td>RM<%= GeneralTerm.currencyFormat(v.getAmount())%></td>
                    </tr>
                    <tr>
                        <th>Voucher Date</th>
                        <td><%= AccountingPeriod.fullDateMonth(v.getDate())%></td>
                        <th>Amount (String)</th>
                        <td><%= v.getRm()%></td>
                    </tr>

                    <tr>
                        <th>Remarks</th>
                        <td colspan="3"><%= v.getRemarks()%></td>
                    </tr>
                    <tr class="togglerow" style="display: none">
                        <th>Paid Address</th>
                        <td><%= v.getPaidaddress()%></td>
                        <th>Paid Postcode</th>
                        <td><%= v.getPaidpostcode()%></td>
                    </tr>
                    <tr class="togglerow" style="display: none">
                        <th>Paid City</th>
                        <td><%= v.getPaidcity()%></td>
                        <th>Paid State</th>
                        <td><%= v.getPaidstate()%></td>
                    </tr>
                    <tr class="togglerow" style="display: none">
                        <th>Rounding</th>
                        <td><%= v.getRacoacode()%> - <%= v.getRacoadesc()%></td>
                        <th>Paid Type</th>
                        <td><%= v.getPaidtype()%></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th colspan="5">Cheque Information<!--<small>asdada</small>-->
                            <span class="pull-right">
                                <%= UIConfig.renderSecondSubButton(log, v.getRefer(), mod.getModuleID())%>
                            </span>
                        </th>
                    </tr>
                    <tr>
                        <th>Cheque No</th>
                        <th>Date</th>
                        <th>Bank</th>
                        <th>Amount</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <%List<CbOfficialCek> listRefer = (List<CbOfficialCek>) OfficialReceiptDAO.getAllCheque(log, v.getRefer());
                        if (listRefer.isEmpty()) {%>
                    <tr>
                        <td colspan="5">
                            <span class="font-small-red">No data available.</span>
                        </td>
                    </tr>
                    <%}
                        for (CbOfficialCek c : listRefer) {%>
                    <tr class="activerowx" id="<%= c.getRefer()%>">
                        <td class="tdrow">&nbsp;<i class="fa fa-paperclip" aria-hidden="true"></i>&nbsp;&nbsp;<%= c.getCekno()%> </td>
                        <td class="tdrow">&nbsp;<%= AccountingPeriod.fullDateMonth(c.getDate())%></td>
                        <td class="tdrow">&nbsp;<%= c.getBank()%> - <%= c.getBank()%></td>
                        <td class="tdrow" align="right">&nbsp;<%= GeneralTerm.currencyFormat(c.getAmount())%></td>
                        <td class="tdrow" align="right">&nbsp;
                            <%
                                boolean[] typeArrayOn = {false, true, true};
                                String[] classArray = {"", "edititem", "deleteitem"};
                                String[] nameArray = {"", "edit_nd", "delete_nd"};
                                out.println(UIConfig.renderActionGroupButton(log, c.getRefer(), mod.getModuleID(), typeArrayOn, classArray, nameArray));
                            %>
                        </td>
                    </tr>
                    <tr id="togglerow_nd<%= c.getRefer()%>" style="display: none">
                        <td colspan="5">
                            <table class="table table-bordered">
                                <thead></thead>
                                <tbody>
                                    <tr>
                                        <th>Reference No</th>
                                        <td><%= c.getRefer()%></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <%}%>
                </tbody>
            </table>

            <table class="table table-bordered table-striped table-hover" id="dataTables">
                <thead>
                    <tr>
                        <th colspan="5">Detail<!--<small>asdada</small>-->
                            <span class="pull-right">
                                <%= UIConfig.renderFirstSubButton(log, v.getRefer(), mod.getModuleID())%>
                            </span>
                        </th>
                    </tr>
                    <tr>
                        <th>Reference No</th>
                        <th>Account</th>
                        <th>Sub Account</th>
                        <th>Amount</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <%List<OfficialCreditItem> listAll = (List<OfficialCreditItem>) OfficialReceiptDAO.getAllINVItem(log, v.getRefer());
                    if (listAll.isEmpty()) {%>
                <tbody>
                    <tr>
                        <td colspan="5">
                            <span class="font-small-red">No data available.</span>
                        </td>
                    </tr>
                    <%}
                        for (OfficialCreditItem j : listAll) {%>
                    <tr class="activerowy" id="<%= j.getRefer()%>">
                        <td class="tdrow">&nbsp;<%= j.getRefer()%></td>
                        <td class="tdrow">&nbsp;<%= j.getCoacode()%> - <%= j.getCoadescp()%></td>
                        <td class="tdrow">&nbsp;<%= j.getSatype()%> / <%= j.getSacode()%> / <%= j.getSadesc()%> </td>
                        <td class="tdrow" align="right">&nbsp;<%= GeneralTerm.currencyFormat(Double.parseDouble(j.getAmount()))%></td>
                        <td class="tdrow" align="right">&nbsp;
                            <%
                                boolean[] typeArrayOn = {false, true, true};
                                String[] classArray = {"", "edititem", "deleteitem"};
                                String[] nameArray = {"", "edit_st", "delete_st"};
                                out.println(UIConfig.renderActionGroupButton(log, j.getRefer(), mod.getModuleID(), typeArrayOn, classArray, nameArray));
                            %>
                        </td>
                    </tr>
                    <tr id="togglerow_st<%= j.getRefer()%>" style="display: none">
                        <td colspan="5">
                            <table class="table table-bordered">
                                <thead></thead>
                                <tbody>
                                    <tr>
                                        <th width="15%">Location</th>
                                        <td  width="35%"><%= j.getLoclevel()%> / <%= j.getLoccode()%> / <%= j.getLocname()%></td>
                                        <th width="15%">Taxation</th>
                                        <td width="35%"><%= j.getTaxcode()%> - <%= j.getTaxdescp()%> / <%= j.getSadesc()%></td>
                                    </tr>
                                    <tr>
                                        <th>Tax Amount</th>
                                        <td><%= j.getTaxamt()%></td>
                                        <th>Tax Rate</th>
                                        <td><%= j.getTaxrate()%></td>
                                    </tr>
                                    <tr>
                                        <th>Remarks</th>
                                        <td colspan="3"><%= j.getRemarks()%></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
                <%}%>
            </table>
        </div>
    </div>
</div>    