<%-- 
    Document   : ac_lot_activity_distmember
    Created on : Dec 22, 2016, 11:07:27 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.ext.ParseSafely"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="com.lcsb.fms.dao.management.activity.LotActivityDAO"%>
<%@page import="com.lcsb.fms.model.management.activity.LotTempMember"%>
<%@page import="com.lcsb.fms.model.setup.configuration.LotMemberLot"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.LotMemberDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<table class="table table-bordered table-striped table-hover" id="list">
    <thead>

        <tr>
            <th>Member Code</th>
            <th>Member Name</th>
            <th>Acre</th>
            <th>Hectar</th>
            <th>Profit</th>
        </tr>
    </thead>
    <tbody>
        <%List<LotTempMember> listx = (List<LotTempMember>) LotActivityDAO.getAllMemberByLot(request.getParameter("sessionid"));
            int i = 0;
            double totalProfit = 0.0;

            for (LotTempMember j : listx) {
                i++;

                double profit = Double.parseDouble(LotMemberDAO.getInfoLotByCode(j.getCode(), j.getLotcode()).getHectarage()) * ParseSafely.parseDoubleSafely(request.getParameter("suggested"));
                totalProfit += profit;
        %>
        <tr  id="<%= j.getId()%>">
            <td class="tdrow"><%= j.getCode()%></td>
            <td class="tdrow"><%= j.getName()%></td>
            <td class="tdrow">&nbsp;<span class="pull-right"><%= LotMemberDAO.getInfoLotByCode(j.getCode(), j.getLotcode()).getAcre()%></span></td>
            <td class="tdrow">&nbsp;<span class="pull-right"><%= LotMemberDAO.getInfoLotByCode(j.getCode(), j.getLotcode()).getHectarage()%></span></td>
            <td class="tdrow">&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(profit)%></span></td>
        </tr>
        <%}%>
        <tr>
            <td class="tdrow"></td>
            <td class="tdrow"></td>
            <td class="tdrow">&nbsp;</td>
            <td class="tdrow">&nbsp;</td>
            <th class="tdrow">&nbsp;<span class="pull-right"><%= GeneralTerm.currencyFormat(totalProfit)%></span></th>
        </tr>
    </tbody>
</table>
