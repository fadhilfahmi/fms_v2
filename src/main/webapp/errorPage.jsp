<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" sizes="57x57" href="assets/fav/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="assets/fav/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="assets/fav/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="assets/fav/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="assets/fav/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="assets/fav/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="assets/fav/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="assets/fav/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="assets/fav/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="assets/fav/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="assets/fav/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="assets/fav/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="assets/fav/favicon-16x16.png">
        <link rel="manifest" href="assets/fav/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="assets/fav/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <title>FMS | Server Error</title>

        <!-- Bootstrap -->
        <link href="bower_components/bootstrap.min.css" rel="stylesheet">

        <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<script src="jsfunction/jquery-1.12.0.min.js"></script>
        <!-- Custom Theme Style -->
        <link href="css/custom/custom.css" rel="stylesheet">
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {

                $(".login").click(function () {
                    window.location = "/fms_v2/";
                    return false;
                });



            });
        </script>
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <!-- page content -->
                <div class="col-md-12">
                    <div class="col-middle">
                        <div class="text-center">
                            <h1 class="error-number"><i class="fa fa-clock-o" aria-hidden="true"></i></h1>
                            <h2>Session Timeout</h2>
                            <p>Your session has expired, try refreshing or log in again. If the problem persists feel free to contact us.
                            </p>
                            <p>Contact administrator at Ext 162 or email fadhilfahmi@lcsb.com.my</p>
                            <div class="mid_center">
                                <button class="btn btn-default login" type="button">Go to Login Page</button>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->
            </div>
        </div>


    </body>
</html>