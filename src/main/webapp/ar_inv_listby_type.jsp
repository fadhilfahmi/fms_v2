<%-- 
    Document   : ar_inv_cpo
    Created on : Oct 31, 2016, 12:41:21 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoice"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) ModuleDAO.getModule(log,request.getParameter("moduleid"));
    String prodCode = SalesInvoiceDAO.getProdCode(mod.getModuleID());
%>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        var oTable = $('#list').DataTable({
            responsive: true,
            "aaSorting": [[0, "desc"]]
        });

        $('#list tbody').on('click', 'tr', function () {
            var data = oTable.row(this).data();
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=editlist&referno=" + data[0],
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $('body').on('click', '#refresh', function (e) {
            oTable.draw();
            return false;
        });

    });
</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="add-new" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i> Add <%= mod.getModuleDesc()%></button>
                    </td>
                    <td valign="middle" align="right">
                        &nbsp;<button id="refresh" class="btn btn-default btn-sm pull-right refresh"><i class="fa fa-refresh"></i> Refresh</button>
                    </td>
                </tr>
            </table>
            <hr>       
            <div class="dataTable_wrapper"> 
                <table class="table table-bordered table-striped table-hover" id="list">
                    <thead>

                        <tr>
                            <th>Reference No</th>
                            <th>Date</th>
                            <th>Buyer Name</th>
                            <th>Commodity</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%List<SalesInvoice> listx = (List<SalesInvoice>) SalesInvoiceDAO.getAllSI(log,prodCode);

                                for (SalesInvoice j : listx) {%>
                        <tr class="activerowy" id="<%= j.getInvref()%>">
                            <td class="tdrow"><%= j.getInvref()%></td>
                            <td class="tdrow"><%= j.getInvdate()%></td>
                            <td class="tdrow">&nbsp;<%= j.getBname()%></td>
                            <td class="tdrow">&nbsp;<%= j.getProdname()%></td>
                            <td class="tdrow">&nbsp;<%= j.getAmountno()%></td>
                            <td class="tdrow">&nbsp;</td>
                            <td class="tdrow" align="right">&nbsp;
                                <div class="btn-group btn-group-xs" role="group" aria-label="...">
                                    <button id="<%= j.getInvref()%>" class="btn btn-default btn-xs edititem" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-pencil"></i></button>
                                    <button id="<%= j.getInvref()%>" class="btn btn-default btn-xs deleteitem" title="<%= mod.getModuleID()%>"  name="delete_st"><i class="fa fa-trash-o"></i></button>
                                </div>
                            </td>
                        </tr>
                        <%}%>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
