<%-- 
    Document   : user_dashboard
    Created on : Apr 7, 2017, 3:54:26 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.template.user.Dashboard"%>
<%@page import="com.lcsb.fms.dao.administration.om.UserDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");

    out.println(Dashboard.viewDashboard(log));
%>

<!--<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        <%if (UserDAO.getDashboardAccess(log, log.getUserID(), "020903")) {%>
            $.ajax({
                url: "PathController?moduleid=020903&process=summaryinvoice",
                success: function (result) {
                    $('#datatable-render-inv').empty().html(result).hide().fadeIn(300);
                }
            });
        <%}
        if (UserDAO.getDashboardAccess(log, log.getUserID(), "020908")) {%>
            $.ajax({
                url: "PathController?moduleid=020908&process=summarydnote",
                success: function (result) {
                    $('#datatable-render-dnote').empty().html(result).hide().fadeIn(300);
                }
            });
        <%}
        if (UserDAO.getDashboardAccess(log, log.getUserID(), "020909")) {%>
            $.ajax({
                url: "PathController?moduleid=020909&process=summarycnote",
                success: function (result) {
                    $('#datatable-render-cnote').empty().html(result).hide().fadeIn(300);
                }
            });
        <%}%>
    });

</script>
<div class="row">
    <%if (UserDAO.getDashboardAccess(log, log.getUserID(), "020903")) {%>
    <div class="col-lg-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>Sales Invoice <small>Summary of Transaction</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="dataTable_wrapper" id="datatable-render-inv">
                <div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>
            </div>

        </div>
    </div>
    <%}
                                    if (UserDAO.getDashboardAccess(log, log.getUserID(), "020908")) {%>
    <div class="col-lg-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>Sales Debit Note <small>Summary of Transaction</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="dataTable_wrapper" id="datatable-render-dnote">
                <div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <%}
                                    if (UserDAO.getDashboardAccess(log, log.getUserID(), "020909")) {%>
    <div class="col-lg-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>Sales Credit Note <small>Summary of Transaction</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="dataTable_wrapper" id="datatable-render-cnote">
                <div style="width:100px; margin:0 auto; color:#000 !important;"><span style="color:#000 !important;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></span><span class="sr-only">Loading...</span></div>
            </div>

        </div>
    </div>
    <%}%>
</div>-->

