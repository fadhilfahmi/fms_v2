<%-- 
    Document   : gl_jv_edit_list
    Created on : Mar 3, 2016, 3:42:15 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.model.financial.ar.ArContractAgree"%>
<%@page import="com.lcsb.fms.dao.financial.ar.ArContractDAO"%>
<%@page import="com.lcsb.fms.util.dao.GetStatusDAO"%>
<%@page import="com.lcsb.fms.ui.UIConfig"%>
<%@page import="com.lcsb.fms.dao.financial.gl.PostDAO"%>
<%@page import="com.lcsb.fms.dao.financial.gl.DebitCreditNoteDAO"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoice"%>
<%@page import="com.lcsb.fms.model.financial.ar.SalesInvoiceItem"%>
<%@page import="com.lcsb.fms.dao.financial.ar.SalesInvoiceDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.model.Current"%>
<%@page import="com.lcsb.fms.general.GeneralTerm"%>
<%@page import="java.util.List"%>
<%@page import="com.lcsb.fms.util.dao.ModuleDAO"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    SalesInvoiceItem vi = (SalesInvoiceItem) SalesInvoiceDAO.getINVitem(log, request.getParameter("referno"));
    SalesInvoice v = (SalesInvoice) SalesInvoiceDAO.getINV(log, request.getParameter("referno"));
    ArContractAgree ag = (ArContractAgree) ArContractDAO.getEachAgreeUsingContractNo(log, v.getContractno());
    Module mod = (Module) SalesInvoiceDAO.getModule();

    String status = SalesInvoiceDAO.getStatus(log, v.getInvref());

    String topath = request.getParameter("topath");
    String moduleid = mod.getModuleID();
    String token = request.getParameter("status");

    if (topath == null) {
        topath = "viewlist";
    } else {
        //moduleid = request.getParameter("frommodule");
    }

    String classButtonColor = "";
    String classButtonDisable = "";

    if (status.equals("Approved") && token.equals("Checked")) {
        classButtonColor = "btn-warning";
        classButtonDisable = "disabled";
    } else if (status.equals("Checked") && token.equals("Prepared")) {
        classButtonColor = "btn-warning";
        classButtonDisable = "disabled";
    } else if (token.equals("Checked")) {
        classButtonColor = "btn-success";
        classButtonDisable = "";
    } else if (token.equals("Prepared")) {
        classButtonColor = "btn-success";
        classButtonDisable = "";
    }

%>

<style>

    a.thelink {
        cursor: pointer;
        color: #ffffff;
        text-decoration: underline;
    }
    a.thelink:link {
        color: #ffffff;
    }

    /* visited link */
    a.thelink:visited {
        color: #ffffff;
    }

    /* mouse over link */
    a.thelink:hover {
        color: #097a62;
    }

    /* selected link */
    a.thelink:active {
        color: #ffffff;
    }
</style>



<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {


        $('#print').click(function (e) {
            printElement($("#viewjv").html());
        });

        ;


    });
</script>
<table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
    <tr>
        <td valign="middle" align="left">
            <div class="btn-group" role="group" aria-label="...">
                <button id="backtox" class="btn btn-default btn-xs" title="<%= mod.getModuleID()%>"  name="<%= topath%>" type="<%= token%>"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                <button id="<%= v.getInvref()%>" class="btn btn-default btn-xs viewprinted" title="<%= mod.getModuleID()%>"  name="<%= topath%>" type="<%= token%>"><i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;&nbsp;View Printed</button>
                <%
                    if (token.equals("Prepared")) {
                %>
                <button id="<%= v.getInvref()%>" class="btn <%= classButtonColor%> btn-xs checknow <%= classButtonDisable%>" title="<%= mod.getModuleID()%>"  name="<%= topath%>" type="<%= token%>"><i class="fa fa-check-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;Check</button>
                <%
                } else if (token.equals("Checked")) {
                %>
                <button id="<%= v.getInvref()%>" class="btn <%= classButtonColor%> btn-xs approvenow <%= classButtonDisable%>" title="<%= mod.getModuleID()%>"  name="<%= topath%>" type="<%= token%>"><i class="fa fa-check-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;Approve</button>
                <%
                    }
                %>
            </div>

        </td>
    </tr>
</table>
<br>
<table class="table table-bordered table-striped  table-hover">
    <thead>
        <tr>
            <th colspan="4">Voucher Information
                <!--<small><span class="label label-primary">Preparing</span></small>-->
                <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">



                </div>
            </th>
        </tr>
    </thead>
    <tbody class="activerowm tdrow">
        <tr>
            <th width="25%">Status</th>
            <td width="25%"><%= GeneralTerm.getStatusLabel(GetStatusDAO.getStatus(log, v.getInvref()))%><%= DebitCreditNoteDAO.checkDCNoteExist(log, v.getInvref())%></td>
            <th width="25%">Buyer Name</th>
            <td width="25%">(<%= v.getBcode()%>) <%= v.getBname()%> - <%= v.getBtype()%></td>
        </tr>
        <tr>
            <th>Period</th>
            <td><%= v.getYear()%>, <%= v.getPeriod()%></td>
            <th>Location</th>
            <td><%= v.getLoclevel()%> / <%= v.getLoccode()%> / <%= v.getLocdesc()%></td>
        </tr>
        <tr>
            <th>Invoice No</th>
            <td><%= v.getInvref()%></td>
            <th>Amount</th>
            <td>RM<%= GeneralTerm.currencyFormat(v.getAmountno())%></td>
        </tr>
        <tr>
            <th>Invoice Date</th>
            <td><%= v.getInvdate()%></td>
            <th>Remark</th>
            <td><%= v.getRemarks()%></td>
        </tr>
        <tr class="togglerow" style="display: none">
            <th>Buyer Address</th>
            <td><%= v.getBaddress()%></td>
            <th>Buyer Postcode</th>
            <td><%= v.getBpostcode()%></td>
        </tr>
        <tr class="togglerow" style="display: none">
            <th>Buyer City</th>
            <td></td>
            <th>Buyer State</th>
            <td><%= v.getBstate()%></td>
        </tr>
        <tr class="togglerow" style="display: none">
            <th>Rounding</th>
            <td><%= v.getRacoacode()%> - <%= v.getRacoadesc()%></td>
            <th>Contract No</th>
            <td><%= v.getContractno()%>

            </td>
        </tr>
    </tbody>
</table>
<input type="hidden" class="price" id="price" name="price" value="<%= ag.getPrice()%>">
<input type="hidden" id="contractno" value="<%= v.getContractno()%>">
<input type="hidden" id="prodcode" value="<%= v.getProdcode()%>">
<input type="hidden" id="invrefno" value="<%= v.getInvref()%>">
<div class="invoice-content">
    <div class="table-responsive">
        <table class="table table-invoice">
            <thead>
                <tr>
                    <th>SALES INVOICE DETAIL</th>
                    <th><span class="pull-left">ACCOUNT CODE</span></th>
                    <th><span class="pull-right">QUANTITY</span></th>
                    <th><span class="pull-right">TAX</span></th>
                    <th><span class="pull-right">AMOUNT</span></th>
                    <th><span class="pull-right">ACTION</span></th>
                </tr>
            </thead>
            <%List<SalesInvoiceItem> listAll = (List<SalesInvoiceItem>) SalesInvoiceDAO.getAllINVItem(log, v.getInvref());
                double totalTax = 0.0;
                double totalAmount = 0.0;
                double totalQty = 0.0;

                if (listAll.isEmpty()) {%>
            <tbody>
                <tr>
                    <td colspan="5">
                        <span class="font-small-red">No data available.</span>
                    </td>
                </tr>
                <%}
                    for (SalesInvoiceItem j : listAll) {
                        totalTax += j.getTaxamt();
                        totalAmount += j.getAmount();
                        totalQty += j.getQty();

                %>

                <tr>
                    <td>
                        <%= j.getRemarks()%><br />
                        <small><%= j.getRefer()%></small>
                    </td>
                    <td><span class="pull-left"><%= j.getCoacode()%> - <%= j.getCoaname()%></span><br />
                        <small><%= j.getSatype()%> / <%= j.getSacode()%> / <%= j.getSadesc()%></small></td>
                    <td><span class="pull-right"><%= j.getQty()%></span></td>
                    <td><span class="pull-right"><%= GeneralTerm.currencyFormat(j.getTaxamt())%></span></td>
                    <td><span class="pull-right"><%= GeneralTerm.currencyFormat(j.getAmount())%></span></td>
                    <td>
                        <%if (!status.equals("Approved")) {%>
                        <div class="btn-group btn-group-xs" role="group" aria-label="...">
                            <button id="<%= j.getRefer()%>" class="btn btn-default btn-xs edititem" title="<%= mod.getModuleID()%>" name="edit_st"><i class="fa fa-pencil"></i></button>
                            <button id="<%= j.getRefer()%>" class="btn btn-default btn-xs deleteitem" title="<%= mod.getModuleID()%>"  name="delete_st"><i class="fa fa-trash-o"></i></button>
                        </div>
                        <%}%>
                    </td>
                </tr>
                <%}%>

            </tbody>
            <thead>
                <tr>
                    <th>TOTAL</th>
                    <th><span class="pull-right"><%//= GeneralTerm.currencyFormat( totQty )%></span></th>
                    <th><span class="pull-right"><%= GeneralTerm.currencyFormat(totalQty)%></span></th>
                    <th><span class="pull-right"><%= GeneralTerm.currencyFormat(totalTax)%></span></th>
                    <th><span class="pull-right"><%= GeneralTerm.currencyFormat(totalAmount)%></span></th>
                    <th><span class="pull-right"><%//= GeneralTerm.currencyFormat( totGrand )%></span></th>
                </tr>
            </thead>
        </table>
    </div>

</div>
