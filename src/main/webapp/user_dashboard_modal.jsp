<%-- 
    Document   : user_dashboard_modal
    Created on : Aug 30, 2017, 11:42:04 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.administration.om.UserDAO"%>
<%@page import="com.lcsb.fms.model.administration.om.UserDashboardModuleSummary"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    String moduleid = request.getParameter("moduleid");
    String status = request.getParameter("status");
    int count = Integer.parseInt(request.getParameter("count"));
%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {

        $('.summary-content').on('click', '.approvenow', function (event, ui) {
            //var a = $("form").serialize();
            event.stopImmediatePropagation();
            event.preventDefault();
            var a = $(this).attr('id');
            var b = $(this).attr('title');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are you sure to <strong>Approve</strong> this?',
                type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Approve', // <-- Default value is 'OK',
                btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "ProcessController?moduleid=" + b + "&process=approve&referno=" + a,
                            success: function (result) {

                                //$('#table-result-contract').empty().html(result).hide().fadeIn(300);

                                if (result == 1) {
                                    console.log('approved');
                                    $('.approvenow').html('Approved');
                                    $('.approvenow').addClass('disabled');
                                    $('.approvenow').addClass('btn-warning').removeClass('btn-success');
                                  
                                } else {
                                    setTimeout(function () {
                                        //dialogRef.close();
                                        dialogRef.enableButtons(true);
                                        dialogRef.setClosable(true);
                                        //$button.disable();
                                        $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                        dialogRef.getModalBody().html('<i class="fa fa-times fail-red"></i>&nbsp;&nbsp;<span style="font-size:14px">Failed!</span>');
                                    }, 3000);
                                }

                            }
                            ,
                            error: function () {
                                //var conts = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Connection failed to establish. Please try again later.';
                                //$('#table-result-contract').empty().html(conts).hide().fadeIn(300);
                                $('.checknow').html('Failed to Check');
                            }

                        });
                    }
                }
            });
            return false;
        });

        $('.summary-content').on('click', '.checknow', function (event, ui) {
            //var a = $("form").serialize();
            event.stopImmediatePropagation();
            event.preventDefault();
            var a = $(this).attr('id');
            var b = $(this).attr('title');

            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are you sure to <strong>Check</strong> this?',
                type: BootstrapDialog.TYPE_DEFAULT, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
                btnOKLabel: 'Check', // <-- Default value is 'OK',
                btnOKClass: 'btn-success', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $.ajax({
                            url: "ProcessController?moduleid=" + b + "&process=check&referno=" + a,
                            success: function (result) {

                                //$('#table-result-contract').empty().html(result).hide().fadeIn(300);

                                if (result == 1) {
                                    console.log('checked');
                                    $('.checknow').html('Checked');
                                    $('.checknow').addClass('disabled');
                                    $('.checknow').addClass('btn-warning').removeClass('btn-success');
                                  
                                } else {
                                    setTimeout(function () {
                                        //dialogRef.close();
                                        dialogRef.enableButtons(true);
                                        dialogRef.setClosable(true);
                                        //$button.disable();
                                        $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                        dialogRef.getModalBody().html('<i class="fa fa-times fail-red"></i>&nbsp;&nbsp;<span style="font-size:14px">Failed!</span>');
                                    }, 3000);
                                }

                            }
                            ,
                            error: function () {
                                //var conts = '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Connection failed to establish. Please try again later.';
                                //$('#table-result-contract').empty().html(conts).hide().fadeIn(300);
                                $('.checknow').html('Failed to Check');
                            }

                        });
                    }
                }
            });
            return false;
        });

        $('.summary-content').on('click', '.viewprinted', function (event, ui) {
            event.stopImmediatePropagation();
            event.preventDefault();
            var a = $(this).attr('id');
            var b = $(this).attr('title');
            var token = $(this).attr('type');
            $.ajax({
                url: "PathController?moduleid=" + b + "&process=viewprinted2&refer=" + a + "&status=" + token,
                success: function (result) {
                    $('.summary-content').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });

        $('.summary-content').one('click', '#backtox', function (event, ui) {
            var path = $(this).attr('title');//moduleid
            var process = $(this).attr('name');
            var refer = $(this).attr('type');
            var q = '';

            if (refer != null) {
                q = '&referno=' + refer;
            }

            var urlx = "";

            if (process == 'frommodal') {

                var token = refer.split('/');
                //token[0] = "part1"
                //token[1] = "part2
                urlx = "PathController?moduleid=040101&process=" + process + "&tomodule=" + path + "&status=" + refer;
            } else {
                urlx = "PathController?moduleid=" + path + "&process=" + process + q;
            }

            $.ajax({
                url: urlx,
                success: function (result) {
                    // $("#haha").html(result);
                    if (process == 'frommodal') {
                        $('.summary-content').empty().html(result).hide().fadeIn(100);
                    } else {
                        $('#herey').empty().html(result).hide().fadeIn(100);
                    }

                }
            });
            return false;
        });
        
        $('.summary-content').on('click', '.backtosummary', function (event, ui) {
            event.stopImmediatePropagation();
            event.preventDefault();
            var path = $(this).attr('title');//moduleid
            var process = $(this).attr('name');
            var refer = $(this).attr('type');
            var token = $(this).attr('id');
            var q = '';

            q = '&referno=' + refer;

            var urlx = "PathController?moduleid=" + path + "&process=" + process + "&referno=" + refer + "&topath=frommodal&status="+token;

            $.ajax({
                url: urlx,
                success: function (result) {
                    $('.summary-content').empty().html(result).hide().fadeIn(100);

                }
            });
            
            return false;
        });

        $('.viewdetail').click(function (e) {

            var a = $(this).attr('id');
            var b = $(this).attr('title');
            //e.preventDefault();
            $.ajax({
                url: "PathController?moduleid=" + b + "&process=viewsummary&referno=" + a + "&topath=frommodal&status=<%=status%>",
                success: function (result) {
                    $('.summary-content').empty().html(result).hide().fadeIn(300);
                }
            });
            return false;
        });
    });

</script>
<div class="summary-content">
    <table class="table table-invoice" id="list">
        <thead>

            <tr>
                <th>Reference No.</th>
                <th><span class="pull-right">ACTION</span></th>
            </tr>
        </thead>
        <tbody>

            <%List<UserDashboardModuleSummary> listAll = (List<UserDashboardModuleSummary>) UserDAO.getSummaryModule(log, moduleid, status);
                if (listAll.isEmpty()) {%>
            <tr>
                <td colspan="2">
                    <span class="font-small-red">No data available.</span>
                </td>
            </tr>

            <%}
                //double totalDebit = 0.0;
                //double totalCredit = 0.0;
                for (UserDashboardModuleSummary j : listAll) {

                    //    totalDebit += j.getDebit();
                    //    totalCredit += j.getCredit();
            %>
            <tr>
                <td><%= j.getRefer()%></td>

                <td>
                    <%//if (!status.equals("Approved")) {%>
                    <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
                        
                        <button id="<%= j.getRefer()%>" class="btn btn-default btn-xs viewdetail" title="<%= moduleid%>"  name="delete_st"><i class="fa fa-eye" aria-hidden="true"></i> Detail</button>
                        
                    </div>
                    <%//}%>
                </td>
            </tr>
            <%}%>

        </tbody>
    </table>

</div>