<%-- 
    Document   : ap_inv_add_ffb
    Created on : Apr 17, 2017, 11:46:50 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.dao.financial.ap.VendorInvoiceDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) VendorInvoiceDAO.getModule();
%>
<!DOCTYPE html>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('.refineryBtn').prop('disabled', true);

        $('.form-control').keyup(function (e) {
            if (($('#ffb').val() == 0.00) || ($('#mpobpricecpo').val() == 0.00) || ($('#mpobpricepk').val() == 0.00) || ($('#transport').val() == 0.00) || ($('#millcost').val() == 0.00) || ($('#cess').val() == 0.00) || ($('#oer').val() == 0.00) || ($('#ker').val() == 0.00)) {
                $('.refineryBtn').prop('disabled', true);
            } else {
                console.log('ok');
                $('.refineryBtn').prop('disabled', false);
            }

            if (($('#oer').val() > 100) || ($('#ker').val() > 100)) {

                $(this).removeClass('has-success');
                $(this).addClass('has-error');
                $('#res_code').append("<i class=\"fa fa-exclamation-circle fail-red\"></i>&nbsp;&nbsp;Code not available");
            }
        });
        
        $('#getsupplier').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Supplier Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_supplier_assign.jsp?name=sname&code=scode&address=address');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

    });
</script>

<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addffbprocess" ><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>


            <form data-toggle="validator" role="form" id="saveffb">
                <div class="well">
                    <div class="row">
                         <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Supplier Code &nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="scode" name="suppcode">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="getsupplier"><i class="fa fa-cog"></i> Get Supplier</button>
                                    </span>
                                </div></div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Supplier Name &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="sname" name="suppname" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputName" class="control-label">FFB Supplied (Mt)</label>
                                <input type="text" class="form-control input-sm dateformat" id="ffb" name="ffb" placeholder="">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputName" class="control-label">MPOB Price of Palm Oil (RM)</label>
                                <input type="text" class="form-control input-sm dateformat" id="mpobpricecpo" name="mpobpricecpo" placeholder="">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputName" class="control-label">MPOB Price of Palm Kernel (RM)</label>
                                <input type="text" class="form-control input-sm dateformat" id="mpobpricepk" name="mpobpricepk" placeholder="">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Transport of Palm Oil (RM)</label>
                                <input type="text" class="form-control input-sm dateformat" id="transport" name="transport" value="52.00" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Mill Processing Cost (RM)</label>
                                <input type="text" class="form-control input-sm dateformat" id="millcost" name="millcost" value="55.00" placeholder="">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputName" class="control-label">OER %</label><span class="oer-res"></span> 
                                <input type="text" class="form-control input-sm dateformat" id="oer" name="oer" placeholder="">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputName" class="control-label">KER %</label><span class="ker-res"></span> 
                                <input type="text" class="form-control input-sm dateformat" id="ker" name="ker" placeholder="">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputName" class="control-label">CESS MPOB (RM)</label>
                                <input type="text" class="form-control input-sm dateformat" id="cess" name="cess" value="13.00" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Discounted Price for High FFA</label>
                                <input type="text" class="form-control input-sm dateformat" id="discount" name="discount" value="0.00">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputName" class="control-label">Winfall Profit Levy</label>
                                <input type="text" class="form-control input-sm dateformat" id="winfall" name="winfall" value="0.00">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="inputName" class="control-label">External FFB Transport (RM)</label>
                                <input type="text" class="form-control input-sm dateformat" id="externalffb" name="externalffb" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>