<%-- 
    Document   : cf_lotmem_view
    Created on : Apr 26, 2016, 8:22:21 AM
    Author     : qoyum
--%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.setup.configuration.LotMemberDAO"%>
<%@page import="com.lcsb.fms.model.setup.configuration.LotMember"%>
<%@page import="com.lcsb.fms.ui.button.Button"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
LotMember view = (LotMember) LotMemberDAO.getInfo(log,request.getParameter("id"));
%>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
           
            
              $( ".printbutton" ).click(function() {
                  //var a = $("form").serialize();
                  var a = $("#saveform :input[value!='']").serialize(); 
                  //alert($('input[name="ad_amount"]').val());
                  checkField();
                  var b = checkField();
                  if(b==false){
                    $.ajax({
                        data: a,
                        type: 'POST',
                        url: "DataController?do=add&formlevel=master",
                        success: function (result) {
                          $('#maincontainer').empty().html(result).hide().fadeIn(300);
                        }
                    });
                     
                  }    
                  
               
                    return false;
              });
              
         });
        </script>
            <div id ="maincontainer">
                <table id="table_2" cellspacing="0">
                    
               <tr>
                   <td width="30%" class="bd_bottom" align="left" valign="top">Member Code</td>
                   <td width="70%" class="bd_bottom" align="left"><%= view.getMemCode() %></td>
               </tr>
               <tr>
                   <td class="bd_bottom" align="left" valign="top">Title</td>
                   <td class="bd_bottom" align="left"> 
                       <%= view.getTitle()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Use Title</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getUsetitle()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Member Name</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getMemName()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">IC Number</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getIc()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Address</td>
                   <td class="bd_bottom" align="left">
                       <%= view.getAddress()%>
                   </td>
               </tr>
               
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Postcode</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getPostcode()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">City</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getCity()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">State</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getState()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Bank Name</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getBank()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Account Number</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getAcc()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Payment Method</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getPaymethod()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Status</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getStatus()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Telephone Number</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getNotel()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Inactive Date</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getDateInactive()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Lot's Pre Owner</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getPreOwner()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Relationship</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getRelation()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Active Date</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getDateActive()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">New Owner</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getNewOwner()%>
                   </td>
               </tr>
               <tr>
                   <td  class="bd_bottom" align="left" valign="top">Remarks</td>
                   <td  class="bd_bottom" align="left">
                       <%= view.getRemark()%>
                   </td>
               </tr>
              
          </table>
            </div>



