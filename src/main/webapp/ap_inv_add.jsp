<%-- 
    Document   : ap_inv_add.jsp
    Created on : May 13, 2016, 10:00:09 AM
    Author     : user
--%>

<%@page import="com.lcsb.fms.dao.financial.ap.VendorInvoiceDAO"%>
<%@page import="com.lcsb.fms.general.AutoGenerate"%>
<%@page import="com.lcsb.fms.util.dao.ParameterDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) VendorInvoiceDAO.getModule();
%>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>

<link href="datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });
        //$('#JVdate').datepicker({ dateFormat: 'yyyy-mm-dd' });

        $('#date').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        }).change(dateChanged)
                .on('changeDate', dateChanged);
        
        $('#checkdate').datepicker({
            format: 'yyyy-mm-dd',
            defaultDate: 'now',
            autoclose: true
        });
        
        $('#tarikh').datepicker({
                    format: 'yyyy-mm-dd',
                    defaultDate: 'now',
                    autoclose: true
                });


        $('#getsub').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Sub Account Detail',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_sub.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });



        $("#total").keyup(function () {
            var val = $(this).val();
            var to = 'amount';
            //alert(val);
            //alert(checkRegexp(val,to));
            if (checkRegexp(val, to)) {
                $('#res_digit').empty();
                $('#div_digit').removeClass('has-error');
                $('#div_digit').addClass('has-success');
                $('#res_digit').append("<i class=\"fa fa-check-circle right-green\"></i>&nbsp;&nbsp;");
                $('#savebutton').prop('disabled', false);
                convertRM(val, to);
            } else {
                $('#res_digit').empty();
                $('#div_digit').removeClass('has-success');
                $('#div_digit').addClass('has-error');
                $('#res_digit').append("<i class=\"fa fa-exclamation-circle fail-red\"></i>&nbsp;&nbsp;Numeric Formatted Only");
                $('#savebutton').prop('disabled', true);
            }
            //
        });

        $('#getbuyer').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Buyer Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_buyer.jsp?name=bname&code=bcode');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });
        $('#getsupplier').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Supplier Info',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_supplier_assign.jsp?name=sname&code=scode&address=address');
                    $('body').on('click', '.thisresult', function (event) {
                        console.log('tutup');
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });
        $('#getbank').click(function (e) {

            var a = 'bankcode';
            var b = 'bankname';
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Taxation',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_bank.jsp?code=' + a + '&name=' + b);
                    $('body').on('click', '.thisresult', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });

        $('#getlocation').click(function (e) {
            //e.preventDefault();
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Get Location',
                //message: $('<body></body>').load('list_coa.jsp')
                message: function (dialog) {
                    var $content = $('<body></body>').load('list_location.jsp');
                    $('body').on('click', '.thisresult_nd', function (event) {
                        dialog.close();
                        event.preventDefault();
                    });

                    return $content;
                }
            });

            return false;
        });
    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                <tr>
                    <td valign="middle" align="left">
                        <button id="backto" class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;&nbsp;Back</button>
                        <button id="savebutton"  class="btn btn-default btn-sm" title="<%= mod.getModuleID()%>" name="addinv" ><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                    </td>
                </tr>
            </table>
            <br>



            <form data-toggle="validator" role="form" id="saveform">
                <input type="hidden" name="preid" id="pid" value="<%= log.getUserID()%>">
                <input type="hidden" name="prename" id="pname" value="<%= log.getFullname()%>">
                <input type="hidden" name="predate" id="pdate" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">
                <input type="hidden" name="estatecode" value="<%= log.getEstateCode()%>">
                <input type="hidden" name="estatename" value="<%= log.getEstateDescp()%>">
                <div class="well">
                    <div class="row">


                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Invoice No &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="invno" name="invno" placeholder="Auto Generate" autocomplete="off" readonly>   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Reference No&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="invrefno" name="invrefno" placeholder="0.00" autocomplete="off" value="" readonly > 
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Date &nbsp&nbsp<span id="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="date" name="date" autocomplete="off" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">   
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Year &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="year" name="year" placeholder="Code" autocomplete="off" value="<%= AccountingPeriod.getCurYearByCurrentDate() %>">   
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Period &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="period" name="period"  autocomplete="off" value="<%= AccountingPeriod.getCurPeriodByCurrentDate() %>">   
                            </div>
                        </div>


                    </div>
                </div><div class="well">

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Invoice Type&nbsp&nbsp<span class="res_code"></span></label>
                                <select class="form-control input-sm" id="invtype" name="invtype">
                                    <%= ParameterDAO.parameterList(log, "Invoice Type", "")%>
                                </select>
                            </div>  
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Supplier Code &nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="scode" name="suppcode">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm" type="button" id="getsupplier"><i class="fa fa-cog"></i> Get Supplier</button>
                                    </span>
                                </div></div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Supplier Name &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="sname" name="suppname" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">GST ID &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <input type="text" class="form-control input-sm" id="gstid" name="gstid" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Address &nbsp&nbsp<span class="res_code"></span></label>
                                <textarea class="form-control" rows="4"  id="address" name="suppaddress"></textarea>
                            </div>
                        </div>
                    </div></div>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Account Code&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span id="res_digit" class="res_style"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="accode" name="accode" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="accode" id1="acdesc"><i class="fa fa-cog"></i> Account</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Account Description&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="acdesc" name="acdesc" readonly>
                            </div>
                        </div></div></div>



                <div class="well"><div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">RM (Digit) &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="totalamount" name="totalamount" placeholder="" autocomplete="off" > 
                            </div>
                        </div>



                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Remark &nbsp&nbsp<span class="res_code"></span></label>
                                <textarea class="form-control" rows="4"  id="remark" name="remark"></textarea>
                            </div>
                        </div>

                    </div>
                </div>
                <label for="inputName" class="control-label">Optional - Rounding Adjustment</label><small> | If Invoice need a rounding amount</small>
                <div class="well">
                    <div class="row"> 
                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Rounding Adjustment Code&nbsp&nbsp<span class="res_code"></span></label>
                                <div class="form-group-sm input-group">
                                    <input type="text" class="form-control input-sm" id="racoacode" name="racoacode">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-sm getaccount" type="button" id="racoacode" id1="racoadesc"><i class="fa fa-cog"></i> Account</button>
                                    </span>
                                </div>  
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Rounding Adjustment Description&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="racoadesc" name="racoadesc" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                    </div>    
                </div>
                <label for="inputName" class="control-label">Optional - Additional Information</label><small> | Invoice generated from Good Receive Note</small>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group" id="div_digit">
                                <label for="inputName" class="control-label">Good Receive Note &nbsp&nbsp<span id="res_digit" class="res_style"></span></label>
                                <div class="form-group" id="div_code">
                                    <select class="form-control input-sm" id="grnlist" name="grnlist">

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Good Receive Note No.&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="nogrn" name="nogrn" placeholder="" autocomplete="off" > 
                            </div>
                        </div></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Purchase Order No.&nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="porefno" name="porefno" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Invoice Reference No. &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="noinv" name="noinv" placeholder="" autocomplete="off" > 
                            </div>
                        </div>
                    </div>
                </div>

                <div class="well">
                    <div class="row"><div class="col-sm-12">
                            <label for="inputName" class="control-label">Prepared By :  &nbsp&nbsp<span class="res_code"></span></label>
                        </div></div>
                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">ID &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="preid" name="preid" readonly placeholder="" autocomplete="off" value="<%= log.getUserID()%>">   
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Name &nbsp&nbsp<span class="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="prename" name="prename" readonly placeholder="" autocomplete="off" value="<%= log.getFullname()%>"> 
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group" id="div_code">
                                <label for="inputName" class="control-label">Date &nbsp&nbsp<span id="res_code"></span></label>
                                <input type="text" class="form-control input-sm" id="predate" name="predate" readonly autocomplete="off" value="<%= AccountingPeriod.getCurrentTimeStamp()%>">   
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
