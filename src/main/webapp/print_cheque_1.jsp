<%-- 
    Document   : print_cheque
    Created on : Jan 4, 2017, 4:43:43 PM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.template.voucher.FormattedVoucher"%>
<%@page import="com.lcsb.fms.template.voucher.VoucherMaster"%>
<%@page import="com.lcsb.fms.template.voucher.PaymentVoucher_Template"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    //FormattedVoucher v = (FormattedVoucher) VoucherMaster.getMaster(request.getParameter("refer"), log);
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/pcheque.css?1" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
        <script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {

                $('#print').click(function (e) {

                    printCheque($("#viewjv").html());

                });

            });
        </script>


    </head>
    <body>
        <div id ="maincontainer">
            <div class="partition"> 
                <div class="headofpartition"></div>
                <div class="bodyofpartition">
                    <table border="0" id="secondtable" cellspacing="0" cellpadding="6" class="alternate">
                        <tr>
                            <td valign="middle" align="left">


                            </td>
                        </tr>
                    </table>

                    <table id="table_left" width="100%" cellspacing="0" border="0">
                        <tr>
                            <td valign="middle" width="33%" align="left">
                                <div class="btn-group btn-group-sm" role="group" aria-label="...">


                                    <div class="btn-group" role="group" aria-label="...">
                                        <button id="backto" class="btn btn-default btn-sm" title="<%//= v.getModuleid()%>" name="viewlist"><i class="fa fa-arrow-circle-o-left"></i></button>
                                        <button id="print" class="btn btn-default btn-sm" title="<%//= mod.getModuleID()%>" name="viewlist"><i class="fa fa-print" aria-hidden="true"></i> Print</button>




                                    </div>
                                </div>
                                <span>&nbsp;&nbsp;<strong><%//= v.getRefer()%></strong> | Printable Page : <%//= v.getPage()%></span>
                            </td>
                            <td width="60%" style="vertical-align: middle;">
                                <!--<div class="form-group" id="div_code">
                                    
                                    <select class="form-control input-xs" id="reflexcb" name="reflexcb">
                                <%//= ParameterDAO.parameterList("YesNo Type", "No")%>
                            </select>   
                        </div>-->
                            </td>
                        </tr>
                    </table>
                    <table id="maintbl" width="100%" cellspacing="0" border="0">
                        <tr>
                            <td class="greybg">
                                <table id="table_left" width="100%" cellspacing="0">
                                    <tr>
                                        <td width="100%">
                                            <div id="viewjv" style="overflow-y: scroll; height:600px;">
                                                <div class="page">
                                                <img src="image/maybank2.jpg"  style="padding-top:0px">
                                                </div>
                                            </div>
                                </table>
                            </td>
                        </tr>
                        <input type="hidden" id="checkbyid" value="">
                        <input type="hidden" id="checkbyname" value="">
                        <input type="hidden" id="checkbydesign" value="">
                        <input type="hidden" id="checkdate" value="">
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
