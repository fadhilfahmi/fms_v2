<%-- 
    Document   : dev_unapprove
    Created on : Jan 9, 2017, 11:29:21 AM
    Author     : fadhilfahmi
--%>

<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.development.UnapproveDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
%>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });

        $('#go').click(function (e) {
            var refer = $('#refer').val();

            $.ajax({
                url: "dev_unapprove_viewgl.jsp?refer=" + refer,
                success: function (result) {
                    $('#rendergl').empty().html(result).hide().fadeIn(300);
                    $('#unapprove').prop('disabled', false);

                }});

            return false;
        });
        
        $('#unapprove').click(function (e) {
            var refer = $('#refer').val();

                $.ajax({
                    async: false,
                    url: "ProcessController?moduleid=000000&process=unapprove&refer=" + refer,
                    success: function (result) {
                        if(result==2){
                            $('#rendergl').empty().html('<h1>Successful Unapprove & Unpost.</h1>').hide().fadeIn(300);
                        }else if(result==1){
                            $('#rendergl').empty().html('<h1>Successful Unapprove.</h1>').hide().fadeIn(300);
                        }else{
                            
                            if(result.substring(0, 3)=='DNE' || result.substring(0, 3)=='CNE'){
                                $('#rendergl').empty().html('<h1>Successful Unapprove & Unpost. This Voucher has Debit/Credit Note : '+result+'</h1>').hide().fadeIn(300);
                            }else{
                                $('#rendergl').empty().html('<h1>Failed to unapprove.</h1>').hide().fadeIn(300);
                            }
                            
                        }
                        
                    }
                });

            return false;
        });


    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">
            <div class="well">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group" id="div_code">
                            <label for="inputName" class="control-label">Enter Reference No&nbsp&nbsp<i class="fa fa-star" style="font-size:10px; color:#900"></i><span class="res_code"></span></label>
                            <div class="form-group-sm input-group">
                                <input type="text" class="form-control input-sm" id="refer" name="refer">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-sm" type="button" id="go"><i class="fa fa-cog"></i> Go</button>
                                </span>
                            </div>    
                        </div>
                    </div>
                    <div class="col-sm-2">
                    
                    <div class="form-group" id="div_code">
                            <label for="inputName" class="control-label">&nbsp;</label>
                            <div class="form-group-sm input-group">
                                
                    <button id="unapprove"  class="btn btn-default btn-sm" title="<%//= mod.getModuleID()%>" name="unpprove" disabled>Remove from GL</button>
                            </div>    
                        </div>
                    </div>
                            <div class="col-sm-6">
                    
                    <div class="form-group" id="div_code">
                            <label for="inputName" class="control-label">&nbsp;</label>
                            <div class="form-group-sm input-group">
                                
                                Voucher Supported : <strong><%= UnapproveDAO.voucherSupported(log) %></strong>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12" id="rendergl"></div>
            </div>
        </div>
    </div>
</div>