<%-- 
    Document   : gl_jv_add_new
    Created on : Feb 29, 2016, 2:03:11 PM
    Author     : Dell
--%>

<%@page import="com.lcsb.fms.dao.financial.gl.CnOPeriodDAO"%>
<%@page import="com.lcsb.fms.util.model.Module"%>
<%@page import="com.lcsb.fms.general.AccountingPeriod"%>
<%@page import="com.lcsb.fms.util.model.LoginProfile"%>
<%@page import="com.lcsb.fms.dao.financial.gl.JournalVoucherDAO"%>

<%@page import="com.lcsb.fms.util.dao.ConnectionUtil"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    LoginProfile log = (LoginProfile) session.getAttribute("login_detail");
    Module mod = (Module) CnOPeriodDAO.getModule();
%>
<script src="jsfunction/jquery-1.12.0.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/bootstrap.min.js"></script>
<link href="bootstrapdialog/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="bootstrapdialog/bootstrap-dialog.min.js"></script>
<script type="text/javascript" language="javascript" src="jsfunction/jquery_event.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        //$('#JVdate').datepicker({ dateFormat: 'yy-mm-dd' });

        $('#checkunpost').click(function (e) {

            e.preventDefault();
            var refer = $(this).attr('id');
            var moduleid = $(this).attr('title');
            $.ajax({
                url: "PathController?moduleid=<%= mod.getModuleID()%>&process=checkunpost&refer=" + refer,
                success: function (result) {
                    $('#herey').empty().html(result).hide().fadeIn(300);
                }
            });

            return false;

        });

        $('#finalize').click(function (e) {
            e.preventDefault();

            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                //animate: false,
                title: 'Finalizing Account',
                message: 'Period : <%= AccountingPeriod.getCurPeriod(log)%> - Year : <%= AccountingPeriod.getCurYear(log)%>',
                buttons: [{
                        //icon: 'fa fa-circle-o-notch',
                        label: 'Finalize',
                        cssClass: 'btn-primary',
                        autospin: true,
                        action: function (dialogRef) {
                            var $button = this;
                            dialogRef.enableButtons(false);
                            dialogRef.setClosable(false);
                            dialogRef.getModalBody().html('<span style="font-size:14px">Finalizing in progress...</span>');

                            $.ajax({
                                async: false,
                                url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=finalize",
                                success: function (result) {
                                    // $("#haha").html(result);
                                    //$('#herey').empty().html(result).hide().fadeIn(300);
                                    //alert(result);
                                    if (result == 1) {
                                        setTimeout(function () {
                                            //dialogRef.close();
                                            dialogRef.enableButtons(true);
                                            dialogRef.setClosable(true);
                                            $button.disable();
                                            $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                            dialogRef.getModalBody().html('<i class="fa fa-check right-green"></i>&nbsp;&nbsp;<span style="font-size:14px">Success!</span>');
                                        }, 3000);
                                    } else {
                                        setTimeout(function () {
                                            //dialogRef.close();
                                            dialogRef.enableButtons(true);
                                            dialogRef.setClosable(true);
                                            //$button.disable();
                                            $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                            dialogRef.getModalBody().html('<i class="fa fa-times fail-red"></i>&nbsp;&nbsp;<span style="font-size:14px">Finalizing failed!</span>');
                                        }, 3000);
                                    }
                                },
                                error: function () {
                                    dialogRef.enableButtons(true);
                                    dialogRef.setClosable(true);
                                    //$button.disable();
                                    $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                    dialogRef.getModalBody().html('<i class="fa fa-times fail-red"></i>&nbsp;&nbsp;<span style="font-size:14px">Error Occured!</span>');
                                }
                            });

                        }
                    }, {
                        label: 'Close',
                        action: function (dialogRef) {
                            dialogRef.close();
                        }
                    }]
            });

            /* BootstrapDialog.show({
             type:BootstrapDialog.TYPE_DEFAULT,
             title: 'Finalizing Account',
             message: function(dialog) {
             var $content = $('<body></body>').load('gl_accountperiod_set.jsp?');
             //$('body').on('click', '.thisresult_nd', function(event){
             //    dialog.close();
             //    event.preventDefault();
             //});
             return $content;
             }
             });*/
        });

        $('#closeperiod').click(function (e) {
            e.preventDefault();

            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                size: BootstrapDialog.SIZE_WIDE,
                //animate: false,
                title: 'Confirm Close and Open New Period',
                message: function (dialog) {
                    var $content = $('<body></body>').load('gl_close_period_view.jsp');
                    //$('body').on('click', '.thisresult_nd', function(event){
                    //    dialog.close();
                    //    event.preventDefault();
                    //});
                    return $content;
                },
                buttons: [{
                        //icon: 'fa fa-circle-o-notch',
                        label: 'Confirm & Close',
                        cssClass: 'btn-primary',
                        autospin: true,
                        action: function (dialogRef) {
                            var $button = this;
                            dialogRef.enableButtons(false);
                            dialogRef.setClosable(false);
                            //dialogRef.getModalBody().html('<span style="font-size:14px">Finalizing in progress...</span>');

                            $.ajax({
                                async: false,
                                url: "ProcessController?moduleid=<%= mod.getModuleID()%>&process=confirm",
                                success: function (result) {
                                    // $("#haha").html(result);
                                    //$('#herey').empty().html(result).hide().fadeIn(300);
                                    if (result == 1) {
                                        setTimeout(function () {
                                            //dialogRef.close();
                                            dialogRef.enableButtons(true);
                                            dialogRef.setClosable(true);
                                            $button.disable();
                                            $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                            dialogRef.getModalBody().html('<i class="fa fa-check right-green"></i>&nbsp;&nbsp;<span style="font-size:14px">Success!</span>');
                                        }, 3000);
                                    } else {
                                        setTimeout(function () {
                                            //dialogRef.close();
                                            dialogRef.enableButtons(true);
                                            dialogRef.setClosable(true);
                                            //$button.disable();
                                            $button.stopSpin(); // Equals to $button.toggleSpin(false);
                                            dialogRef.getModalBody().html('<i class="fa fa-times fail-red"></i>&nbsp;&nbsp;<span style="font-size:14px">Failed!</span>');
                                        }, 3000);
                                    }
                                }
                            });

                        }
                    }, {
                        label: 'Close',
                        action: function (dialogRef) {
                            dialogRef.close();
                        }
                    }]
            });

            /* BootstrapDialog.show({
             type:BootstrapDialog.TYPE_DEFAULT,
             title: 'Finalizing Account',
             message: function(dialog) {
             var $content = $('<body></body>').load('gl_accountperiod_set.jsp?');
             //$('body').on('click', '.thisresult_nd', function(event){
             //    dialog.close();
             //    event.preventDefault();
             //});
             return $content;
             }
             });*/
        });
    });

</script>
<div id ="maincontainer">
    <div class="partition"> 
        <div class="bodyofpartition">

            <div class="row">
                <div class="col-lg-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Step 1 - Verify Voucher and Trial Closing
                        </div>
                        <div class="panel-body">
                            <div class="list-group">
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-bars right-green"></i></i>&nbsp;&nbsp;1. Pre - Posting
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-bars right-green"></i></i>&nbsp;&nbsp;2. Pre - General Ledger Listing
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-bars right-green"></i></i>&nbsp;&nbsp;3. Pre - Trial Balance
                                </a>

                                <a href="#" class="list-group-item" id="checkunpost">
                                    <i class="fa fa-bars right-green"></i></i>&nbsp;&nbsp;4. Check Unpost Transaction
                                </a>
                            </div>
                        </div>
                        <div class="panel-footer">
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Step 2 - Close & Open New Period
                        </div>
                        <div class="panel-body">
                            <div class="list-group">
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-bars right-green"></i></i>&nbsp;&nbsp;5. Prepare Estate's Debit And Credit note
                                </a>
                                <a href="#" class="list-group-item" id="finalize">
                                    <i class="fa fa-bars right-green"></i></i>&nbsp;&nbsp;6. Finalizing Account Data
                                </a>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <em class="fail-red"><i class="fa fa-exclamation-circle"></i><strong> Please backup now before proceed!</strong></em>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Step 3 - Final
                        </div>
                        <div class="panel-body">
                            <div class="list-group">
                                <a href="#" class="list-group-item" id="closeperiod">
                                    <i class="fa fa-bars right-green"></i></i>&nbsp;&nbsp;1. Confirm Close & Create New Period
                                </a>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <em class="fail-red"><i class="fa fa-exclamation-circle"></i><strong> Please backup again!</strong></em>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>